<?php

/*
error_reporting(E_ALL);
ini_set("display_errors", 1);
*/

require_once("../__scripts/ForgEdit_includes.inc");

## SETUP
$eol		= chr(13).chr(10); // Carriage Return
$separator      = ';';
$From		= '2018-01-01';
$Impianti	= array('001BURIM','002BURIM','003BURIM','004BURIM','005BURIM','006BURIM',
                        '007BURIM','008BURIM','009BURIM','010BURIM','011BURIM','012BURIM',
                        '001GEVIM');

## CREDENZIALI FTP
$ftp_directory	= '/formulari/';
$ftp_server	= 'ftp.burgo.com';
$ftp_username	= 'sintem';
$ftp_password	= '2db$7)d5';

/**********************************************************/

function zeroFiller($value,$char){
    $filled = str_pad(substr($value,0,$char), $char, "0", STR_PAD_LEFT);
    return $filled;
}

function spaceFiller($value,$char){
    $toReplace		= array('�','�','�','�','�','�');
    $replaceWith	= array("E'", "E'", "A'", "O'", "I'", "U'");
    $filled		= str_pad(substr(str_replace($toReplace,$replaceWith,$value),0,$char), $char, " ", STR_PAD_RIGHT);
    return $filled;
}

function getCosto($DB, $ID_MOV_F){
    $negative = false;
    $SQL ="SELECT SUM(euro) AS euro FROM user_movimenti_costi WHERE ID_MOV_F=".$ID_MOV_F.";";
    $ECODB=new DbLink();
    $ECODB->DbConn($DB);
    $ECODB->SdbRead($SQL,"DbRecordset",true,false);
    if($ECODB->DbRecordset[0]['euro']<0)
        $negative = true;
    $costo = number_format(abs($ECODB->DbRecordset[0]['euro']), 2, ".", "");	
    $abs = zeroFiller($costo, 10);
    $rv = $negative? '-'.$abs:' '.$abs;
    return $rv;
}

function decodeTipoMovimento($TIPO, $SenzaTrasporto, $PerRiclassificazione){

    // CAS: carico "standard"
    // CAR: carico per riclassificazione
    // SCS: scarico "standard" (fir)
    // SCR: scarico per riclassificazione
    // SCI: scarico interno

    $rv = '';

    if($TIPO=='C')
        $rv = $PerRiclassificazione==1? 'CAR':'CAS';

    if($TIPO=='S'){
        $rv = 'SCS';
        if($PerRiclassificazione==1) $rv = 'SCR';
        if($SenzaTrasporto==1) $rv = 'SCI';
    }

    return $rv;
}

/**********************************************************/

$FromYear = date('Y', strtotime($From));

if($FromYear<date('Y')){
    for($y=$FromYear;$y<=date('Y');$y++)
        $Databases[] = 'soger'.$y;
}
else
    $Databases[] = 'soger'.$FromYear;

$Fields = array('user_movimenti_fiscalizzati.ID_MOV_F', 'user_movimenti_fiscalizzati.ID_IMP', 'lov_cer.COD_CER', 'user_movimenti_fiscalizzati.NMOV', 'user_movimenti_fiscalizzati.TIPO', 'user_movimenti_fiscalizzati.DTMOV', 'user_movimenti_fiscalizzati.pesoN', 'user_movimenti_fiscalizzati.PS_DESTINO', 'user_movimenti_fiscalizzati.tara', 'user_movimenti_fiscalizzati.NFORM', 'user_movimenti_fiscalizzati.SenzaTrasporto', 'user_movimenti_fiscalizzati.PerRiclassificazione', 'user_movimenti_fiscalizzati.COLLI');

$SQL = "";
for($d=0;$d<count($Databases);$d++){
    $SQL.= "(";
    $SQL.= "SELECT '".$Databases[$d]."' AS DB, ". implode(', '.$Databases[$d].'.', $Fields)." ";
    $SQL.= "FROM ".$Databases[$d].'.'."user_movimenti_fiscalizzati ";
    $SQL.= "JOIN ".$Databases[$d].'.'."user_schede_rifiuti ON ";
    $SQL.=$Databases[$d].'.'."user_schede_rifiuti.ID_RIF = ".$Databases[$d].'.'."user_movimenti_fiscalizzati.ID_RIF ";
    $SQL.= "JOIN ".$Databases[$d].'.'."lov_cer ON ";
    $SQL.=$Databases[$d].'.'."lov_cer.ID_CER = ".$Databases[$d].'.'."user_schede_rifiuti.ID_CER ";
    $SQL.= "WHERE ".$Databases[$d].'.'."user_movimenti_fiscalizzati.ID_IMP IN ('".implode('\', \'',$Impianti)."') ";
    $SQL.= "AND ".$Databases[$d].'.'."user_movimenti_fiscalizzati.NMOV<>9999999";
    $SQL.= ")";
    $nextD = $d+1;
    if(array_key_exists($nextD, $Databases))
        $SQL.= " UNION ";
}
$SQL.= " ORDER BY DB, ID_IMP, NMOV";


## CONNECTION TO CURRENT DATABASE
$FEDIT=new DbLink();
$FEDIT->DbConn('soger'.date('Y'));
$FEDIT->SdbRead($SQL,"DbRecordset",true,false);


## FILE LINES
$lines = array();
for($m=0;$m<count($FEDIT->DbRecordset);$m++){
    $line = array();
    $line[] = spaceFiller($FEDIT->DbRecordset[$m]['ID_IMP'], 8);
    $line[] = spaceFiller($FEDIT->DbRecordset[$m]['COD_CER'], 6);
    $line[] = decodeTipoMovimento($FEDIT->DbRecordset[$m]['TIPO'],
    $FEDIT->DbRecordset[$m]['SenzaTrasporto'],$FEDIT->DbRecordset[$m]['PerRiclassificazione']);
    $line[] = $FEDIT->DbRecordset[$m]['DTMOV'];
    $line[] = zeroFiller(number_format($FEDIT->DbRecordset[$m]['pesoN'], 2, ".", ""), 11);
    $line[] = zeroFiller(number_format($FEDIT->DbRecordset[$m]['PS_DESTINO'], 2, ".", ""), 11);
    $line[] = zeroFiller(number_format($FEDIT->DbRecordset[$m]['tara'], 2, ".", ""), 11);
    $line[] = spaceFiller($FEDIT->DbRecordset[$m]['NFORM'], 14);
    $line[] = getCosto($FEDIT->DbRecordset[$m]['DB'], $FEDIT->DbRecordset[$m]['ID_MOV_F']);
    $line[] = zeroFiller($FEDIT->DbRecordset[$m]['COLLI'], 11);
    $lines[] = implode($separator, $line);
}

## SCRITTURA FILE .TXT
$fileContent    = implode($eol, $lines);
$file           = "movimentisoger.txt";
$fp		= fopen($file, 'a+');
file_put_contents($file, $fileContent);
fclose($fp);

## PUBBLICAZIONE SU AREA FTP
$connessione = ftp_connect($ftp_server);
$login = ftp_login($connessione, $ftp_username, $ftp_password);
ftp_chdir($connessione, $ftp_directory);
ftp_pasv($connessione, TRUE);
ftp_put($connessione, $file, $file, FTP_ASCII);
ftp_close($connessione);

## ELIMINO FILE LOCALE
unlink($file);

?>