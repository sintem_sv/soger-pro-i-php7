 <?php
require_once("../../gmaps/Conections/conectarse_db.php");
//require_once("../../../soger/gmaps/Conections/conectarse_db.php");
require_once("../../__SIS/SIS_SOAP.php");

class Cataloghis extends DbConnector{
    
    private $localCataloghi = Array() ;
    private $lineCataloghi  = Array();
    private $xmlConn;
    private $listaConfronto = Array();
    private $conn;


    public function __construct(){

            $this->xmlConn = new SIS_SOAP("paolo.vaccaneo2960");
            $this->conn = new DbConnector(date('Y'),"soger");
            //DbConnector::DbConnector(date('Y'),"soger");
//            DbConnector::DbConnector("2014","soger");            
            $this->setLineCataloghi();
            $this->setLocalCataloghi();
            $this->confrontoCataloghi();
            
            }

    private function setLineCataloghi(){
            $cataloghi = $this->xmlConn->GetElencoCataloghi();            
            for($c=0; $c<count($cataloghi); $c++){
                    array_push($this->lineCataloghi,array(
                            'catalogo'=>$cataloghi[$c]->catalogo,
                            'versione'=> $cataloghi[$c]->versione,
                            'descrizione'=>$cataloghi[$c]->descrizione));
                            }
    }

    private function setLocalCataloghi(){

            $sql ="SELECT * FROM sis_cataloghi;";
//            $FEDIT->SDBRead($sql,"localCatalog",true,false);
            $sql   = $this->conn->query($sql);
            
            while ($row = $this->conn->fetchArray($sql)){
                array_push($this->localCataloghi,$row);
                }
    }

    public function getLineCataloghi(){
        return $this->lineCataloghi;
    }

    public function getLocalCataloghi(){
        return $this->localCataloghi;
    }
    

    public function getListaCataloghiLineHtml(){

        echo "<hr>LISTA IN SISTRI<hr/>";
        for( $i = 0; $i < count($this->lineCataloghi); $i++){           
            echo "Catalogo ".$this->lineCataloghi[$i]['catalogo']." -- ";
            echo "Versione ".$this->lineCataloghi[$i]['versione']." -- ";
            echo "Descrizione ".$this->lineCataloghi[$i]['descrizione']." -- ";
            echo "<br/><br/>";
            }
    }


    public function getListaCataloghiLocalHtml(){

        echo "<hr>LISTA IN LOCALE<hr/>";
        for( $i = 0; $i < count($this->localCataloghi); $i++){           
            echo "Catalogo ".$this->localCataloghi[$i]['catalogo']." -- ";
            echo "Versione ".$this->localCataloghi[$i]['versione']." -- ";
            echo "Descrizione ".$this->localCataloghi[$i]['descrizione']." -- ";
            echo "<br/><br/>";
            }
    }


    private function confrontoCatalogo($catalogoSistri,$versionSistri){

            $versioneLocal="";
            for( $c=0; $c<count($this->localCataloghi); $c++ ){
                    
                    if  ( $catalogoSistri == $this->localCataloghi[$c]['catalogo'] ) {
                                        $versioneLocal = $this->localCataloghi[$c]['versione'];
                                        }
                    }
              
             if ( $versioneLocal == $versionSistri ) return "1";  //stesso
             if ( $versioneLocal != $versionSistri AND $versioneLocal != "" ) return "2"; // diverso
             if ( $versioneLocal != $versionSistri AND $versioneLocal == "" ) return "3"; //catalogo inesistente

             }


    private function confrontoCataloghi(){

          for ( $i=0; $i<count($this->lineCataloghi); $i++) {
               $lista = $this->confrontoCatalogo($this->lineCataloghi[$i]['catalogo'],$this->lineCataloghi[$i]['versione']);
               array_push($this->listaConfronto,array(
                                'Catalogo'=>$this->lineCataloghi[$i]['catalogo'],
                                'Confronto'=>$lista
                                        ));
               }              
      }

      public function getListaConfronto(){
          return $this->listaConfronto;
      }

      public function getListaConfrontoHTML(){

          $return="";
          $listaConfronto = $this->listaConfronto;

          for ( $c = 0; $c<count($listaConfronto); $c++ ){

                $confronto  = $listaConfronto[$c]['Confronto'];
                switch ($confronto) {

                        case "1":  // STESSO
                                   $return .= $listaConfronto[$c]['Catalogo'];
                                   $return .= " --- Catalogo stesso <br/>";
                            break;
                        case "2":  // DIVERSO
                                   $return .= $listaConfronto[$c]['Catalogo'];
                                   $return .= " --- <span style='color:red'> Catalogo diverso</span> <br/>";
                            break;
                        case "3":  // INESISTENTE
                                   $return .= $listaConfronto[$c]['Catalogo'];
                                   $return .= " --- <span style='color:red'>Catalogo inesistente</span> <br/>";
                            break;
                        }
                }

          return $return;
      }

      public function aggiornaCataloghi(){
                  
                  $listaConfronto = $this->listaConfronto;
                  $repilogo = array();

                  for ( $c = 0; $c<count($listaConfronto); $c++ ){

                        $confronto  = $listaConfronto[$c]['Confronto'];
                        switch ($confronto) {
                              case "1":  // STESSO
                                    array_push($repilogo,array(
                                            'Catalogo' => $listaConfronto[$c]['Catalogo'],
                                            'Stato' => "Mantiene Versione",
                                            ));
                                    break;
                                case "2":  // DIVERSO
                                       $aggiornamento = $this->aggiornaCatalogo($listaConfronto[$c]['Catalogo']);
                                       
                                            if ( $aggiornamento==true ){
                                                    $stato = "<span style='color:red'>Catalogo Aggiornato</span>";
                                                    }else{
                                                            $stato = "<span style='color:red'>Problemi nel aggiornamento</span>";}

                                       array_push($repilogo,array(
                                                'Catalogo' => $listaConfronto[$c]['Catalogo'],
                                                'Stato' => $stato,
                                                ));
                                    break;
                                case "3":  // INESISTENTE

                                        $sqlArray = $this->CreateCatalogoFrom_SIS($listaConfronto[$c]['Catalogo']);
                                        $sqlCreate = $sqlArray['create'];
                                        $sqlInsert = $sqlArray['insert'];
                                        $create =  $this->conn->query($sqlCreate);
                                        $create =  $this->conn->query($sqlInsert);
                                        if ( $create == true ){                                             
                                             $insert_sisCataloghi  = "INSERT INTO sis_cataloghi (`catalogo`, `versione`, `descrizione`) VALUES (";
                                             $insert_sisCataloghi .= "'".  addslashes($listaConfronto[$c]['Catalogo'])."',";
                                             $insert_sisCataloghi .= $this->getVersioneCatalogo_SIS($listaConfronto[$c]['Catalogo']).",";
                                             $insert_sisCataloghi .= "'". addslashes($this->getDescrizioneCatalogo_SIS($listaConfronto[$c]['Catalogo']))."');";
                                             $this->conn->query($insert_sisCataloghi);

                                                array_push($repilogo,array(
                                                        'Catalogo' => $listaConfronto[$c]['Catalogo'],
                                                        'Stato' => "<span style='color:red'>INESISTENTE, appena creato</span>",
                                                        ));                                            
                                        }else{
                                                array_push($repilogo,array(
                                                        'Catalogo' => $listaConfronto[$c]['Catalogo'],
                                                        'Stato' => "<span style='color:red'>INESISTENTE, problemi per creare</span>",
                                                        ));                                            
                                                        }
                                    break;
                                }
                        }

                        return $repilogo;

      }

      public function aggiornaCatalogo($catalogo){

            $drop = $this->eliminaCatalogo($catalogo);
            if ( $drop == true ){
                            $sqlArray = $this->CreateCatalogoFrom_SIS($catalogo); // retorna una array con el sql create e insert
                            $sqlCreate = $sqlArray['create'];
                            $sqlInsert = $sqlArray['insert'];
                            $create =  $this->conn->query($sqlCreate);
                            $create =  $this->conn->query($sqlInsert);
                            if ( $create == true ){
                                 $updateTbl_sisCataloghi = "UPDATE sis_cataloghi SET `versione` = '".$this->getVersioneCatalogo_SIS($catalogo)."' WHERE catalogo = '".addslashes($catalogo)."'";
                                 return $this->conn->query($updateTbl_sisCataloghi);
                                 }else{
                                        $this->fatalError("Non è stato possibile recreare  il ".$catalogo." dopo di essere eliminato");
                                        }
                }else{
                        $this->fatalError("Non è stato possibile eliminare ed aggiornare il catalogo".$catalogo);
                        }
      }


      private function eliminaCatalogo($catalogo){
          $sql = "DROP TABLE sis_".$catalogo;   		  
          return $this->conn->query($sql);
      }


      public function CreateCatalogoFrom_SIS($catalogo){ // retorna una array con el sql create e insert

            $catalogoSIS = $this->xmlConn->GetCatalogo(array("catalogo"=>$catalogo));
            $catalogoSIS = new SimpleXMLElement($catalogoSIS);
            $tbl_nome     = $catalogoSIS->identificativo;
            $versione     = $catalogoSIS->versione;
            $descrizione  = $catalogoSIS->descrizione;

            $create = "CREATE TABLE sis_".$tbl_nome."(";
            $insert = " INSERT INTO `sis_".$tbl_nome."`(";

            for($f=0; $f<count($catalogoSIS->records->record[0]->field); $f++){
                            $tipo   = $catalogoSIS->records->record[0]->field[$f]->tipo;
                            $nome   = addslashes($catalogoSIS->records->record[0]->field[$f]->nome);
                            $valore = $catalogoSIS->records->record[0]->field[$f]->valore;
                            $insert .= ($f != (count($catalogoSIS->records->record[0]->field)-1) )?"`".$nome."`,":"`".$nome."`)";
                            if($tipo == "string" || $tipo == "integer" ){

                                    switch ($tipo) {
                                            case "string":
                                                        $create .= ($f != (count($catalogoSIS->records->record[0]->field)-1))? $nome." VARCHAR(255) NOT NULL,":$nome." VARCHAR(255) NOT NULL";
                                                break;
                                            case "integer":
                                                        $create .= ($f != (count($catalogoSIS->records->record[0]->field)-1))? $nome." INT(11) NOT NULL,":$nome." INT(11) NOT NULL";
                                                break;
                                                }
                                    }else{
                                            $this->fatalError("C'è un nuovo paramtro verificare = ".$tipo."<br/>");
                                            }
                            }
                            
            $create .= ")comment='".$descrizione."';";
            $val="";

            for($rs=0; $rs<count($catalogoSIS->records); $rs++){
                for($r=0; $r<count($catalogoSIS->records->record); $r++){
                    $val .= "(";
                    for($f=0; $f<count($catalogoSIS->records->record[$r]->field); $f++){
                            if($r!=(count($catalogoSIS->records->record)-1)){
                                $val .= ($f!=(count($catalogoSIS->records->record[$r]->field)-1))? "'".addslashes($catalogoSIS->records->record[$r]->field[$f]->valore)."'," : "'".addslashes($catalogoSIS->records->record[$r]->field[$f]->valore)."')," ;
                                }else{
                                      $val .= ($f!=(count($catalogoSIS->records->record[$r]->field)-1))? "'".addslashes($catalogoSIS->records->record[$r]->field[$f]->valore)."'," : "'".addslashes($catalogoSIS->records->record[$r]->field[$f]->valore)."')" ;
                                      }
                            }
                }
            }
            $insert .= " VALUES ".$val.";";
            
            return array("create"=>$create,"insert"=>$insert);

      }

     public function getVersioneCatalogo_SIS($catalogo){

          for ( $i=0; $i<count($this->lineCataloghi); $i++) {
               if ( $catalogo == $this->lineCataloghi[$i]['catalogo'] ){
                        return $this->lineCataloghi[$i]['versione'];
                        }
               }
        }

     public function getDescrizioneCatalogo_SIS($catalogo){

          for ( $i=0; $i<count($this->lineCataloghi); $i++) {
               if ( $catalogo == $this->lineCataloghi[$i]['catalogo'] ){
                        return $this->lineCataloghi[$i]['descrizione'];
                        }
               }
        }

      protected function fatalError($msn){
          $this->sendEmail($msn);
          die();
      }

      public function sendEmailAggiorna($agg){
            $m = "
                <tr>
                <th>CATALOGO</th>
                <th>STATO</th>
                </tr>
                ";

            for($i = 0; $i<count($agg); $i++){
                $m.= "<tr><td>" . $agg[$i]["Catalogo"]. "</td>";
                $m.= "<td>" .$agg[$i]["Stato"]. "</td></tr>";
                }
            $this->sendEmail($m);
     }





      public function sendEmail($msn){
            
            $to = "programmazione2@sintem.it, programmazione@sintem.it";
            //$to = "programmazione2@sintem.it";
            $subject = "Confronto Cataloghi - SIS";

            $message = "
            <html>
            <head>
            <title>HTML email</title>
            </head>
            <body>
            <p>Confronto dei cataloghi SIS</p>
            <table>
         
            ".$msn."
           
            </table>
            </body>
            </html>
            ";

            // Always set content-type when sending HTML email
            $headers  = "From: SINTEM-support<cron@sogerpro.it>\n";
            $headers .= "Reply-To: noreply@sogerpro.it\n";
            $headers .= "MIME-Version: 1.0" . "\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
			
            mail($to,$subject,$message,$headers);          
      }


    
}
//
//error_reporting(E_ALL);
//ini_set("display_errors", 1);


//$cron = new confrontoCataloghi();
//$agg = $cron->aggiornaCataloghi();
//$cron->sendEmailAggiorna($agg);



/*
$insertTbl_sisCataloghi = "INSERT INTO sis_cataloghi (`catalogo`, `versione`, `descrizione`) VALUES (";
$insertTbl_sisCataloghi = "'".  addslashes($listaConfronto[$c]['Catalogo'])."','""'";
$insertTbl_sisCataloghi = ")";
*/


?>
