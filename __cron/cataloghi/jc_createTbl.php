 <?php

require_once("SIS_SOAP.php");

//header("Content-type: text/xml");
//$s = new SIS_SOAP("paolo.vaccaneo2960");
$s = new SIS_SOAP("paolo.vaccaneo2960");

try {

    $xmlcataloghi = $s->GetElencoCataloghi();

    $ListaCataloghi ="";

    

    for($c=0; $c<count($xmlcataloghi); $c++){

        $ListaCataloghi .= ($c+1)." .- ".$xmlcataloghi[$c]->catalogo."<br/>";
        
        $xmlcatalog = $s->GetCatalogo(array("catalogo"=>$xmlcataloghi[$c]->catalogo));
        $catalog = new SimpleXMLElement($xmlcatalog);
        $tbl_nome     = $catalog->identificativo;
        $versione     = $catalog->versione;
        $descrizione  = $catalog->descrizione;

        echo "-- CREATE - ".$tbl_nome." ---<br/>";
        $create = "CREATE TABLE sis_".$tbl_nome."(";
        $insert = "INSERT INTO `sis_".$tbl_nome."`(";

        for($f=0; $f<count($catalog->records->record[0]->field); $f++){
                        $tipo   = $catalog->records->record[0]->field[$f]->tipo;
                        $nome   = addslashes($catalog->records->record[0]->field[$f]->nome);
                        $valore = $catalog->records->record[0]->field[$f]->valore;
                        $insert .= ($f != (count($catalog->records->record[0]->field)-1) )?"`".$nome."`,":"`".$nome."`)";

                        if($tipo == "string" || $tipo == "integer" ){

                                switch ($tipo) {
                                    case "string":
                                                $create .= ($f != (count($catalog->records->record[0]->field)-1))? $nome." VARCHAR(255) NOT NULL,":$nome." VARCHAR(255) NOT NULL";
                                        break;
                                    case "integer":
                                                $create .= ($f != (count($catalog->records->record[0]->field)-1))? $nome." INT(11) NOT NULL,":$nome." INT(11) NOT NULL";
                                        break;
                                }
                        }else{die("Occhio queste è un nuovo paramtro = ".$tipo."<br/>");}
                        }

        $create .= ")comment='".$descrizione."';";
        echo $create;

        echo "<br/>-- INSERT --".$tbl_nome."<br/>";
        $val="";

        for($rs=0; $rs<count($catalog->records); $rs++){
            for($r=0; $r<count($catalog->records->record); $r++){
                $val .= "(";
                for($f=0; $f<count($catalog->records->record[$r]->field); $f++){
                        if($r!=(count($catalog->records->record)-1)){
                            $val .= ($f!=(count($catalog->records->record[$r]->field)-1))? "'".addslashes($catalog->records->record[$r]->field[$f]->valore)."'," : "'".addslashes($catalog->records->record[$r]->field[$f]->valore)."')," ;
                            }else{
                                  $val .= ($f!=(count($catalog->records->record[$r]->field)-1))? "'".addslashes($catalog->records->record[$r]->field[$f]->valore)."'," : "'".addslashes($catalog->records->record[$r]->field[$f]->valore)."')" ;
                                  }
                        }
            }
        }

        $insert .= " VALUES ".$val.";";

        echo $insert;

        echo "<br/><br/><br/>";

        }

        echo "<br/><hr/> Lista dei Cataloghi<hr/><br/>".$ListaCataloghi;
    
} catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n";
}


?>