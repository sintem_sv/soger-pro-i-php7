<?php

/*
error_reporting(E_ALL);
ini_set("display_errors", 1);
*/

function zeroFiller($value,$char){
	$filled = str_pad(substr($value,0,$char), $char, "0", STR_PAD_LEFT);
	return $filled;
	}

require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/STATS_funct.php");

$ImpUniTo = array('Acc', 'Ade', 'Aes', 'Aga', 'Agc', 'Akr1', 'Akr2', 'Akz', 'Alb', 'Ale', 'Alp', 'Alq4', 'Als1', 'Als2', 'Als3', 'Als4', 'Als5', 'Alu', 'Ama', 'Ama2', 'Amg2', 'Amk', 'Arc', 'Ark1', 'Ark2', 'Ask1', 'Ask2', 'Ate1', 'Ate2', 'Ate3', 'Ate4', 'Aut1', 'Aut2', 'Aut3', 'Aut4', 'Avi', 'Avi2', 'Avi3', 'Avi4', 'Avi5', 'Avi7', 'Azi1', 'Azi2', 'Azi3', 'Bat', 'Bax', 'Bay', 'Bcc', 'Ben1', 'Ben2', 'Bil', 'Bit', 'Blt', 'Bot1', 'Bot2', 'Bot3', 'Bot4', 'Bot5', 'Bre1', 'Bre2', 'Bre3', 'Bre4', 'Bre5', 'Bre6', 'Bre7', 'Bri1', 'Bri2', 'Bsf1', 'Bsf3', 'Bsf4', 'Bsf6', 'Bsh1', 'Bsh2', 'Bsh3', 'Bsh5', 'Bsh7', 'Bur1', 'Bur10', 'Bur11', 'Bur12', 'Bur2', 'Bur3', 'Bur4', 'Bur5', 'Bur6', 'Bur7', 'Bur8', 'Bur9', 'Caf1', 'Caf3', 'Caf4', 'Caf5', 'Caf6', 'Caf7', 'Cam', 'Cen', 'Cer', 'Cfp1', 'Cfp2', 'Cgc', 'Cia', 'Cgi', 'Cli1', 'Cli2', 'Cli3', 'Cli4', 'Cls', 'Cms', 'Coq', 'Cor1', 'Cov', 'Cpx', 'Cra1', 'Cra2', 'Cra3', 'Cro', 'Csa', 'Csm', 'Dab1', 'Dab2', 'Dab3', 'Dab4', 'Dab5', 'Dem', 'Det', 'Dif', 'Dih', 'Div', 'Drv1', 'Drv2', 'Drv3', 'Drv4', 'Drv5', 'Dsm', 'Dvl1', 'Dyt1', 'Dyt2', 'Dyt3', 'Eat', 'Eco1', 'Eig1', 'Eig3', 'Ela1', 'Ela2', 'Elb', 'Ele1', 'Ele2', 'Els', 'Els1', 'Els10', 'Els11', 'Els12', 'Els13', 'Els14', 'Els15', 'Els16', 'Els17', 'Els4', 'Els5', 'Els7', 'Els8', 'Elx', 'Emo', 'End1', 'Eoc', 'Era', 'Err', 'Eud1', 'Eur', 'Evi1', 'Evi2', 'Evi3', 'Evo1', 'Evo2', 'Evo3', 'Evo6', 'Ewa', 'Fab1', 'Fab2', 'Fbl', 'Fca1', 'Fca2', 'Fca3', 'Fca4', 'Fca5', 'Fca6', 'Fca7', 'Fcu', 'Fdp', 'Fed1', 'Fed2', 'Fed3', 'Fed4', 'Fed5', 'Fed6', 'Fed7', 'Fer1', 'Fer2', 'Fis1', 'Fis2', 'Fmg', 'Fon1', 'Fon2', 'For', 'Fot', 'Fri', 'Frl', 'Frr', 'Frs', 'Fru1', 'Fru2', 'Fru3', 'Fru4', 'Fru5', 'Fru6', 'Fru7', 'Gai', 'Gat', 'Gea2', 'Gea3', 'Gea4', 'Gea5', 'Geo', 'Gio1', 'Gio2', 'Gir', 'Gom', 'Gre', 'Grn1', 'Grn2', 'Grn3', 'Grn5', 'Grn6', 'Grn7', 'Ibp', 'Icm1', 'Ico', 'Ics', 'Ide', 'Ies1', 'Ies2', 'Ilc', 'Imm', 'Imp', 'Ine', 'Inm1', 'Ins1', 'Ino', 'Int', 'Inv1', 'Ipb', 'Iri', 'Irm', 'Isa', 'Itt1', 'Itt3', 'Ive1', 'Jac', 'Jsy', 'Kck1', 'Kck2', 'Ker1', 'Ker10', 'Ker11', 'Ker2', 'Ker3', 'Ker4', 'Ker5', 'Ker6', 'Ker7', 'Ker8', 'Krf', 'Lac', 'Leg1', 'Leg2', 'Leg3', 'Mae', 'Map1', 'Map2', 'Mar', 'Mch1', 'Mcn1', 'Mcn2', 'Mcp', 'Mcr1', 'Mcr2', 'Mcr3', 'Mem', 'Mic1', 'Mic2', 'Mic3', 'Mic5', 'Mis', 'Mma', 'Mme', 'Moz', 'Mrc', 'Mrp', 'Msr', 'Mst', 'Mwb', 'New1', 'New2', 'New3', 'New4', 'Nin1', 'Nin2', 'Nol1', 'Ntd', 'Oek1', 'Oek2', 'Oek3', 'Oek4', 'Oek5', 'Ofr', 'Ols1', 'Ols2', 'Ols3', 'Olz', 'Omg1', 'Omg2', 'Omv', 'Tek2', 'Ook', 'Ore1', 'Ore2', 'Par', 'Par1', 'Par10', 'Par12', 'Par13', 'Par14', 'Par15', 'Par2', 'Par3', 'Par4', 'Par5', 'Par6', 'Par7', 'PAr8', 'Par9', 'Pep', 'Pik', 'Pin1', 'Pin2', 'Pin3', 'Pin4', 'Pka', 'Ply', 'Prf', 'Qds', 'Rdm', 'Rem1', 'Rem2', 'Rft1', 'Rft2', 'Rie', 'Rub1', 'Rub2', 'Sab2', 'Sac', 'Sad1', 'Sad2', 'San1', 'San2', 'San3', 'Sat1', 'Sat2', 'Sat3', 'Sed1', 'Sed2', 'Set', 'She', 'Sic', 'Sip1', 'Skf3', 'Skf1', 'Skf2', 'Skf4', 'Skf5', 'Skf6', 'Skf7', 'Sme', 'Smm1', 'Smm2', 'Sol', 'Som1', 'Som2', 'Sor', 'Srg', 'Sry', 'Ssm', 'Sta', 'Ste1', 'Sto1', 'Sto2', 'Str2', 'Sun', 'Svs', 'Swh', 'Syn', 'Tak', 'Teb', 'Tek1', 'Tes1', 'Thu', 'Tie11', 'Tie12', 'Tie13', 'Tie9', 'Tit', 'Tob', 'Tor', 'Tri', 'Trw', 'Unc1', 'Unc2', 'Unc4', 'Unf1', 'Unf3', 'Uni', 'Val1', 'Val4', 'Veb', 'Vem', 'Ver', 'Vey', 'Via', 'Vim1', 'Vim2', 'Vim3', 'Vim4', 'Vim5', 'Vin1', 'Vin2', 'Vir2', 'Vis', 'Wac', 'Was', 'Wiz', 'Wur', 'Zad1', 'Zad2', 'Zaf', 'Zam', 'Zan', 'Zch', 'Zfp');

$CSVbuf		= '';

## CONNECTION TO CURRENT DATABASE
$FEDIT = new DbLink();

for($i=0;$i<count($ImpUniTo);$i++){
	if(strlen($ImpUniTo[$i])==3)
		$ID_IMP = '001'.strtoupper($ImpUniTo[$i]).'IM';
	else
		$ID_IMP = zeroFiller(substr($ImpUniTo[$i], 3), 3) . strtoupper(substr($ImpUniTo[$i], 0, 3)) . 'IM';

	$CSVbuf .= AddCSVcolumn($ID_IMP);

	for($y=2010;$y<=2016;$y++){
		$FEDIT->DbConn('soger'.$y);
		
		$SQL = "SELECT ID_IMP FROM user_movimenti_fiscalizzati WHERE ";
		$SQL.= "ID_IMP='".$ID_IMP."' AND produttore=1 AND TIPO='C' AND PerRiclassificazione=0 AND DTMOV IS NOT NULL ";
		$SQL.= "AND DTMOV<>'0000-00-00' ";
		$SQL.= "LIMIT 0,1";
		$FEDIT->SdbRead($SQL,"Checker",true,false);
		if(is_null($FEDIT->Checker)){
			for($m=1;$m<=12;$m++)
				$CSVbuf .= AddCSVcolumn(0);
		}
		else{
			$SQL = "SELECT ";
			$SQL.= "SUM(IF(MONTH(DTMOV) = 1, pesoN, 0)) AS gennaio, ";
			$SQL.= "SUM(IF(MONTH(DTMOV) = 2, pesoN, 0)) AS febbraio, ";
			$SQL.= "SUM(IF(MONTH(DTMOV) = 3, pesoN, 0)) AS marzo, ";
			$SQL.= "SUM(IF(MONTH(DTMOV) = 4, pesoN, 0)) AS aprile, ";
			$SQL.= "SUM(IF(MONTH(DTMOV) = 5, pesoN, 0)) AS maggio, ";
			$SQL.= "SUM(IF(MONTH(DTMOV) = 6, pesoN, 0)) AS giugno, ";
			$SQL.= "SUM(IF(MONTH(DTMOV) = 7, pesoN, 0)) AS luglio, ";
			$SQL.= "SUM(IF(MONTH(DTMOV) = 8, pesoN, 0)) AS agosto, ";
			$SQL.= "SUM(IF(MONTH(DTMOV) = 9, pesoN, 0)) AS settembre, ";
			$SQL.= "SUM(IF(MONTH(DTMOV) = 10, pesoN, 0)) AS ottobre, ";
			$SQL.= "SUM(IF(MONTH(DTMOV) = 11, pesoN, 0)) AS novembre, ";
			$SQL.= "SUM(IF(MONTH(DTMOV) = 12, pesoN, 0)) AS dicembre ";
			$SQL.= "FROM user_movimenti_fiscalizzati WHERE ";
			$SQL.= "ID_IMP='".$ID_IMP."' AND produttore=1 AND TIPO='C' AND PerRiclassificazione=0 AND DTMOV IS NOT NULL ";
			$SQL.= "AND DTMOV<>'0000-00-00';";
			
			$FEDIT->SdbRead($SQL,"DbRecordset",true,false);
		
			$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordset[0]['gennaio']);
			$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordset[0]['febbraio']);
			$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordset[0]['marzo']);
			$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordset[0]['aprile']);
			$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordset[0]['maggio']);
			$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordset[0]['giugno']);
			$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordset[0]['luglio']);
			$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordset[0]['agosto']);
			$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordset[0]['settembre']);
			$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordset[0]['ottobre']);
			$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordset[0]['novembre']);
			$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordset[0]['dicembre']);	
		}
	}
	$CSVbuf .= AddCSVRow();
}

CSVOut($CSVbuf, 'Dati_Mensili');

?>