<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html><head><title>Soger - <?php echo $_SESSION["SqlDescription"]; ?></title>
<link href="../__css/SOGERupdates.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<?php
echo "<h1>" . $_SESSION["SqlDescription"] . "</h1>";
ob_end_flush();
set_time_limit(0);
require("../__libs/SQLFunct.php");
require_once("COMMON_ForgEditClassFiles.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
if(!$FEDIT->DBConn($_SESSION["SqlDb"])) {
	die("problema di connessione");	
}
?>
<?php
#
foreach($_SESSION["SqlFiles"] as $des=>$data) {
	# chiave "sql" -> comando sql diretto
	#die(substr($data[0],0,3));
	if(substr($data[0],0,3)=="sql") {
		echo "<h2>" . substr($data[0],4) . "</h2>";
		$FEDIT->SDBWrite($data[1],true,false,$data[2]);
		for($x=0;$x<50;$x++) {
			echo "<img src=\"../__css/table_add.png\" style=\"float: left;\" alt=\"working\"/>";			
		}
		echo "<img src=\"../__css/tick.png\" style=\"float: left;\" alt=\"done\"/>";
	} else {
		import_sql($data,$_SESSION["SqlPath"],$FEDIT,$data[3]);
	}
}
unset($_SESSION["SqlDb"]);
unset($_SESSION["SqlDescription"]);
unset($_SESSION["SqlFiles"]);
unset($_SESSION["SqlPath"]);
?>
<h2>Esecuzione terminata: <a href="../" title="login" style="color: yellow">torna alla pagina di login</a></h2>
</body>
</html>