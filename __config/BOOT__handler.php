<?php
	#
	#	tenta di aprire SOGERxxxx (xxxx -> anno corrente)
	#
	
	$sql = "SHOW databases;";
	$FEDIT->SDBRead($sql,"DbRecordset");
	$AvailableDB = array();
	foreach($FEDIT->DbRecordset as $k=>$Db) {
		$AvalialbeDB[] = $Db["Database"];
		}

	if(!in_array("soger" . date("Y"), $AvalialbeDB)){
		
		$LastYearDb = "soger" . ((int) ($tmp = date("Y")-1));
		$ActualDb	= "soger" . date("Y");

		if(!$FEDIT->DbConn($LastYearDb)) {
			## creazione database da zero, no duplicazione anno precedente
			$FEDIT->SDBWrite("CREATE DATABASE " . $ActualDb . " DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci",true,false,false);
			$_SESSION["SqlDb"] = $ActualDb; 
			$_SESSION["SqlDescription"] = "Preparazione alla prima esecuzione di soger dell'anno.";
			$_SESSION["SqlPath"] = "__config";
			$_SESSION["SqlFiles"] = array(
				array("Creazione tabella versione database","databaseVer.struct",false,false),
				array("Installazione dati versione database","databaseVer.data",false,false),				
				array("Creazione tabelle utenti","UserTables.struct",false,false),
				array("Creazione amministratore di sistema","Administrator.data",false,false),
				array("Creazione tabella comuni","comuni.struct",false,false),
				array("Installazione lista comuni","comuni.data",false,false),
				array("Creazione tabelle procedure di sicurezza","schede_sicurezza.struct",false,false),
				array("Installazione dati procedure di sicurezza","schede_sicurezza.data",false,false),
				array("Creazione tabella codici CER","cer.struct",false,false),
				array("Installazione lista codici CER","cer.data",false,false),
				array("Creazione tabelle liste valori generiche","lov.struct",false,false),
				array("Istallazione liste valori generiche","lov.data",false,false),
				array("Creazione tabelle liste valori caratterizzazione rifiuto","lov_caratterizzazione.struct",false,false),
				array("Istallazione liste valori caratterizzazione rifiuto","lov_caratterizzazione.data",false,false),
				array("Creazione tabella numeri ONU","onu.struct",false,false),
				array("Installazione lista numeri ONU","onu.data",false,false),
				array("Creazione tabelle anagrafiche e movimenti","anagrafiche.struct",false,false),				
				array("Applicazione criteri di relazione","FKpolicies.const",false,false)
				# le tabelle eco vengono installate "a mano" al momento
				);
			ob_end_flush();
			header("location: __config/DBtalk.php");
			}
		else {

			#
			#	se non esiste il db dell'anno corrente, ma ne esiste uno dell'anno precedente
			#	viene eseguita una copia
			#

			#
			#   controllo se in anno passato erano presenti tabelle eco
			#
			$FEDIT->SDBRead("SHOW TABLES FROM ".$LastYearDb,"DbRecordset",true,false);
			for($t=0;$t<count($FEDIT->DbRecordset);$t++)
				$tables_ly[]=$FEDIT->DbRecordset[$t]['Tables_in_'.$LastYearDb];
			if(!in_array("user_movimenti_costi", $tables_ly))
				$_SESSION['econ_ly']=false;
			else
				$_SESSION['econ_ly']=true;

			# aggiorno la disponibilitÓ di tutti i rifiuti
			$sqlDisponibilita="SELECT ID_RIF FROM $LastYearDb.user_schede_rifiuti";
			$FEDIT->SDBRead($sqlDisponibilita,"DispoRif",true,false);
			for($r=0;$r<count($FEDIT->DispoRif);$r++){
				$IMP_FILTER=false;
				$DISPO_PRINT_OUT = false;
				$IDRIF = $FEDIT->DispoRif[$r]['ID_RIF'];
				$TableName	= "user_movimenti_fiscalizzati";
				$IDMov		= "ID_MOV_F";
				$EXCLUDE_9999999	= true;
				require("__scripts/MovimentiDispoRIF.php");
				if($Disponibilita<0) $Disponibilita=0;
				$FEDIT->SDBWrite("UPDATE $LastYearDb.user_schede_rifiuti SET FKEdisponibilita='$Disponibilita' WHERE ID_RIF='" . $FEDIT->DispoRif[$r]['ID_RIF'] . "'",true,false);
				}

			$FEDIT->SDBWrite("CREATE DATABASE $ActualDb DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci",true,false,false);
			$_SESSION["SqlDb"] = $ActualDb;
			$_SESSION["SqlDescription"] = "Copia del database dell'anno precedente";
			$_SESSION["SqlPath"] = "__config";
			
			$_SESSION["SqlFiles"] = array(
				array("sql Preparazione alla copia","SET FOREIGN_KEY_CHECKS=0",false,false),
				array("Creazione tabelle utenti","UserTables.struct",false,false),
				array("sql Copia dei dati - intestatari","INSERT INTO $ActualDb.core_intestatari SELECT * FROM $LastYearDb.core_intestatari",false,false),
				array("sql Copia dei dati - gruppi","INSERT INTO $ActualDb.core_gruppi SELECT * FROM $LastYearDb.core_gruppi",false,false),
				array("sql Copia dei dati - impianti","INSERT INTO $ActualDb.core_impianti SELECT * FROM $LastYearDb.core_impianti",false,false),
				array("sql Copia dei dati - impianti ISO","INSERT INTO $ActualDb.core_impianti_iso SELECT * FROM $LastYearDb.core_impianti_iso",false,false),
				array("sql Copia dei dati - impianti non conformita'","INSERT INTO $ActualDb.core_impianti_nc SELECT * FROM $LastYearDb.core_impianti_nc",false,false),
				array("sql Copia dei dati - utenti","INSERT INTO $ActualDb.core_users SELECT * FROM $LastYearDb.core_users",false,false),
				array("Creazione tabella comuni","comuni.struct",false,false),
				array("sql Copia dei dati - comuni","INSERT INTO $ActualDb.lov_comuni_istat SELECT * FROM $LastYearDb.lov_comuni_istat",false,false),
				array("Creazione tabella versione database","databaseVer.struct",false,false),
				array("sql Copia dei dati - versione database, part 1 of 2","INSERT INTO $ActualDb.core_dbversion SELECT * FROM $LastYearDb.core_dbversion",false,false),
				array("sql Copia dei dati - versione database, part 2 of 2","INSERT INTO $ActualDb.core_sogerversion SELECT * FROM $LastYearDb.core_sogerversion",false,false),
				array("Creazione tabella codici CER","cer.struct",false,false),
				array("sql Copia dei dati - codici CER","INSERT INTO $ActualDb.lov_cer SELECT * FROM $LastYearDb.lov_cer",false,false),				
				array("sql Copia dei dati - codici CER specchio","INSERT INTO $ActualDb.cer SELECT * FROM $LastYearDb.cer",false,false),
				array("Creazione tabelle anagrafiche e movimenti","anagrafiche.struct",false,false),
				array("sql Copia dei dati - aziende produttori","INSERT INTO $ActualDb.user_aziende_produttori SELECT * FROM $LastYearDb.user_aziende_produttori",false,false),
				array("sql Copia dei dati - aziende destinatari","INSERT INTO $ActualDb.user_aziende_destinatari SELECT * FROM $LastYearDb.user_aziende_destinatari",false,false),
				array("sql Copia dei dati - aziende intermediari","INSERT INTO $ActualDb.user_aziende_intermediari SELECT * FROM $LastYearDb.user_aziende_intermediari",false,false),
				array("sql Copia dei dati - aziende trasportatori","INSERT INTO $ActualDb.user_aziende_trasportatori SELECT * FROM $LastYearDb.user_aziende_trasportatori",false,false),
				array("sql Copia dei dati - impianti produttori","INSERT INTO $ActualDb.user_impianti_produttori SELECT * FROM $LastYearDb.user_impianti_produttori",false,false),
				array("sql Copia dei dati - impianti trasportatori","INSERT INTO $ActualDb.user_impianti_trasportatori SELECT * FROM $LastYearDb.user_impianti_trasportatori",false,false),
				array("sql Copia dei dati - impianti destinatari","INSERT INTO $ActualDb.user_impianti_destinatari SELECT * FROM $LastYearDb.user_impianti_destinatari",false,false),
				array("sql Copia dei dati - impianti intermediari","INSERT INTO $ActualDb.user_impianti_intermediari SELECT * FROM $LastYearDb.user_impianti_intermediari",false,false),
                                array("sql Copia dei dati - autorizzazioni produttori","INSERT INTO $ActualDb.user_autorizzazioni_pro SELECT * FROM $LastYearDb.user_autorizzazioni_pro",false,false),
				array("sql Copia dei dati - autorizzazioni trasportatori","INSERT INTO $ActualDb.user_autorizzazioni_trasp SELECT * FROM $LastYearDb.user_autorizzazioni_trasp",false,false),
				array("sql Copia dei dati - autorizzazioni destinatari","INSERT INTO $ActualDb.user_autorizzazioni_dest SELECT * FROM $LastYearDb.user_autorizzazioni_dest",false,false),
				array("sql Copia dei dati - autorizzazioni intermediari","INSERT INTO $ActualDb.user_autorizzazioni_interm SELECT * FROM $LastYearDb.user_autorizzazioni_interm",false,false),
				array("sql Copia dei dati - autisti","INSERT INTO $ActualDb.user_autisti SELECT * FROM $LastYearDb.user_autisti",false,false),
				array("sql Copia dei dati - automezzi","INSERT INTO $ActualDb.user_automezzi SELECT * FROM $LastYearDb.user_automezzi",false,false),
				array("sql Copia dei dati - rimorchi","INSERT INTO $ActualDb.user_rimorchi SELECT * FROM $LastYearDb.user_rimorchi",false,false),
				array("sql Copia dei dati - schede rifiuti","INSERT INTO $ActualDb.user_schede_rifiuti SELECT * FROM $LastYearDb.user_schede_rifiuti",false,false),
				array("sql Copia dei dati - deposito rifiuti","INSERT INTO $ActualDb.user_schede_rifiuti_deposito SELECT * FROM $LastYearDb.user_schede_rifiuti_deposito",false,false),
				array("sql Copia dei dati - segnaletica","INSERT INTO $ActualDb.user_schede_rifiuti_etsym SELECT * FROM $LastYearDb.user_schede_rifiuti_etsym",false,false),
				array("sql Copia dei dati - macchinari impiegati","INSERT INTO $ActualDb.user_schede_rifiuti_macchinari SELECT * FROM $LastYearDb.user_schede_rifiuti_macchinari",false,false),
				array("sql Copia dei dati - processi di produzione","INSERT INTO $ActualDb.user_schede_rifiuti_processi SELECT * FROM $LastYearDb.user_schede_rifiuti_processi",false,false),
				array("sql Copia dei dati - verifica giacenze iniziali", "UPDATE $ActualDb.user_schede_rifiuti SET user_schede_rifiuti.giac_ini='0'", false, false),
				array("sql Copia dei dati - importazione giacenze iniziali", "UPDATE $ActualDb.user_schede_rifiuti SET user_schede_rifiuti.giac_ini=user_schede_rifiuti.FKEdisponibilita", false, false),
				array("sql Copia dei dati - costi da applicare ai viaggi","INSERT INTO $ActualDb.user_viaggi_impianti SELECT * FROM $LastYearDb.user_viaggi_impianti",false,false),
				array("sql Copia dei dati - promemoria","INSERT INTO $ActualDb.user_promemoria SELECT * FROM $LastYearDb.user_promemoria",false,false),
				array("Creazione tabelle procedure di sicurezza","schede_sicurezza.struct",false,false),
				array("sql Copia dei dati - procedure di sicurezza","INSERT INTO $ActualDb.user_schede_sicurezza SELECT * FROM $LastYearDb.user_schede_sicurezza",false,false),
				array("sql Copia dei dati - procedure di sicurezza - data_sk_sez2","INSERT INTO $ActualDb.data_sk_sez2 SELECT * FROM $LastYearDb.data_sk_sez2",false,false),
				array("sql Copia dei dati - procedure di sicurezza - data_sk_sez3_oc","INSERT INTO $ActualDb.data_sk_sez3_oc SELECT * FROM $LastYearDb.data_sk_sez3_oc",false,false),
				array("sql Copia dei dati - procedure di sicurezza - data_sk_sez3_pl","INSERT INTO $ActualDb.data_sk_sez3_pl SELECT * FROM $LastYearDb.data_sk_sez3_pl",false,false),
				array("sql Copia dei dati - procedure di sicurezza - data_sk_sez3_resp","INSERT INTO $ActualDb.data_sk_sez3_resp SELECT * FROM $LastYearDb.data_sk_sez3_resp",false,false),
				array("Creazione tabelle caratterizzazione rifiuti","lov_caratterizzazione.struct",false,false),
				array("sql Copia dei dati - caratterizzazione rifiuti - destinazione","INSERT INTO $ActualDb.lov_destinazione_rifiuto SELECT * FROM $LastYearDb.lov_destinazione_rifiuto",false,false),
				array("sql Copia dei dati - caratterizzazione rifiuti - fonte","INSERT INTO $ActualDb.lov_fonte_rifiuto SELECT * FROM $LastYearDb.lov_fonte_rifiuto",false,false),
				array("sql Copia dei dati - caratterizzazione rifiuti - origine","INSERT INTO $ActualDb.lov_origine_rifiuto SELECT * FROM $LastYearDb.lov_origine_rifiuto",false,false),
				array("Creazione tabella ATECO2007","lov_ateco2007.struct",false,false),
				array("sql Copia dei dati - codici ATECO 2007","INSERT INTO $ActualDb.lov_ateco2007 SELECT * FROM $LastYearDb.lov_ateco2007",false,false),
				array("Creazione tabelle per Allegato VII","lov_annex.struct",false,false),
				array("sql Copia dei dati - allegato VII - lov_annex_vii_basilea","INSERT INTO $ActualDb.lov_annex_vii_basilea SELECT * FROM $LastYearDb.lov_annex_vii_basilea",false,false),
				array("sql Copia dei dati - allegato VII - lov_annex_vii_destinatari","INSERT INTO $ActualDb.lov_annex_vii_destinatari SELECT * FROM $LastYearDb.lov_annex_vii_destinatari",false,false),
				array("sql Copia dei dati - allegato VII - lov_annex_vii_iiia","INSERT INTO $ActualDb.lov_annex_vii_iiia SELECT * FROM $LastYearDb.lov_annex_vii_iiia",false,false),
				array("sql Copia dei dati - allegato VII - lov_annex_vii_iiib","INSERT INTO $ActualDb.lov_annex_vii_iiib SELECT * FROM $LastYearDb.lov_annex_vii_iiib",false,false),
				array("sql Copia dei dati - allegato VII - lov_annex_vii_ocse","INSERT INTO $ActualDb.lov_annex_vii_ocse SELECT * FROM $LastYearDb.lov_annex_vii_ocse",false,false),
				array("sql Copia dei dati - allegato VII - lov_annex_vii_organizzatori","INSERT INTO $ActualDb.lov_annex_vii_organizzatori SELECT * FROM $LastYearDb.lov_annex_vii_organizzatori",false,false),
				array("Creazione tabelle liste valori","lov.struct",false,false),
				array("Installazione liste valori","lov.data",false,false),				
				array("Creazione tabella numeri ONU","onu.struct",false,false),
				array("sql Copia dei dati - numeri ONU","INSERT INTO $ActualDb.lov_num_onu SELECT * FROM $LastYearDb.lov_num_onu",false,false),	
				array("Creazione tabelle tool importazione dati","import.struct",false,false),
				array("sql Copia dei dati - Importazioni anno precedente","INSERT INTO $ActualDb.importazioni SELECT * FROM $LastYearDb.importazioni",false,false),
				array("Creazione tabelle interoperabilitÓ Sistri","sistri.struct",false,false),
				array("Installazione liste valori Sistri","sistri.data",false,false),
				array("Creazione tabelle indici di gestione","sdg.struct",false,false),
				array("sql Preparazione al ripristino criteri di relazione","SET FOREIGN_KEY_CHECKS=1",false,false),
				array("Creazione tabelle modulo economico","eco_tables.struct",false,false),
				array("Installazione liste valori modulo economico","eco_tables.data",false,false),
				array("sql Copia dati modulo economico - budget","INSERT INTO $ActualDb.user_contratti_budget SELECT * FROM $LastYearDb.user_contratti_budget",false,false),
				array("sql Copia dati modulo economico - contratti destinatari","INSERT INTO $ActualDb.user_contratti_destinatari SELECT * FROM $LastYearDb.user_contratti_destinatari",false,false),
				array("sql Copia dati modulo economico - condizioni contrattuali destinatari","INSERT INTO $ActualDb.user_contratti_dest_cond SELECT * FROM $LastYearDb.user_contratti_dest_cond",false,false),
				array("sql Copia dati modulo economico - contratti intermediari","INSERT INTO $ActualDb.user_contratti_intermediari SELECT * FROM $LastYearDb.user_contratti_intermediari",false,false),
				array("sql Copia dati modulo economico - condizioni contrattuali intermediari","INSERT INTO $ActualDb.user_contratti_int_cond SELECT * FROM $LastYearDb.user_contratti_int_cond",false,false),
				array("sql Copia dati modulo economico - contratti produttori","INSERT INTO $ActualDb.user_contratti_produttori SELECT * FROM $LastYearDb.user_contratti_produttori",false,false),
				array("sql Copia dati modulo economico - condizioni contrattuali produttori","INSERT INTO $ActualDb.user_contratti_pro_cond SELECT * FROM $LastYearDb.user_contratti_pro_cond",false,false),
				array("sql Copia dati modulo economico - contratti trasportatori","INSERT INTO $ActualDb.user_contratti_trasportatori SELECT * FROM $LastYearDb.user_contratti_trasportatori",false,false),
				array("sql Copia dati modulo economico - condizioni contrattuali trasportatori","INSERT INTO $ActualDb.user_contratti_trasp_cond SELECT * FROM $LastYearDb.user_contratti_trasp_cond",false,false),
			);

			ob_end_flush();
			header("location: __config/DBtalk.php");
		}
	}

	#
	#	gestione aggiornamenti
	#

	$handle = opendir('__updates/'); 
	$files = array();
	while (false !== ($file = readdir($handle))) {
		if(!is_dir($file)) {
			if(substr($file,-3,3)=="upd") {
				//echo substr($file,0,strlen($file)-3)."txt";
				if(file_exists("__updates/" . substr($file,0,strlen($file)-3)."txt")) {
					$des = file_get_contents("__updates/" . substr($file,0,strlen($file)-3)."txt"); 	
					} 
				else {
					$des = "Aggiornamento Soger";	
					}
				$files[] = array($des,$file,false,true);
			}
		}
	}

	$ActualDb = "soger" . date("Y");
	$FEDIT->DbConn($ActualDb);

	if(count($files)>0) {
		$busyWithUpdates = true;		
		$_SESSION["SqlDb"] = $ActualDb; 
		$_SESSION["SqlDescription"] = "Installazione aggiornamenti";
		$_SESSION["SqlPath"] = "__updates";
		$_SESSION["SqlFiles"] = $files;
		ob_end_flush();
		header("location: __config/DBtalk.php");
		}
	else {
		$busyWithUpdates = false;	
		}

	#
	#	carica versione programma
	#
	$sql = "SELECT version from core_dbversion ORDER BY installDate DESC LIMIT 1";
	$FEDIT->SDBRead($sql,"DbRecordset",true,false);
	$_SESSION["DbVer"] = $FEDIT->DbRecordset[0]["version"];

	if(!$busyWithUpdates) {

		## METHOD 1: PARAMS VIA GET (test_envi method)
		if(isset($_GET['usr']) && (strpos($_SERVER['HTTP_REFERER'], 'sogerpro.it')!==false || strpos($_SERVER['HTTP_REFERER'], 'sintem')!==false) ) {
			$_SESSION["AutoLog"]["u"] = $_GET['usr'];
			$_SESSION["AutoLog"]["p"] = "demo"; // !!!
			header("location: __scripts/login.php");
			}

		}

?>