function Regolamento1357Attivo(quando){
	if (typeof(quando)==='undefined') 
		var quando = new Date();
	else{
		dt = quando.split('/');
		var quando = new Date(dt[2], dt[1]-1, dt[0]);
		}
	var EntrataInVigore = new Date(2015, 5, 1); //	1 Giugno 2015 (In Javascript mese parte da 0!)
    var diff = EntrataInVigore.getTime() - quando.getTime();
    if(diff<=0)
		return true;
	else
		return false;
	}

function OpenISODialog(prefix, ID_RIF, status, start, end){
	if(start!=''){
		start_array=start.split("-");
		start=start_array[2]+"/"+start_array[1]+"/"+start_array[0];
		}
	if(end!=''){
		end_array=end.split("-");
		end=end_array[2]+"/"+end_array[1]+"/"+end_array[0];
		}
	$( "#prefix" ).val( prefix );
	$( "#ISO_ID_RIF" ).val( ID_RIF );
	$( "#ISO_status" ).val( status );
	$( "#ISO_start" ).val( start );
	$( "#ISO_end" ).val( end );
	if(status==0){
		$("#ISO_start").prop('disabled', true);
		$("#ISO_end").prop('disabled', true);
		}
	$( "#Popup_ApprovazioneISO" ).dialog("open");
	}

function showDuplicazioneRifiuto(ID_RIF, redirect){
		
	$.post('__scripts/GetRifiutoByID.php', {ID_RIF:ID_RIF}, function(phpResponse){
		$( "#originalID_RIF" ).val(ID_RIF);
		$( "#redirection" ).val(redirect);
		$( "#Popup_DuplicazioneRifiuto" ).dialog('option', 'title', 'Duplicazione della scheda rifiuto: '+phpResponse.descrizione);
		$( "#Popup_DuplicazioneRifiuto" ).dialog("open");
		}, 'json');

	}

function setupAdr(ID_ONU){
	var LKUP = new ajaxObject('__scripts/ONUgetDetails.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		responseTxt = responseTxt.replace(/^\s+|\s+$/g, '');
		var data = responseTxt.split("|");
		
		var etichette = data[4];
		if(etichette.indexOf("5.2.2.1.12") !== -1){
			document.getElementById('user_schede_rifiuti:ID_ETICHETTA_ADR').disabled = false;
		}
		else{
			document.getElementById('user_schede_rifiuti:ID_ETICHETTA_ADR').selectedIndex = 0;
			document.getElementById('user_schede_rifiuti:ID_ETICHETTA_ADR').disabled = true;
		}

		var un = data[0];
		if(un == '3316'){
			document.getElementById('user_schede_rifiuti:ID_IMBALLAGGIO_ADR').disabled = false;
		}
		else{
			document.getElementById('user_schede_rifiuti:ID_IMBALLAGGIO_ADR').selectedIndex = 0;
			document.getElementById('user_schede_rifiuti:ID_IMBALLAGGIO_ADR').disabled = true;
		}

	}
	Parameters = 'ID_ONU=' + ID_ONU;
	LKUP.update(Parameters);
}

function showScaricoRicaricoRifiuto(ID_RIF){
		
	$.post('__scripts/GetRifiutoByID.php', {ID_RIF:ID_RIF}, function(phpResponse){
		$( "#originalID_RIF" ).val(ID_RIF);
		$( "#KG" ).val(phpResponse.KG);
		$( "#KGhidden" ).val(phpResponse.KG);
		$( "select[name=ricaricoID_RIF]" ).val(ID_RIF);
		$( "#Popup_ScaricoRicaricoRifiuto" ).dialog('option', 'title', 'Scarico/Ricarico della scheda rifiuto: '+phpResponse.descrizione);
		$( "#Popup_ScaricoRicaricoRifiuto" ).dialog("open");
		}, 'json');

	}
        
function showRicalcoloGiacenzeFinali(){
    $( "#Popup_RicalcoloGiacenzeFinali" ).dialog('option', 'title', 'Ricalcolo delle giacenze finali');
    $( "#Popup_RicalcoloGiacenzeFinali" ).dialog("open");
    }       

function UpdateApprovazioneISO(prefix){
	var error	= false;
	var msg		= "Impossibile salvare:\n\n"
	if($( "#ISO_status" ).val()==1){
		// devo avere entrambe le date nel formato dd/mm/yyyy...
		regexp = /^(0[1-9]|1[0-9]|2[0-9]|3[01]|[1-9])\/+(0[1-9]|1[0-2]|[1-9])\/+(19|20)[0-9]{2}$/
		if( !$( "#ISO_start" ).val().match(regexp) || !$( "#ISO_end" ).val().match(regexp)  ) {
			error=true;
			msg	+= "- verifica il formato delle date immesse (gg/mm/aaaa)\n";
			}
		//... e con end > start
		var arr1 = $("#ISO_start").val().split("/");
		var arr2 = $("#ISO_end").val().split("/");
		var d1 = new Date(arr1[2],arr1[1]-1,arr1[0]);
		var d2 = new Date(arr2[2],arr2[1]-1,arr2[0]);
		var start = d1.getTime();
		var end = d2.getTime();
		if (end < start){
			error=true;
			msg	+= "- la data di scadenza approvazione deve essere successiva a quella di inizio\n";
			}
		}
	if(error) 
		alert(msg);
	else{
		// salvataggio...
		$.post('__scripts/UpdateISOStatus.php', {prefix:$("#prefix").val(), ID_RIF:$("#ISO_ID_RIF").val(), status:$("#ISO_status").val(), start:$("#ISO_start").val(), end:$("#ISO_end").val()}, function(phpResponse){
			alert("Salvataggio ultimato, la pagina verra' ora ricaricata");
			location.reload(); 
			});
		}
	}



$(document).ready(function(){

        $('.datetimePicker').datetimepicker({timeInput: true});

	$( "#Popup_LegendaPittogrammi" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	width: 400, 
	buttons: {
		"Chiudi": function() {
			$( this ).dialog( "close" );
			}
		}
	});

	$('#ShowLegenda').click(function(){
		$( "#Popup_LegendaPittogrammi" ).dialog("open");
	});
        

	$( "#Popup_ApprovazioneISO" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	width: 480, 
	buttons: {
		"Salva": function() {
			UpdateApprovazioneISO($("#prefix").val());
			},
		"Chiudi": function() {
			$( this ).dialog( "close" );
			}
		}
	});

	$( "#Popup_DuplicazioneRifiuto" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	width: 480, 
	buttons: {
		"Prosegui": function() {
			// is riclassificazione
			var selectedRiclassificazione = "";
			var selectedR = $("input[type='radio'][name='riclassificazione']:checked");
			if (selectedR.length > 0) selectedRiclassificazione = selectedR.val();
			// associo vecchi fornitori
			var selectedFornitori = "";
			var selectedF = $("input[type='radio'][name='fornitori']:checked");
			if (selectedF.length > 0) selectedFornitori = selectedF.val();
			// action
			var redirection  = $("#redirection").val();
			redirection		+= '&FGE_action=cloneSchedaRifiuto';
			redirection		+= '&fornitori='+selectedFornitori;
			redirection		+= '&riclassificazione='+selectedRiclassificazione;
			document.location=redirection;
			},
		"Chiudi": function() {
			$( this ).dialog( "close" );
			}
		}
	});


	$( "#Popup_ScaricoRicaricoRifiuto" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	width: 480, 
	buttons: {
		"Prosegui": function() {
			var approved = $('#ricaricoID_RIF').find(":selected").attr("approved");
			var ClassificazioneHP = $('#ricaricoID_RIF').find(":selected").attr("ClassificazioneHP");
			var KG = parseFloat($("#KG").val().replace(',','.'));
			var KGhidden = parseFloat($("#KGhidden").val().replace(',','.'));
			if(approved==0)
				alert("Impossibile procedere con l'operazione: la scheda rifiuto selezionata non e' approvata.");
			else if(ClassificazioneHP==0)
				alert("Impossibile procedere con l'operazione: la scheda rifiuto selezionata non e' stata riclassificata secondo il Regolamento UE 1357/2014.");
			else if(KG==0)
				alert("Impossibile procedere con l'operazione: la scheda rifiuto originale non ha giacenza.");
			else if(KG>KGhidden)
				alert("Impossibile procedere con l'operazione: la quantita' imputata e' superiore a quella in giacenza per il rifiuto originale.");
			else
				ScaricoRicaricoRifiuto();
			},
		"Chiudi": function() {
			$( this ).dialog( "close" );
			}
		}
	});


	$( "#Popup_RicalcoloGiacenzeFinali" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	width: 480, 
	buttons: {
		"Prosegui": function() {
			RicalcoloGiacenzeFinali();
			},
		"Chiudi": function() {
			$( this ).dialog( "close" );
			}
		}
	});

	$("#ISO_status").change(function(){
		if($("#ISO_status").val()==0){
			$("#ISO_start").val('');
			$("#ISO_end").val('');
			$("#ISO_start").prop('disabled', true);
			$("#ISO_end").prop('disabled', true);
			}
		else{
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			if(dd<10) dd='0'+dd;
			if(mm<10) mm='0'+mm;
			today = dd+'/'+mm+'/'+yyyy;
			$("#ISO_start").val(today);
			yyyy++;
			todaY = dd+'/'+mm+'/'+yyyy;
			$("#ISO_end").val(todaY);
			$("#ISO_start").prop('disabled', false);
			$("#ISO_end").prop('disabled', false);
			}
		});
	
	$("#ISO_start").datepicker({
		dateFormat: 'dd/mm/yy'
		});

	// set end ISO_end
	$("#ISO_start").change(function(){
		start_array=$("#ISO_start").val().split("/");
		var Y = start_array[2]; Y++;
		end=start_array[0]+"/"+start_array[1]+"/"+Y;
		$("#ISO_end").val(end);
		});
	
	$("#ISO_end").datepicker({
		dateFormat: 'dd/mm/yy'
		});
                
        $(".dataRiconsegnaIVcopia").datepicker({
		dateFormat: 'dd/mm/yy'
		});

	$( "#Popup_RespingimentoRifiuto" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	width: 480, 
	buttons: {
		"Conferma": function() {
			if (!$.isNumeric($("#CR_pesoN").val()))
				alert('Attenzione!\nVerifica il formato del campo "peso netto".');
			else
				CaricoPerRifiutoRespinto();
			},
		"Chiudi": function() {
			$( this ).dialog( "close" );
			}
		}
	});

	$( "#Popup_LDP_produttore" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	width: 650, 
	buttons: {
		"Salva": function() {
			SalvaDestProd();
			},
		"Chiudi": function() {
			$( this ).dialog( "close" );
			}
		}
	});

	$( "#Popup_LDP_destinatario" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	width: 500, 
	buttons: {
		"Salva": function() {
			SalvaTrattamentoTerminato();
			},
		"Chiudi": function() {
			$( this ).dialog( "close" );
			}
		}
	});

	$('td .RifiutoRespinto').click(function(){

		if( $(this).find('a img').attr("src").indexOf('off')==-1){

			$("#CR_ID_MOV_F").val($(this).attr('CR_ID_MOV_F'));
			$("#CR_pesoN").val($(this).attr('CR_pesoN'));
			var DTFORM_ARRAY = $(this).attr('CR_DTFORM').split('-');
			var DTFORM = DTFORM_ARRAY[2]+'/'+DTFORM_ARRAY[1]+'/'+DTFORM_ARRAY[0];
			$("#CR_NOTER").val("Carico per respingimento del formulario "+$(this).attr('CR_NFORM')+" del "+DTFORM);

			// Set ID_RIF in 'select'
			$('select[name=CR_ID_RIF]').val($(this).attr('CR_ID_RIF'));

			// Show dialog box
			$( "#Popup_RespingimentoRifiuto" ).dialog("open");
			
			}
		else
			alert("Impossibile avviare la procedura per il respingimento del rifiuto: il formulario non e' ancora stato emesso.");

		});
	
	$(document).on('click', '.ShiftRow', function() { 
		
		// destination table id
		var destination_table_id = $(this).attr('table_dest');
		
		// change icon and destination table
		var src = $(this).attr('src');
		var newSrc = $(this).attr('newSrc');
		var table_dest = $(this).attr('table_dest');
		var new_table_dest = $(this).attr('new_table_dest');
		$(this).attr('src', newSrc);
		$(this).attr('newSrc', src);
		$(this).attr('table_dest', new_table_dest);
		$(this).attr('new_table_dest', table_dest);

		// original row
		var tr = $(this).closest("tr").remove().clone();

		// copy the row
		$('#'+destination_table_id+' tr:last').after(tr);
		
		});


	$('td .LDP_produttore').click(function(){

		var LINK_DestProd				= $(this).attr('LINK_DestProd');
		var LINK_TrattamentoTerminato	= $(this).attr('LINK_TrattamentoTerminato');
		$("#LINK_ID_MOV_F").val($(this).attr('LINK_ID_MOV_F'));

		// Trace dialog opener
		$('#dialogTrigger').removeAttr('id');
		$(this).attr('id', 'dialogTrigger');

		// Clean old tables
		$("#LINK_DestProd tr.removable").remove();
		$("#LINK_DestProd_possibili tr.removable").remove();

		// Lista dei FIR gi� legati
		$.post('__scripts/LDP_ListaFIRassociati.php', {LINK_DestProd:LINK_DestProd}, function(phpResponse){

			if(phpResponse.length>0){
				for(r=0;r<phpResponse.length;r++){
					if(phpResponse[r]['DTMOV']!='n.d.'){
						var DTMOV_ARRAY = phpResponse[r]['DTMOV'].split('-');
						var DTMOV = DTMOV_ARRAY[2]+'/'+DTMOV_ARRAY[1]+'/'+DTMOV_ARRAY[0];
						}
					else
						var DTMOV = 'n.d.';					
					// Add table row for each fir
					var manager = '<img style=\'cursor:pointer;\' src=\'__css/cross.png\' newSrc=\'__css/tick.png\' table_dest=\'LINK_DestProd_possibili\' new_table_dest=\'LINK_DestProd\' ID_MOV_F=\''+phpResponse[r]['ID_MOV_F']+'\' class=\'ShiftRow\' />';
					$('#LINK_DestProd tr:last').after('<tr class=\'removable\'><td>'+manager+'</td><td>'+phpResponse[r]['NMOV']+'</td><td>'+phpResponse[r]['FORNITORE']+'</td><td>'+phpResponse[r]['NFORM']+'</td><td>'+DTMOV+'</td><td style=\'text-align:left;\'>'+phpResponse[r]['rifiuto']+'</td><td style=\'text-align:right;\'>'+phpResponse[r]['quantita']+'</td></tr>');
					}
				}
			else{
				// Add table row for each fir
				$('#LINK_DestProd tr:last').after('<tr class=\'removable\'><td colspan=\"6\">Nessun formulario aasociato</td></tr>');
				}


			}, 'json');

		// Lista dei FIR associabili
		$.post('__scripts/LDP_ListaFIRassociabili.php', {LINK_DestProd:LINK_DestProd}, function(phpResponse){

			if(phpResponse.length>0){
				for(r=0;r<phpResponse.length;r++){
					if(phpResponse[r]['DTMOV']!='n.d.'){
						var DTMOV_ARRAY = phpResponse[r]['DTMOV'].split('-');
						var DTMOV = DTMOV_ARRAY[2]+'/'+DTMOV_ARRAY[1]+'/'+DTMOV_ARRAY[0];
						}
					else
						var DTMOV = 'n.d.';
					
					// Add table row for each fir
					var manager = '<img style=\'cursor:pointer;\' src=\'__css/tick.png\' newSrc=\'__css/cross.png\' table_dest=\'LINK_DestProd\' new_table_dest=\'LINK_DestProd_possibili\' ID_MOV_F=\''+phpResponse[r]['ID_MOV_F']+'\' class=\'ShiftRow\' />';
					$('#LINK_DestProd_possibili tr:last').after('<tr class=\'removable\'><td>'+manager+'</td><td>'+phpResponse[r]['NMOV']+'</td><td>'+phpResponse[r]['FORNITORE']+'</td><td>'+phpResponse[r]['NFORM']+'</td><td>'+DTMOV+'</td><td style=\'text-align:left;\'>'+phpResponse[r]['rifiuto']+'</td><td style=\'text-align:right;\'>'+phpResponse[r]['quantita']+'</td></tr>');
					}
				}
			else{
				// Add table row for each fir
				$('#LINK_DestProd_possibili tr:last').after('<tr class=\'removable\'><td colspan=\"6\">Nessun formulario aasociato</td></tr>');
				}


			}, 'json');

		
		// Show dialog box
		$( "#Popup_LDP_produttore" ).dialog("open");

		});


	$('td .LDP_destinatario').click(function(){

		var LINK_DestProd				= $(this).attr('LINK_DestProd');
		var LINK_TrattamentoTerminato	= $(this).attr('LINK_TrattamentoTerminato');
		$("#LINK_ID_MOV_F").val($(this).attr('LINK_ID_MOV_F'));

		// Trace dialog opener
		$('#dialogTrigger').removeAttr('id');
		$(this).attr('id', 'dialogTrigger');

		// Clean old table
		$("#LINK_ListaCarichi tr.removable").remove();

		// Lista dei movimenti di carico
		$.post('__scripts/LDP_ListaCarichi.php', {LINK_DestProd:LINK_DestProd}, function(phpResponse){

			if(phpResponse.length>0){
				for(r=0;r<phpResponse.length;r++){
					
					var DTMOV_ARRAY = phpResponse[r]['DTMOV'].split('-');
					var DTMOV = DTMOV_ARRAY[2]+'/'+DTMOV_ARRAY[1]+'/'+DTMOV_ARRAY[0];
					
					// Add table row for each carico
					$('#LINK_ListaCarichi tr:last').after('<tr class=\'removable\'><td>'+phpResponse[r]['NMOV']+'</td><td>'+DTMOV+'</td><td style=\'text-align:left;\'>'+phpResponse[r]['rifiuto']+'</td><td style=\'text-align:right;\'>'+phpResponse[r]['quantita']+'</td></tr>');
					}
				}
			else{
				
				// Add table row for each carico
				$('#LINK_ListaCarichi tr:last').after('<tr class=\'removable\'><td colspan=\"5\">Nessun movimento di carico aasociato</td></tr>');
				}


			}, 'json');

		// Radio TrattamentoTerminato
		var $radios = $('input:radio[name=LINK_TrattamentoTerminato]');
		$radios.filter('[value=' + LINK_TrattamentoTerminato + ']').prop('checked', true);
		
		// Show dialog box
		$( "#Popup_LDP_destinatario" ).dialog("open");

		});
	
	function ScaricoRicaricoRifiuto(){
		//set useful variables
		var originalID_RIF	= $('#originalID_RIF').val();
		var ricaricoID_RIF	= $('#ricaricoID_RIF').val();
		var ID_AZP			= $('#ricaricoID_UIMP').find(":selected").attr("ID_AZP");
		var codfisc			= $('#ricaricoID_UIMP').find(":selected").attr("codfisc");
		var ID_UIMP			= $('#ricaricoID_UIMP').val();
		var pesoN			= parseFloat($('#KG').val().replace(',','.'));
		var NOTER			= $('#PerRiclassificazione_nota').val();

		//disable form fields and buttons
		$("#ricaricoID_RIF").prop('disabled', true);
		$("#PerRiclassificazione_nota").prop('disabled', true);
		var myButtons = {
			"Chiudi": function() {
				$( this ).dialog( "close" );
				}
			};
		$('#Popup_ScaricoRicaricoRifiuto').dialog('option', 'buttons', myButtons);
		$(".ui-dialog-buttonpane button:contains('Chiudi')").button("disable");

		// show progress bar
		$("#ScaricoRicaricoRifiuto_response").html('<img src="__css/progress_bar.gif" alt="Operazione ni corso, attendere prego" title="Operazione ni corso, attendere prego" />');
		$("#ScaricoRicaricoRifiuto_progress").show();

		//ajax call to create SCARICO and CARICO							
		$.post('__scripts/MovimentiPerRiclassificazione.php', {originalID_RIF:originalID_RIF, ricaricoID_RIF:ricaricoID_RIF, ID_AZP:ID_AZP, codfisc:codfisc, ID_UIMP:ID_UIMP, pesoN:pesoN, NOTER:NOTER}, function(phpResponse){

			// show response
			$("#ScaricoRicaricoRifiuto_response").html('operazione ultimata!');

			// enable CHIUDI button
			$(".ui-dialog-buttonpane button:contains('Chiudi')").button("enable");
			});
		}

	function RicalcoloGiacenzeFinali(){

		var myButtons = {
			"Chiudi": function() {
				$( this ).dialog( "close" );
                                location.reload();
				}
			};
		$('#Popup_RicalcoloGiacenzeFinali').dialog('option', 'buttons', myButtons);
		$(".ui-dialog-buttonpane button:contains('Chiudi')").button("disable");

		// show progress bar
		$("#RicalcoloGiacenzeFinali_response").html('<img src="__css/progress_bar.gif" alt="Operazione ni corso, attendere prego" title="Operazione ni corso, attendere prego" />');
		$("#RicalcoloGiacenzeFinali_progress").show();

		//ajax call to ricalcolo giacenze finali							
		$.post('__scripts/RicalcoloGiacenzeFinali.php', {}, function(phpResponse){

			// show response
			$("#RicalcoloGiacenzeFinali_response").html('operazione ultimata!');

			// enable CHIUDI button
			$(".ui-dialog-buttonpane button:contains('Chiudi')").button("enable");
			});
		}
                
	function CaricoPerRifiutoRespinto(){

		//set useful variables
		var ID_MOV_F = $("#CR_ID_MOV_F").val();
		var pesoN	 = $("#CR_pesoN").val();
		var ID_RIF	 = $("#CR_ID_RIF").val();
		var NOTER	 = $("#CR_NOTER").val();

		//disable form fields and buttons
		$("#CR_pesoN").prop('disabled', true);
		$("#CR_ID_RIF").prop('disabled', true);
		$("#CR_NOTER").prop('disabled', true);
		var myButtons = {
			"Chiudi": function() {
				$( this ).dialog( "close" );
				}
			};
		$('#Popup_RespingimentoRifiuto').dialog('option', 'buttons', myButtons);
		$(".ui-dialog-buttonpane button:contains('Chiudi')").button("disable");
		
		//add table row to showing statua
		$('#CR_table tr:last').after('<tr><td colspan="2">E\' in corso il salvataggio del movimento di carico, attendere...</td><td><img src="__css/progress_bar.gif" /></td></tr>');

		//ajax call to create CARICO							
		$.post('__scripts/MovimentiCaricoAutomaticoPerRifiutoRespinto.php', {ID_MOV_F:ID_MOV_F, pesoN:pesoN, ID_RIF:ID_RIF, NOTER:NOTER}, function(phpResponse){
			// remove last table row
			$('#CR_table tr:last').remove();
			
			//add table row to showing statua
			$('#CR_table tr:last').after('<tr><td colspan="3" style="text-align:center">Operazione ultimata!</td></tr>');

			// enable CHIUDI button
			$(".ui-dialog-buttonpane button:contains('Chiudi')").button("enable");
			});

		}

	function SalvaDestProd(){

		var ID_MOV_F = $("#LINK_ID_MOV_F").val();
		var LINK_DestProd = '';
		$('#LINK_DestProd .ShiftRow').each(function(){ LINK_DestProd += $(this).attr('ID_MOV_F')+',';	});
		if(LINK_DestProd.length>0) LINK_DestProd = LINK_DestProd.substring(0, LINK_DestProd.length - 1);
		
		// Update LINK_DestProd
		$.post('__scripts/LDP_SetDestProd.php', {ID_MOV_F:ID_MOV_F, LINK_DestProd:LINK_DestProd}, function(phpResponse){
			$('#dialogTrigger').attr('LINK_DestProd', LINK_DestProd);
			alert("Salvataggio completato!");
			});
		}

	function SalvaTrattamentoTerminato(){

		var ID_MOV_F = $("#LINK_ID_MOV_F").val();
		var LINK_TrattamentoTerminato = $('input[name=LINK_TrattamentoTerminato]:checked').val();
		
		// Update LINK_TrattamentoTerminato
		$.post('__scripts/LDP_SetTrattamentoTerminato.php', {ID_MOV_F:ID_MOV_F, LINK_TrattamentoTerminato:LINK_TrattamentoTerminato}, function(phpResponse){
			$('#dialogTrigger').attr('LINK_TrattamentoTerminato', LINK_TrattamentoTerminato);
			alert("Salvataggio completato!");
			});
		}



});

function isset () {

    var a = arguments,
        l = a.length,        i = 0,
        undef;
 
    if (l === 0) {
        throw new Error('Empty isset');    }
 
    while (i !== l) {
        if (a[i] === undef || a[i] === null) {
            return false;        }
        i++;
    }
    return true;
}

function in_array(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

/*
PERMESSI UTENTI
*/

function CUSTOM_CHECK_PWD(NotUsed,Fname,FormRef) {
	//window.alert(document.FGEForm_1["core_users:nuovapwd"].value);
	if(document.FGEForm_1["core_users:nuovapwd"].value!=''){
		if(window.confirm("E' stata immessa una nuova password, si e' certi di volerla modificare?")){
			if(document.FGEForm_1["core_users:nuovapwd"].value!=document.FGEForm_1["core_users:nuovapwd_confirm"].value){
				FGE_LockForm = true;
				window.alert("Attenzione! Le due password immesse non corrispondono!");
				document.FGEForm_1["core_users:nuovapwd"].value='';
				document.FGEForm_1["core_users:nuovapwd_confirm"].value='';
				}
			else{
				if(document.FGEForm_1["core_users:nuovapwd"].value.length<8){
					FGE_LockForm = true;
					window.alert("Attenzione! La nuova password deve essere di almeno 8 caratteri!\nImmettere una nuova password");
					document.FGEForm_1["core_users:nuovapwd"].value='';
					document.FGEForm_1["core_users:nuovapwd_confirm"].value='';
					}
				else{
					document.FGEForm_1["core_users:pwd"].value=document.FGEForm_1["core_users:nuovapwd"].value;
					document.FGEForm_1["core_users:nuovapwd"].value='';
					document.FGEForm_1["core_users:nuovapwd_confirm"].value='';
					}
				}
			}
		else{
			FGE_LockForm = true;
			document.FGEForm_1["core_users:nuovapwd"].value='';
			document.FGEForm_1["core_users:nuovapwd_confirm"].value='';
			}
		}
	}

function CHECK_WORKMODE(){
	if(document.FGEForm_1["core_users:produttore"][1].checked && document.FGEForm_1["core_users:trasportatore"][1].checked && document.FGEForm_1["core_users:destinatario"][1].checked && document.FGEForm_1["core_users:intermediario"][1].checked){
		window.alert("E' necessario abilitare almeno un profilo d'accesso");
		FGE_LockForm = true;
			return;
		}
	}

function CUSTOM_USERWORKMODE(NotUsed,FName,FormRef) {
	var WMCheck = new ajaxObject('__scripts/WMCheck.php',FGE_DummyAjaxCallback);
	WMCheck.callback = function (responseTxt, responseStat) {
		ResArray = responseTxt.split("|");	
		
		if(ResArray[0]=='1'){
			document.getElementById("SP_core_users:produttore").style.background = "yellow";
			document.FGEForm_1["core_users:produttore"][0].checked=true;
			document.FGEForm_1["core_users:produttore"][1].disabled=false;
			document.FGEForm_1["core_users:produttore"][0].disabled=false;
			}
		else{
			document.getElementById("SP_core_users:produttore").style.background = "white";
			document.FGEForm_1["core_users:produttore"][1].checked=true;
			document.FGEForm_1["core_users:produttore"][1].disabled=true;
			document.FGEForm_1["core_users:produttore"][0].disabled=true;
			}

		if(ResArray[1]=='1'){
			document.getElementById("SP_core_users:trasportatore").style.background = "yellow";
			document.FGEForm_1["core_users:trasportatore"][0].checked=true;
			document.FGEForm_1["core_users:trasportatore"][1].disabled=false;
			document.FGEForm_1["core_users:trasportatore"][0].disabled=false;
			}
		else{
			document.getElementById("SP_core_users:trasportatore").style.background = "white";
			document.FGEForm_1["core_users:trasportatore"][1].checked=true;
			document.FGEForm_1["core_users:trasportatore"][1].disabled=true;
			document.FGEForm_1["core_users:trasportatore"][0].disabled=true;
			}

		if(ResArray[2]=='1'){
			document.getElementById("SP_core_users:destinatario").style.background = "yellow";
			document.FGEForm_1["core_users:destinatario"][0].checked=true;
			document.FGEForm_1["core_users:destinatario"][1].disabled=false;
			document.FGEForm_1["core_users:destinatario"][0].disabled=false;
			}
		else{
			document.getElementById("SP_core_users:destinatario").style.background = "white";
			document.FGEForm_1["core_users:destinatario"][1].checked=true;
			document.FGEForm_1["core_users:destinatario"][1].disabled=true;
			document.FGEForm_1["core_users:destinatario"][0].disabled=true;
			}
		if(ResArray[3]=='1'){
			document.getElementById("SP_core_users:intermediario").style.background = "yellow";
			document.FGEForm_1["core_users:intermediario"][0].checked=true;
			document.FGEForm_1["core_users:intermediario"][1].disabled=false;
			document.FGEForm_1["core_users:intermediario"][0].disabled=false;
			}
		else{
			document.getElementById("SP_core_users:intermediario").style.background = "white";
			document.FGEForm_1["core_users:intermediario"][1].checked=true;
			document.FGEForm_1["core_users:intermediario"][1].disabled=true;
			document.FGEForm_1["core_users:intermediario"][0].disabled=true;
			}
		}
	Pars = "ID_IMP=" + FName.value;
	WMCheck.update(Pars);
	}

function INHERITS_CONTROL(){

	if(document.getElementById("core_users:GGedit_produttore")!=null){
		var profilo = 'produttore';
		var limit	= 14;
		}
	if(document.getElementById("core_users:GGedit_trasportatore")!=null){
		var profilo = 'trasportatore';
		var limit	= 2;
		}
	if(document.getElementById("core_users:GGedit_destinatario")!=null){
		var profilo = 'destinatario';
		var limit	= 2;
		}
	if(document.getElementById("core_users:GGedit_intermediario")!=null){
		var profilo = 'intermediario';
		var limit	= 2;
		}
	
	var confirm_msg = "Attenzione!";

	if(document.getElementById("core_users:GGedit_"+profilo).value>limit){
		confirm_msg +="\n\nI giorni di modificabilita' della movimentazione indicati superano le disposizioni dell\'articolo 190 del D.Lgs. 152/2006.";
		}

	confirm_msg +="\n\nLe impostazioni saranno propagate a tutte le utenze dell' impianto: desideri proseguire?";

	if(!window.confirm(confirm_msg))
		FGE_LockForm = true;

	}


function CHECK_GIORNI_INVIO_SISTRI(){
	if(document.getElementById("core_users:ck_MOV_SISTRIgg").value>10){
		alert("Attenzione, non e' possibile settare un numero di giorni superiore a 10 per la verifica della trasmissione dei dati a SISTRI.");
		FGE_LockForm = true;
		}
	}



/*
SCHEDE RIFIUTO
*/

function CUSTOM_PERCENTO(NotUsed,Fname,FormRef) {
	if(FormRef["user_schede_rifiuti:FANGHI_ss"].value>100){
		FGE_LockForm = true;
		window.alert("Attenzione! Verificare il valore immesso per % s.s.");
		}
	}

function SCH_RIF_CS_AVV(Avviso,Fname,FormRef) {
	// alert('SCH_RIF_CS_AVV');
	// controllo coerenza dati avvisi carico /scarico (scheda rifiuto)
//{{{ 
	if(Avviso=="2" | Avviso=="3") {
		if(FormRef["user_schede_rifiuti:avv_giorni"].value=="" | FormRef["user_schede_rifiuti:avv_giorni"].value=="0") {
			FGE_Error("Se viene attivato un avviso di carico/scarico, occorre specificare un numero di giorni.\n","user_schede_rifiuti:avv_giorni");
			return;
		}
		if(FormRef["user_schede_rifiuti:avv_quantita"].value=="" | FormRef["user_schede_rifiuti:avv_quantita"].value=="0") {
			FGE_Error("Se viene attivato un avviso di carico/scarico, occorre specificare una quantita'.\n","user_schede_rifiuti:avv_quantita");
			return;
		}
	} else if (Avviso=="1") {
		FormRef["user_schede_rifiuti:avv_giorni"].value = "0";
		FormRef["user_schede_rifiuti:avv_quantita"].value = "0";
	} //}}}
}
function CUSTOM_CLHCHECK(NotUsed,Fname,FormRef) {
/*
	FGE_LockForm = true;
	var error=false;

	//alert('CUSTOM_CLHCHECK');
	// controllo coerenza pericolosit� (scheda rifiuto) 

	TableName = Fname.substring(0, Fname.indexOf(':'));
	ElCount = FormRef.elements.length;

	// CONGRUENZA CLASSI H / PERICOLOSITA'
	if(FormRef[TableName+':pericoloso'][1].checked) {
		for(c=1;c<ElCount;c++) {
			clHpattern = /^user_schede_rifiuti:H[0-9A-Z]+$/
			FName = FormRef.elements[c].name
			if(FName.match(clHpattern)) {
				if(FormRef[FName][0].checked) {
					alert("Ad un rifiuto Non Pericoloso non deve essere associata alcuna Classe H\n",'user_schede_rifiuti:pericoloso');
					var error=true;
					return;
					}
				}
			}
		} 
	else {
 		foundH = false;
		for(c=1;c<ElCount;c++) {
			clHpattern = /^user_schede_rifiuti:H[0-9A-Z]+$/
			FName = FormRef.elements[c].name
			if(FName.match(clHpattern)) {
				if(FormRef[FName][0].checked) {
					foundH = true;
				}
			}
		}
		if(foundH==false) {
			alert("Ad un rifiuto Pericoloso deve essere associata almeno una Classe H\n",'user_schede_rifiuti:pericoloso');
			var error=true;
			return;
			}
		}

	if(!error){
		FGE_LockForm = false;
		FGE_SendForm();
		}
*/
	}


/*
function CUSTOM_30GGDEP(Label,FieldRef,FormRef) {
	FGE_LockForm = true;
	var H9	= FormRef["user_schede_rifiuti:H9"][0].checked;
	var CER	= document.getElementById("user_schede_rifiuti:ID_CER")[document.getElementById("user_schede_rifiuti:ID_CER").selectedIndex].innerHTML;
	var CAP	= CER.slice(0, 2);
	var GG	= parseInt(document.getElementById("user_schede_rifiuti:t_limite").value);
	if(H9 && CAP=='18' && GG>30) {
		FGE_Error("Il rifiuto classificato H9 e con codice CER del capitolo 18 non puo' avere deposito temporaneo superiore a 30 giorni.","user_schede_rifiuti:t_limite");
		return;			
		}
	else{
		FGE_LockForm = false;
		FGE_SendForm();
		}
	}
*/

function Reset_HP(Label,FieldRef,FormRef) {
	if(!FormRef["user_schede_rifiuti:ClassificazioneHP"][0].checked){
		document.getElementById("SP_user_schede_rifiuti:ClassificazioneHP").style.background = "white";
		FormRef["user_schede_rifiuti:HP1"][1].checked=true;
		document.getElementById("SP_user_schede_rifiuti:HP1").style.background = "white";
		FormRef["user_schede_rifiuti:HP2"][1].checked=true;
		document.getElementById("SP_user_schede_rifiuti:HP2").style.background = "white";
		FormRef["user_schede_rifiuti:HP3"][1].checked=true;
		document.getElementById("SP_user_schede_rifiuti:HP3").style.background = "white";
		FormRef["user_schede_rifiuti:HP4"][1].checked=true;
		document.getElementById("SP_user_schede_rifiuti:HP4").style.background = "white";
		FormRef["user_schede_rifiuti:HP5"][1].checked=true;
		document.getElementById("SP_user_schede_rifiuti:HP5").style.background = "white";
		FormRef["user_schede_rifiuti:HP6"][1].checked=true;
		document.getElementById("SP_user_schede_rifiuti:HP6").style.background = "white";
		FormRef["user_schede_rifiuti:HP7"][1].checked=true;
		document.getElementById("SP_user_schede_rifiuti:HP7").style.background = "white";
		FormRef["user_schede_rifiuti:HP8"][1].checked=true;
		document.getElementById("SP_user_schede_rifiuti:HP8").style.background = "white";
		FormRef["user_schede_rifiuti:HP9"][1].checked=true;
		document.getElementById("SP_user_schede_rifiuti:HP9").style.background = "white";
		FormRef["user_schede_rifiuti:HP10"][1].checked=true;
		document.getElementById("SP_user_schede_rifiuti:HP10").style.background = "white";
		FormRef["user_schede_rifiuti:HP11"][1].checked=true;
		document.getElementById("SP_user_schede_rifiuti:HP11").style.background = "white";
		FormRef["user_schede_rifiuti:HP12"][1].checked=true;
		document.getElementById("SP_user_schede_rifiuti:HP12").style.background = "white";
		FormRef["user_schede_rifiuti:HP13"][1].checked=true;
		document.getElementById("SP_user_schede_rifiuti:HP13").style.background = "white";
		FormRef["user_schede_rifiuti:HP14"][1].checked=true;
		document.getElementById("SP_user_schede_rifiuti:HP14").style.background = "white";
		FormRef["user_schede_rifiuti:HP15"][1].checked=true;
		document.getElementById("SP_user_schede_rifiuti:HP15").style.background = "white";
		}
	}

function Class_HP(Label,FieldRef,FormRef){
	var foundHP = false;
	var ElCount = FormRef.elements.length;
	for(c=1;c<ElCount;c++) {
		clHpattern = /^user_schede_rifiuti:H[0-9A-Z]+$/
		FName = FormRef.elements[c].name
		if(FName.match(clHpattern)) {
			if(FormRef[FName][0].checked && FName.substring(20, 22)=='HP'){
				foundHP = true;
				}
			}
		}
	if(foundHP){
		document.getElementById("SP_user_schede_rifiuti:ClassificazioneHP").style.background = "yellow";
		FormRef["user_schede_rifiuti:ClassificazioneHP"][0].checked=true;
		}
	}

function CUSTOM_ONU(Label,FieldRef,FormRef) {

	FGE_LockForm = true;
	var error=false;

	var LKUP = new ajaxObject('__scripts/ONUgetDetails.php',FGE_DummyAjaxCallback);	
	LKUP.callback = function (responseTxt, responseStat) {

		responseTxt = responseTxt.replace(/^\s+|\s+$/g, '');
		var data = responseTxt.split("|");
		var description = data[0];
		var classe = data[1];
		var nas = data[2];
		var disposizioni_speciali = data[3];
		var etichette = data[4];

		// controllo campi obbligatori: ID_CER, descrizione, ID_UMIS, ID_SF, ID_SPEC, C_S_ID
        if(FormRef["user_schede_rifiuti:ID_CER"].selectedIndex==0 || FormRef["user_schede_rifiuti:ID_UMIS"].selectedIndex==0 || FormRef["user_schede_rifiuti:ID_SF"].selectedIndex==0 || FormRef["user_schede_rifiuti:ID_SPEC"].selectedIndex==0 || FormRef["user_schede_rifiuti:C_S_ID"].selectedIndex==0 || trim(FormRef["user_schede_rifiuti:descrizione"].value)==''){
			alert("Verifica di aver compilato tutti i campi obbligatori:\n- Codice CER\n- Descrizione del rifiuto\n- Unita' di misura\n- Stato fisico\n- Specificita' del rifiuto\n- Avvisi di carico",FieldRef);
			var error=true;
			return;		
			}
                        
		// SE RIFIUTO E' CER 18.xx.xx ED E' H9, IL DEP TEMP MAX NON DEVE ESSERE >30
		if(typeof(FormRef["user_schede_rifiuti:t_limite"]) != 'undefined'){
			//var H9	= FormRef["user_schede_rifiuti:H9"][0].checked;
			var HP9	= FormRef["user_schede_rifiuti:HP9"][0].checked;
			var CER	= document.getElementById("user_schede_rifiuti:ID_CER")[document.getElementById("user_schede_rifiuti:ID_CER").selectedIndex].innerHTML;
			var CAP	= CER.slice(0, 2);
			var GG	= parseInt(document.getElementById("user_schede_rifiuti:t_limite").value);
			if(HP9 && CAP=='18' && GG>30) {
				alert("Il rifiuto classificato infettivo e con codice CER del capitolo 18 non puo' avere deposito temporaneo superiore a 30 giorni.","user_schede_rifiuti:t_limite");
				var error=true;
				return;
				}
			}

		// SE GESTISCO DEPOSITO CON CRITERIO TEMPORALE, DEVE ESSERE >0
		if(typeof(FormRef["user_schede_rifiuti:t_limite"]) != 'undefined'){
			var GG	= parseInt(document.getElementById("user_schede_rifiuti:t_limite").value);
			if(GG<1) {
				alert("Attenzione, indicare un deposito temporaneo superiore a 0 giorni.","user_schede_rifiuti:t_limite");
				var error=true;
				return;		
				}
			}
		
		// controllo se il rifiuto apprtiene alla classe 6.2
        //if(trim(classe)=='6.2' && trim(document.FGEForm_1["user_schede_rifiuti:ONU_62"].value)==''){
		//	alert("Il rifiuto risulta sottoposto all\'ADR in classe 6.2: e' necessario indicare gli estremi di una persona di riferimento.\n",FieldRef);
		//	var error=true;
		//	return;		
		//	}

		// adr NO ma ONU selezionato
		if(FormRef["user_schede_rifiuti:adr"][1].checked && FormRef[FieldRef].selectedIndex!=0) {
			alert("Il rifiuto non risulta sottoposto all\'ADR, impossibile assegnare un numero ONU.\n",FieldRef);
			var error=true;
			return;		
			}
		
		// adr S� ma ONU non selezionato
		if(FormRef["user_schede_rifiuti:adr"][0].checked && FormRef[FieldRef].selectedIndex==0) {
			alert("Il rifiuto risulta sottoposto all\'ADR, ma non e' stato assegnato un numero ONU.\n",FieldRef);
			var error=true;
			return;		
			}

		// SE n.a.s. con disposizione 274 o 318, oppure 3509, allora "ADR: componenti pericolosi" � obbligatorio
		// aggiungiamo: sempre che non sia 2.1.3.5.5
		if( ( (nas=='1' && (disposizioni_speciali.indexOf('274')>-1 || disposizioni_speciali.indexOf('318')>-1)) || description=='3509') && FormRef["user_schede_rifiuti:ADR_2_1_3_5_5"][1].checked && trim(FormRef["user_schede_rifiuti:ONU_per"].value)==''){
			alert("Il numero ONU scelto prevede l'indicazione obbigatoria dei componenti pericolosi.\n",FieldRef);
			var error=true;
			return;		
			}

		if( (etichette.indexOf("5.2.2.1.12") !== -1) && document.getElementById('user_schede_rifiuti:ID_ETICHETTA_ADR').selectedIndex == 0){
			alert("Il numero ONU scelto prevede l'indicazione obbigatoria di un'etichetta come disposto nel capitolo 5.2.2.1.12 dell'ADR.\n",FieldRef);
			var error=true;
			return;	
		}

		// CONGRUENZA CLASSI H / PERICOLOSITA'
		var ElCount = FormRef.elements.length;
		if(FormRef['user_schede_rifiuti:pericoloso'][1].checked) {
			for(c=1;c<ElCount;c++) {
				clHPpattern = /^user_schede_rifiuti:HP[0-9A-Z]+$/
				FName = FormRef.elements[c].name
				if(FName.match(clHPpattern)) {
					if(FormRef[FName][0].checked) {
						alert("Ad un rifiuto non pericoloso non deve essere associata alcuna classe di pericolo\n",'user_schede_rifiuti:pericoloso');
						var error=true;
						return;
						}
					}
				}
			} 
		else {
			foundHP = false;
			for(c=1;c<ElCount;c++) {
				clHPpattern = /^user_schede_rifiuti:HP[0-9A-Z]+$/
				FName = FormRef.elements[c].name
				if(FName.match(clHPpattern)) {
					if(FormRef[FName][0].checked) {
						foundHP = true;
						}
					}
				}

			if(foundHP==false) {
				alert("Ad un rifiuto Pericoloso deve essere associata almeno una Classe HP\n",'user_schede_rifiuti:pericoloso');
				var error=true;
				return;
				}			
			}

		if(!error){
			FGE_LockForm = false;
			FGE_SendForm();
			}
		}

	Parameters = 'ID_ONU=' + document.FGEForm_1["user_schede_rifiuti:ID_ONU"].value;
	LKUP.update(Parameters);
	}



// popolo campo di testo editabile con descrizione onu
function CUSTOM_DESONU(NotUsed,FieldRef,FormRef) {

	var LKUP = new ajaxObject('__scripts/ONUdescription.php',FGE_DummyAjaxCallback);	
	LKUP.callback = function (responseTxt, responseStat) {

		ONUdata		= responseTxt.split("|");
		ONUdes		= ONUdata[0];
		Etichette	= ONUdata[1];
		
		// Precompilo descrizione UN e componenti pericolosi
		document.FGEForm_1["user_schede_rifiuti:ONU_DES"].value=ONUdes;
		if(document.FGEForm_1["user_schede_rifiuti:ID_ONU"].value==3002)
			document.FGEForm_1["user_schede_rifiuti:ONU_per"].value="Contiene residui di ";

		// Gestione etichette per nuove rubriche ADR con nota 5.2.2.1.12
		if(Etichette.indexOf("5.2.2.1.12") !== -1){
			document.getElementById('user_schede_rifiuti:ID_ETICHETTA_ADR').disabled = false;
		}
		else{
			document.getElementById('user_schede_rifiuti:ID_ETICHETTA_ADR').selectedIndex = 0;
			document.getElementById('user_schede_rifiuti:ID_ETICHETTA_ADR').disabled = true;
		}

		var un = ONUdata[2];
		if(un == '3316'){
			document.getElementById('user_schede_rifiuti:ID_IMBALLAGGIO_ADR').disabled = false;
		}
		else{
			document.getElementById('user_schede_rifiuti:ID_IMBALLAGGIO_ADR').selectedIndex = 0;
			document.getElementById('user_schede_rifiuti:ID_IMBALLAGGIO_ADR').disabled = true;
		}

	}	
	Parameters = 'ID_ONU=' + document.FGEForm_1["user_schede_rifiuti:ID_ONU"].value;
	LKUP.update(Parameters);	 //}}}
}


function CONTROL_FAMIGLIE_DI_SOSTANZE(Label,FieldRef,FormRef) {
	
	// se non si usano pericolosi
	if(FormRef[FieldRef.name][1].checked)
		document.FGEForm_1["user_schede_rifiuti:et_comp_per"].value='nessuna sostanza pericolosa';
	else{
		// si usano sostanze pericolose
		if(document.FGEForm_1["user_schede_rifiuti:et_comp_per"].value==trim('nessuna sostanza pericolosa'))
			document.FGEForm_1["user_schede_rifiuti:et_comp_per"].value='';
		}
	}

function CUSTOM_DEP_TMP(Label,FieldRef,FormRef) {
//{{{ 
	if(FieldRef.name=="user_schede_rifiuti:q_limite") {
		if(FieldRef.value!="0" && !isNaN(FieldRef.value) && FieldRef.value!="") {
			document.getElementById("user_schede_rifiuti:t_limite").value="0";
		}
	} else {
		if(FieldRef.value!="0" && !isNaN(FieldRef.value) && FieldRef.value!="") {
			document.getElementById("user_schede_rifiuti:q_limite").value="0";
		}		
	}
	FieldRef.style.background = "white"; //}}}
}
function COD_CER_EDIT_CK(NotUsed,FName,FormRef) {
//{{{ 

	var CerCheck = new ajaxObject('__scripts/LoadCER.php',FGE_DummyAjaxCallback);
	CerCheck.callback = function (responseTxt, responseStat) {

		// CER 18.01.03
		if(typeof(FormRef["user_schede_rifiuti:t_limite"]) != 'undefined'){
			if(FName.value=='0001063'){
				FormRef["user_schede_rifiuti:t_limite"].value=30;
				}
			else{
				FormRef["user_schede_rifiuti:t_limite"].value=90;
				}
			}

//		FormRef["user_schede_rifiuti:H1"][1].checked=true;
//		FormRef["user_schede_rifiuti:H2"][1].checked=true;
//		FormRef["user_schede_rifiuti:H3A"][1].checked=true;
//		FormRef["user_schede_rifiuti:H3B"][1].checked=true;
//		FormRef["user_schede_rifiuti:H4"][1].checked=true;
//		FormRef["user_schede_rifiuti:H5"][1].checked=true;
//		FormRef["user_schede_rifiuti:H6"][1].checked=true;
//		FormRef["user_schede_rifiuti:H7"][1].checked=true;
//		FormRef["user_schede_rifiuti:H8"][1].checked=true;
//		FormRef["user_schede_rifiuti:H9"][1].checked=true;
//		FormRef["user_schede_rifiuti:H10"][1].checked=true;
//		FormRef["user_schede_rifiuti:H11"][1].checked=true;
//		FormRef["user_schede_rifiuti:H12"][1].checked=true;
//		FormRef["user_schede_rifiuti:H13"][1].checked=true;
//		FormRef["user_schede_rifiuti:H14"][1].checked=true;
//		FormRef["user_schede_rifiuti:H15"][1].checked=true;
		FormRef["user_schede_rifiuti:HP1"][1].checked=true;
		FormRef["user_schede_rifiuti:HP2"][1].checked=true;
		FormRef["user_schede_rifiuti:HP3"][1].checked=true;
		FormRef["user_schede_rifiuti:HP3"][1].checked=true;
		FormRef["user_schede_rifiuti:HP4"][1].checked=true;
		FormRef["user_schede_rifiuti:HP5"][1].checked=true;
		FormRef["user_schede_rifiuti:HP6"][1].checked=true;
		FormRef["user_schede_rifiuti:HP7"][1].checked=true;
		FormRef["user_schede_rifiuti:HP8"][1].checked=true;
		FormRef["user_schede_rifiuti:HP9"][1].checked=true;
		FormRef["user_schede_rifiuti:HP10"][1].checked=true;
		FormRef["user_schede_rifiuti:HP11"][1].checked=true;
		FormRef["user_schede_rifiuti:HP12"][1].checked=true;
		FormRef["user_schede_rifiuti:HP13"][1].checked=true;
		FormRef["user_schede_rifiuti:HP14"][1].checked=true;
		FormRef["user_schede_rifiuti:HP15"][1].checked=true;
		//FormRef["user_schede_rifiuti:pericoloso"][0].disabled=false;
		//FormRef["user_schede_rifiuti:pericoloso"][1].disabled=false;		
		if(responseTxt=="1") {
			FormRef["user_schede_rifiuti:pericoloso"][0].checked=true;
			FormRef["user_schede_rifiuti:pericoloso"][1].checked=false;
			// abilito edit classi H
//			FormRef["user_schede_rifiuti:H1"][0].disabled=false;
//			FormRef["user_schede_rifiuti:H1"][1].disabled=false;
//			FormRef["user_schede_rifiuti:H2"][0].disabled=false;
//			FormRef["user_schede_rifiuti:H2"][1].disabled=false;
//			FormRef["user_schede_rifiuti:H3A"][0].disabled=false;
//			FormRef["user_schede_rifiuti:H3A"][1].disabled=false;
//			FormRef["user_schede_rifiuti:H3B"][0].disabled=false;
//			FormRef["user_schede_rifiuti:H3B"][1].disabled=false;
//			FormRef["user_schede_rifiuti:H4"][0].disabled=false;
//			FormRef["user_schede_rifiuti:H4"][1].disabled=false;
//			FormRef["user_schede_rifiuti:H5"][0].disabled=false;
//			FormRef["user_schede_rifiuti:H5"][1].disabled=false;
//			FormRef["user_schede_rifiuti:H6"][0].disabled=false;
//			FormRef["user_schede_rifiuti:H6"][1].disabled=false;
//			FormRef["user_schede_rifiuti:H7"][0].disabled=false;
//			FormRef["user_schede_rifiuti:H7"][1].disabled=false;
//			FormRef["user_schede_rifiuti:H8"][0].disabled=false;
//			FormRef["user_schede_rifiuti:H8"][1].disabled=false;
//			FormRef["user_schede_rifiuti:H9"][0].disabled=false;
//			FormRef["user_schede_rifiuti:H9"][1].disabled=false;
//			FormRef["user_schede_rifiuti:H10"][0].disabled=false;
//			FormRef["user_schede_rifiuti:H10"][1].disabled=false;
//			FormRef["user_schede_rifiuti:H11"][0].disabled=false;
//			FormRef["user_schede_rifiuti:H11"][1].disabled=false;
//			FormRef["user_schede_rifiuti:H12"][0].disabled=false;
//			FormRef["user_schede_rifiuti:H12"][1].disabled=false;
//			FormRef["user_schede_rifiuti:H13"][0].disabled=false;
//			FormRef["user_schede_rifiuti:H13"][1].disabled=false;
//			FormRef["user_schede_rifiuti:H14"][0].disabled=false;
//			FormRef["user_schede_rifiuti:H14"][1].disabled=false;
//			FormRef["user_schede_rifiuti:H15"][0].disabled=false;
//			FormRef["user_schede_rifiuti:H15"][1].disabled=false;

			// abilito edit classi HP
			FormRef["user_schede_rifiuti:HP1"][0].disabled=false;
			FormRef["user_schede_rifiuti:HP1"][1].disabled=false;
			FormRef["user_schede_rifiuti:HP2"][0].disabled=false;
			FormRef["user_schede_rifiuti:HP2"][1].disabled=false;
			FormRef["user_schede_rifiuti:HP3"][0].disabled=false;
			FormRef["user_schede_rifiuti:HP3"][1].disabled=false;
			FormRef["user_schede_rifiuti:HP4"][0].disabled=false;
			FormRef["user_schede_rifiuti:HP4"][1].disabled=false;
			FormRef["user_schede_rifiuti:HP5"][0].disabled=false;
			FormRef["user_schede_rifiuti:HP5"][1].disabled=false;
			FormRef["user_schede_rifiuti:HP6"][0].disabled=false;
			FormRef["user_schede_rifiuti:HP6"][1].disabled=false;
			FormRef["user_schede_rifiuti:HP7"][0].disabled=false;
			FormRef["user_schede_rifiuti:HP7"][1].disabled=false;
			FormRef["user_schede_rifiuti:HP8"][0].disabled=false;
			FormRef["user_schede_rifiuti:HP8"][1].disabled=false;
			FormRef["user_schede_rifiuti:HP9"][0].disabled=false;
			FormRef["user_schede_rifiuti:HP9"][1].disabled=false;
			FormRef["user_schede_rifiuti:HP10"][0].disabled=false;
			FormRef["user_schede_rifiuti:HP10"][1].disabled=false;
			FormRef["user_schede_rifiuti:HP11"][0].disabled=false;
			FormRef["user_schede_rifiuti:HP11"][1].disabled=false;
			FormRef["user_schede_rifiuti:HP12"][0].disabled=false;
			FormRef["user_schede_rifiuti:HP12"][1].disabled=false;
			FormRef["user_schede_rifiuti:HP13"][0].disabled=false;
			FormRef["user_schede_rifiuti:HP13"][1].disabled=false;
			FormRef["user_schede_rifiuti:HP14"][0].disabled=false;
			FormRef["user_schede_rifiuti:HP14"][1].disabled=false;
			FormRef["user_schede_rifiuti:HP15"][0].disabled=false;
			FormRef["user_schede_rifiuti:HP15"][1].disabled=false;

			// � pericoloso, mostro bottone per creare scheda sistri
			document.getElementById('CreaSchedaFromRif').style.display = 'block';
			} 
		else {
			FormRef["user_schede_rifiuti:pericoloso"][0].checked=false;			
			FormRef["user_schede_rifiuti:pericoloso"][1].checked=true;
			// disabilito edit classi H
//			FormRef["user_schede_rifiuti:H1"][0].disabled=true;
//			FormRef["user_schede_rifiuti:H1"][1].disabled=true;
//			FormRef["user_schede_rifiuti:H2"][0].disabled=true;
//			FormRef["user_schede_rifiuti:H2"][1].disabled=true;
//			FormRef["user_schede_rifiuti:H3A"][0].disabled=true;
//			FormRef["user_schede_rifiuti:H3A"][1].disabled=true;
//			FormRef["user_schede_rifiuti:H3B"][0].disabled=true;
//			FormRef["user_schede_rifiuti:H3B"][1].disabled=true;
//			FormRef["user_schede_rifiuti:H4"][0].disabled=true;
//			FormRef["user_schede_rifiuti:H4"][1].disabled=true;
//			FormRef["user_schede_rifiuti:H5"][0].disabled=true;
//			FormRef["user_schede_rifiuti:H5"][1].disabled=true;
//			FormRef["user_schede_rifiuti:H6"][0].disabled=true;
//			FormRef["user_schede_rifiuti:H6"][1].disabled=true;
//			FormRef["user_schede_rifiuti:H7"][0].disabled=true;
//			FormRef["user_schede_rifiuti:H7"][1].disabled=true;
//			FormRef["user_schede_rifiuti:H8"][0].disabled=true;
//			FormRef["user_schede_rifiuti:H8"][1].disabled=true;
//			FormRef["user_schede_rifiuti:H9"][0].disabled=true;
//			FormRef["user_schede_rifiuti:H9"][1].disabled=true;
//			FormRef["user_schede_rifiuti:H10"][0].disabled=true;
//			FormRef["user_schede_rifiuti:H10"][1].disabled=true;
//			FormRef["user_schede_rifiuti:H11"][0].disabled=true;
//			FormRef["user_schede_rifiuti:H11"][1].disabled=true;
//			FormRef["user_schede_rifiuti:H12"][0].disabled=true;
//			FormRef["user_schede_rifiuti:H12"][1].disabled=true;
//			FormRef["user_schede_rifiuti:H13"][0].disabled=true;
//			FormRef["user_schede_rifiuti:H13"][1].disabled=true;
//			FormRef["user_schede_rifiuti:H14"][0].disabled=true;
//			FormRef["user_schede_rifiuti:H14"][1].disabled=true;
//			FormRef["user_schede_rifiuti:H15"][0].disabled=true;
//			FormRef["user_schede_rifiuti:H15"][1].disabled=true;

			// disabilito edit classi HP
			FormRef["user_schede_rifiuti:HP1"][0].disabled=true;
			FormRef["user_schede_rifiuti:HP1"][1].disabled=true;
			FormRef["user_schede_rifiuti:HP2"][0].disabled=true;
			FormRef["user_schede_rifiuti:HP2"][1].disabled=true;
			FormRef["user_schede_rifiuti:HP3"][0].disabled=true;
			FormRef["user_schede_rifiuti:HP3"][1].disabled=true;
			FormRef["user_schede_rifiuti:HP4"][0].disabled=true;
			FormRef["user_schede_rifiuti:HP4"][1].disabled=true;
			FormRef["user_schede_rifiuti:HP5"][0].disabled=true;
			FormRef["user_schede_rifiuti:HP5"][1].disabled=true;
			FormRef["user_schede_rifiuti:HP6"][0].disabled=true;
			FormRef["user_schede_rifiuti:HP6"][1].disabled=true;
			FormRef["user_schede_rifiuti:HP7"][0].disabled=true;
			FormRef["user_schede_rifiuti:HP7"][1].disabled=true;
			FormRef["user_schede_rifiuti:HP8"][0].disabled=true;
			FormRef["user_schede_rifiuti:HP8"][1].disabled=true;
			FormRef["user_schede_rifiuti:HP9"][0].disabled=true;
			FormRef["user_schede_rifiuti:HP9"][1].disabled=true;
			FormRef["user_schede_rifiuti:HP10"][0].disabled=true;
			FormRef["user_schede_rifiuti:HP10"][1].disabled=true;
			FormRef["user_schede_rifiuti:HP11"][0].disabled=true;
			FormRef["user_schede_rifiuti:HP11"][1].disabled=true;
			FormRef["user_schede_rifiuti:HP12"][0].disabled=true;
			FormRef["user_schede_rifiuti:HP12"][1].disabled=true;
			FormRef["user_schede_rifiuti:HP13"][0].disabled=true;
			FormRef["user_schede_rifiuti:HP13"][1].disabled=true;
			FormRef["user_schede_rifiuti:HP14"][0].disabled=true;
			FormRef["user_schede_rifiuti:HP14"][1].disabled=true;
			FormRef["user_schede_rifiuti:HP15"][0].disabled=true;
			FormRef["user_schede_rifiuti:HP15"][1].disabled=true;

			// non � pericoloso, mostro bottone per creare scheda sistri
			document.getElementById('CreaSchedaFromRif').style.display = 'none';
			}

		FormRef["user_schede_rifiuti:pericoloso"][0].disabled=true;
		FormRef["user_schede_rifiuti:pericoloso"][1].disabled=true;
	}
	Pars = "cer=" + FName.value;
	CerCheck.update(Pars); //}}}
}


function BUDGETANNO(NotUsed,FieldRef,FormRef){
	// attenzione, non viene fatto controllo sulla presenza di virgola ( i decimali si fanno col punto, no separatore migliaia )
	// se c'� virgola non vengono considerati i decimali ( 1000,5 => 1000 )

	var gennaio = parseFloat(FormRef["user_contratti_budget:euro_gen"].value);
	var febbraio = parseFloat(FormRef["user_contratti_budget:euro_feb"].value);
	var marzo = parseFloat(FormRef["user_contratti_budget:euro_mar"].value);
	var aprile = parseFloat(FormRef["user_contratti_budget:euro_apr"].value);
	var maggio = parseFloat(FormRef["user_contratti_budget:euro_mag"].value);
	var giugno = parseFloat(FormRef["user_contratti_budget:euro_giu"].value);
	var luglio = parseFloat(FormRef["user_contratti_budget:euro_lug"].value);
	var agosto = parseFloat(FormRef["user_contratti_budget:euro_ago"].value);
	var settembre = parseFloat(FormRef["user_contratti_budget:euro_set"].value);
	var ottobre = parseFloat(FormRef["user_contratti_budget:euro_ott"].value);
	var novembre = parseFloat(FormRef["user_contratti_budget:euro_nov"].value);
	var dicembre = parseFloat(FormRef["user_contratti_budget:euro_dic"].value);
	var somma = gennaio+febbraio+marzo+aprile+maggio+giugno+luglio+agosto+settembre+ottobre+novembre+dicembre;
	//window.alert(somma);	
	if(somma!=parseFloat(FormRef["user_contratti_budget:euro_anno"].value)){
		window.alert("Attenzione!\nLa somma dei budget mensili non corrisponde al budget annuale.");
		FGE_LockForm = true;
		}
	}




/*
ANAGRAFICHE SOGGETTI
*/

function UPDATE_NATION(NotUsed,FieldRef,FormRef){
	data=FieldRef.name.split(":");
	table=data[0];
	
	var LKUP = new ajaxObject('__scripts/getNation.php',FGE_DummyAjaxCallback);	
	LKUP.callback = function (responseTxt, responseStat) {
		document.FGEForm_1[table+":IN_ITALIA"].value=responseTxt;
		}	
	Parameters = 'ID_COM=' + document.FGEForm_1[table+":ID_COM"].value;
	LKUP.update(Parameters);
	}

function ASK_INHERIT(ActualFieldValue,FieldRef,FormRef){
	TableField=FieldRef.split(":");
	Table=TableField[0];
	switch(Table){
                case "user_autorizzazioni_pro":
			PRI="ID_AUTH";
			PREV="PREV_num_aut";
			break;		
		case "user_autorizzazioni_dest":
			PRI="ID_AUTHD";
			PREV="PREV_num_aut";
			break;			
		case "user_autorizzazioni_trasp":
			PRI="ID_AUTHT";
			PREV="PREV_num_aut";
			break;			
		case "user_autorizzazioni_interm":
			PRI="ID_AUTHI";
			PREV="PREV_num_aut";
			break;			
		case "user_automezzi":
			PRI="ID_AUTO";
			PREV="PREV_description";
			break;			
		case "user_rimorchi":
			PRI="ID_RMK";
			PREV="PREV_description";
			break;			
		}
	if(document.getElementById(Table+":"+PRI).value!=""){
		if(window.confirm("Desidera che le modifiche vengano apportate a tutti i record affini?"))
			document.FGEForm_1[Table + ":inherit_edits"].value=1;
		}
	else{
		document.getElementById(Table+":"+PREV).value=ActualFieldValue;
		}
	}

function CUSTOM_PIVA(pi,Fname,FormRef) {
	// controllo validit� partita iva / codice fiscale solo se azienda italiana
	data=Fname.split(":");
	table=data[0];
	if(document.FGEForm_1[table+":IN_ITALIA"].value==1){

		if(!FGE_isMandatory(Fname) && pi=="") {
			return false;
			}	
				
		if( pi.length != 11 ) {
			FGE_Error("Il campo \'partita IVA\' deve essere di 11 caratteri",Fname);
			return;
		}
		validi = "0123456789";
			for( i = 0; i < 11; i++ ){
				if(validi.indexOf( pi.charAt(i) ) == -1) {
				FGE_Error("Il contenuto del campo \'partita IVA\' e\' errato.\n",Fname);
				return;
				}
			}
		s = 0;
		for( i = 0; i <= 9; i += 2 ) 
			s += pi.charCodeAt(i) - '0'.charCodeAt(0);
			for( i = 1; i <= 9; i += 2 ){
					c = 2*( pi.charCodeAt(i) - '0'.charCodeAt(0) );
					if( c > 9 )  c = c - 9;
					s += c;
			}
			if( ( 10 - s%10 )%10 != pi.charCodeAt(10) - '0'.charCodeAt(0) ) {
				FGE_Error("Il contenuto del campo \'partita IVA\' e\' errato.\n",Fname);
				return; 
			}
		
		}
	}



function CUSTOM_CFISC(cf,Fname,FormRef) {

	// controllo validit� codice fiscale se azienda italiana

	data=Fname.split(":");
	table=data[0];
	if(document.FGEForm_1[table+":IN_ITALIA"].value==1){

		if(!FGE_isMandatory(Fname) && cf=="") {
			return false;
			}
		var validi, i, s, set1, set2, setpari, setdisp;
		//if( cf == '' )  return '';
		cf = cf.toUpperCase();
		if( cf.length != 16 && cf.length != 11) {
		FGE_Error("Il campo \'codice fiscale\' deve essere di 16 o 11 caratteri\n",Fname);
		return;
		}
		if(cf.length == 16) {
		validi = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		for( i = 0; i < 16; i++ ){
			if( validi.indexOf( cf.charAt(i) ) == -1 ) {
			FGE_Error("Il campo \'codice fiscale\' e\' errato\n",Fname);
			}
		}
		set1 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		set2 = "ABCDEFGHIJABCDEFGHIJKLMNOPQRSTUVWXYZ";
		setpari = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		setdisp = "BAKPLCQDREVOSFTGUHMINJWZYX";
		s = 0;
		for( i = 1; i <= 13; i += 2 )
			s += setpari.indexOf( set2.charAt( set1.indexOf( cf.charAt(i) )));
		for( i = 0; i <= 14; i += 2 )
			s += setdisp.indexOf( set2.charAt( set1.indexOf( cf.charAt(i) )));
		if( s%26 != cf.charCodeAt(15)-'A'.charCodeAt(0) ) {
			FGE_Error("Il campo \'codice fiscale\' e\' errato\n",Fname);
			return;
		}
		}
		if(cf.length==11) {
			validi = "0123456789";
			for( i = 0; i < 11; i++ ){
				if(validi.indexOf( cf.charAt(i) ) == -1) {
					FGE_Error("Il contenuto del campo \'codice fiscale\' e\' errato.\n",Fname);
					return;
				}
			}
			s = 0;
			for( i = 0; i <= 9; i += 2 ) 
				s += cf.charCodeAt(i) - '0'.charCodeAt(0);
				for( i = 1; i <= 9; i += 2 ){
				c = 2*( cf.charCodeAt(i) - '0'.charCodeAt(0) );
				if( c > 9 )  c = c - 9;
				s += c;
				}
				if( ( 10 - s%10 )%10 != cf.charCodeAt(10) - '0'.charCodeAt(0) ) {
					FGE_Error("Il contenuto del campo \'codice fiscale\' e\' errato.\n",Fname);
					return; 
				}	    
			}

		// controllo che il codice fiscale non sia gi� presente nel db
		var Tables = new Array("user_aziende_produttori", "user_aziende_trasportatori", "user_aziende_intermediari", "user_aziende_destinatari");
		if(in_array(table, Tables)){

			switch(table){
				case "user_aziende_produttori":
					var filter = document.FGEForm_1[table+":ID_AZP"].value;
					break;
				
				case "user_aziende_trasportatori":
					var filter = document.FGEForm_1[table+":ID_AZT"].value;
					break;
				
				case "user_aziende_intermediari":
					var filter = document.FGEForm_1[table+":ID_AZI"].value;
					break;

				case "user_aziende_destinatari":
					var filter = document.FGEForm_1[table+":ID_AZD"].value;
					break;
				}
			if( filter=='' ) filter = '0';
			FGE_LockForm = true;
			var LKUP = new ajaxObject('__scripts/CHECK_UniqueCF.php',FGE_DummyAjaxCallback);
			LKUP.callback = function (responseTxt, responseStat) {
				ResArray = responseTxt.split("|");
				if(ResArray[0]=='1'){
					if(!window.confirm("Attenzione, un'azienda con lo stesso codice fiscale e' gia' presente in archivio ("+ResArray[1]+").\n\nProcedere al salvataggio?")){
						FGE_Error("Salvataggio annullato.\n",Fname);
						return;
						}
					else FGE_LockForm = false;
					FGE_SendForm();
					}
				else FGE_LockForm = false;
				FGE_SendForm();
				}
			Parameters = 'CF=' + cf + '&table=' + table + '&filter=' + filter;
			LKUP.update(Parameters);

			}
		}
	}

function patentiADR(label,FRef,FormRef) {
	// abilita / disabilita campi patente, rilascio e scadenza x gli autisti
//{{{ 
	if(document.forms[0]['user_autisti:adr'][1].checked) {
		document.getElementById('user_autisti:patente').disabled=true;
		document.getElementById('user_autisti:rilascio').disabled=true;
		document.getElementById('user_autisti:scadenza').disabled=true;
	} else {
		document.getElementById('user_autisti:patente').disabled=false;
		document.getElementById('user_autisti:rilascio').disabled=false;
		document.getElementById('user_autisti:scadenza').disabled=false;
	} //}}}
}

function patentiADRcheck() {
	// controllo pertinenza dati patenti adr autisti
//{{{ 
	if(document.forms[0]['user_autisti:adr'][0].checked) {
		if(document.forms[0]['user_autisti:patente'].value=="") {
			FGE_Error("Se l\'autista e\' abilitato ADR occorre inserire la patente\n",'user_autisti:patente');		
		}
		if(document.forms[0]['user_autisti:rilascio'].value=="") {
			FGE_Error("Se l\'autista e\' abilitato ADR occorre inserire la data di rilascio della patente",'user_autisti:rilascio');		
		}
		if(document.forms[0]['user_autisti:scadenza'].value=="") {
			FGE_Error("Se l\'autista e\' abilitato ADR occorre inserire la data di scadenza della patente",'user_autisti:scadenza');		
		}
	} //}}}
}

function CUSTOM_ALERT_COSTI(label,FName,FormRef) {	
	var ID_MZ_TRA=document.FGEForm_1["user_automezzi:ID_MZ_TRA"].options[document.FGEForm_1["user_automezzi:ID_MZ_TRA"].options.selectedIndex].value;
	var FURGONE = '7';
	if(ID_MZ_TRA==FURGONE)
		window.alert('Ricorda che la selezione del furgone implica minori costi stimati per il trasporto');
	}


function UPDATE_QL_ADR(label,FName,FormRef) {	
	TableName = FName.name.substring(0, FName.name.indexOf(':'));
	Esente=FName.value;
	selectAutisti	= document.getElementById(TableName+':ID_AUTST');
	selectAutomezzi	= document.getElementById(TableName+':ID_AUTO');
	selectRimorchi	= document.getElementById(TableName+':ID_RMK');
	var LKUP = new ajaxObject('__mov_snipplets/CMB_ADR.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		//window.alert(responseTxt);
		//responseTxt="<option>autisti</option>[nextItem]<option>automezzi</option>[nextItem]<option>rimorchi</option>";
		selectArray=responseTxt.split("[nextItem]");		
		// 0-Autisti // 1-Automezzi // 2-Rimorchi
		selectAutisti.innerHTML=selectArray[0];
		selectAutomezzi.innerHTML=selectArray[1];
		selectRimorchi.innerHTML=selectArray[2];
		//window.alert(selectAutisti.innerHTML);	
		}
	Parameters = 'Esente=' + Esente + '&ID_UIMT=' + document.getElementById(TableName+':ID_UIMT').value + '&ID_AUTHT=' + document.getElementById(TableName+':ID_AUTHT').value;
	LKUP.update(Parameters);
	}


/*
MOVIMENTI - CORREZIONE SCARICO
*/
var CorrezioneMovBusy = false;
var MovCorrRetry;


function CorreggiSC_Inter(){
	
	//
	//	crea movimento di carico automatico - versione intermediario
	//
	if(document.FGEForm_1["user_movimenti_fiscalizzati:ID_AZP"].value=="garbage" | document.FGEForm_1["user_movimenti_fiscalizzati:ID_UIMP"].value=="garbage") {
		alert("Un Produttore ed un Impianto devono essere selezionati!");
		return;
		}

	var AutoCarico = new ajaxObject('__scripts/MovimentiCaricoAutomatico.php',FGE_DummyAjaxCallback);
		AutoCarico.callback = function (responseTxt, responseStat) {
			document.FGEForm_1["user_movimenti_fiscalizzati:NMOV"].value = Math.ceil(document.FGEForm_1["user_movimenti_fiscalizzati:NMOV"].value)+1;
			CUSTOM_CORR_SCARICO_SCREEN_UPD(document.FGEForm_1["user_movimenti_fiscalizzati:ID_RIF"].value, 'user_movimenti_fiscalizzati');
			FGE_LockForm = false;
			Message = "Il movimento sta per essere salvato...";
			MovCorrRetry = setInterval("CorrezioneMovimentoInvioDifferito(Message)", 100);
		}

	Differenza = parseFloat(document.FGEForm_1["user_movimenti_fiscalizzati:quantita"].value)-parseFloat(document.FGEForm_1["user_movimenti_fiscalizzati:FKEdisponibilita"].value);
	UnMisura = document.FGEForm_1["user_movimenti_fiscalizzati:FKEumis"].value;
	PesoSpec = document.FGEForm_1["user_movimenti_fiscalizzati:FKEpesospecifico"].value;
	Kg =  QConvert(Differenza,UnMisura,PesoSpec);
	Parameters  = "DIFF=" + Differenza + "&KG=" + Kg + "&ID_RIF=" + document.FGEForm_1["user_movimenti_fiscalizzati:ID_RIF"].value;
	Parameters += "&Dispo=" + document.FGEForm_1["user_movimenti_fiscalizzati:FKEdisponibilita"].value;
	Parameters += "&NMOV=" + document.FGEForm_1["user_movimenti_fiscalizzati:NMOV"].value;
	Parameters += "&UMIS=" + document.FGEForm_1["user_movimenti_fiscalizzati:FKEumis"].value;
	Parameters += "&SF=" + document.FGEForm_1["user_movimenti_fiscalizzati:FKESF"].value;
	Parameters += "&PS=" + document.FGEForm_1["user_movimenti_fiscalizzati:FKEpesospecifico"].value;
	// PARAMETRI PRODUTTORE
	Parameters += "&ID_AZP=" + document.FGEForm_1["user_movimenti_fiscalizzati:ID_AZP"].value;
	Parameters += "&FKEcfiscP=" + document.FGEForm_1["user_movimenti_fiscalizzati:FKEcfiscP"].value;
	Parameters += "&ID_UIMP=" + document.FGEForm_1["user_movimenti_fiscalizzati:ID_UIMP"].value;

	// se non sono produttore, nel carico automatico riporto anche i dati di trasportatore, destinatario e intermediario..
	// il carico sarebbe l'ingresso nell'impianto del rifiuto con formulario, dovrebbe essere fatto manualmente, ma alcuni (Marchesini) lo registrano come movimento di scarico
	if(document.FGEForm_1["user_movimenti_fiscalizzati:produttore"].value==0){
		// DTFORM
		Parameters += "&DTFORM=" + document.FGEForm_1["user_movimenti_fiscalizzati:DTFORM"].value;
		// NFORM
		Parameters += "&NFORM=" + document.FGEForm_1["user_movimenti_fiscalizzati:NFORM"].value;
		// ADR
		if(document.FGEForm_1["user_movimenti_fiscalizzati:adrA"].checked) adr=0; else adr=1;
		Parameters+="&adr=" + adr;
		// NUMERO ONU
		if(document.FGEForm_1["user_movimenti_fiscalizzati:ID_ONU"].value!="garbage") {
			Parameters += "&ID_ONU=" + document.FGEForm_1["user_movimenti_fiscalizzati:ID_ONU"].value;
			}
		else{
			Parameters += "&ID_ONU=0";
			}
		// Quantit� limitata
		if(document.FGEForm_1["user_movimenti_fiscalizzati:QLA"].checked) QL=0; else QL=1;
		Parameters+="&QL=" + QL;
		// PARAMETRI DESTINATARIO
		Parameters += "&ID_AZD=" + document.FGEForm_1["user_movimenti_fiscalizzati:ID_AZD"].value;
		Parameters += "&FKEcfiscD=" + document.FGEForm_1["user_movimenti_fiscalizzati:FKEcfiscD"].value;
		Parameters += "&ID_UIMD=" + document.FGEForm_1["user_movimenti_fiscalizzati:ID_UIMD"].value;
		if(document.FGEForm_1["user_movimenti_fiscalizzati:ID_AUTHD"].value!="garbage") {
			Parameters += "&ID_AUTHD=" + document.FGEForm_1["user_movimenti_fiscalizzati:ID_AUTHD"].value;
			}
		else {
			Parameters += "&ID_AUTHD=0";	
			}
		Parameters += "&FKErilasciD=" + document.FGEForm_1[table+":FKErilascioD"].value;
		Parameters += "&FKEscadenzaD=" + document.FGEForm_1[table+":FKEscadenzaD"].value;
		if(document.FGEForm_1["user_movimenti_fiscalizzati:ID_OP_RS"].value!="garbage") {
			Parameters += "&ID_OP_RS=" + document.FGEForm_1["user_movimenti_fiscalizzati:ID_OP_RS"].value;
			}
		else {
			Parameters += "&ID_OP_RS=0";	
			}
		// PARAMETRI TRASPORTATORE
		Parameters += "&ID_AZT=" + document.FGEForm_1["user_movimenti_fiscalizzati:ID_AZT"].value;
		Parameters += "&FKEcfiscT=" + document.FGEForm_1["user_movimenti_fiscalizzati:FKEcfiscT"].value;
		Parameters += "&ID_UIMT=" + document.FGEForm_1["user_movimenti_fiscalizzati:ID_UIMT"].value;
		if(document.FGEForm_1["user_movimenti_fiscalizzati:ID_AUTHT"].value!="garbage") {
			Parameters += "&ID_AUTHT=" + document.FGEForm_1["user_movimenti_fiscalizzati:ID_AUTHT"].value;
			}
		else {
			Parameters += "&ID_AUTHT=0";	
			}
		Parameters += "&FKErilasciT=" + document.FGEForm_1[table+":FKErilascioT"].value;
		Parameters += "&FKEscadenzaT=" + document.FGEForm_1[table+":FKEscadenzaT"].value;
		// PARAMETRI INTERMEDIARIO
		Parameters += "&ID_AZI=" + document.FGEForm_1["user_movimenti_fiscalizzati:ID_AZI"].value;
		Parameters += "&FKEcfiscI=" + document.FGEForm_1["user_movimenti_fiscalizzati:FKEcfiscI"].value;
		Parameters += "&ID_UIMI=" + document.FGEForm_1["user_movimenti_fiscalizzati:ID_UIMI"].value;
		if(document.FGEForm_1["user_movimenti_fiscalizzati:ID_AUTHI"].value!="garbage") {
			Parameters += "&ID_AUTHI=" + document.FGEForm_1["user_movimenti_fiscalizzati:ID_AUTHI"].value;
			}
		else {
			Parameters += "&ID_AUTHI=0";	
			}
		Parameters += "&FKErilasciI=" + document.FGEForm_1[table+":FKErilascioI"].value;
		Parameters += "&FKEscadenzaI=" + document.FGEForm_1[table+":FKEscadenzaI"].value;
		}

	// data movimento attuale
	Parameters += "&DTMV=" + document.FGEForm_1["user_movimenti_fiscalizzati:DTMOV"].value;
	Parameters += "&TableName=user_movimenti_fiscalizzati";

	//window.alert(Parameters);

	AutoCarico.update(Parameters);		
	}


function CorreggiSC(table) {
//{{{ 

	//window.alert(table);
	switch(table){
		default:
		case "user_movimenti_fiscalizzati":
			pri="ID_MOV_F";
			break;
		case "user_movimenti":
			pri="ID_MOV";
			break;
		}

	Limite = document.ScaricoInterface["FKELimite"].value;
	document.getElementById('okbutton').disabled=true;
	for(c=0;c<document.ScaricoInterface["correzione"].length;c++) {
		//
		//	scarica max possibile
		//
		if(document.ScaricoInterface["correzione"][c].checked && c==0 && !document.ScaricoInterface["correzione"][c].disabled) {
			document.FGEForm_1[table+":quantita"].value = Limite;
			document.FGEForm_1[table+":qta_hidden"].value = Limite;
			CUSTOM_QNT(null,document.FGEForm_1[table+":quantita"],null);
			DeSelectALL();
			alert("Il movimento e' stato modificato impostando lo scarico alla massima quantita' possibile");
			document.getElementById('okbutton').disabled=false;
			FGE_LockForm = false;			
		}
		//
		//	modifica giacenza iniziale
		//
		if(document.ScaricoInterface["correzione"][c].checked && c==1 && !document.ScaricoInterface["correzione"][c].disabled) {
			var ModGiacINI = new ajaxObject('__scripts/MovimentiModificaGiacINI.php',FGE_DummyAjaxCallback);	
			ModGiacINI.callback = function (responseTxt, responseStat) {
				CUSTOM_CORR_SCARICO_SCREEN_UPD(document.FGEForm_1[table+":ID_RIF"].value, table);
				DeSelectALL();
				FGE_LockForm = false;
				document.tmp_contoterzi["canSave"].value=1;
				Message = "E' stata incrementata la giacenza iniziale per compensare la quantita' in eccesso dello scarico corrente.\n\nIl movimento di scarico verra' ora salvato";
				MovCorrRetry = setInterval("CorrezioneMovimentoInvioDifferito(Message)", 100);
			}
			Differenza = parseFloat(document.FGEForm_1[table+":quantita"].value)-parseFloat(document.FGEForm_1[table+":FKEdisponibilita"].value);
			Parameters = "ID_RIF=" + document.FGEForm_1[table+":ID_RIF"].value + "&DIFF=" + Differenza ;
			ModGiacINI.update(Parameters);
			document.getElementById('okbutton').disabled=false;
		}
		//
		//	modifica il movimento precedente
		//
		if(document.ScaricoInterface["correzione"][c].checked && c==2 && !document.ScaricoInterface["correzione"][c].disabled) {
			var PrevMovEDIT = new ajaxObject('__scripts/MovimentiModificaMovPrecedente.php',FGE_DummyAjaxCallback);
			PrevMovEDIT.callback = function (responseTxt, responseStat) {
				CUSTOM_CORR_SCARICO_SCREEN_UPD(document.FGEForm_1[table+":ID_RIF"].value, table);
				DeSelectALL();
				FGE_LockForm = false;
				document.tmp_contoterzi["canSave"].value=1;
				Message = "E' stata incrementata la quantita' dell'ultimo movimento di carico per compensare la quantita' in eccesso dello scarico corrente.\n\nIl movimento di scarico verra' ora salvato";
				MovCorrRetry = setInterval("CorrezioneMovimentoInvioDifferito(Message)", 100);
			}	
			Differenza = parseFloat(document.FGEForm_1[table+":quantita"].value)-parseFloat(document.FGEForm_1[table+":FKEdisponibilita"].value);
			UnMisura = document.FGEForm_1[table+":FKEumis"].value;
			PesoSpec = document.FGEForm_1[table+":FKEpesospecifico"].value;
			Kg =  QConvert(Differenza,UnMisura,PesoSpec);
			//Parameters = pri+"="			+ document.FGEForm_1[table+":FKElastCarico"].value + "&DIFF=" + Differenza;
			Parameters  = pri+"="			+ document.FGEForm_1[table+":"+pri].value;
			Parameters += "&DIFF="			+ Differenza;
			Parameters += "&KG="			+ Kg;
			Parameters += "&tara="			+ document.FGEForm_1[table+":tara"].value;
			Parameters += "&pesoL="			+ document.FGEForm_1[table+":pesoL"].value;
			Parameters += "&TableName="		+ table;
			Parameters += "&NMOV="			+ document.FGEForm_1[table+":NMOV"].value;
			Parameters += "&ID_RIF="		+ document.FGEForm_1[table+":ID_RIF"].value;
			Parameters += "&ID_IMP="		+ document.FGEForm_1[table+":ID_IMP"].value;
			Parameters += "&produttore="	+ document.FGEForm_1[table+":produttore"].value;
			Parameters += "&trasportatore=" + document.FGEForm_1[table+":trasportatore"].value;
			Parameters += "&destinatario="  + document.FGEForm_1[table+":destinatario"].value;
			Parameters += "&intermediario=" + document.FGEForm_1[table+":intermediario"].value;
			PrevMovEDIT.update(Parameters);	 
		}
		//
		//	crea movimento di carico automatico
		//
		if(document.ScaricoInterface["correzione"][c].checked && c==3 && !document.ScaricoInterface["correzione"][c].disabled) {
			if(document.FGEForm_1[table+":ID_AZP"].value=="garbage" | document.FGEForm_1[table+":ID_UIMP"].value=="garbage") {
				DeSelectALL();	
				alert("Un Produttore ed un Impianto devono essere selezionati!");
				return;
				}
						
			var AutoCarico = new ajaxObject('__scripts/MovimentiCaricoAutomatico.php',FGE_DummyAjaxCallback);
			AutoCarico.callback = function (responseTxt, responseStat) {
				//window.alert(responseTxt);
				if(document.FGEForm_1[TableName+":NMOV"]!==undefined){
					if(document.FGEForm_1[TableName+":NMOV"].value!='9999999')
						document.FGEForm_1[table+":NMOV"].value = Math.ceil(document.FGEForm_1[table+":NMOV"].value)+1;
					}
				CUSTOM_CORR_SCARICO_SCREEN_UPD(document.FGEForm_1[table+":ID_RIF"].value, table);
				DeSelectALL();
				FGE_LockForm = false;
				document.tmp_contoterzi["canSave"].value=1;
				Message = "E' stato creato un movimento di carico per compensare la quantita' in eccesso dello scarico corrente.\n\nIl movimento di scarico verra' ora salvato";
				MovCorrRetry = setInterval("CorrezioneMovimentoInvioDifferito(Message)", 100);
				}
			Differenza = parseFloat(document.FGEForm_1[table+":pesoN"].value)-parseFloat(document.FGEForm_1[table+":FKEdisponibilita"].value);
			UnMisura = document.FGEForm_1[table+":FKEumis"].value;
			PesoSpec = document.FGEForm_1[table+":FKEpesospecifico"].value;
			// Differenza � sempre in Kg
			Kg = Differenza;
			Differenza = INVConvert(Differenza,UnMisura,PesoSpec);
			Parameters  = "DIFF=" + Differenza + "&KG=" + Kg + "&ID_RIF=" + document.FGEForm_1[table+":ID_RIF"].value + "&originalID_RIF=" + document.FGEForm_1[table+":originalID_RIF"].value;
			Parameters += "&Dispo=" + document.FGEForm_1[table+":FKEdisponibilita"].value;

			// SE STO FACENDO SCHEDA SISTRI, NMOV E' NASCOSTO
			Parameters += "&NMOV=" + document.FGEForm_1[table+":NMOV"].value;
			Parameters += "&UMIS=" + document.FGEForm_1[table+":FKEumis"].value;
			Parameters += "&SF=" + document.FGEForm_1[table+":FKESF"].value;
			Parameters += "&PS=" + document.FGEForm_1[table+":FKEpesospecifico"].value;
			//lotto
			Parameters += "&lotto=" + document.FGEForm_1[table+":lotto"].value;
			Parameters += "&TableName=" + table;
			var useFanghi=testForObject("FGED_"+table+":FANGHI");
			if(useFanghi){
				if(document.FGEForm_1[table+":FANGHI"][0].checked)
					var Fanghi=1;
				else
					var Fanghi=0;
				Parameters +="&FANGHI="+Fanghi;
				}
			else
				Parameters +="&FANGHI=0";
			// PARAMETRI PRODUTTORE
			Parameters += "&ID_AZP=" + document.FGEForm_1[table+":ID_AZP"].value;
			Parameters += "&FKEcfiscP=" + document.FGEForm_1[table+":FKEcfiscP"].value;
			Parameters += "&ID_UIMP=" + document.FGEForm_1[table+":ID_UIMP"].value;
		// se non sono produttore, nel carico automatico riporto anche i dati di trasportatore, destinatario e intermediario..
		// il carico sarebbe l'ingresso nell'impianto del rifiuto con formulario, dovrebbe essere fatto manualmente, ma alcuni (Marchesini) lo registrano come movimento di scarico
		if(document.FGEForm_1[table+":produttore"].value==0){
			// DT_FORM
			Parameters += "&DTFORM=" + document.FGEForm_1[table+":DTFORM"].value;
			// NFORM
			Parameters += "&NFORM=" + document.FGEForm_1[table+":NFORM"].value;
			// ANNEX VII
			// Parameters += "&N_ANNEX_VII=" + document.FGEForm_1[table+":N_ANNEX_VII"].value;
			// ADR
			if(document.FGEForm_1[table+":adrA"].checked) adr=0; else adr=1;
			Parameters+="&adr=" + adr;
			// NUMERO ONU
			if(document.FGEForm_1[table+":ID_ONU"].value!="garbage") {
				Parameters += "&ID_ONU=" + document.FGEForm_1[table+":ID_ONU"].value;
				}
			else{
				Parameters += "&ID_ONU=0";
				}
			// Quantit� limitata
			if(document.FGEForm_1[table+":QLA"].checked) QL=0; else QL=1;
			Parameters+="&QL=" + QL;
			// PARAMETRI DESTINATARIO
			Parameters += "&ID_AZD=" + document.FGEForm_1[table+":ID_AZD"].value;
			Parameters += "&FKEcfiscD=" + document.FGEForm_1[table+":FKEcfiscD"].value;
			Parameters += "&ID_UIMD=" + document.FGEForm_1[table+":ID_UIMD"].value;
			if(document.FGEForm_1[table+":ID_AUTHD"].value!="garbage") {
				Parameters += "&ID_AUTHD=" + document.FGEForm_1[table+":ID_AUTHD"].value;
				}
			else {
				Parameters += "&ID_AUTHD=0";	
				}
			Parameters += "&FKErilascioD=" + document.FGEForm_1[table+":FKErilascioD"].value;
			Parameters += "&FKEscadenzaD=" + document.FGEForm_1[table+":FKEscadenzaD"].value;
			if(document.FGEForm_1[table+":ID_OP_RS"].value!="garbage") {
				Parameters += "&ID_OP_RS=" + document.FGEForm_1[table+":ID_OP_RS"].value;
				}
			else {
				Parameters += "&ID_OP_RS=0";	
				}
			// PARAMETRI TRASPORTATORE
			Parameters += "&ID_AZT=" + document.FGEForm_1[table+":ID_AZT"].value;
			Parameters += "&FKEcfiscT=" + document.FGEForm_1[table+":FKEcfiscT"].value;
			Parameters += "&ID_UIMT=" + document.FGEForm_1[table+":ID_UIMT"].value;
			if(document.FGEForm_1[table+":ID_AUTHT"].value!="garbage") {
				Parameters += "&ID_AUTHT=" + document.FGEForm_1[table+":ID_AUTHT"].value;
				}
			else {
				Parameters += "&ID_AUTHT=0";	
				}
			Parameters += "&FKErilascioT=" + document.FGEForm_1[table+":FKErilascioT"].value;
			Parameters += "&FKEscadenzaT=" + document.FGEForm_1[table+":FKEscadenzaT"].value;
			
			// PARAMETRI INTERMEDIARIO
		
			if(document.FGEForm_1[table+":ID_AZI"].value!="garbage") {
				Parameters += "&ID_AZI=" + document.FGEForm_1[table+":ID_AZI"].value;
				}
			else
				Parameters += "&ID_AZI=0";

			Parameters += "&FKEcfiscI=" + document.FGEForm_1[table+":FKEcfiscI"].value;

			if(document.FGEForm_1[table+":ID_UIMI"].value!="garbage") {
				Parameters += "&ID_UIMI=" + document.FGEForm_1[table+":ID_UIMI"].value;
				}
			else
				Parameters += "&ID_UIMI=0";

			if(document.FGEForm_1[table+":ID_AUTHI"].value!="garbage") {
				Parameters += "&ID_AUTHI=" + document.FGEForm_1[table+":ID_AUTHI"].value;
				}
			else {
				Parameters += "&ID_AUTHI=0";	
				}
			Parameters += "&FKErilasciI=" + document.FGEForm_1[table+":FKErilascioI"].value;
			Parameters += "&FKEscadenzaI=" + document.FGEForm_1[table+":FKEscadenzaI"].value;
			}

			// data movimento attuale
			if(document.FGEForm_1[table+":DTMOV"]===undefined){
				var today	= new Date();
				var d		= today.getDate();
				var m		= today.getMonth()+1;
				var y		= today.getFullYear();
				var DTMOV	= d+'/'+m+'/'+y;
				}
			else DTMOV = document.FGEForm_1[table+":DTMOV"].value;

			Parameters += "&DTMV=" + DTMOV;
			AutoCarico.update(Parameters);	
		}
		
	}
}

function CorrezioneMovimentoInvioDifferito(Message) {
//{{{ 
	if(!CorrezioneMovBusy) {
		clearInterval(MovCorrRetry);
		alert(Message);
		FGEForm_1_SendCheck(document.forms[0]);
		//window.alert(document.forms[0]);
	} else {
		//window.alert("2");
		if(!MovCorrRetry) {
			//window.alert("3");
			MovCorrRetry = setInterval("CorrezioneMovimentoInvioDifferito(Message)", 100);
			//window.alert("4");
		}
	} //}}}
}

function DeSelectALL() {
//{{{ 
	document.getElementById('SOGER_ScaricoInterface').style.zIndex = -1;
	for(q=0;q<document.ScaricoInterface["correzione"].length;q++) {
			document.ScaricoInterface["correzione"][q].checked = false;
	} //}}}
}

/*
MOVIMENTI - ANNULLA FISCALIZZAZIONE
*/

function DELETE_LAST_FISC(){
	var message='Sei sicuro di voler annullare l\'ultima fiscalizzazione eseguita?\n\n';
		message+='I movimenti relativi all\'ultima fiscalizzazione eseguita (e con data non inferiore a 14 giorni fa) verranno cancellati dal registro fiscale e potranno nuovamente essere fiscalizzati dal registro industriale.\n\n';
		message+='Ovviamente l\'eliminazione dei movimenti dal registro fiscale comporta la perdita dei dati relativi al rientro della IV copia e del peso verificato a destino.';
	if(window.confirm(message)){
		var LKUP = new ajaxObject('__mov_snipplets/FRM_DELETE_FISCAL.php',FGE_DummyAjaxCallback);	
		LKUP.callback = function (responseTxt, responseStat) {
			window.alert(responseTxt);
			window.location.reload();
			}	
		Parameters = '';
		LKUP.update(Parameters);
		}
	}


/*
MOVIMENTI - RIORDINO CRONOLOGICO
*/

function RESTORE_NMOV(gg){

	var message ='ATTENZIONE!\n\n';	
		message+='Cliccando su OK il programma ordinera\' cronologicamente per data di movimento le registrazioni degli ultimi '+gg+' giorni.\n\n';
		message+='A parita\' di data del movimento, il programma inserira\' a registro prima le operazioni di carico, poi quelle id scarico.\n\n';
		message+='Si raccomanda di utilizzare la funzione con la dovuta cautela e di verificare preventivamente l\'eventuale stampa del registro.\n\n';
		message+='Si desidera procedere?';
	
	if(window.confirm(message)){
		var LKUP = new ajaxObject('__mov_snipplets/RESTORE_NMOV.php',FGE_DummyAjaxCallback);	
		LKUP.callback = function (responseTxt, responseStat) {
			window.alert(responseTxt);
			window.location.reload();
			}	
		Parameters = '';
		LKUP.update(Parameters);
		}
	}


/*
MOVIMENTI - MODIFICA VELOCE
*/
function MassiveMovFastSave(counter) {
    Errs  = "";
    PatFloat = /^([0-9])+\.?([0-9])*$/;
    PatDt = /^(0[1-9]|1[0-9]|2[0-9]|3[01]|[1-9])\/+(0[1-9]|1[0-2]|[1-9])\/+(19|20)[0-9]{2}$/;
    PatDtTime = /^\d{2}\/\d{2}\/\d{4}\s*(?:\d{2}:\d{2}(?::\d{2})?)?$/;
    MovToSave=0;
    par="";

    for(fir=0;fir<=counter;fir++){
        if(document.getElementById('TYPE_'+fir) && document.getElementById('UPDATE_'+fir).value==1){
            MovToSave=MovToSave+1;
            ID_MOVIMENTO		= document.getElementById('ID_'+fir).value;
            NUM_MOVIMENTO		= document.getElementById('NUM_'+ID_MOVIMENTO).value;
            PESO_DESTINO		= document.getElementById('PD|'+ID_MOVIMENTO).value;
            DATA_RICONSEGNA		= document.getElementById('dtRF'+fir).value;
            DEST_ARRIVO 		= document.getElementById('DEST_ARRIVO|'+ID_MOVIMENTO).value;
            DEST_ACC_RIFIUTO            = document.getElementById('DEST_ACC_RIFIUTO|'+ID_MOVIMENTO).value;
            if(PESO_DESTINO!="" && !PESO_DESTINO.match(PatFloat)){
                Errs += "Il formato del campo 'Peso riscontrato a destino' del movimento numero "+NUM_MOVIMENTO+" e' errato.\n";
                }
            if(DATA_RICONSEGNA!="" && !DATA_RICONSEGNA.match(PatDt)){
                Errs += "Il formato del campo 'Data di riconsegna IV copia' del movimento numero "+NUM_MOVIMENTO+" e' errato.\n";
                }
            if(DEST_ARRIVO!="" && !DEST_ARRIVO.match(PatDtTime)){
                Errs += "Il formato del campo 'Data e ora presa in carico' del movimento numero "+NUM_MOVIMENTO+" e' errato.\n";
                }
            if(MovToSave>1) par=par+"&";
            par=par+MovToSave+"="+ID_MOVIMENTO+"|"+PESO_DESTINO+"|"+DATA_RICONSEGNA+"|"+DEST_ARRIVO+"|"+DEST_ACC_RIFIUTO;
        }
    }

    if(Errs!="") {
        alert('Errore\n-----------------------------\n' + Errs);
        return;
    } 
    else {
        if(confirm('Salvare i dati?')) {
            document.MovList.action = "__mov_snipplets/MOV_MassiveFastSave.php?" + par;
            document.MovList.submit();
            //window.alert(par);
        }
    }
}

/*
MOVIMENTI - FORMULARIO
*/
function CreaData() {
//{{{ 
		var data = new Date();
		gg = data.getDate() + "/";
		mm = data.getMonth() + 1 + "/";
		aaaa = data.getFullYear();
		return gg + mm + aaaa; //}}}
}


/*
MOVIMENTI - COLLETTAME
*/

/*
function CUSTOM_COLLETTAME(NotUsed,FieldRef,FormRef){
	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	
	if(document.FGEForm_1[TableName+":collettame"][0].checked){
		document.getElementById(TableName+":percorso").value='';
		}
	else{
		document.getElementById(TableName+":percorso").value="VIA DIRETTA";
		}
	}
*/

/*
MOVIMENTI - SPECIFICA ALTRO_TIPO_IMBALLAGGIO (SISTRI)
*/

function CUSTOM_ALLOW_TIPO_IMBALLAGGIO(NotUsed,FieldRef,FormRef){
	
	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	
	
	var ID_TIPO_IMBALLAGGIO = document.getElementById(TableName+":ID_TIPO_IMBALLAGGIO").value;
	
	if(ID_TIPO_IMBALLAGGIO=='2090'){
		document.FGEForm_1[TableName+":ALTRO_TIPO_IMBALLAGGIO"].disabled	= false;
		}
	else{
		document.getElementById(TableName+":ALTRO_TIPO_IMBALLAGGIO").value	= '';
		document.FGEForm_1[TableName+":ALTRO_TIPO_IMBALLAGGIO"].disabled	= true;
		}

	}



/*
MOVIMENTI - C. Proprio / C. Terzi
*/

function PROPRIO_TERZI(NotUsed,FieldRef,FormRef){
/*
	TableName = FieldRef.substring(0, FieldRef.indexOf(':'));

	// ABILITAZIONE CONTO TERZI
	if(SOGER_ContoTerziCheck==true){

		if(document.title.indexOf("dettaglio")==-1 && document.getElementById(TableName+":TIPO").value=='S') {

			CF_TRASP	= document.getElementById(TableName+":FKEcfiscT").value.toUpperCase();
			CF_DEST		= document.getElementById(TableName+":FKEcfiscD").value.toUpperCase();
			ContoProprio= document.getElementById(TableName+":NumAlboAutotraspProprio").value;
			ContoTerzi  = document.getElementById(TableName+":NumAlboAutotrasp").value;

			if(CF_TRASP!=""){			
				if(CF_TRASP!=CF_DEST){
					//window.alert("conto terzi");
					if(trim(ContoTerzi)==""){
						msg ="Attenzione, abilitazione al trasporto Conto Terzi non presente.";
						window.alert(msg);
						if(SOGER_ContoTerziLock==true){
							FGE_LockForm = true;
							document.ScaricoInterface["BLOCKED"].value=1;
							}
						else{
							document.ScaricoInterface["BLOCKED"].value=0;
							}						
						return;
						}
					else
						document.ScaricoInterface["BLOCKED"].value=0;
					}
				}	
			}
		}
*/
	}


function PROPRIO_TERZI_AUTOMEZZI(NotUsed,FieldRef,FormRef){

	var TableName = FieldRef.substring(0, FieldRef.indexOf(':'));
        
        if(SOGER_CHECK_FDA==true){
            FGE_LockForm = true;
            var LKUP = new ajaxObject('__FDA/CheckSimilar.php',FGE_DummyAjaxCallback);
            LKUP.callback = function (responseTxt, responseStat) {
                counter = responseTxt;
                var fakeValue = 'garbage';
                var validation = $("#user_movimenti_fiscalizzati\\:ID_AUTO").val()!=fakeValue;

                console.log(validation);
                console.log(counter);

                if(validation && counter=="0")
                    $( "#Popup_FDA" ).dialog("open");
                else{
                    FGE_LockForm = false;
                    FGE_SendForm();
                }
            }
            var cer = $('#FDA-cer').val();
            var mezzo1 = $( '#user_movimenti_fiscalizzati\\:ID_AUTO option:selected' ).text();
            var mezzo2 = $( '#user_movimenti_fiscalizzati\\:ID_RMK option:selected' ).text();
            Parameters = 'cer=' + cer;
            Parameters+= '&mezzo1=' + mezzo1;
            mezzo2 = mezzo2.replace(/\s/g, '');
            if(mezzo2!='')
                Parameters+= '&mezzo2=' + mezzo2;

            console.log(mezzo2);
            LKUP.update(Parameters);
        }
        else{
        if(SOGER_FDALock && document.getElementById(TableName+":FDAinvalid").value==1){
            FGE_LockForm = true;
            window.alert("Attenzione! Impossibile salvare il movimento: la verifica delle targhe ha dato esito negativo.");
            return;
        }
        }

	// ID_LIM

	// 1 - nessuna limitazione
	// 2 - solo conto terzi
	// 3 - solo conto proprio
	// 4 - nessun trasporto -> eliminato
        
        if(SOGER_ContoTerziCheck==true){

		CF_TRASP	= document.getElementById(TableName+":FKEcfiscT").value;
		CF_DEST		= document.getElementById(TableName+":FKEcfiscD").value;
		ID_LIM		= document.tmp_contoterzi_mezzi["automezzoID_LIM"].value;

		//window.alert(ID_LIM);

		if(CF_TRASP!=""){		

			if(CF_TRASP!=CF_DEST){
				//window.alert("conto terzi");
				//if(ID_LIM==3 | ID_LIM==4){
				if(ID_LIM==3){
					msg ="Abilitazioni al trasporto non coerenti!\n\n";
					//msg+="Si sta eseguendo un trasporto in conto terzi, ma l'automezzo non risulta autorizzato.\n";
					//msg+="Verificare le limitazioni relative all'automezzo nell'anagrafica del trasportatore.\n";
					//msg+="Il movimento verra' comunque salvato.";
					window.alert(msg);
					}
				}
			//else{
				//window.alert("conto proprio");
				//if(ID_LIM==2 | ID_LIM==4){
				//	msg ="Abilitazioni al trasporto non coerenti!\n\n";
				//	msg+="Si sta eseguendo un trasporto in conto proprio, ma l'automezzo non risulta autorizzato.\n";
				//	msg+="Verificare le limitazioni relative all'automezzo nell'anagrafica del trasportatore.\n";
				//	msg+="Il movimento verra' comunque salvato.";
				//	window.alert(msg);
				//	}
				//}
			}
		}

    }
    
function FDA_getEcoAuthorization(EcoApiKey, EcoTimestamp, SharedSecret){

    var message_encoded = unescape(encodeURIComponent(EcoApiKey + EcoTimestamp));
    var secret_encoded = unescape(encodeURIComponent(SharedSecret));
    
    var hash = CryptoJS.HmacSHA256(message_encoded, secret_encoded);
    var hashBase64 = CryptoJS.enc.Base64.stringify(hash);
        
    return "HMAC "+hashBase64;
    
}

function PROPRIO_TERZI_RIMORCHI(NotUsed,FieldRef,FormRef){

	TableName = FieldRef.substring(0, FieldRef.indexOf(':'));	

	// ID_LIM

	// 1 - nessuna limitazione
	// 2 - solo conto terzi
	// 3 - solo conto proprio
	// 4 - nessun trasporto

	CF_TRASP	= document.getElementById(TableName+":FKEcfiscT").value;
	CF_DEST		= document.getElementById(TableName+":FKEcfiscD").value;
	ID_LIM		= document.tmp_contoterzi_mezzi["rimorchioID_LIM"].value;

	//window.alert(ID_LIM);

	if(CF_TRASP!=""){		

		if(CF_TRASP!=CF_DEST){
			//window.alert("conto terzi");
			//if(ID_LIM==3 | ID_LIM==4){
			if(ID_LIM==3){
				msg ="Abilitazioni al trasporto non coerenti!\n\n";
				//msg+="Si sta eseguendo un trasporto in conto terzi, ma il rimorchio non risulta autorizzato.\n";
				//msg+="Verificare le limitazioni relative al rimorchio nell'anagrafica del trasportatore.\n";
				//msg+="Il movimento verra' comunque salvato.";
				window.alert(msg);
				}
			}
		//else{
			//window.alert("conto proprio");
			//if(ID_LIM==2){
				//msg ="Abilitazioni al trasporto non coerenti!\n\n";
				//msg+="Si sta eseguendo un trasporto in conto proprio, ma il rimorchio non risulta autorizzato.\n";
				//msg+="Verificare le limitazioni relative al rimorchio nell'anagrafica del trasportatore.\n";
				//msg+="Il movimento verra' comunque salvato.";
				//window.alert(msg);
				//}
			//}
		}

	}



/*
MOVIMENTI - QUANTITA'
*/
function MOVIMENTOZERO(NotUsed,FieldRef,FormRef) {

	TableName = FieldRef.substring(0, FieldRef.indexOf(':'));
	FormRef[TableName+":qta_hidden"].value=FormRef[TableName+":quantita"].value;
/*
	if(Math.round(FormRef[TableName+":quantita"].value)<0) {
		alert('Attenzione: il movimento ha quantita\' negativa!\n\nImpossibile salvare il movimento: verificare le quantita\'');
		FGE_LockForm = true;
		return;
		} 

	if(Math.ceil(FormRef[TableName+":quantita"].value)==0) {
		if(!confirm('La quantita\' movimentata e\' zero: procedere comunque?')) {
			FGE_LockForm = true;
			return;
			} 
		}
	
	if(TableName=="user_movimenti_fiscalizzati" && FormRef[TableName+":TIPO"].value=="C"){
		if(Math.round(FormRef[TableName+":quantita"].value)<Math.round(FormRef[TableName+":qta_hidden"].value) && Math.round(FormRef[TableName+":qta_hidden"].value)>0){
			alert('Attenzione: il movimento ha quantita\' inferiore a quella originale ( ' + Math.round(FormRef[TableName+":qta_hidden"].value) + ' ) !\n\nImpossibile salvare il movimento: verificare le quantita\'');
			FGE_LockForm = true;
			return;
			}
		}
*/	
	}


function CUSTOM_ZEROQ(TableName) {
	// azzera quantit� e pesi
//{{{ 
	document.FGEForm_1[TableName+":tara"].value = 0;
	document.FGEForm_1[TableName+":pesoL"].value = 0;
	document.FGEForm_1[TableName+":pesoN"].value = 0;
	document.FGEForm_1[TableName+":quantita"].value = 0;
	document.FGEForm_1[TableName+":qta_hidden"].value = 0;
//}}}	
}
function CUSTOM_PESON(NotUsed,FieldRef,FormRef) {
	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	
	// genera il peso netto in Kg e la quantit� nella unit� di misura nativa (a partire da tara+peso lordo)
//{{{ 
	pesoLordo = document.FGEForm_1[TableName+":pesoL"].value;
	Tara	  = document.FGEForm_1[TableName+":tara"].value;
	UnMisura  = document.FGEForm_1[TableName+":FKEumis"].value;
	PesoSpec  = document.FGEForm_1[TableName+":FKEpesospecifico"].value;
	
	if(document.FGEForm_1[TableName+":ID_RIF"].value!="garbage") {
		if(document.FGEForm_1[TableName+":quantita"].value=="0" | (document.FGEForm_1[TableName+":pesoL"].value!="0" | document.FGEForm_1[TableName+":tara"].value!="0")) {
			var netto = pesoLordo - Tara;
			document.FGEForm_1[TableName+":pesoN"].value = parseFloat(netto.toFixed(2));
			var quantita = INVConvert(netto, UnMisura, PesoSpec);
			document.FGEForm_1[TableName+":quantita"].value = parseFloat(quantita.toFixed(2));
			document.FGEForm_1[TableName+":qta_hidden"].value = document.FGEForm_1[TableName+":quantita"].value;
		}
	} else {
		alert('Selezionare prima un rifiuto!');	
		CUSTOM_ZEROQ(TableName);
	}	
	 //}}}
}
function CUSTOM_QNT(NotUsed,FieldRef,FormRef) {

	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	

	// genera il peso netto in Kg (a partire dalla quantit� nella unit� di misura nativa) 

	document.FGEForm_1[TableName+":tara"].value = 0;
	document.FGEForm_1[TableName+":pesoL"].value = 0;
	quantita = document.FGEForm_1[TableName+":quantita"].value;
	UnMisura = document.FGEForm_1[TableName+":FKEumis"].value;
	PesoSpec = document.FGEForm_1[TableName+":FKEpesospecifico"].value;
	
	if(document.FGEForm_1[TableName+":ID_RIF"].value!="garbage") {
		document.FGEForm_1[TableName+":pesoN"].value = QConvert(quantita,UnMisura,PesoSpec);
		}
	else {
		alert('Selezionare prima un rifiuto!');
		CUSTOM_ZEROQ(TableName);
		}
	
	// SISTRI: posso modificare qta anche nei mov. fiscalizzati
	if(TableName=='user_movimenti_fiscalizzati')
		document.ScaricoInterface["correzione"][2].disabled=false;
	
	/*
	if(document.FGEForm_1[TableName+":TIPO"].value=='S'){
		var LKUP = new ajaxObject('__scripts/MovimentiIsAnnuale.php',FGE_DummyAjaxCallback);	
		LKUP.callback = function (responseTxt, responseStat) {	
			if(responseTxt=='1')
				document.ScaricoInterface["correzione"][2].disabled=true;
			else{
				document.ScaricoInterface["correzione"][2].disabled=false;
				}
			}
		Parameters = 'IDlastCarico=' + document.FGEForm_1[TableName+":FKElastCarico"].value;
		Parameters+= '&TableName=' + TableName;
		LKUP.update(Parameters);
		}
	*/
	}


function QConvert(Q,UM,PSpec) {
	// converte dalla quantit� nella unit� di misura nativa a Kg (due decimali)
//{{{ 
		Q=Q*100;
		switch(UM) {
			case "Kg.":
				valore = Math.round(Q);
			break;
			case "Mc.":
				valore = Math.round(Q*PSpec*1000);
			break;
			case "Litri":
				valore = Math.round(Q*PSpec);
			break;
		}	
		return valore/100;
		//}}}
}
function INVConvert(Q,UM,PSpec) {
	// converte da Kg alla quantit� nella unit� di misura nativa
//{{{ 
		Q=Q*100;
		switch(UM) {
			case "Kg.":
				valore = Q;
			break;
			case "Mc.":
				valore = Math.round(Q/PSpec/1000);
			break;
			case "Litri":
				valore = Math.round(Q/PSpec);
			break;
		}	 
		return valore/100;
		//}}}	
}



function MOV_DISPO_CHECK(Disponibilita,FieldRef,FormRef) {
	
	
	FGE_LockForm = true;
	var error=false;
	

	TableName = FieldRef.substring(0, FieldRef.indexOf(':'));
	if(TableName=='user_movimenti') pri = "ID_MOV"; else pri = "ID_MOV_F";

	

/****************** EX MOVIMENTOZERO() *******************/

	// CONTROLLO QTA NEGATIVA
	if(Math.round(FormRef[TableName+":quantita"].value)<0) {
		alert('Attenzione: il movimento ha quantita\' negativa!\n\nImpossibile salvare il movimento: verificare le quantita\'');
		var error = true;
		return;
		} 

	// CONTROLLO QTA == 0
	if(Math.ceil(FormRef[TableName+":quantita"].value)==0) {
		if(!confirm('La quantita\' movimentata e\' zero: procedere comunque?')) {
			var error = true;
			return;
			} 
		}
                
        // CONTROLLO SIA SELEZIONATO ID_UIMP
        if (document.getElementById(TableName+":ID_UIMP") != null){
            if(document.getElementById(TableName+":ID_UIMP").value=='garbage'){
                alert('Attenzione: verifica di aver selezionato l\'unita\' locale del produttore');
		var error = true;
		return;
            }
        }
	
	// CONTROLLO DIMINUZIONE CARICO
	/*
	if(TableName=="user_movimenti_fiscalizzati" && FormRef[TableName+":TIPO"].value=="C"){
		if(Math.round(FormRef[TableName+":quantita"].value)<Math.round(FormRef[TableName+":qta_hidden"].value) && Math.round(FormRef[TableName+":qta_hidden"].value)>0){
			alert('Attenzione: il movimento ha quantita\' inferiore a quella originale ( ' + Math.round(FormRef[TableName+":qta_hidden"].value) + ' ) !\n\nImpossibile salvare il movimento: verificare le quantita\'');
			var error = true;
			return;
			}
		}
	*/

/***************** EX PROPRIO_TERZI() *******************/
	// ABILITAZIONE CONTO TERZI
	if(SOGER_ContoTerziCheck==true){
		if(document.title.indexOf("dettaglio")==-1 && document.getElementById(TableName+":TIPO").value=='S') {
			CF_TRASP	= document.getElementById(TableName+":FKEcfiscT").value.toUpperCase();
			CF_DEST		= document.getElementById(TableName+":FKEcfiscD").value.toUpperCase();
			ContoProprio= document.getElementById(TableName+":NumAlboAutotraspProprio").value;
			ContoTerzi  = document.getElementById(TableName+":NumAlboAutotrasp").value;
			if(CF_TRASP!="" && CF_TRASP!=CF_DEST && trim(ContoTerzi)==""){
				window.alert("Attenzione, abilitazione al trasporto Conto Terzi non presente.");
				if(SOGER_ContoTerziLock==true){
					var error = true;
					return
					}
				}
			}
		}



	// CONTROLLO COMPILAZIONE TIPO DI IMBALLAGGIO // SISTRI
	// IF IE<9 getComputedStyle isn't defined!
	if (!window.getComputedStyle) {
		window.getComputedStyle = function(el, pseudo) {
			this.el = el;
			this.getPropertyValue = function(prop) {
				var re = /(\-([a-z]){1})/g;
				if (prop == 'float') prop = 'styleFloat';
				if (re.test(prop)) {
					prop = prop.replace(re, function () {
					return arguments[2].toUpperCase();
					});
				}
			return el.currentStyle[prop] ? el.currentStyle[prop] : null;
			}
		return this;
		}
	}

	var element = document.getElementById('imageRif');
	var style	= window.getComputedStyle(element);
	var bg_img	= style.getPropertyValue('background-image');

	var TIPO	= document.getElementById(TableName+":TIPO").value;
	var PAGINA_DETTAGLIO	= document.title.indexOf("dettaglio");

	var PRODUTTORE = document.getElementById(TableName+":produttore").value;

	var pericoloso_img_1	= "imageRif1";
	var pericoloso_img_2	= "imageRif3";
	var PERICOLOSO_1		= (bg_img.indexOf(pericoloso_img_1) != -1);
	var PERICOLOSO_2		= (bg_img.indexOf(pericoloso_img_2) != -1);

	if (document.getElementById(TableName+":ID_TIPO_IMBALLAGGIO") != null){

		var ID_TIPO_IMBALLAGGIO = document.getElementById(TableName+":ID_TIPO_IMBALLAGGIO").value;
		var ALTRO_TIPO_IMBALLAGGIO = document.getElementById(TableName+":ALTRO_TIPO_IMBALLAGGIO").value;
		ALTRO_TIPO_IMBALLAGGIO = ALTRO_TIPO_IMBALLAGGIO.replace(/^\s+|\s+$/g,'');

		if(!document.FGEForm_1[TableName+":ID_TIPO_IMBALLAGGIO"].disabled && PRODUTTORE=='1' && PAGINA_DETTAGLIO==-1 && TIPO=='S' && (PERICOLOSO_1 || PERICOLOSO_2) && (ID_TIPO_IMBALLAGGIO=='garbage' || (ID_TIPO_IMBALLAGGIO=='2090' && ALTRO_TIPO_IMBALLAGGIO=='')) && document.FGEForm_1[TableName+":SenzaTrasporto"][1].checked==true){
			alert("Attenzione, si sta movimentando un rifiuto pericoloso: e' necessario indicare il tipo di imballaggio.");
			var error = true;
			return;
			}
		//else alert("controllo tipo imballaggio..........OK!");
		}

	// BLOCCO MOVIMENTAZIONE SENZA CLASSIFICAZIONE HP
	if(document.FGEForm_1[TableName+":DTMOV"]===undefined){
		var today	= new Date();
		var d		= today.getDate();
		var m		= today.getMonth()+1;
		var y		= today.getFullYear();
		var DTMOV	= d+'/'+m+'/'+y;
		//var DTMOV	= '00/00/0000';
		}
	else 
		var DTMOV = document.FGEForm_1[TableName+":DTMOV"].value;

	if(document.getElementById('HiddenField_ClassificazioneHP').value==0 && Regolamento1357Attivo(DTMOV)){
		alert("Attenzione, impossibile movimentare il rifiuto. E' necessario riclassificare come da Regolamento UE 1357/2014.");
		var error = true;
		return;
		}
	//else alert("controllo classi HP..........OK!");

	var NFORM = '';
	if(typeof(document.FGEForm_1[TableName+":NFORM"])!='undefined')
		NFORM = document.getElementById(TableName+':NFORM').value;

	var LKUP = new ajaxObject('__mov_snipplets/FRM_VARIOUS_CHECKS.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		ResArray = responseTxt.split("|");
		//alert(dump(ResArray));
		if(ResArray[0]==1){
			// imposta i valori per la correzione del movimento - scarico
			if(document.FGEForm_1[TableName+":TIPO"].value=="S"){
				
				if(Disponibilita<0) {
					alert("Errore:\n------------------------\nla disponibilita' del rifiuto e' negativa");
					var error = true;
					return;
					}
				//else alert("controllo disponibilita' >=0..........OK!");

				if(parseFloat(FormRef[TableName+":pesoN"].value)>parseFloat(Disponibilita) && document.ScaricoInterface["BLOCKED"].value==0) {
					var error = true;
					if(ResArray[2]==0){
						// non posso modificare il carico precedente
						document.ScaricoInterface["correzione"][2].disabled=true;
						}
					// se il numero di movimento dello scarico � 9999999, allora posso creare un nuovo carico
					if(document.FGEForm_1[TableName+":NMOV"].value=='9999999'){
						document.ScaricoInterface["correzione"][3].disabled=false;
						}
					differenza = parseFloat(FormRef[TableName+":pesoN"].value)-parseFloat(Disponibilita);
					differenza = differenza.toFixed(2);
					document.ScaricoInterface["FKELimite"].value = Disponibilita;
					document.ScaricoInterface["D_FKELimite"].value = Disponibilita + " Kg.";
					document.ScaricoInterface["FKELimiteUM"].value = document.FGEForm_1[TableName+":FKEumis"].value;
					document.ScaricoInterface["D_FKELimiteDIFF"].value = differenza + " Kg.";
					document.ScaricoInterface["FKELimiteDIFF"].value = differenza;
					document.getElementById('SOGER_ScaricoInterface').style.zIndex = 2;
					}
				}
			}
		else{
			window.alert(ResArray[1]);
			var error = true;
			return;
			}
		if(!error){
			FGE_LockForm = false;
			FGE_SendForm();
			}
		}
	Parameters = 'TableName=' + TableName + '&TIPO=' + document.getElementById(TableName+':TIPO').value + '&pri=' + pri + '&NMOV=' + document.getElementById(TableName+':NMOV').value + '&filter=' + document.getElementById(TableName+':'+pri).value + '&NFORM=' + NFORM + '&DTMOV=' + DTMOV + '&ID_RIF=' + document.getElementById(TableName+":ID_RIF").value;
	LKUP.update(Parameters);
	}

function CUSTOM_CORR_SCARICO_SCREEN_UPD(IDRif, table) {
	// correzione movimento di scarico: aggiorna i valori a video dopo la correzione del mov. di scarico
	// (mod. giacenza iniziale / modifica mov precedente)


	switch(table){
		default:
		case "user_movimenti_fiscalizzati":
			pri="ID_MOV_F";
			break;
		case "user_movimenti":
			pri="ID_MOV";
			break;
		}

	var UPD = new ajaxObject('__scripts/MovimentiFakeRifData.php',FGE_DummyAjaxCallback);
	UPD.callback = function (responseTxt, responseStat) {
		ResArray = responseTxt.split("|");	
		document.FGEForm_1[table+":FKEdisponibilita"].value = ResArray[15];
		document.FGEForm_1[table+":FKEgiacINI"].value = ResArray[16];
		document.FGEForm_1[table+":FKElastCarico"].value = ResArray[17];
		CorrezioneMovBusy = false;
		}
	CorrezioneMovBusy = true;
	Parameters = 'IDrif=' + IDRif + "&Tipo=" + document.FGEForm_1[table+":TIPO"].value + "&"+pri+"=" + document.FGEForm_1[table+":"+pri].value;
	Parameters+= '&TableName=' + table;
	UPD.update(Parameters);

}


/* UPLOAD IMMAGINE */
function verificaImg() {
	
	var LKUP = new ajaxObject('__mov_snipplets/UploadImgCheck.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		window.alert(responseTxt);
		}
	Parameters = 'path=' + document.UploadImmagine.immagine.value;
	LKUP.update(Parameters);
	}


/*
STAMPA ETICHETTE
*/

function CHECK_SYMBOLS(){
	// max 3 divieti
	// max 5 prescrizioni
	var divieti=0;
	var prescrizioni=0;

	if(document.FGEForm_1["user_schede_rifiuti_etsym:d1"][0].checked) divieti++;
	if(document.FGEForm_1["user_schede_rifiuti_etsym:d3"][0].checked) divieti++;
	if(document.FGEForm_1["user_schede_rifiuti_etsym:d4"][0].checked) divieti++;
	if(document.FGEForm_1["user_schede_rifiuti_etsym:d5"][0].checked) divieti++;

	if(document.FGEForm_1["user_schede_rifiuti_etsym:p2"][0].checked) prescrizioni++;
	if(document.FGEForm_1["user_schede_rifiuti_etsym:p6"][0].checked) prescrizioni++;
	if(document.FGEForm_1["user_schede_rifiuti_etsym:p8"][0].checked) prescrizioni++;
	if(document.FGEForm_1["user_schede_rifiuti_etsym:p10"][0].checked) prescrizioni++;
	if(document.FGEForm_1["user_schede_rifiuti_etsym:p12"][0].checked) prescrizioni++;
	if(document.FGEForm_1["user_schede_rifiuti_etsym:p13"][0].checked) prescrizioni++;
	if(document.FGEForm_1["user_schede_rifiuti_etsym:p18"][0].checked) prescrizioni++;

	if(divieti>3 || prescrizioni>5){
		window.alert("Attenzione, si possono selezionare al massimo 3 divieti e 5 prescrizioni.");
		FGE_LockForm = true;
		return;		
		}

	}

function EtichettePrintShowInterface(ID_RIF) {

	document.EtichetteInterface.ID_RIF.value=ID_RIF;
//{{{ 
	if(window.pageYOffset) {
		NewPos = window.pageYOffset;
		NewPos = (NewPos+100)+ "px";
		document.getElementById('SOGER_EtichetteInterface').style.zIndex =  1;
		document.getElementById('SOGER_EtichetteInterface').style.display =  "block";
		document.getElementById('SOGER_EtichetteInterface').style.top = NewPos;
	} else {
		window.scroll(10,10);
		document.getElementById('SOGER_EtichetteInterface').style.zIndex =  1;
		document.getElementById('SOGER_EtichetteInterface').style.display =  "block";
	}
}


function EtichettePrintGo() {
 
	var selectRegolamento = document.getElementById("REGOLAMENTO");
	var Regolamento = selectRegolamento.options[selectRegolamento.selectedIndex].value;
	var selectColori = document.getElementById("COLORI");
	var Colori = selectColori.options[selectColori.selectedIndex].value;

	document.getElementById('SOGER_EtichetteInterface').style.display =  "none";
	document.getElementById('SOGER_EtichetteInterface').style.zIndex =  -1;
	location.replace("__scripts/ETICHETTE.php?filter="+document.EtichetteInterface.ID_RIF.value+"&colori="+Colori+"&regolamento="+Regolamento);
	
	}


function EtichettePrintAbort() {
	document.getElementById('SOGER_EtichetteInterface').style.display =  "none";
	document.getElementById('SOGER_EtichetteInterface').style.zIndex =  -1;	
}




/*
STAMPA FORMULARIO
*/
function FormularioPrintShowInterface(DestUrl) {
//{{{ 
	if(window.pageYOffset) {
		NewPos = window.pageYOffset;
		NewPos = (NewPos+100)+ "px";
		document.getElementById('SOGER_FormularioInterface').style.zIndex =  1;
		document.getElementById('SOGER_FormularioInterface').style.display =  "block";
		document.getElementById('SOGER_FormularioInterface').style.top = NewPos;
	} else {
		window.scroll(10,10);
		document.getElementById('SOGER_FormularioInterface').style.zIndex =  1;
		document.getElementById('SOGER_FormularioInterface').style.display =  "block";
	}

	document.getElementById('FormularioInterface').action = DestUrl; //}}}
}
function FormularioPrintGo() {
//{{{ 
	if (document.FormularioInterface.FISCALE[0].checked) {
		document.getElementById('FormularioInterface').action += "&Fiscale=0";
	} else {
		document.getElementById('FormularioInterface').action += "&Fiscale=1";
	}
	document.getElementById('SOGER_FormularioInterface').style.display =  "none";
	document.getElementById('SOGER_FormularioInterface').style.zIndex =  -1;
	self.location = document.getElementById('FormularioInterface').action; //}}}
}
function FormularioPrintAbort() {
	document.getElementById('SOGER_FormularioInterface').style.display =  "none";
	document.getElementById('SOGER_FormularioInterface').style.zIndex =  -1;	
}

/*
STAMPA REGISTRO
*/

function Registro(FormRef, Fiscale) {
	
	// l'intermediario non vede "da" e "a"
	// if(typeof(FormRef.da) != 'undefined'){
		if(FormRef.da.value=='' | FormRef.a.value=='') {
			alert('I campi \'da\' ed \'a\' devono essere compilati!');
			return false;
			}
		if(isNaN(FormRef.da.value) | isNaN(FormRef.a.value)) {
			alert('I campi \'da\' ed \'a\' devono essere numerici!');
			return false;
			}
		if(Math.ceil(FormRef.da.value)>Math.ceil(FormRef.a.value)) {
			alert('Il campo \'a\' deve essere maggiore del campo \'da\'!');
			return false;
			}
	//	}
	if(isNaN(FormRef.paginaIniz.value)) {
		alert('I campo \'numero di pagina iniziale\' deve essere numerico!');
		return false;
	}
	// FormRef.Vidimato[0].checked se true : registro vidimato
	if(FormRef.Vidimato[0].checked == true )
		var vidimato = 1;
	else
		var vidimato = 0;

	url = "__scripts/FGE_DataGridEdit.php?table=user_movimenti_fiscalizzati&FGE_action=print&Registro&Fiscalizzato&Vidimato=" + vidimato;
	if(typeof(FormRef.da) != 'undefined'){
		url+="&da=" + FormRef.da.value + "&a=" + FormRef.a.value;
		}
	if(FormRef.cartaBianca.checked) {
		url += "&cartaBianca";	
		}
	if(FormRef.Npagina.checked) {
		url += "&Npagina";	
		}
	url += "&PaginaIniz=" + FormRef.paginaIniz.value; 
	url += "&Fiscale=" + Fiscale;
	self.location = url; //}}}
}

/*
STAMPA REGISTRO
*/

function RegistroUnicoPD(FormRef, Fiscale) {
	
	PatDt = /^(0[1-9]|1[0-9]|2[0-9]|3[01]|[1-9])\/+(0[1-9]|1[0-2]|[1-9])\/+(19|20)[0-9]{2}$/;
	DA_DATA		= FormRef.da_data.value;
	A_DATA		= FormRef.a_data.value;
	if(FormRef.Vidimato[0].checked == true )
		var vidimato = 1;
	else
		var vidimato = 0;
	
	if(DA_DATA=='' | A_DATA=='') {
		alert("I campi 'Dalla data' e 'Alla data' devono essere compilati!");
		return false;
		}
			
	if(!DA_DATA.match(PatDt)){
		alert("Il formato del campo 'Dalla data' e' errato.");
		return false;
		}

	if(!A_DATA.match(PatDt)){
		alert("Il formato del campo 'Alla data' e' errato.");
		return false;
		}

	if(isNaN(FormRef.paginaIniz.value)) {
		alert('I campo \'numero di pagina iniziale\' deve essere numerico!');
		return false;
		}

	url = "__scripts/FGE_DataGridEdit.php?table=user_movimenti_fiscalizzati&FGE_action=print&Registro&Fiscalizzato&Vidimato=" + vidimato;
	if(typeof(FormRef.da_data) != 'undefined'){
		url+="&da_data=" + FormRef.da_data.value + "&a_data=" + FormRef.a_data.value;
		}
	if(FormRef.cartaBianca.checked) {
		url += "&cartaBianca";	
		}
	if(FormRef.Npagina.checked) {
		url += "&Npagina";	
		}
	url += "&PaginaIniz=" + FormRef.paginaIniz.value; 
	url += "&Fiscale=" + Fiscale;
	url += "&UnicoPD";
	self.location = url; //}}}
}

function RegistroFanghi(FormRef, Fiscale) {
//{{{ 

	if(FormRef.da.value=='' | FormRef.a.value=='') {
		alert('I campi \'da\' ed \'a\' devono essere compilati!');
		return false;
	}
	if(isNaN(FormRef.da.value) | isNaN(FormRef.a.value)) {
		alert('I campi \'da\' ed \'a\' devono essere numerici!');
		return false;
	}
	if(isNaN(FormRef.paginaIniz.value)) {
		alert('I campo \'numero di pagina iniziale\' deve essere numerico!');
		return false;
	}
	if(Math.ceil(FormRef.da.value)>Math.ceil(FormRef.a.value)) {
		alert('Il campo \'a\' deve essere maggiore del campo \'da\'!');
		return false;
	}

	url = "__scripts/FGE_DataGridEdit.php?table=user_movimenti_fiscalizzati&FGE_action=print&Registro&Fanghi&da=" + FormRef.da.value + "&a=" + FormRef.a.value;
	url += "&PaginaIniz=" + FormRef.paginaIniz.value; 
	url += "&Fiscale=" + Fiscale;
	self.location = url; //}}}
}

function RegistroIndustriale1(FormRef) {

	date=FormRef.week_cronologico.value.split("|");
	da  = date[0];
	a	= date[1];

	author=document.getElementById('author').value;

	url = "__scripts/FGE_DataGridEdit.php?table=user_movimenti&FGE_action=print&Registro&Industriale&Cronologico&da=" + da + "&a=" + a + "&author=" + author;
	self.location = url;
	}

function RegistroIndustriale2(FormRef) {

	date=FormRef.week_settimanale.value.split("|");
	da  = date[0];
	a	= date[1];

	author=document.getElementById('author').value;

	url = "__scripts/FGE_DataGridEdit.php?table=user_movimenti&FGE_action=print&Registro&Industriale&Settimanale&da=" + da + "&a=" + a + "&author=" + author;
	self.location = url;
	}


function RegistroPrintShowInterface() {
//{{{ 
	if(window.pageYOffset) {
		NewPos = window.pageYOffset;
		NewPos = (NewPos+100)+ "px";
		document.getElementById('SOGER_RegistroInterface').style.zIndex =  1;
		document.getElementById('SOGER_RegistroInterface').style.display =  "block";
		document.getElementById('SOGER_RegistroInterface').style.top = NewPos;
	} else {
		window.scroll(10,10);
		document.getElementById('SOGER_RegistroInterface').style.zIndex =  1;
		document.getElementById('SOGER_RegistroInterface').style.display =  "block";
	}
}

function RegistroFanghiPrintShowInterface() {
//{{{ 
	if(window.pageYOffset) {
		NewPos = window.pageYOffset;
		NewPos = (NewPos+100)+ "px";
		document.getElementById('SOGER_RegistroFanghiInterface').style.zIndex =  1;
		document.getElementById('SOGER_RegistroFanghiInterface').style.display =  "block";
		document.getElementById('SOGER_RegistroFanghiInterface').style.top = NewPos;
	} else {
		window.scroll(10,10);
		document.getElementById('SOGER_RegistroFanghiInterface').style.zIndex =  1;
		document.getElementById('SOGER_RegistroFanghiInterface').style.display =  "block";
	}
}

function RegistroPrintGo() {
	/*
	if (document.RegistroInterface.FISCALE[0].checked) {
		var Fiscale = 0;
	} else {
		var Fiscale = 1;
	}
	document.getElementById('SOGER_RegistroInterface').style.display =  "none";
	document.getElementById('SOGER_RegistroInterface').style.zIndex =  -1;
	//self.location = document.getElementById('RegistroInterface').action;
	Registro(StampaRegistro,Fiscale);
	*/
	var StampaRegistro=document.getElementById("StampaRegistro");
	Registro(StampaRegistro,1);
}

function RegistroUnicoPDPrintGo() {
	var StampaRegistroUnicoPD=document.getElementById("StampaRegistroUnicoPD");
	RegistroUnicoPD(StampaRegistroUnicoPD,0);
}

function RegistroFanghiPrintGo() {
	/*
	if (document.RegistroFanghiInterface.FISCALE[0].checked) {
		var Fiscale = 0;
	} else {
		var Fiscale = 1;
	}
	document.getElementById('SOGER_RegistroFanghiInterface').style.display =  "none";
	document.getElementById('SOGER_RegistroFanghiInterface').style.zIndex =  -1;
	//self.location = document.getElementById('RegistroFanghiInterface').action; //}}}
	RegistroFanghi(StampaRegistroFanghi,Fiscale);
	*/
	RegistroFanghi(StampaRegistroFanghi,1);
}


function RegistroPrintAbort() {
	document.getElementById('SOGER_RegistroInterface').style.display =  "none";
	document.getElementById('SOGER_RegistroInterface').style.zIndex =  -1;	
}
function RegistroFanghiPrintAbort() {
	document.getElementById('SOGER_RegistroFanghiInterface').style.display =  "none";
	document.getElementById('SOGER_RegistroFanghiInterface').style.zIndex =  -1;	
}

/*
STAMPA NUMERAZIONE REGISTRO PER VIDIMAZIONE
*/
function RegistroVidimazione(FormRef) {
//{{{ 
	if(isNaN(FormRef.paginaTot.value)) {
		alert('I campo \'numero di pagine totali\' deve essere numerico!');
		return false;
	}
	if(isNaN(FormRef.paginaIniz.value)) {
		alert('I campo \'numero di pagina iniziale\' deve essere numerico!');
		return false;
	}
	if(isNaN(FormRef.AnnoRegistro.value)) {
		alert('I campo \'anno di riferimento\' deve essere numerico!');
		return false;
	}
	url = "__scripts/FGE_DataGridEdit.php?table=numerazione_vidimazione&FGE_action=print&PaginaTotale=" + FormRef.paginaTot.value + "&PaginaIniz=" + FormRef.paginaIniz.value + "&AnnoRegistro=" + FormRef.AnnoRegistro.value + "&CodiceRegistro=" + FormRef.CodiceRegistro.value;
	if(FormRef.RagSoc.checked)
		url+="&RagSoc";
	self.location = url; //}}}
}


/*
MOVIMENTI - SELEZIONE SOGGETTI - RIFIUTO 
*/
/*
function CUSTOM_UNIQUE_FIR(NotUsed,FieldRef,FormRef) {

	FGE_LockForm = true;
	
	TableName = FieldRef.substring(0, FieldRef.indexOf(':'));
	if(TableName=='user_movimenti') 
		pri = "ID_MOV";
	else
		pri = "ID_MOV_F";
	
	var LKUP = new ajaxObject('__mov_snipplets/FRM_CHECKFIR.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		ResArray = responseTxt.split("|");
		if(ResArray[0]==1){
			FGE_LockForm = false;
			// via liscio
			MOV_DISPO_CHECK(document.getElementById(TableName+':FKEdisponibilita').value, FieldRef, FormRef );
			}
		else{
			window.alert(ResArray[1]);
			FGE_LockForm = true;
			return;
			}
		}

	Parameters = 'TableName=' + TableName + '&pri=' + pri + '&NMOV=' + document.getElementById(TableName+':NMOV').value + '&filter=' + document.getElementById(TableName+':'+pri).value + '&NFORM=' + document.getElementById(TableName+':NFORM').value + '&DTFORM=' + document.getElementById(TableName+':DTFORM').value;
	LKUP.update(Parameters);
	}
*/

function testForObject(Id, Tag){
  var o = document.getElementById(Id);
  if (o)  {
    if (Tag)    {
      if (o.tagName.toLowerCase() == Tag.toLowerCase())      {
        return o;
      }
    }
    else    {
      return o;
    }
  }
  return null;
}




function CUSTOM_RIFMOV(NotUsed,FieldRef,FormRef, Quantita) {
	if(typeof(Quantita)==='undefined') Quantita = 0;
	//alert(dump(FormRef));
	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	

	// reset sensitive fields to avoid unproper record save
	objONU=document.getElementById(TableName+":ID_ONU");
	document.FGEForm_1[TableName+":FKEdisponibilita"].value = 0;
	if((document.FGEForm_1[TableName+":TIPO"].value=="S" && objONU.type!="hidden") | (document.FGEForm_1[TableName+":TIPO"].value=="C" && document.FGEForm_1[TableName+":produttore"].value==0) ) {

                // produttore - azienda
                document.FGEForm_1[TableName+":ID_AZP"].options.length = 0;
		var opt = document.createElement('option');
			opt.value = 'garbage';
			opt.innerHTML = '';
		document.FGEForm_1[TableName+":ID_AZP"].appendChild(opt);
		// produttore - impianto
		document.FGEForm_1[TableName+":ID_UIMP"].options.length = 0;
		var opt = document.createElement('option');
			opt.value = 'garbage';
			opt.innerHTML = '';
		document.FGEForm_1[TableName+":ID_UIMP"].appendChild(opt);
		
		// trasportatore - azienda
		document.FGEForm_1[TableName+":ID_AZT"].options.length = 0;
		var opt = document.createElement('option');
			opt.value = 'garbage';
			opt.innerHTML = '';
		document.FGEForm_1[TableName+":ID_AZT"].appendChild(opt);
		// trasportatore - impianto
		document.FGEForm_1[TableName+":ID_UIMT"].options.length = 0;
		var opt = document.createElement('option');
			opt.value = 'garbage';
			opt.innerHTML = '';
		document.FGEForm_1[TableName+":ID_UIMT"].appendChild(opt);
		// trasportatore - autorizzazione
		document.FGEForm_1[TableName+":ID_AUTHT"].options.length = 0;
		var opt = document.createElement('option');
			opt.value = 'garbage';
			opt.innerHTML = '';
		document.FGEForm_1[TableName+":ID_AUTHT"].appendChild(opt);
		// trasportatore - varchar fields
		document.FGEForm_1[TableName+":FKEcfiscT"].value = '';
		document.FGEForm_1[TableName+":FKErilascioT"].value = '';
		document.FGEForm_1[TableName+":FKEscadenzaT"].value = '';
		document.FGEForm_1[TableName+":NumAlboAutotrasp"].value = '';
		document.FGEForm_1[TableName+":NumAlboAutotraspProprio"].value = '';
		
		// destinatario - azienda
		document.FGEForm_1[TableName+":ID_AZD"].options.length = 0;
		var opt = document.createElement('option');
			opt.value = 'garbage';
			opt.innerHTML = '';
		document.FGEForm_1[TableName+":ID_AZD"].appendChild(opt);
		// destinatario - impianto
		document.FGEForm_1[TableName+":ID_UIMD"].options.length = 0;
		var opt = document.createElement('option');
			opt.value = 'garbage';
			opt.innerHTML = '';
		document.FGEForm_1[TableName+":ID_UIMD"].appendChild(opt);
		// destinatario - autorizzazione
		document.FGEForm_1[TableName+":ID_AUTHD"].options.length = 0;
		var opt = document.createElement('option');
			opt.value = 'garbage';
			opt.innerHTML = '';
		document.FGEForm_1[TableName+":ID_AUTHD"].appendChild(opt);
		// destinatario - varchar fields
		document.FGEForm_1[TableName+":FKEcfiscD"].value = '';
		document.FGEForm_1[TableName+":ID_OP_RS"].value = 'garbage';
		document.FGEForm_1[TableName+":FKErilascioD"].value = '';
		document.FGEForm_1[TableName+":FKEscadenzaD"].value = '';
                
                // intermediario - azienda
		document.FGEForm_1[TableName+":ID_AZI"].options.length = 0;
		var opt = document.createElement('option');
			opt.value = 'garbage';
			opt.innerHTML = '';
		document.FGEForm_1[TableName+":ID_AZI"].appendChild(opt);
		// intermediario - impianto
		document.FGEForm_1[TableName+":ID_UIMI"].options.length = 0;
		var opt = document.createElement('option');
			opt.value = 'garbage';
			opt.innerHTML = '';
		document.FGEForm_1[TableName+":ID_UIMI"].appendChild(opt);
		// intermediario - autorizzazione
		document.FGEForm_1[TableName+":ID_AUTHI"].options.length = 0;
		var opt = document.createElement('option');
			opt.value = 'garbage';
			opt.innerHTML = '';
		document.FGEForm_1[TableName+":ID_AUTHI"].appendChild(opt);
		// intermediario - varchar fields
		document.FGEForm_1[TableName+":FKEcfiscI"].value = '';
		document.FGEForm_1[TableName+":FKErilascioI"].value = '';
		document.FGEForm_1[TableName+":FKEscadenzaI"].value = '';
		}


//{{{
	var LKUP = new ajaxObject('__scripts/MovimentiFakeRifData.php',FGE_DummyAjaxCallback);	
	LKUP.callback = function (responseTxt, responseStat) {
		CUSTOM_ZEROQ(TableName);
		//alert(dump(responseTxt));
		ResArray = responseTxt.split("|");

		AdrAndPericoloso=ResArray[1].split("&&");
		
		Adr=AdrAndPericoloso[0];
		ID_ONU=AdrAndPericoloso[1]; // -> se vuoto = stringa NULLO
		if(ID_ONU=='NULLO') ID_ONU='';
		Pericoloso=AdrAndPericoloso[2];
		CAR_NumDocumento=AdrAndPericoloso[3]; // -> se vuoto = stringa NULLO
		if(CAR_NumDocumento=='NULLO') CAR_NumDocumento='';
		CAR_Laboratorio=AdrAndPericoloso[4]; // -> se vuoto = stringa NULLO
		if(CAR_Laboratorio=='NULLO') CAR_Laboratorio='';
		CAR_DataAnalisi=AdrAndPericoloso[5]; // -> se vuoto = stringa NULLO
		//if(isset(CAR_DataAnalisi) && CAR_DataAnalisi!=''){
		if(typeof CAR_DataAnalisi=='string' && CAR_DataAnalisi!='NULLO'){
			data=CAR_DataAnalisi.split("-");
			CAR_DataAnalisi=data[2]+"/"+data[1]+"/"+data[0];
			}
		else CAR_DataAnalisi='';
		prescrizioni_mov=AdrAndPericoloso[6];		// -> se vuoto = stringa NULLO
		lotto=AdrAndPericoloso[7];					// -> se vuoto = stringa NULLO
		if(prescrizioni_mov=='NULLO') prescrizioni_mov='';
		if(typeof(lotto) == 'undefined' || lotto=='NULLO') lotto='';

		UmAndFanghi			= ResArray[2].split("&&");
		Um					= UmAndFanghi[0];
		Fanghi				= UmAndFanghi[1];

		SFandOriginalID_RIF	= ResArray[0].split("&&");
		SF					= SFandOriginalID_RIF[0];
		originalID_RIF		= SFandOriginalID_RIF[1];
		ClassificazioneHP	= SFandOriginalID_RIF[2];

		document.FGEForm_1[TableName+":FKESF"].value = SF;							// stato fisico
		document.FGEForm_1[TableName+":originalID_RIF"].value = originalID_RIF;		// originalID_RIF
		// hidden field for validation only
		document.getElementById('HiddenField_ClassificazioneHP').value = ClassificazioneHP;		// ClassificazioneHP
		document.FGEForm_1[TableName+":FKEumis"].value = Um;						// unita di misura
		document.FGEForm_1[TableName+":lotto"].value = lotto;						// numero di lotto

		var useFanghi=testForObject("FGED_"+TableName+":FANGHI");					// reg. fanghi
		if(useFanghi){
			if(Fanghi=="1"){																
				document.FGEForm_1[TableName+":FANGHI"][0].checked=true;
				document.FGEForm_1[TableName+":FANGHI"][1].checked=false;
				document.getElementById("SP_"+TableName+":FANGHI").style.background = "yellow";
				}
			else{
				document.FGEForm_1[TableName+":FANGHI"][0].checked=false;
				document.FGEForm_1[TableName+":FANGHI"][1].checked=true;
				document.getElementById("SP_"+TableName+":FANGHI").style.background = "white";
				}
			}

		// NUMERO ONU
		// prima MELFI voleva che in automatico si selezionasse il numero onu inserito in scheda rif. con possibilit� di editarlo.. ora vuole tendina bianca!
//		if(document.getElementById(TableName+":TIPO").value=='S'){
		if(document.getElementById(TableName+":TIPO").value=="S" | document.FGEForm_1[TableName+":produttore"].value==0 ) {
			//objONU=document.getElementById(TableName+":ID_ONU");
			if(objONU.type!="hidden"){
				var A= document.getElementById(TableName+":ID_ONU").options, L= A.length; 
					while(L){ 
						if (A[--L].value==ID_ONU){ 
							document.getElementById(TableName+":ID_ONU").selectedIndex= L; 
							L= 0; 
						} 
					}
				}
			else
				objONU.value=ID_ONU;
			if(document.getElementById(TableName+":TIPO").value=="S" && document.FGEForm_1[TableName+":produttore"].value==1 ) {
				document.FGEForm_1[TableName+":CAR_NumDocumento"].value	= CAR_NumDocumento;
				document.FGEForm_1[TableName+":CAR_Laboratorio"].value	= CAR_Laboratorio;
				document.FGEForm_1[TableName+":CAR_DataAnalisi"].value	= CAR_DataAnalisi;
				document.FGEForm_1[TableName+":prescrizioni_mov"].value	= prescrizioni_mov;
				}
			}
	
		document.FGEForm_1[TableName+":FKEpesospecifico"].value = ResArray[3];			// peso specifico
		//window.alert(TableName);
		objONU=document.getElementById(TableName+":ID_ONU");
		// nello scarico quando sono gestore non c'� il produttore
		if(document.FGEForm_1[TableName+":TIPO"].value=="C" | objONU.type!="hidden"){
			document.FGEForm_1[TableName+":FKEcfiscP"].value = ResArray[4];					// codice fiscale Produttore
			FGE_AjaxRefresh('FGED_'+TableName+':ID_AZP');									// aggiorna azienda Produttore
			FGE_AjaxRefresh('FGED_'+TableName+':ID_UIMP');									// aggiorna autorizzazione Produttore
			}
		else{
			FGE_AjaxRefresh('FGED_'+TableName+':ID_AZD');									// aggiorna azienda Destinatario
			// destinatario
			if(ResArray[6]!=""){
				RilascioAndTrattamento=ResArray[6].split("&&");
				Rilascio    = RilascioAndTrattamento[0];
				Trattamento = RilascioAndTrattamento[1];
				}
			else{
				Rilascio	= "";
				Trattamento	= 0;
				}
			document.FGEForm_1[TableName+":FKEcfiscD"].value = ResArray[5];					// codice fiscale destinatario
			FGE_AjaxRefresh('FGED_'+TableName+':ID_UIMD');									// aggiorna impianto Destinatario
			FGE_AjaxRefresh('FGED_'+TableName+':ID_AUTHD');									// aggiorna autorizzazione Destinatario
			FGE_AjaxRefresh('FGED_'+TableName+':ID_OP_RS');									// aggiorna op Rec/Smalt Destinatario
			ORIGINEDATI_TRANSFRONTALIERO=ResArray[9].split("&&");
			ORIGINEDATI = ORIGINEDATI_TRANSFRONTALIERO[0];
			TRANSFRONTALIERO = ORIGINEDATI_TRANSFRONTALIERO[1];
			// transfrontaliero
			if(TRANSFRONTALIERO=="0"){
				document.FGEForm_1[TableName+":Transfrontaliero"][0].checked=true;
				document.FGEForm_1[TableName+":Transfrontaliero"][1].checked=false;
				}
			else{
				document.FGEForm_1[TableName+":Transfrontaliero"][0].checked=false;
				document.FGEForm_1[TableName+":Transfrontaliero"][1].checked=true;
				}

			CUSTOM_ROSETTE('rosetteD',ORIGINEDATI);											// coccarda autorizzazione Destinatario
			document.FGEForm_1[TableName+":FKErilascioD"].value = Rilascio;					// autorizzazione (rilascio) Destinatario
			CUSTOM_SCADENZA_AUT(ResArray[7],"destinatario",ResArray[8],TableName);			// autorizzazione (scadenza) Destinatario
			document.FGEForm_1[TableName+":ID_TRAT"].value = Trattamento;					// aggiorna tipo di trattamento
			}

		// aggiorno solo se sono in uno scarico o in un carico come soggetto != produttore
		if((document.FGEForm_1[TableName+":TIPO"].value=="S" && objONU.type!="hidden") | (document.FGEForm_1[TableName+":TIPO"].value=="C" && document.FGEForm_1[TableName+":produttore"].value==0) ) {
			FGE_AjaxRefresh('FGED_'+TableName+':ID_AZT');								// aggiorna azienda Trasportatore
			FGE_AjaxRefresh('FGED_'+TableName+':ID_AZD');								// aggiorna azienda Destinatario
			FGE_AjaxRefresh('FGED_'+TableName+':ID_AZI');								// aggiorna azienda Intermediario
			document.FGEForm_1[TableName+":ID_AZI"].options[0].selected=true;
			}

		CUSTOM_IMAGE_RIF(Pericoloso,Adr);

		if(document.FGEForm_1[TableName+":TIPO"].value=="S" | (document.FGEForm_1[TableName+":TIPO"].value=="C" && document.FGEForm_1[TableName+":produttore"].value==0) ) {
			
			if(objONU.type!="hidden"){

				// se cambio rifiuto QL torna "no"
				document.FGEForm_1[TableName+":QL"][0].checked=false;
				document.FGEForm_1[TableName+":QL"][1].checked=true;

				if(Adr=="1"){														//adr/rid
					// sblocco tendina esenzione per quantit� limitata e numero onu
					document.FGEForm_1[TableName+":QL"][0].disabled		= false;
					document.FGEForm_1[TableName+":QL"][1].disabled		= false;
					document.FGEForm_1[TableName+":ID_ONU"].disabled	= false;

					document.FGEForm_1[TableName+":adr"][0].checked		= true;
					document.FGEForm_1[TableName+":adr"][1].checked		= false;
					document.getElementById("SP_"+TableName+":adr").style.background = "yellow";
					}
				else{
					// blocco tendina esenzione per quantit� limitata e numero onu
					document.FGEForm_1[TableName+":QL"][0].disabled		= true;
					document.FGEForm_1[TableName+":QL"][1].disabled		= true;
					document.FGEForm_1[TableName+":ID_ONU"].disabled	= true;
					document.FGEForm_1[TableName+":ID_ONU"].options[0].selected=true;

					document.FGEForm_1[TableName+":adr"][0].checked		= false;
					document.FGEForm_1[TableName+":adr"][1].checked		= true;
					document.getElementById("SP_"+TableName+":adr").style.background = "white";
					}

				if(ResArray[6]!=""){
					RilascioAndTrattamento=ResArray[6].split("&&");
					Rilascio    = RilascioAndTrattamento[0];
					Trattamento = RilascioAndTrattamento[1];
					}
				else{
					Rilascio	= "";
					Trattamento	= 0;
					}

				CF_PROPRIO_TERZI= ResArray[10].split("&&");
				CodiceFiscaleT  = CF_PROPRIO_TERZI[0];
				ContoProprio	= CF_PROPRIO_TERZI[1];
				ContoTerzi		= CF_PROPRIO_TERZI[2];

				// trasportatore
				document.FGEForm_1[TableName+":FKEcfiscT"].value = CodiceFiscaleT;				// codice fiscale Trasportatore
				document.FGEForm_1[TableName+":NumAlboAutotraspProprio"].value = ContoProprio;	// aut conto proprio Trasportatore
				document.FGEForm_1[TableName+":NumAlboAutotrasp"].value = ContoTerzi;			// aut conto terzi Trasportatore
				document.FGEForm_1[TableName+":FKErilascioT"].value = ResArray[11];				// autorizzazione (rilascio) Trasportatore
				CUSTOM_ROSETTE('rosetteT',ResArray[14]);										// coccarda autorizzazione Trasportatore
				CUSTOM_SCADENZA_AUT(ResArray[12],"trasportatore",ResArray[13],TableName);		// autorizzazione (scadenza) Trasportatore
				FGE_AjaxRefresh('FGED_'+TableName+':ID_UIMT');	
				// aggiorna impianto Trasportatore
				FGE_AjaxRefresh('FGED_'+TableName+':ID_AUTHT');									// aggiorna autorizzazione Trasportatore

				// destinatario
				document.FGEForm_1[TableName+":FKEcfiscD"].value = ResArray[5];					// codice fiscale destinatario
				FGE_AjaxRefresh('FGED_'+TableName+':ID_UIMD');									// aggiorna impianto Destinatario
				FGE_AjaxRefresh('FGED_'+TableName+':ID_AUTHD');	
                                // aggiorna autorizzazione Destinatario
				FGE_AjaxRefresh('FGED_'+TableName+':ID_OP_RS');									// aggiorna op Rec/Smalt Destinatario
				ORIGINEDATI_TRANSFRONTALIERO=ResArray[9].split("&&");
				ORIGINEDATI = ORIGINEDATI_TRANSFRONTALIERO[0];
				TRANSFRONTALIERO = ORIGINEDATI_TRANSFRONTALIERO[1];
				// transfrontaliero
				if(TRANSFRONTALIERO=="0"){
					document.FGEForm_1[TableName+":Transfrontaliero"][0].checked=true;
					document.FGEForm_1[TableName+":Transfrontaliero"][1].checked=false;
					}
				else{
					document.FGEForm_1[TableName+":Transfrontaliero"][0].checked=false;
					document.FGEForm_1[TableName+":Transfrontaliero"][1].checked=true;
					}

				CUSTOM_ROSETTE('rosetteD',ORIGINEDATI);											// coccarda autorizzazione Destinatario
				document.FGEForm_1[TableName+":FKErilascioD"].value = Rilascio;					// autorizzazione (rilascio) Destinatario
				CUSTOM_SCADENZA_AUT(ResArray[7],"destinatario",ResArray[8],TableName);			// autorizzazione (scadenza) Destinatario
				document.FGEForm_1[TableName+":ID_TRAT"].value = Trattamento;					// aggiorna tipo di trattamento

				// intermediario
				if(document.FGEForm_1[TableName+":ID_AZI"].options[document.FGEForm_1[TableName+":ID_AZI"].options.selectedIndex].value=='garbage'){
					document.FGEForm_1[TableName+":FKEcfiscI"].value = '';							// codice fiscale Intermediario
					//FGE_AjaxRefresh('FGED_'+TableName+':ID_UIMI');									// aggiorna impianto Intermediario
					document.FGEForm_1[TableName+":ID_UIMI"].options[0].selected=true;
					//FGE_AjaxRefresh('FGED_'+TableName+':ID_AUTHI');									// aggiorna autorizzazione Intermediario
					document.FGEForm_1[TableName+":ID_AUTHI"].options[0].selected=true;
					CUSTOM_ROSETTE('rosetteI',0);													// coccarda autorizzazione Intermediario
					document.FGEForm_1[TableName+":FKErilascioI"].value = '';						// autorizzazione (rilascio) Intermediario
					document.FGEForm_1[TableName+":FKEscadenzaI"].value = '';						// autorizzazione (scadenza) Intermediario
					}
			
				
				// rifiuto
				document.FGEForm_1[TableName+":FKEdisponibilita"].value = ResArray[15];
				document.FGEForm_1[TableName+":FKEgiacINI"].value = ResArray[16];
				document.FGEForm_1[TableName+":FKElastCarico"].value = ResArray[17];

				// 17 - ID dell' ultimo movimento di carico
				// 19 - il rifiuto ha giacenza iniziale automatica, non posso incrementarla manualmente
				if(document.FGEForm_1[TableName+":TIPO"].value=="S"){
					if(ResArray[17]!='0' || ResArray[19]=='1'){
						document.ScaricoInterface["correzione"][1].disabled=true;
						}
					else{
						document.ScaricoInterface["correzione"][1].disabled=false;
						}
					}

				if(document.FGEForm_1[TableName+":TIPO"].value=="S" && objONU.type!="hidden"){
					if(ResArray[15]==0){
						document.ScaricoInterface["correzione"][0].disabled=true;	
						}
					else{
						document.ScaricoInterface["correzione"][0].disabled=false;	
						}
					// [17] � l' ID dell' ultimo movimento di carico
					// [18] dice se tale carico ha fiscale=1
					if(ResArray[17]==0){
						document.ScaricoInterface["correzione"][2].disabled=true;	
						}
					else{						
						document.ScaricoInterface["correzione"][1].disabled=true;						
						// SISTRI: posso modificare la quantit� se l' ultimo carico non ha fiscale=1
						if(ResArray[18]=='0' || TableName=='user_movimenti_fiscalizzati')
							document.ScaricoInterface["correzione"][2].disabled=false;
						else
							document.ScaricoInterface["correzione"][2].disabled=true;
						}
					}

				}
			else{
				// rifiuto
				document.FGEForm_1[TableName+":FKEdisponibilita"].value = ResArray[15];
				document.FGEForm_1[TableName+":FKEgiacINI"].value = ResArray[16];
				document.FGEForm_1[TableName+":FKElastCarico"].value = ResArray[17];

				//lavoro con maschera ridotta
				document.ScaricoInterface["correzione"][0].disabled=false;
				document.ScaricoInterface["correzione"][1].disabled=true;
				document.ScaricoInterface["correzione"][2].disabled=true;	
				document.ScaricoInterface["correzione"][3].disabled=true;
				}

			}

		else { //movimento di carico
			document.FGEForm_1[TableName+":FKEdisponibilita"].value = ResArray[5];	//disponibilit� del rifiuto
			
			// sono produttore
			if(document.FGEForm_1[TableName+":produttore"].value==1){
				
				// non c'� rifiuto selezionato, azzero tutto
				if(FieldRef.value=="garbage") {
					document.FGEForm_1[TableName+":FKErilascioT"].value = "";
					document.FGEForm_1[TableName+":FKErilascioD"].value = "";
					document.FGEForm_1[TableName+":FKErilascioI"].value = "";
					document.FGEForm_1[TableName+":FKEcfiscT"].value = "";
					document.FGEForm_1[TableName+":FKEcfiscD"].value = "";				
					document.FGEForm_1[TableName+":FKEcfiscI"].value = "";				
					}

				}
			// sono trasportatore o destinatario
			else{
				if(FieldRef.value=="garbage") {
					FGE_AjaxRefresh('FGED_'+TableName+':ID_UIMD');						// aggiorna impianto Destinatario
					FGE_AjaxRefresh('FGED_'+TableName+':ID_AUTHD');						// aggiorna autorizzazione Destinatario
					FGE_AjaxRefresh('FGED_'+TableName+':ID_UIMT');						// aggiorna impianto Trasportatore
					FGE_AjaxRefresh('FGED_'+TableName+':ID_AUTHT');						// aggiorna autorizzazione Trasportatore
					FGE_AjaxRefresh('FGED_'+TableName+':ID_UIMI');						// aggiorna impianto Intermediari
					FGE_AjaxRefresh('FGED_'+TableName+':ID_AUTHI');						// aggiorna autorizzazione Intermediari
					document.FGEForm_1[TableName+":FKErilascioT"].value = "";
					document.FGEForm_1[TableName+":FKErilascioD"].value = "";
					document.FGEForm_1[TableName+":FKErilascioI"].value = "";
					document.FGEForm_1[TableName+":FKEcfiscT"].value = "";
					document.FGEForm_1[TableName+":FKEcfiscD"].value = "";				
					document.FGEForm_1[TableName+":FKEcfiscI"].value = "";				
				}
			}
			
			if(Quantita>0){ // ---> Carichi creati da avviso in home "caricare x kg del rifiuto ---"
				document.getElementById(TableName+":quantita").value = Quantita;
				CUSTOM_QNT('', document.getElementById(TableName+":quantita"), '');
				}

			}
		}
	
	Parameters = 'IDrif=' + FieldRef.value + '&Tipo=' + document.FGEForm_1[TableName+":TIPO"].value + '&TableName=' + TableName;
	
	// SE STO FACENDO SCHEDA SISTRI, NON HO DTMOV.. DTMOV E' DATA CORRENTE
	if(document.FGEForm_1[TableName+":DTMOV"]===undefined){
		var today	= new Date();
		var d		= today.getDate();
		var m		= today.getMonth()+1;
		var y		= today.getFullYear();
		var DTMOV	= d+'/'+m+'/'+y;
		}
	// STO FACENDO SCARICO + FIR
	else var DTMOV = document.FGEForm_1[TableName+":DTMOV"].value;
	Parameters+= '&DTMOV=' + DTMOV;
	// for current movement exclusion in dispo calculatio
	Parameters+= '&NMOV=' + document.FGEForm_1[TableName+":NMOV"].value;

	LKUP.update(Parameters);	 //}}}
}



/*
MOVIMENTI - SELEZIONE SOGGETTI - AZIENDE
*/
function CUSTOM_PRODMOV(NotUsed,FieldRef,FormRef) {	// ok
	
	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	
	// selezione produttore
	var LKUP = new ajaxObject('__mov_snipplets/CMB_Aziende.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		ResArray = responseTxt.split("|");
		document.FGEForm_1[TableName+":FKEcfiscP"].value = ResArray[0];
		FGE_AjaxRefresh('FGED_'+TableName+':ID_UIMP');	
	}
	Parameters = 'IDazienda=' + FieldRef.value + "&AppyTo=produttori"  + '&Tipo=' + document.FGEForm_1[TableName+":TIPO"].value;
	Parameters+= '&TableName=' + TableName;
	LKUP.update(Parameters);
}
function CUSTOM_DESTMOV(NotUsed,FieldRef,FormRef) {	// ok

	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	

	// selezione destinatario
//{{{ 
	var LKUP = new ajaxObject('__mov_snipplets/CMB_Aziende.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		ResArray = responseTxt.split("|");
               
		RilascioAndTrattamento=ResArray[1].split("&&");
		Rilascio    = RilascioAndTrattamento[0];
		Trattamento = RilascioAndTrattamento[1];

		document.FGEForm_1[TableName+":FKEcfiscD"].value = ResArray[0];
		FGE_AjaxRefresh('FGED_'+TableName+':ID_UIMD');
		FGE_AjaxRefresh('FGED_'+TableName+':ID_AUTHD');
		document.FGEForm_1[TableName+":FKErilascioD"].value = Rilascio;		// autorizzazione (rilascio) Destinatario
		document.FGEForm_1[TableName+":ID_TRAT"].value = Trattamento;
		CUSTOM_SCADENZA_AUT(ResArray[2],"destinatario",ResArray[3],TableName);
		CUSTOM_ROSETTE('rosetteD',ResArray[4]);
                if(document.FGEForm_1[TableName+":TIPO"].value=="S" || document.FGEForm_1["user_movimenti_fiscalizzati:produttore"].value==0) {
			FGE_AjaxRefresh('FGED_'+TableName+':ID_OP_RS');
                        } 
		// transfrontaliero
		if(ResArray[5]=="0"){
			document.FGEForm_1[TableName+":Transfrontaliero"][0].checked=true;
			document.FGEForm_1[TableName+":Transfrontaliero"][1].checked=false;
			}
		else{
			document.FGEForm_1[TableName+":Transfrontaliero"][0].checked=false;
			document.FGEForm_1[TableName+":Transfrontaliero"][1].checked=true;
			}

		}
	Parameters = 'IDazienda=' + FieldRef.value + "&AppyTo=destinatari&ID_RIF=" + document.FGEForm_1[TableName+":ID_RIF"].value  + '&Tipo=' + document.FGEForm_1[TableName+":TIPO"].value;
	Parameters+= '&TableName=' + TableName;
	LKUP.update(Parameters);	 //}}}
}
function CUSTOM_INTMOV(NotUsed,FieldRef,FormRef) {	// ok

	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	

	// selezione intermediario
//{{{ 
	var LKUP = new ajaxObject('__mov_snipplets/CMB_Aziende.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		ResArray = responseTxt.split("|");
		document.FGEForm_1[TableName+":FKEcfiscI"].value = ResArray[0];
		FGE_AjaxRefresh('FGED_'+TableName+':ID_UIMI');
		FGE_AjaxRefresh('FGED_'+TableName+':ID_AUTHI');
		document.FGEForm_1[TableName+":FKErilascioI"].value = ResArray[1];		// autorizzazione (rilascio) intermediario
		CUSTOM_SCADENZA_AUT(ResArray[2],"intermediario",ResArray[3],TableName);
		CUSTOM_ROSETTE('rosetteI',ResArray[4]);
		}
	Parameters = 'IDazienda=' + FieldRef.value + "&AppyTo=intermediari&ID_RIF=" + document.FGEForm_1[TableName+":ID_RIF"].value  + '&Tipo=' + document.FGEForm_1[TableName+":TIPO"].value;
	Parameters+= '&TableName=' + TableName;
	LKUP.update(Parameters);	 //}}}
}
function CUSTOM_TRASPMOV(NotUsed,FieldRef,FormRef) {	// ok

	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	

	// selezione trasportatore
//{{{ 
	var LKUP = new ajaxObject('__mov_snipplets/CMB_Aziende.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		ResArray = responseTxt.split("|");

		CF_Proprio_Terzi=ResArray[0].split("&&");
		CodiceFiscaleT	= CF_Proprio_Terzi[0];
		ContoProprio	= CF_Proprio_Terzi[1];
		ContoTerzi		= CF_Proprio_Terzi[2];

		document.FGEForm_1[TableName+":FKEcfiscT"].value = CodiceFiscaleT;
		document.FGEForm_1[TableName+":NumAlboAutotraspProprio"].value = ContoProprio;					// aut conto proprio Trasportatore
		document.FGEForm_1[TableName+":NumAlboAutotrasp"].value = ContoTerzi;						// aut conto terzi Trasportatore
		document.FGEForm_1[TableName+":FKErilascioT"].value = ResArray[1];
		CUSTOM_SCADENZA_AUT(ResArray[2],"trasportatore",ResArray[3],TableName);
		CUSTOM_ROSETTE('rosetteT',ResArray[4]);
		FGE_AjaxRefresh('FGED_'+TableName+':ID_UIMT');
		FGE_AjaxRefresh('FGED_'+TableName+':ID_AUTHT');			
	}
	Parameters = 'IDazienda=' + FieldRef.value + "&AppyTo=trasportatori&ID_RIF=" + document.FGEForm_1[TableName+":ID_RIF"].value  + '&Tipo=' + document.FGEForm_1[TableName+":TIPO"].value;
	Parameters+= '&TableName=' + TableName;
	LKUP.update(Parameters);	 //}}}
}

function CUSTOM_DST_IMP_MOV(NotUsed,FieldRef,FormRef) {	// ok

	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	

	var LKUP = new ajaxObject('__mov_snipplets/CMB_Impianti.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		ResArray = responseTxt.split("|");		

		RilascioAndTrattamento=ResArray[0].split("&&");
		Rilascio    = RilascioAndTrattamento[0];
		Trattamento = RilascioAndTrattamento[1];

		document.FGEForm_1[TableName+":FKErilascioD"].value = Rilascio;
		document.FGEForm_1[TableName+":ID_TRAT"].value = Trattamento;
		FGE_AjaxRefresh('FGED_'+TableName+':ID_AUTHD');
		CUSTOM_SCADENZA_AUT(ResArray[1],"destinatario",ResArray[2],TableName);
		CUSTOM_ROSETTE('rosetteD',ResArray[3]);
		if(document.FGEForm_1[TableName+":TIPO"].value=="S" || document.FGEForm_1["user_movimenti_fiscalizzati:produttore"].value==0) {
			FGE_AjaxRefresh('FGED_'+TableName+':ID_OP_RS');
			}

		// transfrontaliero
		if(ResArray[4]=="0"){
			document.FGEForm_1[TableName+":Transfrontaliero"][0].checked=true;
			document.FGEForm_1[TableName+":Transfrontaliero"][1].checked=false;
			}
		else{
			document.FGEForm_1[TableName+":Transfrontaliero"][0].checked=false;
			document.FGEForm_1[TableName+":Transfrontaliero"][1].checked=true;
			}
		}
	Parameters = 'IDimpianto=' + FieldRef.value + "&AppyTo=destinatari&ID_RIF=" + document.FGEForm_1[TableName+":ID_RIF"].value + '&Tipo=' + document.FGEForm_1[TableName+":TIPO"].value;
	Parameters+= '&TableName=' + TableName;
	LKUP.update(Parameters);
	}

function CUSTOM_TRS_IMP_MOV(NotUsed,FieldRef,FormRef) {	// ok

	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	

//{{{ 
	var LKUP = new ajaxObject('__mov_snipplets/CMB_Impianti.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		ResArray = responseTxt.split("|");
		document.FGEForm_1[TableName+":FKErilascioT"].value = ResArray[0];
		CUSTOM_SCADENZA_AUT(ResArray[1],"trasportatore",ResArray[2],TableName);
		CUSTOM_ROSETTE('rosetteT',ResArray[3]);
		FGE_AjaxRefresh('FGED_'+TableName+':ID_AUTHT');		
	}
	Parameters = 'IDimpianto=' + FieldRef.value + "&AppyTo=trasportatori&ID_RIF=" + document.FGEForm_1[TableName+":ID_RIF"].value + '&Tipo=' + document.FGEForm_1[TableName+":TIPO"].value;
	Parameters+= '&TableName=' + TableName;
	LKUP.update(Parameters);	 //}}}	
}
function CUSTOM_INT_IMP_MOV(NotUsed,FieldRef,FormRef) {	// ok

	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	

//{{{ 
	var LKUP = new ajaxObject('__mov_snipplets/CMB_Impianti.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		ResArray = responseTxt.split("|");
		document.FGEForm_1[TableName+":FKErilascioI"].value = ResArray[0];
		CUSTOM_SCADENZA_AUT(ResArray[1],"intermediario",ResArray[2],TableName);
		CUSTOM_ROSETTE('rosetteI',ResArray[3]);
		FGE_AjaxRefresh('FGED_'+TableName+':ID_AUTHI');		
	}
	Parameters = 'IDimpianto=' + FieldRef.value + "&AppyTo=intermediari&ID_RIF=" + document.FGEForm_1[TableName+":ID_RIF"].value + '&Tipo=' + document.FGEForm_1[TableName+":TIPO"].value;
	Parameters+= '&TableName=' + TableName;
	LKUP.update(Parameters);
	/*
		if(document.FGEForm_1[TableName+":ID_AZI"].options[document.FGEForm_1[TableName+":ID_AZI"].options.selectedIndex].value!='garbage')
		LKUP.update(Parameters);
	else{
		document.FGEForm_1[TableName+":ID_UIMI"].options[0].selected=true;
		window.alert("Selezionare prima l' intermediario");
		}
	*/
	 //}}}	
}
/*
MOVIMENTI - AUTORIZZAZIONI
*/
function CUSTOM_SCADENZA_AUT(scadenza,soggetto,differenzaGG,TableName) {
	// avvisi date di scadenza autorizzazioni (movimenti)
//{{{ 
	switch(soggetto) {
		case "trasportatore":
		campo = TableName+":FKEscadenzaT";
		SOGER_AutLockTRA = false;
		break;
		case "destinatario":
		SOGER_AutLockDEST = false;
		campo = TableName+":FKEscadenzaD";
		break;
		case "intermediario":
		SOGER_AutLockDEST = false;
		campo = TableName+":FKEscadenzaI";
		break;
	}
	if(scadenza=='0000-00-00') {
		alert("Attenzione:\nLa data di scadenza dell\'autorizzazione del " +  soggetto + " e\' mancante");
		document.FGEForm_1[campo].value = "";
		switch(soggetto) {
			case "trasportatore":
			SOGER_AutLockTRA = true;
			break;
			case "destinatario":
			SOGER_AutLockDEST = true;
			break;
			case "intermediario":
			SOGER_AutLockINT = true;
			break;
		}
	} else {
		if(scadenza!="") {
			if(Math.ceil(differenzaGG)<=0) {
				document.FGEForm_1[campo].value = scadenza;
			} else {
				document.FGEForm_1[campo].value = scadenza;
				alert("Attenzione:\nL\'autorizzazione del " +  soggetto + " e\' scaduta");
				switch(soggetto) {
					case "trasportatore":
					SOGER_AutLockTRA = true;
					break;
					case "destinatario":
					SOGER_AutLockDEST = true;
					break;
					case "intermediario":
					SOGER_AutLockINT = true;
					break;
				}
			}
		} else {
			document.FGEForm_1[campo].value = "";	
		}
	}	 //}}}
}


function SOGER_TraspDestFormLock() {
	if (SOGER_CHECK_TRASPORTATORE==true | SOGER_CHECK_DESTINATARIO==true) {

		var SOGER_TraspLock = false;
		var SOGER_DestLock = false;

		if(document.getElementById("user_movimenti:produttore")){
			var TableName="user_movimenti";
			}
		else{
			var TableName="user_movimenti_fiscalizzati";
			}
		
		if(document.getElementById(TableName+":SenzaTrasporto")) // -> se � hidden ha questo id
			var trasp_needed = false;
		else{
			if(document.FGEForm_1[TableName+":SenzaTrasporto"][1].checked==true) // -> se � visible ha questo name
				var trasp_needed = true;
			else
				var trasp_needed = false;
			}

		if (SOGER_CHECK_TRASPORTATORE==true && trasp_needed==true){
			if(document.title.indexOf("dettaglio")==-1 && document.getElementById(TableName+":produttore").value=='1' && document.getElementById(TableName+":TIPO").value=='S' && document.getElementById(TableName+':ID_AUTHT').value=='garbage' ){
				alert("Attenzione, verifica di aver inserito tutte le informazioni relative al trasportatore.");
				SOGER_TraspLock=true;
				return false;
				}
			}

		if (SOGER_CHECK_DESTINATARIO==true){
			if(document.title.indexOf("dettaglio")==-1 && document.getElementById(TableName+":TIPO").value=='S' && (document.getElementById(TableName+':ID_AUTHD').value=='garbage' | document.getElementById(TableName+':ID_OP_RS').value=='garbage')){
				alert("Attenzione, verifica di aver inserito tutte le informazioni relative al destinatario.");
				SOGER_DestLock=true;
				return false;
				}
			}

		if(!SOGER_TraspLock && !SOGER_DestLock){
			return true;
			}

		}

	else {
		//alert("non ci sono");
		return true;
	}
}

function CUSTOM_IMAGE_RIF(PERICOLOSO,ADR){
	var Counter=0;
	if(PERICOLOSO==1) Counter+=1;
	if(ADR==1) Counter+=2;
	//counter 0 -> non pericoloso
	//counter 1 -> pericoloso, no adr
	//counter 2 -> non pericoloso, adr
	//counter 3 -> pericoloso, adr
	obj=document.getElementById('imageRif').style.backgroundImage = 'url("__css/imageRif'+Counter+'.gif")';
	}
	
function CUSTOM_ROSETTE(ID_FIELD,ID_ORIGINE_DATI){
	switch(ID_ORIGINE_DATI){
		default:
		case "1":
			obj=document.getElementById(ID_FIELD).style.backgroundImage = 'url("__css/rosette_gray.png")';
			break;
		case "2":
			obj=document.getElementById(ID_FIELD).style.backgroundImage = 'url("__css/rosette_red.png")';
			break;
		case "3":
			obj=document.getElementById(ID_FIELD).style.backgroundImage = 'url("__css/rosette_orange.png")';
			break;
		case "4":
			obj=document.getElementById(ID_FIELD).style.backgroundImage = 'url("__css/rosette_green.png")';
			break;
		}
	}

function CUSTOM_TRS_AUT_MOV(NotUsed,FieldRef,FormRef) {	// ok

	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	
	// scrive a video le date di rilascio e scadenza x le autorizzazioni dei trasportatori
//{{{ 
	var LKUP = new ajaxObject('__mov_snipplets/CMB_Autorizzazioni.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		ResArray = responseTxt.split("|");
		document.FGEForm_1[TableName+":FKErilascioT"].value = ResArray[0];
		CUSTOM_ROSETTE('rosetteT',ResArray[3]);
		CUSTOM_SCADENZA_AUT(ResArray[1],"trasportatore",ResArray[2],TableName);
	}
	Parameters = 'IDautorizzazione=' + FieldRef.value + "&AppyTo=trasportatori";
	Parameters+= '&TableName=' + TableName;
	LKUP.update(Parameters);	 //}}}	
}
function CUSTOM_INT_AUT_MOV(NotUsed,FieldRef,FormRef) {	// ok

	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	
	// scrive a video le date di rilascio e scadenza x le autorizzazioni dei trasportatori
//{{{ 
	var LKUP = new ajaxObject('__mov_snipplets/CMB_Autorizzazioni.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		ResArray = responseTxt.split("|");
		document.FGEForm_1[TableName+":FKErilascioI"].value = ResArray[0];
		CUSTOM_ROSETTE('rosetteI',ResArray[3]);
		CUSTOM_SCADENZA_AUT(ResArray[1],"intermediario",ResArray[2],TableName);
	}
	Parameters = 'IDautorizzazione=' + FieldRef.value + "&AppyTo=intermediari";
	Parameters+= '&TableName=' + TableName;
	if(document.FGEForm_1[TableName+":ID_UIMI"].options[document.FGEForm_1[TableName+":ID_UIMI"].options.selectedIndex].value!='garbage')
		LKUP.update(Parameters);
	else{
		document.FGEForm_1[TableName+":ID_AUTHI"].options[0].selected=true;
		window.alert("Selezionare prima l' impianto intermediario");
		}
}
function CUSTOM_DST_AUT_MOV(NotUsed,FieldRef,FormRef) {	// ok
	// scrive a video le date di rilascio e scadenza x le autorizzazioni dei destinatari
//{{{ 
	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	

	var LKUP = new ajaxObject('__mov_snipplets/CMB_Autorizzazioni.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		ResArray = responseTxt.split("|");
					
		RilascioAndTrattamento=ResArray[0].split("&&");
		Rilascio    = RilascioAndTrattamento[0];
		Trattamento = RilascioAndTrattamento[1];

		document.FGEForm_1[TableName+":FKErilascioD"].value = Rilascio;
		document.FGEForm_1[TableName+":ID_TRAT"].value = Trattamento;
		CUSTOM_ROSETTE('rosetteD',ResArray[3]);
		CUSTOM_SCADENZA_AUT(ResArray[1],"destinatario",ResArray[2],TableName);
		FGE_AjaxRefresh('FGED_'+TableName+':ID_OP_RS');		
	}
	Parameters = 'IDautorizzazione=' + FieldRef.value + "&AppyTo=destinatari";
	Parameters+= '&TableName=' + TableName;
	LKUP.update(Parameters);	 //}}}	
}
/*
MOVIMENTI - AUTISTI / RIMORCHI
*/
function CUSTOM_AUT_MOV(NotUsed,FieldRef,FormRef) {

	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	

//{{{ 
	var LKUP = new ajaxObject('__scripts/MovimentiAUTRMK.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		ResArray = responseTxt.split("|");
		document.FGEForm_1[TableName+":FKEmzAUT"].value = ResArray[0];
		document.FGEForm_1[TableName+":FKEadrAUT"].value = ResArray[1];	
		CUSTOM_ROSETTE('rosetteA',ResArray[2]);
		document.tmp_contoterzi_mezzi["automezzoID_LIM"].value = ResArray[3];	
		FGE_AjaxRefresh('FGED_'+TableName+':ID_AUTST');
	}
	Parameters = 'ID=' + FieldRef.value + "&AppyTo=automezzi";
	LKUP.update(Parameters); //}}}
}
function CUSTOM_RMK_MOV(NotUsed,FieldRef,FormRef) {

	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	

//{{{ 
	var LKUP = new ajaxObject('__scripts/MovimentiAUTRMK.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		ResArray = responseTxt.split("|");
		document.FGEForm_1[TableName+":FKEmzRMK"].value = ResArray[0];
		document.FGEForm_1[TableName+":FKEadrRMK"].value = ResArray[1];
		CUSTOM_ROSETTE('rosetteR',ResArray[2]);
		document.tmp_contoterzi_mezzi["rimorchioID_LIM"].value = ResArray[3];	
	}
	Parameters = 'ID=' + FieldRef.value + "&AppyTo=rimorchi";
	LKUP.update(Parameters); //}}}
}

/*
function CUSTOM_CLEAR_AUT() {
	// azzera le autorizzazioni di destinatari e trasportatori
//{{{ 
	document.FGEForm_1["user_movimenti:FKErilascioT"].value = "";
	document.FGEForm_1["user_movimenti:FKEscadenzaT"].value = "";
	document.FGEForm_1["user_movimenti:FKErilascioD"].value = "";
	document.FGEForm_1["user_movimenti:FKEscadenzaD"].value = "";	
//}}}
}

function CUSTOM_CLEAR_CF() {
	// azzera i codici fiscali di destinatari e trasportatori
//{{{ 
	document.FGEForm_1["user_movimenti:FKEcfiscD"].value = "";
	document.FGEForm_1["user_movimenti:FKEcfiscT"].value = ""; //}}}
}
*/

function AddConducenteFast(ImpTraspID) {
//{{{ 
	document.getElementById('ConducenteBOX').style.display = "block";
	var AutistaForm = new ajaxObject('__scripts/AddConducenteFAST.php',FGE_DummyAjaxCallback);
	AutistaForm.callback = function (responseTxt, responseStat) {
		document.getElementById('ConducenteBOX').innerHTML = responseTxt;
	}
	Pars = "ImpID=" + ImpTraspID;
	AutistaForm.update(Pars); //}}}		
}

/*
MOVIMENTI - Verifiche ADR
*/

function AjaxAutista(TableName) {
	
	var LKUP = new ajaxObject('__mov_snipplets/CreaAutista.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		show_hide('autista');
		document.CreaAutista.nome.value="";
		document.CreaAutista.description.value="";
		FGE_AjaxRefresh('FGED_'+TableName+':ID_AUTST');
		}
	Parameters = 'Nome=' + document.CreaAutista.nome.value + "&Cognome=" + document.CreaAutista.description.value  + '&Trasportatore=' + document.CreaAutista.HiddenTrasportatore.value;
	LKUP.update(Parameters);
	}

function AjaxTarghe(TableName, AlboCheck) {
    var LKUP = new ajaxObject('__mov_snipplets/CreaTarghe.php',FGE_DummyAjaxCallback);
    LKUP.callback = function (responseTxt, responseStat) {
        show_hide_targhe('targhe');
        document.CreaTarghe.user_automezzi_description.value="";
        document.CreaTarghe.user_rimorchi_description.value="";
        FGE_AjaxRefresh('FGED_'+TableName+':ID_AUTO');
        var adr="no";
        if(document.CreaTarghe.HiddenTrasportatoreAdr.value==1)
            adr="sì";
        document.FGEForm_1[TableName+":FKEmzAUT"].value = "AUTOCARRO";
        document.FGEForm_1[TableName+":FKEadrAUT"].value = adr;
        FGE_AjaxRefresh('FGED_'+TableName+':ID_RMK');
    }
    var BlackBox = 0;
    var AdrAutomezzo = 0;
    var AdrRimorchio = 0;
    if($("#BlackBox").prop('checked') == true) BlackBox = 1;
    if($("#AdrAutomezzo").prop('checked') == true) AdrAutomezzo = 1;
    if($("#AdrRimorchio").prop('checked') == true) AdrRimorchio = 1;
    var ID_AUTO_TYPE = $("#ID_AUTO_TYPE").val();
    var ID_RMK_TYPE = $("#ID_RMK_TYPE").val();
    Parameters = 'albo=' + AlboCheck + '&automezzo=' + document.CreaTarghe.user_automezzi_description.value + "&ID_AUTO_TYPE=" + ID_AUTO_TYPE + "&rimorchio=" + document.CreaTarghe.user_rimorchi_description.value + "&ID_RMK_TYPE=" + ID_RMK_TYPE + '&autorizzazione=' + document.CreaTarghe.HiddenTrasportatoreAuth.value + '&adr_automezzo=' + AdrAutomezzo + '&adr_rimorchio=' + AdrRimorchio + '&BlackBox=' + BlackBox;
    LKUP.update(Parameters);
}


/*
MOVIMENTI - ANNEX VII
*/

function CheckAnnexVIIAndPrint(){
	var URL = document.URL;
	//document.forms[0]["redirection"].value = 'AllegatoVII.php?table='+TableName+'&amp;filter='+filter+'&amp;pri='+pri;
	document.forms[0]["redirection"].value = URL+'&PrintAnnexVII=1'
	FGEForm_1_SendCheck(document.forms[0]);
	}

function CUSTOM_REFERENTE_COPIA(NotUsed,FieldRef,FormRef){
	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	
	
	var ID_ORG = document.FGEForm_1[TableName+":ID_ORG"].value;
	// 1 - produttore
	// 2 - intermediario

	if(ID_ORG=='1'){
		document.FGEForm_1[TableName+":REFERENTE_GEN"].value=document.FGEForm_1[TableName+":REFERENTE_ORG"].value;
		}

	}

function CUSTOM_REFERENTI_ANNEX_VII(NotUsed,FieldRef,FormRef){

	TableName = FieldRef.name.substring(0, FieldRef.name.indexOf(':'));	

	var ID_ORG = document.FGEForm_1[TableName+":ID_ORG"].value;
	// 1 - produttore
	// 2 - intermediario

	switch(ID_ORG){

		case 'garbage':
			document.FGEForm_1[TableName+":REFERENTE_GEN"].disabled=true;
			document.FGEForm_1[TableName+":REFERENTE_GEN"].value='';
			document.FGEForm_1[TableName+":REFERENTE_ORG"].value='';
			break;
		
		case '1':
			//blocco campo per referente generatore (dati uguali all'organizzatore)
			document.FGEForm_1[TableName+":REFERENTE_GEN"].disabled=true;
			document.FGEForm_1[TableName+":REFERENTE_GEN"].value=document.FGEForm_1[TableName+":REFERENTE_ORG"].value;
			break;

		case '2':
			//sblocco campo per referente generatore (dati diversi dall'organizzatore)
			document.FGEForm_1[TableName+":REFERENTE_GEN"].disabled=false;
			document.FGEForm_1[TableName+":REFERENTE_GEN"].value='';
			break;

		}

	}


/*
CONTENITORI
*/

function SUSPEND_CARICHI_AUTOMATICI(){

	var data = new Date();
	var day, month, year;
	day=data.getDate();
	month=data.getMonth(); month=month+1;
	if(month< 10) month="0"+month;
	year=data.getFullYear();


	// s� - eseguo carichi
	if(document.FGEForm_1["user_schede_rifiuti_deposito:eseguo_carico_automatico"][0].checked && document.getElementById("last_eseguo").value==0){
		document.getElementById("user_schede_rifiuti_deposito:last_carico").value=	day + '/' + month + '/' + year;
		document.getElementById("user_schede_rifiuti_deposito:last_check").value=	day + '/' + month + '/' + year;
		}
	
	// no - carichi sospesi	
	/*
	else{
		window.alert("jj");
		last_carico=document.getElementById('lastc').value.split('-');
		last_carico=last_carico[2]+"/"+last_carico[1]+"/"+last_carico[0];
		last_check=document.getElementById('lastck').value.split('-');
		last_check=last_check[2]+"/"+last_check[1]+"/"+last_check[0];
		document.getElementById("user_schede_rifiuti_deposito:last_carico").value=last_carico;
		document.getElementById("user_schede_rifiuti_deposito:last_check").value=last_check;
		}
	*/



	//window.alert(document.getElementById("user_schede_rifiuti_deposito:last_carico").value);
	//window.alert(document.getElementById("user_schede_rifiuti_deposito:last_check").value);
	//window.alert(document.getElementById("last_eseguo").value);
	}

function CUSTOM_CLONE_MAXSTOCK(NotUsed,FieldRef,FormRef){
	document.FGEForm_1["user_schede_rifiuti_deposito:MaxStockClone"].value = document.FGEForm_1["user_schede_rifiuti_deposito:MaxStock"].value;
	}

function CUSTOM_MEDIAGG(NotUsed,FieldRef,FormRef) {

	if(document.FGEForm_1["user_schede_rifiuti_deposito:carico_automatico"].value!=""){
		
		// qta carico automatico
		var CaricoAutomatico = parseFloat(document.FGEForm_1["user_schede_rifiuti_deposito:carico_automatico"].value);
		
		// peso specifico
		if(document.FGEForm_1["user_schede_rifiuti_deposito:peso_spec"].value!='' && document.FGEForm_1["user_schede_rifiuti_deposito:peso_spec"].value!=0)
			var peso_spec = parseFloat(document.FGEForm_1["user_schede_rifiuti_deposito:peso_spec"].value);
		else
			var peso_spec = 1;

		// converto qta carico automatico in Kg
		switch(document.FGEForm_1["user_schede_rifiuti_deposito:ID_UMIS"].value){
			case '1': //kg
				var quantita = CaricoAutomatico;
				break;
			case '2': //litri
				var quantita = CaricoAutomatico * peso_spec;
				break;
			case '3': //metri cubi
				var quantita = CaricoAutomatico * peso_spec * 1000;
				break;
			}
		}
	else
		var quantita	= 0;

	if(document.FGEForm_1["user_schede_rifiuti_deposito:intervallo"].value!="" && document.FGEForm_1["user_schede_rifiuti_deposito:intervallo"].value!="0")
		var intervallo	= parseInt(document.FGEForm_1["user_schede_rifiuti_deposito:intervallo"].value);
	else
		var intervallo	= 1;
	
	if(document.FGEForm_1["user_schede_rifiuti_deposito:ID_ICA"].value=='1')
		var giorni	= intervallo;
	else
		var giorni	= intervallo * 5; // 5 giorni lavorativi

	var mediaGG_stima		= quantita/giorni;
	document.FGEForm_1["user_schede_rifiuti_deposito:media_gg_stima"].value=mediaGG_stima;
	document.FGEForm_1["user_schede_rifiuti_deposito:media_settimanale_stima"].value=mediaGG_stima*5;

	}

function CHECK_INFODEP(NotUsed,FieldRef,FormRef) {

	// do not edit here
	if(document.FGEForm_1["user_schede_rifiuti_deposito:last_carico"].value.indexOf("-")!=-1 && document.FGEForm_1["user_schede_rifiuti_deposito:last_check"].value.indexOf("-")!=-1){
		LastCarico=document.FGEForm_1["user_schede_rifiuti_deposito:last_carico"].value.split("-");
		document.FGEForm_1["user_schede_rifiuti_deposito:last_carico"].value=LastCarico[2]+"/"+LastCarico[1]+"/"+LastCarico[0];
		LastCheck=document.FGEForm_1["user_schede_rifiuti_deposito:last_check"].value.split("-");
		document.FGEForm_1["user_schede_rifiuti_deposito:last_check"].value=LastCheck[2]+"/"+LastCheck[1]+"/"+LastCheck[0];
		}
	// end;


	// tutti i controlli solo se devo eseguire carichi automatici
	if(document.FGEForm_1["user_schede_rifiuti_deposito:eseguo_carico_automatico"][0].checked){

		// ho selezionato il contenitore ma non ho indicato quanti ne ho
		if(document.FGEForm_1["user_schede_rifiuti_deposito:ID_CONT"].value!='garbage' && document.FGEForm_1["user_schede_rifiuti_deposito:NumCont"].value<1){
			window.alert("Attenzione!\nE' stato selezionato il tipo di contenitore ma non il numero.");
			FGE_LockForm = true;
			return;
			}

		if(document.FGEForm_1["user_schede_rifiuti_deposito:ID_CONT"].value=='15' && (document.FGEForm_1["user_schede_rifiuti_deposito:MaxStock"].value<1 || document.FGEForm_1["user_schede_rifiuti_deposito:NumCont"].value>1)){
			window.alert("Attenzione!\nSelezionando il cumulo e' necessario indicare il quantitativo massimo stoccabile e \"numero di contenitori\" deve essere 1");
			FGE_LockForm = true;
			return;
			}

		var stop=false;
		var msg="Attenzione!";

		// non ho selezionato il contenitore, evidentemente sto salvando info su carichi automatici.. ho inserito tutto?
		//if(document.FGEForm_1["user_schede_rifiuti_deposito:ID_CONT"].value=='garbage'){
		if(document.FGEForm_1["user_schede_rifiuti_deposito:carico_automatico"].value!="" | document.FGEForm_1["user_schede_rifiuti_deposito:intervallo"].value!="" | document.FGEForm_1["user_schede_rifiuti_deposito:ID_ICA"].value!="garbage"){
			if(document.FGEForm_1["user_schede_rifiuti_deposito:carico_automatico"].value<1){
				stop=true;
				msg+="\n- indicare una quantita' valida per l' esecuzione dei carichi automatici";
				}
			if(document.FGEForm_1["user_schede_rifiuti_deposito:intervallo"].value<1){
				stop=true;
				msg+="\n- indicare un intervallo valido per l' esecuzione dei carichi automatici";
				}
			if(document.FGEForm_1["user_schede_rifiuti_deposito:ID_ICA"].value=='garbage'){
				stop=true;
				msg+="\n- indicare se l' intervallo e' espresso in giorni o settimane";
				}
			if(document.FGEForm_1["user_schede_rifiuti_deposito:MaxStock"].value<1 && document.FGEForm_1["user_schede_rifiuti_deposito:ID_CONT"].value=='garbage'){
				stop=true;
				msg+="\n- non e' stato selezionato alcun contenitore, selezionarlo o indicare il quantitativo massimo stoccabile";
				}
			if(stop){
				window.alert(msg);
				FGE_LockForm = true;
				return;
				}
			}
		}
	// fine dei controlli sui dati per carico automatico
	}


function F(S) { return S.replace(/(..)\/(..)\/(....)/, "$3/$2/$1") }

function CUSTOM_SETPORTATA(){

	var LKUP = new ajaxObject('__scripts/SetPortata.php',FGE_DummyAjaxCallback);	
	LKUP.callback = function (responseTxt, responseStat) {	
		document.tmp_portata['portata'].value=responseTxt;
		}
	
	Parameters = 'ID_CONT=' + document.FGEForm_1["user_schede_rifiuti_deposito:ID_CONT"].value;
	LKUP.update(Parameters);	 //}}}

	}



function updateTotale(ID_MZ_TRA){
	var costo_gasolio	= parseFloat(document.getElementById(ID_MZ_TRA+"-costo_gasolio").value);
	var costo_autostrada= parseFloat(document.getElementById(ID_MZ_TRA+"-costo_autostrada").value);
	var costo_rc		= parseFloat(document.getElementById(ID_MZ_TRA+"-costo_rc").value);
	var costo_bra		= parseFloat(document.getElementById(ID_MZ_TRA+"-costo_bra").value);
	var costo_conducente= parseFloat(document.getElementById(ID_MZ_TRA+"-costo_conducente").value);
	var costo_usura		= parseFloat(document.getElementById(ID_MZ_TRA+"-costo_usura").value);
	var costo_generale	= parseFloat(document.getElementById(ID_MZ_TRA+"-costo_generale").value);
	var totale			= Number(costo_gasolio + costo_autostrada + costo_rc + costo_bra + costo_conducente + costo_usura + costo_generale);
	document.getElementById(ID_MZ_TRA+"-costo_totale").value=totale.toFixed(3);
	}




/*
UTILITA' VARIA
*/


function trim(str, chars) {
	return ltrim(rtrim(str, chars), chars);
}
 
function ltrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
 
function rtrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

function isset(variable){
  return (typeof(window[variable]) == "undefined") ? false : true;
}





function CalcKeyCode(aChar) {
  var character = aChar.substring(0,1);
  var code = aChar.charCodeAt(0);
  return code;
}

function checkNumber(val) {
  var strPass = val.value;
  var strLength = strPass.length;
	
	  for(c=0;c<strLength;c++){
		var lchar = val.value.charAt(c);
		var cCode = CalcKeyCode(lchar);  
		if (!(cCode==46) && !(cCode>47 && cCode<58)) {
		  //var myNumber = val.value.substring(0, (strLength) - 1);
		  var myNumber = val.value.replace(lchar,'');
		  val.value = myNumber;
		  }
		}

  return false;
}