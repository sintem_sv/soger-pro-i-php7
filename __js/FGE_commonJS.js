var FirstErrorField;
var formRef;
var FGE_errors = new Array();
var FGE_badFields = new Array();
var FGE_MandatoryFields = new Array(); 
var FGE_AjaxLkUps = new Array();
var FGE_AjaxLkUpsStatus = new Array(); 
var FGE_DisabledFields = new Array();
var FGE_LockForm;
var FormSubmitRetry;
var FGE_ScaricoCorr;

function FGE_DummyAjaxCallback () {
	// this needs to be overrided
	return null;	
}
function FGE_AjaxLookUp(InArray) {
	Table = InArray[1];
	Field = InArray[2];
	TabIndex = InArray[3];
	LkTable = InArray[4];
	OnChangeJs = InArray[5];
	Detail = InArray[6];
	ForceFirst = InArray[7];
	Disabl = InArray[8];
	var LKUP = new ajaxObject('__scripts/FGE_AjaxLookUp.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		//window.alert(responseTxt);
      	document.getElementById(InArray[0]).innerHTML=responseTxt;
		//window.alert(responseTxt);
		DivRefreshedID = "FGED_" + InArray[1]+":"+InArray[2];
		FGE_AjaxLkUpsStatus[DivRefreshedID] = true;
		//alert("Refresh done: " + DivRefreshedID + " " + FGE_AjaxLkUpsStatus[DivRefreshedID]);// + " status: " + FGE_AjaxLkUpsStatus["FGED_" + Table+":"+Field]); 
	}
	Parameters = 'table=' + Table + '&field=' + Field + '&Tidx=' + TabIndex + '&LkTable=' + LkTable + '&OnCng=' + OnChangeJs + '&Detail=' + Detail;
	Parameters += '&ForceFirst=' + ForceFirst + '&Disabl=' + Disabl;
	LKUP.update(Parameters);
}
function FGE_AxajCalls() {
	for(DivName in FGE_AjaxLkUps) {
		FGE_AjaxLkUpsStatus[DivName] = false;
		FGE_AjaxLookUp(FGE_AjaxLkUps[DivName]);
	}
}
function FGE_AjaxRefresh(DivID) {
	//window.alert(DivID);
	FGE_AjaxLkUpsStatus[DivID] = false;
	//alert("Refresh: " + DivID + " status: " + FGE_AjaxLkUpsStatus[DivID]); 
	//alert(FGE_AjaxLkUps[DivID]);
	FGE_AjaxLookUp(FGE_AjaxLkUps[DivID]); // linea 17
}
function FGE_AjaxDgRefresh(Parameters,GridID) {
	var DG = new ajaxObject('__scripts/FGE_AjaxDataGrid.php',FGE_DummyAjaxCallback);
	DG.callback = function (responseTxt, responseStat) {
		//alert(responseTxt);
		document.getElementById('FGE_DG_' + GridID).innerHTML=responseTxt;
	}
	DG.update(Parameters);
}
function FGE_LkUpDetail(FieldValue,TableN,FieldN,LkTable) {
	if(FieldValue=='garbage') {
		return null;
	} else {
		var LKUP = new ajaxObject('__scripts/FGE_AjaxLookUpDetail.php',FGE_DummyAjaxCallback);
		LKUP.callback = function (responseTxt, responseStat) {
			document.getElementById('FGE_Detail'+TableN+':'+FieldN).innerHTML= responseTxt;
		}
	Parameters = 'table=' + LkTable + '&filter=' + FieldValue + '&field=' + FieldN;
	LKUP.update(Parameters);
	}
}
function ComboShowDetail(Table,Field,LkTable) {
	divRef = 'FGED_'+Table+':'+Field;
	if(document.getElementById(divRef).style.height=='') {
		document.getElementById(divRef).style.height = '150px';
		FGE_LkUpDetail(document.forms[0][Table+':'+Field].value,Table,Field,LkTable);
	} else {
		document.getElementById('FGE_Detail'+Table+':'+Field).innerHTML='';
		document.getElementById(divRef).style.height = '';
	}
}

function FGE_DisableFields() {
	for(dsb=0;dsb<FGE_DisabledFields.length;dsb++) {
			document.getElementById(FGE_DisabledFields[dsb]).disabled = true;
	}
}
function FGE_ReEnableFields() {
	for(c=0;c<document.forms[0].elements.length;c++) {
		if(document.forms[0].elements[c].type!="radio") {
			document.forms[0].elements[c].disabled = false;
		} else {
			document.getElementById(document.forms[0].elements[c].name+"A").disabled = false;
			document.getElementById(document.forms[0].elements[c].name+"B").disabled = false;
		}
	}
}
function FGE_Error(errorMsg,Fname) {
/*
Stores error messages (passed thru errorMsg parameter) 
Stores the names of fields with bad data (passed thru Fname parameter)

FGE_SendForm() will use FGE_badFields to set the focus on 
the first of the bad fields if forms' validation fails 
*/
	FGE_errors.push(errorMsg);
	FGE_badFields.push(Fname);
}
function FGE_AddMandatory(Fname) {
/*
adds a field to FGE_MandatoryFields array
FGE_MandatoryFields array is used by Regular Expression handling:
Regular expression check will be skipped if a field is not a mandatory one 
*/
	FGE_MandatoryFields.push(Fname);
}
function FGE_isMandatory(Fname) {
/*
returns true if a field is mandatory (ckecks FGE_MandatoryFields array)	
*/
	for (var i = 0; i < FGE_MandatoryFields.length; i++) {
		if(Fname == FGE_MandatoryFields[i]) {
			return true;
		}
	}
	return false;	
}

function FGE_AxajDoneCheck() {
	LkupCheck = true;
	for(LkupName in FGE_AjaxLkUpsStatus) {
		//alert("Send check: " + LkupName + " status: " + FGE_AjaxLkUpsStatus[LkupName]);
		if(!FGE_AjaxLkUpsStatus[LkupName]) {
			LkupCheck = false;			
		}
	}
	if(!LkupCheck) {
		if(window.pageYOffset) {
			YPos = window.pageYOffset;
			YPos = (YPos+100) + "px"
			document.getElementById("Wait").style.zIndex = 1;
			document.getElementById('Wait').style.top = YPos;
			document.getElementById("Wait").style.display = "block";
		} else {
			window.scroll(10,10);
			document.getElementById("Wait").style.zIndex = 1;
			document.getElementById("Wait").style.display = "block";
		}
		FGE_LockForm = true;
		//alert("start timer");
		if(!FormSubmitRetry) {
			FormSubmitRetry = setInterval("FGEForm_1_SendCheck(document.forms[0])", 400);
		}
	} else {
		document.getElementById("Wait").style.zIndex = -1;
		document.getElementById("Wait").style.display = "none";
		FGE_LockForm = false;
		clearInterval(FormSubmitRetry);
	}	
	return LkupCheck;
}

function SOGER_AutFormLock() {
	if (SOGER_AutLock==true) {
		//
		//	in CUSTOM_SCADENZA_AUT viene valorizzata la booleana
		//	qui controllata

		if(SOGER_AutLockTRA | SOGER_AutLockDEST | SOGER_AutLockINT) {
			alert("Impossibile salvare il movimento: autorizzazione trasportatore, destinatario o intermediario mancante/scaduta");
			return false;
		} else {
			return true;
		}
	} else {
		//alert("non ci sono");
		return true;
	}
}

function FGE_SendForm() {
/*
displays an error alert if  form validation fails 
and put focus on first of the bad fields
*/
	if(FGE_LockForm==true) {
		FGE_errors = new Array();
		return false;
	}
	if(FGE_errors.length>0) {
		focusOn = FGE_badFields[0];
		ErrMsg = "";
		for (var i = 0; i < FGE_errors.length; i++) {
			ErrMsg = ErrMsg + FGE_errors[i] + "\n";
		}
		alert('Errore\n-----------------------------\n' + ErrMsg);
		FGE_errors = new Array();
		FGE_badFields = new Array();
		FGE_MandatoryFields = new Array(); 
		formRef.elements[focusOn].focus();
		formRef.elements[focusOn].style.backgroundColor='#FF0000';
		} else {
		FGE_ReEnableFields();
		document.getElementById("SubmitButton").disabled=true;
		formRef.submit();
	}
}
function FGE_MandatoryTXTFields(name,content,Fname) {
/*
validation script for mandatory text fields
*/
	FGE_AddMandatory(Fname);
	if(content.length==0) {
		FGE_Error("Il campo \'" + name + "\' non deve essere vuoto",Fname);
	}
}
function FGE_MandatoryINTFields(name,content,Fname) {
/*
validation script for integer number fields
*/
	FGE_AddMandatory(Fname);
	if(content.length==0) {
		FGE_Error("Il campo \'" + name + "\' non deve essere vuoto",Fname);
	}
	if(isNaN(content)) {
		FGE_Error("Il campo \'" + name + "\' deve contenere un numero intero",Fname);
	}
}
function FGE_MandatoryBool(name,FieldRef,Fname) {
/*
validation script for mandatory true/false radio buttons 
*/
	FGE_AddMandatory(Fname);
	if(!FieldRef[0].checked && !FieldRef[1].checked) {
		FGE_Error("Occorre selezionare un valore per il campo \'" + name + "\'",Fname);
	}
}
function FGE_MandatorySelect(name,selIdx,Fname) {
/*
validation script for mandatory combo control 
*/
	FGE_AddMandatory(Fname);
	if(selIdx==0) {
		FGE_Error("Occorre selezionare un valore per il campo \'" + name + "\'",Fname);
	}
}
function FGE_RegExpCheck(RPat,content,Fname,name) {
/*
validation script for Regular Expression patterns
the script is skipped if the field is empty and it's not mandatory
*/
	if(!FGE_isMandatory(Fname) && content=="") {
		return false;
	}
	if(!content.match(RPat)) {
		FGE_Error("Il formato del campo \'" + name + "\' e\' errato",Fname);	
	}
}


function dump(arr,level){
	var dumped_text = "";
	if(!level) level = 0;
	//The padding given at the beginning of the line.
	var level_padding = "";
	for(var j=0;j<level+1;j++) level_padding += "    ";

	if(typeof(arr) == 'object') { //Array/Hashes/Objects
		for(var item in arr) {
			var value = arr[item];

			if(typeof(value) == 'object') { //If it is an array,
				dumped_text += level_padding + "'" + item + "' ...\n";
				dumped_text += dump(value,level+1);
			} else {
				dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
			}
		}
	} else { //Stings/Chars/Numbers etc.
		dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
	}
	return dumped_text;
        }
