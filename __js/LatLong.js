/*
Global vars
*/

var latitude;
var longitude;

var NewLatitudine;
var NewLongitudine;

var datiAzienda;



$(document).ready(function(){

$( "#jqpopup_LatLong" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	width: 450, 
	buttons: {
		"Salva posizione": function() {
			SaveNewLatLng($(this).attr('rel'));
			},
		"Chiudi": function() {
			$("#address").val('');
			$( this ).dialog( "close" );
			}
		}
	});


$(document).on('click', '.jq_LatLong', function(){

	// Show dialog box
	$( "#jqpopup_LatLong" ).dialog("open");
	$( "#jqpopup_LatLong" ).attr("rel", $(this).attr('rel'));
	
	var ref = $(this).attr('rel');
	ShowMap(ref);

	});



// END
});





















function SaveNewLatLng(ref){
	$.post('__scripts/SetLatLng.php', {ref:ref, latitudine:NewLatitudine, longitudine:NewLongitudine}, function(phpResponse){
		NewLatitudine = undefined;
		NewLongitudine = undefined;
		//$('[id="'+ref+'"]').attr('src', '__css/marker_on.gif');
		$('img[rel="'+ref+'"]').attr('src', '__css/marker_on.gif');
		alert("Salvataggio ultimato");
		});
	}


function CenterMap() {
	var address = document.getElementById('address').value;
	geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        var myOptions = {
        zoom: 15,
        center: results[0].geometry.location,
        mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("LatLong_map"), myOptions);

		// infowindow coi dati dell'azienda
		var infowindow = new google.maps.InfoWindow({
			content: datiAzienda
			});

		// metto il marker nella posizione dell'azienda
		marker = new google.maps.Marker({ 
			map:map, 
			draggable:true,
			animation:google.maps.Animation.DROP, 
			position:results[0].geometry.location 
			});
		
		NewLatitudine = marker.getPosition().lat();
		NewLongitudine = marker.getPosition().lng();

		// intercetto il click sul marker per mostrare infowindow con dati azienda
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map,marker);
			});

		google.maps.event.addListener(marker, 'dragstart', function() {
			infowindow.close(map,marker);
			});

		google.maps.event.addListener(marker, 'dragend', function() {
			NewLatitudine = marker.getPosition().lat();
			NewLongitudine = marker.getPosition().lng();
			});
		  }
		});
	  }


function ShowMap(ref){

	// recupero dati azienda
	$.post('__scripts/GetInfoWindowData.php', {ref:ref}, function(phpResponse){
		
		datiAzienda = "<div id='InfoWindow' style='min-width:180px;overflow:hidden;'>";
			datiAzienda+= "<p class='title'>"+phpResponse.azienda+"</p>";
			datiAzienda+= "<p class='indirizzo'>"+phpResponse.indirizzo+"<br />"+phpResponse.comune+" ("+phpResponse.provincia+") <br />"+phpResponse.nazione+"</p>";
		datiAzienda+= "</div>";

		// center map address
		//alert(dump(phpResponse));
		if(phpResponse.StdLatLng){
			var address = phpResponse.comune + " (" + phpResponse.provincia + ")";
			
			var geocoder = new google.maps.Geocoder();
            geocoder.geocode({"address": address}, function(results, status){
				if(status=="OK"){
					latitude	= results[0].geometry.location.lat();
					longitude	= results[0].geometry.location.lng();
					}
				else{
                    alert("Comune non trovato, contattare l'assistenza.");
					latitude	= 0;
					longitude	= 0;
                    }
				ShowMap_step2(address, latitude, longitude);
                });
			}
		else{
			var address		= phpResponse.indirizzo + ", " + phpResponse.comune + " (" + phpResponse.provincia + ")";
			latitude		= phpResponse.latitude;
			longitude		= phpResponse.longitude;
			ShowMap_step2(address, latitude, longitude);
			}


		}, "json");

	}



function ShowMap_step2(address, latitude, longitude){

		document.getElementById('address').value = address;
		
		// coordinate dell'azienda
		azienda = new google.maps.LatLng(latitude, longitude);

		// opzioni della mappa
		var myOptions = {

			// coordinate punto centrale ( azienda )
			center: azienda,

			// livello di zoom
			zoom: 15,

			// tipo di mappa
			mapTypeId: google.maps.MapTypeId.ROADMAP
			};

		// creo la mappa con le opzioni sopra definite
		var map = new google.maps.Map(document.getElementById('LatLong_map'), myOptions);

		// infowindow coi dati dell'azienda
		var infowindow = new google.maps.InfoWindow({
			content: datiAzienda,
			maxWidth : 350
			});

		//datiAzienda = undefined;

		// metto il marker nella posizione dell'azienda
		marker = new google.maps.Marker({ 
			map:map, 
			draggable:true,
			animation:google.maps.Animation.DROP, 
			position:azienda 
			});
		
		// intercetto il click sul marker per mostrare infowindow con dati azienda
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map,marker);
			});

		google.maps.event.addListener(marker, 'dragstart', function() {
			infowindow.close(map,marker);
			});

		google.maps.event.addListener(marker, 'dragend', function() {
			NewLatitudine = marker.getPosition().lat();
			NewLongitudine = marker.getPosition().lng();
			});


}