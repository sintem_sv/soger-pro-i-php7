<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__libs/zip3.php");
require_once("../__libs/fpdf.php");
require_once("../__classes/SogerMailer.php");

$mail = new SogerMailer();
$mail->isHTML(true);
$mail->setFrom('cron@sogerpro.it', 'MEMO So.Ge.R. PRO');
$mail->addAddress('programmazione@sintem.it');
$mail->Subject  = 'INIZIO INVIO MEMO';
$message    = 'Inizio!';
//$message  = utf8_encode($message);
$mail->Body = $message;
$mail->send();

## CONNECTION TO DATABASE
$FEDIT=new DbLink();
$FEDIT->DbConn('soger'.date('Y'));

## RETRIVE USER TO GENERATE REG
$SQL="SELECT ID_IMP, produttore, trasportatore, destinatario, intermediario, MODULO_SIS, MODULO_LDP FROM core_impianti WHERE ID_IMP IN (SELECT DISTINCT ID_IMP FROM core_users WHERE manutenzione=0 AND bloccatoEcostudio=0 AND usr<>'sintem');";

$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);

## CYCLE IMP
for($i=0;$i<count($FEDIT->DbRecordSet);$i++){
			
	# ID_IMP
	$ID_IMP = $FEDIT->DbRecordSet[$i]['ID_IMP'];
	$MODULO_SIS = $FEDIT->DbRecordSet[$i]['MODULO_SIS'];
	$MODULO_LDP = $FEDIT->DbRecordSet[$i]['MODULO_LDP'];

	# ID_USR
	$SQL = "SELECT ID_USR, GGedit_produttore, GGedit_trasportatore, GGedit_destinatario, GGedit_intermediario FROM core_users WHERE ID_IMP='".$ID_IMP."' AND usr<>'sintem' AND manutenzione=0 AND bloccatoEcostudio=0 ORDER BY R5 DESC LIMIT 0, 1;";
	$FEDIT->SdbRead($SQL,"DbRecordSetUSR",true,false);
        $USER = $FEDIT->DbRecordSetUSR[0]['ID_USR'];
        
	# workmode
	$WM_global = array('produttore', 'trasportatore', 'destinatario', 'intermediario');
        foreach($WM_global AS $k=>$w){
            if($FEDIT->DbRecordSet[$i][$w]=='0')
                unset($WM_global[$k]);
        }
        $WM_global = array_values($WM_global);
        
        
	for($w=0;$w<count($WM_global);$w++){
            
            # Giorni per edit registro
            $GGedit	= $FEDIT->DbRecordSetUSR[0]['GGedit_'.$WM_global[$w]];
            
            // Se � il 31-12 stampo tutto!
            $PRINT_UNTIL =  date('d-m')!='31-12'? date('Y-m-d', strtotime('-'.$GGedit.' day', strtotime(date('Y-m-d')))) : date('Y').'-12-31';
            
            $SQLMIN = "SELECT MIN( NMOV ) AS MIN FROM user_movimenti_fiscalizzati WHERE user_movimenti_fiscalizzati.".$WM_global[$w]." =1 AND user_movimenti_fiscalizzati.ID_IMP = '".$ID_IMP."' AND DTMOV<'".$PRINT_UNTIL."' AND NMOV<>9999999;";
            $FEDIT->SDBRead($SQLMIN,"DbRecordSetMIN",true,false);

            if(!is_null($FEDIT->DbRecordSetMIN[0]['MIN'])){

                $MIN	= $FEDIT->DbRecordSetMIN[0]['MIN'];

                $SQLMAX	= "SELECT MAX( NMOV ) AS MAX FROM user_movimenti_fiscalizzati WHERE user_movimenti_fiscalizzati.".$WM_global[$w]." =1 AND user_movimenti_fiscalizzati.ID_IMP = '".$ID_IMP."' AND DTMOV<'".$PRINT_UNTIL."' AND NMOV<>9999999;";
                $FEDIT->SDBRead($SQLMAX,"DbRecordSetMAX",true,false);
                if(!is_null($FEDIT->DbRecordSetMAX[0]['MAX']))
                    $MAX	= $FEDIT->DbRecordSetMAX[0]['MAX'];
                else{
                    $SQLMAX	= "SELECT MAX( NMOV ) AS MAX FROM user_movimenti_fiscalizzati WHERE user_movimenti_fiscalizzati.".$WM_global[$w]." =1 AND user_movimenti_fiscalizzati.ID_IMP = '".$ID_IMP."' AND NMOV<>9999999;";
                    $FEDIT->SDBRead($SQLMAX,"DbRecordSetMAX",true,false);
                    $MAX	= $FEDIT->DbRecordSetMAX[0]['MAX'];	
                }
                
                // reg print preferences
                $SQL	= "SELECT REG_PRNT_X AS core_usersREG_PRNT_X, REG_PRNT_Y AS core_usersREG_PRNT_Y, reg_trasp_doublecheck AS core_usersreg_trasp_doublecheck, CER_AXT AS core_usersCER_AXT, REG_RS_CARICHI AS core_usersREG_RS_CARICHI, REG_NOMECER AS core_usersREG_NOMECER, REG_PS_DEST AS core_usersREG_PS_DEST, REG_IDSIS_SCHEDA AS core_usersREG_IDSIS_SCHEDA, REG_DIS_PROD AS core_usersREG_DIS_PROD, ID_UMIS AS core_usersID_UMIS, ID_UMIS_INT AS core_usersID_UMIS_INT, UMIS_RIF AS core_usersUMIS_RIF, PRNT_CLASSIH AS core_usersPRNT_CLASSHIH FROM core_users WHERE ID_USR=".$USER.";";
                $FEDIT->SDBRead($SQL,"DbRecordSetPR",true,false);
                $RegPreferences = $FEDIT->DbRecordSetPR[0];

                // reg data
                if($WM_global[$w]!='intermediario'){
                    $sql  = "SELECT";
                    $sql .= " user_movimenti_fiscalizzati.ID_MOV_F, user_movimenti_fiscalizzati.FISCALE, user_movimenti_fiscalizzati.PerRiclassificazione,user_movimenti_fiscalizzati.TIPO,user_movimenti_fiscalizzati.NMOV,user_movimenti_fiscalizzati.DTMOV, user_movimenti_fiscalizzati.idSIS_scheda, user_movimenti_fiscalizzati.idSIS, user_movimenti_fiscalizzati.dataSchedaSistri_invio";
                    $sql .= ",user_movimenti_fiscalizzati.NFORM,user_movimenti_fiscalizzati.DTFORM,user_movimenti_fiscalizzati.N_ANNEX_VII,user_movimenti_fiscalizzati.lotto,user_movimenti_fiscalizzati.FKErifCarico";
                    $sql .= ",user_movimenti_fiscalizzati.quantita,user_movimenti_fiscalizzati.NOTER ,user_movimenti_fiscalizzati.VER_DESTINO,user_movimenti_fiscalizzati.PS_DESTINO, user_movimenti_fiscalizzati.LINK_DestProd";
                    $sql .= ",user_schede_rifiuti.descrizione,user_schede_rifiuti.NOTER AS NOTE_RIFIUTO,user_schede_rifiuti.RifDaManutenzione,lov_cer.COD_CER,lov_cer.PERICOLOSO as pericoloso, peso_spec";
                    $sql .= ",lov_stato_fisico.description AS STFdes";
                    $sql .= ",H1,H2,H3A,H3B,H4,H5,H6,H7,H8,H9,H10,H11,H12,H13,H14,H15";
                    $sql .= ",HP1,HP2,HP3,HP4,HP5,HP6,HP7,HP8,HP9,HP10,HP11,HP12,HP13,HP14,HP15";
                    $sql .= ",ANN_scheda, ANN_scheda_causale, ANN_registrazione, ANN_registrazione_causale";
                    $sql .= ",lov_misure.ID_UMIS AS Umisura";
                    $sql .= ",user_aziende_produttori.description AS PRdes,user_impianti_produttori.description AS IMdes";
                    $sql .= ",COMpro.description AS COMdes,COMpro.shdes_prov AS COMproPR, COMpro.des_prov AS COMproDesProv, COMpro.IN_ITALIA AS COMproItaliano";
                    $sql .= ",lov_operazioni_rs.description AS OPRSdes";
                    $sql .= ",lov_cer.description AS NomeCER";
                    $sql .= ",user_aziende_intermediari.description AS INTdes,user_aziende_intermediari.commerciante AS INTcommerciante,user_aziende_intermediari.esclusoFIR AS esclusoFIR,user_aziende_intermediari.indirizzo AS INTind";
                    $sql .= ",user_aziende_intermediari.codfisc AS INTcfisc, user_aziende_intermediari.ID_COM AS INTidcom";
                    $sql .= ",user_autorizzazioni_interm.num_aut AS INTaut,user_movimenti_fiscalizzati.FKErilascioI AS INTrilascio";
                    $sql .= ",COMint.description AS COMintdes,COMint.shdes_prov AS COMintPR, COMint.CAP AS INTCAP, COMint.des_prov AS COMintDesProv, COMint.IN_ITALIA AS COMintItaliano";
                    $sql .= " FROM user_movimenti_fiscalizzati"; 
                    $sql .= " JOIN user_schede_rifiuti on user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF";
                    $sql .= " JOIN lov_cer on user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
                    $sql .= " JOIN lov_stato_fisico ON user_schede_rifiuti.ID_SF=lov_stato_fisico.ID_SF";
                    $sql .= " JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS";

                    // potrebbe non esservi produttore nel caso di destinatario con maschera ridotta (LEFT)
                    $sql .= " LEFT JOIN user_aziende_produttori ON user_movimenti_fiscalizzati.ID_AZP=user_aziende_produttori.ID_AZP";
                    $sql .= " LEFT JOIN user_impianti_produttori ON user_impianti_produttori.ID_UIMP=user_movimenti_fiscalizzati.ID_UIMP"; 
                    $sql .= " LEFT JOIN lov_comuni_istat AS COMpro ON user_impianti_produttori.ID_COM=COMpro.ID_COM";

                    $sql .= " LEFT JOIN lov_operazioni_rs ON user_movimenti_fiscalizzati.ID_OP_RS=lov_operazioni_rs.ID_OP_RS";
                    $sql .= " LEFT JOIN user_aziende_intermediari ON user_movimenti_fiscalizzati.ID_AZI=user_aziende_intermediari.ID_AZI";
                    $sql .= " LEFT JOIN user_impianti_intermediari ON user_impianti_intermediari.ID_UIMI=user_movimenti_fiscalizzati.ID_UIMI"; 
                    $sql .= " LEFT JOIN user_autorizzazioni_interm ON user_movimenti_fiscalizzati.ID_AUTHI=user_autorizzazioni_interm.ID_AUTHI";
                    $sql .= " LEFT JOIN lov_comuni_istat AS COMint ON user_aziende_intermediari.ID_COM=COMint.ID_COM";
                }
                else{
                    $sql = "SELECT";
                    $sql .= " user_movimenti_fiscalizzati.ID_MOV_F, user_movimenti_fiscalizzati.FISCALE,user_movimenti_fiscalizzati.TIPO,user_movimenti_fiscalizzati.NMOV,user_movimenti_fiscalizzati.DTMOV, user_movimenti_fiscalizzati.idSIS_scheda, user_movimenti_fiscalizzati.idSIS, user_movimenti_fiscalizzati.dataSchedaSistri_invio";
                    $sql .= ",user_movimenti_fiscalizzati.NFORM,user_movimenti_fiscalizzati.DTFORM";
                    $sql .= ",user_movimenti_fiscalizzati.quantita,user_movimenti_fiscalizzati.PS_DESTINO,user_movimenti_fiscalizzati.NOTER";
                    $sql .= ",user_schede_rifiuti.descrizione,user_schede_rifiuti.NOTER AS NOTE_RIFIUTO,lov_cer.COD_CER,lov_cer.PERICOLOSO as pericoloso, peso_spec";
                    $sql .= ",lov_stato_fisico.description AS STFdes";
                    $sql .= ",H1,H2,H3A,H3B,H4,H5,H6,H7,H8,H9,H10,H11,H12,H13,H14,H15";
                    $sql .= ",HP1,HP2,HP3,HP4,HP5,HP6,HP7,HP8,HP9,HP10,HP11,HP12,HP13,HP14,HP15";
                    $sql .= ",ANN_scheda, ANN_scheda_causale, ANN_registrazione, ANN_registrazione_causale";
                    $sql .= ",lov_misure.ID_UMIS AS Umisura";
                    $sql .= ",user_aziende_produttori.description AS PRdes,user_aziende_produttori.codfisc AS PRCF,user_impianti_produttori.description AS IMPRdes";
                    $sql .= ",user_aziende_destinatari.description AS DEdes,user_aziende_destinatari.codfisc AS DECF,user_impianti_destinatari.description AS IMDEdes";
                    $sql .= ",user_aziende_trasportatori.description AS TRdes,user_aziende_trasportatori.codfisc AS TRCF,user_impianti_trasportatori.description AS IMTRdes";
                    $sql .= ",COMpro.description AS COMPRdes,COMpro.shdes_prov AS COMPRPR";
                    $sql .= ",COMdes.description AS COMDEdes,COMdes.shdes_prov AS COMDEPR";
                    $sql .= ",COMtra.description AS COMTRdes,COMtra.shdes_prov AS COMTRPR";
                    $sql .= ",lov_operazioni_rs.description AS OPRSdes";
                    $sql .= ",lov_cer.description AS NomeCER";
                    $sql .= " FROM user_movimenti_fiscalizzati"; 
                    $sql .= " JOIN user_schede_rifiuti on user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF";
                    $sql .= " JOIN lov_cer on user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
                    $sql .= " JOIN lov_stato_fisico ON user_schede_rifiuti.ID_SF=lov_stato_fisico.ID_SF";
                    $sql .= " JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS";
                    $sql .= " JOIN user_aziende_produttori ON user_movimenti_fiscalizzati.ID_AZP=user_aziende_produttori.ID_AZP";
                    $sql .= " JOIN user_impianti_produttori ON user_impianti_produttori.ID_UIMP=user_movimenti_fiscalizzati.ID_UIMP"; 
                    $sql .= " JOIN lov_comuni_istat AS COMpro ON user_impianti_produttori.ID_COM=COMpro.ID_COM";
                    $sql .= " JOIN user_aziende_destinatari ON user_movimenti_fiscalizzati.ID_AZD=user_aziende_destinatari.ID_AZD";
                    $sql .= " JOIN user_impianti_destinatari ON user_impianti_destinatari.ID_UIMD=user_movimenti_fiscalizzati.ID_UIMD"; 
                    $sql .= " JOIN lov_comuni_istat AS COMdes ON user_impianti_destinatari.ID_COM=COMdes.ID_COM";
                    $sql .= " JOIN user_aziende_trasportatori ON user_movimenti_fiscalizzati.ID_AZT=user_aziende_trasportatori.ID_AZT";
                    $sql .= " JOIN user_impianti_trasportatori ON user_impianti_trasportatori.ID_UIMT=user_movimenti_fiscalizzati.ID_UIMT"; 
                    $sql .= " JOIN lov_comuni_istat AS COMtra ON user_impianti_trasportatori.ID_COM=COMtra.ID_COM";
                    $sql .= " LEFT JOIN lov_operazioni_rs ON user_movimenti_fiscalizzati.ID_OP_RS=lov_operazioni_rs.ID_OP_RS";
                }

                // complete query
                $sql .= " WHERE (NMOV>='" . $MIN . "' AND NMOV<='" . $MAX . "') ";
                $sql .= " AND user_movimenti_fiscalizzati.".$WM_global[$w]."=1 ";
                $sql .= " AND user_movimenti_fiscalizzati.ID_IMP='" . $ID_IMP . "'";
                $sql .= " AND NMOV<>9999999 ";
                $sql .= " ORDER BY NMOV ASC";
                $FEDIT->SDBRead($sql,"DbRecordSetMov",true,false);

                //die(var_dump($FEDIT->DbRecordSetMov));                
                
                if(isset($FEDIT->DbRecordSetMov)){

                    // doc name
                    $DcName	= "Registro_rifiuti_".$WM_global[$w]."_al_".date('d-m-Y');
                    // pagination
                    $Xcorr	= $RegPreferences["core_usersREG_PRNT_X"];
                    $Ycorr	= $RegPreferences["core_usersREG_PRNT_Y"];
                    if($Xcorr>4)	$Xcorr=4;
                    if($Ycorr>12)	$Ycorr=12;
                    $x = 2 + $Xcorr;
                    $y = 12 + $Ycorr;
                    $GridsCount = 0;
                    $PageCount = 1;
                    $PageNr = 1;
                    $CustomPageIniz = false;
                    $TotalPages = ceil(count($FEDIT->DbRecordSetMov)/3);
                    $BufAnno = date("Y");						
                    $orientation="P";	
                    $um="mm";
                    $Format = array(210,297);
                    $ZeroMargin = true;
                    $FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin);
                    $FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
			
                    # crea sfondi (griglia dati)
                    for($c=0;$c<count($FEDIT->DbRecordSetMov);$c++) {

                        # MODULO_LDP
                        $LDP = '';
                        if($MODULO_LDP=='1' AND $FEDIT->DbRecordSetMov[$c]["TIPO"]=="C" AND $WM_global[$w]=="produttore" AND $FEDIT->DbRecordSetMov[$c]['LINK_DestProd']!='' AND !is_null($FEDIT->DbRecordSetMov[$c]['LINK_DestProd'])){
                            $LDP.= "Formulari dal cui trattamento deriva il carico: ";
                            $FIR = explode(',', $FEDIT->DbRecordSetMov[$c]['LINK_DestProd']);
                            for($f=0;$f<count($FIR);$f++){
                                $SQL = "SELECT IF(DTFORM IS NULL, 'n.d.', DTFORM) AS DTFORM, NFORM, NMOV, lov_cer.COD_CER, user_schede_rifiuti.descrizione ";
                                $SQL.= "FROM user_movimenti_fiscalizzati ";
                                $SQL.= "JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_movimenti_fiscalizzati.ID_RIF ";
                                $SQL.= "JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
                                $SQL.= "WHERE ID_MOV_F=".$FIR[$f].";";
                                $FEDIT->SDBRead($SQL,"DbRecordSetLDP",true,false);
                                $LDP.= $FEDIT->DbRecordSetLDP[0]['NFORM']." ";
                                if($FEDIT->DbRecordSetLDP[0]['DTFORM']!='n.d.'){
                                    $DTFORM_array = explode('-', $FEDIT->DbRecordSetLDP[0]['DTFORM']);
                                    $DTFORM = $DTFORM_array[2].'/'.$DTFORM_array[1].'/'.$DTFORM_array[0];
                                    $LDP.= "del ".$DTFORM;
                                }
                                $LDP.= "(mov ".$FEDIT->DbRecordSetLDP[0]['NMOV'].")";
                                $LDP.= ", ";
                            }
                            $LDP = substr($LDP, 0, strlen($LDP)-2);
                            $LDP.= " (mov. del registro destinatario)";
                        }

                        if($FEDIT->DbRecordSetMov[$c]['FISCALE']=='0'){
                            $sql = "UPDATE user_movimenti_fiscalizzati SET FISCALE=1 WHERE ID_MOV_F=".$FEDIT->DbRecordSetMov[$c]['ID_MOV_F'].";";
                            $FEDIT->SDBWrite($sql,true,false);
                        }

                        if($WM_global[$w]!='intermediario'){					
                            SfondoRegistro($x,$y,$FEDIT->FGE_PdfBuffer);
                            DatiRegistro($FEDIT->DbRecordSetMov[$c],$x,$y,$FEDIT->FGE_PdfBuffer,$WM_global[$w],$RegPreferences, $MODULO_SIS, $LDP);
                        }
                        else{
                            SfondoRegistroInt($x,$y,$FEDIT->FGE_PdfBuffer);
                            DatiRegistroInt($FEDIT->DbRecordSetMov[$c],$x,$y,$FEDIT->FGE_PdfBuffer,$WM_global[$w],$RegPreferences, $MODULO_SIS, $LDP);
                        }
                        $y +=84.6;
                        $GridsCount++;
                        #ultima pagina del registro
                        if($PageCount==$TotalPages) {										
                            $FEDIT->FGE_PdfBuffer->SetXY(190,271);
                            $FEDIT->FGE_PdfBuffer->MultiCell(25,5,$PageNr,0,"L");
                        }
                        #pagine intermedie del registro
                        if($GridsCount==3 && $PageCount<$TotalPages) {						
                            # numeri di pagina
                            $FEDIT->FGE_PdfBuffer->SetXY(190,271);
                            $FEDIT->FGE_PdfBuffer->MultiCell(25,5,$PageNr,0,"L");
                            $FEDIT->FGE_PdfBuffer->AddPage();
                            $GridsCount = 0;
                            $PageCount++;
                            $PageNr++;
                            $x = 2 + $Xcorr;
                            $y = 12 + $Ycorr;
                        }
                    }

                    $FEDIT->FGE_PdfBuffer->Output('__reg/'.$ID_IMP.'/'.$DcName.'.pdf',"F");
                    $FEDIT->FGE_PdfBuffer = NULL;

                    // MAIL
                    $SQL = "SELECT DISTINCT mail FROM user_promemoria WHERE ID_IMP='".$ID_IMP."' AND id_prom=1 AND id_fprom=1;";
                    $FEDIT->SDBRead($SQL,"DbRecordSetMAIL",true,false);

                    if($FEDIT->DbRecsNum>0){
                        
                        for($m=0; $m<count($FEDIT->DbRecordSetMAIL); $m++){

                            // Stabilimento
                            $sql = "SELECT core_impianti.description AS Impianto, lov_comuni_istat.description AS Comune FROM core_impianti JOIN lov_comuni_istat ON core_impianti.ID_COM=lov_comuni_istat.ID_COM WHERE ID_IMP='".$ID_IMP."';";
                            $FEDIT->SDBRead($sql,"DbRecordSetStabilimento",true,false);
                            $Stabilimento = $FEDIT->DbRecordSetStabilimento[0]['Impianto'] . ' di ' . $FEDIT->DbRecordSetStabilimento[0]['Comune'];

                            $MAIL = $FEDIT->DbRecordSetMAIL[$m]['mail'];

                            ## SEND NOTIFICATION MAIL TO USER
                            $mail = new SogerMailer();
                            $mail->isHTML(true);
                            $mail->setFrom('cron@sogerpro.it', 'MEMO So.Ge.R. PRO');
                            $mail->addAddress($MAIL);
                            $mail->Subject = 'Attestazione della generazione del registro carico/scarico rifiuti '.$WM_global[$w];

                            $message	 = 'Gentile utente, <br /><br />';
                            $message	.= '<b>Il registro di carico / scarico dei rifiuti per il profilo d\'uso '.$WM_global[$w].' dello stabilimento '.$Stabilimento.' � stato generato, in formato pdf inalterabile,</b> relativo alla settimana appena trascorsa, corredato di tutta la movimentazione rifiuti dalla data del 1 gennaio dell\'anno fiscale corrente.<br /><br />';
                            $message	.= 'Il documento � quindi conforme al dispositivo previsto dall\'articolo 190, comma 5, del D.Lgs. 152/2006 e s.m.i. (<i>gestione dei registri con le procedure e le modalit� fissate dalla normativa sui registri IVA</i>).<br /><br />'; 
                            $message	.= 'Il file del registro � scaricabile all\'indirizzo internet che � indicato nella mail che segue a breve.<br /><br />';
                            $message	.= '<i>Questa e-mail � generata automaticamente da So.Ge.R. PRO, si prega di non rispondere.</i><br /><br />';
                            $message	.= '<i>Per qualsiasi segnalazione o necessit� di assistenza, Vi invitiamo all\'utilizzo del servizio ticket apposito.</i><br /><br />';
                            $message	.= 'ID_IMP: '.$ID_IMP.'<br />';
                            $message	.= 'ID_USR: '.$USER.'<br />';
                            //$message  = utf8_encode($message);
                            $mail->Body = $message;
                                                
                            if(!$mail->send()){
                                $NoticeMail = new SogerMailer();
                                $NoticeMail->isHTML(true);
                                $NoticeMail->setFrom('cron@sogerpro.it', 'MEMO So.Ge.R. PRO');
                                $NoticeMail->addAddress('programmazione@sintem.it');
                                $NoticeMail->Subject = 'ERRORE INVIO MEMO';
                                $NoticeMail->Body = 'Errore nell\'invio della mail di notifica a '.$MAIL.' ('.$ID_IMP.' - '.$WM_global[$w] . ') da parte di '.$_SERVER["PHP_SELF"];
                                $NoticeMail->send();
                                }
                            else{
                                $mail2 = new SogerMailer();
                                $mail2->isHTML(true);
                                $mail2->setFrom('cron@sogerpro.it', 'MEMO So.Ge.R. PRO');
                                $mail2->addAddress($MAIL);
                                $mail2->Subject = 'Link per il download del registro carico/scarico rifiuti '.$WM_global[$w];

                                $message	 = 'Gentile utente,<br /><br />';
                                $message	.= 'come anticipato ti informiamo che il file del registro di carico e scarico rifiuti � scaricabile dal seguente indirizzo: <a href="https://www.sogerpro.it/soger/index.php?App=documents">https://www.sogerpro.it/soger/index.php?App=documents</a>.<br /><br />';
                                $message	.= 'Per entrare nella pagina e scaricare il file � necessario eseguire il login.<br /><br />';
                                $message	.= '<i>Questa e-mail � generata automaticamente da So.Ge.R. PRO, si prega di non rispondere.</i><br /><br />';
                                $message	.= '<i>Per qualsiasi segnalazione o necessit� di assistenza, Vi invitiamo all\'utilizzo del servizio ticket apposito.</i><br /><br />';
                                //$message  = utf8_encode($message);
                                $mail2->Body = $message;
                                $mail2->send();
                                }

                        }
                    }
                    
                }
            }
        }
    }



$mail = new SogerMailer();
$mail->isHTML(true);
$mail->setFrom('cron@sogerpro.it', 'MEMO So.Ge.R. PRO');
$mail->addAddress('programmazione@sintem.it');
$mail->Subject  = 'FINE INVIO MEMO';
$message    = 'Fine!';
//$message  = utf8_encode($message);
$mail->Body = $message;
$mail->send();







########################################### USEFUL FUNCTIONS

function number_format_unlimited_precision($number,$decimal = '.'){
	if(strpos($decimal, $number)!==false && strpos($decimal, $number)!==0){
		$broken_number = explode($decimal,$number);
		if($broken_number[1]==0){
			return number_format($broken_number[0], 0, ".", "");
			}
		else{
			return number_format($broken_number[0], 0, "", "").$decimal.$broken_number[1];
			}
		}
	else{
		return number_format($number, 2, ".", "");
		}
	}

function KgQConvert($in,$Um,$PesoSpec) {
//{{{
	if($in=="0") {
		return 0;
	}
	switch($Um) {
		# regole per arrotondare:
		# - se il valore da arrotondare � <1 arrotondo per eccesso con due decimali
		# - se il valore da arrotondare � >1 arrotondo per eccesso o difetto senza decimali
		case "M3":
			$arrotondato=round(($in/$PesoSpec/1000),2);
			if($arrotondato<1){
				if($arrotondato==0)
					return 0.01;
				else
					return round(($in/$PesoSpec/1000),2);
				}
			else
				return round(($in/$PesoSpec/1000),0);
			//return round(($in/$PesoSpec/1000),2);
		break;
		case "Litri":
			$arrotondato=round(($in/$PesoSpec),2);
			if($arrotondato<1){
				if($arrotondato==0)
					return 0.01;
				else
					return round(($in/$PesoSpec),2);
				}
			else
				return round(($in/$PesoSpec),0);
			//return round(($in/$PesoSpec),2);
		break;
		case "Kg":
			return "Kg";
		break;
	} //}}}
}


function SfondoRegistro($XStart,$YStart,&$PdfBuffer) {
	$PdfBuffer->Image("../__scripts/Registro.png",$XStart,$YStart,203,79,"PNG");
	}
function SfondoRegistroInt($XStart,$YStart,&$PdfBuffer) {
	$PdfBuffer->Image("../__scripts/RegistroInt.png",$XStart,$YStart,203,79,"PNG");
	}
function DatiRegistro(&$Recordset,$x,$y,&$PdfBuffer, $Workmode, $RegPreferences, $MODULO_SIS, $LDP) {
//{{{ 
	global $SOGER,$FEDIT;
	# carico/scarico
	if($Recordset["TIPO"]=="S") {
		$PdfBuffer->SetXY($x+20,$y+1);
		$PdfBuffer->MultiCell(5,5,"X",0);
		} 
	else {
		if($Workmode=='trasportatore' && $RegPreferences["core_usersreg_trasp_doublecheck"]=="1"){
			$PdfBuffer->SetXY($x+20,$y+1);
			$PdfBuffer->MultiCell(5,5,"X",0);
			}
		$PdfBuffer->SetXY($x+20,$y+10);
		$PdfBuffer->MultiCell(5,5,"X",0);
		}
	# data movimento
	if(!is_null($Recordset["DTMOV"]) && $Recordset["DTMOV"]!="0000-00-00") {
		$PdfBuffer->SetXY($x+6,$y+18);
		$PdfBuffer->MultiCell(20,5,date("d/m/Y",strtotime($Recordset["DTMOV"])),0);
		$DTMOV = $Recordset["DTMOV"];
		}
	else
		$DTMOV = date('Y-m-d');

	# numero movimento
	$PdfBuffer->SetXY($x+6,$y+27);
	$PdfBuffer->MultiCell(20,5,$Recordset["NMOV"],0);
	# numero formulario
	if(!is_null($Recordset["NFORM"])) {
		$PdfBuffer->SetXY($x+5,$y+44);
		$PdfBuffer->MultiCell(20,5,$Recordset["NFORM"],0);
	}
	# data formulario
	if($Recordset["DTFORM"]!="0000-00-00" && !is_null($Recordset["DTFORM"]) && !is_null($Recordset["NFORM"])) {
		$PdfBuffer->SetXY($x+6,$y+52);
		$PdfBuffer->MultiCell(20,5,date("d/m/Y",strtotime($Recordset["DTFORM"])),0);
		}
	# rif movimenti di carico
	if(!is_null($Recordset["FKErifCarico"])) {
		$maxChar=28;
		if(strlen($Recordset["FKErifCarico"])<=$maxChar){
			$PdfBuffer->SetXY($x+4,$y+69);
			$otherC = '';
			$PdfBuffer->MultiCell(22,5,$Recordset["FKErifCarico"],0,"L");		
			}
		else{
			$PdfBuffer->SetXY($x+4,$y+69);
			$otherC = $Recordset["FKErifCarico"];
			$PdfBuffer->MultiCell(22,5,"Vedi annotazioni",0,"L");
			}
		}	
	else
		$otherC = '';

	# codice cer
	$PdfBuffer->SetXY($x+37,$y+6);
	if($RegPreferences["core_usersCER_AXT"]=="1" && $Recordset["pericoloso"]==1)
		$PdfBuffer->MultiCell(30,5,$Recordset["COD_CER"]."*",0);
	else
		$PdfBuffer->MultiCell(30,5,$Recordset["COD_CER"],0);
	# descrizione rifiuto
	$PdfBuffer->SetXY($x+30,$y+23);
	$PdfBuffer->MultiCell(45,4,$Recordset["descrizione"],0,"L");	
	# stato fisico
	$PdfBuffer->SetXY($x+30,$y+48);	
	$PdfBuffer->MultiCell(45,4,$Recordset["STFdes"],0,"L");
	# classi H 	
	$CLASSIH = "";
	foreach($Recordset as $k=>$v) {
		if($k[0]=="H" && $k[1]!="P" && $v=="1") {
			$CLASSIH .= $k . " ";
		}
	}
	# classi HP
	$CLASSIHP = "";
	foreach($Recordset as $k=>$v) {
		if($k[0]=="H" && $k[1]=="P" && $v=="1") {
			$CLASSIHP .= $k . " ";
		}
	}

	# COMPRESENZA HP4 / HP8
	if($RegPreferences['core_usersPRNT_HP4_HP8']==0){
		if (strpos($CLASSIHP, 'HP4') !== false && strpos($CLASSIHP, 'HP8') !== false) {
			$CLASSIHP = str_replace('HP4 ', '', $CLASSIHP);
		}
	}

	# CLASSIFICAZIONE DI PERICOLO
	$PdfBuffer->SetXY($x+30,$y+61);
	$PdfBuffer->MultiCell(70,5,$CLASSIHP,0,"L");

	# recupero / smaltimento
	# toppa ecostudio
	if(($Recordset["TIPO"]=="S" | $RegPreferences["core_usersREG_RS_CARICHI"]=="1") && !is_null($Recordset["OPRSdes"])) {
		if(strstr($Recordset["OPRSdes"],"R")) {
			$PdfBuffer->SetXY($x+65.4,$y+73.2);	
			$PdfBuffer->MultiCell(5,5,"X",0);
			$PdfBuffer->SetXY($x+84.5,$y+73.2);
			$PdfBuffer->MultiCell(10,5,$Recordset["OPRSdes"],0,"L");
		} else {
			$PdfBuffer->SetXY($x+30,$y+73.2);
			$PdfBuffer->MultiCell(5,5,"X",0);
			$PdfBuffer->SetXY($x+53,$y+73.2);
			$PdfBuffer->MultiCell(10,5,$Recordset["OPRSdes"],0,"L");
		}

	}
	# annotazioni
		
		$PdfBuffer->SetXY($x+168.5,$y+7);
		$note = "";

		# note registro scritte manualmente
		if(!is_null($Recordset["NOTER"])) {
			$note .= $Recordset["NOTER"] . "\n";
			}

		# NOTE RELATIVE ALLA VECCHIA CLASSIFICAZIONE (H)
		// fino a quando?
//		if($RegPreferences["core_usersPRNT_CLASSIH"]=="1"){
//			if(trim($CLASSIH)!="" && strtotime(date($DTMOV))>=strtotime('2015-06-01') && !$CLASSIH_PRINTED){
//				$note.="Classificazione secondo Direttiva 2008/98/CE: ".$CLASSIH."\n";
//				}
//			}
		
		if(!is_null($Recordset["NOTE_RIFIUTO"]))
			$note .= $Recordset["NOTE_RIFIUTO"]."\n";
		
		# commerciante
		if(!is_null($Recordset["INTdes"]) AND $Recordset["INTcommerciante"]==1 AND $Recordset["esclusoFIR"]==0) {
			$note.="COMMERCIANTE: \n";
			$note.=$Recordset["INTdes"]." - C.F.: ".$Recordset["INTcfisc"]."\n";
			$note.=$Recordset["INTind"]. " - " . $Recordset["COMintdes"] . " (" . $Recordset["COMintPR"] . ")\n";
			}

		# carichi di riferimento
		if(!is_null($Recordset["N_ANNEX_VII"]) AND trim($Recordset["N_ANNEX_VII"])!=''){
			$note .="Allegato VII n�: " . $Recordset["N_ANNEX_VII"] . "\n";
			}

		# numero di lotto
		if(!is_null($Recordset["lotto"]) AND trim($Recordset["lotto"])!=''){
			$note .="Lotto numero: " . $Recordset["lotto"] . "\n";
			}

		# carichi di riferimento
		if($otherC!=""){
			$note .="Riferimenti di carico: " . $otherC . "\n";
			}

		# peso a destino
		if(($Recordset["VER_DESTINO"]=="1" OR $RegPreferences['core_usersREG_PS_DEST']=="1") && !is_null($Recordset["PS_DESTINO"]) && trim($Recordset["PS_DESTINO"])!="" && (($Recordset["TIPO"]=="S" && $Workmode=="produttore") ) ) {
			$note .="Peso a destino: " . number_format_unlimited_precision($Recordset["PS_DESTINO"])." Kg\n";
			}
		
		# nome cer
		if($RegPreferences["core_usersREG_NOMECER"]=="1") {
			$note .="Nome CER: ".$Recordset["NomeCER"]."\n";
			}
		
		# rifiuti da manutenzione
		if($Recordset["RifDaManutenzione"]=='1' AND $Recordset["TIPO"]=="S" AND $Workmode=="destinatario"){
			$note .="Rifiuto da attivit� di manutenzione delle infrastrutture (Art. 230 D.Lgs. 152/2006)";
			}

		# ID della Scheda SISTRI Produttore
		if($Recordset["DTMOV"]>'2014-02-28' && $Workmode=="produttore" && $MODULO_SIS=='1' && $RegPreferences["core_usersREG_IDSIS_SCHEDA"]=='1' && $Recordset["TIPO"]=="S" && !is_null($Recordset['idSIS_scheda']) && trim($Recordset['idSIS_scheda'])!='' ){
			if($note!="") $note .= "\n";
			$note .="Generata Scheda SISTRI Produttore id ".$Recordset['idSIS_scheda'];
			if(!is_null($Recordset["dataSchedaSistri_invio"]) && trim($Recordset["dataSchedaSistri_invio"])!=''){
				$DataInvioScheda = date("d/m/Y",strtotime($Recordset["dataSchedaSistri_invio"]));
				$note .=" del ".$DataInvioScheda;
				}
			}
		
		# Causale annullamento in Sistri
		if($Recordset["ANN_registrazione"]=='1' && $RegPreferences["core_usersREG_IDSIS_SCHEDA"]=='1'){
			if($note!="") $note .= "\n";
			$note .="Registrazione cronologica SISTRI ".$Recordset['idSIS']." annullata. Motivazione: ".addslashes($Recordset["ANN_registrazione_causale"]);
			}
		if($Recordset["ANN_scheda"]=='1' && $RegPreferences["core_usersREG_IDSIS_SCHEDA"]=='1'){
			if($note!="") $note .= "\n";
			$note .="Scheda SISTRI ".$Recordset['idSIS_scheda']." annullata. Motivazione: ".addslashes($Recordset["ANN_scheda_causale"]);
			}

		# Modulo LDP: FIR di riferimento del registro destinatario
		if($LDP!='')
			$note.=$LDP;

		$PdfBuffer->MultiCell(35,4,$note,0,"L");
	
	
	# produttore
	if($RegPreferences["core_usersREG_DIS_PROD"]=="0") {
		$produttore = $Recordset["PRdes"] . "\n";
		$produttore .= "Impianto di " . $Recordset["IMdes"] . " - " . $Recordset["COMdes"];
		// se produttore estero, stampo nazione
		if($Recordset["COMproItaliano"]==1)
			$produttore .= " (" . $Recordset["COMproPR"] . ")";
		else
			$produttore .= " (" . $Recordset["COMproDesProv"] . ")";
		$PdfBuffer->SetXY($x+100,$y+11);
		$PdfBuffer->MultiCell(65,4,$produttore,0,"L");
		}


	# intermediario
	if(!is_null($Recordset["INTdes"]) AND $Recordset["INTcommerciante"]==0 AND $Recordset["esclusoFIR"]==0) {
		$PdfBuffer->SetXY($x+114,$y+40.5);
		$PdfBuffer->MultiCell(45,4,$Recordset["INTdes"],0,"L");
		$PdfBuffer->SetXY($x+105,$y+69.5);
		$PdfBuffer->MultiCell(45,4,$Recordset["INTcfisc"],0,"L");
		$sede = $Recordset["INTind"] . " - " . $Recordset["INTCAP"] . " - " . $Recordset["COMintdes"];
		// se intermediario estero, stampo nazione
		if($Recordset["COMintItaliano"]==1)
			$sede .= " (" . $Recordset["COMintPR"] . ")";
		else
			$sede .= " (" . $Recordset["COMintDesProv"] . ")";
		$PdfBuffer->SetXY($x+105,$y+57);
		$PdfBuffer->MultiCell(60,4,$sede,0,"L");

		if(!is_null($Recordset["INTaut"])) {
			$PdfBuffer->SetXY($x+115,$y+74);
			$aut = $Recordset["INTaut"] . " del " . $Recordset["INTrilascio"];
			$PdfBuffer->MultiCell(50,4,$aut,0,"L");	
		}
	}

	# quantita

	if($Recordset["peso_spec"]=="0") 
		$Recordset["peso_spec"]="1";

	switch($Recordset["Umisura"]) { #
		case "1":	# scheda rifiuto in KG
			
			switch($RegPreferences["core_usersID_UMIS"]){ #unit� misura stampa registro
				case "garbage": # garbage = id 0, ogni riga del registro avr� l'unit� di misura della scheda rifiuto a cui fa riferimento.
				default:
				case "1": #kg
					$PdfBuffer->SetXY($x+80,$y+19);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L");
					break;

				case "2": #litri
					$PdfBuffer->SetXY($x+80,$y+36);
					$Q = KgQConvert($Recordset["quantita"],"Litri",$Recordset["peso_spec"]);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Q),0,"L");
					break;

				case "3": #mc
					$PdfBuffer->SetXY($x+80,$y+53);
					$Q = KgQConvert($Recordset["quantita"],"M3",$Recordset["peso_spec"]);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Q),0,"L"); 
					break;
				}

			if($RegPreferences["core_usersUMIS_RIF"]==1 && $Recordset["Umisura"] != $RegPreferences["core_usersID_UMIS"]){
				$PdfBuffer->SetXY($x+80,$y+19);
				$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L");		
				}

		break;
		
		case "2":	# scheda rifiuto in LITRI
			switch($RegPreferences["core_usersID_UMIS"]){ #unit� misura stampa registro
			
				case "1": #kg
					$PdfBuffer->SetXY($x+80,$y+19);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]*$Recordset["peso_spec"]),0,"L");
					break;

				case "garbage":
				default:
				case "2": #litri
					$PdfBuffer->SetXY($x+80,$y+36);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L"); 					
					break;

				case "3": #mc
					$PdfBuffer->SetXY($x+80,$y+53);
					if($Recordset["quantita"]!="0") {
						$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]/1000),0,"L");
					} else {
						$PdfBuffer->MultiCell(15,4,"0",0,"L");	
					}
					break;

				}

			if($RegPreferences["core_usersUMIS_RIF"]==1 && $Recordset["Umisura"] != $RegPreferences["core_usersID_UMIS"]){
					$PdfBuffer->SetXY($x+80,$y+36);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L"); 					
				}

		break;
		
		case "3":	# scheda rifiuto in METRI CUBI

			switch($RegPreferences["core_usersID_UMIS"]){ #unit� misura stampa registro
				case "1": #kg
					$PdfBuffer->SetXY($x+80,$y+19);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]*$Recordset["peso_spec"]*1000) ,0,"L");
					break;

				case "2": #litri
					$PdfBuffer->SetXY($x+80,$y+36);
					if($Recordset["quantita"]!="0") {
						$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]*1000),0,"L");
					} else {
						$PdfBuffer->MultiCell(15,4,"0",0,"L");	
					}			
					break;
				
				case "garbage":
				default:
				case "3": #mc
					$PdfBuffer->SetXY($x+80,$y+53);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L"); 
					break;

				}

			if($RegPreferences["core_usersUMIS_RIF"]==1 && $Recordset["Umisura"] != $RegPreferences["core_usersID_UMIS"]){
					$PdfBuffer->SetXY($x+80,$y+53);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L"); 
				}

		break;
		
	}
	//}}}
}



function DatiRegistroInt(&$Recordset,$x,$y,&$PdfBuffer, $Workmode, $RegPreferences, $MODULO_SIS) {
//{{{ 
	global $SOGER,$FEDIT;

	# data movimento
	if(!is_null($Recordset["DTMOV"]) && $Recordset["DTMOV"]!="0000-00-00") {
		$PdfBuffer->SetXY($x+7,$y+13);
		$PdfBuffer->MultiCell(20,5,date("d/m/Y",strtotime($Recordset["DTMOV"])),0);
	}

	# numero movimento
	$PdfBuffer->SetXY($x+8,$y+21);
	$PdfBuffer->MultiCell(20,5,"Mov. ". $Recordset["NMOV"],0);
	
	# numero formulario
	if($Recordset["NFORM"]) {
		$PdfBuffer->SetXY($x+5,$y+33);
		$PdfBuffer->MultiCell(25,5,$Recordset["NFORM"],0);
	}
	
	# data formulario
	if(!is_null($Recordset["DTFORM"]) && $Recordset["DTFORM"]!="0000-00-00") {
		$PdfBuffer->SetXY($x+7,$y+41);
		$PdfBuffer->MultiCell(20,5,date("d/m/Y",strtotime($Recordset["DTFORM"])),0);
	}	
	
	
	# codice cer
	$PdfBuffer->SetXY($x+47,$y+13);
	if($RegPreferences["core_usersCER_AXT"]=="1" && $Recordset["pericoloso"]==1)
		$PdfBuffer->MultiCell(30,5,$Recordset["COD_CER"]."*",0);
	else
		$PdfBuffer->MultiCell(30,5,$Recordset["COD_CER"],0);
	
	
	# descrizione rifiuto
	$PdfBuffer->SetXY($x+30,$y+21);
	$PdfBuffer->MultiCell(45,4,$Recordset["descrizione"],0,"L");	

	# stato fisico
	$PdfBuffer->SetXY($x+30,$y+41);	
	$PdfBuffer->MultiCell(45,4,$Recordset["STFdes"],0,"L");
	# classi H 	
	$CLASSIH = "";
	foreach($Recordset as $k=>$v) {
		if($k[0]=="H" && $v=="1") {
			$CLASSIH .= $k . " ";
		}
	}

	# classi HP
	$CLASSIHP = "";
	foreach($Recordset as $k=>$v) {
		if($k[0]=="H" && $k[1]=="P" && $v=="1") {
			$CLASSIHP .= $k . " ";
		}
	}

	# COMPRESENZA HP4 / HP8
	if($RegPreferences['core_usersPRNT_HP4_HP8']==0){
		if (strpos($CLASSIHP, 'HP4') !== false && strpos($CLASSIHP, 'HP8') !== false) {
			$CLASSIHP = str_replace('HP4 ', '', $CLASSIHP);
		}
	}

	# CLASSIFICAZIONE DI PERICOLO
	$PdfBuffer->SetXY($x+30,$y+61);
	$PdfBuffer->MultiCell(70,5,$CLASSIHP,0,"L");

	# recupero / smaltimento
	if(!is_null($Recordset["OPRSdes"])) {
		if(strstr($Recordset["OPRSdes"],"R")) {
			$PdfBuffer->SetXY($x+27.5,$y+73.5);	
			$PdfBuffer->MultiCell(5,5,"X",0);
			$PdfBuffer->SetXY($x+60,$y+73.5);
			$PdfBuffer->MultiCell(10,5,$Recordset["OPRSdes"],0,"L");
		} else {
			$PdfBuffer->SetXY($x+27.5,$y+69.5);
			$PdfBuffer->MultiCell(5,5,"X",0);
			$PdfBuffer->SetXY($x+60,$y+69.5);
			$PdfBuffer->MultiCell(10,5,$Recordset["OPRSdes"],0,"L");
		}

	}

	# produttore
	if($RegPreferences["core_usersREG_DIS_PROD"]=="0") {
		$PdfBuffer->SetXY($x+90,$y+9);
		$PdfBuffer->MultiCell(50,4,$Recordset["PRdes"],0,"L");
		$PdfBuffer->SetXY($x+93,$y+17);
		$PdfBuffer->MultiCell(40,4,$Recordset["PRCF"],0,"L");
		$PdfBuffer->SetXY($x+90,$y+25);
		$PdfBuffer->MultiCell(50,4,$Recordset["IMPRdes"] . " - " . $Recordset["COMPRdes"] . " (" . $Recordset["COMPRPR"] . ")",0,"L");
		}

	# destinatario
	$PdfBuffer->SetXY($x+149,$y+9);
	$PdfBuffer->MultiCell(50,4,$Recordset["DEdes"],0,"L");
	$PdfBuffer->SetXY($x+152,$y+17);
	$PdfBuffer->MultiCell(40,4,$Recordset["DECF"],0,"L");
	$PdfBuffer->SetXY($x+149,$y+25);
	$PdfBuffer->MultiCell(50,4,$Recordset["IMDEdes"] . " - " . $Recordset["COMDEdes"] . " (" . $Recordset["COMDEPR"] . ")",0,"L");

	# trasportatore
	$PdfBuffer->SetXY($x+90,$y+45);
	$PdfBuffer->MultiCell(50,4,$Recordset["TRdes"],0,"L");
	$PdfBuffer->SetXY($x+93,$y+58);
	$PdfBuffer->MultiCell(40,4,$Recordset["TRCF"],0,"L");
	$PdfBuffer->SetXY($x+90,$y+66);
	$PdfBuffer->MultiCell(50,4,$Recordset["IMTRdes"] . " - " . $Recordset["COMTRdes"] . " (" . $Recordset["COMTRPR"] . ")",0,"L");


	# annotazioni
	$PdfBuffer->SetXY($x+145,$y+45);
	$note = "";
	
	if(!is_null($Recordset["PS_DESTINO"]) && trim($Recordset["PS_DESTINO"])!="")
		$note .="Peso a destino: " . number_format_unlimited_precision($Recordset["PS_DESTINO"])." Kg \n";

	if(!is_null($Recordset["NOTER"]))
		$note .= $Recordset["NOTER"]."\n";
	
	if(!is_null($Recordset["NOTE_RIFIUTO"]))
		$note .= $Recordset["NOTE_RIFIUTO"]."\n";

	if($RegPreferences["core_usersREG_NOMECER"]=="1")
		$note .="Nome CER: ".$Recordset["NomeCER"]."\n";

	$PdfBuffer->MultiCell(55,4,$note,0,"L");



	# quantita

	if($Recordset["peso_spec"]=="0") 
		$Recordset["peso_spec"]="1";

	switch($Recordset["Umisura"]) { #
		case "1":	# scheda rifiuto in KG
			
			switch($RegPreferences["core_usersID_UMIS_INT"]){ #unit� misura stampa registro
				case "garbage": # garbage = id 0, ogni riga del registro avr� l'unit� di misura della scheda rifiuto a cui fa riferimento.
				default:
				case "1": #kg
					$PdfBuffer->SetXY($x+75,$y+17);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L");
					break;

				case "2": #litri
					$PdfBuffer->SetXY($x+75,$y+32);
					$Q = KgQConvert($Recordset["quantita"],"Litri",$Recordset["peso_spec"]);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Q),0,"L");
					break;
				}
	
			if($RegPreferences["core_usersUMIS_RIF"]==1 && ($Recordset["Umisura"] != $RegPreferences["core_usersID_UMIS_INT"])){
				$PdfBuffer->SetXY($x+75,$y+17);
				$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L");		
				}

		break;
		
		case "2":	# scheda rifiuto in LITRI
			switch($RegPreferences["core_usersID_UMIS_INT"]){ #unit� misura stampa registro
			
				case "1": #kg
					$PdfBuffer->SetXY($x+75,$y+17);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]*$Recordset["peso_spec"]),0,"L");
					break;

				case "garbage":
				default:
				case "2": #litri
					$PdfBuffer->SetXY($x+75,$y+32);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L"); 					
					break;

				}

			if($RegPreferences["core_usersUMIS_RIF"]==1 && $Recordset["Umisura"] != $RegPreferences["core_usersID_UMIS_INT"]){
					$PdfBuffer->SetXY($x+75,$y+32);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L"); 					
				}

		break;
		
		case "3":	# scheda rifiuto in METRI CUBI

			switch($RegPreferences["core_usersID_UMIS_INT"]){ #unit� misura stampa registro
				case "1": #kg
					$PdfBuffer->SetXY($x+75,$y+17);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]*$Recordset["peso_spec"]*1000),0,"L");
					break;

				case "garbage":
				default:
				case "2": #litri
					$PdfBuffer->SetXY($x+75,$y+32);
					if($Recordset["quantita"]!="0") {
						$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]*1000),0,"L");
					} else {
						$PdfBuffer->MultiCell(15,4,"0",0,"L");	
					}			
					break;

				}

			if($RegPreferences["core_usersUMIS_RIF"]==1 && $Recordset["Umisura"] != $RegPreferences["core_usersID_UMIS_INT"]){
					$PdfBuffer->SetXY($x+75,$y+32);
					if($Recordset["quantita"]!="0") {
						$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]*1000),0,"L");
					} else {
						$PdfBuffer->MultiCell(15,4,"0",0,"L");	
					}			
				}

		break;
		
	}

}


?>