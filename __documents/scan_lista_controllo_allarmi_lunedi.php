<?php
/*
error_reporting(E_ALL);
ini_set("display_errors", 1);
*/

//session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__libs/zip3.php");
require_once("../__libs/fpdf.php");
require_once("../__classes/SogerMailer.php");


## CONNECTION TO DATABASE
$FEDIT=new DbLink();
$FEDIT->DbConn('soger'.date('Y'));
$AnnoDB		= date('Y');


## RETRIVE USER TO GENERATE LIST
$SQL="SELECT DISTINCT ID_USR, mail FROM user_promemoria WHERE id_prom=3 AND id_fprom=1 AND mail<>'' AND mail IS NOT NULL;";
$FEDIT->SdbRead($SQL,"DbRecordSetUserData",true,false);


## CYCLE USER
for($i=0;$i<count(@$FEDIT->DbRecordSetUserData);$i++){

	$USER=$FEDIT->DbRecordSetUserData[$i]['ID_USR'];
	$MAIL=$FEDIT->DbRecordSetUserData[$i]['mail'];

	# USER SETTINGS
	$SQL="SELECT nome, cognome, ID_IMP, AUT_SCAD_LOCK, FDA_LOCK, CONTO_TERZI_CHECK, CONTO_TERZI_LOCK, CHECK_DESTINATARIO, CHECK_TRASPORTATORE, CHECK_MOV_ORDER, CHECK_DOUBLE_FIR, ck_RIT_FORM, ck_RIT_FORMgg, ck_AUT, ck_AUTgg, ck_ANALISI, ck_ANALISIgg, ck_MOV_SISTRI, ck_MOV_SISTRIgg, ck_CNTR, ck_DEP_TEMP, ck_DEP_TEMPgg, q_limite, q_limite_p, q_limite_t, ID_GDEP, GGedit_produttore, GGedit_trasportatore, GGedit_destinatario, GGedit_intermediario FROM core_users WHERE ID_USR=".$USER.";";
	$FEDIT->SdbRead($SQL,"DbRecordSetUserSetting",true,false);
	$ID_IMP = $FEDIT->DbRecordSetUserSetting[0]['ID_IMP'];

        # MOV TABLE AND MODULES
	$SQL="SELECT REG_IND, MODULO_SIS, MODULO_FDA FROM core_impianti WHERE ID_IMP='".$ID_IMP."';";
	$FEDIT->SdbRead($SQL,"DbRecordSetREG_IND",true,false);
	if($FEDIT->DbRecordSetREG_IND[0]['REG_IND']==1) $TableName="user_movimenti"; else $TableName="user_movimenti_fiscalizzati";
	$MODULO_SIS = $FEDIT->DbRecordSetREG_IND[0]['MODULO_SIS'];
        $MODULO_FDA = $FEDIT->DbRecordSetREG_IND[0]['MODULO_FDA'];

	if($FEDIT->DbRecordSetUserSetting[0]["AUT_SCAD_LOCK"]==1)		$LkAutScadute='attivo';		else $LkAutScadute='non attivo';
        if($FEDIT->DbRecordSetUserSetting[0]["FDA_LOCK"]==1 && $MODULO_FDA==1)	$LkFDA='attivo';		else $LkFDA='non attivo';
	if($FEDIT->DbRecordSetUserSetting[0]["CONTO_TERZI_CHECK"]==1)		$CkContoTerzi='attivo';		else $CkContoTerzi='non attivo';
	if($FEDIT->DbRecordSetUserSetting[0]["CONTO_TERZI_LOCK"]==1)		$LkContoTerzi='attivo';		else $LkContoTerzi='non attivo';
	if($FEDIT->DbRecordSetUserSetting[0]["CHECK_DESTINATARIO"]==1)		$CkDestinatario='attivo';	else $CkDestinatario='non attivo';
	if($FEDIT->DbRecordSetUserSetting[0]["CHECK_TRASPORTATORE"]==1)		$CkTrasportatore='attivo';	else $CkTrasportatore='non attivo';
	if($FEDIT->DbRecordSetUserSetting[0]["CHECK_MOV_ORDER"]==1)		$CkMovOrder='attivo';		else $CkMovOrder='non attivo';
	if($FEDIT->DbRecordSetUserSetting[0]["CHECK_DOUBLE_FIR"]==1)		$CkDoubleFir='attivo';		else $CkDoubleFir='non attivo';

	$SQL="SELECT description FROM lov_gestione_deposito WHERE ID_GDEP=".$FEDIT->DbRecordSetUserSetting[0]["ID_GDEP"].";";
	$FEDIT->SDBRead($SQL,"DbRecordSet",true,false);
	$GestioneDep=$FEDIT->DbRecordSet[0]['description'];

	# CONTROLLI
	$CkRitornoFormulario				= $FEDIT->DbRecordSetUserSetting[0]["ck_RIT_FORM"];
	$intervalloForm						= $FEDIT->DbRecordSetUserSetting[0]["ck_RIT_FORMgg"];
	$CkAutorizzazioni					= $FEDIT->DbRecordSetUserSetting[0]["ck_AUT"];
	$intervallo							= $FEDIT->DbRecordSetUserSetting[0]["ck_AUTgg"];
	$CkAnalisi							= $FEDIT->DbRecordSetUserSetting[0]["ck_ANALISI"];
	$intervalloAnalisi					= $FEDIT->DbRecordSetUserSetting[0]["ck_ANALISIgg"];
	$CkMOV_SISTRI						= $FEDIT->DbRecordSetUserSetting[0]["ck_MOV_SISTRI"];
	$intervalloMOV_SISTRI				= $FEDIT->DbRecordSetUserSetting[0]["ck_MOV_SISTRIgg"];
	$CkContributi						= $FEDIT->DbRecordSetUserSetting[0]["ck_CNTR"];
	$CkDepositoTemporaneo				= $FEDIT->DbRecordSetUserSetting[0]["ck_DEP_TEMP"];
	$CkDepositoTemporaneogg				= $FEDIT->DbRecordSetUserSetting[0]["ck_DEP_TEMPgg"];
	$CkDepositoTemporaneoM3				= $FEDIT->DbRecordSetUserSetting[0]["q_limite"];
	$CkDepositoTemporaneoM3pericolosi	= $FEDIT->DbRecordSetUserSetting[0]["q_limite_p"];
	$CkDepositoTemporaneoM3totali		= $FEDIT->DbRecordSetUserSetting[0]["q_limite_t"];

        # GIORNI MODIFICA MOVIMENTI
        $GGedit_produttore                      = $FEDIT->DbRecordSetUserSetting[0]["GGedit_produttore"];
        $GGedit_trasportatore                   = $FEDIT->DbRecordSetUserSetting[0]["GGedit_trasportatore"];
        $GGedit_destinatario                    = $FEDIT->DbRecordSetUserSetting[0]["GGedit_destinatario"];
        $GGedit_intermediario                   = $FEDIT->DbRecordSetUserSetting[0]["GGedit_intermediario"];

	# workmode
	$SQL="SELECT produttore, trasportatore, destinatario, intermediario FROM core_impianti WHERE ID_IMP='".$ID_IMP."';";
	$FEDIT->SdbRead($SQL,"DbRecordSetWM",true,false);
	$WM_global = Array();

	# reg. produttore
	if($FEDIT->DbRecordSetWM[0]['produttore']=='1'){
		$WM				= 'produttore';
		array_push($WM_global, $WM);
		}
	# reg. trasportatore
	if($FEDIT->DbRecordSetWM[0]['trasportatore']=='1'){
		$WM				= 'trasportatore';
		array_push($WM_global, $WM);
		}
	# reg. destinatario
	if($FEDIT->DbRecordSetWM[0]['destinatario']=='1'){
		$WM				= 'destinatario';
		array_push($WM_global, $WM);
		}
	# reg. intermediario
	if($FEDIT->DbRecordSetWM[0]['intermediario']=='1'){
		$WM				= 'intermediario';
		array_push($WM_global, $WM);
		}

	for($w=0;$w<count($WM_global);$w++){

		$WORKMODE	= $WM_global[$w];

		$LISTA1		= array();
		$LISTA2		= array();
		$LISTA3		= array();
		$OKmsg		= "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare";

		// doc name
		$DcName	= "Lista_di_controllo_allarmi_".$WM_global[$w]."_al_".date('d-m-Y');

		// pagination
		$orientation="P";
		$um="mm";
		$Format = array(210,297);
		$ZeroMargin = true;
		$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin,$DcName);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',12);

		$Ypos		= 30;
		$Xpos		= 10;
		$SmallFont	= 6; // -> Testi degli allarmi
		$MediumFont	= 7; // -> Riferimenti normativi
		$LargeFont	= 12;// -> Nome del controllo

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);

		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"Gestione rifiuti","TLR","C", 1);
		$Ypos+=$LargeFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"Funzione di Controllo - Lista di controllo allarmi","BLR","C", 1);


		## INTESTAZIONE AZIENDA
		$SQL ="SELECT core_impianti.description as azienda, indirizzo, lov_comuni_istat.description AS comune, lov_comuni_istat.shdes_prov as provincia ";
		$SQL.="FROM core_impianti JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM=core_impianti.ID_COM ";
		$SQL.="WHERE core_impianti.ID_IMP='".$ID_IMP."';";
		$FEDIT->SDBRead($SQL,"DbRecordSet",true,false);

		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);
		$Ypos+=12;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,12,$FEDIT->DbRecordSet[0]['azienda'],"TLR","C");
		$Ypos+=12;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"Stabilimento di ".$FEDIT->DbRecordSet[0]['indirizzo']." - ".$FEDIT->DbRecordSet[0]['comune']." (".$FEDIT->DbRecordSet[0]['provincia'].")","LR","C");
		$Ypos+=12;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"Utente ".$FEDIT->DbRecordSetUserSetting[0]["nome"]. " " .$FEDIT->DbRecordSetUserSetting[0]["cognome"],"BLR","C");


		## LIVELLO DI SICUREZZA

		$Ypos+=30;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"Livello di sicurezza di gestione dei rifiuti","TLR","C", 1);
		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);

		$Ypos+=12;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Blocco salvataggio movimentazione con soggetti con autorizzazione scadute/assente","BLR","L");
		$Xpos+=160;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$LkAutScadute,"BLR","C");

                $Ypos+=10;
		$Xpos=10;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Blocco salvataggio movimentazione la cui verifica targhe con Albo ha dato esito negativo","BLR","L");
		$Xpos+=160;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$LkFDA,"BLR","C");

		$Ypos+=10;
		$Xpos=10;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Blocco salvataggio movimentazione con trasportatori con abilitazione conto terzi assente","BLR","L");
		$Xpos+=160;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$LkContoTerzi,"BLR","C");

		$Ypos+=10;
		$Xpos=10;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Allarme movimentazione con trasportatori con abilitazione conto terzi assente","BLR","L");
		$Xpos+=160;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$CkContoTerzi,"BLR","C");

		$Ypos+=10;
		$Xpos=10;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Blocco salvataggio formulari senza destinatario","BLR","L");
		$Xpos+=160;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$CkDestinatario,"BLR","C");

		$Ypos+=10;
		$Xpos=10;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Blocco salvataggio formulari senza trasportatore","BLR","L");
		$Xpos+=160;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$CkTrasportatore,"BLR","C");

		$Ypos+=10;
		$Xpos=10;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Blocco salvataggio movimenti fuori ordine cronologico","BLR","L");
		$Xpos+=160;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$CkMovOrder,"BLR","C");

		$Ypos+=10;
		$Xpos=10;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Blocco salvataggio formulari gi� registrati","BLR","L");
		$Xpos+=160;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$CkDoubleFir,"BLR","C");

		$Ypos+=10;
		$Xpos=10;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Modalit� di gestione del deposito","BLR","L");
		$Xpos+=160;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$GestioneDep,"BLR","C");
		$Xpos=10;

                #
                #	GIORNI MODIFICA MOVIMENTI
                #
                $Ypos+=10;
                CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
                if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
                $Xpos=10;
                $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
                $FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Registro ".$WORKMODE." - Movimenti modificabili per giorni","BLR","L");
                $Xpos+=160;
                $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
                $GG = ${"GGedit_".$WORKMODE};
                if($WORKMODE=="produttore") $limit=14; else $limit=2;
                if($GG>$limit){
                    $FEDIT->FGE_PdfBuffer->SetFillColor(255,0,0);
                    $FEDIT->FGE_PdfBuffer->MultiCell(30,10,$GG,"BLR","C",1);
                    $avviso = "Attenzione, i giorni di modificabilit� della movimentazione indicati in configurazione superano le disposizioni dell'articolo 190 del D.Lgs. 152/2006 sulla corretta tenuta del registro di carico e scarico rifiuti.";
                    $Ypos+=10;
                    CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
                    if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
                    $Xpos=10;
                    $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
                    $FEDIT->FGE_PdfBuffer->MultiCell(190,10,$avviso,"BLR","L");
                    $LISTA1['GGEDIT'][] = $avviso;
                }
                else{
                    $FEDIT->FGE_PdfBuffer->MultiCell(30,10,$GG,"BLR","C");
                    $LISTA3['GGEDIT'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
                }
                $Xpos=10;


		#
		#	GENERAZIONE AUTOMATICA REGISTRO C/S
		#
		$sql="SELECT * FROM user_promemoria WHERE ID_USR IN (SELECT ID_USR FROM core_users WHERE ID_IMP='".$ID_IMP."' AND usr<>'sintem') AND id_prom=1;";
		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
		if($FEDIT->DbRecsNum==0)
			$LISTA1['NOTIFICA_REG_CS'][] = "Attenzione, procedura non attiva.";
		else
			$LISTA3['NOTIFICA_REG_CS'][] = "La procedura � attiva.";

		#
		#	GENERAZIONE AUTOMATICA REGISTRO ADR
		#
		$sql="SELECT * FROM user_promemoria WHERE ID_USR IN (SELECT ID_USR FROM core_users WHERE ID_IMP='".$ID_IMP."' AND usr<>'sintem') AND id_prom=2;";
		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
		if($FEDIT->DbRecsNum==0)
			$LISTA1['NOTIFICA_REG_ADR'][] = "Attenzione, procedura non attiva.";
		else
			$LISTA3['NOTIFICA_REG_ADR'][] = "La procedura � attiva.";


		#
		#	RICLASSIFICAZIONE REG. 1357/2014
		#

		$sql ="SELECT descrizione, lov_cer.COD_CER ";
		$sql.="FROM user_schede_rifiuti JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
		$sql.="WHERE ".$WORKMODE."='1' AND ID_IMP='".$ID_IMP."' ";
		$sql.="AND ClassificazioneHP=0 AND approved=1 ORDER BY COD_CER";
		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

		if($FEDIT->DbRecsNum>0) {

			for($d=0;$d<count($FEDIT->DbRecordSet);$d++){
				$LISTA1['REG_1357'][] = "Il rifiuto ".$FEDIT->DbRecordSet[$d]['COD_CER']." - ".$FEDIT->DbRecordSet[$d]['descrizione']." deve essere riclassificato come disposto dal Regolamento UE 1357/2014.";
				}
			}



			#
			#	SCADENZA ANALISI
			#
			if($CkAnalisi=='1') {

				$sql  = "SELECT lov_cer.COD_CER, descrizione, CAR_NumDocumento, CAR_DataAnalisi, CAR_DataScadenzaAnalisi ";
				$sql .= " FROM user_schede_rifiuti JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
				$sql .= " WHERE (CAR_DataScadenzaAnalisi< ADDDATE(CURDATE(),$intervalloAnalisi) OR CAR_DataScadenzaAnalisi IS NULL OR CAR_DataScadenzaAnalisi='0000-00-00') ";
				$sql .= " AND ID_IMP='" . $ID_IMP . "'";
				$sql .= " AND approved=1 ";
				$sql .= " AND CAR_DataAnalisi<>'0000-00-00' AND CAR_DataAnalisi IS NOT NULL ";
				$sql .= " AND user_schede_rifiuti.".$WORKMODE."=1 ";

				$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
				if($FEDIT->DbRecsNum>0) {
					foreach($FEDIT->DbRecordSet as $k=>$dati) {

						$avviso = "CER " . $dati["COD_CER"] . " - " . $dati["descrizione"] . ":";

						$DataDocumento_array = explode("-", $dati["CAR_DataAnalisi"]);
						$DataDocumento = $DataDocumento_array[2]."/".$DataDocumento_array[1]."/".$DataDocumento_array[0];

						if($dati["CAR_DataScadenzaAnalisi"]=="0000-00-00" OR is_null($dati["CAR_DataScadenzaAnalisi"])) {
							$avviso.= " la data di fine validit� dell'analisi ".$dati["CAR_NumDocumento"]." del ".$DataDocumento." � mancante.";
							$LISTA1['ANALISI'][] = $avviso;
						} else {
							#
							# necessit� di cambiare l'allarme a seconda che l'autorizzazione sia scaduta o in scadenza!
							#
							$oggi=mktime(0, 0, 0, date('m'), date('d'), date('Y'));
							$scade=explode('-', $dati['CAR_DataScadenzaAnalisi']);
							$scadenza=mktime(0, 0, 0, $scade[1], $scade[2], $scade[0]);
							$differenza=($scadenza - $oggi)/(60*60*24);
							if($differenza>=0){
								$avviso.=" l'analisi ".$dati["CAR_NumDocumento"]." del ".$DataDocumento." � prossima al termine della sua validit�.";
								$LISTA2['ANALISI'][] = $avviso;
								}
							else{
								$avviso.=" l'analisi ".$dati["CAR_NumDocumento"]." del ".$DataDocumento." ha superato il termine della sua validit� ed � necessario ripeterla.";
								$LISTA1['ANALISI'][] = $avviso;
								}
							}
						}
					}
				else{
					$LISTA3['ANALISI'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
					}
		}

#
#	FIRMA REGISTRAZIONI CRONOLOGICHE SISTRI
#
if($WORKMODE=='produttore' AND $MODULO_SIS=='1' AND $CkMOV_SISTRI=='1'){

	$sql  = "SELECT NMOV, DTMOV, dataRegistrazioniCrono_invio, statoRegistrazioniCrono ";
	$sql .= "FROM user_movimenti_fiscalizzati JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_movimenti_fiscalizzati.ID_RIF ";
	$sql .= "WHERE (statoRegistrazioniCrono='NON FIRMATA' OR statoRegistrazioniCrono IS NULL) ";
	$sql .= "AND user_movimenti_fiscalizzati.ID_IMP='".$ID_IMP."' ";
	$sql .= "AND user_movimenti_fiscalizzati.produttore=1 AND pericoloso=1 AND DTMOV>'2014-02-28' ";
	$INTERVAL = 10 - $intervalloMOV_SISTRI;
	$sql .= "AND (CURDATE()>=DATE_ADD(DTMOV, INTERVAL ".$INTERVAL." DAY)) ";
	$sql .= "AND NMOV<>9999999 ";
	$sql .= "AND (PerRiclassificazione=0 OR TIPO='C') ";
	$sql .= "ORDER BY DTMOV, dataRegistrazioniCrono_invio, NMOV";

	$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
	if($FEDIT->DbRecsNum>0) {

		foreach($FEDIT->DbRecordSet as $k=>$dati) {
			$avviso		= '';

			$oggi		= mktime(0, 0, 0, date('m'), date('d'), date('Y'));

			if(is_null($dati['statoRegistrazioniCrono'])){
				$operazione		="trasmettere e firmare";
				$dataMovimento	= explode('-', $dati['DTMOV']);
				$giornoInvio	= mktime(0, 0, 0, $dataMovimento[1], $dataMovimento[2], $dataMovimento[0]);
				}
			else{
				$operazione		="firmare";
				$dataRegistrazioniCrono_invio	= explode('-', $dati['dataRegistrazioniCrono_invio']);
				$giornoInvio	= mktime(0, 0, 0, $dataRegistrazioniCrono_invio[1], $dataRegistrazioniCrono_invio[2], $dataRegistrazioniCrono_invio[0]);
				}

			$differenza	= 10 - ($oggi - $giornoInvio)/(60*60*24);

			if($differenza>=0){
				if($differenza<=$intervalloMOV_SISTRI)
					$LISTA2['REG_CRONO_SISTRI'][] =" Attenzione, restano ".$differenza." giorni per ".$operazione." la registrazione cronologica relativa al movimento numero ".$dati['NMOV'].".";
				}
			else{
				$LISTA1['REG_CRONO_SISTRI'][] =" Attenzione, da ".abs($differenza)." giorni � superato il termine entro il quale ".$operazione." la registrazione cronologica relativa al movimento numero ".$dati['NMOV'].".";
				}

			}
		}
	else{
		$LISTA3['REG_CRONO_SISTRI'][0] = $OKmsg;
		}

	}


#
#	FIRMA SCHEDE SISTRI
#
if($WORKMODE=='produttore' AND $MODULO_SIS=='1' AND $CkMOV_SISTRI=='1'){

	$sql  = "SELECT NMOV, DTMOV, dataSchedaSistri_invio, statoSchedaSistri ";
	$sql .= "FROM user_movimenti_fiscalizzati JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_movimenti_fiscalizzati.ID_RIF ";
	$sql .= "WHERE (statoSchedaSistri='NON FIRMATA' OR statoSchedaSistri IS NULL) ";
	$sql .= "AND user_movimenti_fiscalizzati.ID_IMP='".$ID_IMP."' AND TIPO='S' ";
	$sql .= "AND user_movimenti_fiscalizzati.produttore=1 AND pericoloso=1 AND DTMOV>'2014-02-28' ";
	$INTERVAL = 10 - $intervalloMOV_SISTRI;
	$sql .= "AND (CURDATE()>=DATE_ADD(DTMOV, INTERVAL ".$INTERVAL." DAY)) ";
	$sql .= "AND PerRiclassificazione=0 ";
	$sql .= "ORDER BY DTMOV, dataSchedaSistri_invio, NMOV";

	$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
	if($FEDIT->DbRecsNum>0) {

		foreach($FEDIT->DbRecordSet as $k=>$dati) {
			$avviso		= '';

			$oggi		= mktime(0, 0, 0, date('m'), date('d'), date('Y'));

			if(is_null($dati['statoSchedaSistri'])){
				$operazione		="trasmettere e firmare";
				$dataMovimento	= explode('-', $dati['DTMOV']);
				$giornoInvio	= mktime(0, 0, 0, $dataMovimento[1], $dataMovimento[2], $dataMovimento[0]);
				}
			else{
				$operazione		="firmare";
				$dataSchedaSistri_invio	= explode('-', $dati['dataSchedaSistri_invio']);
				$giornoInvio	= mktime(0, 0, 0, $dataSchedaSistri_invio[1], $dataSchedaSistri_invio[2], $dataSchedaSistri_invio[0]);
				}

			$differenza	= 10 - ($oggi - $giornoInvio)/(60*60*24);

			if($differenza>=0){
				if($differenza<=$intervalloMOV_SISTRI)
					$LISTA2['SCHEDE_SISTRI'][] =" Attenzione, restano ".$differenza." giorni per ".$operazione." la scheda SISTRI relativa al movimento numero ".$dati['NMOV'].".";
				}
			else{
				$LISTA1['SCHEDE_SISTRI'][] =" Attenzione, da ".abs($differenza)." giorni � superato il termine entro il quale ".$operazione." la scheda SISTRI relativa al movimento numero ".$dati['NMOV'].".";
				}

			}
		}
	else{
		$LISTA3['SCHEDE_SISTRI'][0] = $OKmsg;
		}

	}


		#
		#	Schede rifiuto in ADR (n.a.s., no 2.1.3.5.5) senza indicazione componenti pericolosi
		#
		$sql ="SELECT descrizione, lov_cer.COD_CER, lov_num_onu.nas, lov_num_onu.disposizioni_speciali ";
		$sql.="FROM user_schede_rifiuti JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
		$sql.="JOIN lov_num_onu ON lov_num_onu.ID_ONU=user_schede_rifiuti.ID_ONU ";
		$sql.="WHERE ".$WORKMODE."='1' AND ID_IMP='".$ID_IMP."' ";
		$sql.="AND adr=1 AND nas=1 AND (disposizioni_speciali LIKE '%274%' OR disposizioni_speciali LIKE '%318%') AND ADR_2_1_3_5_5=0 AND trim(ONU_per)='' AND approved=1 ORDER BY COD_CER";
		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

		if($FEDIT->DbRecsNum>0) {
			for($d=0;$d<count($FEDIT->DbRecordSet);$d++){
				$LISTA1['ADRNAS'][] = "Il rifiuto ".$FEDIT->DbRecordSet[$d]['COD_CER']." - ".$FEDIT->DbRecordSet[$d]['descrizione']." � sottoposto alla normativa ADR e gli � stato attribuito un numero ONU che richiede obbligatoriamente l'indicazione dei componenti pericolosi.";
				}
			}
		else{
			$LISTA3['ADRNAS'][0] = $OKmsg;
			}

	#
	#	Formulari senza numero di formulario
	#

	if($WORKMODE=='produttore'){
		$sql ="SELECT NMOV FROM user_movimenti_fiscalizzati ";
		$sql.="WHERE ".$WORKMODE."='1' AND ID_IMP='".$ID_IMP."' ";
		$sql.="AND PerRiclassificazione=0 AND NMOV<>9999999 AND TIPO='S' AND SenzaTrasporto=0 AND TIPO_S_INTERNO=0 AND (TRIM(NFORM)='' OR NFORM IS NULL) AND (TRIM(N_ANNEX_VII)='' OR N_ANNEX_VII IS NULL) ";
		$sql.="ORDER BY NMOV ASC ";
		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

		if($FEDIT->DbRecsNum>0) {
			for($d=0;$d<count($FEDIT->DbRecordSet);$d++){
				$LISTA1['NONFORM'][] = "Il movimento numero ".$FEDIT->DbRecordSet[$d]['NMOV']." fa riferimento ell'emissione di un formulario per il quale non � stato indicato il numero.";
				}
			}
		else{
			$LISTA3['NONFORM'][0] = $OKmsg;
			}
		}


		#
		#	ORGANIZZAZIONE DEPOSITO
		#
		$sql ="SELECT lov_cer.COD_CER, user_schede_rifiuti.FKEdisponibilita AS giacenza, lov_contenitori.portata, lov_contenitori.m_cubi, user_schede_rifiuti_deposito.NumCont, user_schede_rifiuti_deposito.AVVISO_SINGOLO, user_schede_rifiuti_deposito.MaxStock, lov_cer.cod_cer, user_schede_rifiuti.descrizione, user_schede_rifiuti.ID_UMIS ";
		$sql.="FROM user_schede_rifiuti ";
		$sql.="JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
		$sql.="RIGHT JOIN user_schede_rifiuti_deposito ON user_schede_rifiuti.ID_RIF=user_schede_rifiuti_deposito.ID_RIF ";
		$sql.="LEFT JOIN lov_contenitori ON user_schede_rifiuti_deposito.ID_CONT=lov_contenitori.ID_CONT ";
		$sql.="WHERE user_schede_rifiuti.ID_IMP='".$ID_IMP."' AND user_schede_rifiuti.FKEdisponibilita>0 ";
		$TableSpec = "user_schede_rifiuti.";
		$sql.="AND ".$TableSpec.$WORKMODE."=1 ";
		$sql.="AND ".$TableSpec."approved=1 ";
		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
		$C_PIENI=0;

		if(isset($FEDIT->DbRecordSet)){

			for($s=0;$s<count($FEDIT->DbRecordSet);$s++){

				if($FEDIT->DbRecordSet[$s]['MaxStock']>0){
					if($FEDIT->DbRecordSet[$s]['AVVISO_SINGOLO']=='1')
						$MAX=$FEDIT->DbRecordSet[$s]['MaxStock'];
					else
						$MAX=$FEDIT->DbRecordSet[$s]['MaxStock'] * $FEDIT->DbRecordSet[$s]['NumCont'];
					}
				else{
					if($FEDIT->DbRecordSet[$s]['AVVISO_SINGOLO']=='1'){
						switch($FEDIT->DbRecordSet[$s]['ID_UMIS']){
							case 1: //kg
								$MAX=$FEDIT->DbRecordSet[$s]['portata'];
								break;
							case 2: //litri
								$MAX=$FEDIT->DbRecordSet[$s]['m_cubi'] * 1000;
								break;
							case 3: //m cubi
								$MAX=$FEDIT->DbRecordSet[$s]['m_cubi'];
								break;
							}
						}
					else{
						switch($FEDIT->DbRecordSet[$s]['ID_UMIS']){
							case 1: //kg
								$MAX=$FEDIT->DbRecordSet[$s]['NumCont'] * $FEDIT->DbRecordSet[$s]['portata'];
								break;
							case 2: //litri
								$MAX=$FEDIT->DbRecordSet[$s]['NumCont'] * $FEDIT->DbRecordSet[$s]['m_cubi'] * 1000;
								break;
							case 3: //m cubi
								$MAX=$FEDIT->DbRecordSet[$s]['NumCont'] * $FEDIT->DbRecordSet[$s]['m_cubi'];
								break;
							}
						}
					}

				if($FEDIT->DbRecordSet[$s]['giacenza']>=$MAX && $MAX>0 ){
					$C_PIENI++;
					$LISTA2['ORGANIZZA_DEPOSITO'][] = $FEDIT->DbRecordSet[$s]['COD_CER']." ".$FEDIT->DbRecordSet[$s]['descrizione'].": attenzione, contenitore pieno.";
					}
				}
			}
		if($C_PIENI==0){
			$LISTA3['ORGANIZZA_DEPOSITO'][] = "Nessun contenitore risulta essere pieno.";
			}










		#
		#	AVVISO DEPOSITO TEMPORANEO (RIF SINGOLO)
		#
		if($CkDepositoTemporaneo=="1" && $WORKMODE=="produttore") {

			$sql = "SELECT giac_ini, 0 AS isGiacIniAuto,user_schede_rifiuti.originalID_RIF AS ID_RIF,lov_cer.COD_CER,lov_cer.pericoloso,NMOV,DTMOV,TIPO,quantita,lov_misure.ID_UMIS,lov_misure.description AS misura,peso_spec,descrizione,q_limite,t_limite ";
			$sql .= " FROM user_schede_rifiuti ";
			$sql .= " LEFT JOIN ".$TableName." ON user_schede_rifiuti.ID_RIF=".$TableName.".ID_RIF AND NMOV<>9999999 AND PerRiclassificazione=0";
			$sql .= " JOIN lov_misure ON user_schede_rifiuti.ID_UMIS=lov_misure.ID_UMIS";
			$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
			$sql .= " WHERE (q_limite<>'0' OR t_limite<>0)";
			$sql .= " AND user_schede_rifiuti.produttore='1'";
			$sql .= " AND user_schede_rifiuti.ID_IMP='" . $ID_IMP . "'";
			$sql .= " AND ".$TableName.".ID_IMP='" . $ID_IMP . "'";
			$sql .= " AND user_schede_rifiuti.FKEdisponibilita>0";

			$sql2 = "SELECT giac_ini, 1 AS isGiacIniAuto,user_schede_rifiuti.originalID_RIF AS ID_RIF,lov_cer.COD_CER,lov_cer.pericoloso, NMOV,DTMOV,TIPO,quantita,lov_misure.ID_UMIS,lov_misure.description AS misura,peso_spec,descrizione,q_limite,t_limite ";
			$sql2 .= " FROM user_schede_rifiuti ";
			$sql2 .= " JOIN user_movimenti_giacenze_iniziali ON user_schede_rifiuti.ID_RIF=user_movimenti_giacenze_iniziali.ID_RIF";
			$sql2 .= " JOIN lov_misure ON user_schede_rifiuti.ID_UMIS=lov_misure.ID_UMIS";
			$sql2 .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
			$sql2 .= " WHERE (q_limite<>'0' OR t_limite<>0)";
			$sql2 .= " AND user_schede_rifiuti.produttore='1'";
			$sql2 .= " AND user_schede_rifiuti.ID_IMP='" . $ID_IMP . "'";
			$sql2 .= " AND user_movimenti_giacenze_iniziali.ID_IMP='" . $ID_IMP . "'";
			$sql2 .= " AND user_schede_rifiuti.FKEdisponibilita>0";


			$sqlUnion = "(".$sql.") UNION (".$sql2.")";
			$sqlUnion.= " ORDER BY ID_RIF ASC, isGiacIniAuto DESC, NMOV ASC, DTMOV ASC";

			//echo "1: ".$sql."<hr>";

			$FEDIT->SDBRead($sqlUnion,"DbRecordSet",true,false);

			$DetTmp = array();
			if($FEDIT->DbRecsNum>0) {
				$RifBuffer = "";
				foreach($FEDIT->DbRecordSet as $k=>$dati) {

					//echo "IMP: ".$ID_IMP. " - RIF: ".$dati["ID_RIF"]."<hr>";

					#############################
					if($dati["ID_RIF"]!=$RifBuffer) {
						$RifBuffer = $dati["ID_RIF"];

						# MODALITA CONTROLLO (TEMPO / PESO)
						if($dati["q_limite"]!=0 && !is_null($dati["q_limite"])) {
							if($dati["ID_UMIS"]=="1" | $dati["peso_spec"]!="0") {
								$mode = "Q";
								}
							else {
								$mode = "U";	# indefinito: se non si tratta di Kg e manca peso specifico x convertire
								}
							}
						else {
							$mode = "T";
							}

						# setup (giac ini, modo verifica)
						# set giac_ini solo se � giacenza "manuale"
						$dati["giac_ini"] = ($dati['isGiacIniAuto']==0? $dati["giac_ini"]:0);
						if($dati["q_limite"]!=0) {
							if($dati["ID_UMIS"]=="1" | $dati["peso_spec"]!="0") { # m3 | presenza valore peso specifico
								$subTotQ[$RifBuffer] = array($dati["q_limite"],toM3($dati["giac_ini"],$dati["ID_UMIS"],$dati["peso_spec"]),$dati["COD_CER"]." - ".$dati["descrizione"]);
								}
							else {
								$subTotQ[$RifBuffer] = array($dati["q_limite"],0,$dati["COD_CER"]." - ".$dati["descrizione"]);
								}
							}
						else {
							$subTotT = $dati["giac_ini"];
							$subTotT_C[$dati["ID_RIF"]] = $dati["giac_ini"];
							}
						}
					#############################





					if($mode=="Q") {
						if($dati["TIPO"]=="C") {
							$subTotQ[$dati["ID_RIF"]][1] +=  toM3($dati["quantita"],$dati["ID_UMIS"],$dati["peso_spec"]);
							}
						else {
							$subTotQ[$dati["ID_RIF"]][1] -= toM3($dati["quantita"],$dati["ID_UMIS"],$dati["peso_spec"]);
							}
						}

					if($dati["TIPO"]=="C" && $mode!="T"){
						$subTotT -= $dati["quantita"];
						if($subTotT==0) {
							if(isset($tmpCarichi[$dati["ID_RIF"]])) {
								unset($tmpCarichi[$dati["ID_RIF"]]);
								}
							}
						}

				} //chiude foreach

				$TotC				= 0;
				$TotS				= 0;
				$RIFbuf				= "";
				$counter			= 0;
				$NPfound			= false;
				$Pfound				= false;
				$rifiuti			= array();
				$CarichiRifiuto		= array();



				for($m=0;$m<count($FEDIT->DbRecordSet);$m++){
					//echo "2: ".$sql."<hr>";
					$indice=count($rifiuti);

					if($FEDIT->DbRecordSet[$m]['ID_RIF']!=$RIFbuf) {
						$rifiuti[$indice]['totC']=($FEDIT->DbRecordSet[$m]['isGiacIniAuto']==0? $FEDIT->DbRecordSet[$m]['giac_ini']:0);
						$rifiuti[$indice]['COD_CER']=$FEDIT->DbRecordSet[$m]['COD_CER'];
						$rifiuti[$indice]['RifID']=$FEDIT->DbRecordSet[$m]['ID_RIF'];
						$rifiuti[$indice]['giac_ini']=($FEDIT->DbRecordSet[$m]['isGiacIniAuto']==0? $FEDIT->DbRecordSet[$m]['giac_ini']:0);
						$rifiuti[$indice]['descrizione']=$FEDIT->DbRecordSet[$m]['descrizione'];
						$rifiuti[$indice]['UM']=$FEDIT->DbRecordSet[$m]['misura'];
						$rifiuti[$indice]['IDUM']=$FEDIT->DbRecordSet[$m]['ID_UMIS'];
						$rifiuti[$indice]['PSPEC']=$FEDIT->DbRecordSet[$m]['peso_spec'];
						$rifiuti[$indice]['t_limite']=$FEDIT->DbRecordSet[$m]['t_limite'];
						if($FEDIT->DbRecordSet[$m]['TIPO']=="C"){
							$rifiuti[$indice]['carico'][0]['qta']=$FEDIT->DbRecordSet[$m]['quantita'];
							$rifiuti[$indice]['carico'][0]['num']=$FEDIT->DbRecordSet[$m]['NMOV'];
							$rifiuti[$indice]['carico'][0]['data']=$FEDIT->DbRecordSet[$m]['DTMOV'];
							@$rifiuti[$indice]['totC']+=$FEDIT->DbRecordSet[$m]['quantita'];
							}
						if($FEDIT->DbRecordSet[$m]['TIPO']=="S")
							@$rifiuti[$indice]['totS']=$FEDIT->DbRecordSet[$m]['quantita'];
						}
					else{
						# somme
						if(isset($rifiuti[$indice-1]['carico']))
							$countCarichi=count($rifiuti[$indice-1]['carico']);
						else
							$countCarichi=0;
						if($FEDIT->DbRecordSet[$m]['TIPO']=="C"){
							$rifiuti[$indice-1]['carico'][$countCarichi]['qta']=$FEDIT->DbRecordSet[$m]['quantita'];
							$rifiuti[$indice-1]['carico'][$countCarichi]['num']=$FEDIT->DbRecordSet[$m]['NMOV'];
							$rifiuti[$indice-1]['carico'][$countCarichi]['data']=$FEDIT->DbRecordSet[$m]['DTMOV'];
							$rifiuti[$indice-1]['totC']+=$FEDIT->DbRecordSet[$m]['quantita'];
							}
						if($FEDIT->DbRecordSet[$m]['TIPO']=="S")
							@$rifiuti[$indice-1]['totS']+=$FEDIT->DbRecordSet[$m]['quantita'];
						}
					$RIFbuf=$FEDIT->DbRecordSet[$m]['ID_RIF'];
					}

				//if($ID_IMP='009PARIM') var_dump($rifiuti);

				for($r=0;$r<count($rifiuti);$r++){
					$string=checkCarichi($rifiuti[$r], $CarichiRifiuto);
					$result=explode("|",$string);
					if(@$result[5]!="" && $result[4]>0)
						$tmpCarichi[$result[5]] = array($result[1],$result[2],$result[4],$result[3],$result[0]);
					}

				# VERIFICA TEMPO
				if(isset($tmpCarichi) && @count($tmpCarichi)>0) {
					foreach($tmpCarichi as $k=>$dt) {
						$showPreAlarm = ( strtotime($dt[0]) + ($dt[2]*24*60*60) - ($CkDepositoTemporaneogg*24*60*60) );
						$showAlarm = ( strtotime($dt[0]) + ($dt[2]*24*60*60) );

						//if($ID_IMP='009PARIM') echo "RIFIUTO: ".$dt[3]." | TIME: " .time(). " | SHOW_ALARM: ".$showAlarm." | SHOW_PREALARM: ".$showPreAlarm."   -- ".$CkDepositoTemporaneogg."<hr>";

						if( time() >= $showAlarm ){
							$differenza = dateDiff("d",date('m-d-Y',$showPreAlarm),date("m/d/Y"));
							$DetTmp[$dt[3]] = array($dt[3],$subTotT,$dt[2],"giorni",$dt[4]."/".date('Y', strtotime($dt[0])),"T1");
							}
						else{
							if( time() >= $showPreAlarm ){
								$differenza=abs(strtotime(date('Y-m-d',$showAlarm)) - strtotime(date('Y-m-d')))/(86400);
								$DetTmp[$dt[3]] = array($dt[3],$subTotT,$dt[2],"giorni",$dt[4]."/".date('Y', strtotime($dt[0])),"T2",$differenza);
								}
							}
						}
					}



				# VERIFICA PESO
				if(isset($subTotQ) && @count($subTotQ)>0) {
					foreach($subTotQ as $k=>$dt) {
						if($dt[1]>$dt[0]) {
							$DetTmp[$k] = array($dt[2],$dt[1],$dt[0],$dt[4],"m3","Q");
						}
					}
				}



			} //chiude if 341


		if(count($DetTmp)>0) {

				$avvisoDetTmp = array();

				foreach($DetTmp as $k=>$avv) {
					if($k!=''){
						switch($avv[5]){
							case "Q":
								if($FEDIT->DbRecordSetUserSetting[0]["ID_GDEP"]==2){
									$LISTA1['DEP_TMP_SINGOLO'][] = "Il rifiuto \"" . $avv[0] . "\" ha superato la giacenza di " . $avv[2] . " " . $avv[3] . " (giacenza attuale: " . $avv[1] . " m3)";
									}
								break;
							case "T1":
								if($FEDIT->DbRecordSetUserSetting[0]["ID_GDEP"]==1){
									if($avv[4]==0) $carico="giacenza iniziale"; else $carico="carico numero ".$avv[4];
									$LISTA1['DEP_TMP_SINGOLO'][] = "Il rifiuto \"" . $avv[0] . "\" ha superato il periodo massimo di deposito " . $avv[2] . " " . $avv[3] . " ( ".$carico." ) ";
									}
								break;
							case "T2":
								if($FEDIT->DbRecordSetUserSetting[0]["ID_GDEP"]==1){
									if($avv[4]==0) $carico="giacenza iniziale"; else $carico="carico numero ".$avv[4];
									$LISTA2['DEP_TMP_SINGOLO'][] = "Restano ".number_format($avv[6],0,",","")." giorni per effettuare uno scarico del rifiuto \"" . $avv[0] . "\", prima che questo superi  il periodo massimo di deposito di " . $avv[2] . " " . $avv[3] ." ( ".$carico." ) ";
									}
								break;
							}
						}
					}
				}
			else{
				$LISTA3['DEP_TMP_SINGOLO'][] = "Deposito temporaneo per singolo rifiuto nei valori stabiliti";
				}

			unset($DetTmp);
			unset($tmpCarichi);

			}












		#
		#	AVVISO DEPOSITO TEMPORANEO (SOMMA RIFIUTI NON PERICOLOSI)
		#
		if($CkDepositoTemporaneo=="1" && $WORKMODE=="produttore" && $CkDepositoTemporaneoM3>0 && $FEDIT->DbRecordSetUserSetting[0]["ID_GDEP"]==2) {

			$sql = "SELECT giac_ini,user_schede_rifiuti.originalID_RIF AS ID_RIF, lov_cer.COD_CER,TIPO,quantita,lov_misure.ID_UMIS,lov_misure.description AS misura,peso_spec,descrizione";
			$sql .= " FROM user_schede_rifiuti ";
			$sql .= " LEFT JOIN ".$TableName." ON user_schede_rifiuti.ID_RIF=".$TableName.".ID_RIF AND NMOV<>9999999 AND PerRiclassificazione=0";
			$sql .= " JOIN lov_misure ON user_schede_rifiuti.ID_UMIS=lov_misure.ID_UMIS";
			$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
			$sql .= " AND user_schede_rifiuti.produttore='1' AND peso_spec<>'0'";
			$sql .= " AND user_schede_rifiuti.ID_IMP='" . $ID_IMP . "' WHERE user_schede_rifiuti.pericoloso='0' ORDER BY lov_cer.COD_CER ASC,NMOV ASC";
			$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
			if($FEDIT->DbRecsNum>0) {
				$CerBuffer = "";
				$DetTmp = array();
				$SubTot = array();
				foreach($FEDIT->DbRecordSet as $k=>$dati) {
					if($dati["COD_CER"]!=$CerBuffer) {
						$SubTot[$dati["COD_CER"]] = toM3($dati["giac_ini"],$dati["ID_UMIS"],$dati["peso_spec"]);
						$CerBuffer = $dati["COD_CER"];
						}
					if($dati["TIPO"]=="C") {
						$SubTot[$dati["COD_CER"]] +=  toM3($dati["quantita"],$dati["ID_UMIS"],$dati["peso_spec"]);
						}
					else {
						$SubTot[$dati["COD_CER"]] -=  toM3($dati["quantita"],$dati["ID_UMIS"],$dati["peso_spec"]);
						}
					}
				foreach($SubTot as $cer=>$q) {
					if($q>$CkDepositoTemporaneoM3) {
						$DetTmp[$cer] = array($q,$CkDepositoTemporaneoM3,"m3");
					}
				}

				if(count($DetTmp)>0) {
					foreach($DetTmp as $k=>$avv) {
						if($k!=''){
							$LISTA1['DEP_TMP_NP'][] = "I rifiuti CER $k hanno superato la giacenza di $CkDepositoTemporaneoM3 m3 (giacenza attuale: " .  $avv[0] . " m3).";
							}
						}
					}
				else{
					$LISTA3['DEP_TMP_NP'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
					}
				}
			}



		#
		#	AVVISO DEPOSITO TEMPORANEO (SOMMA RIFIUTI PERICOLOSI)
		#
		if($CkDepositoTemporaneo=="1" && $WORKMODE=="produttore" && $CkDepositoTemporaneoM3pericolosi>0 && $FEDIT->DbRecordSetUserSetting[0]["ID_GDEP"]==2) {

			$sql = "SELECT giac_ini,user_schede_rifiuti.originalID_RIF AS ID_RIF ,lov_cer.COD_CER,TIPO,quantita,lov_misure.ID_UMIS,lov_misure.description AS misura,peso_spec,descrizione";
			$sql .= " FROM user_schede_rifiuti ";
			$sql .= " LEFT JOIN ".$TableName." ON user_schede_rifiuti.ID_RIF=".$TableName.".ID_RIF AND NMOV<>9999999 AND PerRiclassificazione=0";
			$sql .= " JOIN lov_misure ON user_schede_rifiuti.ID_UMIS=lov_misure.ID_UMIS";
			$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
			$sql .= " AND user_schede_rifiuti.produttore='1' AND peso_spec<>'0'";
			$sql .= " AND user_schede_rifiuti.ID_IMP='" . $ID_IMP . "' WHERE user_schede_rifiuti.pericoloso='1' ORDER BY lov_cer.COD_CER ASC,NMOV ASC";
			$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
			if($FEDIT->DbRecsNum>0) {
				$CerBuffer = "";
				$DetTmp = array();
				$SubTot = array();
				foreach($FEDIT->DbRecordSet as $k=>$dati) {
					if($dati["COD_CER"]!=$CerBuffer) {
						$SubTot[$dati["COD_CER"]] = toM3($dati["giac_ini"],$dati["ID_UMIS"],$dati["peso_spec"]);
						$CerBuffer = $dati["COD_CER"];
						}
					if($dati["TIPO"]=="C") {
						$SubTot[$dati["COD_CER"]] +=  toM3($dati["quantita"],$dati["ID_UMIS"],$dati["peso_spec"]);
						}
					else {
						$SubTot[$dati["COD_CER"]] -=  toM3($dati["quantita"],$dati["ID_UMIS"],$dati["peso_spec"]);
						}
					}
				foreach($SubTot as $cer=>$q) {
					if($q>$CkDepositoTemporaneoM3pericolosi) {
						$DetTmp[$cer] = array($q,$CkDepositoTemporaneoM3pericolosi,"m3");
						}
					}
				if(count($DetTmp)>0) {
					foreach($DetTmp as $k=>$avv) {
						if($k!=''){
							$LISTA1['DEP_TMP_P'] = "I rifiuti CER $k hanno superato la giacenza di $CkDepositoTemporaneoM3pericolosi m3 (giacenza attuale: " .  $avv[0] . " m3).";
							}
						}
					}
				else{
					$LISTA3['DEP_TMP_P'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
					}
				}
			}





		#
		#	AVVISO DEPOSITO TEMPORANEO (SOMMA TUTTI I RIFIUTI)
		#
		if($CkDepositoTemporaneo=="1" && $WORKMODE=="produttore" && $CkDepositoTemporaneoM3totali>0 && $FEDIT->DbRecordSetUserSetting[0]["ID_GDEP"]==2) {

			$sql = "SELECT giac_ini,user_schede_rifiuti.originalID_RIF AS ID_RIF, lov_cer.COD_CER,TIPO,quantita,lov_misure.ID_UMIS,lov_misure.description AS misura,peso_spec,descrizione";
			$sql .= " FROM user_schede_rifiuti ";
			$sql .= " LEFT JOIN ".$TableName." ON user_schede_rifiuti.ID_RIF=".$TableName.".ID_RIF AND NMOV<>9999999 AND PerRiclassificazione=0";
			$sql .= " JOIN lov_misure ON user_schede_rifiuti.ID_UMIS=lov_misure.ID_UMIS";
			$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
			$sql .= " AND user_schede_rifiuti.produttore='1' AND peso_spec<>'0'";
			$sql .= " AND user_schede_rifiuti.ID_IMP='" . $ID_IMP . "' ORDER BY lov_cer.COD_CER ASC,NMOV ASC";
			$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
			if($FEDIT->DbRecsNum>0) {
				$CerBuffer = "";
				$DetTmp = array();
				$SubTot = array();
				foreach($FEDIT->DbRecordSet as $k=>$dati) {
					if($dati["COD_CER"]!=$CerBuffer) {
						$SubTot[$dati["COD_CER"]] = toM3($dati["giac_ini"],$dati["ID_UMIS"],$dati["peso_spec"]);
						$CerBuffer = $dati["COD_CER"];
					}
					if($dati["TIPO"]=="C") {
						$SubTot[$dati["COD_CER"]] +=  toM3($dati["quantita"],$dati["ID_UMIS"],$dati["peso_spec"]);
					} else {
						$SubTot[$dati["COD_CER"]] -=  toM3($dati["quantita"],$dati["ID_UMIS"],$dati["peso_spec"]);
					}
				}
				foreach($SubTot as $cer=>$q) {
					if($q>$CkDepositoTemporaneoM3pericolosi) {
						$DetTmp[$cer] = array($q,$CkDepositoTemporaneoM3pericolosi,"m3");
					}
				}
				if(count($DetTmp)>0) {
					foreach($DetTmp as $k=>$avv) {
						if($k!=''){
							$LISTA1['DEP_TMP_TUTTI'][] = "I rifiuti CER $k hanno superato la giacenza di $CkDepositoTemporaneoM3totali m3 (giacenza attuale: " .  $avv[0] . " m3)";
							}
						}
					}
				else{
					$LISTA3['DEP_TMP_TUTTI'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
					}
				}
			}






		#
		#	AVVISI DI CARICO DALL' ULTIMO CARICO
		#

		$sql = "SELECT NMOV,descrizione,user_schede_rifiuti.ID_RIF,C_S_ID,avv_giorni,avv_quantita,user_schede_rifiuti.ID_IMP,DTMOV,TIPO,lov_misure.description FROM user_schede_rifiuti ";
		$sql .= "JOIN ".$TableName." ON user_schede_rifiuti.ID_RIF=".$TableName.".ID_RIF JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS ";
		$sql .= "WHERE user_schede_rifiuti.ID_IMP='" . $ID_IMP . "' ";
		$sql .= "AND ".$TableName.".TIPO = 'C' ";
		$TableSpec = "user_schede_rifiuti.";
		$sql .= "AND ".$TableSpec.$WORKMODE."=1 ";
		$sql .= "AND ".$TableSpec."approved=1 ";
		$sql .= "AND C_S_ID<>'1' AND NMOV<>9999999 AND PerRiclassificazione=0 ORDER BY NMOV ASC";
		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
		$AvvisiC = array();
		$AvvisiC_future = array();
		if($FEDIT->DbRecsNum>0) {
			foreach($FEDIT->DbRecordSet as $k=>$v) {
				if($v["C_S_ID"]=="2") {
					# avv dall'ultimo carico
					$differenza = dateDiff("d",date("m/d/Y",strtotime($v["DTMOV"])),date("m/d/Y"));
					if($differenza>=$v["avv_giorni"]) {
						$AvvisiC[$v["ID_RIF"]] = array($v["description"],$v["descrizione"],$v["avv_quantita"],$v["DTMOV"]);
						}
					elseif ($differenza<$v["avv_giorni"]) {
						unset($AvvisiC[$v["ID_RIF"]]);
						$AvvisiC_future[$v["ID_RIF"]] = array($v["description"],$v["descrizione"],$v["avv_quantita"],$v["DTMOV"]);
						}
					}
				}
			}
		if(isset($AvvisiC)){
			foreach($AvvisiC as $k=>$v) {
				$LISTA1['AVVISO_CARICO_C'][] = "Caricare "  . $v[2] . " " . $v[0] . " del rifiuto \"" . $v[1] . "\"";
				}
			}
		if(isset($AvvisiC_future)){
			foreach($AvvisiC_future as $k=>$v) {
				$LISTA3['AVVISO_CARICO_C'][] = "Rifiuto \"" . $v[1] . "\": il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
				}
			}
		else
			$LISTA3['AVVISO_CARICO_C'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";

		#
		#	AVVISI DI CARICO DALL' ULTIMO SCARICO
		#
		$sql = "SELECT NMOV,descrizione,user_schede_rifiuti.ID_RIF,C_S_ID,avv_giorni,avv_quantita,user_schede_rifiuti.ID_IMP,DTMOV,TIPO,lov_misure.description FROM user_schede_rifiuti ";
		$sql .= "JOIN ".$TableName." ON user_schede_rifiuti.ID_RIF=".$TableName.".ID_RIF JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS ";
		$sql .= "WHERE user_schede_rifiuti.ID_IMP='" . $ID_IMP . "' ";
		$sql .= "AND ".$TableName.".TIPO = 'S' ";

		$TableSpec = "user_schede_rifiuti.";
		$sql .= "AND ".$TableSpec.$WORKMODE."=1 ";
		$sql .= "AND ".$TableSpec."approved=1 ";
		$sql .= "AND C_S_ID<>'1' AND NMOV<>9999999 AND PerRiclassificazione=0 ORDER BY NMOV ASC";
		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
		$AvvisiS = array();
		$AvvisiS_future = array();
		if($FEDIT->DbRecsNum>0) {
			foreach($FEDIT->DbRecordSet as $k=>$v) {
				if($v["C_S_ID"]=="3") {
				# avv dall'ultimo scarico
				$differenza = dateDiff("d",date("m/d/Y",strtotime($v["DTMOV"])),date("m/d/Y"));
				if($differenza>=$v["avv_giorni"]) {
					$AvvisiS[$v["ID_RIF"]] = array($v["description"],$v["descrizione"],$v["avv_quantita"],$v["DTMOV"] );
					}
				elseif ($differenza<$v["avv_giorni"]) {
					unset($AvvisiS[$v["ID_RIF"]]);
					$AvvisiS_future[$v["ID_RIF"]] = array($v["description"],$v["descrizione"],$v["avv_quantita"],$v["DTMOV"]);
					}
				}
			}
		}
		if(isset($AvvisiS)){
			foreach($AvvisiS as $k=>$v) {
				$LISTA1['AVVISO_CARICO_S'][] = "Caricare "  . $v[2] . " " . $v[0] . " del rifiuto \"" . $v[1] . "\"";
				}
			}
		if(isset($AvvisiS_future)){
			foreach($AvvisiS_future as $k=>$v) {
				$LISTA3['AVVISO_CARICO_S'][] = "Rifiuto \"" . $v[1] . "\": il sistema non ha individuato, al periodo considerato, criticit� da segnalare";
				}
			}
		else
			$LISTA3['AVVISO_CARICO_S'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";



		#
		#	AVVISI CARICO DALL' ULTIMO MOVIMENTO
		#

		$sql = "SELECT NMOV,descrizione,user_schede_rifiuti.ID_RIF,C_S_ID,avv_giorni,avv_quantita,user_schede_rifiuti.ID_IMP,DTMOV,TIPO,lov_misure.description FROM user_schede_rifiuti ";
		$sql .= "JOIN ".$TableName." ON user_schede_rifiuti.ID_RIF=".$TableName.".ID_RIF JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS ";
		$sql .= "WHERE user_schede_rifiuti.ID_IMP='" . $ID_IMP . "' ";

		$TableSpec = "user_schede_rifiuti.";
		$sql .= "AND ".$TableSpec.$WORKMODE."=1 ";
		$sql .= "AND ".$TableSpec."approved=1 ";
		$sql .= "AND C_S_ID<>'1' AND NMOV<>9999999 AND PerRiclassificazione=0 ORDER BY NMOV ASC";
		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
		if($FEDIT->DbRecsNum>0) {
			$AvvisiM = array();
			$AvvisiM_future = array();
			foreach($FEDIT->DbRecordSet as $k=>$v) {
				if($v["C_S_ID"]=="4") {
				# avv dall'ultimo movimento
				$differenza = dateDiff("d",date("m/d/Y",strtotime($v["DTMOV"])),date("m/d/Y"));
				if($differenza>=$v["avv_giorni"]) {
					$AvvisiM[$v["ID_RIF"]] = array($v["description"],$v["descrizione"],$v["avv_quantita"],$v["DTMOV"]);
					}
				elseif ($differenza<$v["avv_giorni"]) {
					unset($AvvisiM[$v["ID_RIF"]]);
					$AvvisiM_future[$v["ID_RIF"]] = array($v["description"],$v["descrizione"],$v["avv_quantita"],$v["DTMOV"]);
					}
				}
			}
		}
		if(isset($AvvisiM)){
			foreach($AvvisiM as $k=>$v) {
				$LISTA1['AVVISO_CARICO_M'][] = "Caricare "  . $v[2] . " " . $v[0] . " del rifiuto \"" . $v[1] . "\"";
				}
			}
		if(isset($AvvisiM_future)){
			foreach($AvvisiM_future as $k=>$v) {
				$LISTA3['AVVISO_CARICO_M'][] = "Rifiuto \"" . $v[1] . "\": il sistema non ha individuato, al periodo considerato, criticit� da segnalare";
				}
			}
		else
			$LISTA3['AVVISO_CARICO_M'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";



		#
		#	AVVISI RITORNO QUARTA COPIA FORMULARIO
		#
		if($CkRitornoFormulario=="1" AND $WORKMODE=="produttore") {
			$sql = "SELECT ID_MOV_F,NMOV,NFORM,DTFORM,DT_FORM,dt_in_trasp FROM user_movimenti_fiscalizzati ";
			$sql .= "WHERE user_movimenti_fiscalizzati.ID_IMP='" . $ID_IMP . "' ";
			$TableSpec = "user_movimenti_fiscalizzati.";
			$sql .= " AND ".$TableSpec.$WORKMODE."=1 ";
			$sql .= " AND ".$TableSpec."approved=1 ";
			$sql .= " AND ( ";
			$sql .= " (user_movimenti_fiscalizzati.DTFORM<>'0000-00-00' AND user_movimenti_fiscalizzati.DTFORM IS NOT NULL) ";
			$sql .= " OR ";
			$sql .= " (user_movimenti_fiscalizzati.dt_in_trasp<>'0000-00-00' AND user_movimenti_fiscalizzati.dt_in_trasp IS NOT NULL) ";
			$sql .= " ) ";
			$sql .= " AND IF ((user_movimenti_fiscalizzati.dt_in_trasp <>'0000-00-00' AND user_movimenti_fiscalizzati.dt_in_trasp IS NOT NULL), dt_in_trasp, DTFORM) <= (SUBDATE(CURDATE(),INTERVAL '$intervalloForm' DAY)) ";
			$sql .= " AND TIPO='S' ";
			$sql .= " AND (DT_FORM='0000-00-00' OR ISNULL(DT_FORM))";
			$sql .= " AND SenzaTrasporto = 0 ";
			$sql .= " AND NMOV<>9999999 ";
			$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
			if($FEDIT->DbRecsNum>0) {
				foreach($FEDIT->DbRecordSet as $k=>$dati) {
					$DTFORM		= explode('-', $dati['DTFORM']);
					$ts_oggi	= mktime(0, 0, 0, date('m'), date('d'), date('Y'));
					$ts_DTFORM	= mktime(0, 0, 0, $DTFORM[1], $DTFORM[2], $DTFORM[0]);
					$seconds	= $ts_oggi - $ts_DTFORM;
					$GiorniTrascorsi = abs(intval($seconds/86400));
					if($GiorniTrascorsi<=90)
						$LISTA2['QUARTE_COPIE'][] = "La quarta copia del formulario " . $dati["NFORM"] . " del movimento num. " . $dati["NMOV"] . ", emesso ".$GiorniTrascorsi." giorni fa, non � ancora rientrata.";
					else
						$LISTA1['QUARTE_COPIE'][] = "La quarta copia del formulario " . $dati["NFORM"] . " del movimento num. " . $dati["NMOV"] . ", emesso ".$GiorniTrascorsi." giorni fa, non � rientrata nei 90 giorni previsti.";
					}
				}
			else{
				$LISTA3['QUARTE_COPIE'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
				}
			}





		#
		#	AUTORIZZAZIONI INTERMEDIARI
		#
		if($CkAutorizzazioni=='1') {
			$sql  = "SELECT ADDDATE(CURDATE(),$intervallo) AS dateSpan,user_impianti_intermediari.description AS IMdes,user_aziende_intermediari.description,user_aziende_intermediari.ID_AZI";
			$sql .= ",user_schede_rifiuti.descrizione AS CERdes,COD_CER";
			$sql .= ",ID_AUTHI,user_autorizzazioni_interm.num_aut,user_autorizzazioni_interm.scadenza,user_autorizzazioni_interm.rilascio";
			$sql .= " FROM user_aziende_intermediari JOIN user_impianti_intermediari ON user_impianti_intermediari.ID_AZI=user_aziende_intermediari.ID_AZI";
			$sql .= " JOIN user_autorizzazioni_interm ON user_autorizzazioni_interm.ID_UIMI=user_impianti_intermediari.ID_UIMI";
			$sql .= " JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_autorizzazioni_interm.ID_RIF";
			$sql .= " JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER";
			$sql .= " WHERE user_autorizzazioni_interm.scadenza< ADDDATE(CURDATE(),$intervallo) AND user_autorizzazioni_interm.approved=1 ";
			$sql .= " AND user_aziende_intermediari.ID_IMP='" . $ID_IMP . "'";
			$sql .= " AND user_autorizzazioni_interm.ID_AUT='1'"; // --> 0= domanda iscrizione; 1= autorizzazione;
			$sql .= " AND user_schede_rifiuti.approved=1 ";
			$TableSpec = "user_aziende_intermediari.";
			$sql.="AND ".$TableSpec.$WORKMODE."=1 ";
			$sql.="AND ".$TableSpec."approved=1 ";
			unset($TableSpec);

			$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
			if($FEDIT->DbRecsNum>0) {
				foreach($FEDIT->DbRecordSet as $k=>$dati) {
					if($dati["scadenza"]=="0000-00-00") {
						$avviso = " Scadenza autorizzazione mancante: ";
						$avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
						$avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"];
						$avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
						$LISTA1['AUT_I'][] = $avviso;
						}
					else {
						#
						# necessit� di cambiare l'allarme a seconda che l'autorizzazione sia scaduta o in scadenza!
						#
						$oggi=mktime(0, 0, 0, date('m'), date('d'), date('Y'));
						$scade=explode('-', $dati['scadenza']);
						$scadenza=mktime(0, 0, 0, $scade[1], $scade[2], $scade[0]);
						$differenza=($scadenza - $oggi)/(60*60*24);
						if($differenza>=0){
							$avviso="Autorizzazione in scadenza: ";
							$avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
							$avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"];
							$avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
							$LISTA2['AUT_I'][] = $avviso;
							}
						else{
							$avviso="Autorizzazione scaduta: ";
							$avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
							$avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"];
							$avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
							$LISTA1['AUT_I'][] = $avviso;
							}
						}
					}
				}
			else{
				$LISTA3['AUT_I'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
				}
			}



		#
		#	DOMANDA ISCRIZIONE INTERMEDIARI
		#
		if($CkAutorizzazioni=='1') {

			$sql  = "SELECT ADDDATE(CURDATE(),$intervallo) AS dateSpan,user_impianti_intermediari.description AS IMdes,user_aziende_intermediari.description,user_aziende_intermediari.ID_AZI";
			$sql .= ",user_schede_rifiuti.descrizione AS CERdes,COD_CER";
			$sql .= ",ID_AUTHI,user_autorizzazioni_interm.num_aut,user_autorizzazioni_interm.scadenza,user_autorizzazioni_interm.rilascio";
			$sql .= " FROM user_aziende_intermediari JOIN user_impianti_intermediari ON user_impianti_intermediari.ID_AZI=user_aziende_intermediari.ID_AZI";
			$sql .= " JOIN user_autorizzazioni_interm ON user_autorizzazioni_interm.ID_UIMI=user_impianti_intermediari.ID_UIMI";
			$sql .= " JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_autorizzazioni_interm.ID_RIF";
			$sql .= " JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER";
			$sql .= " WHERE user_autorizzazioni_interm.scadenza< ADDDATE(CURDATE(),$intervallo) AND user_autorizzazioni_interm.approved=1 ";
			$sql .= " AND user_aziende_intermediari.ID_IMP='" . $ID_IMP . "'";
			$sql .= " AND user_autorizzazioni_interm.ID_AUT='0'"; // --> 0= domanda iscrizione; 1= autorizzazione;
			$sql .= " AND user_schede_rifiuti.approved=1 ";
			$TableSpec = "user_aziende_intermediari.";
			$sql.="AND ".$TableSpec.$WORKMODE."=1 ";
			$sql.="AND ".$TableSpec."approved=1 ";
			unset($TableSpec);

			$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
			if($FEDIT->DbRecsNum>0) {
				foreach($FEDIT->DbRecordSet as $k=>$dati) {

					$oggi		= mktime(0, 0, 0, date('m'), date('d'), date('Y'));
					$rilascio	= explode('-', $dati['rilascio']);
					$rilascio	= mktime(0, 0, 0, $rilascio[1], $rilascio[2], $rilascio[0]);
					$differenza_rilascio=($rilascio - $oggi)/(60*60*24);

					if($dati["scadenza"]=="0000-00-00"){
						if($differenza_rilascio<=60)
							$avviso = "Sono trascorsi almeno 60 giorni dalla data di presentazione della domanda di iscrizione: ";
						else
							$avviso = "Scadenza domanda di iscrizione mancante: ";
						$avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
						$avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"];
						$avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
						$LISTA1['DOM_ISC_I'][] = $avviso;
						}
					else {
						#
						# necessit� di cambiare l'allarme a seconda che l'autorizzazione sia scaduta o in scadenza!
						#
						$scade=explode('-', $dati['scadenza']);
						$scadenza=mktime(0, 0, 0, $scade[1], $scade[2], $scade[0]);
						$differenza=($scadenza - $oggi)/(60*60*24);
						if($differenza>=0){
							$avviso="Domanda di iscrizione in scadenza: ";
							$avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
							$avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"];
							$avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
							$LISTA2['DOM_ISC_I'][] = $avviso;
							}
						else{
							$avviso="Domanda di iscrizione scaduta: ";
							$avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
							$avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"];
							$avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
							$LISTA1['DOM_ISC_I'][] = $avviso;
							}
						}
					}
				}
			else{
				$LISTA3['DOM_ISC_I'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
				}
			}






		#
		#	AUTORIZZAZIONI DESTINATARI
		#
		if($CkAutorizzazioni=='1') {

			$sql = "SELECT ADDDATE(CURDATE(),$intervallo) AS dateSpan,user_impianti_destinatari.description AS IMdes,user_aziende_destinatari.description,user_aziende_destinatari.ID_AZD";
			$sql .= ",user_schede_rifiuti.descrizione AS CERdes,COD_CER";
			$sql .= ",ID_AUTHD,user_autorizzazioni_dest.num_aut,user_autorizzazioni_dest.scadenza,user_autorizzazioni_dest.rilascio";
			$sql .= " FROM user_aziende_destinatari JOIN user_impianti_destinatari ON user_impianti_destinatari.ID_AZD=user_aziende_destinatari.ID_AZD";
			$sql .= " JOIN user_autorizzazioni_dest ON user_autorizzazioni_dest.ID_UIMD=user_impianti_destinatari.ID_UIMD";
			$sql .= " JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_autorizzazioni_dest.ID_RIF";
			$sql .= " JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER";
			$sql .= " WHERE user_autorizzazioni_dest.scadenza< ADDDATE(CURDATE(),$intervallo) AND user_autorizzazioni_dest.approved=1 ";
			$sql .= " AND user_aziende_destinatari.ID_IMP='" . $ID_IMP . "'";
			$sql .= " AND user_schede_rifiuti.approved=1 ";
			$TableSpec = "user_aziende_destinatari.";
			$sql.="AND ".$TableSpec.$WORKMODE."=1 ";
			$sql.="AND ".$TableSpec."approved=1 ";
			unset($TableSpec);

			$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
			if($FEDIT->DbRecsNum>0) {
				foreach($FEDIT->DbRecordSet as $k=>$dati) {
					if($dati["scadenza"]=="0000-00-00"){
						$avviso = "Scadenza autorizzazione mancante: ";
						$avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
						$avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"];
						$avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
						$LISTA1['AUT_D'][] = $avviso;
						}
					else {
						#
						# necessit� di cambiare l'allarme a seconda che l'autorizzazione sia scaduta o in scadenza!
						#
						$oggi=mktime(0, 0, 0, date('m'), date('d'), date('Y'));
						$scade=explode('-', $dati['scadenza']);
						$scadenza=mktime(0, 0, 0, $scade[1], $scade[2], $scade[0]);
						$differenza=($scadenza - $oggi)/(60*60*24);
						if($differenza>=0){
							$avviso="Autorizzazione in scadenza:";
							$avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
							$avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"];
							$avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
							$LISTA2['AUT_D'][] = $avviso;
							}
						else{
							$avviso="Autorizzazione scaduta: ";
							$avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
							$avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"];
							$avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
							$LISTA1['AUT_D'][] = $avviso;
							}
						}
					}
				}
			else{
				$LISTA3['AUT_D'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
				}
			}







		#
		#	AUTORIZZAZIONI TRASPORTATORI
		#
		if($CkAutorizzazioni=='1') {

			$sql = "SELECT ADDDATE(CURDATE(),$intervallo) AS dateSpan,user_impianti_trasportatori.description AS IMdes,user_aziende_trasportatori.description,user_aziende_trasportatori.ID_AZT";
			$sql .= ",user_schede_rifiuti.descrizione AS CERdes,COD_CER";
			$sql .= ",ID_AUTHT,user_autorizzazioni_trasp.num_aut,user_autorizzazioni_trasp.scadenza,user_autorizzazioni_trasp.rilascio";
			$sql .= " FROM user_aziende_trasportatori JOIN user_impianti_trasportatori ON user_impianti_trasportatori.ID_AZT=user_aziende_trasportatori.ID_AZT";
			$sql .= " JOIN user_autorizzazioni_trasp ON user_autorizzazioni_trasp.ID_UIMT=user_impianti_trasportatori.ID_UIMT";
			$sql .= " JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_autorizzazioni_trasp.ID_RIF";
			$sql .= " JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER";
			$sql .= " WHERE user_autorizzazioni_trasp.scadenza< ADDDATE(CURDATE(),$intervallo) AND user_autorizzazioni_trasp.approved=1 ";
			$sql .= "AND user_aziende_trasportatori.ID_IMP='" . $ID_IMP . "'";
			$sql .= " AND user_schede_rifiuti.approved=1 ";
			$TableSpec = "user_aziende_trasportatori.";
			$sql.="AND ".$TableSpec.$WORKMODE."=1 ";
			$sql.="AND ".$TableSpec."approved=1 ";
			unset($TableSpec);

			$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
			if($FEDIT->DbRecsNum>0) {
				foreach($FEDIT->DbRecordSet as $k=>$dati) {
					if($dati["scadenza"]=="0000-00-00"){
						$avviso = "Scadenza Numero Albo mancante: ";
						$avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
						$avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"];
						$avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
						$LISTA1['AUT_T'][] = $avviso;
						}
					else {
						#
						# necessit� di cambiare l'allarme a seconda che l'autorizzazione sia scaduta o in scadenza!
						#
						$oggi=mktime(0, 0, 0, date('m'), date('d'), date('Y'));
						$scade=explode('-', $dati['scadenza']);
						$scadenza=mktime(0, 0, 0, $scade[1], $scade[2], $scade[0]);
						$differenza=($scadenza - $oggi)/(60*60*24);
						if($differenza>=0){
							$avviso="Numero albo in scadenza:";
							$avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
							$avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"];
							$avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
							$LISTA2['AUT_T'][] = $avviso;
							}
						else{
							$avviso="Numero albo scaduto: ";
							$avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
							$avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"];
							$avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
							$LISTA1['AUT_T'][] = $avviso;
							}
						}
					}
				}
			else{
				$LISTA3['AUT_T'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
				}
			}


	#
	#	MEZZI SENZA BLACK BOX
	#
	if($WORKMODE=='produttore' AND $MODULO_SIS=='1'){
		$sql ="SELECT DISTINCT user_aziende_trasportatori.ID_AZT, user_aziende_trasportatori.description as trasportatore, user_automezzi.description AS targa FROM `user_automezzi` JOIN user_autorizzazioni_trasp ON user_autorizzazioni_trasp.ID_AUTHT=user_automezzi.ID_AUTHT JOIN user_impianti_trasportatori ON user_impianti_trasportatori.ID_UIMT=user_autorizzazioni_trasp.ID_UIMT JOIN user_aziende_trasportatori ON user_aziende_trasportatori.ID_AZT=user_impianti_trasportatori.ID_AZT JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_autorizzazioni_trasp.ID_RIF WHERE BlackBox=0 AND user_autorizzazioni_trasp.approved=1 AND user_impianti_trasportatori.approved=1 AND user_aziende_trasportatori.approved=1 AND user_aziende_trasportatori.".$WORKMODE."=1 AND user_aziende_trasportatori.ID_IMP='".$ID_IMP."' AND pericoloso=1 ORDER BY trasportatore, targa;";
		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

		if($FEDIT->DbRecsNum>0) {

			$AZT_buffer	= 0;
			$avvisi		= array();
			$ContaAvvisi= 0;

			for($d=0;$d<count($FEDIT->DbRecordSet);$d++){

				if($FEDIT->DbRecordSet[$d]['ID_AZT']!=$AZT_buffer){
					$avviso ="Attenzione, i seguenti automezzi del trasportatore ".$FEDIT->DbRecordSet[$d]['trasportatore']." risultano sprovvisti della Black Box SISTRI: ";
					$ContaAvvisi++;
					}
				$AZT_buffer	= $FEDIT->DbRecordSet[$d]['ID_AZT'];
				$avviso.=$FEDIT->DbRecordSet[$d]['targa'];
				$NextRecord = $d+1;
				if($NextRecord<count($FEDIT->DbRecordSet) AND $AZT_buffer==$FEDIT->DbRecordSet[$NextRecord]['ID_AZT'])
					$avviso.=", ";

				$avvisi[$ContaAvvisi] = $avviso;

				}
			for($a=1; $a<=count($avvisi); $a++){
				$LISTA1['BLACK_BOX'][] = $avvisi[$a];
				}
			}
		else{
			$LISTA3['BLACK_BOX'][] =  "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
			}

		}



		#
		#	AUTORIZZAZIONI PRODUTTORI
		#
		if($CkAutorizzazioni=='1') {

			$sql = "SELECT ADDDATE(CURDATE(),$intervallo) AS dateSpan,user_impianti_produttori.description AS IMdes,user_aziende_produttori.description,user_aziende_produttori.ID_AZP";
			$sql .= ",user_schede_rifiuti.descrizione AS CERdes,COD_CER";
			$sql .= ",ID_AUTH,user_autorizzazioni_pro.num_aut,user_autorizzazioni_pro.scadenza,user_autorizzazioni_pro.rilascio";
			$sql .= " FROM user_aziende_produttori JOIN user_impianti_produttori ON user_impianti_produttori.ID_AZP=user_aziende_produttori.ID_AZP";
			$sql .= " JOIN user_autorizzazioni_pro ON user_autorizzazioni_pro.ID_UIMP=user_impianti_produttori.ID_UIMP";
			$sql .= " JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_autorizzazioni_pro.ID_RIF";
			$sql .= " JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER";
			$sql .= " WHERE user_autorizzazioni_pro.scadenza< ADDDATE(CURDATE(),$intervallo) AND user_autorizzazioni_pro.approved=1";
			$sql .= " AND user_aziende_produttori.ID_IMP='" . $ID_IMP . "'";
			$sql .= " AND user_schede_rifiuti.approved=1 ";
			$TableSpec = "user_aziende_produttori.";
			$sql.="AND ".$TableSpec.$WORKMODE."=1 ";
			$sql.="AND ".$TableSpec."approved=1 ";
			unset($TableSpec);

			$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
			if($FEDIT->DbRecsNum>0) {
				foreach($FEDIT->DbRecordSet as $k=>$dati) {
					if($dati["scadenza"]=="0000-00-00"){
						$avviso = "Scadenza autorizzazione mancante: ";
						$avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
						$avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"];
						$avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
						$LISTA1['AUT_P'][] = $avviso;
						}
					else {
						#
						# necessit� di cambiare l'allarme a seconda che l'autorizzazione sia scaduta o in scadenza!
						#
						$oggi=mktime(0, 0, 0, date('m'), date('d'), date('Y'));
						$scade=explode('-', $dati['scadenza']);
						$scadenza=mktime(0, 0, 0, $scade[1], $scade[2], $scade[0]);
						$differenza=($scadenza - $oggi)/(60*60*24);
						if($differenza>=0){
							$avviso="Autorizzazione in scadenza:";
							$avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
							$avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"];
							$avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
							$LISTA2['AUT_P'][] = $avviso;
							}
						else{
							$avviso="Autorizzazione scaduta: ";
							$avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
							$avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"];
							$avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
							$LISTA1['AUT_P'][] = $avviso;
							}
						}
					}
				}
			else{
				$LISTA3['AUT_P'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
				}
			}




		#
		#	PATENTI ADR
		#
		$sql = "SELECT user_autisti.ID_AUTST,nome,user_autisti.description AS cognome,rilascio,scadenza,user_aziende_trasportatori.description as AZdes,user_impianti_trasportatori.description AS IMPdes";
		$sql .= " FROM user_autisti ";
		$sql .= " JOIN user_impianti_trasportatori ON user_impianti_trasportatori.ID_UIMT=user_autisti.ID_UIMT";
		$sql .= " JOIN user_aziende_trasportatori ON user_aziende_trasportatori.ID_AZT=user_impianti_trasportatori.ID_AZT";
		$sql .= " WHERE (scadenza<'" . date("Y-m-d") . "' OR scadenza='0000-00-00') AND (patente<>'' AND NOT ISNULL(patente)) ";
		$sql .= " AND adr='1'";
		$sql .= " AND ID_IMP='" . $ID_IMP . "'";
		$TableSpec="user_aziende_trasportatori.";
		$sql.="AND ".$TableSpec.$WORKMODE."=1 ";
		$sql.="AND ".$TableSpec."approved=1 ";
		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
		if($FEDIT->DbRecsNum>0) {
			foreach($FEDIT->DbRecordSet as $k=>$dati) {
				$avviso = " La Patente ADR di ";
				$avviso .= $dati["cognome"] . " " . $dati["nome"];
				$avviso .= " (" . $dati["AZdes"] . ", impianto di " . $dati["IMPdes"] . ")";
				if($dati["scadenza"]=="0000-00-00") {
					$avviso .= " non ha indicazione di scadenza";
					}
				else {
					$avviso .= " � scaduta";
					}
				$LISTA1['PATENTI_ADR'][] = $avviso;
				}
			}
		else{
			$LISTA3['PATENTI_ADR'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
			}


		#
		#	SCADENZA APPROVAZIONE CARATTERIZZAZIONI
		#
		$sql ="SELECT lov_cer.COD_CER, ID_RIF, descrizione, CAR_approved_start, CAR_approved_end ";
		$sql.="FROM user_schede_rifiuti ";
		$sql.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
		$sql.="WHERE ID_IMP='".$ID_IMP."' ";
		$sql.="AND CAR_approved_end<NOW() ";
		$TableSpec = "user_schede_rifiuti.";
		$sql.="AND ".$TableSpec.$WORKMODE."=1 ";
		$sql.="AND ".$TableSpec."approved=1 ";
		unset($TableSpec);

		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
		if($FEDIT->DbRecsNum>0) {
			foreach($FEDIT->DbRecordSet as $k=>$dati) {
				$ScadenzaApprovazione_array = explode("-",$dati['CAR_approved_end']);
				$ScadenzaApprovazione = (int)$ScadenzaApprovazione_array[2]."/".(int)$ScadenzaApprovazione_array[1]."/".(int)$ScadenzaApprovazione_array[0];
				$LISTA1['SCAD_CAR'][] = "L'approvazione della caratterizzazione per il rifiuto ".$dati['COD_CER']." ".$dati['descrizione']." � scaduta il ".$ScadenzaApprovazione;
				}
			}
		else{
			$LISTA3['SCAD_CAR'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
			}


		#
		#	SCADENZA APPROVAZIONE CLASSIFICAZIONE
		#
		$sql ="SELECT lov_cer.COD_CER, ID_RIF, descrizione, CLASS_approved_start, CLASS_approved_end ";
		$sql.="FROM user_schede_rifiuti ";
		$sql.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
		$sql.="WHERE ID_IMP='".$ID_IMP."' ";
		$sql.="AND CLASS_approved_end<NOW() ";
		$TableSpec = "user_schede_rifiuti.";
		$sql.="AND ".$TableSpec.$WORKMODE."=1 ";
		$sql.="AND ".$TableSpec."approved=1 ";
		unset($TableSpec);

		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
		if($FEDIT->DbRecsNum>0) {
			foreach($FEDIT->DbRecordSet as $k=>$dati) {
				$ScadenzaApprovazione_array = explode("-",$dati['CLASS_approved_end']);
				$ScadenzaApprovazione = (int)$ScadenzaApprovazione_array[2]."/".(int)$ScadenzaApprovazione_array[1]."/".(int)$ScadenzaApprovazione_array[0];
				$LISTA1['SCAD_CLASS'][] = "L'approvazione della classificazione per il rifiuto ".$dati['COD_CER']." ".$dati['descrizione']." � scaduta il ".$ScadenzaApprovazione;
				}
			}
		else{
			$LISTA3['SCAD_CLASS'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
			}



		#
		#	SCADENZA APPROVAZIONE ETICHETTE
		#
		$sql ="SELECT lov_cer.COD_CER, ID_RIF, descrizione, et_approved_start, et_approved_end ";
		$sql.="FROM user_schede_rifiuti ";
		$sql.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
		$sql.="WHERE ID_IMP='".$ID_IMP."' ";
		$sql.="AND et_approved_end<NOW() ";
		$TableSpec = "user_schede_rifiuti.";
		$sql.="AND ".$TableSpec.$WORKMODE."=1 ";
		$sql.="AND ".$TableSpec."approved=1 ";
		unset($TableSpec);

		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
		if($FEDIT->DbRecsNum>0) {
			foreach($FEDIT->DbRecordSet as $k=>$dati) {
				$ScadenzaApprovazione_array = explode("-",$dati['et_approved_end']);
				$ScadenzaApprovazione = (int)$ScadenzaApprovazione_array[2]."/".(int)$ScadenzaApprovazione_array[1]."/".(int)$ScadenzaApprovazione_array[0];
				$LISTA1['SCAD_ETI'][] = "L'approvazione dell'etichetta per il rifiuto ".$dati['COD_CER']." ".$dati['descrizione']." � scaduta il ".$ScadenzaApprovazione;
				}
			}
		else{
			$LISTA3['SCAD_ETI'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
			}






		#
		#	SCADENZA APPROVAZIONE PROCEDURE DI SICUREZZA
		#
		$sql ="SELECT lov_cer.COD_CER, ID_RIF, descrizione, sk_approved_start, sk_approved_end ";
		$sql.="FROM user_schede_rifiuti ";
		$sql.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
		$sql.="WHERE ID_IMP='".$ID_IMP."' ";
		$sql.="AND sk_approved_end<NOW() ";
		$TableSpec = "user_schede_rifiuti.";
		$sql.="AND ".$TableSpec.$WORKMODE."=1 ";
		$sql.="AND ".$TableSpec."approved=1 ";
		unset($TableSpec);

		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
		if($FEDIT->DbRecsNum>0) {
			foreach($FEDIT->DbRecordSet as $k=>$dati) {
				$ScadenzaApprovazione_array = explode("-",$dati['sk_approved_end']);
				$ScadenzaApprovazione = (int)$ScadenzaApprovazione_array[2]."/".(int)$ScadenzaApprovazione_array[1]."/".(int)$ScadenzaApprovazione_array[0];
				$LISTA1['SCAD_SK'][] = "L'approvazione della procedura di sicurezza per il rifiuto ".$dati['COD_CER']." ".$dati['descrizione']." � scaduta il ".$ScadenzaApprovazione;
				}
			}
		else{
			$LISTA3['SCAD_SK'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
			}





		#
		#	SCADENZA APPROVAZIONE ANALISI
		#
		$sql ="SELECT lov_cer.COD_CER, ID_RIF, descrizione, CAR_Analisi_approved_start, CAR_Analisi_approved_end ";
		$sql.="FROM user_schede_rifiuti ";
		$sql.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
		$sql.="WHERE ID_IMP='".$ID_IMP."' ";
		$sql.="AND CAR_Analisi_approved_end<NOW() ";
		$TableSpec = "user_schede_rifiuti.";
		$sql.="AND ".$TableSpec.$WORKMODE."=1 ";
		$sql.="AND ".$TableSpec."approved=1 ";
		unset($TableSpec);

		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
		if($FEDIT->DbRecsNum>0) {
			foreach($FEDIT->DbRecordSet as $k=>$dati) {
				$ScadenzaApprovazione_array = explode("-",$dati['CAR_Analisi_approved_end']);
				$ScadenzaApprovazione = (int)$ScadenzaApprovazione_array[2]."/".(int)$ScadenzaApprovazione_array[1]."/".(int)$ScadenzaApprovazione_array[0];
				$LISTA1['SCAD_ANA'][] = "L'approvazione dell'analisi per il rifiuto ".$dati['COD_CER']." ".$dati['descrizione']." � scaduta il ".$ScadenzaApprovazione;
				}
			}
		else{
			$LISTA3['SCAD_ANA'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
			}






		if($CkContributi=='1') {
			#
			#	CONTRIBUTO ANNUALE TRASPORTATORI
			#
			$sql = "SELECT description,ID_AZT,contributo";
			$sql .= " FROM user_aziende_trasportatori ";
			$sql .= " WHERE contributo<'" . date("Y-m-d") . "' AND contributo<>'0000-00-00'";
			$sql .= " AND esenzione_contributo=0 ";
			$sql .= " AND ID_IMP='" . $ID_IMP . "'";
			$TableSpec='user_aziende_trasportatori.';
			$sql.="AND ".$TableSpec.$WORKMODE."=1 ";
			$sql.="AND ".$TableSpec."approved=1 ";
			$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
			if($FEDIT->DbRecsNum>0) {
				foreach($FEDIT->DbRecordSet as $k=>$dati) {
					$LISTA1['CONT_TRASP'][] = "Il contributo di " . $dati["description"] . " � scaduto.";
					}
				}
			else{
				$LISTA3['CONT_TRASP'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
				}
			}



		if($CkContributi=='1') {
			#
			#	CONTRIBUTO ANNUALE DESTINATARI
			#
			$sql = "SELECT description,ID_AZD,contributo";
			$sql .= " FROM user_aziende_destinatari ";
			$sql .= " WHERE contributo<'" . date("Y-m-d") . "' AND contributo<>'0000-00-00'";
			$sql .= " AND esenzione_contributo=0 ";
			$sql .= " AND ID_IMP='" . $ID_IMP . "'";
			$TableSpec='user_aziende_destinatari.';
			$sql.="AND ".$TableSpec.$WORKMODE."=1 ";
			$sql.="AND ".$TableSpec."approved=1 ";
			$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
			if($FEDIT->DbRecsNum>0) {
				foreach($FEDIT->DbRecordSet as $k=>$dati) {
					$LISTA1['CONT_DEST'][] = "Il contributo di " . $dati["description"] . " � scaduto.";
					}
				}
			else{
				$LISTA3['CONT_DEST'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
				}
			}


		if($CkContributi=='1') {
			#
			#	CONTRIBUTO ANNUALE INTERMEDIARI
			#
			$sql = "SELECT description,ID_AZI,contributo";
			$sql .= " FROM user_aziende_intermediari ";
			$sql .= " WHERE contributo<'" . date("Y-m-d") . "' AND contributo<>'0000-00-00'";
			$sql .= " AND esenzione_contributo=0 ";
			$sql .= " AND ID_IMP='" . $ID_IMP . "'";
			$TableSpec='user_aziende_intermediari.';
			$sql.="AND ".$TableSpec.$WORKMODE."=1 ";
			$sql.="AND ".$TableSpec."approved=1 ";
			$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
			if($FEDIT->DbRecsNum>0) {
				foreach($FEDIT->DbRecordSet as $k=>$dati) {
					$LISTA1['CONT_INTERM'][] = "Il contributo di " . $dati["description"] . " � scaduto.";
					}
				}
			else{
				$LISTA3['CONT_INTERM'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
				}

			}



		#########
		# PRINT #
		#########



		if(count($LISTA1)>0){
			$FEDIT->FGE_PdfBuffer->addpage();
			$Ypos=30;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(190,7,"LISTA N. 1","TLR","C");

			$Ypos+=7;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(190,7,"Controllo del superamento dei limiti di Legge o di Procedura","LBR","C");

                        #
                        #	GIORNI MODIFICA MOVIMENTI
                        #
                        if(isset($LISTA1['GGEDIT'])){
                            $Ypos+=20;
                            CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
                            if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

                            $FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
                            $FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
                            $FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
                            $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
                            $FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Giorni per la modifica dei movimenti del Registro Carico e Scarico Rifiuti","TRL","L",1);

                            $Ypos+=$LargeFont;
                            CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
                            if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
                            $FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
                            $FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
                            $FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
                            $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
                            $FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 190, comma 1-quater, D.Lgs. 152/2006.","RL","L",1);

                            $Ypos+=7;
                            CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
                            if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
                            $FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
                            $FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
                            $FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
                            $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
                            $FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2","RLB","L",1);

                            $FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

                            $Ypos+=$MediumFont;
                            $FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
                            $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
                            $FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica che il sistema sia configurato per rispettare le tempistiche circa la possibilit� di modificare i movimenti nel registro.","RLB","L");

                            $Ypos+=$SmallFont;
                            CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
                            if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
                            $FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
                            $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

                            for($a=0;$a<count($LISTA1['GGEDIT']);$a++){
                                if($a<count($LISTA1['GGEDIT'])-1){
                                    $FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['GGEDIT'][$a],"RL","L");
                                    $Ypos+=$SmallFont;
                                    CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
                                    if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
                                    $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
                                }
                                else
                                    $FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['GGEDIT'][$a],"RLB","L");
                                }
                            }


			#
			#	GENERAZIONE AUTOMATICA REGISTRO C/S
			#
			if(isset($LISTA1['NOTIFICA_REG_CS'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Generazione automatica del Registro Carico e Scarico Rifiuti","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 190, comma 1, D.Lgs. 152/2006.","RL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica se � attivo il sistema di generazione automatica del Registro di Carico e Scarico Rifiuti.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['NOTIFICA_REG_CS']);$a++){
					if($a<count($LISTA1['NOTIFICA_REG_CS'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['NOTIFICA_REG_CS'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['NOTIFICA_REG_CS'][$a],"RLB","L");
					}
				}




			#
			#	GENERAZIONE AUTOMATICA REGISTRO ADR
			#
			if(isset($LISTA1['NOTIFICA_REG_ADR'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Generazione automatica del Registro ADR","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 1, comma 1, D.M. 3/1/2011","RL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica se � attivo il sistema di generazione automatica del Registro ADR.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['NOTIFICA_REG_ADR']);$a++){
					if($a<count($LISTA1['NOTIFICA_REG_ADR'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['NOTIFICA_REG_ADR'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['NOTIFICA_REG_ADR'][$a],"RLB","L");
					}
				}

	#
	#	RICLASSIFICAZIONE REG. 1357/2014
	#
	if(isset($LISTA1['REG_1357'])){

		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Riclassificazione secondo il Regolamento UE 1357/2014","TRL","L",1);

		$Ypos+=$LargeFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: Regolamento UE 1357/2014.","RBL","L",1);

		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		$Ypos+=$MediumFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Attuazione: verifica, tra tutti i rifiuti approvati, se ve ne sono alcuni la cui classificazione � da ripetersi a seguito dell'entrata in vigore del Regolamento UE 1357/2014.","RLB","L");

		$Ypos+=$SmallFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

		for($a=0;$a<count($LISTA1['REG_1357']);$a++){
			if($a<count($LISTA1['REG_1357'])-1){
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['REG_1357'][$a],"RL","L");
				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['REG_1357'][$a],"RLB","L");
			}
		}






#
#	SCADENZA ANALISI
#
if(isset($LISTA1['ANALISI'])){

		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Scadenza analisi","TRL","L",1);

		$Ypos+=$LargeFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 8 del D.M. 5/2/1998.","RBL","L",1);

		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		$Ypos+=$MediumFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Attuazione: verifica, tra tutti i rifiuti approvati, se ve ne sono alcuni la cui analisi � priva della data di fine validit� o gi� scaduta.","RLB","L");

		$Ypos+=$SmallFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

		for($a=0;$a<count($LISTA1['ANALISI']);$a++){
			if($a<count($LISTA1['ANALISI'])-1){
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['ANALISI'][$a],"RL","L");
				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['ANALISI'][$a],"RLB","L");
			}
		}



#
#	REGISTRAZIONI CRONO SISTRI
#
if(isset($LISTA1['REG_CRONO_SISTRI'])){

		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Invio e firma delle registrazioni cronologiche a SISTRI","TRL","L",1);

		$Ypos+=$LargeFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 13, D.M. 52 del 18/02/2011 e s.m.i.","RBL","L",1);

		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		$Ypos+=$MediumFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Attuazione: verifica che tutti i movimenti pericolosi del registro fiscale siano stati inviati a SISTRI e firmati.","RLB","L");

		$Ypos+=$SmallFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

		for($a=0;$a<count($LISTA1['REG_CRONO_SISTRI']);$a++){
			if($a<count($LISTA1['REG_CRONO_SISTRI'])-1){
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['REG_CRONO_SISTRI'][$a],"RL","L");
				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['REG_CRONO_SISTRI'][$a],"RLB","L");
			}
		}



#
#	SCHEDE SISTRI
#
if(isset($LISTA1['SCHEDE_SISTRI'])){

		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Invio e firma delle schede SISTRI","TRL","L",1);

		$Ypos+=$LargeFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 13, D.M. 52 del 18/02/2011 e s.m.i.","RBL","L",1);

		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		$Ypos+=$MediumFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Attuazione: verifica che tutti i formulari di rifiuti pericolosi del registro fiscale siano stati inviati a SISTRI e firmati.","RLB","L");

		$Ypos+=$SmallFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

		for($a=0;$a<count($LISTA1['SCHEDE_SISTRI']);$a++){
			if($a<count($LISTA1['SCHEDE_SISTRI'])-1){
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['SCHEDE_SISTRI'][$a],"RL","L");
				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['SCHEDE_SISTRI'][$a],"RLB","L");
			}
		}







			#
			#	RIFIUTI ADR 6.2 MA SENZA PERSONA DI RIFERIMENTO
			#
			if(isset($LISTA1['ADR62'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Rifiuti in ADR � Classe 6.2","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo sui trasporti: art. 1, comma 1, D.M. 3/1/2011.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Attuazione: verifica, tra tutti i rifiuti approvati e sottoposti in ADR, di quelli in classe 6.2 senza l�indicazione del nominativo della persona responsabile.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['ADR62']);$a++){
					if($a<count($LISTA1['ADR62'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['ADR62'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['ADR62'][$a],"RLB","L");
					}
				}


	#
	#	Schede rifiuto in ADR (n.a.s., no 2.1.3.5.5) senza indicazione componenti pericolosi
	#
	if(isset($LISTA1['ADRNAS'])){

		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Rifiuti in ADR � Indicazione del componente pericoloso","TRL","L",1);

		$Ypos+=$LargeFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo sui trasporti: normativa ADR.","RBL","L",1);

		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		$Ypos+=$MediumFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Attuazione: verifica, tra tutti i rifiuti approvati e sottoposti in ADR, di quelli con numero ONU che richiede obbligatoriamente la specifica del componente pericoloso. Vengono esclusi i rifiuti classificati secondo 2.1.3.5.5","RLB","L");

		$Ypos+=$SmallFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

		for($a=0;$a<count($LISTA1['ADRNAS']);$a++){
			if($a<count($LISTA1['ADRNAS'])-1){
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['ADRNAS'][$a],"RL","L");
				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['ADRNAS'][$a],"RLB","L");
			}
		}



	#
	#	Formulari senza numero di formulario
	#
	if(isset($LISTA1['NONFORM'])){

		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Indicazione del numero di formulario","TRL","L",1);

		$Ypos+=$LargeFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo	ambientale: D.Lgs. 152/2006.","RBL","L",1);

		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		$Ypos+=$MediumFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Attuazione: verifica l'indicazione del numero di formulario corrispondente al movimento di scarico.","RLB","L");

		$Ypos+=$SmallFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

		for($a=0;$a<count($LISTA1['NONFORM']);$a++){
			if($a<count($LISTA1['NONFORM'])-1){
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['NONFORM'][$a],"RL","L");
				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['NONFORM'][$a],"RLB","L");
			}
		}




			#
			#	AVVISO DEPOSITO TEMPORANEO (RIF SINGOLO)
			#
			if(isset($LISTA1['DEP_TMP_SINGOLO'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Deposito temporaneo � Singolo rifiuto","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 188, comma 1, lettera n, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutti i rifiuti, di quelli che, superano (o stanno per superare) la soglia di volume/giorni prevista operativamente per il deposito temporaneo.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['DEP_TMP_SINGOLO']);$a++){
					if($a<count($LISTA1['DEP_TMP_SINGOLO'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['DEP_TMP_SINGOLO'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['DEP_TMP_SINGOLO'][$a],"RLB","L");
					}
				}




			#
			#	AVVISO DEPOSITO TEMPORANEO (SOMMA NON PERICOLOSI)
			#
			if(isset($LISTA1['DEP_TMP_NP'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Deposito Temporaneo - CER (m3 non pericolosi)","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 188, comma 1, lettera n, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutti i rifiuti, che la somma dei volumi dei rifiuti non pericolosi sia superiore a previsto per il deposito temporaneo.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['DEP_TMP_NP']);$a++){
					if($a<count($LISTA1['DEP_TMP_NP'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['DEP_TMP_NP'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['DEP_TMP_NP'][$a],"RLB","L");
					}
				}



			#
			#	AVVISO DEPOSITO TEMPORANEO (SOMMA PERICOLOSI)
			#
			if(isset($LISTA1['DEP_TMP_P'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Deposito Temporaneo - CER (m3 pericolosi)","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 188, comma 1, lettera n, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutti i rifiuti, che la somma dei volumi dei rifiuti pericolosi sia superiore a previsto per il deposito temporaneo.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['DEP_TMP_P']);$a++){
					if($a<count($LISTA1['DEP_TMP_P'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['DEP_TMP_P'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['DEP_TMP_P'][$a],"RLB","L");
					}
				}





			#
			#	AVVISO DEPOSITO TEMPORANEO (TUTTI I RIFIUTI)
			#
			if(isset($LISTA1['DEP_TMP_TUTTI'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Deposito Temporaneo - CER (m3 totali)","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 188, comma 1, lettera n, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutti i rifiuti, che la somma dei volumi di tutti i rifiuti sia superiore a previsto per il deposito temporaneo.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['DEP_TMP_TUTTI']);$a++){
					if($a<count($LISTA1['DEP_TMP_TUTTI'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['DEP_TMP_TUTTI'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['DEP_TMP_TUTTI'][$a],"RLB","L");
					}
				}




			#
			#	AVVISI DI CARICO DALL' ULTIMO CARICO
			#
			if(isset($LISTA1['AVVISO_CARICO_C'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Avvisi di carico (dall'ultimo carico)","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 190, comma 1, D.Lgs. 152/2006.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutti i rifiuti, che non siano trascorsi i giorni indicati in anagrafica dall�ultimo movimento di carico eseguito.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['AVVISO_CARICO_C']);$a++){
					if($a<count($LISTA1['AVVISO_CARICO_C'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['AVVISO_CARICO_C'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['AVVISO_CARICO_C'][$a],"RLB","L");
					}
				}





			#
			#	AVVISI DI CARICO DALL' ULTIMO SCARICO
			#
			if(isset($LISTA1['AVVISO_CARICO_S'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Avvisi di carico (dall'ultimo scarico)","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 190, comma 1, D.Lgs. 152/2006.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutti i rifiuti, che non siano trascorsi i giorni indicati in anagrafica dall�ultimo movimento di scarico eseguito.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['AVVISO_CARICO_S']);$a++){
					if($a<count($LISTA1['AVVISO_CARICO_S'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['AVVISO_CARICO_S'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['AVVISO_CARICO_S'][$a],"RLB","L");
					}
				}






			#
			#	AVVISI DI CARICO DALL' ULTIMO MOVIMENTO
			#
			if(isset($LISTA1['AVVISO_CARICO_M'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Avvisi di carico (dall'ultimo movimento)","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 190, comma 1, D.Lgs. 152/2006.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutti i rifiuti, che non siano trascorsi i giorni indicati in anagrafica dall�ultimo movimento eseguito.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['AVVISO_CARICO_M']);$a++){
					if($a<count($LISTA1['AVVISO_CARICO_M'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['AVVISO_CARICO_M'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['AVVISO_CARICO_M'][$a],"RLB","L");
					}
				}


	#
	#	AVVISI RITORNO QUARTA COPIA FORMULARIO
	#
	if(isset($LISTA1['QUARTE_COPIE'])){

		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Ritorno quarta copia formulario","TRL","L",1);

		$Ypos+=$LargeFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 193, comma 2, D.Lgs. 152/2006.","RBL","L",1);

		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera d).","RLB","L",1);

		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		$Ypos+=$MediumFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica l'effettivo rientro delle IV copie dei formulari emessi.","RLB","L");

		$Ypos+=$SmallFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

		for($a=0;$a<count($LISTA1['QUARTE_COPIE']);$a++){
			if($a<count($LISTA1['QUARTE_COPIE'])-1){
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['QUARTE_COPIE'][$a],"RL","L");
				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['QUARTE_COPIE'][$a],"RLB","L");
			}
		}



			#
			#	AUTORIZZAZIONI INTERMEDIARI
			#
			if(isset($LISTA1['AUT_I'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Autorizzazioni intermediari","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 212, comma 5, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutte le autorizzazioni degli intermediari, di quelle che sono prive della data di fine validit� o gi� scadute.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['AUT_I']);$a++){
					if($a<count($LISTA1['AUT_I'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['AUT_I'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['AUT_I'][$a],"RLB","L");
					}
				}





			if(isset($LISTA1['DOM_ISC_I'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Domanda di iscrizione intermediari","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 212, comma 5, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutte le domande di iscrizione alla categoria 8 degli intermediari, di quelle che sono prive della data di fine validit� o gi� scadute.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['DOM_ISC_I']);$a++){
					if($a<count($LISTA1['DOM_ISC_I'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['DOM_ISC_I'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['DOM_ISC_I'][$a],"RLB","L");
					}
				}


			#
			#	AUTORIZZAZIONI DESTINATARI
			#
			if(isset($LISTA1['AUT_D'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Autorizzazioni destinatari","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 208 e art. 216, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutte le autorizzazioni dei destinatari, di quelle che sono prive della data di fine validit� o gi� scadute.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['AUT_D']);$a++){
					if($a<count($LISTA1['AUT_D'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['AUT_D'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['AUT_D'][$a],"RLB","L");
					}
				}





			#
			#	AUTORIZZAZIONI TRASPORTATORI
			#
			if(isset($LISTA1['AUT_T'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Autorizzazioni trasportatori","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 212, comma 6 D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutte le autorizzazioni dei trasportatori, di quelle che sono prive della data di fine validit� o gi� scadute.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['AUT_T']);$a++){
					if($a<count($LISTA1['AUT_T'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['AUT_T'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['AUT_T'][$a],"RLB","L");
					}
				}



			#
			#	BLACK BOX AUTOMEZZI
			#
			if(isset($LISTA1['BLACK_BOX'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Verifica presenza Black Box SISTRI su automezzi","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 212, comma 9, D.Lgs. 152/2006","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica tra tutti gli automezzi dei trasportatori quelli per i quali non � stata attestata la presenza della Black Box SISTRI.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['BLACK_BOX']);$a++){
					if($a<count($LISTA1['BLACK_BOX'])-1){
						$rows = round(strlen($LISTA1['BLACK_BOX'][$a])/120);
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['BLACK_BOX'][$a],"RL","L");
						$Ypos+=$SmallFont*$rows;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
						if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['BLACK_BOX'][$a],"RLB","L");
					}
				}







			#
			#	AUTORIZZAZIONI PRODUTTORI
			#
			if(isset($LISTA1['AUT_P'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Autorizzazioni produttori","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 208, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutte le autorizzazioni dei produttori, di quelle che sono prive della data di fine validit� o gi� scadute.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['AUT_P']);$a++){
					if($a<count($LISTA1['AUT_P'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['AUT_P'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['AUT_P'][$a],"RLB","L");
					}
				}



			#
			#	PATENTI ADR
			#
			if(isset($LISTA1['PATENTI_ADR'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Patenti ADR","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo sui trasporti: art. 1, comma 1, D.M. 3/1/2011.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutte le patenti ADR degli autisti, di quelle prive della data di fine validit� o che sono gi� scadute.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['PATENTI_ADR']);$a++){
					if($a<count($LISTA1['PATENTI_ADR'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['PATENTI_ADR'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['PATENTI_ADR'][$a],"RLB","L");
					}
				}




			#
			#	AVVISI CERTIFICATO AVVENUTO SMALTIMENTO
			#
			if(isset($LISTA1['CERT_SMALT'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Ritorno certificato avvenuto smaltimento","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 188, comma 4, D.Lgs. 152/2006.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutti i formulari di rifiuti destinati a D13, D14 e D15, di quelli  per i quali non risulta registrato il rientro del certificato di avvenuto smaltimento.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['CERT_SMALT']);$a++){
					if($a<count($LISTA1['CERT_SMALT'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['CERT_SMALT'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['CERT_SMALT'][$a],"RLB","L");
					}
				}





			#
			#	AVVISI CERTIFICATO AVVENUTO SMALTIMENTO
			#
			if(isset($LISTA1['DICH_DISCARICA'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Dichiarazioni di ammissibilit� in discarica scadute","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 11, D.Lgs. 36/2003.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica di tutte le dichiarazioni di ammissibilit� in discarica, indicate nell�anagrafica del rifiuto, che risultano scadute e non pi� valide.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['DICH_DISCARICA']);$a++){
					if($a<count($LISTA1['DICH_DISCARICA'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['DICH_DISCARICA'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['DICH_DISCARICA'][$a],"RLB","L");
					}
				}



			#
			#	SCADENZA APPROVAZIONE CARATTERIZZAZIONI
			#
			if(isset($LISTA1['SCAD_CAR'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Approvazione caratterizzazioni","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art.258, comma 4, secondo periodo D.Lgs. 152/2006 e D.L. 91/2014.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera d)","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica che non sia trascorso pi� di un anno dalla data di approvazione delle caratterizzazioni dei rifiuti.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['SCAD_CAR']);$a++){
					if($a<count($LISTA1['SCAD_CAR'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['SCAD_CAR'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['SCAD_CAR'][$a],"RLB","L");
					}
				}



			#
			#	SCADENZA APPROVAZIONE CLASSIFICAZIONI
			#
			if(isset($LISTA1['SCAD_CLASS'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Approvazione classificazioni","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: D.L. 91/2014.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera d)","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica che non sia trascorso pi� di un anno dalla data di approvazione delle classificazioni dei rifiuti.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['SCAD_CLASS']);$a++){
					if($a<count($LISTA1['SCAD_CLASS'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['SCAD_CLASS'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['SCAD_CLASS'][$a],"RLB","L");
					}
				}



			#
			#	SCADENZA APPROVAZIONE ETICHETTE
			#
			if(isset($LISTA1['SCAD_ETI'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Approvazione etichette","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art.258, comma 4, secondo periodo D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera d)","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica che non sia trascorso pi� di un anno dalla data di approvazione delle etichette da apporre ai contenitori per lo stoccaggio dei rifiuti.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['SCAD_ETI']);$a++){
					if($a<count($LISTA1['SCAD_ETI'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['SCAD_ETI'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['SCAD_ETI'][$a],"RLB","L");
					}
				}





			#
			#	SCADENZA APPROVAZIONE PROCEDURE DI SICUREZZA
			#
			if(isset($LISTA1['SCAD_SK'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Approvazione procedure di sicurezza","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: Allegato D parte IV D.Lgs. 152/2006.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica che non sia trascorso pi� di un anno dalla data di approvazione delle procedure di sicurezza per i rifiuti.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['SCAD_SK']);$a++){
					if($a<count($LISTA1['SCAD_SK'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['SCAD_SK'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['SCAD_SK'][$a],"RLB","L");
					}
				}







			#
			#	SCADENZA APPROVAZIONE ANALISI
			#
			if(isset($LISTA1['SCAD_ANA'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Approvazione analisi","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art.258, comma 4, secondo periodo D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera d)","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica che non sia trascorso pi� di un anno dalla data di approvazione delle analisi dei rifiuti.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['SCAD_ANA']);$a++){
					if($a<count($LISTA1['SCAD_ANA'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['SCAD_ANA'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['SCAD_ANA'][$a],"RLB","L");
					}
				}













			#
			#	Contributi trasportatori
			#
			if(isset($LISTA1['CONT_TRASP'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Contributi trasportatori","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 1, D.M. 350/1998.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica di tutte le anagrafiche dei trasportatori per le quali non risulta pagato il contributo per l'anno corrente.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['CONT_TRASP']);$a++){
					if($a<count($LISTA1['CONT_TRASP'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['CONT_TRASP'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['CONT_TRASP'][$a],"RLB","L");
					}
				}





			#
			#	Contributi destinatari
			#
			if(isset($LISTA1['CONT_DEST'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Contributi destinatari","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 24 comma 3, D.M. 120/2014.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica di tutte le anagrafiche dei destinatari per le quali non risulta pagato il contributo per l'anno corrente.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['CONT_DEST']);$a++){
					if($a<count($LISTA1['CONT_DEST'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['CONT_DEST'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['CONT_DEST'][$a],"RLB","L");
					}
				}







			#
			#	Contributi intermediari
			#
			if(isset($LISTA1['CONT_INTERM'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Contributi intermediari","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 1 D.M. 350/1998.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica di tutte le anagrafiche degli intermediari per le quali non risulta pagato il contributo per l'anno corrente.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA1['CONT_INTERM']);$a++){
					if($a<count($LISTA1['CONT_INTERM'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['CONT_INTERM'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA1['CONT_INTERM'][$a],"RLB","L");
					}
				}





			} // CHIUDE LISTA 1


		if(count($LISTA2)>0){
			$FEDIT->FGE_PdfBuffer->addpage();
			$Ypos=30;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(190,7,"LISTA N. 2","TLR","C");

			$Ypos+=7;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(190,7,"Controllo per il preavviso del superamento dei limiti di Legge o di Procedura","LBR","C");


#
#	SCADENZA ANALISI
#
if(isset($LISTA2['ANALISI'])){

		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Scadenza analisi","TRL","L",1);

		$Ypos+=$LargeFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 8 del D.M. 5/2/1998.","RBL","L",1);

		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		$Ypos+=$MediumFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Attuazione: verifica, tra tutti i rifiuti approvati, se ve ne sono alcuni la cui analisi � prossima alla scadenza.","RLB","L");

		$Ypos+=$SmallFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

		for($a=0;$a<count($LISTA2['ANALISI']);$a++){
			if($a<count($LISTA2['ANALISI'])-1){
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['ANALISI'][$a],"RL","L");
				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['ANALISI'][$a],"RLB","L");
			}
		}


#
#	REGISTRAZIONI CRONO SISTRI
#
if(isset($LISTA2['REG_CRONO_SISTRI'])){

		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Invio e firma delle registrazioni cronologiche a SISTRI","TRL","L",1);

		$Ypos+=$LargeFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 13, D.M. 52 del 18/02/2011 e s.m.i.","RBL","L",1);

		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		$Ypos+=$MediumFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Attuazione: verifica che tutti i movimenti pericolosi del registro fiscale siano stati inviati a SISTRI e firmati.","RLB","L");

		$Ypos+=$SmallFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

		for($a=0;$a<count($LISTA2['REG_CRONO_SISTRI']);$a++){
			if($a<count($LISTA2['REG_CRONO_SISTRI'])-1){
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['REG_CRONO_SISTRI'][$a],"RL","L");
				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['REG_CRONO_SISTRI'][$a],"RLB","L");
			}
		}



#
#	SCHEDE SISTRI
#
if(isset($LISTA2['SCHEDE_SISTRI'])){

		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Invio e firma delle schede SISTRI","TRL","L",1);

		$Ypos+=$LargeFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 13, D.M. 52 del 18/02/2011 e s.m.i.","RBL","L",1);

		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		$Ypos+=$MediumFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Attuazione: verifica che tutti i formulari di rifiuti pericolosi del registro fiscale siano stati inviati a SISTRI e firmati.","RLB","L");

		$Ypos+=$SmallFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

		for($a=0;$a<count($LISTA2['SCHEDE_SISTRI']);$a++){
			if($a<count($LISTA2['SCHEDE_SISTRI'])-1){
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['SCHEDE_SISTRI'][$a],"RL","L");
				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['SCHEDE_SISTRI'][$a],"RLB","L");
			}
		}



			#
			#	ORGANIZZAZIONE DEPOSITO
			#
			if(isset($LISTA2['ORGANIZZA_DEPOSITO'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Gestione deposito rifiuti","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: procedura interna.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutti i rifiuti, di quelli la cui giacenza � pari o superiore alla portata massima dei contenitori.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA2['ORGANIZZA_DEPOSITO']);$a++){
					if($a<count($LISTA2['ORGANIZZA_DEPOSITO'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['ORGANIZZA_DEPOSITO'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['ORGANIZZA_DEPOSITO'][$a],"RLB","L");
					}
				}


			#
			#	AVVISO DEPOSITO TEMPORANEO (RIF SINGOLO)
			#
			if(isset($LISTA2['DEP_TMP_SINGOLO'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Deposito temporaneo � Singolo rifiuto","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 188, comma 1, lettera n, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutti i rifiuti, di quelli che, superano (o stanno per superare) la soglia di volume/giorni prevista operativamente per il deposito temporaneo.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA2['DEP_TMP_SINGOLO']);$a++){
					if($a<count($LISTA2['DEP_TMP_SINGOLO'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['DEP_TMP_SINGOLO'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['DEP_TMP_SINGOLO'][$a],"RLB","L");
					}
				}






			#
			#	AVVISI RITORNO QUARTA COPIA FORMULARIO
			#
			if(isset($LISTA2['QUARTE_COPIE'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Ritorno quarta copia formulario","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 193, comma 2, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera d).","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica l'effettivo rientro delle IV copie dei formulari emessi.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA2['QUARTE_COPIE']);$a++){
					if($a<count($LISTA2['QUARTE_COPIE'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['QUARTE_COPIE'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['QUARTE_COPIE'][$a],"RLB","L");
					}
				}





			#
			#	AUTORIZZAZIONI INTERMEDIARI
			#
			if(isset($LISTA2['AUT_I'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Autorizzazioni intermediari","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 212, comma 5, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutte le autorizzazioni degli intermediari, di quelle che sono prossime alla scadenza.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA2['AUT_I']);$a++){
					if($a<count($LISTA2['AUT_I'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['AUT_I'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['AUT_I'][$a],"RLB","L");
					}
				}


			#
			# DOMANDA ISCRIZIONE INTERMEDIARI
			#
			if(isset($LISTA2['DOM_ISC_I'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Domanda di iscrizione intermediari","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 212, comma 5, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutte le domande di iscrizione alla categoria 8 degli intermediari, di quelle che sono prossime alla scadenza.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA2['DOM_ISC_I']);$a++){
					if($a<count($LISTA2['DOM_ISC_I'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['DOM_ISC_I'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['DOM_ISC_I'][$a],"RLB","L");
					}
				}




			#
			#	AUTORIZZAZIONI DESTINATARI
			#
			if(isset($LISTA2['AUT_D'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Autorizzazioni destinatari","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 208 e art. 216, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutte le autorizzazioni dei destinatari, di quelle che sono prossime alla scadenza.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA2['AUT_D']);$a++){
					if($a<count($LISTA2['AUT_D'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['AUT_D'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['AUT_D'][$a],"RLB","L");
					}
				}







			#
			#	AUTORIZZAZIONI TRASPORTATORI
			#
			if(isset($LISTA2['AUT_T'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Autorizzazioni trasportatori","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 212, comma 6 D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutte le autorizzazioni dei trasportatori, di quelle che sono prossime alla scadenza.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA2['AUT_T']);$a++){
					if($a<count($LISTA2['AUT_T'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['AUT_T'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['AUT_T'][$a],"RLB","L");
					}
				}






			#
			#	AUTORIZZAZIONI PRODUTTORI
			#
			if(isset($LISTA2['AUT_P'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Autorizzazioni produttori","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 208, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutte le autorizzazioni dei produttori, di quelle che sono prossime alla scadenza.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA2['AUT_P']);$a++){
					if($a<count($LISTA2['AUT_P'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['AUT_P'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA2['AUT_P'][$a],"RLB","L");
					}
				}


			} // CHIUDE LISTA 2


		if(count($LISTA3)>0){

			$FEDIT->FGE_PdfBuffer->addpage();
			$Ypos=30;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(190,7,"LISTA N. 3","TLR","C");

			$Ypos+=7;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(190,7,"Controlli a esito positivo del superamento dei limiti di Legge o di Procedura","LBR","C");

                        #
                        #	GIORNI MODIFICA MOVIMENTI
                        #
                        if(isset($LISTA3['GGEDIT'])){

                                $Ypos+=20;
                                CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
                                if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

                                $FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
                                $FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
                                $FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
                                $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
                                $FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Giorni per la modifica dei movimenti del Registro Carico e Scarico Rifiuti","TRL","L",1);

                                $Ypos+=$LargeFont;
                                CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
                                if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
                                $FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
                                $FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
                                $FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
                                $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
                                $FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 190, comma 1-quater, D.Lgs. 152/2006.","RBL","L",1);

                                $Ypos+=7;
                                CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
                                if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
                                $FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
                                $FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
                                $FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
                                $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
                                $FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2","RLB","L",1);

                                $FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

                                $Ypos+=$MediumFont;
                                CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
                                if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
                                $FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
                                $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
                                $FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica che il sistema sia configurato per rispettare le tempistiche circa la possibilit� di modificare i movimenti nel registro.","RLB","L");

                                $Ypos+=$SmallFont;
                                CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
                                if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
                                $FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
                                $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

                                for($a=0;$a<count($LISTA3['GGEDIT']);$a++){
                                        if($a<count($LISTA3['GGEDIT'])-1){
                                                $FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['GGEDIT'][$a],"RL","L");
                                                $Ypos+=$SmallFont;
                                                CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
                                                if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
                                                $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
                                                }
                                        else
                                                $FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['GGEDIT'][$a],"RLB","L");
                                        }
                                }


			#
			#	GENERAZIONE AUTOMATICA REGISTRO C/S
			#
			if(isset($LISTA3['NOTIFICA_REG_CS'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Generazione automatica del Registro Carico e Scarico Rifiuti","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 190, comma 1, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica se � attivo il sistema di generazione automatica del Registro di Carico e Scarico Rifiuti.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['NOTIFICA_REG_CS']);$a++){
					if($a<count($LISTA3['NOTIFICA_REG_CS'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['NOTIFICA_REG_CS'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['NOTIFICA_REG_CS'][$a],"RLB","L");
					}
				}



			#
			#	GENERAZIONE AUTOMATICA REGISTRO ADR
			#
			if(isset($LISTA3['NOTIFICA_REG_ADR'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Generazione automatica del Registro ADR","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 1, comma 1, D.M. 3/1/2011","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica se � attivo il sistema di generazione automatica del Registro ADR.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['NOTIFICA_REG_ADR']);$a++){
					if($a<count($LISTA3['NOTIFICA_REG_ADR'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['NOTIFICA_REG_ADR'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['NOTIFICA_REG_ADR'][$a],"RLB","L");
					}
				}

#
#	SCADENZA ANALISI
#
if(isset($LISTA3['ANALISI'])){

		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Scadenza analisi","TRL","L",1);

		$Ypos+=$LargeFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 8 del D.M. 5/2/1998.","RBL","L",1);

		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		$Ypos+=$MediumFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Attuazione: verifica, tra tutti i rifiuti approvati, se ve ne sono alcuni la cui analisi � priva della data di fine validit�, prossima alla scadenza o scaduta.","RLB","L");

		$Ypos+=$SmallFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

		for($a=0;$a<count($LISTA3['ANALISI']);$a++){
			if($a<count($LISTA3['ANALISI'])-1){
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['ANALISI'][$a],"RL","L");
				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['ANALISI'][$a],"RLB","L");
			}
		}



#
#	REGISTRAZIONI CRONO SISTRI
#
if(isset($LISTA3['REG_CRONO_SISTRI'])){

		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Invio e firma delle registrazioni cronologiche a SISTRI","TRL","L",1);

		$Ypos+=$LargeFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 13, D.M. 52 del 18/02/2011 e s.m.i.","RBL","L",1);

		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		$Ypos+=$MediumFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Attuazione: verifica che tutti i movimenti pericolosi del registro fiscale siano stati inviati a SISTRI e firmati.","RLB","L");

		$Ypos+=$SmallFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

		for($a=0;$a<count($LISTA3['REG_CRONO_SISTRI']);$a++){
			if($a<count($LISTA3['REG_CRONO_SISTRI'])-1){
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['REG_CRONO_SISTRI'][$a],"RL","L");
				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['REG_CRONO_SISTRI'][$a],"RLB","L");
			}
		}



#
#	SCHEDE SISTRI
#
if(isset($LISTA3['SCHEDE_SISTRI'])){

		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Invio e firma delle schede SISTRI","TRL","L",1);

		$Ypos+=$LargeFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 13, D.M. 52 del 18/02/2011 e s.m.i.","RBL","L",1);

		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		$Ypos+=$MediumFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Attuazione: verifica che tutti i formulari di rifiuti pericolosi del registro fiscale siano stati inviati a SISTRI e firmati.","RLB","L");

		$Ypos+=$SmallFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

		for($a=0;$a<count($LISTA3['SCHEDE_SISTRI']);$a++){
			if($a<count($LISTA3['SCHEDE_SISTRI'])-1){
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['SCHEDE_SISTRI'][$a],"RL","L");
				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['SCHEDE_SISTRI'][$a],"RLB","L");
			}
		}



			#
			#	RIFIUTI ADR 6.2 MA SENZA PERSONA DI RIFERIMENTO
			#
			if(isset($LISTA3['ADR62'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Rifiuti in ADR � Classe 6.2","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo sui trasporti: art. 1, comma 1, D.M. 3/1/2011.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Attuazione: verifica, tra tutti i rifiuti approvati e sottoposti in ADR, di quelli in classe 6.2 senza l�indicazione del nominativo della persona responsabile.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['ADR62']);$a++){
					if($a<count($LISTA3['ADR62'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['ADR62'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['ADR62'][$a],"RLB","L");
					}
				}


	#
	#	Schede rifiuto in ADR (n.a.s., no 2.1.3.5.5) senza indicazione componenti pericolosi
	#
	if(isset($LISTA3['ADRNAS'])){

		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Rifiuti in ADR � Indicazione del componente pericoloso","TRL","L",1);

		$Ypos+=$LargeFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo sui trasporti: normativa ADR.","RBL","L",1);

		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		$Ypos+=$MediumFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Attuazione: verifica, tra tutti i rifiuti approvati e sottoposti in ADR, di quelli con numero ONU che richiede obbligatoriamente la specifica del componente pericoloso. Vengono esclusi i rifiuti classificati secondo 2.1.3.5.5","RLB","L");

		$Ypos+=$SmallFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

		for($a=0;$a<count($LISTA3['ADRNAS']);$a++){
			if($a<count($LISTA3['ADRNAS'])-1){
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['ADRNAS'][$a],"RL","L");
				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['ADRNAS'][$a],"RLB","L");
			}
		}


	#
	#	Formulari senza numero di formulario
	#
	if(isset($LISTA3['NONFORM'])){

		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Indicazione del numero di formulario","TRL","L",1);

		$Ypos+=$LargeFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo	ambientale: D.Lgs. 152/2006.","RBL","L",1);

		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		$Ypos+=$MediumFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Attuazione: verifica l'indicazione del numero di formulario corrispondente al movimento di scarico.","RLB","L");

		$Ypos+=$SmallFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

		for($a=0;$a<count($LISTA3['NONFORM']);$a++){
			if($a<count($LISTA3['NONFORM'])-1){
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['NONFORM'][$a],"RL","L");
				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['NONFORM'][$a],"RLB","L");
			}
		}




	#
	#	ORGANIZZAZIONE DEPOSITO
	#
	if(isset($LISTA3['ORGANIZZA_DEPOSITO'])){

		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Gestione deposito rifiuti","TRL","L",1);

		$Ypos+=$LargeFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: procedura interna.","RBL","L",1);

		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		$Ypos+=$MediumFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutti i rifiuti, di quelli la cui giacenza � pari o superiore alla portata massima dei contenitori.","RLB","L");

		$Ypos+=$SmallFont;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

		for($a=0;$a<count($LISTA3['ORGANIZZA_DEPOSITO']);$a++){
			if($a<count($LISTA3['ORGANIZZA_DEPOSITO'])-1){
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['ORGANIZZA_DEPOSITO'][$a],"RL","L");
				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['ORGANIZZA_DEPOSITO'][$a],"RLB","L");
			}
		}



			#
			#	AVVISO DEPOSITO TEMPORANEO (RIF SINGOLO)
			#
			if(isset($LISTA3['DEP_TMP_SINGOLO'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Deposito temporaneo � Singolo rifiuto","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 188, comma 1, lettera n, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutti i rifiuti, di quelli che, superano (o stanno per superare) la soglia di volume/giorni prevista operativamente per il deposito temporaneo.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['DEP_TMP_SINGOLO']);$a++){
					if($a<count($LISTA3['DEP_TMP_SINGOLO'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['DEP_TMP_SINGOLO'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['DEP_TMP_SINGOLO'][$a],"RLB","L");
					}
				}




			#
			#	AVVISO DEPOSITO TEMPORANEO (SOMMA NON PERICOLOSI)
			#
			if(isset($LISTA3['DEP_TMP_NP'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Deposito Temporaneo - CER (m3 non pericolosi)","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 188, comma 1, lettera n, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutti i rifiuti, che la somma dei volumi dei rifiuti non pericolosi sia superiore a previsto per il deposito temporaneo.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['DEP_TMP_NP']);$a++){
					if($a<count($LISTA3['DEP_TMP_NP'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['DEP_TMP_NP'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['DEP_TMP_NP'][$a],"RLB","L");
					}
				}





			#
			#	AVVISO DEPOSITO TEMPORANEO (TUTTI I RIFIUTI)
			#
			if(isset($LISTA3['DEP_TMP_TUTTI'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Deposito Temporaneo - CER (m3 totali)","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 188, comma 1, lettera n, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutti i rifiuti, che la somma dei volumi di tutti i rifiuti sia superiore a previsto per il deposito temporaneo.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['DEP_TMP_TUTTI']);$a++){
					if($a<count($LISTA3['DEP_TMP_TUTTI'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['DEP_TMP_TUTTI'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['DEP_TMP_TUTTI'][$a],"RLB","L");
					}
				}



			#
			#	AVVISI DI CARICO DALL' ULTIMO CARICO
			#
			if(isset($LISTA3['AVVISO_CARICO_C'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Avvisi di carico (dall'ultimo carico)","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 190, comma 1, D.Lgs. 152/2006.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutti i rifiuti, che non siano trascorsi i giorni indicati in anagrafica dall�ultimo movimento di carico eseguito.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['AVVISO_CARICO_C']);$a++){
					if($a<count($LISTA3['AVVISO_CARICO_C'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['AVVISO_CARICO_C'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['AVVISO_CARICO_C'][$a],"RLB","L");
					}
				}





			#
			#	AVVISI DI CARICO DALL' ULTIMO SCARICO
			#
			if(isset($LISTA3['AVVISO_CARICO_S'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Avvisi di carico (dall'ultimo scarico)","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 190, comma 1, D.Lgs. 152/2006.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutti i rifiuti, che non siano trascorsi i giorni indicati in anagrafica dall�ultimo movimento di scarico eseguito.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['AVVISO_CARICO_S']);$a++){
					if($a<count($LISTA3['AVVISO_CARICO_S'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['AVVISO_CARICO_S'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['AVVISO_CARICO_S'][$a],"RLB","L");
					}
				}





			#
			#	AVVISI DI CARICO DALL' ULTIMO MOVIMENTO
			#
			if(isset($LISTA3['AVVISO_CARICO_M'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Avvisi di carico (dall'ultimo movimento)","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 190, comma 1, D.Lgs. 152/2006.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutti i rifiuti, che non siano trascorsi i giorni indicati in anagrafica dall�ultimo movimento eseguito.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['AVVISO_CARICO_M']);$a++){
					if($a<count($LISTA3['AVVISO_CARICO_M'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['AVVISO_CARICO_M'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['AVVISO_CARICO_M'][$a],"RLB","L");
					}
				}





			#
			#	AVVISI RITORNO QUARTA COPIA FORMULARIO
			#
			if(isset($LISTA3['QUARTE_COPIE'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Ritorno quarta copia formulario","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 193, comma 2, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera d).","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica l'effettivo rientro delle IV copie dei formulari emessi.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['QUARTE_COPIE']);$a++){
					if($a<count($LISTA3['QUARTE_COPIE'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['QUARTE_COPIE'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['QUARTE_COPIE'][$a],"RLB","L");
					}
				}




			#
			#	AUTORIZZAZIONI INTERMEDIARI
			#
			if(isset($LISTA3['AUT_I'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Autorizzazioni intermediari","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 212, comma 5, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutte le autorizzazioni degli intermediari, di quelle che sono prive della data di fine validit�, sono gi� scadute o sono prossime alla scadenza.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['AUT_I']);$a++){
					if($a<count($LISTA3['AUT_I'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['AUT_I'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['AUT_I'][$a],"RLB","L");
					}
				}



			#
			# DOMANDA ISCRIZIONE INTERMEDIARI
			#
			if(isset($LISTA3['DOM_ISC_I'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Domanda di iscrizione intermediari","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 212, comma 5, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutte le domande di iscrizione alla categoria 8 degli intermediari, di quelle che sono prive della data di fine validit�, sono gi� scadute o sono prossime alla scadenza.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['DOM_ISC_I']);$a++){
					if($a<count($LISTA3['DOM_ISC_I'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['DOM_ISC_I'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['DOM_ISC_I'][$a],"RLB","L");
					}
				}



			#
			#	AUTORIZZAZIONI DESTINATARI
			#
			if(isset($LISTA3['AUT_D'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Autorizzazioni destinatari","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 208 e art. 216, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutte le autorizzazioni dei destinatari, di quelle che sono prive della data di fine validit�, sono gi� scadute o sono prossime alla scadenza.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['AUT_D']);$a++){
					if($a<count($LISTA3['AUT_D'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['AUT_D'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['AUT_D'][$a],"RLB","L");
					}
				}







			#
			#	AUTORIZZAZIONI TRASPORTATORI
			#
			if(isset($LISTA3['AUT_T'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Autorizzazioni trasportatori","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 212, comma 6 D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutte le autorizzazioni dei trasportatori, di quelle che sono prive della data di fine validit�, sono gi� scadute o sono prossime alla scadenza.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['AUT_T']);$a++){
					if($a<count($LISTA3['AUT_T'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['AUT_T'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['AUT_T'][$a],"RLB","L");
					}
				}



			#
			#	BLACK BOX AUTOMEZZI
			#
			if(isset($LISTA3['BLACK_BOX'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Verifica presenza Black Box SISTRI su automezzi","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 212, comma 9, D.Lgs. 152/2006","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica tra tutti gli automezzi dei trasportatori quelli per i quali non � stata attestata la presenza della Black Box SISTRI.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['BLACK_BOX']);$a++){
					if($a<count($LISTA3['BLACK_BOX'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['BLACK_BOX'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['BLACK_BOX'][$a],"RLB","L");
					}
				}






			#
			#	AUTORIZZAZIONI PRODUTTORI
			#
			if(isset($LISTA3['AUT_P'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Autorizzazioni produttori","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 208, D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera b), numero 2.","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutte le autorizzazioni dei produttori, di quelle che sono prive della data di fine validit�, sono gi� scadute o sono prossime alla scadenza.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['AUT_P']);$a++){
					if($a<count($LISTA3['AUT_P'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['AUT_P'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['AUT_P'][$a],"RLB","L");
					}
				}




			#
			#	PATENTI ADR
			#
			if(isset($LISTA3['PATENTI_ADR'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Patenti ADR","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo sui trasporti: art. 1, comma 1, D.M. 3/1/2011.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutte le patenti ADR degli autisti, di quelle prive della data di fine validit� o che sono gi� scadute.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['PATENTI_ADR']);$a++){
					if($a<count($LISTA3['PATENTI_ADR'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['PATENTI_ADR'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['PATENTI_ADR'][$a],"RLB","L");
					}
				}




			#
			#	AVVISI CERTIFICATO AVVENUTO SMALTIMENTO
			#
			if(isset($LISTA3['CERT_SMALT'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Ritorno certificato avvenuto smaltimento","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 188, comma 4, D.Lgs. 152/2006.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica, tra tutti i formulari di rifiuti destinati a D13, D14 e D15, di quelli  per i quali non risulta registrato il rientro del certificato di avvenuto smaltimento.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['CERT_SMALT']);$a++){
					if($a<count($LISTA3['CERT_SMALT'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['CERT_SMALT'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['CERT_SMALT'][$a],"RLB","L");
					}
				}




			#
			#	AVVISI CERTIFICATO AVVENUTO SMALTIMENTO
			#
			if(isset($LISTA3['DICH_DISCARICA'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Dichiarazioni di ammissibilit� in discarica scadute","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 11, D.Lgs. 36/2003.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica di tutte le dichiarazioni di ammissibilit� in discarica, indicate nell�anagrafica del rifiuto, che risultano scadute e non pi� valide.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['DICH_DISCARICA']);$a++){
					if($a<count($LISTA3['DICH_DISCARICA'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['DICH_DISCARICA'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['DICH_DISCARICA'][$a],"RLB","L");
					}
				}




			#
			#	SCADENZA APPROVAZIONE CARATTERIZZAZIONI
			#
			if(isset($LISTA3['SCAD_CAR'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Approvazione caratterizzazioni","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art.258, comma 4, secondo periodo D.Lgs. 152/2006 e D.L. 91/2014.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera d)","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica che non sia trascorso pi� di un anno dalla data di approvazione delle caratterizzazioni dei rifiuti.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['SCAD_CAR']);$a++){
					if($a<count($LISTA3['SCAD_CAR'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['SCAD_CAR'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['SCAD_CAR'][$a],"RLB","L");
					}
				}




			#
			#	SCADENZA APPROVAZIONE CLASSIFICAZIONI
			#
			if(isset($LISTA3['SCAD_CLASS'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Approvazione classificazioni","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: D.L. 91/2014.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera d)","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica che non sia trascorso pi� di un anno dalla data di approvazione delle classificazioni dei rifiuti.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['SCAD_CLASS']);$a++){
					if($a<count($LISTA3['SCAD_CLASS'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['SCAD_CLASS'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['SCAD_CLASS'][$a],"RLB","L");
					}
				}




			#
			#	SCADENZA APPROVAZIONE ETICHETTE
			#
			if(isset($LISTA3['SCAD_ETI'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Approvazione etichette","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art.258, comma 4, secondo periodo D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera d)","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica che non sia trascorso pi� di un anno dalla data di approvazione delle etichette da apporre ai contenitori per lo stoccaggio dei rifiuti.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['SCAD_ETI']);$a++){
					if($a<count($LISTA3['SCAD_ETI'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['SCAD_ETI'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['SCAD_ETI'][$a],"RLB","L");
					}
				}





			#
			#	SCADENZA APPROVAZIONE PROCEDURE DI SICUREZZA
			#
			if(isset($LISTA3['SCAD_SK'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Approvazione procedure di sicurezza","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: Allegato D parte IV D.Lgs. 152/2006.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica che non sia trascorso pi� di un anno dalla data di approvazione delle procedure di sicurezza per i rifiuti.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['SCAD_SK']);$a++){
					if($a<count($LISTA3['SCAD_SK'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['SCAD_SK'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['SCAD_SK'][$a],"RLB","L");
					}
				}







			#
			#	SCADENZA APPROVAZIONE ANALISI
			#
			if(isset($LISTA3['SCAD_ANA'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Approvazione analisi","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art.258, comma 4, secondo periodo D.Lgs. 152/2006.","RBL","L",1);

				$Ypos+=7;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento Legge 231/2001: art. 25 undecies, comma 2, lettera d)","RLB","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica che non sia trascorso pi� di un anno dalla data di approvazione delle analisi dei rifiuti.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['SCAD_ANA']);$a++){
					if($a<count($LISTA3['SCAD_ANA'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['SCAD_ANA'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['SCAD_ANA'][$a],"RLB","L");
					}
				}




			#
			#	Contributi trasportatori
			#
			if(isset($LISTA3['CONT_TRASP'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Contributi trasportatori","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 1, D.M. 350/1998.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica di tutte le anagrafiche dei trasportatori per le quali non risulta pagato il contributo per l'anno corrente.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['CONT_TRASP']);$a++){
					if($a<count($LISTA3['CONT_TRASP'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['CONT_TRASP'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['CONT_TRASP'][$a],"RLB","L");
					}
				}





			#
			#	Contributi destinatari
			#
			if(isset($LISTA3['CONT_DEST'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Contributi destinatari","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 24 comma 3, D.M. 120/2014.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica di tutte le anagrafiche dei destinatari per le quali non risulta pagato il contributo per l'anno corrente.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['CONT_DEST']);$a++){
					if($a<count($LISTA3['CONT_DEST'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['CONT_DEST'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['CONT_DEST'][$a],"RLB","L");
					}
				}







			#
			#	Contributi intermediari
			#
			if(isset($LISTA3['CONT_INTERM'])){

				$Ypos+=20;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$LargeFont,"Contributi intermediari","TRL","L",1);

				$Ypos+=$LargeFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$MediumFont);
				$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
				$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$MediumFont,"Riferimento normativo ambientale: art. 1 D.M. 350/1998.","RBL","L",1);

				$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

				$Ypos+=$MediumFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,"Condizione: verifica di tutte le anagrafiche degli intermediari per le quali non risulta pagato il contributo per l'anno corrente.","RLB","L");

				$Ypos+=$SmallFont;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$SmallFont);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);

				for($a=0;$a<count($LISTA3['CONT_INTERM']);$a++){
					if($a<count($LISTA3['CONT_INTERM'])-1){
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['CONT_INTERM'][$a],"RL","L");
						$Ypos+=$SmallFont;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
						if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
						$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
						}
					else
						$FEDIT->FGE_PdfBuffer->MultiCell(190,$SmallFont,$LISTA3['CONT_INTERM'][$a],"RLB","L");
					}
				}



			} // CHIUDE LISTA 3

		//echo '__alarm/'.$ID_IMP.'/'.$USER.'/'.$DcName.'.pdf<hr>';
		$FEDIT->FGE_PdfBuffer->Output('__alarm/'.$ID_IMP.'/'.$USER.'/'.$DcName.'.pdf','F');
		$FEDIT->FGE_PdfBuffer = NULL;

		// Stabilimento
		$sql = "SELECT core_impianti.description AS Impianto, lov_comuni_istat.description AS Comune FROM core_impianti JOIN lov_comuni_istat ON core_impianti.ID_COM=lov_comuni_istat.ID_COM WHERE ID_IMP='".$ID_IMP."';";
		$FEDIT->SDBRead($sql,"DbRecordSetStabilimento",true,false);
		$Stabilimento = $FEDIT->DbRecordSetStabilimento[0]['Impianto'] . ' di ' . $FEDIT->DbRecordSetStabilimento[0]['Comune'];

		## SEND NOTIFICATION MAIL TO USER
                $mail = new SogerMailer();
                $mail->isHTML(true);
                $mail->setFrom('cron@sogerpro.it', 'MEMO So.Ge.R. PRO');
                $mail->addAddress($MAIL);
                $mail->Subject  = 'Generata la lista di controllo allarmi '.$WM_global[$w];
		$message    = 'Gentile utente, <br /><br />';
		$message    .= '<b>la lista di controllo allarmi per il profilo d\'uso '.$WM_global[$w].' dello stabilimento '.$Stabilimento.' � stata generata in formato pdf.</b><br /><br />';
		$message    .= 'Il file � ora scaricabile all\'indirizzo <a href="https://www.sogerpro.it/soger/index.php?App=documents">https://www.sogerpro.it/soger/index.php?App=documents</a>.<br /><br />';
		$message    .= 'Per entrare � necessario eseguire il login.<br /><br />';
		$message    .= 'Questa e-mail � generata automaticamente da So.Ge.R. PRO, si prega di non rispondere.<br /><br />';
		$message    .= 'Per qualsiasi segnalazione o necessit� di assistenza, Vi invitiamo all\'utilizzo del servizio ticket apposito.<br /><br />';
		$message    .= 'ID_IMP: '.$ID_IMP.'<br />';
		$message    .= 'ID_USR: '.$USER.'<br />';
		//$message  = utf8_encode($message);
                $mail->Body = $message;

		if(!$mail->send()){
                    $NoticeMail = new SogerMailer();
                    $NoticeMail->isHTML(true);
                    $NoticeMail->setFrom('cron@sogerpro.it', 'MEMO So.Ge.R. PRO');
                    $NoticeMail->addAddress('programmazione@sintem.it');
                    $NoticeMail->Subject = 'ERRORE INVIO MEMO';
                    $NoticeMail->Body = 'Errore nell\'invio della mail di notifica a '.$MAIL.' ('.$ID_IMP.' - '.$WM_global[$w] . ') da parte di '.$_SERVER["PHP_SELF"];
                    $NoticeMail->send();
                    }

		}
	}

###############################

function CheckYPos(&$Ypos,&$PdfObj,$asCSV=false) {
//{{{
if($asCSV) {
	return;
}
if($PdfObj->CurOrientation=="P") {
	if($Ypos>260) {
		$PdfObj->addpage();
		$Ypos = 10;
	}
} else {
	if($Ypos>170) {
		$PdfObj->addpage();
		$Ypos = 10;
	}
}
	//}}}
}


function CheckCarichi($rif, $CarichiRifiuto){

	if(isset($rif['totS'])) $TotS=$rif['totS']; else $TotS=0;
	$giacOK=false;
	if( ($rif['totC']+$rif['giac_ini']) >$TotS AND ( isset($rif['carico']) ) ){

		# 0 - giacenza iniziale
		if($rif['giac_ini']>0 && !$giacOK){
			$CarichiRifiuto[$rif['RifID']][0]['qta']=$rif['giac_ini'];
			$CarichiRifiuto[$rif['RifID']][0]['num']=0;
			//$CarichiRifiuto[$rif['RifID']][0]['data']="0000-00-00";
			$CarichiRifiuto[$rif['RifID']][0]['data']=date("Y")."-01-01";
			$giacOK=true;
			}
		# 1 - array carichi rifiuto
		if(isset($rif['carico'])){
			for($k=0;$k<count($rif['carico']);$k++){
					if(isset($CarichiRifiuto[$rif['RifID']])) $index=count($CarichiRifiuto[$rif['RifID']]); else $index=0;
					$CarichiRifiuto[$rif['RifID']][$index]['qta']=$rif['carico'][$k]['qta'];
					$CarichiRifiuto[$rif['RifID']][$index]['num']=$rif['carico'][$k]['num'];
					$CarichiRifiuto[$rif['RifID']][$index]['data']=$rif['carico'][$k]['data'];
				}
			}
		# 2 - sottraggo qta da scaricato fino a quando scaricato resta positivo
		for($i=0;$i<count($CarichiRifiuto[$rif['RifID']]);$i++){
//			print_r("TotS=".var_dump($TotS)." tolgo ".var_dump($CarichiRifiuto[$rif['RifID']][$i]['qta'])."<br />");
			if($TotS>=0){
				$TotS=round($TotS, 2)-round($CarichiRifiuto[$rif['RifID']][$i]['qta'], 2);
//			print_r("TotS=".var_dump($TotS)."<br />");
				if($TotS<0)
					$lastCarico=$CarichiRifiuto[$rif['RifID']][$i]['num'];
					$lastCaricoData=$CarichiRifiuto[$rif['RifID']][$i]['data'];
				}
			}


		if(isset($lastCarico) && !is_null($lastCarico)){
			return(@$lastCarico."|".@$lastCaricoData."|".abs(@$TotS)."|".$rif["COD_CER"] . " - " . $rif["descrizione"]."|".$rif["t_limite"]."|".$rif["RifID"]);
			//print_r("RIF ".$rif['RifID'].", devo scaricare dal carico: ".$lastCarico. " Kg ".abs($TotS)."<br/>\n\r");
			}
		}
	}

// le date devono essere in formato m/d/Y
function dateDiff($interval,$dateTimeBegin,$dateTimeEnd) {
//{{{
	$dateTimeBegin=strtotime($dateTimeBegin);
	$dateTimeEnd=strtotime($dateTimeEnd);

	if($dateTimeEnd === -1 | $dateTimeBegin===-1) {
		return("either start or end date is invalid");
	}
	$dif=$dateTimeEnd - $dateTimeBegin;
	switch($interval) {
		case "d"://days
		return(floor($dif/86400)); //86400s=1d
		case "ww"://Week
		return(floor($dif/604800)); //604800s=1week=1semana
		case "m": //similar result "m" dateDiff Microsoft
		$monthBegin=(date("Y",$dateTimeBegin)*12)+
		date("n",$dateTimeBegin);
		$monthEnd=(date("Y",$dateTimeEnd)*12)+
		date("n",$dateTimeEnd);
		$monthDiff=$monthEnd-$monthBegin;
		return($monthDiff);
		case "yyyy": //similar result "yyyy" dateDiff Microsoft
		return(date("Y",$dateTimeEnd) - date("Y",$dateTimeBegin));
		default:
		return(floor($dif/86400)); //86400s=1d
	} //}}}
}



function toM3($in,$Um,$PesoSpec) {
//{{{
	if($in=="0") {
		return 0;
	}
	switch($Um) {
		case "3": # m3
			return $in;
		break;
		case "2": # Litri->M3
			$tmp = $in*$PesoSpec;
			return round(($tmp/$PesoSpec/1000),2);
		break;
		case "1": # Kg->M3
			# pezza
			if($PesoSpec==0) $PesoSpec=1;
			# fine pezza
			return round(($in/$PesoSpec/1000),2);
		break;
	} //}}}
}
?>
