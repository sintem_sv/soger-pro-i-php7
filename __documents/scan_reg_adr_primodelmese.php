<?php

set_time_limit(0)

error_reporting(E_ALL);
ini_set("display_errors", 1);

//session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__libs/zip3.php");
require_once("../__libs/fpdf.php");
require_once("../__classes/SogerMailer.php");

$mail = new SogerMailer();
$mail->isHTML(true);
$mail->setFrom('cron@sogerpro.it', 'MEMO So.Ge.R. PRO');
$mail->addAddress('programmazione@sintem.it');
$mail->Subject  = 'INIZIO INVIO MEMO ADR';
$message    = 'Inizio!';
$mail->Body = $message;
$mail->send();


## CONNECTION TO DATABASE
$FEDIT=new DbLink();
$FEDIT->DbConn('soger'.date('Y'));
$AnnoDB		= date('Y');



## RETRIVE USER TO GENERATE REG
$SQL="SELECT DISTINCT ID_USR, mail FROM user_promemoria WHERE id_prom=2 AND id_fprom=2 AND mail<>'' AND mail IS NOT NULL;";
$FEDIT->SdbRead($SQL,"DbRecordSetUserData",true,false);

## CYCLE USER
for($i=0;$i<count($FEDIT->DbRecordSetUserData);$i++){

	$USER=$FEDIT->DbRecordSetUserData[$i]['ID_USR'];
	$MAIL=$FEDIT->DbRecordSetUserData[$i]['mail'];

	# ID_IMP
	$SQL="SELECT ID_IMP FROM core_users WHERE ID_USR=".$USER.";";
	$FEDIT->SdbRead($SQL,"DbRecordSetID_IMP",true,false);
	$ID_IMP = $FEDIT->DbRecordSetID_IMP[0]['ID_IMP'];

	# workmode
	$SQL="SELECT produttore, trasportatore, destinatario, intermediario FROM core_impianti WHERE ID_IMP='".$ID_IMP."';";
	$FEDIT->SdbRead($SQL,"DbRecordSetWM",true,false);

	$WM_global = Array();

	# reg. produttore
	if($FEDIT->DbRecordSetWM[0]['produttore']=='1'){
		$WM				= 'produttore';
		array_push($WM_global, $WM);
		}
	# reg. trasportatore
	if($FEDIT->DbRecordSetWM[0]['trasportatore']=='1'){
		$WM				= 'trasportatore';
		array_push($WM_global, $WM);
		}
	# reg. destinatario
	if($FEDIT->DbRecordSetWM[0]['destinatario']=='1'){
		$WM				= 'destinatario';
		array_push($WM_global, $WM);
		}
	# reg. intermediario
	if($FEDIT->DbRecordSetWM[0]['intermediario']=='1'){
		$WM				= 'intermediario';
		array_push($WM_global, $WM);
		}

	for($w=0;$w<count($WM_global);$w++){

		$Month		= array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
		$Mese		= array("Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre");
		$WORKMODE	= $WM_global[$w];

		$PesoAnno	= 0;
		$FirAnno	= 0;

		// doc name
		$DcName	= "Registro_ADR_".$WM_global[$w]."_al_".date('d-m-Y');
		
		// pagination
		$orientation="P";	
		$um="mm";
		$Format = array(210,297);
		$ZeroMargin = true;
		$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin,$DcName);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',12);
	
		$Ypos		= 30;
		$Xpos		= 10;

		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"REGISTRO ADR ".$AnnoDB,"TLBR","C");
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

		for($m=0;$m<count($Month);$m++){
			$sql ="SELECT DTFORM, NFORM, lov_num_onu.description AS ONU, SUBSTRING(des, 1, 100) as des, imb, pesoN FROM user_movimenti_fiscalizzati ";
			$sql.="JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF = user_schede_rifiuti.ID_RIF ";
			$sql.="JOIN lov_cer ON lov_cer.ID_CER = user_schede_rifiuti.ID_CER ";
			$sql.="JOIN lov_num_onu ON lov_num_onu.ID_ONU = user_schede_rifiuti.ID_ONU ";
			$sql.="WHERE user_movimenti_fiscalizzati.ID_IMP = '".$ID_IMP."' ";
			$sql.="AND user_movimenti_fiscalizzati.adr = 1 ";
			$sql.="AND user_movimenti_fiscalizzati.".$WORKMODE." = 1 ";
			$sql.="AND MONTHNAME(DTFORM) = '".$Month[$m]."' ";
			$sql.="AND NMOV<>9999999 ";
			$sql.="ORDER BY DTFORM;";
			$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

			$PesoDelMese	= 0;
			$FirDelMese		= 0;
				
			$Ypos+=20;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
			$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
			$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(190,7,$Mese[$m],"TRBL","L",1);
			
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',6);
			$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

			// intestazione colonne
			$Ypos+=7;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,7,"Data","TBRL","C");
			$Xpos+=20;
			
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,7,"Formulario","TBRL","C");
			$Xpos+=20;
			
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(120,7,"N. ONU","TBRL","L");
			$Xpos+=120;
			
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(30,7,"Peso netto","TBRL","R");
			$Xpos=10;


			if(isset($FEDIT->DbRecordSet)){
				for($r=0;$r<count($FEDIT->DbRecordSet);$r++){
					$Ypos+=7;
					CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
					
					
					$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
					$DTFORM_array = explode("-",$FEDIT->DbRecordSet[$r]['DTFORM']);
					$DTFORM=$DTFORM_array[2]."/".$DTFORM_array[1]."/".$DTFORM_array[0];
					$FEDIT->FGE_PdfBuffer->MultiCell(20,7,$DTFORM,"TBRL","C");
					$Xpos+=20;
					
					$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(20,7,$FEDIT->DbRecordSet[$r]['NFORM'],"TBRL","C");
					$Xpos+=20;
					
					$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(120,7,$FEDIT->DbRecordSet[$r]['ONU'] . " - " . $FEDIT->DbRecordSet[$r]['des'] . "..., " . $FEDIT->DbRecordSet[$r]['imb'],"TBRL","L");
					$Xpos+=120;
					
					$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(30,7,$FEDIT->DbRecordSet[$r]['pesoN']." kg","TBRL","R");
					$Xpos=10;

					$PesoDelMese+=$FEDIT->DbRecordSet[$r]['pesoN'];
					$FirDelMese++;

					}
				}
			
			// totale mese
			$Ypos+=7;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

			$Xpos=10;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(95,7,"TOTALE","TBRL","C");
			
			$Xpos+=95;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(47.5,7,$FirDelMese." formulari","TBRL","C");
			
			$Xpos+=47.5;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(47.5,7,$PesoDelMese." kg","TBRL","C");
				
			$PesoAnno+=$PesoDelMese;
			$FirAnno+=$FirDelMese;
			
			$Xpos=10;
			}


		// totale anno

		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,7,"ANNO ".date('Y'),"TRBL","L",1);
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',6);
		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		$Xpos=10;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(95,7,"TOTALE","TBRL","C");

		$Xpos+=95;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(47.5,7,$FirAnno." formulari","TBRL","C");

		$Xpos+=47.5;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(47.5,7,$PesoAnno." kg","TBRL","C");

		$Xpos=10;

		$FEDIT->FGE_PdfBuffer->Output('__adr/'.$ID_IMP.'/'.$USER.'/'.$DcName.'.pdf','F');
		$FEDIT->FGE_PdfBuffer = NULL;
		
		// Stabilimento
		$sql = "SELECT core_impianti.description AS Impianto, lov_comuni_istat.description AS Comune FROM core_impianti JOIN lov_comuni_istat ON core_impianti.ID_COM=lov_comuni_istat.ID_COM WHERE ID_IMP='".$ID_IMP."';";
		$FEDIT->SDBRead($sql,"DbRecordSetStabilimento",true,false);
		$Stabilimento = $FEDIT->DbRecordSetStabilimento[0]['Impianto'] . ' di ' . $FEDIT->DbRecordSetStabilimento[0]['Comune'];

		## SEND NOTIFICATION MAIL TO USER
		$mail = new SogerMailer();
                $mail->isHTML(true);
                $mail->setFrom('cron@sogerpro.it', 'MEMO So.Ge.R. PRO');
                $mail->addAddress($MAIL);
                $mail->Subject  = 'Generato il registro ADR '.$WM_global[$w];
		$message    = 'Gentile utente, <br /><br />';
		$message    .= '<b>Il registro ADR per il profilo d\'uso '.$WM_global[$w].' dello stabilimento '.$Stabilimento.' � stato generato in formato pdf.</b><br /><br />';
		$message    .= 'Il file del registro � ora scaricabile all\'indirizzo <a href="https://www.sogerpro.it/soger/index.php?App=documents">https://www.sogerpro.it/soger/index.php?App=documents</a>.<br /><br />';
		$message    .= 'Per entrare � necessario eseguire il login.<br /><br />';
		$message    .= 'Questa e-mail � generata automaticamente da So.Ge.R. PRO, si prega di non rispondere.<br /><br />';
		$message    .= 'Per qualsiasi segnalazione o necessit� di assistenza, Vi invitiamo all\'utilizzo del servizio ticket apposito.<br /><br />';
        $message    .= 'ID_IMP: '.$ID_IMP.'<br />';
		$message    .= 'ID_USR: '.$USER.'<br />';
                //$message  = utf8_encode($message);
                $mail->Body = $message;
                
                if(!$mail->send()){
                    $NoticeMail = new SogerMailer();
                    $NoticeMail->isHTML(true);
                    $NoticeMail->setFrom('cron@sogerpro.it', 'MEMO So.Ge.R. PRO');
                    $NoticeMail->addAddress('programmazione@sintem.it');
                    $NoticeMail->Subject = 'ERRORE INVIO MEMO';
                    $NoticeMail->Body = 'Errore nell\'invio della mail di notifica a '.$MAIL.' ('.$ID_IMP.' - '.$WM_global[$w] . ') da parte di '.$_SERVER["PHP_SELF"];
                    $NoticeMail->send();
                    }

		}
	}

$mail = new SogerMailer();
$mail->isHTML(true);
$mail->setFrom('cron@sogerpro.it', 'MEMO So.Ge.R. PRO');
$mail->addAddress('programmazione@sintem.it');
$mail->Subject  = 'FINE INVIO MEMO ADR';
$message    = 'Fine!';
$mail->Body = $message;
$mail->send();

###############################

function CheckYPos(&$Ypos,&$PdfObj,$asCSV=false) {
//{{{ 
if($asCSV) {
	return;	
}
if($PdfObj->CurOrientation=="P") {
	if($Ypos>260) {
		$PdfObj->addpage();
		$Ypos = 10;
	} 
} else {
	if($Ypos>170) {
		$PdfObj->addpage();
		$Ypos = 10;
	} 
}
	//}}}
}




?>