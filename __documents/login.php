<?php

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");

$FEDIT->FGE_UseTables(array("core_users"));
$FEDIT->FGE_SetSelectFields(array("ID_USR","usr","pwd","superpwd","description","nome","cognome","email","telefono","sadmin","manutenzione","bloccatoEcostudio", "produttore","trasportatore","destinatario","intermediario","ID_IMP"),"core_users");

// PERMESSI OPERATORE
$FEDIT->FGE_SetSelectFields(array("O1","O2","O3","O4","O5","O6","O7","O8","O9","O10","O11","O12"),"core_users");
// PERMESSI GESTORE
$FEDIT->FGE_SetSelectFields(array("G1","G2","G3","G4","G5","G6","G7","G8","G9","G10","G11","G12","G13","G14"),"core_users");
// PERMESSI DELEGATO
$FEDIT->FGE_SetSelectFields(array("D1","D2","D3","D4","D5","D6","D7"),"core_users");
// PERMESSI RESPONSABILE
$FEDIT->FGE_SetSelectFields(array("R1","R2","R3","R4","R5","R6","R7"),"core_users");


$FEDIT->FGE_SetSelectFields(array("FRM_DefaultNFORM","FRM_DIS_PRO","FRM_DIS_DES","FRM_DIS_TRA","FRM_DIS_INT","FRM_DIS_QNT","FRM_DefaultVD","FRM_ADR","FRM_SET_HR","FRM_SET_DATE","CHECK_DESTINATARIO","CHECK_TRASPORTATORE","CHECK_MOV_ORDER","CHECK_DOUBLE_FIR","MAP_showInd"),"core_users");
$FEDIT->FGE_SetSelectFields(array("FRM_PRNT_X","FRM_PRNT_Y","FRM_FONT_ID","FRM_FONT_SIZE_ID","REG_PRNT_X","REG_PRNT_Y","REG_DIS_PROD","REG_NOMECER","REG_RS_CARICHI","REG_PS_DEST","REG_IDSIS_SCHEDA","REG_NUMERO_SCHEDA","NOTER_SISTRI","NOTEF_SISTRI","reg_trasp_doublecheck"),"core_users");
$FEDIT->FGE_SetSelectFields(array("ck_ADR","ck_validazione_conferimento","ck_RIT_FORM","ck_RIT_FORMgg","FRM_PRINT_CER","FRM_PRINT_CONTO_TERZI"),"core_users");
$FEDIT->FGE_SetSelectFields(array("ck_AUT","ck_CNTR","ck_AUTgg","ck_ANALISI","ck_ANALISIgg","ck_MOV_SISTRI","ck_MOV_SISTRIgg","ck_DEP_TEMP","ck_DEP_TEMPgg"),"core_users");
$FEDIT->FGE_SetSelectFields(array("q_limite","q_limite_p","NMOVDEF","NMOVSHOW","via_diretta","OPRDsec","AUT_SCAD_LOCK","FDA_LOCK","CONTO_TERZI_LOCK","CONTO_TERZI_CHECK","AutistaOTF","TargheOTF","CER_AXT","PRNT_CLASSIH"),"core_users");
$FEDIT->FGE_SetSelectFields(array("REG_QNT_M","REG_QNT_L","ID_UMIS","ID_UMIS_INT","UMIS_RIF","FANGHI","mov_fiscalize_merge","mov_fiscalize_order","mov_fiscalize_rif","ID_FISC_PROD","ID_FISC_DEST","ID_FISC_TRASP","ID_FISC_INTERM","ID_DAY","ID_GDEP","GGedit_produttore","GGedit_trasportatore", "GGedit_destinatario","GGedit_intermediario","RESTORE_NMOV","FRM_pericolosi","FRM_LAYOUT_DEST","FRM_LAYOUT_SCARICO_INTERNO","REG_PROD_NOTEC"),"core_users");

#SISTRI
$FEDIT->FGE_SetSelectFields(array("SIS_identity","SIS_Nome_Delegato","SIS_Cognome_Delegato","idSIS_sede", "sede_description", "idSIS_regCrono", "regCrono_type"),"core_users");


$FEDIT->FGE_PostListen();



$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);



// strcasecmp serve a mantenere il login case insensitive

## LOGIN FALLITO
if($FEDIT->DbRecsNum==0 | $FEDIT->DbRecsNum>1 | ( strcasecmp($FEDIT->DbRecordSet[0]['core_usersusr'],$_POST['core_users:usr'])!=0 | strcasecmp($FEDIT->DbRecordSet[0]['core_userspwd'],$_POST['core_users:pwd'])!=0 ) ){

## SOLO PER SACLA'...
//if($FEDIT->DbRecsNum==0 | $FEDIT->DbRecsNum>1){
	
	## 0 RECORD NEL DB
	if($FEDIT->DbRecsNum==0) {
		$SOGER->SetFeedback("nome utente / password errati","1");
		$SOGER->AppLocation ="login";
		require_once("../__includes/COMMON_sleepForgEdit.php");
		require_once("../__includes/COMMON_sleepSoger.php");
		header("Location: ../__documents/index.php");
		} 
	
	## + DI 1 RECORD NEL DB
	if($FEDIT->DbRecsNum>1) {
		$SOGER->SetFeedback("combinazione utente / password non univoca, contattare l' assistenza","1");
		$SOGER->AppLocation ="login";
		require_once("../__includes/COMMON_sleepForgEdit.php");
		require_once("../__includes/COMMON_sleepSoger.php");
		header("Location: ../__documents/index.php");
		} 
	
	## COMBO USR/PWD NON CORRISPONDE A USR/PWD RESTITUITI DA LETTURA DB ( apparentemente inspiegabile.. caso Carnini )..
	if(	strcasecmp($FEDIT->DbRecordSet[0]['core_usersusr'],$_POST['core_users:usr'])!=0 | strcasecmp($FEDIT->DbRecordSet[0]['core_userspwd'],$_POST['core_users:pwd'])!=0 ){
		$SOGER->SetFeedback("errore lettura database, ritentare il login - se il problema persiste contattare l'assistenza","1");
		$SOGER->AppLocation ="login";
		require_once("../__includes/COMMON_sleepForgEdit.php");
		require_once("../__includes/COMMON_sleepSoger.php");
		header("Location: ../__documents/index.php");
		}

	}

## LOGIN RIUSCITO
else {

	if($FEDIT->DbRecordSet[0]["core_usersmanutenzione"]==1) {
		$SOGER->SetFeedback("account bloccato per manutenzione","1");
		$SOGER->AppLocation ="login_documents";
		require_once("../__includes/COMMON_sleepForgEdit.php");
		require_once("../__includes/COMMON_sleepSoger.php");
		header("Location: ../__documents/index.php");
		}
	if($FEDIT->DbRecordSet[0]["core_usersbloccatoEcostudio"]==1) {
		$SOGER->utenteInsolvente();
		$SOGER->AppLocation ="login";
		require_once("../__includes/COMMON_sleepForgEdit.php");
		require_once("../__includes/COMMON_sleepSoger.php");
		header("Location: ../");
		}
	foreach($FEDIT->DbRecordSet as $idx=>$values) {
		foreach($values as $key=>$content) {
			$SOGER->UserData[$key] = $content;
			}
		}
	if($SOGER->UserData["core_userssadmin"]=="1") {
		$SOGER->AppLocation ="AdminMenu";
		} 
	else {
		
		$FEDIT->FGE_FlushTableInfo();
		$FEDIT->FGE_UseTables(array("core_impianti","core_gruppi","core_intestatari"));
		$FEDIT->FGE_SetSelectFields(array("ID_IMP","description","codfisc","produttore","trasportatore","destinatario","intermediario","ente","SUPER_USER","REG_IND", "MODULO_SIS", "MODULO_FDA", "MODULO_WMS", "MODULO_ECO", "MODULO_LDP", "MUD_PS_DEST"),"core_impianti");
		$FEDIT->FGE_SetSelectFields("description","core_gruppi");
		$FEDIT->FGE_SetSelectFields(array("description","codfisc"),"core_intestatari");
		$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_usersID_IMP"],"core_impianti");
		$FEDIT->FGE_DescribeFields();
		$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,true);

		$SOGER->UserData = array_merge($SOGER->UserData, $FEDIT->DbRecordSet[0]);
		# sets default user profile
		if($SOGER->UserData["core_impiantiproduttore"]=="1") {
			$SOGER->UserData["workmode"] = "produttore";
			} elseif ($SOGER->UserData["core_impiantitrasportatore"]=="1") {
				$SOGER->UserData["workmode"] = "trasportatore";	
				} elseif($SOGER->UserData["core_impiantidestinatario"]=="1") {
					$SOGER->UserData["workmode"] = "destinatario";
					}elseif($SOGER->UserData["core_impiantiintermediario"]=="1") {
						$SOGER->UserData["workmode"] = "intermediario";
						}
		# sets default user SISTRI preferences
//		if(!is_null($SOGER->UserData['core_usersidSIS_sede']))
//			$SOGER->UserData['idSIS_sede']=$SOGER->UserData['core_usersidSIS_sede'];
//		if(!is_null($SOGER->UserData['core_usersidSIS_regCrono']))
//			$SOGER->UserData['idSIS_regCrono']=$SOGER->UserData['core_usersidSIS_regCrono'];
//		if(!is_null($SOGER->UserData['core_usersregCrono_type']))
//			$SOGER->UserData['regCrono_type']=$SOGER->UserData['core_usersregCrono_type'];

		$SOGER->AppLocation ="DownloadMenuFromLogin";
	}

	//die(var_dump($SIS));

	require_once("../__includes/COMMON_sleepForgEdit.php");
	require_once("../__includes/COMMON_sleepSoger.php");
	//require_once("../__includes/COMMON_sleepSIS.php");
	header("Location: ../");
	}

?>