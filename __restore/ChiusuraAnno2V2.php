<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('max_execution_time', 900); // 15 min

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/zip3.php");
//require_once("../__scripts/MUD_funct.php"); //?

global $SOGER;
//$AnnoCorrente = date('Y');
$AnnoCorrente = '2022';


function RemoveCarichiScaricati($rif, $CarichiRifiuto){

    if(isset($rif['totS'])) $TotS=$rif['totS']; else $TotS=0;
    $giacOK=false;
    $PrimoDaScaricare=0;

    # 0 - giacenza iniziale
    if($rif['giac_ini']>0 && !$giacOK){
        $CarichiRifiuto[$rif['RifID']][0]['qta']=$rif['giac_ini'];
        $CarichiRifiuto[$rif['RifID']][0]['num']=0;
        $CarichiRifiuto[$rif['RifID']][0]['ID_MOV_F']=0;
        $CarichiRifiuto[$rif['RifID']][0]['idSIS']=0;
        $CarichiRifiuto[$rif['RifID']][0]['FKEpesospecifico']=$rif['FKEpesospecifico'];
        $CarichiRifiuto[$rif['RifID']][0]['FKEumis']=$rif['FKEumis'];
        $CarichiRifiuto[$rif['RifID']][0]['ID_IMP']=$rif['ID_IMP'];
        $CarichiRifiuto[$rif['RifID']][0]['workmode']=$rif['workmode'];
        $CarichiRifiuto[$rif['RifID']][0]['data']="0000-00-00";
        $CarichiRifiuto[$rif['RifID']][0]['CER']=$rif['COD_CER'];
        $CarichiRifiuto[$rif['RifID']][0]['desc']=$rif['descrizione'];
        $giacOK=true;
    }

    if(array_key_exists('carico', $rif)){
        # 1 - array carichi rifiuto
        for($k=0;$k<count($rif['carico']);$k++){
            if(isset($CarichiRifiuto[$rif['RifID']])) $index=count($CarichiRifiuto[$rif['RifID']]); else $index=0;
            $CarichiRifiuto[$rif['RifID']][$index]['qta']=$rif['carico'][$k]['qta'];
            $CarichiRifiuto[$rif['RifID']][$index]['num']=$rif['carico'][$k]['num'];
            $CarichiRifiuto[$rif['RifID']][$index]['ID_MOV_F']=$rif['carico'][$k]['ID_MOV_F'];
            $CarichiRifiuto[$rif['RifID']][$index]['idSIS']=$rif['carico'][$k]['idSIS'];
            $CarichiRifiuto[$rif['RifID']][$index]['FKEpesospecifico']=$rif['FKEpesospecifico'];
            $CarichiRifiuto[$rif['RifID']][$index]['FKEumis']=$rif['FKEumis'];
            $CarichiRifiuto[$rif['RifID']][$index]['ID_IMP']=$rif['ID_IMP'];
            $CarichiRifiuto[$rif['RifID']][$index]['ID_UIMP']=$rif['carico'][$k]['ID_UIMP'];
            $CarichiRifiuto[$rif['RifID']][$index]['workmode']=$rif['workmode'];
            $CarichiRifiuto[$rif['RifID']][$index]['data']=$rif['carico'][$k]['data'];
            $CarichiRifiuto[$rif['RifID']][$index]['CER']=$rif['COD_CER'];
            $CarichiRifiuto[$rif['RifID']][$index]['desc']=$rif['descrizione'];
        }
        # 2 - sottraggo qta da scaricato fino a quando scaricato resta positivo
        for($i=0;$i<count($CarichiRifiuto[$rif['RifID']]);$i++){
            if($TotS>=0){
                $TotS-=$CarichiRifiuto[$rif['RifID']][$i]['qta'];
                //print_r("<br />Rifiuto ".$rif['RifID'].", elimino il carico ".$CarichiRifiuto[$rif['RifID']][$i]['num']." ( ".$i." ) restano da scaricare: ".$TotS."<br /><hr>");
                $CarichiRifiuto[$rif['RifID']][$i]['qta']=abs($TotS);
                if($TotS<0){
                    $PrimoDaScaricare=$i;
                    //print_r("Primo da scaricare: ".$i."<br />");
                }
            }
            //else{
                //print_r("Non mi restano carichi da scaricare");
            //}
        }

        $count=count($CarichiRifiuto[$rif['RifID']]);
        for($i=0;$i<$count;$i++){
            //print_r("<br />Indice: ".$i." elimino se minore di ".$PrimoDaScaricare." ( count=".count($CarichiRifiuto[$rif['RifID']])." ) oppure ho scaricato tutto (Primo da scaricare = 0)");
            if($i<$PrimoDaScaricare || ($PrimoDaScaricare==0 && $TotS)){
                //print_r("<br />Carico eliminato<br />");
                unset($CarichiRifiuto[$rif['RifID']][$i]);
            }
        }
    }

    if(is_null($CarichiRifiuto[$rif['RifID']])) $CarichiRifiuto[$rif['RifID']] = [];

    $CarichiRifiuto[$rif['RifID']]=array_merge($CarichiRifiuto[$rif['RifID']]);

    return($CarichiRifiuto);
}

$TableName  = 'user_movimenti_fiscalizzati';
$PrintOut   = 'true';
$IDMov      = "ID_MOV_F";
$YEAR       = $AnnoCorrente;
$FEDIT->DbConn('soger'.$YEAR);

$LIMIT = $_GET['ID_IMPcounter']? $_GET['ID_IMPcounter']:0;

$SQL = "SELECT COUNT(DISTINCT(ID_IMP)) AS CONTA_ID_IMP FROM user_schede_rifiuti WHERE giac_ini>0;";
$FEDIT->SDBRead($SQL,"countID_IMP",true,false);
$COUNT_ID_IMP = $FEDIT->countID_IMP[0]['CONTA_ID_IMP'];

$SQL = "SELECT DISTINCT ID_IMP FROM user_schede_rifiuti WHERE giac_ini>0 ORDER BY ID_IMP LIMIT ".$LIMIT.", ".$COUNT_ID_IMP.";";
$FEDIT->SDBRead($SQL,"DbRecordSetID_IMP",true,false);

//echo "<h1>Leggo rifiuti con disponibilita>0 per ID IMP ".$FEDIT->DbRecordSetID_IMP[0]['ID_IMP'].": ".date("H:i:s")."</h1>";

$sql ="SELECT originalID_RIF, ID_RIF, produttore FROM user_schede_rifiuti ";
$sql.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER WHERE giac_ini>0 AND ID_IMP='".$FEDIT->DbRecordSetID_IMP[0]['ID_IMP']."' ";
$sql.="ORDER BY COD_CER, descrizione ASC, ID_RIF ASC;";

$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
$rifs = $wm = array();
for($r=0;$r<count($FEDIT->DbRecordSet);$r++){
    $rifs[] = $FEDIT->DbRecordSet[$r]['originalID_RIF'];
    $wm[]   = $FEDIT->DbRecordSet[$r]['produttore'];
}

$YEAR-=1;
$FEDIT->DbConn('soger'.$YEAR);
$DbRecordSet = array();

for($r=0;$r<count($rifs);$r++){
    $sql  = "SELECT ID_MOV_F,idSIS,DTMOV,NMOV,quantita,user_movimenti_fiscalizzati.ID_UIMP,user_movimenti_fiscalizzati.originalID_RIF AS originalRifID,user_movimenti_fiscalizzati.ID_RIF AS RifID,TIPO,";
    $sql .= " user_schede_rifiuti.ID_CER as CERref,user_schede_rifiuti.descrizione,user_schede_rifiuti.ID_RIFPROD_F,lov_cer.COD_CER,user_schede_rifiuti.giac_ini, ";
    $sql .= " user_movimenti_fiscalizzati.ID_IMP, FKEumis, FKEpesospecifico, (CASE WHEN user_movimenti_fiscalizzati.produttore=1 THEN 'produttore' WHEN user_movimenti_fiscalizzati.trasportatore=1 THEN 'trasportatore' WHEN user_movimenti_fiscalizzati.destinatario=1 THEN 'destinatario' WHEN user_movimenti_fiscalizzati.intermediario=1 THEN 'intermediario' ELSE 'produttore' END) AS workmode ";
    $sql .= " FROM user_movimenti_fiscalizzati";
    $sql .= " JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF";
    $sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
    $sql .= " WHERE user_movimenti_fiscalizzati.NMOV<>9999999 AND PerRiclassificazione=0 AND user_movimenti_fiscalizzati.originalID_RIF=".$rifs[$r]." ";
    if($wm[$r])
        $sql .= " ORDER BY user_movimenti_fiscalizzati.ID_UIMP ASC, TIPO ASC, NMOV ASC, DTMOV ASC ";
    else
        $sql .= " ORDER BY TIPO ASC, NMOV ASC, DTMOV ASC ";

    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);
    if(isset($FEDIT->DbRecordSet))
        $DbRecordSet = array_merge($DbRecordSet, $FEDIT->DbRecordSet);
}

$TotC = 0;
$TotS = 0;
$RIFbuf = "";
$UIMPbuf = "";
$rifiuti = array();
$CarichiRifiuto = array();

for($i=0;$i<count($DbRecordSet);$i++){
    $indice=count($rifiuti);
    if($DbRecordSet[$i]['originalRifID']!=$RIFbuf || ($DbRecordSet[$i]['workmode']=='produttore' && $DbRecordSet[$i]['ID_UIMP']!=$UIMPbuf)) {
        $rifiuti[$indice]['totC']=$DbRecordSet[$i]['giac_ini'];
        $rifiuti[$indice]['COD_CER']=$DbRecordSet[$i]['COD_CER'];
        $rifiuti[$indice]['CERref']=$DbRecordSet[$i]['CERref'];
        $rifiuti[$indice]['RifID']=$DbRecordSet[$i]['RifID'];
        $rifiuti[$indice]['originalRifID']=$DbRecordSet[$i]['originalRifID'];
        $rifiuti[$indice]['giac_ini']=$DbRecordSet[$i]['giac_ini'];
        $rifiuti[$indice]['descrizione']=$DbRecordSet[$i]['descrizione'];
        $rifiuti[$indice]['ID_RIFPROD_F']=$DbRecordSet[$i]['ID_RIFPROD_F'];
        $rifiuti[$indice]['FKEpesospecifico']=$DbRecordSet[$i]['FKEpesospecifico'];
        $rifiuti[$indice]['FKEumis']=$DbRecordSet[$i]['FKEumis'];
        $rifiuti[$indice]['ID_IMP']=$DbRecordSet[$i]['ID_IMP'];
        $rifiuti[$indice]['workmode']=$DbRecordSet[$i]['workmode'];
        if($DbRecordSet[$i]['TIPO']=="C"){
            $rifiuti[$indice]['carico'][0]['qta']=$DbRecordSet[$i]['quantita'];
            $rifiuti[$indice]['carico'][0]['FKEpesospecifico']=$DbRecordSet[$i]['FKEpesospecifico'];
            $rifiuti[$indice]['carico'][0]['FKEumis']=$DbRecordSet[$i]['FKEumis'];
            $rifiuti[$indice]['carico'][0]['num']=$DbRecordSet[$i]['NMOV'];
            $rifiuti[$indice]['carico'][0]['ID_MOV_F']=$DbRecordSet[$i]['ID_MOV_F'];
            $rifiuti[$indice]['carico'][0]['idSIS']=$DbRecordSet[$i]['idSIS'];
            $rifiuti[$indice]['carico'][0]['data']=$DbRecordSet[$i]['DTMOV'];
            $rifiuti[$indice]['carico'][0]['ID_IMP']=$DbRecordSet[$i]['ID_IMP'];
            $rifiuti[$indice]['carico'][0]['ID_UIMP']=$DbRecordSet[$i]['ID_UIMP'];
            $rifiuti[$indice]['carico'][0]['workmode']=$DbRecordSet[$i]['workmode'];
            $rifiuti[$indice]['totC']+=$DbRecordSet[$i]['quantita'];
        }
        if($DbRecordSet[$i]['TIPO']=="S")
            @$rifiuti[$indice]['totS']=$DbRecordSet[$i]['quantita'];
    }
    else{
        $rifiuti[$indice-1]['RifID']=$DbRecordSet[$i]['RifID'];
        # somme
        $countCarichi=(isset($rifiuti[$indice-1]['carico'])? count($rifiuti[$indice-1]['carico']) : 0);
        if($DbRecordSet[$i]['TIPO']=="C"){
            $rifiuti[$indice-1]['carico'][$countCarichi]['qta']=$DbRecordSet[$i]['quantita'];
            $rifiuti[$indice-1]['carico'][$countCarichi]['num']=$DbRecordSet[$i]['NMOV'];
            $rifiuti[$indice-1]['carico'][$countCarichi]['ID_MOV_F']=$DbRecordSet[$i]['ID_MOV_F'];
            $rifiuti[$indice-1]['carico'][$countCarichi]['idSIS']=$DbRecordSet[$i]['idSIS'];
            $rifiuti[$indice-1]['carico'][$countCarichi]['FKEpesospecifico']=$DbRecordSet[$i]['FKEpesospecifico'];
            $rifiuti[$indice-1]['carico'][$countCarichi]['FKEumis']=$DbRecordSet[$i]['FKEumis'];
            $rifiuti[$indice-1]['carico'][$countCarichi]['ID_IMP']=$DbRecordSet[$i]['ID_IMP'];
            $rifiuti[$indice-1]['carico'][$countCarichi]['ID_UIMP']=$DbRecordSet[$i]['ID_UIMP'];
            $rifiuti[$indice-1]['carico'][$countCarichi]['workmode']=$DbRecordSet[$i]['workmode'];
            $rifiuti[$indice-1]['carico'][$countCarichi]['data']=$DbRecordSet[$i]['DTMOV'];
            $rifiuti[$indice-1]['totC']+=$DbRecordSet[$i]['quantita'];
            }
        if($DbRecordSet[$i]['TIPO']=="S")
            @$rifiuti[$indice-1]['totS']+=$DbRecordSet[$i]['quantita'];
    }
    $UIMPbuf=$DbRecordSet[$i]['ID_UIMP'];
    $RIFbuf=$DbRecordSet[$i]['originalRifID'];
}

//var_dump($rifiuti);die();
//echo "<h1>Ho tutti i carichi dei rifiuti con disponibilita>0, procedo ad eliminare quelli scaricati - START: ".date("H:i:s")."</h1>";
$FEDIT->DbConn('soger'.$AnnoCorrente);
for($r=0;$r<count($rifiuti);$r++){
    $carichi=RemoveCarichiScaricati($rifiuti[$r], $CarichiRifiuto);
    //var_dump($carichi);echo "<hr>";
    if($carichi){
        foreach($carichi AS $rif=>$carico){
            # calcola pesoN
            for($c=0;$c<count($carico);$c++){
                $carico[$c]['pesoN']=$carico[$c]['qta'];
                if($carico[$c]['FKEumis']=='Litri') $carico[$c]['pesoN']=$carico[$c]['qta']*$carico[$c]['FKEpesospecifico'];
                if($carico[$c]['FKEumis']=='Mc.') $carico[$c]['pesoN']=$carico[$c]['qta']*$carico[$c]['FKEpesospecifico']*1000;
                $sql ="INSERT INTO user_movimenti_giacenze_iniziali ";
                $sql.="(ID_RIF, ID_MOV_F, NMOV, DTMOV, idSIS, quantita, pesoN, TIPO, ID_UIMP, FKEpesospecifico, FKEumis, ID_IMP, workmode) ";
                $sql.="VALUES (".$rif.", ".$carico[$c]['ID_MOV_F'].", ".$carico[$c]['num'].", '".$carico[$c]['data']."', '".$carico[$c]['idSIS']."', ".$carico[$c]['qta'].", ".$carico[$c]['pesoN'].", 'C', '".$carico[$c]['ID_UIMP']."', ".$carico[$c]['FKEpesospecifico'].", '".$carico[$c]['FKEumis']."', '".$carico[$c]['ID_IMP']."', '".$carico[$c]['workmode']."');";
                //echo $sql."<br />";
                //echo "<hr>";
                $FEDIT->SDBWrite($sql,true,false);
            }
        }
    }
}
$sql = "UPDATE user_movimenti_giacenze_iniziali SET idSIS=NULL WHERE idSIS='' OR idSIS=0;";
$FEDIT->SDBWrite($sql,true,false);

$sql = "UPDATE user_movimenti_giacenze_iniziali SET NMOV='Giac.Iniz.', DTMOV='".$AnnoCorrente."-01-01' WHERE NMOV=0;";
$FEDIT->SDBWrite($sql,true,false);

$LIMIT++;

if($LIMIT<$COUNT_ID_IMP)
    header('Location:'.$_SERVER['PHP_SELF'].'?ID_IMPcounter='.$LIMIT);
else
    die("FINITO!");

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>