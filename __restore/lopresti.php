<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/zip3.php");
require_once("../__libs/SQLFunct.php");
require_once("../__scripts/STATS_funct.php");
require_once("../__scripts/MUD_funct.php");
require_once("../__libs/SogerECO_funct.php");

global $SOGER;

$MOVIMENTI_AL		= "2015-01-25";
$IMP_CON_DATA_AVVIO = array();
$IMP_AND_DATE		= array();
$CSVbuf				= '';
$DcName				= str_replace('-','',$MOVIMENTI_AL);

function var_dump_pre($mixed = null) {
	echo '<pre>';
	var_dump($mixed);
	echo '</pre>';
	return null;
	}

$SQL2010 = "SELECT DISTINCT soger2010.user_movimenti_fiscalizzati.ID_IMP FROM soger2010.user_movimenti_fiscalizzati WHERE soger2010.user_movimenti_fiscalizzati.ID_IMP IS NOT NULL AND soger2010.user_movimenti_fiscalizzati.ID_IMP<>'';";
$FEDIT->SdbRead($SQL2010,"DbRecordSetIMP2010",true,false);
$SQL2011 = "SELECT DISTINCT soger2011.user_movimenti_fiscalizzati.ID_IMP FROM soger2011.user_movimenti_fiscalizzati WHERE soger2011.user_movimenti_fiscalizzati.ID_IMP IS NOT NULL AND soger2011.user_movimenti_fiscalizzati.ID_IMP<>'';";
$FEDIT->SdbRead($SQL2011,"DbRecordSetIMP2011",true,false);
$SQL2012 = "SELECT DISTINCT soger2012.user_movimenti_fiscalizzati.ID_IMP FROM soger2012.user_movimenti_fiscalizzati WHERE soger2012.user_movimenti_fiscalizzati.ID_IMP IS NOT NULL AND soger2012.user_movimenti_fiscalizzati.ID_IMP<>'';";
$FEDIT->SdbRead($SQL2012,"DbRecordSetIMP2012",true,false);
$SQL2013 = "SELECT DISTINCT soger2013.user_movimenti_fiscalizzati.ID_IMP FROM soger2013.user_movimenti_fiscalizzati WHERE soger2013.user_movimenti_fiscalizzati.ID_IMP IS NOT NULL AND soger2013.user_movimenti_fiscalizzati.ID_IMP<>'';";
$FEDIT->SdbRead($SQL2013,"DbRecordSetIMP2013",true,false);
$SQL2014 = "SELECT DISTINCT soger2014.user_movimenti_fiscalizzati.ID_IMP FROM soger2014.user_movimenti_fiscalizzati WHERE soger2014.user_movimenti_fiscalizzati.ID_IMP IS NOT NULL AND soger2014.user_movimenti_fiscalizzati.ID_IMP<>'';";
$FEDIT->SdbRead($SQL2014,"DbRecordSetIMP2014",true,false);
$SQL2015 = "SELECT DISTINCT soger2015.user_movimenti_fiscalizzati.ID_IMP FROM soger2015.user_movimenti_fiscalizzati WHERE soger2015.user_movimenti_fiscalizzati.ID_IMP IS NOT NULL AND soger2015.user_movimenti_fiscalizzati.ID_IMP<>'';";
$FEDIT->SdbRead($SQL2015,"DbRecordSetIMP2015",true,false);

$IMP2010 = $FEDIT->DbRecordSetIMP2010;
$IMP2011 = $FEDIT->DbRecordSetIMP2011;
$IMP2012 = $FEDIT->DbRecordSetIMP2012;
$IMP2013 = $FEDIT->DbRecordSetIMP2013;
$IMP2014 = $FEDIT->DbRecordSetIMP2014;
$IMP2015 = $FEDIT->DbRecordSetIMP2015;


#2010
$ATTIVI_DAL_2010 = array();
$ATTIVI_DAL_2010_POI_SOSPESI = array();
for($i=0;$i<count($IMP2010);$i++){
	if(!in_array($IMP2010[$i], $IMP2011) OR !in_array($IMP2010[$i], $IMP2012) OR !in_array($IMP2010[$i], $IMP2013) OR !in_array($IMP2010[$i], $IMP2014) OR !in_array($IMP2010[$i], $IMP2015) )
		array_push($ATTIVI_DAL_2010_POI_SOSPESI, $IMP2010[$i]);
	else
		array_push($ATTIVI_DAL_2010, $IMP2010[$i]['ID_IMP']);
	}

#2011
$ATTIVI_DAL_2011 = array();
$ATTIVI_DAL_2011_POI_SOSPESI = array();
for($i=0;$i<count($IMP2011);$i++){
	if(!in_array($IMP2011[$i], $IMP2012) OR !in_array($IMP2011[$i], $IMP2013) OR !in_array($IMP2011[$i], $IMP2014) OR !in_array($IMP2011[$i], $IMP2015) )
		array_push($ATTIVI_DAL_2011_POI_SOSPESI, $IMP2011[$i]);
	else
		array_push($ATTIVI_DAL_2011, $IMP2011[$i]['ID_IMP']);
	}

#2012
$ATTIVI_DAL_2012 = array();
$ATTIVI_DAL_2012_POI_SOSPESI = array();
for($i=0;$i<count($IMP2012);$i++){
	if(!in_array($IMP2012[$i], $IMP2013) OR !in_array($IMP2012[$i], $IMP2014) OR !in_array($IMP2012[$i], $IMP2015) )
		array_push($ATTIVI_DAL_2012_POI_SOSPESI, $IMP2012[$i]);
	else
		array_push($ATTIVI_DAL_2012, $IMP2012[$i]['ID_IMP']);
	}

#2013
$ATTIVI_DAL_2013 = array();
$ATTIVI_DAL_2013_POI_SOSPESI = array();
for($i=0;$i<count($IMP2013);$i++){
	if(!in_array($IMP2013[$i], $IMP2014) OR !in_array($IMP2013[$i], $IMP2015) )
		array_push($ATTIVI_DAL_2013_POI_SOSPESI, $IMP2013[$i]);
	else
		array_push($ATTIVI_DAL_2013, $IMP2013[$i]['ID_IMP']);
	}

#2014
$ATTIVI_DAL_2014 = array();
$ATTIVI_DAL_2014_POI_SOSPESI = array();
for($i=0;$i<count($IMP2014);$i++){
	if(!in_array($IMP2014[$i], $IMP2015) )
		array_push($ATTIVI_DAL_2014_POI_SOSPESI, $IMP2014[$i]);
	else
		array_push($ATTIVI_DAL_2014, $IMP2014[$i]['ID_IMP']);
	}

#2015
$ATTIVI_DAL_2015 = array();
$ATTIVI_DAL_2015_POI_SOSPESI = array();
for($i=0;$i<count($IMP2015);$i++){
	array_push($ATTIVI_DAL_2015, $IMP2015[$i]['ID_IMP']);
	}


$IMPIANTI_ATTIVI_2010_STRING = implode('", "', $ATTIVI_DAL_2010);
$IMPIANTI_ATTIVI_2010_WHERE = '("' . substr($IMPIANTI_ATTIVI_2010_STRING, 3, strlen($IMPIANTI_ATTIVI_2010_STRING) ) . '")';

$IMPIANTI_ATTIVI_2011_STRING = implode('", "', $ATTIVI_DAL_2011);
$IMPIANTI_ATTIVI_2011_WHERE = '("' . substr($IMPIANTI_ATTIVI_2011_STRING, 3, strlen($IMPIANTI_ATTIVI_2011_STRING) ) . '")';

$IMPIANTI_ATTIVI_2012_STRING = implode('", "', $ATTIVI_DAL_2012);
$IMPIANTI_ATTIVI_2012_WHERE = '("' . substr($IMPIANTI_ATTIVI_2012_STRING, 3, strlen($IMPIANTI_ATTIVI_2012_STRING) ) . '")';

$IMPIANTI_ATTIVI_2013_STRING = implode('", "', $ATTIVI_DAL_2013);
$IMPIANTI_ATTIVI_2013_WHERE = '("' . substr($IMPIANTI_ATTIVI_2013_STRING, 3, strlen($IMPIANTI_ATTIVI_2013_STRING) ) . '")';

$IMPIANTI_ATTIVI_2014_STRING = implode('", "', $ATTIVI_DAL_2014);
$IMPIANTI_ATTIVI_2014_WHERE = '("' . substr($IMPIANTI_ATTIVI_2014_STRING, 3, strlen($IMPIANTI_ATTIVI_2014_STRING) ) . '")';

$IMPIANTI_ATTIVI_2015_STRING = implode('", "', $ATTIVI_DAL_2015);
$IMPIANTI_ATTIVI_2015_WHERE = '("' . substr($IMPIANTI_ATTIVI_2015_STRING, 3, strlen($IMPIANTI_ATTIVI_2015_STRING) ) . '")';

/*
echo "<h1>Attivi dal 2010:</h1>";
var_dump_pre($ATTIVI_DAL_2010);
echo "<h1>Attivi dal 2010 che poi hanno sospeso:</h1>";
var_dump_pre($ATTIVI_DAL_2010_POI_SOSPESI);
echo "<hr>";

echo "<h1>Attivi dal 2011:</h1>";
var_dump_pre($ATTIVI_DAL_2011);
echo "<h1>Attivi dal 2011 che poi hanno sospeso:</h1>";
var_dump_pre($ATTIVI_DAL_2011_POI_SOSPESI);
echo "<hr>";

echo "<h1>Attivi dal 2012:</h1>";
var_dump_pre($ATTIVI_DAL_2012);
echo "<h1>Attivi dal 2012 che poi hanno sospeso:</h1>";
var_dump_pre($ATTIVI_DAL_2012_POI_SOSPESI);
echo "<hr>";

echo "<h1>Attivi dal 2013:</h1>";
var_dump_pre($ATTIVI_DAL_2013);
echo "<h1>Attivi dal 2013 che poi hanno sospeso:</h1>";
var_dump_pre($ATTIVI_DAL_2013_POI_SOSPESI);
echo "<hr>";

echo "<h1>Attivi dal 2014:</h1>";
var_dump_pre($ATTIVI_DAL_2014);
echo "<h1>Attivi dal 2014 che poi hanno sospeso:</h1>";
var_dump_pre($ATTIVI_DAL_2014_POI_SOSPESI);
echo "<hr>";

echo $IMPIANTI_ATTIVI_2010_WHERE."<hr>";
echo $IMPIANTI_ATTIVI_2011_WHERE."<hr>";
echo $IMPIANTI_ATTIVI_2012_WHERE."<hr>";
echo $IMPIANTI_ATTIVI_2013_WHERE."<hr>";
echo $IMPIANTI_ATTIVI_2014_WHERE."<hr>";
echo $IMPIANTI_ATTIVI_2015_WHERE."<hr>";

die();
*/

$SQL="SELECT * FROM

(

SELECT soger2015.lov_cer.COD_CER, soger2015.user_movimenti_fiscalizzati.ID_IMP, soger2015.lov_comuni_istat.des_reg AS REGIONE, REPLACE( ROUND( SUM( soger2015.user_movimenti_fiscalizzati.pesoN ), 2 ), '.', ',') AS KG_STIMATI, REPLACE( ROUND( SUM( soger2015.user_movimenti_fiscalizzati.PS_DESTINO ), 2 ), '.', ',') AS KG_VERIFICATI, IF(PS_DESTINO=1, 's�', 'no') AS VERIFICA_RICHIESTA, YEAR(soger2015.user_movimenti_fiscalizzati.DTFORM) AS ANNO, DATE_ADD( soger2015.user_movimenti_fiscalizzati.DTFORM, INTERVAL( 1 - DAYOFWEEK( soger2015.user_movimenti_fiscalizzati.DTFORM ) ) +1 DAY ) AS SETTIMANA_DAL, DATE_ADD( soger2015.user_movimenti_fiscalizzati.DTFORM, INTERVAL( 7 - DAYOFWEEK( soger2015.user_movimenti_fiscalizzati.DTFORM ) ) +1 DAY ) AS SETTIMANA_AL FROM soger2015.user_movimenti_fiscalizzati JOIN soger2015.user_schede_rifiuti ON soger2015.user_schede_rifiuti.ID_RIF = soger2015.user_movimenti_fiscalizzati.ID_RIF JOIN soger2015.lov_cer ON soger2015.lov_cer.ID_CER = soger2015.user_schede_rifiuti.ID_CER JOIN soger2015.core_impianti ON soger2015.core_impianti.ID_IMP = soger2015.user_movimenti_fiscalizzati.ID_IMP JOIN soger2015.lov_comuni_istat ON soger2015.lov_comuni_istat.ID_COM = soger2015.core_impianti.ID_COM WHERE soger2015.lov_cer.COD_CER IN ('120101', '120102', '150101', '150102', '150106') AND soger2015.user_movimenti_fiscalizzati.TIPO = 'S' AND soger2015.user_movimenti_fiscalizzati.Transfrontaliero =0 AND soger2015.user_movimenti_fiscalizzati.produttore =1 AND soger2015.user_movimenti_fiscalizzati.ID_AZT <>0 AND soger2015.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND DTFORM<='".$MOVIMENTI_AL."' AND WEEK( soger2015.user_movimenti_fiscalizzati.DTFORM ) IS NOT NULL AND YEAR(soger2015.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2015.user_movimenti_fiscalizzati.DTFORM)>'2009' AND soger2015.user_movimenti_fiscalizzati.ID_IMP IN ".$IMPIANTI_ATTIVI_2015_WHERE." GROUP BY soger2015.user_movimenti_fiscalizzati.ID_IMP, soger2015.lov_cer.COD_CER, CONCAT( ANNO, SETTIMANA_DAL )

UNION DISTINCT

SELECT soger2014.lov_cer.COD_CER, soger2014.user_movimenti_fiscalizzati.ID_IMP, soger2014.lov_comuni_istat.des_reg AS REGIONE, REPLACE( ROUND( SUM( soger2014.user_movimenti_fiscalizzati.pesoN ), 2 ), '.', ',') AS KG_STIMATI, REPLACE( ROUND( SUM( soger2014.user_movimenti_fiscalizzati.PS_DESTINO ), 2 ), '.', ',')  AS KG_VERIFICATI, IF(PS_DESTINO=1, 's�', 'no') AS VERIFICA_RICHIESTA, YEAR(soger2014.user_movimenti_fiscalizzati.DTFORM) AS ANNO, DATE_ADD( soger2014.user_movimenti_fiscalizzati.DTFORM, INTERVAL( 1 - DAYOFWEEK( soger2014.user_movimenti_fiscalizzati.DTFORM ) ) +1 DAY ) AS SETTIMANA_DAL, DATE_ADD( soger2014.user_movimenti_fiscalizzati.DTFORM, INTERVAL( 7 - DAYOFWEEK( soger2014.user_movimenti_fiscalizzati.DTFORM ) ) +1 DAY ) AS SETTIMANA_AL FROM soger2014.user_movimenti_fiscalizzati JOIN soger2014.user_schede_rifiuti ON soger2014.user_schede_rifiuti.ID_RIF = soger2014.user_movimenti_fiscalizzati.ID_RIF JOIN soger2014.lov_cer ON soger2014.lov_cer.ID_CER = soger2014.user_schede_rifiuti.ID_CER JOIN soger2014.core_impianti ON soger2014.core_impianti.ID_IMP = soger2014.user_movimenti_fiscalizzati.ID_IMP JOIN soger2014.lov_comuni_istat ON soger2014.lov_comuni_istat.ID_COM = soger2014.core_impianti.ID_COM WHERE soger2014.lov_cer.COD_CER IN ('120101', '120102', '150101', '150102', '150106') AND soger2014.user_movimenti_fiscalizzati.TIPO = 'S' AND soger2014.user_movimenti_fiscalizzati.Transfrontaliero =0 AND soger2014.user_movimenti_fiscalizzati.produttore =1 AND soger2014.user_movimenti_fiscalizzati.ID_AZT <>0 AND soger2014.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND WEEK( soger2014.user_movimenti_fiscalizzati.DTFORM ) IS NOT NULL AND YEAR(soger2014.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2014.user_movimenti_fiscalizzati.DTFORM)>'2009' AND soger2014.user_movimenti_fiscalizzati.ID_IMP IN ".$IMPIANTI_ATTIVI_2014_WHERE." GROUP BY soger2014.user_movimenti_fiscalizzati.ID_IMP, soger2014.lov_cer.COD_CER, CONCAT( ANNO, SETTIMANA_DAL )

UNION DISTINCT

SELECT soger2013.lov_cer.COD_CER, soger2013.user_movimenti_fiscalizzati.ID_IMP, soger2013.lov_comuni_istat.des_reg AS REGIONE, REPLACE( ROUND( SUM( soger2013.user_movimenti_fiscalizzati.pesoN ), 2 ), '.', ',') AS KG_STIMATI, REPLACE( ROUND( SUM( soger2013.user_movimenti_fiscalizzati.PS_DESTINO ), 2 ), '.', ',') AS KG_VERIFICATI, IF(PS_DESTINO=1, 's�', 'no') AS VERIFICA_RICHIESTA, YEAR(soger2013.user_movimenti_fiscalizzati.DTFORM) AS ANNO, DATE_ADD( soger2013.user_movimenti_fiscalizzati.DTFORM, INTERVAL( 1 - DAYOFWEEK( soger2013.user_movimenti_fiscalizzati.DTFORM ) ) +1 DAY ) AS SETTIMANA_DAL, DATE_ADD( soger2013.user_movimenti_fiscalizzati.DTFORM, INTERVAL( 7 - DAYOFWEEK( soger2013.user_movimenti_fiscalizzati.DTFORM ) ) +1 DAY ) AS SETTIMANA_AL FROM soger2013.user_movimenti_fiscalizzati JOIN soger2013.user_schede_rifiuti ON soger2013.user_schede_rifiuti.ID_RIF = soger2013.user_movimenti_fiscalizzati.ID_RIF JOIN soger2013.lov_cer ON soger2013.lov_cer.ID_CER = soger2013.user_schede_rifiuti.ID_CER JOIN soger2013.core_impianti ON soger2013.core_impianti.ID_IMP = soger2013.user_movimenti_fiscalizzati.ID_IMP JOIN soger2013.lov_comuni_istat ON soger2013.lov_comuni_istat.ID_COM = soger2013.core_impianti.ID_COM WHERE soger2013.lov_cer.COD_CER IN ('120101', '120102', '150101', '150102', '150106') AND soger2013.user_movimenti_fiscalizzati.TIPO = 'S' AND soger2013.user_movimenti_fiscalizzati.Transfrontaliero =0 AND soger2013.user_movimenti_fiscalizzati.produttore =1 AND soger2013.user_movimenti_fiscalizzati.ID_AZT <>0 AND soger2013.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND WEEK( soger2013.user_movimenti_fiscalizzati.DTFORM ) IS NOT NULL AND YEAR(soger2013.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2013.user_movimenti_fiscalizzati.DTFORM)>'2009' AND soger2013.user_movimenti_fiscalizzati.ID_IMP IN ".$IMPIANTI_ATTIVI_2013_WHERE." GROUP BY soger2013.user_movimenti_fiscalizzati.ID_IMP, soger2013.lov_cer.COD_CER, CONCAT( ANNO, SETTIMANA_DAL )

UNION DISTINCT

SELECT soger2012.lov_cer.COD_CER, soger2012.user_movimenti_fiscalizzati.ID_IMP, soger2012.lov_comuni_istat.des_reg AS REGIONE, REPLACE( ROUND( SUM( soger2012.user_movimenti_fiscalizzati.pesoN ), 2 ), '.', ',') AS KG_STIMATI, REPLACE( ROUND( SUM( soger2012.user_movimenti_fiscalizzati.PS_DESTINO ), 2 ), '.', ',') AS KG_VERIFICATI, IF(PS_DESTINO=1, 's�', 'no') AS VERIFICA_RICHIESTA, REPLACE( ROUND( SUM( soger2012.user_movimenti_fiscalizzati.PS_DESTINO ), 2 ), '.', ',') AS KG_STIMATI, YEAR(soger2012.user_movimenti_fiscalizzati.DTFORM) AS ANNO, DATE_ADD( soger2012.user_movimenti_fiscalizzati.DTFORM, INTERVAL( 1 - DAYOFWEEK( soger2012.user_movimenti_fiscalizzati.DTFORM ) ) +1 DAY ) AS SETTIMANA_DAL, DATE_ADD( soger2012.user_movimenti_fiscalizzati.DTFORM, INTERVAL( 7 - DAYOFWEEK( soger2012.user_movimenti_fiscalizzati.DTFORM ) ) +1 DAY ) AS SETTIMANA_AL FROM soger2012.user_movimenti_fiscalizzati JOIN soger2012.user_schede_rifiuti ON soger2012.user_schede_rifiuti.ID_RIF = soger2012.user_movimenti_fiscalizzati.ID_RIF JOIN soger2012.lov_cer ON soger2012.lov_cer.ID_CER = soger2012.user_schede_rifiuti.ID_CER JOIN soger2012.core_impianti ON soger2012.core_impianti.ID_IMP = soger2012.user_movimenti_fiscalizzati.ID_IMP JOIN soger2012.lov_comuni_istat ON soger2012.lov_comuni_istat.ID_COM = soger2012.core_impianti.ID_COM WHERE soger2012.lov_cer.COD_CER IN ('120101', '120102', '150101', '150102', '150106') AND soger2012.user_movimenti_fiscalizzati.TIPO = 'S' AND soger2012.user_movimenti_fiscalizzati.Transfrontaliero =0 AND soger2012.user_movimenti_fiscalizzati.produttore =1 AND soger2012.user_movimenti_fiscalizzati.ID_AZT <>0 AND soger2012.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND WEEK( soger2012.user_movimenti_fiscalizzati.DTFORM ) IS NOT NULL AND YEAR(soger2012.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2012.user_movimenti_fiscalizzati.DTFORM)>'2009' AND soger2012.user_movimenti_fiscalizzati.ID_IMP IN ".$IMPIANTI_ATTIVI_2012_WHERE." GROUP BY soger2012.user_movimenti_fiscalizzati.ID_IMP, soger2012.lov_cer.COD_CER, CONCAT( ANNO, SETTIMANA_DAL )

UNION DISTINCT

SELECT soger2011.lov_cer.COD_CER, soger2011.user_movimenti_fiscalizzati.ID_IMP, soger2011.lov_comuni_istat.des_reg AS REGIONE, REPLACE( ROUND( SUM( soger2011.user_movimenti_fiscalizzati.pesoN ), 2 ), '.', ',') AS KG_STIMATI, REPLACE( ROUND( SUM( soger2011.user_movimenti_fiscalizzati.PS_DESTINO ), 2 ), '.', ',') AS KG_VERIFICATI, IF(PS_DESTINO=1, 's�', 'no') AS VERIFICA_RICHIESTA, YEAR(soger2011.user_movimenti_fiscalizzati.DTFORM) AS ANNO, DATE_ADD( soger2011.user_movimenti_fiscalizzati.DTFORM, INTERVAL( 1 - DAYOFWEEK( soger2011.user_movimenti_fiscalizzati.DTFORM ) ) +1 DAY ) AS SETTIMANA_DAL, DATE_ADD( soger2011.user_movimenti_fiscalizzati.DTFORM, INTERVAL( 7 - DAYOFWEEK( soger2011.user_movimenti_fiscalizzati.DTFORM ) ) +1 DAY ) AS SETTIMANA_AL FROM soger2011.user_movimenti_fiscalizzati JOIN soger2011.user_schede_rifiuti ON soger2011.user_schede_rifiuti.ID_RIF = soger2011.user_movimenti_fiscalizzati.ID_RIF JOIN soger2011.lov_cer ON soger2011.lov_cer.ID_CER = soger2011.user_schede_rifiuti.ID_CER JOIN soger2011.core_impianti ON soger2011.core_impianti.ID_IMP = soger2011.user_movimenti_fiscalizzati.ID_IMP JOIN soger2011.lov_comuni_istat ON soger2011.lov_comuni_istat.ID_COM = soger2011.core_impianti.ID_COM WHERE soger2011.lov_cer.COD_CER IN ('120101', '120102', '150101', '150102', '150106') AND soger2011.user_movimenti_fiscalizzati.TIPO = 'S' AND soger2011.user_movimenti_fiscalizzati.Transfrontaliero =0 AND soger2011.user_movimenti_fiscalizzati.produttore =1 AND soger2011.user_movimenti_fiscalizzati.ID_AZT <>0 AND soger2011.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND WEEK( soger2011.user_movimenti_fiscalizzati.DTFORM ) IS NOT NULL AND YEAR(soger2011.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2011.user_movimenti_fiscalizzati.DTFORM)>'2009' AND soger2011.user_movimenti_fiscalizzati.ID_IMP IN ".$IMPIANTI_ATTIVI_2011_WHERE." GROUP BY soger2011.user_movimenti_fiscalizzati.ID_IMP, soger2011.lov_cer.COD_CER, CONCAT( ANNO, SETTIMANA_DAL )

UNION DISTINCT

SELECT soger2010.lov_cer.COD_CER, soger2010.user_movimenti_fiscalizzati.ID_IMP, soger2010.lov_comuni_istat.des_reg AS REGIONE, REPLACE( ROUND( SUM( soger2010.user_movimenti_fiscalizzati.pesoN ), 2 ), '.', ',') AS KG_STIMATI, REPLACE( ROUND( SUM( soger2010.user_movimenti_fiscalizzati.PS_DESTINO ), 2 ), '.', ',') AS KG_VERIFICATI, IF(PS_DESTINO=1, 's�', 'no') AS VERIFICA_RICHIESTA, YEAR(soger2010.user_movimenti_fiscalizzati.DTFORM) AS ANNO, DATE_ADD( soger2010.user_movimenti_fiscalizzati.DTFORM, INTERVAL( 1 - DAYOFWEEK( soger2010.user_movimenti_fiscalizzati.DTFORM ) ) +1 DAY ) AS SETTIMANA_DAL, DATE_ADD( soger2010.user_movimenti_fiscalizzati.DTFORM, INTERVAL( 7 - DAYOFWEEK( soger2010.user_movimenti_fiscalizzati.DTFORM ) ) +1 DAY ) AS SETTIMANA_AL FROM soger2010.user_movimenti_fiscalizzati JOIN soger2010.user_schede_rifiuti ON soger2010.user_schede_rifiuti.ID_RIF = soger2010.user_movimenti_fiscalizzati.ID_RIF JOIN soger2010.lov_cer ON soger2010.lov_cer.ID_CER = soger2010.user_schede_rifiuti.ID_CER JOIN soger2010.core_impianti ON soger2010.core_impianti.ID_IMP = soger2010.user_movimenti_fiscalizzati.ID_IMP JOIN soger2010.lov_comuni_istat ON soger2010.lov_comuni_istat.ID_COM = soger2010.core_impianti.ID_COM WHERE soger2010.lov_cer.COD_CER IN ('120101', '120102', '150101', '150102', '150106') AND soger2010.user_movimenti_fiscalizzati.TIPO = 'S' AND soger2010.user_movimenti_fiscalizzati.Transfrontaliero =0 AND soger2010.user_movimenti_fiscalizzati.produttore =1 AND soger2010.user_movimenti_fiscalizzati.ID_AZT <>0 AND soger2010.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND WEEK( soger2010.user_movimenti_fiscalizzati.DTFORM ) IS NOT NULL AND YEAR(soger2010.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2010.user_movimenti_fiscalizzati.DTFORM)>'2009' AND soger2010.user_movimenti_fiscalizzati.ID_IMP IN ".$IMPIANTI_ATTIVI_2010_WHERE." GROUP BY soger2010.user_movimenti_fiscalizzati.ID_IMP, soger2010.lov_cer.COD_CER, CONCAT( ANNO, SETTIMANA_DAL )

)

AS DatiSettimanali ORDER BY ANNO, SETTIMANA_DAL, COD_CER, ID_IMP;";

$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);

/*
echo "<table>";
echo "<tr>";
echo "<th>CODICE CER</th><th>IMPIANTO</th><th>REGIONE</th><th>KG</th><th>ANNO</th><th>SETTIMANA N�</th><th>DAL</th><th>AL</th><th>DATA AVVIO</th>";
echo "</tr>\n";
*/

for($r=0;$r<count($FEDIT->DbRecordSet);$r++){

	$YEAR_FOR_MINDATE = 2010;
	$YtoCHECK = array("ATTIVI_DAL_2015", "ATTIVI_DAL_2014", "ATTIVI_DAL_2013", "ATTIVI_DAL_2012", "ATTIVI_DAL_2011", "ATTIVI_DAL_2010");
	
	if(!in_array($FEDIT->DbRecordSet[$r]['ID_IMP'], $IMP_CON_DATA_AVVIO)){

		for($y=0;$y<count($YtoCHECK);$y++){

                        if(!in_array($FEDIT->DbRecordSet[$r]['ID_IMP'], ${$YtoCHECK[$y]})){
				$YEAR = substr($YtoCHECK[$y], -4);
				$YEAR_FOR_MINDATE = (int)$YEAR + 1;
				break;
				}

			}
			
		// ricavo data prima movimentazione
		$SQL = "SELECT MIN(MINDATE) AS MINDATE FROM ( ";

		$SQL2010 = "SELECT MIN(soger2010.user_movimenti_fiscalizzati.DTFORM) AS MINDATE FROM soger2010.user_movimenti_fiscalizzati WHERE soger2010.user_movimenti_fiscalizzati.ID_IMP='".$FEDIT->DbRecordSet[$r]['ID_IMP']."' AND soger2010.user_movimenti_fiscalizzati.TIPO='S' AND soger2010.user_movimenti_fiscalizzati.produttore=1 AND soger2010.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND soger2010.user_movimenti_fiscalizzati.ID_AZT<>0 AND soger2010.user_movimenti_fiscalizzati.Transfrontaliero=0  AND WEEK(soger2010.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2010.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2010.user_movimenti_fiscalizzati.DTFORM)>'2009' UNION DISTINCT ";

		$SQL2011 = "SELECT MIN(soger2011.user_movimenti_fiscalizzati.DTFORM) AS MINDATE FROM soger2011.user_movimenti_fiscalizzati WHERE soger2011.user_movimenti_fiscalizzati.ID_IMP='".$FEDIT->DbRecordSet[$r]['ID_IMP']."' AND soger2011.user_movimenti_fiscalizzati.TIPO='S' AND soger2011.user_movimenti_fiscalizzati.produttore=1 AND soger2011.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND soger2011.user_movimenti_fiscalizzati.ID_AZT<>0 AND soger2011.user_movimenti_fiscalizzati.Transfrontaliero=0  AND WEEK(soger2011.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2011.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2011.user_movimenti_fiscalizzati.DTFORM)>'2009' UNION DISTINCT ";

		$SQL2012 = "SELECT MIN(soger2012.user_movimenti_fiscalizzati.DTFORM) AS MINDATE FROM soger2012.user_movimenti_fiscalizzati WHERE soger2012.user_movimenti_fiscalizzati.ID_IMP='".$FEDIT->DbRecordSet[$r]['ID_IMP']."' AND soger2012.user_movimenti_fiscalizzati.TIPO='S' AND soger2012.user_movimenti_fiscalizzati.produttore=1 AND soger2012.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND soger2012.user_movimenti_fiscalizzati.ID_AZT<>0 AND soger2012.user_movimenti_fiscalizzati.Transfrontaliero=0  AND WEEK(soger2012.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2012.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2012.user_movimenti_fiscalizzati.DTFORM)>'2009' UNION DISTINCT ";

		$SQL2013 = "SELECT MIN(soger2013.user_movimenti_fiscalizzati.DTFORM) AS MINDATE FROM soger2013.user_movimenti_fiscalizzati WHERE soger2013.user_movimenti_fiscalizzati.ID_IMP='".$FEDIT->DbRecordSet[$r]['ID_IMP']."' AND soger2013.user_movimenti_fiscalizzati.TIPO='S' AND soger2013.user_movimenti_fiscalizzati.produttore=1 AND soger2013.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND soger2013.user_movimenti_fiscalizzati.ID_AZT<>0 AND soger2013.user_movimenti_fiscalizzati.Transfrontaliero=0  AND WEEK(soger2013.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2013.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2013.user_movimenti_fiscalizzati.DTFORM)>'2009' UNION DISTINCT ";

		$SQL2014 = "SELECT MIN(soger2014.user_movimenti_fiscalizzati.DTFORM) AS MINDATE FROM soger2014.user_movimenti_fiscalizzati WHERE soger2014.user_movimenti_fiscalizzati.ID_IMP='".$FEDIT->DbRecordSet[$r]['ID_IMP']."' AND soger2014.user_movimenti_fiscalizzati.TIPO='S' AND soger2014.user_movimenti_fiscalizzati.produttore=1 AND soger2014.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND soger2014.user_movimenti_fiscalizzati.ID_AZT<>0 AND soger2014.user_movimenti_fiscalizzati.Transfrontaliero=0  AND WEEK(soger2014.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2014.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2014.user_movimenti_fiscalizzati.DTFORM)>'2009' UNION DISTINCT ";

		$SQL2015 = "SELECT MIN(soger2015.user_movimenti_fiscalizzati.DTFORM) AS MINDATE FROM soger2015.user_movimenti_fiscalizzati WHERE soger2015.user_movimenti_fiscalizzati.ID_IMP='".$FEDIT->DbRecordSet[$r]['ID_IMP']."' AND soger2015.user_movimenti_fiscalizzati.TIPO='S' AND soger2015.user_movimenti_fiscalizzati.produttore=1 AND soger2015.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND soger2015.user_movimenti_fiscalizzati.ID_AZT<>0 AND soger2015.user_movimenti_fiscalizzati.Transfrontaliero=0  AND WEEK(soger2015.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2015.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2015.user_movimenti_fiscalizzati.DTFORM)>'2009' UNION DISTINCT ";

		switch($YEAR_FOR_MINDATE){
			case 2010:
				$SQL.=$SQL2010.$SQL2011.$SQL2012.$SQL2013.$SQL2014.$SQL2015;
				break;
			case 2011:
				$SQL.=$SQL2011.$SQL2012.$SQL2013.$SQL2014.$SQL2015;
				break;
			case 2012:
				$SQL.=$SQL2012.$SQL2013.$SQL2014.$SQL2015;
				break;
			case 2013:
				$SQL.=$SQL2013.$SQL2014.$SQL2015;
				break;
			case 2014:
				$SQL.=$SQL2014.$SQL2015;
				break;
			case 2015:
				$SQL.=$SQL2015;
				break;
			}


		// remove last UNION DISTINCT
		$SQL = substr($SQL, 0, strlen($SQL)-15);

		$SQL.=" ) AS t";

		$FEDIT->SdbRead($SQL,"DbRecordSetMINDATE",true,false);
		$MINDATE = $FEDIT->DbRecordSetMINDATE[0]['MINDATE'];
		$IMP_AND_DATE[$FEDIT->DbRecordSet[$r]['ID_IMP']] = $MINDATE;
		array_push($IMP_CON_DATA_AVVIO, $FEDIT->DbRecordSet[$r]['ID_IMP']);
		}

	$ddate = $FEDIT->DbRecordSet[$r]['SETTIMANA_DAL'];
	$date  = new DateTime($ddate);
	$week  = $date->format("W");

/*
	echo "<tr>";
	echo "<td>".$FEDIT->DbRecordSet[$r]['COD_CER']."</td>";
	echo "<td>".$FEDIT->DbRecordSet[$r]['ID_IMP']."</td>";
	echo "<td>".$FEDIT->DbRecordSet[$r]['REGIONE']."</td>";
	echo "<td>".$FEDIT->DbRecordSet[$r]['KG']."</td>";
	echo "<td>".$FEDIT->DbRecordSet[$r]['ANNO']."</td>";
	echo "<td>".$week."</td>";
	echo "<td>".$FEDIT->DbRecordSet[$r]['SETTIMANA_DAL']."</td>";
	echo "<td>".$FEDIT->DbRecordSet[$r]['SETTIMANA_AL']."</td>";
	echo "<td>".$IMP_AND_DATE[$FEDIT->DbRecordSet[$r]['ID_IMP']]."</td>";
	echo "</tr>";
*/

	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$r]['COD_CER']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$r]['ID_IMP']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$r]['REGIONE']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$r]['KG']);
	//$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$r]['ANNO']);
	$CSVbuf .= AddCSVcolumn($week);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$r]['SETTIMANA_DAL']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$r]['SETTIMANA_AL']);
	$CSVbuf .= AddCSVcolumn($IMP_AND_DATE[$FEDIT->DbRecordSet[$r]['ID_IMP']]);
	$CSVbuf .= AddCSVRow();

	}

//echo "</table>";

CSVOut($CSVbuf,$DcName);
	
require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>