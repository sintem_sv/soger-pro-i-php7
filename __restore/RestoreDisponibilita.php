<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/zip3.php");
require_once("../__scripts/STATS_funct.php");
require_once("../__scripts/MUD_funct.php");

global $SOGER;

if(isset($_GET['TableName']))
	$TableName  = $_GET['TableName'];
if(isset($_GET['ID_IMP']))
	$ID_IMP		= $_GET['ID_IMP'];
if(isset($_GET['workmode']))
	$workmode	= $_GET['workmode'];
if(isset($_GET['PrintOut'])) 
	$PrintOut	= $_GET['PrintOut'];

if($TableName=="user_movimenti") $IDMov="ID_MOV"; else $IDMov="ID_MOV_F";


$SQL ="SELECT ".$IDMov.", user_schede_rifiuti.giac_ini, user_schede_rifiuti.ID_UMIS,  user_schede_rifiuti.peso_spec, ".$TableName.".ID_RIF, DTMOV, NMOV, TIPO, quantita, pesoN, ".$TableName.".FKEdisponibilita, lov_cer.COD_CER, user_schede_rifiuti.descrizione ";
$SQL.="FROM ".$TableName." ";
$SQL.="JOIN user_schede_rifiuti ON ".$TableName.".ID_RIF=user_schede_rifiuti.ID_RIF ";
$SQL.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
$SQL.="WHERE ".$TableName.".ID_IMP='".$ID_IMP."' AND ".$TableName.".".$workmode."=1 ORDER BY ".$TableName.".ID_RIF, NMOV ASC";

if($PrintOut) echo $SQL."<br /><br />";
$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
$notShownYet=true;
$rifBuffer=0;

for($m=0; $m<count($FEDIT->DbRecordSet); $m++){

	if($FEDIT->DbRecordSet[$m]['ID_RIF']!=$rifBuffer){
		# converto giac_ini in kg
		switch($FEDIT->DbRecordSet[$m]['ID_UMIS']){
			case 1:
				$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'];
				break;
			case 2:
				$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'] *  $FEDIT->DbRecordSet[$m]['peso_spec'];
				break;
			case 3:
				$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'] *  $FEDIT->DbRecordSet[$m]['peso_spec'] * 1000;
				break;
			}

		$Disponibilita=$KG_giac_ini;
		$rifBuffer=$FEDIT->DbRecordSet[$m]['ID_RIF'];
		$notShownYet=true;
		if($PrintOut){
			echo "</table><br />";
			echo "<table style=\"width:800px;border: 1px solid black\">";
			# intestazione colonne tabella
			echo "<tr>";
			echo "<td style=\"width:100px;border: 1px solid black\">RIFIUTO</td>";
			echo "<td style=\"width:100px;border: 1px solid black\">ID</td>";
			echo "<td style=\"width:100px;border: 1px solid black\">NMOV</td>";
			echo "<td style=\"width:100px;border: 1px solid black\">DATA</td>";
			echo "<td style=\"width:100px;border: 1px solid black\">TIPO</td>";
			echo "<td style=\"width:100px;border: 1px solid black\">QUANTITA'</td>";
			echo "<td style=\"width:100px;border: 1px solid black\">PESO NETTO</td>";
			echo "<td style=\"width:100px;border: 1px solid black\">DISPONIBILITA' DB</td>";
			echo "<td style=\"width:100px;border: 1px solid black\">DISPONIBILITA' Ricalcolata</td>";
			echo "<td style=\"width:100px;border: 1px solid black\">DISPONIBILITA' Dopo movimento</td>";
			echo "<td style=\"width:100px;border: 1px solid black\">Query</td>";
			echo "</tr>";
			}
		}
	else{
		for($i=0;$i<$m;$i++){
			if($FEDIT->DbRecordSet[$i]['ID_RIF']==$FEDIT->DbRecordSet[$m]['ID_RIF']){
				if($FEDIT->DbRecordSet[$i]['TIPO']=='C')
					$Disponibilita+=$FEDIT->DbRecordSet[$i]['pesoN'];
				else
					$Disponibilita-=$FEDIT->DbRecordSet[$i]['pesoN'];
				}
			}
		}

	if($FEDIT->DbRecordSet[$m]['giac_ini']>0 and $notShownYet and $PrintOut){
		$notShownYet=false;
		echo "<tr>";
		echo "<td style=\"width:100px;border: 1px solid black\">".$FEDIT->DbRecordSet[$m]['COD_CER']." ".$FEDIT->DbRecordSet[$m]['descrizione']."</td>";
		echo "<td style=\"width:100px;border: 1px solid black\">-</td>";
		echo "<td style=\"width:100px;border: 1px solid black\">Giac.Iniz.</td>";
		echo "<td style=\"width:100px;border: 1px solid black\">-</td>";
		echo "<td style=\"width:100px;border: 1px solid black\">C</td>";
		echo "<td style=\"width:100px;border: 1px solid black\">".$FEDIT->DbRecordSet[$m]['giac_ini']. "</td>";
		# converto giac_ini in kg
		switch($FEDIT->DbRecordSet[$m]['ID_UMIS']){
			case 1:
				$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'];
				break;
			case 2:
				$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'] *  $FEDIT->DbRecordSet[$m]['peso_spec'];
				break;
			case 3:
				$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'] *  $FEDIT->DbRecordSet[$m]['peso_spec'] * 1000;
				break;
			}
		echo "<td style=\"width:100px;border: 1px solid black\">".$KG_giac_ini. "</td>";
		echo "<td style=\"width:100px;border: 1px solid black\">0</td>";
		echo "<td style=\"width:100px;border: 1px solid black\">0</td>";
		echo "<td style=\"width:100px;border: 1px solid black\">".$KG_giac_ini. "</td>";
		echo "<td style=\"width:100px;border: 1px solid black\"></td>";
		echo "</tr>";
		}

	if($PrintOut){
		echo "<tr>";
		echo "<td style=\"width:100px;border: 1px solid black\">".$FEDIT->DbRecordSet[$m]['COD_CER']." ".$FEDIT->DbRecordSet[$m]['descrizione']."</td>";
		echo "<td style=\"width:100px;border: 1px solid black\">".$FEDIT->DbRecordSet[$m][$IDMov]." </td>";
		echo "<td style=\"width:100px;border: 1px solid black\">".$FEDIT->DbRecordSet[$m]['NMOV']." </td>";
		echo "<td style=\"width:100px;border: 1px solid black\">".$FEDIT->DbRecordSet[$m]['DTMOV']." </td>";
		echo "<td style=\"width:100px;border: 1px solid black\">".$FEDIT->DbRecordSet[$m]['TIPO']."</td>";
		echo "<td style=\"width:100px;border: 1px solid black\">".$FEDIT->DbRecordSet[$m]['quantita']. "</td>";
		echo "<td style=\"width:100px;border: 1px solid black\">".$FEDIT->DbRecordSet[$m]['pesoN']. "</td>";
		echo "<td style=\"width:100px;border: 1px solid black\">".$FEDIT->DbRecordSet[$m]['FKEdisponibilita']."</td>";
		}
	if($FEDIT->DbRecordSet[$m]['FKEdisponibilita']<>number_format($Disponibilita, 2, '.', '')){
		
		if($PrintOut){ 
			if(number_format($Disponibilita, 2, '.', '')<0){
				echo "<td style=\"width:100px;border: 1px solid black; color:red; font-weight:bold;\"># ".number_format($Disponibilita, 2, '.', '')."</td>";
				}
			else{
				echo "<td style=\"width:100px;border: 1px solid black; color:red; font-weight:bold;\">".number_format($Disponibilita, 2, '.', '')."</td>";
				}
			}

		$sql="UPDATE ".$TableName." SET FKEdisponibilita=".number_format($Disponibilita, 2, '.', '')." WHERE ".$IDMov."=".$FEDIT->DbRecordSet[$m][$IDMov];
		$FEDIT->SDBWrite($sql,true,false);
		}
	else{
		if($PrintOut){
			if(number_format($Disponibilita, 2, '.', '')<0){
				echo "<td style=\"width:100px;border: 1px solid black; color:red; font-weight:bold;\"># ".number_format($Disponibilita, 2, '.', '')."</td>";
				}
			else{
				echo "<td style=\"width:100px;border: 1px solid black\">".number_format($Disponibilita, 2, '.', '')."</td>";
				}
			}
		$sql="";
		}

	if($PrintOut){
		if($FEDIT->DbRecordSet[$m]['TIPO']=='C'){
			$DisponibilitaSeguente = $Disponibilita + $FEDIT->DbRecordSet[$m]['pesoN'];
			echo "<td style=\"width:100px;border: 1px solid black\">".number_format($DisponibilitaSeguente, 2, '.', '')."</td>";
			}
		else{
			$DisponibilitaSeguente = $Disponibilita - $FEDIT->DbRecordSet[$m]['pesoN'];
			if(number_format($DisponibilitaSeguente, 2, '.', '')<0){
				echo "<td style=\"width:100px;border: 1px solid black; color:red; font-weight:bold;\"># ".number_format($DisponibilitaSeguente, 2, '.', '')."</td>";
				}
			else
				echo "<td style=\"width:100px;border: 1px solid black\">".number_format($DisponibilitaSeguente, 2, '.', '')."</td>";			
			}
		}
	
	if($PrintOut) 
		echo "<td style=\"width:100px;border: 1px solid black\">".$sql."</td>";

	# converto giac_ini in kg
	switch($FEDIT->DbRecordSet[$m]['ID_UMIS']){
		case 1:
			$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'];
			break;
		case 2:
			$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'] *  $FEDIT->DbRecordSet[$m]['peso_spec'];
			break;
		case 3:
			$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'] *  $FEDIT->DbRecordSet[$m]['peso_spec'] * 1000;
			break;
		}

	$Disponibilita=$KG_giac_ini;

	
	if($PrintOut) echo "</tr>";

	}

if($PrintOut) echo "</table>";


## aggiorno riferimenti carico
$LegamiSISTRI	= false;
require("../__scripts/MovimentiRifMovCarico.php");




//print_r($FEDIT->DbRecordSet);

//$FEDIT->SDBWrite($SQL,true,false);

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>
