<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/zip3.php");
require_once("../__scripts/STATS_funct.php");
require_once("../__scripts/MUD_funct.php");

global $SOGER;

$SQL ="SELECT ID_RIF, COD_CER, descrizione, lov_stato_fisico.description AS stato_fisico, CAR_Ciclo, et_comp_per, CAR_MatPrime ";
$SQL.="FROM user_schede_rifiuti ";
$SQL.="JOIN lov_stato_fisico ON lov_stato_fisico.ID_SF=user_schede_rifiuti.ID_SF ";
$SQL.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
$SQL.="WHERE ID_IMP='001NTDIM' ORDER BY COD_CER, descrizione ";
$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);

for($r=0;$r<count($FEDIT->DbRecordSet);$r++){
	echo $FEDIT->DbRecordSet[$r]['COD_CER'].";";
	echo " ;";
	echo $FEDIT->DbRecordSet[$r]['descrizione'].";";
	echo $FEDIT->DbRecordSet[$r]['stato_fisico'].";";
	echo $FEDIT->DbRecordSet[$r]['CAR_Ciclo'].";";
	echo $FEDIT->DbRecordSet[$r]['et_comp_per'].";";
	echo $FEDIT->DbRecordSet[$r]['CAR_MatPrime'].";";
	
	//lavorazioni
	$sql = "SELECT lov_processi.description AS processo FROM user_schede_rifiuti_processi JOIN lov_processi ON lov_processi.ID_PROC=user_schede_rifiuti_processi.ID_PROC WHERE user_schede_rifiuti_processi.ID_RIF=".$FEDIT->DbRecordSet[$r]['ID_RIF'].";";
	$FEDIT->SdbRead($sql,"DbRecordSetProcessi",true,false);
	$processi = "";
	if(isset($FEDIT->DbRecordSetProcessi)){
		for($p=0;$p<count($FEDIT->DbRecordSetProcessi);$p++){
			$processi .= $FEDIT->DbRecordSetProcessi[$p]['processo']." - ";
			}
		$processi = substr($processi,0,strlen($processi)-3);
		}
	echo $processi.";";
	
	//macchinari
	$sql = "SELECT lov_macchinari.description AS macchina FROM user_schede_rifiuti_macchinari JOIN lov_macchinari ON lov_macchinari.ID_MAC=user_schede_rifiuti_macchinari.ID_MAC WHERE user_schede_rifiuti_macchinari.ID_RIF=".$FEDIT->DbRecordSet[$r]['ID_RIF'].";";
	$FEDIT->SdbRead($sql,"DbRecordSetMacchinari",true,false);
	$macchinari = "";
	if(isset($FEDIT->DbRecordSetMacchinari)){
		for($m=0;$m<count($FEDIT->DbRecordSetMacchinari);$m++){
			$macchinari .= $FEDIT->DbRecordSetMacchinari[$m]['macchina']." - ";
			}
		$macchinari = substr($macchinari,0,strlen($macchinari)-3);
		}
	echo $macchinari.";";

	echo "<br />";
	}

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>