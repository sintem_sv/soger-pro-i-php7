<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/zip3.php");
require_once("../__scripts/STATS_funct.php");
require_once("../__scripts/MUD_funct.php");

global $SOGER;


$SQL="SELECT DISTINCT ID_CER FROM user_schede_rifiuti WHERE ID_CER>0;";
$FEDIT->SdbRead($SQL,"CERs",true,false);
for($c=0;$c<count($FEDIT->CERs);$c++){
	
	#CODICE CER
	$SQL2="SELECT COD_CER FROM lov_cer WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER'].";";
	$FEDIT->SdbRead($SQL2,"CER",true,false);
	if($FEDIT->DbRecsNum>0){
		
		//echo $FEDIT->CER[0]['COD_CER'].";";

		#N� SCHEDE DEL CODICE CER
		$SQL3="SELECT COUNT( ID_CER ) AS QTA FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER'].";";
		$FEDIT->SdbRead($SQL3,"QTA",true,false);
		$TOTALI_CER=$FEDIT->QTA[0]['QTA'];
		echo $TOTALI_CER.";";

		#N� SCHEDE DEL CODICE CER CON H1
		$H1="SELECT COUNT( H1 ) AS H1 FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER']." AND H1=1;";
		$FEDIT->SdbRead($H1,"H1",true,false);
		$SCHEDE_H1=$FEDIT->H1[0]['H1'];
		$PERC_H1=$SCHEDE_H1/$TOTALI_CER*100;
		echo number_format($PERC_H1, 2, ',', '').";";
		
		#N� SCHEDE DEL CODICE CER CON H2
		$H2="SELECT COUNT( H2 ) AS H2 FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER']." AND H2=1;";
		$FEDIT->SdbRead($H2,"H2",true,false);
		$SCHEDE_H2=$FEDIT->H2[0]['H2'];
		$PERC_H2=$SCHEDE_H2/$TOTALI_CER*100;
		echo number_format($PERC_H2, 2, ',', '').";";
		
		#N� SCHEDE DEL CODICE CER CON H3A
		$H3A="SELECT COUNT( H3A ) AS H3A FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER']." AND H3A=1;";
		$FEDIT->SdbRead($H3A,"H3A",true,false);
		$SCHEDE_H3A=$FEDIT->H3A[0]['H3A'];
		$PERC_H3A=$SCHEDE_H3A/$TOTALI_CER*100;
		echo number_format($PERC_H3A, 2, ',', '').";";
		
		#N� SCHEDE DEL CODICE CER CON H3B
		$H3B="SELECT COUNT( H3B ) AS H3B FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER']." AND H3B=1;";
		$FEDIT->SdbRead($H3B,"H3B",true,false);
		$SCHEDE_H3B=$FEDIT->H3B[0]['H3B'];
		$PERC_H3B=$SCHEDE_H3B/$TOTALI_CER*100;
		echo number_format($PERC_H3B, 2, ',', '').";";
		
		#N� SCHEDE DEL CODICE CER CON H4
		$H4="SELECT COUNT( H4 ) AS H4 FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER']." AND H4=1;";
		$FEDIT->SdbRead($H4,"H4",true,false);
		$SCHEDE_H4=$FEDIT->H4[0]['H4'];
		$PERC_H4=$SCHEDE_H4/$TOTALI_CER*100;
		echo number_format($PERC_H4, 2, ',', '').";";
		
		#N� SCHEDE DEL CODICE CER CON H1
		$H5="SELECT COUNT( H5 ) AS H5 FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER']." AND H5=1;";
		$FEDIT->SdbRead($H5,"H5",true,false);
		$SCHEDE_H5=$FEDIT->H5[0]['H5'];
		$PERC_H5=$SCHEDE_H5/$TOTALI_CER*100;
		echo number_format($PERC_H5, 2, ',', '').";";
		
		#N� SCHEDE DEL CODICE CER CON H6
		$H6="SELECT COUNT( H6 ) AS H6 FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER']." AND H6=1;";
		$FEDIT->SdbRead($H6,"H6",true,false);
		$SCHEDE_H6=$FEDIT->H6[0]['H6'];
		$PERC_H6=$SCHEDE_H6/$TOTALI_CER*100;
		echo number_format($PERC_H6, 2, ',', '').";";
		
		#N� SCHEDE DEL CODICE CER CON H1
		$H7="SELECT COUNT( H7 ) AS H7 FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER']." AND H7=1;";
		$FEDIT->SdbRead($H7,"H7",true,false);
		$SCHEDE_H7=$FEDIT->H7[0]['H7'];
		$PERC_H7=$SCHEDE_H7/$TOTALI_CER*100;
		echo number_format($PERC_H7, 2, ',', '').";";
		
		#N� SCHEDE DEL CODICE CER CON H8
		$H8="SELECT COUNT( H8 ) AS H8 FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER']." AND H8=1;";
		$FEDIT->SdbRead($H8,"H8",true,false);
		$SCHEDE_H8=$FEDIT->H8[0]['H8'];
		$PERC_H8=$SCHEDE_H8/$TOTALI_CER*100;
		echo number_format($PERC_H8, 2, ',', '').";";
		
		#N� SCHEDE DEL CODICE CER CON H9
		$H9="SELECT COUNT( H9 ) AS H9 FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER']." AND H9=1;";
		$FEDIT->SdbRead($H9,"H9",true,false);
		$SCHEDE_H9=$FEDIT->H9[0]['H9'];
		$PERC_H9=$SCHEDE_H9/$TOTALI_CER*100;
		echo number_format($PERC_H9, 2, ',', '').";";
		
		#N� SCHEDE DEL CODICE CER CON H10
		$H10="SELECT COUNT( H10 ) AS H10 FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER']." AND H10=1;";
		$FEDIT->SdbRead($H10,"H10",true,false);
		$SCHEDE_H10=$FEDIT->H10[0]['H10'];
		$PERC_H10=$SCHEDE_H10/$TOTALI_CER*100;
		echo number_format($PERC_H10, 2, ',', '').";";
		
		#N� SCHEDE DEL CODICE CER CON H11
		$H11="SELECT COUNT( H11 ) AS H11 FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER']." AND H11=1;";
		$FEDIT->SdbRead($H11,"H11",true,false);
		$SCHEDE_H11=$FEDIT->H11[0]['H11'];
		$PERC_H11=$SCHEDE_H11/$TOTALI_CER*100;
		echo number_format($PERC_H11, 2, ',', '').";";
		
		#N� SCHEDE DEL CODICE CER CON H12
		$H12="SELECT COUNT( H12 ) AS H12 FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER']." AND H12=1;";
		$FEDIT->SdbRead($H12,"H12",true,false);
		$SCHEDE_H12=$FEDIT->H12[0]['H12'];
		$PERC_H12=$SCHEDE_H12/$TOTALI_CER*100;
		echo number_format($PERC_H12, 2, ',', '').";";
		
		#N� SCHEDE DEL CODICE CER CON H13
		$H13="SELECT COUNT( H13 ) AS H13 FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER']." AND H13=1;";
		$FEDIT->SdbRead($H13,"H13",true,false);
		$SCHEDE_H13=$FEDIT->H13[0]['H13'];
		$PERC_H13=$SCHEDE_H13/$TOTALI_CER*100;
		echo number_format($PERC_H13, 2, ',', '').";";
		
		#N� SCHEDE DEL CODICE CER CON H14
		$H14="SELECT COUNT( H14 ) AS H14 FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER']." AND H14=1;";
		$FEDIT->SdbRead($H14,"H14",true,false);
		$SCHEDE_H14=$FEDIT->H14[0]['H14'];
		$PERC_H14=$SCHEDE_H14/$TOTALI_CER*100;
		echo number_format($PERC_H14, 2, ',', '').";";
		
		#N� SCHEDE DEL CODICE CER CON H15
		$H15="SELECT COUNT( H15 ) AS H15 FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER']." AND H15=1;";
		$FEDIT->SdbRead($H15,"H15",true,false);
		$SCHEDE_H15=$FEDIT->H15[0]['H15'];
		$PERC_H15=$SCHEDE_H15/$TOTALI_CER*100;
		echo number_format($PERC_H15, 2, ',', '').";";
		
		echo "<br />";
		}
	}

/*
SELECT COD_CER, H1, H2, H3A, H3B, H4, H5, H6, H7, H8, H9, H10, H11, H12, H13, H14, H15
FROM user_schede_rifiuti
JOIN lov_cer ON user_schede_rifiuti.ID_CER = lov_cer.ID_CER
WHERE lov_cer.PERICOLOSO =1 ORDER BY COD_CER ASC
*/

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>