<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/zip3.php");
require_once("../__scripts/MUD_funct.php"); //?

global $SOGER;

$TableName  = 'user_movimenti_fiscalizzati';
$PrintOut   = 'true';
$IDMov      = "ID_MOV_F";

$sqlSchedeRifiuto = "SELECT COUNT(ID_RIF) AS totali FROM user_schede_rifiuti;";
$FEDIT->SDBRead($sqlSchedeRifiuto,"SchedeRifiuto",true,false);
$SchedeRifiuto = $FEDIT->SchedeRifiuto[0]['totali'];

$Passo = 1000;
$CicliRedirect = ceil($SchedeRifiuto/$Passo);
$CicloCorrente = $_GET['CicloCorrente']? $_GET['CicloCorrente']:1;

$LIMIT = $Passo*$CicloCorrente;
$LOG = '';
        
# aggiorno la disponibilitÓ sulla tabella schede rifiuti
$LOG.="Aggiorno user_schede_rifiuti.disponibilita - START: ".date("H:i:s")."\r\n";
$sqlDisponibilita ="SELECT ID_RIF FROM user_schede_rifiuti LIMIT ".$LIMIT.", 1000";
$FEDIT->SDBRead($sqlDisponibilita,"DispoRif",true,false);
for($r=0;$r<count($FEDIT->DispoRif);$r++){
    $IMP_FILTER=false;
    $DISPO_PRINT_OUT = false;
    $IDRIF = $FEDIT->DispoRif[$r]['ID_RIF'];
    $TableName          = "user_movimenti_fiscalizzati";
    $IDMov		= "ID_MOV_F";
    $EXCLUDE_9999999	= true;
    require("../__scripts/ChiusuraAnno-MovimentiDispoRIF.php");
    if($Disponibilita<0) $Disponibilita=0;
    $sql = "UPDATE user_schede_rifiuti SET FKEdisponibilita='$Disponibilita' WHERE ID_RIF='" . $FEDIT->DispoRif[$r]['ID_RIF'] . "'";
    $LOG.=$sql."\r\n";
    $FEDIT->SDBWrite($sql,true,false);
    }
$LOG.="Aggiorno user_schede_rifiuti.disponibilita - END: ".date("H:i:s")."\r\n";

if($CicloCorrente<$CicliRedirect){
    
    $LOG.="\r\n*** FINE CICLO ". $CicloCorrente . " di " . $CicliRedirect ." ***\r\n\r\n";
    
    $tmpName	= tempnam(sys_get_temp_dir(), 'DISPO_LOG.txt');
    $file	= fopen($tmpName, 'a');
    fwrite($file, $LOG);
    fclose($file);
    
    $CicloCorrente++;
    header('Location:'.$_SERVER['PHP_SELF'].'?CicloCorrente='.$CicloCorrente);
    }
else{
    $LOG.="\r\n*** FINITO TUTTI I CICLI ***";
    
    $tmpName	= tempnam(sys_get_temp_dir(), 'DISPO_LOG.txt');
    $file	= fopen($tmpName, 'a');
    fwrite($file, $LOG);
    fclose($file);
    }


require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>