<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/zip3.php");
require_once("../__scripts/STATS_funct.php");
require_once("../__scripts/MUD_funct.php");

global $SOGER;

if(isset($_GET['TableName']))
	$TableName  = $_GET['TableName'];
if(isset($_GET['ID_IMP']))
	$ID_IMP		= $_GET['ID_IMP'];
if(isset($_GET['workmode']))
	$workmode	= $_GET['workmode'];

if($TableName=="user_movimenti") $IDMov="ID_MOV"; else $IDMov="ID_MOV_F";

$SQL ="SELECT NMOV, DTMOV, NFORM, DTFORM, FKErifCarico FROM ".$TableName." ";
$SQL.="WHERE ".$TableName.".ID_IMP='".$ID_IMP."' AND ".$TableName.".".$workmode."=1 AND TIPO='S' ORDER BY NMOV ASC";
//echo $SQL."<br /><br />";
$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);

# FKErifCarico could be 'Giac.Iniz.' or INT separated by comma ( 7, 13 )

echo "<table border='1'>";
	echo "<tr>";
		echo "<th>Num mov</th><th>Data mov</th><th>Num FIR</th><th>Data FIR</th><th>Rif. carico</th>";
	echo "</tr>";

for($m=0; $m<count($FEDIT->DbRecordSet); $m++){
	echo "<tr>";
		echo "<td>".$FEDIT->DbRecordSet[$m]['NMOV']."</td>";
		echo "<td>".$FEDIT->DbRecordSet[$m]['DTMOV']."</td>";
		echo "<td>".$FEDIT->DbRecordSet[$m]['NFORM']."</td>";
		echo "<td>".$FEDIT->DbRecordSet[$m]['DTFORM']."</td>";

		echo "<td>";
		$Carichi = explode(",",$FEDIT->DbRecordSet[$m]['FKErifCarico']);
		for($c=0;$c<count($Carichi);$c++){
			if(stripos($Carichi[$c], "giac")===false){
				$SQL ="SELECT DTMOV FROM ".$TableName." WHERE ID_IMP='".$ID_IMP."' AND ".$TableName.".".$workmode."=1 AND TIPO='C' AND NMOV=".$Carichi[$c].";";
				$FEDIT->SdbRead($SQL,"DbRecordSetCarico",true,false);
				$DataCarico = $FEDIT->DbRecordSetCarico[0]['DTMOV'];
				$DataScaricoFormattata_array=explode("-", $FEDIT->DbRecordSet[$m]['DTMOV']);
				$DataScaricoFormattata = $DataScaricoFormattata_array[1]."/".$DataScaricoFormattata_array[2]."/".$DataScaricoFormattata_array[0];
				$DataCaricoFormattata_array=explode("-", $DataCarico);
				$DataCaricoFormattata = $DataCaricoFormattata_array[1]."/".$DataCaricoFormattata_array[2]."/".$DataCaricoFormattata_array[0];
				$GiorniTrascorsi = dateDiff("d",$DataCaricoFormattata,$DataScaricoFormattata);
				if($GiorniTrascorsi>90) $GiorniTrascorsi="<span style='color:red;'>".$GiorniTrascorsi."</span>";
				echo "Carico ".$Carichi[$c]." del ".$DataCarico.": ".$GiorniTrascorsi." giorni trascorsi<br />";		
				}
			else{
				echo "Giacenza iniziale: impossibile determinare giorni trascorsi<br />";
				}
			}
		echo "</td>";

	echo "</tr>";
	}

echo "</table>";



// le date devono essere in formato m/d/Y
function dateDiff($interval,$dateTimeBegin,$dateTimeEnd) {
//{{{
	$dateTimeBegin=strtotime($dateTimeBegin);
	$dateTimeEnd=strtotime($dateTimeEnd);

	if($dateTimeEnd === -1 | $dateTimeBegin===-1) {
		return("either start or end date is invalid");
	}
	$dif=$dateTimeEnd - $dateTimeBegin;
	switch($interval) {
		case "d"://days
		return(floor($dif/86400)); //86400s=1d
		case "ww"://Week
		return(floor($dif/604800)); //604800s=1week=1semana
		case "m": //similar result "m" dateDiff Microsoft
		$monthBegin=(date("Y",$dateTimeBegin)*12)+
		date("n",$dateTimeBegin);
		$monthEnd=(date("Y",$dateTimeEnd)*12)+
		date("n",$dateTimeEnd);
		$monthDiff=$monthEnd-$monthBegin;
		return($monthDiff);
		case "yyyy": //similar result "yyyy" dateDiff Microsoft
		return(date("Y",$dateTimeEnd) - date("Y",$dateTimeBegin));
		default:
		return(floor($dif/86400)); //86400s=1d
	} //}}}
}




require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>