<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('max_execution_time', 900); // 15 min

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/zip3.php");
require_once("../__scripts/STATS_funct.php");
require_once("../__scripts/MUD_funct.php");

global $SOGER;

# aggiorno la disponibilitÓ di tutti i rifiuti per lo scorso anno, quindi da soger devo entrare nel db anno precedente
$LastYear=date('Y');$LastYear--;
$CurrentYear=date('Y');

$LastYear = 2021;
$CurrentYear = 2022;

$LIMIT = $_GET['LIMIT']? $_GET['LIMIT']:0;
$PASSO = 1000;

$SQL = "SELECT COUNT(DISTINCT(ID_RIF)) AS CONTA_ID_RIF FROM user_schede_rifiuti;";
$FEDIT->SDBRead($SQL,"countID_RIF",true,false);
$COUNT_ID_RIF = $FEDIT->countID_RIF[0]['CONTA_ID_RIF'];

$sqlDisponibilita="SELECT ID_RIF, ID_UMIS, peso_spec FROM soger".$LastYear.".user_schede_rifiuti ORDER BY ID_RIF LIMIT ".$LIMIT.", ".$PASSO.";";

$FEDIT->SDBRead($sqlDisponibilita,"DispoRif",true,false);
for($r=0;$r<count($FEDIT->DispoRif);$r++){
	$IMP_FILTER=false;
	$DISPO_PRINT_OUT = false;
	$IDRIF = $FEDIT->DispoRif[$r]['ID_RIF'];
	$TableName	= "user_movimenti_fiscalizzati";
	$IDMov		= "ID_MOV_F";
	$EXCLUDE_9999999	= true;
	require("../__scripts/MovimentiDispoRIF.php");
	if($Disponibilita<0) $Disponibilita=0;
	// la disponibilitÓ viene restituita in Kg, ma la giac_ini va indicata con l'unitÓ di misura della scheda rif
	
	switch($FEDIT->DispoRif[$r]['ID_UMIS']){
		// kg
		case 1: 
			$giac_ini = $Disponibilita;
			break;
		// litri
		case 2:
			$giac_ini = $Disponibilita / $FEDIT->DispoRif[$r]['peso_spec'];
			break;
		// mc
		case 3:
			$giac_ini = $Disponibilita / $FEDIT->DispoRif[$r]['peso_spec'] / 1000;
			break;
		}
	
	$Update="UPDATE soger".$CurrentYear.".user_schede_rifiuti SET giac_ini='$Disponibilita', FKEdisponibilita='$Disponibilita' WHERE ID_RIF='" . $FEDIT->DispoRif[$r]['ID_RIF'] . "'";
	$FEDIT->SDBWrite($Update,true,false);
	//print_r("ID_RIF='" . $FEDIT->DispoRif[$r]['ID_RIF'] . "'"." | ".$Disponibilita." | ".$GiacINI." | ".$LastCarico."<hr>");
	//print_r($Update."<hr>");
	}

if($LIMIT<$COUNT_ID_RIF){
	$LIMIT=$LIMIT+$PASSO;
    header('Location:'.$_SERVER['PHP_SELF'].'?LIMIT='.$LIMIT);
}
else
    die("FINITO!");

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>
