<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/zip3.php");
require_once("../__scripts/STATS_funct.php");
require_once("../__scripts/MUD_funct.php");

global $SOGER;

$CER = $_GET['CER'];

$SQL ="SELECT WEEK( DTFORM, 3 ) AS SETTIMANA, ROUND( SUM( IF( PS_DESTINO >0, PS_DESTINO, pesoN ) ) , 2 ) AS KG, ";
$SQL.="COUNT( DISTINCT (user_movimenti_fiscalizzati.ID_IMP) ) AS ImpiantiTotali, ";
$SQL.="ROUND( (ROUND( SUM( IF( PS_DESTINO >0, PS_DESTINO, pesoN ) ) , 2 ) / COUNT( DISTINCT (user_movimenti_fiscalizzati.ID_IMP) ) ) , 2) AS MediaImpianto ";
$SQL.="FROM user_movimenti_fiscalizzati ";
$SQL.="JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF = user_movimenti_fiscalizzati.ID_RIF ";
$SQL.="JOIN lov_cer ON lov_cer.ID_CER = user_schede_rifiuti.ID_CER WHERE COD_CER = '".$CER."' ";
$SQL.="AND TIPO = 'S' ";
$SQL.="AND user_movimenti_fiscalizzati.produttore =1 ";
$SQL.="AND ID_AZT <>0 ";
$SQL.="AND ID_AZT IS NOT NULL ";
$SQL.="GROUP BY SETTIMANA ";
$SQL.="ORDER BY SETTIMANA ";

$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);

for($w=0;$w<count($FEDIT->DbRecordSet);$w++){
	if(!is_null($FEDIT->DbRecordSet[$w]['SETTIMANA'])){
		echo $FEDIT->DbRecordSet[$w]['SETTIMANA'].";";
		echo number_format($FEDIT->DbRecordSet[$w]['KG'], 2, ',', '.').";";

		echo $FEDIT->DbRecordSet[$w]['ImpiantiTotali'].";";
		echo number_format($FEDIT->DbRecordSet[$w]['MediaImpianto'], 2, ',', '.').";";
		$SQL ="SELECT user_movimenti_fiscalizzati.ID_IMP, ROUND( SUM( IF( PS_DESTINO >0, PS_DESTINO, pesoN ) ) , 2 ) AS KG FROM user_movimenti_fiscalizzati ";
		$SQL.="JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF = user_movimenti_fiscalizzati.ID_RIF ";
		$SQL.="JOIN lov_cer ON lov_cer.ID_CER = user_schede_rifiuti.ID_CER ";
		$SQL.="WHERE COD_CER = '".$CER."' ";
		$SQL.="AND TIPO = 'S' ";
		$SQL.="AND user_movimenti_fiscalizzati.produttore =1 ";
		$SQL.="AND ID_AZT <>0 ";
		$SQL.="AND ID_AZT IS NOT NULL ";
		$SQL.="AND WEEK( DTFORM, 3 )=".$FEDIT->DbRecordSet[$w]['SETTIMANA']." ";
		$SQL.="GROUP BY user_movimenti_fiscalizzati.ID_IMP ";
		$FEDIT->SdbRead($SQL,"DbRecordSetImp",true,false);
		$SopraLaMedia=0;
		$SottoLaMedia=0;
		for($i=0;$i<count($FEDIT->DbRecordSetImp);$i++){
			if($FEDIT->DbRecordSetImp[$i]['KG']>=$FEDIT->DbRecordSet[$w]['MediaImpianto']){
				$SopraLaMedia++;
				}
			else{
				$SottoLaMedia++;
				}
			}
		echo $SopraLaMedia.";";
		echo $SottoLaMedia.";";
		echo "<br />";
		}
	}


require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>