<?php
/*
error_reporting(E_ALL);
ini_set("display_errors", 1);
*/

//session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__libs/zip3.php");
require_once("../__libs/fpdf.php");

function getStartAndEndDate($week, $year){
    $time = strtotime("1 January $year", time());
    $day = date('w', $time);
    $time += ((7*$week)+1-$day)*24*3600;
    $return[0] = date('Y-n-j', $time);
    $time += 6*24*3600;
    $return[1] = date('Y-n-j', $time);
    return $return;
	}

$CER	= array('120101', '120102', '150101', '150102', '150106');

$SQL ="SELECT CONCAT('Settimana n. ', WEEK(DTFORM), ' del ', YEAR(DTFORM)) AS SETTIMANA, COD_CER, user_movimenti_fiscalizzati.ID_IMP, ROUND( SUM( IF( PS_DESTINO >0, PS_DESTINO, pesoN ) ) , 2 ) AS KG ";
$SQL.="FROM user_movimenti_fiscalizzati ";
$SQL.="JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF = user_movimenti_fiscalizzati.ID_RIF ";
$SQL.="JOIN lov_cer ON lov_cer.ID_CER = user_schede_rifiuti.ID_CER ";
$SQL.="JOIN core_impianti ON core_impianti.ID_IMP=user_movimenti_fiscalizzati.ID_IMP ";
$SQL.="JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM=core_impianti.ID_COM ";
$SQL.="WHERE COD_CER IN ('120101', '120102', '150101', '150102', '150106') ";
$SQL.="AND TIPO = 'S' ";
$SQL.="AND Transfrontaliero=0 ";
$SQL.="AND user_movimenti_fiscalizzati.produttore =1 ";
$SQL.="AND ID_AZT <>0 ";
$SQL.="AND ID_AZT IS NOT NULL ";
$SQL.="AND WEEK(DTFORM) IS NOT NULL AND YEAR(DTFORM) IS NOT NULL ";
$SQL.="GROUP BY user_movimenti_fiscalizzati.ID_IMP, COD_CER, SETTIMANA ";
$SQL.="ORDER BY COD_CER, YEAR(DTFORM), WEEK(DTFORM)";

echo $SQL."<hr>";

	
die();

for($y=0; $y<count($YEAR); $y++){

	$FEDIT=new DbLink();
	$FEDIT->DbConn('soger'.$YEAR[$y]);

	for($c=0; $c<count($CER);$c++){

		$ARRAY_IMP = "CER_COMPANY_".$_GET['GRUPPO'];
		$ARRAY_IMP = ${$ARRAY_IMP}[$CER[$c]];

			//if(!$StampataIntestazione){
				echo "<tr>";

					echo "<th style=\"border:1px solid #000;\">anno</th>";
					echo "<th style=\"border:1px solid #000;\">cer</th>";
					echo "<th style=\"border:1px solid #000;\">impianto</th>";
					echo "<th style=\"border:1px solid #000;\">regione</th>";
					for($s=0;$s<53;$s++){
						$start_end = getStartAndEndDate($s, 2014);
						$w=$s+1;
						echo "<th style=\"border:1px solid #000;\">Settimana ".$w."<br />(".$start_end[0]." - ".$start_end[1].")</th>";
						}

				echo "</tr>";
			//	$StampataIntestazione = true;
			//	}


		for($a=0; $a<count($ARRAY_IMP); $a++){

			$SQL ="SELECT WEEK(DTFORM) AS SETTIMANA, ROUND( SUM( IF( PS_DESTINO >0, PS_DESTINO, pesoN ) ) , 2 ) AS KG ";
			$SQL.="FROM user_movimenti_fiscalizzati ";
			$SQL.="JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF = user_movimenti_fiscalizzati.ID_RIF ";
			$SQL.="JOIN lov_cer ON lov_cer.ID_CER = user_schede_rifiuti.ID_CER ";
			$SQL.="JOIN core_impianti ON core_impianti.ID_IMP=user_movimenti_fiscalizzati.ID_IMP ";
			$SQL.="JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM=core_impianti.ID_COM ";
			$SQL.="WHERE COD_CER = '".$CER[$c]."' ";
			$SQL.="AND TIPO = 'S' ";
			$SQL.="AND Transfrontaliero=0 ";
			$SQL.="AND user_movimenti_fiscalizzati.produttore =1 ";
			$SQL.="AND ID_AZT <>0 ";
			$SQL.="AND ID_AZT IS NOT NULL ";
			$SQL.="AND YEAR(DTFORM)=".$YEAR[$y]." ";
			$SQL.="AND user_movimenti_fiscalizzati.ID_IMP='".$ARRAY_IMP[$a]."' ";
			$SQL.="GROUP BY SETTIMANA ";
			$SQL.="ORDER BY WEEK(DTFORM)";

			//echo $SQL."<hr>";
			$FEDIT->SdbRead($SQL,"Kg",true,false);

			for($m=0; $m<count($FEDIT->Kg); $m++){
				$Kg[$FEDIT->Kg[$m]['SETTIMANA']] = $FEDIT->Kg[$m]['KG'];
				}

			$SQL_REG = "SELECT des_reg FROM lov_comuni_istat WHERE ID_COM=(SELECT ID_COM FROM core_impianti WHERE ID_IMP='".$ARRAY_IMP[$a]."');";
			$FEDIT->SdbRead($SQL_REG,"Regione",true,false);



			echo "<tr>";

				echo "<td style=\"border:1px solid #000;\">".$YEAR[$y]."</td>";
				echo "<td style=\"border:1px solid #000;\">".$CER[$c]."</td>";
				echo "<td style=\"border:1px solid #000;\">".$ARRAY_IMP[$a]."</td>";
				echo "<td style=\"border:1px solid #000;\">".$FEDIT->Regione[0]['des_reg']."</td>";
				for($s=0;$s<53;$s++){
					$w=$s+1;
					echo "<td style=\"border:1px solid #000;\">".(!$Kg[$w] ? '0,00' : str_replace('.',',',$Kg[$w]))."</td>";
					}

			echo "</tr>";

			unset($Kg);

			}

		}

	}

echo "</table>";

?>