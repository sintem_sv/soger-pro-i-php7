<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
 <HEAD>
  <TITLE> New Document </TITLE>
  <META NAME="Generator" CONTENT="EditPlus">
  <META NAME="Author" CONTENT="">
  <META NAME="Keywords" CONTENT="">
  <META NAME="Description" CONTENT="">
	<style type="text/css">
	table.DatiTable{
		}
	table.DatiTable td.LastUpdate{
		font-size: x-small;
		text-align:center;
		}
	table.DatiTable td, table.DatiTable th{
		color:#ffffff;
		font-family:verdana;
		font-size:small;
		}
	body{
		background-color:#23507D;
		padding:0; margin:0;
		}
	</style>
 </HEAD>

<?php
		$path = "../__config/DbServer.txt";
		$fileLines = file($path);
		foreach($fileLines as $LineNumber=>$Content) {
			$Content=rtrim($Content);
			
			# URL
			if(strstr($Content,"URL->")!==false) {
				$url = str_replace("URL->","",$Content);
				}

			# PORT	
			if(strstr($Content,"PRT->")!==false) {
				$tmp = str_replace("PRT->","",$Content);
				if($tmp=="default") {
					$port = null;
					} 
				else {
					$port = $tmp;
					}
				}

			# USR
			if(strstr($Content,"USR->")!==false) {
				$usr = str_replace("USR->","",$Content);
				}

			# PWD
			if(strstr($Content,"PWD->")!==false) {
				$tmp = str_replace("PWD->","",$Content);
				if($tmp=="default") {
					$pwd = "";
					} 
				else {
					$pwd = $tmp;
					}
				}
			}	

	$conn=mysqli_connect($url, $usr, $pwd);
	if(mysqli_select_db($conn,"soger".date('Y')) ){
		
		#carichi totali
		$sql="SELECT COUNT(ID_MOV) AS TOT_C FROM user_movimenti WHERE TIPO='C';";
		$result = mysqli_query($conn, $sql);
		$row = mysqli_fetch_assoc($result);
		$TOT_C=$row['TOT_C'];
		
		#carichi oggi
		$sql="SELECT COUNT(ID_MOV) AS C FROM user_movimenti WHERE TIPO='C' AND DTMOV='".date('Y-m-d')."';";
		$result = mysqli_query($conn, $sql);
		$row = mysqli_fetch_assoc($result);
		$C=$row['C'];
		
		#scarichi totali
		$sql="SELECT COUNT(ID_MOV) AS TOT_S FROM user_movimenti WHERE TIPO='S';";
		$result = mysqli_query($conn, $sql);
		$row = mysqli_fetch_assoc($result);
		$TOT_S=$row['TOT_S'];
		
		#scarichi oggi
		$sql="SELECT COUNT(ID_MOV) AS S FROM user_movimenti WHERE TIPO='S' AND DTMOV='".date('Y-m-d')."';";
		$result = mysqli_query($conn, $sql);
		$row = mysqli_fetch_assoc($result);
		$S=$row['S'];
		
		#formulari totali
		$sql="SELECT COUNT(ID_MOV) AS TOT_F FROM user_movimenti WHERE TIPO='S' AND NFORM<>'';";
		$result = mysqli_query($conn, $sql);
		$row = mysqli_fetch_assoc($result);
		$TOT_F=$row['TOT_F'];
		
		#formulari oggi
		$sql="SELECT COUNT(ID_MOV) AS F FROM user_movimenti WHERE TIPO='S' AND NFORM<>'' AND DTMOV='".date('Y-m-d')."';";
		$result = mysqli_query($conn, $sql);
		$row = mysqli_fetch_assoc($result);
		$F=$row['F'];
		}

	else echo "Impossibile connettersi al server MySQL";
	mysqli_close($conn);
?>


 <BODY>

	<table name="DatiTable" class="DatiTable">
		<tr>
			<th width="370">&nbsp;</th>
			<th width="75">Totali</th>
			<th width="75">Oggi</th>
			<th rowspan="4"><img src="stats.png" alt="Dati utilizzo So.Ge.R. PRO" title="Dati utilizzo So.Ge.R. PRO" /></td>
		</tr>
		<tr>
			<th align="left">Operazioni di carico registrate a sistema:</th>
			<td align="center"><?php echo $TOT_C; ?></td>
			<td align="center"><?php echo $C; ?></td>
		</tr>
		<tr>
			<th align="left">Operazioni di scarico registrate a sistema:</th>
			<td align="center"><?php echo $TOT_S; ?></td>
			<td align="center"><?php echo $S; ?></td>
		</tr>
		<tr>
			<th align="left">Formulari emessi da So.Ge.R. PRO</th>
			<td align="center"><?php echo $TOT_F; ?></td>
			<td align="center"><?php echo $F; ?></td>
		</tr>
		<tr>
			<td colspan="4" class="LastUpdate">Dati aggiornati al: <?php echo date ('d/m/Y').", ore ". date('H:i:s'); ?></td>
		</tr>
	</table>
  

 </BODY>
</HTML>
