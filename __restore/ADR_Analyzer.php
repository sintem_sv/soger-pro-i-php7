<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/zip3.php");
require_once("../__scripts/STATS_funct.php");
require_once("../__scripts/MUD_funct.php");

global $SOGER;


$SQL="SELECT DISTINCT ID_CER FROM user_schede_rifiuti WHERE ID_CER>0;";
$FEDIT->SdbRead($SQL,"CERs",true,false);
for($c=0;$c<count($FEDIT->CERs);$c++){
	
	#CODICE CER
	$SQL2="SELECT COD_CER, PERICOLOSO FROM lov_cer WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER'].";";
	$FEDIT->SdbRead($SQL2,"CER",true,false);
	if($FEDIT->DbRecsNum>0){
		
		echo $FEDIT->CER[0]['COD_CER'].";";
		echo $FEDIT->CER[0]['PERICOLOSO'].";";

		#N� SCHEDE DEL CODICE CER
		$SQL3="SELECT COUNT( ID_CER ) AS QTA FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER'].";";
		$FEDIT->SdbRead($SQL3,"QTA",true,false);
		$TOTALI_CER=$FEDIT->QTA[0]['QTA'];
		echo $TOTALI_CER.";";

		#N� SCHEDE DEL CODICE CER CON ADR=s�
		$ADR_YES="SELECT COUNT( adr ) AS ADR_YES FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER']." AND adr=1;";
		$FEDIT->SdbRead($ADR_YES,"ADR_YES",true,false);
		$SCHEDE_ADR_YES=$FEDIT->ADR_YES[0]['ADR_YES'];
		$PERC_ADR_YES=$SCHEDE_ADR_YES/$TOTALI_CER*100;
		//assoluto
		echo number_format($SCHEDE_ADR_YES, 0, ',', '').";";
		//%
		echo number_format($PERC_ADR_YES, 2, ',', '').";";
		
		#N� SCHEDE DEL CODICE CER CON ADR=no
		$ADR_NO="SELECT COUNT( adr ) AS ADR_NO FROM user_schede_rifiuti WHERE ID_CER=".$FEDIT->CERs[$c]['ID_CER']." AND adr=0;";
		$FEDIT->SdbRead($ADR_NO,"ADR_NO",true,false);
		$SCHEDE_ADR_NO=$FEDIT->ADR_NO[0]['ADR_NO'];
		$PERC_ADR_NO=$SCHEDE_ADR_NO/$TOTALI_CER*100;
		//assoluto
		echo number_format($SCHEDE_ADR_NO, 0, ',', '').";";
		//%
		echo number_format($PERC_ADR_NO, 2, ',', '').";";
		
		
		echo "<br />";
		}
	}

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>