<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__scripts/STATS_funct.php");

global $SOGER;

// solido pulverulento --> in polvere o polverulenti
$SQL="UPDATE user_schede_rifiuti SET ID_STATO_FISICO_RIFIUTO=1 WHERE ID_SF=1;";
$FEDIT->SDBWrite($SQL,true,false);

// solido non pulverulento --> solidi
$SQL="UPDATE user_schede_rifiuti SET ID_STATO_FISICO_RIFIUTO=2 WHERE ID_SF=2;";
$FEDIT->SDBWrite($SQL,true,false);

// fangoso palabile --> fangosi
$SQL="UPDATE user_schede_rifiuti SET ID_STATO_FISICO_RIFIUTO=4 WHERE ID_SF=3;";
$FEDIT->SDBWrite($SQL,true,false);

// liquido --> liquidi
$SQL="UPDATE user_schede_rifiuti SET ID_STATO_FISICO_RIFIUTO=5 WHERE ID_SF=4;";
$FEDIT->SDBWrite($SQL,true,false);


	
require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>
