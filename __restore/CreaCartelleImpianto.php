<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/zip3.php");
require_once("../__scripts/STATS_funct.php");
require_once("../__scripts/MUD_funct.php");

global $SOGER;

$SQL="SELECT ID_IMP FROM core_impianti";
$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);

if(!is_dir("../__documents/__reg")) mkdir("../__documents/__reg", 0777);
if(!is_dir("../__documents/__alarm")) mkdir("../__documents/__alarm", 0777);
if(!is_dir("../__documents/__adr")) mkdir("../__documents/__adr", 0777);


for($m=0; $m<count($FEDIT->DbRecordSet); $m++){

	
	if(!is_dir("../__documents/__reg/".$FEDIT->DbRecordSet[$m]["ID_IMP"])){
		echo "<h3> - Creo cartelle registro per l'impianto ".$FEDIT->DbRecordSet[$m]["ID_IMP"]."</h3>";
		mkdir("../__documents/__reg/".$FEDIT->DbRecordSet[$m]["ID_IMP"], 0777);
		}
	
	if(!is_dir("../__documents/__adr/".$FEDIT->DbRecordSet[$m]["ID_IMP"])){
		echo "<h3> - Creo cartelle registro ADR per l'impianto ".$FEDIT->DbRecordSet[$m]["ID_IMP"]."</h3>";
		mkdir("../__documents/__adr/".$FEDIT->DbRecordSet[$m]["ID_IMP"], 0777);
		}

	if(!is_dir("../__documents/__alarm/".$FEDIT->DbRecordSet[$m]["ID_IMP"])){
		echo "<h3> - Creo cartelle allarmi per l'impianto ".$FEDIT->DbRecordSet[$m]["ID_IMP"]."</h3>";
		mkdir("../__documents/__alarm/".$FEDIT->DbRecordSet[$m]["ID_IMP"], 0777);
		}

	$SQL="SELECT ID_USR FROM core_users WHERE ID_IMP='".$FEDIT->DbRecordSet[$m]["ID_IMP"]."';";
	$FEDIT->SdbRead($SQL,"DbRecordSetUsers",true,false);

	for($u=0;$u<count($FEDIT->DbRecordSetUsers);$u++){
		
		if(!is_dir("../__documents/__reg/".$FEDIT->DbRecordSet[$m]["ID_IMP"]."/".$FEDIT->DbRecordSetUsers[$u]['ID_USR'])) {
			echo "<h3> - - Creo cartelle registro per l'utente ".$FEDIT->DbRecordSetUsers[$u]['ID_USR']."</h3>";
			mkdir("../__documents/__reg/".$FEDIT->DbRecordSet[$m]["ID_IMP"]."/".$FEDIT->DbRecordSetUsers[$u]['ID_USR'], 0777);
			}		
		
		if(!is_dir("../__documents/__adr/".$FEDIT->DbRecordSet[$m]["ID_IMP"]."/".$FEDIT->DbRecordSetUsers[$u]['ID_USR'])){
			echo "<h3> - - Creo cartelle registro ADR per l'utente ".$FEDIT->DbRecordSetUsers[$u]['ID_USR']."</h3>";
			mkdir("../__documents/__adr/".$FEDIT->DbRecordSet[$m]["ID_IMP"]."/".$FEDIT->DbRecordSetUsers[$u]['ID_USR'], 0777);
			}

		if(!is_dir("../__documents/__alarm/".$FEDIT->DbRecordSet[$m]["ID_IMP"]."/".$FEDIT->DbRecordSetUsers[$u]['ID_USR'])) {
			echo "<h3> - - Creo cartelle allarmi per l'utente ".$FEDIT->DbRecordSetUsers[$u]['ID_USR']."</h3>";
			mkdir("../__documents/__alarm/".$FEDIT->DbRecordSet[$m]["ID_IMP"]."/".$FEDIT->DbRecordSetUsers[$u]['ID_USR'], 0777);
			}
		
		}
	echo "<hr>";
	}

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>