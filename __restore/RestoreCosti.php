<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/zip3.php");
require_once("../__libs/SQLFunct.php");
require_once("../__scripts/STATS_funct.php");
require_once("../__scripts/MUD_funct.php");
require_once("../__libs/SogerECO_funct.php");

global $SOGER;

if(isset($_GET['ID_IMP']))
	$impianto	= $_GET['ID_IMP'];
if(isset($_GET['workmode']))
	$workmode	= $_GET['workmode'];
if(isset($_GET['dal']))
	$dal	= $_GET['dal'];
else
	$dal	= date('Y').'-01-01';

$SQL="SELECT ID_MOV_F, ID_IMP FROM user_movimenti_fiscalizzati WHERE ID_IMP='".$impianto."' AND ".$workmode."=1 AND DTMOV>=".$dal.";";
//$SQL="SELECT DISTINCT ID_MOV_F, ID_IMP FROM `user_movimenti_costi_bis` WHERE ID_MOV_F IS NOT NULL AND ID_IMP <> '';"; 
$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
$Movimenti=$FEDIT->DbRecordSet;

$totale = count($Movimenti);

for($m=0;$m<$totale;$m++){
	$MovID		= $Movimenti[$m]['ID_MOV_F'];
	
	$SQL		= "SELECT ID_UIMD, ID_AZI, DTMOV FROM `user_movimenti_fiscalizzati` WHERE ID_MOV_F=".$MovID.";"; 
	$FEDIT->SdbRead($SQL,"DbRecordSetIds",true,false);
	
	$ID_AZI		= $FEDIT->DbRecordSetIds[0]['ID_AZI'];
	$ID_UIMD	= $FEDIT->DbRecordSetIds[0]['ID_UIMD'];
	$DTMOV		= $FEDIT->DbRecordSetIds[0]['DTMOV'];

	$sql = "DELETE FROM user_movimenti_costi WHERE ID_MOV_F='$MovID'";
	$FEDIT->SDBWrite($sql,true,false);
	
	$impianto	= $Movimenti[$m]['ID_IMP'];
	require("../__libs/SogerECO_GeneraCosti.php");
	$counter = $m+1;
	echo "Calcolati ".$counter." di ".$totale." movimenti<hr>";
	}
	
require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>