<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/zip3.php");
//require_once("../__scripts/MUD_funct.php"); //?

global $SOGER;


function RemoveCarichiScaricati($rif, $CarichiRifiuto){

if(isset($rif['totS'])) $TotS=$rif['totS']; else $TotS=0;
$giacOK=false;
$PrimoDaScaricare=0;

//if($rif['totC']>$TotS){
	# 0 - giacenza iniziale
	if($rif['giac_ini']>0 && !$giacOK){
		$CarichiRifiuto[$rif['RifID']][0]['qta']=$rif['giac_ini'];
		$CarichiRifiuto[$rif['RifID']][0]['num']=0;
		$CarichiRifiuto[$rif['RifID']][0]['ID_MOV_F']=0;
		$CarichiRifiuto[$rif['RifID']][0]['idSIS']=0;
		$CarichiRifiuto[$rif['RifID']][0]['FKEpesospecifico']=$rif['FKEpesospecifico'];
		$CarichiRifiuto[$rif['RifID']][0]['FKEumis']=$rif['FKEumis'];
		$CarichiRifiuto[$rif['RifID']][0]['ID_IMP']=$rif['ID_IMP'];
		$CarichiRifiuto[$rif['RifID']][0]['workmode']=$rif['workmode'];
		$CarichiRifiuto[$rif['RifID']][0]['data']="0000-00-00";
		$CarichiRifiuto[$rif['RifID']][0]['CER']=$rif['COD_CER'];
		$CarichiRifiuto[$rif['RifID']][0]['desc']=$rif['descrizione'];
		$giacOK=true;
		}

	# 1 - array carichi rifiuto
	for($k=0;$k<count($rif['carico']);$k++){
		        if(isset($CarichiRifiuto[$rif['RifID']])) $index=count($CarichiRifiuto[$rif['RifID']]); else $index=0;
                $CarichiRifiuto[$rif['RifID']][$index]['qta']=$rif['carico'][$k]['qta'];
                $CarichiRifiuto[$rif['RifID']][$index]['num']=$rif['carico'][$k]['num'];
                $CarichiRifiuto[$rif['RifID']][$index]['ID_MOV_F']=$rif['carico'][$k]['ID_MOV_F'];
                $CarichiRifiuto[$rif['RifID']][$index]['idSIS']=$rif['carico'][$k]['idSIS'];
                $CarichiRifiuto[$rif['RifID']][$index]['FKEpesospecifico']=$rif['FKEpesospecifico'];
                $CarichiRifiuto[$rif['RifID']][$index]['FKEumis']=$rif['FKEumis'];
                $CarichiRifiuto[$rif['RifID']][$index]['ID_IMP']=$rif['ID_IMP'];
                $CarichiRifiuto[$rif['RifID']][$index]['workmode']=$rif['workmode'];
                $CarichiRifiuto[$rif['RifID']][$index]['data']=$rif['carico'][$k]['data'];
                $CarichiRifiuto[$rif['RifID']][$index]['CER']=$rif['COD_CER'];
                $CarichiRifiuto[$rif['RifID']][$index]['desc']=$rif['descrizione'];
		}
	# 2 - sottraggo qta da scaricato fino a quando scaricato resta positivo
	for($i=0;$i<count($CarichiRifiuto[$rif['RifID']]);$i++){
		if($TotS>=0){
			$TotS-=$CarichiRifiuto[$rif['RifID']][$i]['qta'];
			//print_r("<br />Rifiuto ".$rif['RifID'].", elimino il carico ".$CarichiRifiuto[$rif['RifID']][$i]['num']." ( ".$i." ) restano da scaricare: ".$TotS."<br /><hr>");
			$CarichiRifiuto[$rif['RifID']][$i]['qta']=abs($TotS);
			if($TotS<0){
				$PrimoDaScaricare=$i;
				//print_r("Primo da scaricare: ".$i."<br />");
				}
			}
                        //else{
                            //print_r("Non mi restano carichi da scaricare");
                        //}
		}

	$count=count($CarichiRifiuto[$rif['RifID']]);
        for($i=0;$i<$count;$i++){
            //print_r("<br />Indice: ".$i." elimino se minore di ".$PrimoDaScaricare." ( count=".count($CarichiRifiuto[$rif['RifID']])." ) oppure ho scaricato tutto (Primo da scaricare = 0)");
            if($i<$PrimoDaScaricare || ($PrimoDaScaricare==0 && $rif['totS'])){
                //print_r("<br />Carico eliminato<br />");
                unset($CarichiRifiuto[$rif['RifID']][$i]);
            }
        }

        if(is_null($CarichiRifiuto[$rif['RifID']])) $CarichiRifiuto[$rif['RifID']] = [];

	$CarichiRifiuto[$rif['RifID']]=array_merge($CarichiRifiuto[$rif['RifID']]);

	return($CarichiRifiuto);
}

$TableName  = 'user_movimenti_fiscalizzati';
$PrintOut   = 'true';
$IDMov      = "ID_MOV_F";
$YEAR = date('Y');
$FEDIT->DbConn('soger'.$YEAR);
echo "<h1>Leggo rifiuti con disponibilita>0: ".date("H:i:s")."</h1>";

$sql ="SELECT originalID_RIF, ID_RIF FROM user_schede_rifiuti WHERE giac_ini>0;";
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
$rifs = array();
for($r=0;$r<count($FEDIT->DbRecordSet);$r++){
    $rifs[] = $FEDIT->DbRecordSet[$r]['originalID_RIF'];
    }
$id_rifs = implode(",", $rifs);

$YEAR=$YEAR-1;
$FEDIT->DbConn('soger'.$YEAR);

$sql  = "SELECT ID_MOV_F,idSIS,DTMOV,NMOV,quantita,user_movimenti_fiscalizzati.ID_UIMP,user_movimenti_fiscalizzati.originalID_RIF AS originalRifID,user_movimenti_fiscalizzati.ID_RIF AS RifID,TIPO,";
$sql .= " user_schede_rifiuti.ID_CER as CERref,user_schede_rifiuti.descrizione,user_schede_rifiuti.ID_RIFPROD_F,lov_cer.COD_CER,user_schede_rifiuti.giac_ini, ";
$sql .= " user_movimenti_fiscalizzati.ID_IMP, FKEumis, FKEpesospecifico, (CASE WHEN user_movimenti_fiscalizzati.produttore=1 THEN 'produttore' WHEN user_movimenti_fiscalizzati.trasportatore=1 THEN 'trasportatore' WHEN user_movimenti_fiscalizzati.destinatario=1 THEN 'destinatario' WHEN user_movimenti_fiscalizzati.intermediario=1 THEN 'intermediario' ELSE 'produttore' END) AS workmode ";
$sql .= " FROM user_movimenti_fiscalizzati";
$sql .= " JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF";
$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
$sql .= " WHERE user_movimenti_fiscalizzati.NMOV<>9999999 AND PerRiclassificazione=0 AND user_movimenti_fiscalizzati.originalID_RIF IN (".$id_rifs.") ";
$sql .= " ORDER BY COD_CER, descrizione ASC, RifID ASC, user_movimenti_fiscalizzati.ID_UIMP ASC, TIPO ASC, NMOV ASC, DTMOV ASC ";
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
$TotC = 0;
$TotS = 0;
$RIFbuf = "";
$UIMPbuf = "";
$rifiuti = array();
$CarichiRifiuto = array();

for($i=0;$i<count($FEDIT->DbRecordSet);$i++){
    $indice=count($rifiuti);
    if($FEDIT->DbRecordSet[$i]['originalRifID']!=$RIFbuf || $FEDIT->DbRecordSet[$i]['ID_UIMP']!=$UIMPbuf) {
        $rifiuti[$indice]['totC']=$FEDIT->DbRecordSet[$i]['giac_ini'];
        $rifiuti[$indice]['COD_CER']=$FEDIT->DbRecordSet[$i]['COD_CER'];
        $rifiuti[$indice]['CERref']=$FEDIT->DbRecordSet[$i]['CERref'];
        $rifiuti[$indice]['RifID']=$FEDIT->DbRecordSet[$i]['RifID'];
        $rifiuti[$indice]['originalRifID']=$FEDIT->DbRecordSet[$i]['originalRifID'];
        $rifiuti[$indice]['giac_ini']=$FEDIT->DbRecordSet[$i]['giac_ini'];
        $rifiuti[$indice]['descrizione']=$FEDIT->DbRecordSet[$i]['descrizione'];
        $rifiuti[$indice]['ID_RIFPROD_F']=$FEDIT->DbRecordSet[$i]['ID_RIFPROD_F'];
        $rifiuti[$indice]['FKEpesospecifico']=$FEDIT->DbRecordSet[$i]['FKEpesospecifico'];
        $rifiuti[$indice]['FKEumis']=$FEDIT->DbRecordSet[$i]['FKEumis'];
        $rifiuti[$indice]['ID_IMP']=$FEDIT->DbRecordSet[$i]['ID_IMP'];
        $rifiuti[$indice]['workmode']=$FEDIT->DbRecordSet[$i]['workmode'];
        if($FEDIT->DbRecordSet[$i]['TIPO']=="C"){
            $rifiuti[$indice]['carico'][0]['qta']=$FEDIT->DbRecordSet[$i]['quantita'];
            $rifiuti[$indice]['carico'][0]['FKEpesospecifico']=$FEDIT->DbRecordSet[$i]['FKEpesospecifico'];
            $rifiuti[$indice]['carico'][0]['FKEumis']=$FEDIT->DbRecordSet[$i]['FKEumis'];
            $rifiuti[$indice]['carico'][0]['num']=$FEDIT->DbRecordSet[$i]['NMOV'];
            $rifiuti[$indice]['carico'][0]['ID_MOV_F']=$FEDIT->DbRecordSet[$i]['ID_MOV_F'];
            $rifiuti[$indice]['carico'][0]['idSIS']=$FEDIT->DbRecordSet[$i]['idSIS'];
            $rifiuti[$indice]['carico'][0]['data']=$FEDIT->DbRecordSet[$i]['DTMOV'];
            $rifiuti[$indice]['carico'][0]['ID_IMP']=$FEDIT->DbRecordSet[$i]['ID_IMP'];
            $rifiuti[$indice]['carico'][0]['ID_UIMP']=$FEDIT->DbRecordSet[$i]['ID_UIMP'];
            $rifiuti[$indice]['carico'][0]['workmode']=$FEDIT->DbRecordSet[$i]['workmode'];
            $rifiuti[$indice]['totC']+=$FEDIT->DbRecordSet[$i]['quantita'];
            }
        if($FEDIT->DbRecordSet[$i]['TIPO']=="S")
            @$rifiuti[$indice]['totS']=$FEDIT->DbRecordSet[$i]['quantita'];
        }
    else{
		$rifiuti[$indice-1]['RifID']=$FEDIT->DbRecordSet[$i]['RifID'];
        # somme
        $countCarichi=(isset($rifiuti[$indice-1]['carico'])? count($rifiuti[$indice-1]['carico']) : 0);
        if($FEDIT->DbRecordSet[$i]['TIPO']=="C"){
            $rifiuti[$indice-1]['carico'][$countCarichi]['qta']=$FEDIT->DbRecordSet[$i]['quantita'];
            $rifiuti[$indice-1]['carico'][$countCarichi]['num']=$FEDIT->DbRecordSet[$i]['NMOV'];
            $rifiuti[$indice-1]['carico'][$countCarichi]['ID_MOV_F']=$FEDIT->DbRecordSet[$i]['ID_MOV_F'];
            $rifiuti[$indice-1]['carico'][$countCarichi]['idSIS']=$FEDIT->DbRecordSet[$i]['idSIS'];
            $rifiuti[$indice-1]['carico'][$countCarichi]['FKEpesospecifico']=$FEDIT->DbRecordSet[$i]['FKEpesospecifico'];
            $rifiuti[$indice-1]['carico'][$countCarichi]['FKEumis']=$FEDIT->DbRecordSet[$i]['FKEumis'];
            $rifiuti[$indice-1]['carico'][$countCarichi]['ID_IMP']=$FEDIT->DbRecordSet[$i]['ID_IMP'];
            $rifiuti[$indice-1]['carico'][$countCarichi]['ID_UIMP']=$FEDIT->DbRecordSet[$i]['ID_UIMP'];
            $rifiuti[$indice-1]['carico'][$countCarichi]['workmode']=$FEDIT->DbRecordSet[$i]['workmode'];
            $rifiuti[$indice-1]['carico'][$countCarichi]['data']=$FEDIT->DbRecordSet[$i]['DTMOV'];
            $rifiuti[$indice-1]['totC']+=$FEDIT->DbRecordSet[$i]['quantita'];
            }
        if($FEDIT->DbRecordSet[$i]['TIPO']=="S")
            @$rifiuti[$indice-1]['totS']+=$FEDIT->DbRecordSet[$i]['quantita'];
        }
    $UIMPbuf=$FEDIT->DbRecordSet[$i]['ID_UIMP'];
    $RIFbuf=$FEDIT->DbRecordSet[$i]['originalRifID'];
    }

//var_dump($rifiuti);die();
echo "<h1>Ho tutti i carichi dei rifiuti con disponibilita>0, procedo ad eliminare quelli scaricati - START: ".date("H:i:s")."</h1>";
$FEDIT->DbConn('soger2016');
for($r=0;$r<count($rifiuti);$r++){
    $carichi=RemoveCarichiScaricati($rifiuti[$r], $CarichiRifiuto);
    //var_dump($carichi);echo "<hr>";
    if($carichi){
		foreach($carichi AS $rif=>$carico){
			# calcola pesoN
			for($c=0;$c<count($carico);$c++){
				$carico[$c]['pesoN']=$carico[$c]['qta'];
				if($carico[$c]['FKEumis']=='Litri') $carico[$c]['pesoN']=$carico[$c]['qta']*$carico[$c]['FKEpesospecifico'];
				if($carico[$c]['FKEumis']=='Mc.') $carico[$c]['pesoN']=$carico[$c]['qta']*$carico[$c]['FKEpesospecifico']*1000;
				$sql ="INSERT INTO user_movimenti_giacenze_iniziali ";
				$sql.="(ID_RIF, ID_MOV_F, NMOV, DTMOV, idSIS, quantita, pesoN, TIPO, FKEpesospecifico, FKEumis, ID_IMP, workmode) ";
				$sql.="VALUES (".$rif.", ".$carico[$c]['ID_MOV_F'].", ".$carico[$c]['num'].", '".$carico[$c]['data']."', '".$carico[$c]['idSIS']."', ".$carico[$c]['qta'].", ".$carico[$c]['pesoN'].", 'C', ".$carico[$c]['FKEpesospecifico'].", '".$carico[$c]['FKEumis']."', '".$carico[$c]['ID_IMP']."', '".$carico[$c]['workmode']."');";
				echo $sql."<br />";
				echo "<hr>";
				$FEDIT->SDBWrite($sql,true,false);
				}
			}
		}
    }
$sql = "UPDATE user_movimenti_giacenze_iniziali SET idSIS=NULL WHERE idSIS='' OR idSIS=0;";
$FEDIT->SDBWrite($sql,true,false);

$YEAR=date('Y');
$sql = "UPDATE user_movimenti_giacenze_iniziali SET NMOV='Giac.Iniz.', DTMOV='".$YEAR."-01-01' WHERE NMOV=0;";
$FEDIT->SDBWrite($sql,true,false);

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>