<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/zip3.php");
require_once("../__libs/SQLFunct.php");
require_once("../__scripts/STATS_funct.php");
require_once("../__scripts/MUD_funct.php");
require_once("../__libs/SogerECO_funct.php");

global $SOGER;

$SQL="SELECT ID_RIF, sk_approved AS ProceduraDiSicurezza, sk_approved_start AS ProceduraDiSicurezza_start, CLASS_approved AS Classificazione, CLASS_approved_start AS Classificazione_start, CAR_Analisi_approved AS Analisi, CAR_Analisi_approved_start AS Analisi_start, CAR_approved AS Caratterizzazione, CAR_approved_start AS Caratterizzazione_start FROM user_schede_rifiuti;";
$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
$Rifiuti=$FEDIT->DbRecordSet;

$totale = count($Rifiuti);

echo "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">";
echo "<tr><th>ID RIF</th><th colspan=\"3\">Procedura di sicurezza</th><th colspan=\"3\">Classificazione</th><th colspan=\"3\">Analisi</th><th colspan=\"3\">Caratterizzazione</th><th>Note</th><tr>";

for($r=0;$r<count($Rifiuti);$r++){

	echo "<tr>";

	echo "<td>".$Rifiuti[$r]['ID_RIF']."</td>";

	# PROCEDURA DI SICUREZZA
	echo "<td>".$Rifiuti[$r]['ProceduraDiSicurezza']."</td>";
	echo "<td>".$Rifiuti[$r]['ProceduraDiSicurezza_start']."</td>";
	$sicurezza_doc = "../__upload/sicurezza/".$Rifiuti[$r]['ID_RIF'].".doc";
	$sicurezza_rtf = "../__upload/sicurezza/".$Rifiuti[$r]['ID_RIF'].".rtf";
	$sicurezza_pdf = "../__upload/sicurezza/".$Rifiuti[$r]['ID_RIF'].".pdf";
	if(!file_exists($sicurezza_doc) AND !file_exists($sicurezza_rtf) AND !file_exists($sicurezza_pdf) ){
		echo "<td>...missing...</td>";
		$missing = true;
		}
	else{
		echo "<td>OK</td>";
		$missing = false;
		}
	if($missing AND !is_null($Rifiuti[$r]['ProceduraDiSicurezza_start'])){
		echo "<td style=\"color:red;\">AHI AHI AHI</td>";
		$sql = "UPDATE user_schede_rifiuti SET sk_approved=0, sk_approved_start=NULL WHERE ID_RIF=".$Rifiuti[$r]['ID_RIF'].";"; 
		$FEDIT->SDBWrite($sql,true,false);
		}
	else echo "<td>OK</td>";

	# CLASSIFICAZIONE
	echo "<td>".$Rifiuti[$r]['Classificazione']."</td>";
	echo "<td>".$Rifiuti[$r]['Classificazione_start']."</td>";
	$classificazione_doc = "../__upload/classificazione/".$Rifiuti[$r]['ID_RIF'].".doc";
	$classificazione_rtf = "../__upload/classificazione/".$Rifiuti[$r]['ID_RIF'].".rtf";
	$classificazione_pdf = "../__upload/classificazione/".$Rifiuti[$r]['ID_RIF'].".pdf";
	if(!file_exists($classificazione_doc) AND !file_exists($classificazione_rtf) AND !file_exists($classificazione_pdf) ){
		echo "<td>...missing...</td>";
		$missing = true;
		}
	else{
		echo "<td>OK</td>";
		$missing = false;
		}
	if($missing AND !is_null($Rifiuti[$r]['Classificazione_start'])){
		echo "<td style=\"color:red;\">AHI AHI AHI</td>";
		$sql = "UPDATE user_schede_rifiuti SET CLASS_approved=0, CLASS_approved_start=NULL WHERE ID_RIF=".$Rifiuti[$r]['ID_RIF'].";"; 
		$FEDIT->SDBWrite($sql,true,false);
		}
	else echo "<td>OK</td>";

	# ANALISI
	echo "<td>".$Rifiuti[$r]['Analisi']."</td>";
	echo "<td>".$Rifiuti[$r]['Analisi_start']."</td>";
	$analisi_pdf = "../__upload/analisi/".$Rifiuti[$r]['ID_RIF'].".pdf";
	if(!file_exists($analisi_pdf)){
		echo "<td>...missing...</td>";
		$missing = true;
		}
	else{
		echo "<td>OK</td>";
		$missing = false;
		}
	if($missing AND !is_null($Rifiuti[$r]['Analisi_start'])){
		echo "<td style=\"color:red;\">AHI AHI AHI</td>";
		$sql = "UPDATE user_schede_rifiuti SET CAR_Analisi_approved=0, CAR_Analisi_approved_start=NULL WHERE ID_RIF=".$Rifiuti[$r]['ID_RIF'].";"; 
		$FEDIT->SDBWrite($sql,true,false);
		}
	else echo "<td>OK</td>";

	# CARATTERIZZAZIONE
	echo "<td>".$Rifiuti[$r]['Caratterizzazione']."</td>";
	echo "<td>".$Rifiuti[$r]['Caratterizzazione_start']."</td>";
	$caratterizzazione_doc = "../__upload/caratterizzazioni/".$Rifiuti[$r]['ID_RIF'].".doc";
	$caratterizzazione_rtf = "../__upload/caratterizzazioni/".$Rifiuti[$r]['ID_RIF'].".rtf";
	$caratterizzazione_pdf = "../__upload/caratterizzazioni/".$Rifiuti[$r]['ID_RIF'].".pdf";
	if(!file_exists($caratterizzazione_doc) AND !file_exists($caratterizzazione_rtf) AND !file_exists($caratterizzazione_pdf) ){
		echo "<td>...missing...</td>";
		$missing = true;
		}
	else{
		echo "<td>OK</td>";
		$missing = false;
		}
	if($missing AND !is_null($Rifiuti[$r]['Caratterizzazione_start'])){
		echo "<td style=\"color:red;\">AHI AHI AHI</td>";
		$sql = "UPDATE user_schede_rifiuti SET CAR_approved=0, CAR_approved_start=NULL WHERE ID_RIF=".$Rifiuti[$r]['ID_RIF'].";"; 
		$FEDIT->SDBWrite($sql,true,false);
		}
	else echo "<td>OK</td>";

	echo "</tr>\n";

	
	}

echo "</table>";
	
require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>