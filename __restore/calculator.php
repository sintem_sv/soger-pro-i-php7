<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/zip3.php");
require_once("../__libs/SQLFunct.php");
require_once("../__scripts/STATS_funct.php");
require_once("../__scripts/MUD_funct.php");
require_once("../__libs/SogerECO_funct.php");

global $SOGER;

$MOVIMENTI_AL		= date("Y-m-d");
$ANNO				= $_GET['anno'];
$IMP_CON_DATA_AVVIO = array();
$IMP_AND_DATE		= array();
$CSVbuf				= '';
$DcName				= "Dati movimentazione ".$ANNO;


function var_dump_pre($mixed = null) {
	echo '<pre>';
	var_dump($mixed);
	echo '</pre>';
	return null;
	}

$SQL = "SELECT DISTINCT soger".$ANNO.".user_movimenti_fiscalizzati.ID_IMP FROM soger".$ANNO.".user_movimenti_fiscalizzati WHERE soger".$ANNO.".user_movimenti_fiscalizzati.ID_IMP IS NOT NULL AND soger".$ANNO.".user_movimenti_fiscalizzati.ID_IMP<>'';";
$FEDIT->SdbRead($SQL,"DbRecordSetIMP",true,false);

$IMP = $FEDIT->DbRecordSetIMP;
for($i=0;$i<count($IMP);$i++){
	$IMP2[] = $IMP[$i]['ID_IMP'];
	}
$IMP = $IMP2;

$IMPIANTI_ATTIVI = implode('", "', $IMP);
$IMPIANTI_ATTIVI_WHERE = '("' . substr($IMPIANTI_ATTIVI , 3, strlen($IMPIANTI_ATTIVI) ) . '")';

$SQL="SELECT soger".$ANNO.".lov_cer.COD_CER, soger".$ANNO.".user_movimenti_fiscalizzati.ID_IMP, soger".$ANNO.".lov_comuni_istat.des_reg AS REGIONE, DTFORM, REPLACE( ROUND( SUM( soger".$ANNO.".user_movimenti_fiscalizzati.pesoN ), 2 ), '.', ',') AS KG_STIMATI, REPLACE( ROUND( SUM( soger".$ANNO.".user_movimenti_fiscalizzati.PS_DESTINO ), 2 ), '.', ',')  AS KG_VERIFICATI, IF(VER_DESTINO=1, 's�', 'no') AS VERIFICA_RICHIESTA, YEAR(soger".$ANNO.".user_movimenti_fiscalizzati.DTFORM) AS ANNO, DATE_ADD( soger".$ANNO.".user_movimenti_fiscalizzati.DTFORM, INTERVAL( 1 - DAYOFWEEK( soger".$ANNO.".user_movimenti_fiscalizzati.DTFORM ) ) +1 DAY ) AS SETTIMANA_DAL, DATE_ADD( soger".$ANNO.".user_movimenti_fiscalizzati.DTFORM, INTERVAL( 7 - DAYOFWEEK( soger".$ANNO.".user_movimenti_fiscalizzati.DTFORM ) ) +1 DAY ) AS SETTIMANA_AL FROM soger".$ANNO.".user_movimenti_fiscalizzati JOIN soger".$ANNO.".user_schede_rifiuti ON soger".$ANNO.".user_schede_rifiuti.ID_RIF = soger".$ANNO.".user_movimenti_fiscalizzati.ID_RIF JOIN soger".$ANNO.".lov_cer ON soger".$ANNO.".lov_cer.ID_CER = soger".$ANNO.".user_schede_rifiuti.ID_CER JOIN soger".$ANNO.".core_impianti ON soger".$ANNO.".core_impianti.ID_IMP = soger".$ANNO.".user_movimenti_fiscalizzati.ID_IMP JOIN soger".$ANNO.".lov_comuni_istat ON soger".$ANNO.".lov_comuni_istat.ID_COM = soger".$ANNO.".core_impianti.ID_COM WHERE DTFORM<='".$ANNO."-12-31' AND DTFORM>='".$ANNO."-01-01' AND soger".$ANNO.".lov_cer.COD_CER IN ('120101', '120102', '150101', '150102', '150106') AND soger".$ANNO.".user_movimenti_fiscalizzati.TIPO = 'S' AND soger".$ANNO.".user_movimenti_fiscalizzati.Transfrontaliero =0 AND soger".$ANNO.".user_movimenti_fiscalizzati.produttore =1 AND soger".$ANNO.".user_movimenti_fiscalizzati.ID_AZT <>0 AND soger".$ANNO.".user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND WEEK( soger".$ANNO.".user_movimenti_fiscalizzati.DTFORM ) IS NOT NULL AND YEAR(soger".$ANNO.".user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND soger".$ANNO.".user_movimenti_fiscalizzati.ID_IMP IN ".$IMPIANTI_ATTIVI_WHERE." GROUP BY soger".$ANNO.".user_movimenti_fiscalizzati.ID_IMP, soger".$ANNO.".lov_cer.COD_CER, CONCAT( ANNO, SETTIMANA_DAL ) ORDER BY ANNO, SETTIMANA_DAL, COD_CER, ID_IMP;";

$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);

for($r=0;$r<count($FEDIT->DbRecordSet);$r++){

	$YEAR_FOR_MINDATE = 2007;
	
	if(!in_array($FEDIT->DbRecordSet[$r]['ID_IMP'], $IMP_CON_DATA_AVVIO)){

		// ricavo data prima movimentazione
		$SQL = "SELECT MIN(MINDATE) AS MINDATE FROM ( ";

		$SQL2008 = "SELECT MIN(soger2008.user_movimenti_fiscalizzati.DTFORM) AS MINDATE FROM soger2008.user_movimenti_fiscalizzati WHERE soger2008.user_movimenti_fiscalizzati.ID_IMP='".$FEDIT->DbRecordSet[$r]['ID_IMP']."' AND soger2008.user_movimenti_fiscalizzati.TIPO='S' AND soger2008.user_movimenti_fiscalizzati.produttore=1 AND soger2008.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND soger2008.user_movimenti_fiscalizzati.ID_AZT<>0 AND soger2008.user_movimenti_fiscalizzati.Transfrontaliero=0  AND WEEK(soger2008.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2008.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2008.user_movimenti_fiscalizzati.DTFORM)>='".$YEAR_FOR_MINDATE."' UNION DISTINCT ";

		$SQL2009 = "SELECT MIN(soger2009.user_movimenti_fiscalizzati.DTFORM) AS MINDATE FROM soger2009.user_movimenti_fiscalizzati WHERE soger2009.user_movimenti_fiscalizzati.ID_IMP='".$FEDIT->DbRecordSet[$r]['ID_IMP']."' AND soger2009.user_movimenti_fiscalizzati.TIPO='S' AND soger2009.user_movimenti_fiscalizzati.produttore=1 AND soger2009.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND soger2009.user_movimenti_fiscalizzati.ID_AZT<>0 AND soger2009.user_movimenti_fiscalizzati.Transfrontaliero=0  AND WEEK(soger2009.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2009.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2009.user_movimenti_fiscalizzati.DTFORM)>='".$YEAR_FOR_MINDATE."' UNION DISTINCT ";

		$SQL2010 = "SELECT MIN(soger2010.user_movimenti_fiscalizzati.DTFORM) AS MINDATE FROM soger2010.user_movimenti_fiscalizzati WHERE soger2010.user_movimenti_fiscalizzati.ID_IMP='".$FEDIT->DbRecordSet[$r]['ID_IMP']."' AND soger2010.user_movimenti_fiscalizzati.TIPO='S' AND soger2010.user_movimenti_fiscalizzati.produttore=1 AND soger2010.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND soger2010.user_movimenti_fiscalizzati.ID_AZT<>0 AND soger2010.user_movimenti_fiscalizzati.Transfrontaliero=0  AND WEEK(soger2010.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2010.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2010.user_movimenti_fiscalizzati.DTFORM)>='".$YEAR_FOR_MINDATE."' UNION DISTINCT ";

		$SQL2011 = "SELECT MIN(soger2011.user_movimenti_fiscalizzati.DTFORM) AS MINDATE FROM soger2011.user_movimenti_fiscalizzati WHERE soger2011.user_movimenti_fiscalizzati.ID_IMP='".$FEDIT->DbRecordSet[$r]['ID_IMP']."' AND soger2011.user_movimenti_fiscalizzati.TIPO='S' AND soger2011.user_movimenti_fiscalizzati.produttore=1 AND soger2011.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND soger2011.user_movimenti_fiscalizzati.ID_AZT<>0 AND soger2011.user_movimenti_fiscalizzati.Transfrontaliero=0  AND WEEK(soger2011.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2011.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2011.user_movimenti_fiscalizzati.DTFORM)>='".$YEAR_FOR_MINDATE."' UNION DISTINCT ";

		$SQL2012 = "SELECT MIN(soger2012.user_movimenti_fiscalizzati.DTFORM) AS MINDATE FROM soger2012.user_movimenti_fiscalizzati WHERE soger2012.user_movimenti_fiscalizzati.ID_IMP='".$FEDIT->DbRecordSet[$r]['ID_IMP']."' AND soger2012.user_movimenti_fiscalizzati.TIPO='S' AND soger2012.user_movimenti_fiscalizzati.produttore=1 AND soger2012.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND soger2012.user_movimenti_fiscalizzati.ID_AZT<>0 AND soger2012.user_movimenti_fiscalizzati.Transfrontaliero=0  AND WEEK(soger2012.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2012.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2012.user_movimenti_fiscalizzati.DTFORM)>='".$YEAR_FOR_MINDATE."' UNION DISTINCT ";

		$SQL2013 = "SELECT MIN(soger2013.user_movimenti_fiscalizzati.DTFORM) AS MINDATE FROM soger2013.user_movimenti_fiscalizzati WHERE soger2013.user_movimenti_fiscalizzati.ID_IMP='".$FEDIT->DbRecordSet[$r]['ID_IMP']."' AND soger2013.user_movimenti_fiscalizzati.TIPO='S' AND soger2013.user_movimenti_fiscalizzati.produttore=1 AND soger2013.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND soger2013.user_movimenti_fiscalizzati.ID_AZT<>0 AND soger2013.user_movimenti_fiscalizzati.Transfrontaliero=0  AND WEEK(soger2013.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2013.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2013.user_movimenti_fiscalizzati.DTFORM)>='".$YEAR_FOR_MINDATE."' UNION DISTINCT ";

		$SQL2014 = "SELECT MIN(soger2014.user_movimenti_fiscalizzati.DTFORM) AS MINDATE FROM soger2014.user_movimenti_fiscalizzati WHERE soger2014.user_movimenti_fiscalizzati.ID_IMP='".$FEDIT->DbRecordSet[$r]['ID_IMP']."' AND soger2014.user_movimenti_fiscalizzati.TIPO='S' AND soger2014.user_movimenti_fiscalizzati.produttore=1 AND soger2014.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND soger2014.user_movimenti_fiscalizzati.ID_AZT<>0 AND soger2014.user_movimenti_fiscalizzati.Transfrontaliero=0  AND WEEK(soger2014.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2014.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2014.user_movimenti_fiscalizzati.DTFORM)>='".$YEAR_FOR_MINDATE."' UNION DISTINCT ";

		$SQL2015 = "SELECT MIN(soger2015.user_movimenti_fiscalizzati.DTFORM) AS MINDATE FROM soger2015.user_movimenti_fiscalizzati WHERE soger2015.user_movimenti_fiscalizzati.ID_IMP='".$FEDIT->DbRecordSet[$r]['ID_IMP']."' AND soger2015.user_movimenti_fiscalizzati.TIPO='S' AND soger2015.user_movimenti_fiscalizzati.produttore=1 AND soger2015.user_movimenti_fiscalizzati.ID_AZT IS NOT NULL AND soger2015.user_movimenti_fiscalizzati.ID_AZT<>0 AND soger2015.user_movimenti_fiscalizzati.Transfrontaliero=0  AND WEEK(soger2015.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2015.user_movimenti_fiscalizzati.DTFORM) IS NOT NULL AND YEAR(soger2015.user_movimenti_fiscalizzati.DTFORM)>='".$YEAR_FOR_MINDATE."' UNION DISTINCT ";

		switch($YEAR_FOR_MINDATE){
			case 2007:
				$SQL.=$SQL2008.$SQL2009.$SQL2010.$SQL2011.$SQL2012.$SQL2013.$SQL2014.$SQL2015;
				break;
			case 2008:
				$SQL.=$SQL2009.$SQL2010.$SQL2011.$SQL2012.$SQL2013.$SQL2014.$SQL2015;
				break;
			case 2009:
				$SQL.=$SQL2010.$SQL2011.$SQL2012.$SQL2013.$SQL2014.$SQL2015;
				break;
			case 2010:
				$SQL.=$SQL2011.$SQL2012.$SQL2013.$SQL2014.$SQL2015;
				break;
			case 2011:
				$SQL.=$SQL2012.$SQL2013.$SQL2014.$SQL2015;
				break;
			case 2012:
				$SQL.=$SQL2013.$SQL2014.$SQL2015;
				break;
			case 2013:
				$SQL.=$SQL2014.$SQL2015;
				break;
			case 2014:
				$SQL.=$SQL2015;
				break;
			}


		// remove last UNION DISTINCT
		$SQL = substr($SQL, 0, strlen($SQL)-15);

		$SQL.=" ) AS t";

		$FEDIT->SdbRead($SQL,"DbRecordSetMINDATE",true,false);
		$MINDATE = $FEDIT->DbRecordSetMINDATE[0]['MINDATE'];
		$IMP_AND_DATE[$FEDIT->DbRecordSet[$r]['ID_IMP']] = $MINDATE;
		array_push($IMP_CON_DATA_AVVIO, $FEDIT->DbRecordSet[$r]['ID_IMP']);
		}

	$ddate = $FEDIT->DbRecordSet[$r]['SETTIMANA_DAL'];
	$date  = new DateTime($ddate);
	$week  = $date->format("W");

/*
	echo "<tr>";
	echo "<td>".$FEDIT->DbRecordSet[$r]['COD_CER']."</td>";
	echo "<td>".$FEDIT->DbRecordSet[$r]['ID_IMP']."</td>";
	echo "<td>".$FEDIT->DbRecordSet[$r]['REGIONE']."</td>";
	echo "<td>".$FEDIT->DbRecordSet[$r]['DTFORM']."</td>";
	echo "<td>".$FEDIT->DbRecordSet[$r]['KG_STIMATI']."</td>";
	echo "<td>".$FEDIT->DbRecordSet[$r]['KG_VERIFICATI']."</td>";
	echo "<td>".$FEDIT->DbRecordSet[$r]['VERIFICA_RICHIESTA']."</td>";
	echo "<td>".$FEDIT->DbRecordSet[$r]['ANNO']."</td>";
	echo "<td>".$week."</td>";
	echo "<td>".$FEDIT->DbRecordSet[$r]['SETTIMANA_DAL']."</td>";
	echo "<td>".$FEDIT->DbRecordSet[$r]['SETTIMANA_AL']."</td>";
	echo "<td>".$IMP_AND_DATE[$FEDIT->DbRecordSet[$r]['ID_IMP']]."</td>";
	echo "</tr>";
*/

	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$r]['COD_CER']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$r]['ID_IMP']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$r]['REGIONE']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$r]['DTFORM']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$r]['KG_STIMATI']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$r]['KG_VERIFICATI']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$r]['VERIFICA_RICHIESTA']);
	$CSVbuf .= AddCSVcolumn($week);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$r]['SETTIMANA_DAL']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$r]['SETTIMANA_AL']);
	$CSVbuf .= AddCSVcolumn($IMP_AND_DATE[$FEDIT->DbRecordSet[$r]['ID_IMP']]);
	$CSVbuf .= AddCSVRow();

	}

//echo "</table>";

CSVOut($CSVbuf,$DcName);
	
require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>