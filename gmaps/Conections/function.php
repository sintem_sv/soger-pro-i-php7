<?php
function ThumbNail($rutaFoto, $thumbFoto, $nameFoto, $max_alto = 114, $max_ancho = 151, $overwrite = true){
	$checkedFoto = file_exists($rutaFoto);
	if ( $checkedFoto === true ){
		$sizeFoto = getimagesize( $rutaFoto );
		$anchoFoto = $sizeFoto[0];
		$altoFoto = $sizeFoto[1];
		
		if ($overwrite == false){
			$checkedThumb = file_exists ($thumbFoto);
			
			if ( $checkedThumb === true ){
				return true;
			}
		}
			
		$ExtImg = strrchr ($rutaFoto, ".");
	
		if ( $ExtImg == ".gif" ){
			$imageActual = imagecreatefromgif ($rutaFoto);
			
		}elseif ( $ExtImg == ".jpg" || $ExtImg == ".jpeg" ){
			$imageActual = imagecreatefromjpeg ($rutaFoto);
		}elseif ( $ExtImg == ".png" ){
			$imageActual = imagecreatefrompng ($rutaFoto);
		}
		touch( $thumbFoto );
		chmod( $thumbFoto, 0777 );
		
		$razonFoto = $anchoFoto / $altoFoto;

		if ($anchoFoto > $altoFoto){
			$altoFinal = $max_alto;
			$anchoFinal = ceil($max_alto*$razonFoto);
			$destajeX = ($anchoFinal - $max_ancho)/-2;
			$destajeY = 0;
		}else{
			$anchoFinal = $max_ancho;
			$altoFinal = ceil($max_ancho/$razonFoto);
			$destajeX = 0;
			$destajeY = ($altoFinal - $max_alto)/-2;
		}
			
		$ImagenFinal = imagecreatetruecolor ($max_ancho, $max_alto);
		imagecopyresized ($ImagenFinal, $imageActual, $destajeX, $destajeY, 0, 0, $anchoFinal, $altoFinal, $anchoFoto, $altoFoto);
		$controlImage = imagejpeg ( $ImagenFinal, $thumbFoto, 100);
			
		return ($controlImage);
	}else{
		return false;
	}
}
?>