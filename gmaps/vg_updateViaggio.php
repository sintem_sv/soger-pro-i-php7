<?php
$year           = $_POST['year'];
$matriz         = $_POST['matriz'];
$idmov          = $_POST['idmov'];
$tipoMov        = $_POST['tipoMov'];
$mz             = $_POST['trasporto'];
$azione         = $_POST['azione'];
$idv            = $_POST['idv'];
$resumeRoute    = $_POST['resumeRoute'];
$ttime          = $_POST['time'];
$tdistance      = $_POST['distance'];


require_once("vg_viaggio.php");
if($azione=="updateByRoute"){   
              $create = new Viaggio($year,$matriz,$idmov,$tipoMov,$mz);
              $create->setIDVIAGGIOinMovimento($idv);
              $create->setViaggioByID($idv);
              $create->durata = $ttime;
              $create->distanza = $tdistance;
              $create->costo_calcolato = $create->costo_totale * $tdistance / 1000;
              $create->deleteViaggiTappaDetailsByIDV($idv);
              $create->saveViaggio();
              $create->updateTappaDetailsByResumeRoute($idv,$resumeRoute);
}

?>
