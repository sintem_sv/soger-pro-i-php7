<?php
$year = date('Y');
require_once("../connessione/conectarse_db.php");
$conectarse=new DbConnector($year);
?>

<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type='text/javascript' src='../js/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='../js/jquery-ui-1.8.7.custom.min.js'></script>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDPLZQB2D2m6Kq5fcSZBlWtKZMOvBCD10U&sensor=false'></script>

<script type="text/javascript">

var directionsService = new google.maps.DirectionsService();

        $(document).ready(function(){

                        <?php
                            if(isset ($_GET['start'])){
                                echo "getDistance();";
                            }
                        ?>

           $("#retorna").click(function(){

                    getDistance();

                        <?php
                            if(isset ($_GET['start'])){
                                $start = $_GET['start'];
                                echo 'url = "http://'.$_SERVER['SERVER_NAME'].'/soger/gmaps/setKilometri/useApi/visualizaMovimenti.php?start='.$start.'"';
                                }else{
                                    $start = 0;
                                    echo 'url = "http://'.$_SERVER['SERVER_NAME'].'/soger/gmaps/setKilometri/useApi/visualizaMovimenti.php?start='.$start.'"';
                                }

                        ?>

                    }) // END CLICK retorna

        })


         function dump(arr,level){
                                var dumped_text = "";
                                if(!level) level = 0;
                                //The padding given at the beginning of the line.
                                var level_padding = "";
                                for(var j=0;j<level+1;j++) level_padding += "    ";

                                if(typeof(arr) == 'object') { //Array/Hashes/Objects
                                        for(var item in arr) {
                                                var value = arr[item];

                                                if(typeof(value) == 'object') { //If it is an array,
                                                        dumped_text += level_padding + "'" + item + "' ...\n";
                                                        dumped_text += dump(value,level+1);
                                                } else {
                                                        dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                                                }
                                        }
                                } else { //Stings/Chars/Numbers etc.
                                        dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
                                }
                                return dumped_text;
         }



        var count=0;
        var total=0;

         function trovaKm(address){

                            var index = address['index'];
                            var request = {
                            origin: address['da'],
                            destination: address['a'],
                            travelMode: google.maps.DirectionsTravelMode.DRIVING
                            };

                                directionsService.route(request, function(response, status) {

                                            
                                            if (status == google.maps.DirectionsStatus.OK) {
                                                    count++;
                                                    var route = response.routes[0];
                                                    var distance =   route.legs[0].distance.value;
                                                    
                                                    km_td = "#distance"+index;
                                                    $(km_td).text(distance);
                                                    if(count==total){
                                                            updateTable();
                                                            }
                                                }else{
                                                    count++;
                                                    km_td     = "#distance"+index;
                                                    $(km_td).text("0");
                                                    if(count==total){
                                                            updateTable();
                                                            }
                                                    }
                            });



         }


        function getDistance(){
                    var address=[];
                    lastTr=$("#tbIndirizzi tr:last").find("td").eq(0).text();
                    $("#tbIndirizzi .mov").each(function (index, domEle) {

                          da = $(this).find("#from"+index).text();
                          a = $(this).find("#to"+index).text();

                          var ruta = {"da": da, "a": a, "index": index}
                          address.push(ruta);
                      }); // END EACH

                      // SET GLOBAL VAR;

                      total=address.length;
                      for(i=0;i<address.length;i++){
                                addr=address[i];
                                trovaKm(addr);
                                }
        }


        function refrescaPage(){


            lastMov = $("#tbIndirizzi tr:last").find("td").eq(1).text();
            url = "http://"+<?= $_SERVER['SERVER_NAME']?>+"/soger/gmaps/setKilometri/useApi/visualizaMovimenti.php?start="+lastMov;
            location.href=url;
            }



        var ajaxcount=0


        function updateTable(){
                array_mov = [];
                $("#tbIndirizzi .mov").each(function (index, domEle) {
                                  movf = $(this).find("#idMov"+index).text();
                                  km  = $(this).find("#distance"+index).text();
                                  if(km!=""){
                                              $.ajax({
                                                        type: "POST",
                                                        async: false,
                                                        url: "updateTable.php",
                                                        data: {mov:movf,km:km},
                                                        dataType: 'json',
                                                        success: function(dati){

                                                            ajaxcount = ajaxcount+1;
                                                            if(total==ajaxcount){refrescaPage();}
                                                            }
                                                        });
                                   } // END IF
                                          
                 }); // END EACH

                                    

          }



</script>


</head>
    <body>
        <div id="retorna"><button>Aggiorna KM</button></div>
    </body>
</html>

<?php
//$sql="SELECT ID_MOV_F, ID_UIMP, ID_UIMD, CONCAT( ID_UIMP, ID_UIMD )
//FROM  `".$tabMov."`
//WHERE ID_UIMP <>0000000
//AND ID_UIMD <>0000000
//AND ID_UIMT <>0000000
//AND produttore =1
//GROUP BY (
//CONCAT( ID_UIMP, ID_UIMD )
//)";

//select *from user_movimenti_fiscalizzati
//        where TIPO='S' AND
//        produttore=1
//        AND trasportatore=0
//        AND intermediario=0
//        AND destinatario=0
//        AND ID_UIMP<>0
//        AND ID_UIMT<>0
//        AND ID_UIMD<>0
//        AND km=0



$sql="SELECT ID_MOV_F, ID_UIMP, ID_UIMD
FROM  `user_movimenti_fiscalizzati`
WHERE 
TIPO='S' 
AND produttore=1
AND trasportatore=0
AND intermediario=0
AND destinatario=0
AND ID_UIMP <>0000000
AND ID_UIMD <>0000000
AND ID_UIMT <>0000000
AND km=0
AND produttore =1
AND ID_MOV_F >".$start." limit 0,30";

$sql=$conectarse->query($sql);
$n = 0;



echo "<table id='tbIndirizzi'>
        <tr style='background-color:#A5BFDD;color:#565656;' align='center'>
            <td>N°</td>
            <td>"."ID_MOV_F"."</td>
            <td>FROM</td>
            <td>GLOOGLE - FROM</td>
            <td>TO</td>
            <td>GLOOGLE - TO</td>
            <td>KM</td>
            <td>Status</td>

         </tr>";
while($result=$conectarse->fetchArray($sql)){

($n%2==0)?$style="#ffffff":$style="#F9F290";
        $UIMD =  $result['ID_UIMD'];
        $UIMP =  $result['ID_UIMP'];
        $ID_MOV = $result['ID_MOV_F'];
        for($i=0;$i<2;$i++){

            if($i==0){
                $ruolo="produttori";
                $col="UIMP";
                $imp=$UIMP;
                $class="from";
                echo "<tr class='mov' style='background-color:".$style.";color:#565656;'><td class='numMov'>".$n."</td><td id='idMov".$n."'>".$ID_MOV."</td>";
                }else{
                    $ruolo="destinatari";
                    $col="UIMD";
                    $imp=$UIMD;
                    $class="to";
                    }



            $sqlIn = "SELECT c.description as comune,c.shdes_prov as provincia,c.nazione,i.description
            as indirizzo FROM lov_comuni_istat c
            JOIN user_impianti_".$ruolo." i ON c.ID_COM = i.ID_COM
            WHERE i.ID_".$col."=".$imp;
            $sqlIn=$conectarse->query($sqlIn);
            $sqlIn=$conectarse->fetchArray($sqlIn);

            $address  =  $sqlIn["indirizzo"].",";
            $address .=  $sqlIn["comune"].",";
            $address .=  $sqlIn["provincia"].",";
            $address .=  $sqlIn["nazione"];

            echo "</td>&nbsp;<td><span class='".$class."' id='$class$n'>".$address."</span></td>";
            echo "<td><span style='color:#D84116' class='".$class."g' id='".$class.$n."g'></span></td>";
            echo ($i==1)?"<td id='distance".$n."' class='setdistance'></td><td id='status".$n."'>StandBy</td></tr>":"";
        }
        $n=$n+1;        
}
        echo "</table><span>total movimenti = </span><span id='totalMov'>".$n."</span>";
   
?>