<?php
$year           = $_POST['year'];
$matriz         = $_POST['matriz'];
$idmov          = $_POST['idmov'];
$tipoMov        = $_POST['tipoMov'];

require_once("Conections/conectarse_db.php");

class Movimenti extends DbConnector
{
    var $ID_UIMP;
    var $indirizzoP;
    var $com_prod;
    var $regione_prod;
    var $nazione_prod;    
    var $plat;
    var $plog;
    var $pdefault;
    var $pgloogle;
    var $tappe;
    var $ID_UIMD;
    var $indirizzoD;
    var $com_dest;
    var $regione_dest;
    var $nazione_dest;
    var $dlat;
    var $dlog;        
    var $ddefault;
    var $dgloogle;
    var $tabellaMov;
    var $tipoMov;
    var $mov;
    var $CAP_dest;
    var $CAP_prod;
    var $ID_IMP;
    

    function __construct($year,$matriz,$idmov,$tipoMov){
                DbConnector::__construct($year,$matriz);
                $this->setDati($idmov,$tipoMov);
                }
                
    function setDati($mov,$tipoMov){

                $this->mov = $mov;
                if ( $tipoMov == "ID_MOV_F" ) {
                     $this->tabellaMov  = "user_movimenti_fiscalizzati";
                     $this->tipoMov     = "ID_MOV_F";

                     } else {
                            $this->tabellaMov  = "user_movimenti";
                            $this->tipoMov     = "ID_MOV";
                            }


                $sql = "SELECT mv.ID_IMP AS ID_IMP, mv.percorso AS percorso, cp.description AS com_prod, cp.CAP AS CAP_prod, cd.CAP AS CAP_dest, cp.des_reg AS regione_prod, cp.nazione AS nazione_prod, cd.des_reg AS regione_dest, cd.nazione AS nazione_dest, cd.description AS com_dest, p.description AS indirizzoP, p.ID_UIMP, p.latitude AS plat, p.longitude AS plog, d.latitude AS dlat, d.longitude AS dlog, d.ID_UIMD, d.description AS indirizzoD
                FROM user_impianti_produttori p, user_impianti_destinatari d, ".$this->tabellaMov." mv, lov_comuni_istat cp, lov_comuni_istat cd
                WHERE cd.ID_COM = d.ID_COM
                AND cp.ID_COM = p.ID_COM
                AND p.ID_UIMP = mv.ID_UIMP
                AND mv.ID_UIMD = d.ID_UIMD
                AND mv.".$this->tipoMov." = ".$this->mov;
                $sql   = DbConnector::query($sql);
                $row   = DbConnector::fetchArray($sql);

                $this->ID_IMP 	= $row['ID_IMP'];

                $this->com_prod 	= $row['com_prod'];
                $this->CAP_prod 	= $row['CAP_prod'];
                $this->regione_prod 	= $row['regione_prod'];
                $this->nazione_prod 	= $row['nazione_prod'];
                $this->indirizzoP 	= $row['indirizzoP'];
                $this->ID_UIMP          = $row['ID_UIMP'];
                $this->plat             = $row['plat'];
                $this->plog             = $row['plog'];

                $this->regione_dest 	= $row['regione_dest'];
                $this->nazione_dest 	= $row['nazione_dest'];
                $this->com_dest 	= $row['com_dest'];
                $this->CAP_dest 	= $row['CAP_dest'];
                $this->dlat             = $row['dlat'];
                $this->dlog             = $row['dlog'];
                $this->ID_UIMD          = $row['ID_UIMD'];
                $this->indirizzoD       = $row['indirizzoD'];
                $this->tappe            = $this->setTappeByPercorso($row['percorso']);

    }


    function setTappeByPercorso($percorso){

                $tappe = array();

                if($percorso!=""){
                        if($percorso!="VIA DIRETTA"){
                               $percorso = explode("-", $percorso);
                                foreach ($percorso as $key => $value) {
                                    if ($value!=""){
                                        $tappe[]=$value;
                                        }
                                    }
                                }
                         }
                return $tappe;
    }





    function getTappe(){

        return $this->tappe;  // return array percorso
    }

    function getProduttore(){ // return  array (status,result,default,coordinate(array(latitude,longitude)))

        $response = array();
        $coo    =   $this->plat.",".$this->plog;
        $this->pdefault = $this->indirizzoP.", ".$this->com_prod.", ".$this->nazione_prod;
        $p      =   file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?latlng=".$coo."&sensor=true");
        $p      =   json_decode($p);

        if($p->status == "OK"){
            $this->pgloogle =  $p->results[0]->formatted_address;
            $response['status'] = "OK";
            }else{
                    $recursivo =  $this->findRecursiveAddress($this->indirizzoP,$this->com_prod,$this->regione_prod,$this->nazione_prod);
                    if ( $recursivo['status'] == "OK" ){
                                $this->pgloogle = $recursivo['results']['address'];
                                $response['status'] = "OK";
                                $this->setCoordinateImpianto("produttori","ID_UIMP",$this->ID_UIMP,$recursivo['results']['coordinate']);
                                } else {
                                        $this->pgloogle = "non trovato";
                                        $response['status'] = "ERROR";
                                        }
     
                    }
        $response['result'] = $this->pgloogle;
        $response['coordinate']    =   array('latitude'=>$this->plat,'longitude'=>$this->plog);
        $response['default'] = $this->pdefault;
        return $response;
    }
    
    function getDestinatario(){    
                    $response = array();
                    
                    $coo    =   $this->dlat.",".$this->dlog;
                    $this->ddefault = $this->indirizzoD.", ".$this->com_dest.", ".$this->nazione_dest;
                    $d      =   file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?latlng=".$coo."&sensor=true&language=it");
                    $d      =   json_decode($d);

                    if($d->status == "OK"){
                        $this->dgloogle =  $d->results[0]->formatted_address;
                        $response['status'] = "OK";
                        }else{
                                $recursivo =  $this->findRecursiveAddress($this->indirizzoD,$this->com_dest,$this->regione_dest,$this->nazione_dest);
                                if ( $recursivo['status'] == "OK" ){
                                            $this->dgloogle = $recursivo['results']['address'];
                                            $response['status'] = "OK";
                                             $this->setCoordinateImpianto("destinatari","ID_UIMD",$this->ID_UIMD,$recursivo['results']['coordinate']);
                                            } else {
                                                    $this->dgloogle = "non trovato";
                                                    $response['status'] = "ERROR";
                                                    }
                               }
                    $response['result'] = $this->dgloogle;
                    $response['coordinate']    =   array('latitude'=>$this->dlat,'longitude'=>$this->dlog);
                    $response['default'] = $this->ddefault;
                    return $response;
    }

    function setCoordinateImpianto($tipoimpianto,$tipoid,$id,$coordinate){
            $sql = "UPDATE user_impianti_".$tipoimpianto." SET latitude = ".$coordinate['latitude'].", longitude = ".$coordinate['longitude']." WHERE ".$tipoid." =".$id;
            $sql   = DbConnector::query($sql);
            return $sql;
    }

   
    function getTappeDetailsByApi(){ // USARE PER TROVARE LE TAPPE CON LE API
            $response = array();
            $tappe = $this->tappe;
            for($i=0; $i<count($tappe); $i++){
                        $t = $this->getDataApiByAddress($tappe[$i]);
                        array_push($response,$t);
                        }

            return $response;
    }

    function getDataApiByAddress($adr){

                $dati = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($adr)."&sensor=true&language=it");
                $dati = json_decode($dati);
                
                $response = Array();

                if($dati->status=="OK"){                        
                          $log                       =   $dati->results[0]->geometry->location->lng;
                          $lat                       =   $dati->results[0]->geometry->location->lat;
                          $response['status']        =   "OK";
                          $response['results']['address']       =   $dati->results[0]->formatted_address;
                          $response['results']['coordinate']    =   array('latitude'=>$lat,'longitude'=>$log);
                          $response['default']   =  $adr;
                          }else{
                                   $response['results']  =   NULL;
                                   $response['status']   =   "fail";
                                   $response['default']   =  $adr;
                                   }

                return $response;
    }

    function findRecursiveAddress($ind,$comume,$prov,$nazione){
               
                $adr = $ind.", ".$comume.", ".$prov.", ".$nazione;
                $result = $this->getDataApiByAddress($adr);

                $status = $result['status'];
                if ( $status=="OK" ){
                    return $result;
                    }else{                        
                            $baseComune = $comume.", ".$prov.", ".$nazione;
                            $baseComune = getDataApiByAddress($baseComune);
                            $statusComune = $baseComune['status'];
                            if ( $statusComune=="OK" ){
                                    return $baseComune;
                                    } else {
                                             $baseProv   = $prov.", ".$nazione;
                                             $baseProv   = getDataApiByAddress($baseProv);
                                             $statusProv = $baseProv['status'];
                                             if ( $statusProv == "OK" ){
                                                        return $baseProv;
                                                        } else {
                                                                 $baseNazione   = getDataApiByAddress($baseNazione);
                                                                 $statusNazione = $baseNazione['status'];
                                                                 if ( $statusNazione == "OK" ){
                                                                            return $baseNazione;
                                                                            } else {
                                                                                    return $baseNazione;
                                                                                    }
                                                                }
                                            }
                            }
                       
    }

    function setIDVIAGGIOinMovimento($idv){
            $sql =  "UPDATE ".$this->tabellaMov." SET ID_VIAGGIO = ".$idv." WHERE ".$this->tipoMov." = ".$this->mov;
            DbConnector::query($sql);

    }

    function removeViaggioMovimento(){
            $sql =  "UPDATE ".$this->tabellaMov." SET ID_VIAGGIO = 0 WHERE ".$this->tipoMov." = ".$this->mov;
            DbConnector::query($sql);

    }
}




?>
