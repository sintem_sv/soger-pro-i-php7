var da      = "";
var a       = "";
var idv     = "";
var action  = "";
var idmov   = "";
var tipoMov = "";
var anno    = "";
var matriz;
var tipoMz  = "";
var totalTime = 0;
var totalDistance=0;
var directionsService = new google.maps.DirectionsService();
var directionsDisplay;
var marker;
var map;
var wayaddress = [];
var styleCostoRuta;
var costoBase;
var ricalcolaStatus=0;

$(document).ready(function(){

                $("#VerificaTappeViaggio").dialog({
                                autoOpen: false,
                                modal:true,
                                width: 750,
                                position:'top',
                                buttons:[
                                        {
                                        text: "Conferma",
                                        click: function() {                                                          
                                                            confermaVerificaViaggio();
                                                        }
                                        },
                                        {
                                        text:"Cancella",
                                        click: function(){
                                                    if(ricalcolaStatus==0){                                                        
                                                        cancellaVerificaViaggio();
                                                    }else{
                                                            $("#VerificaTappeViaggio").dialog("close");
                                                            }
                                        }
                                        }
                                        ]
                                }).dialog("widget").find(".ui-dialog-titlebar-close").hide();

                $('#tableRoute').hide();
                $('.btn').hover(
                                function() {$(this).addClass('ui-state-hover');},
                                function() {$(this).removeClass('ui-state-hover');}
                                );

                $("#searchByinput").click(function(){
                                renderAffinaRicerca($("#searchAdr").val());
                                })

                $("#salvaRicerca").click(function(){
                        adrApi = $("#addressMarker").text();
                        if(adrApi!="non trovato"){
                                idvt = $("#searchAdr").attr("idvt");
                                rw   = "#tp_idvt"+idvt;
                                coord = $("#searchAdr").attr("latLog");

                                $.post("vg_afinnaSearch.php",{
                                                year:anno,matriz:matriz,
                                                idmov:idmov,tipoMov:tipoMov,
                                                trasporto:tipoMz,
                                                idv:idv,coord:coord,
                                                idt:idvt,adrGloogle:adrApi,
                                                azione:"updateTappa"},
                                                function(data){
                                                            if(data==true){
                                                                    $(rw+" td:nth-child(3)").text(adrApi);
                                                                    $(rw).attr("style","");
                                                                    }
                                                },"json")

                                }else{
                                        alert("affina la ricerca !! indirizzo al momento non trovato");
                                        }
                          })


  }); // END JQ document ready

function ricalcola(){
        ricalcolaStatus=1;
        $.post("vg_ricalcolaImpianti.php",{
                        year:anno,matriz:matriz,
                        idmov:idmov,tipoMov:tipoMov,
                        trasporto:tipoMz,idv:idv,
                        azione:"ricalcolaImpianti"},function (data){

                        VerificaTappeViaggio(data);

                        },"json")

}

 function cancellaVerificaViaggio(){

     $.post("vg_cancellaVerificaViaggio.php",{
                year:anno,matriz:matriz,
                idmov:idmov,tipoMov:tipoMov,
                trasporto:tipoMz,idv:idv,
                azione:"cancellaVerifica"},
                    function(){
				parent.location.replace('../__scripts/status.php?area=UserVediMovimentiFiscali');
                    },"json")

 }


function confermaVerificaViaggio(){
                $.post("vg_confermaVerificaViaggio.php",{
                                year:anno,matriz:matriz,
                                idmov:idmov,tipoMov:tipoMov,
                                trasporto:tipoMz,idv:idv,
                                azione:"confermaVerifica"},function (data){
                                    if(data['status']=='OK'){
                                        $('#tbTappeErrate').find("tr:gt(0)").remove();
                                        $("#VerificaTappeViaggio").dialog('close');
                                        fixTappeToRenderViaggio(data['result']);
                                        }else{alert("Attenzione! \n\n\n Alcuni indirizzi che hai indicato non trovano \n riscontro nelle API di Google. \n\n Si prega di verificarne la correttezza.");}
                                },"json")
}


  function initialize() {
                    directionsDisplay = new google.maps.DirectionsRenderer();
                    var myOptions = {
                                    zoom:7,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP
                                    }

                    map = new google.maps.Map(document.getElementById("map"),myOptions);
                    directionsDisplay.setMap(map);
                    directionsDisplay.setPanel(document.getElementById("directionsPanel"));
                    getParametri();
 }

 function getParametri(){
                    da      = urldecode(gup('da'));
                    a       = urldecode(gup('a'));
                    idv     = urldecode(gup('ID_VIAGGIO'));
                    action  = urldecode(gup('action'));
                    tipoMov = urldecode(gup('ID_MOV_F'));
                    anno    = urldecode(gup('anno'));
                    tipoMz  = urldecode(gup('ID_MZ_TRA'));
                    matriz  =  urldecode(gup('matrix'));

                    if(tipoMov!=""){
                            tipoMov = "ID_MOV_F";
                            idmov   = urldecode(gup('ID_MOV_F'));
                            }else{
                                  tipoMov = "ID_MOV";
                                  idmov   = urldecode(gup('ID_MOV'));
                                  styleCostoRuta = "display:none;"
                                  }

                    ( action == "create" )? creareViaggio() : showViaggio();
 }


 function showViaggio() {

            $.post("vg_showViaggio.php",{
                                   year:anno,matriz:matriz,
                                   idmov:idmov,tipoMov:tipoMov,
                                   trasporto:tipoMz,idv:idv,
                                   azione:"show"
            },function(data){
                       sethtmlByViaggio(data['viaggio']);
                       fixTappeToRenderViaggio(data['tappe']);
            },"json")

 }

 function creareViaggio(){

                    $.ajax({
                                async:false,
                                dataType:"json",
                                type: 'POST',
                                url: "vg_creare.php",
                                data: {year:anno,matriz:matriz,
                                       idmov:idmov,tipoMov:tipoMov,
                                       trasporto:tipoMz,
                                       azione:"create"},
                                success: function(data) {  
                                        dataViaggio = data['viaggio'];
                                        dataTappa   = data['tappe'];
                                        idv = data['tappe'][0]['ID_VIAGGIO']
                                        tpnonTrovato=0;
                                        sethtmlByViaggio(data['viaggio']);

                                        for( i=0; i<data['tappe'].length; i++){
                                            if(data['tappe'][i]['indirizzo_google']=='non trovato')
                                                        tpnonTrovato=1;
                                        }
                                        if(tpnonTrovato==1){
                                            VerificaTappeViaggio(data['tappe']);
                                            }else{
                                                   fixTappeToRenderViaggio(data['tappe']);
                                                    }
                                        }
                    //                      beforeSend:function(){},
                    //                      error:function(objXMLHttpRequest){}
                    });

 }

 function VerificaTappeViaggio(tappe){
                    $('#tbTappeErrate').find("tr:gt(0)").remove();
                    $("#tbTappeErrate .tpSearch").remove();
                    $("#addressMarker").text("");
                    $("#searchAdr").val("");
                    html="";
                    for( i=0; i<tappe.length; i++){

                                switch (tappe[i]['ID_TIPO_TAPPA']) {
                                        case "1":
                                                  tipo = "Partenza";
                                            break;
                                        case "2":
                                                  tipo = "Tappa";
                                            break;
                                        case "3":
                                                  tipo = "Destino";
                                            break;
                                        }

                                if(tappe[i]['indirizzo_google']=='non trovato'){
                                    trclasse = "style='background-color:#D43037; color:#fff'";
                                    }else{
                                            trclasse = "style=''";
                                            }

                                html +="<tr "+trclasse+" id='tp_idvt"+tappe[i]['ID_TAPPA']+"' class='tpSearch'>"
                                html +="<td>"+tipo+"</td>";
                                if(tipo=="Partenza" || tipo == "Destino"){
                                        html +="<td>"+tappe[i]['indirizzo']+","+tappe[i]['cap']+","+tappe[i]['comune']+","+tappe[i]['nazione']+"</td>";
                                        }else{
                                              html +="<td>"+tappe[i]['indirizzo']+"</td>";
                                              }
                                html +="<td>"+tappe[i]['indirizzo_google']+"</td>";
                                html +="<td><span onclick='affinaSearch(\""+tappe[i]['ID_TAPPA']+"\")' class='ui-state-default ui-corner-all ui-button btn'>Ricerca</span></td>";
                                html +="</tr>";
                     }

                     $("#tbTappeErrate").append(html);
                     $('#tbTappeErrate .btn').hover(
                            function() {$(this).addClass('ui-state-hover');},
                            function() {$(this).removeClass('ui-state-hover');}
                            );

                     $("#tbTappeErrate tr:odd").addClass("striped");

                     $("#VerificaTappeViaggio").dialog("open");
 }

 function affinaSearch(tappa){

$("#tbTappeErrate tr").each(function() {
        $(this).find("td:nth-child(1)").attr("style","");
        });

                    $("#searchAdr").attr("latLog","");
                    rw = "#tp_idvt"+tappa;
                    $(rw+" td:nth-child(1)").attr("style","background-color:#88CC22;color:#174F05");
                    tipotp = $(rw+" td:nth-child(1)").text();
                    tipoIndirizzo = $(rw+" td:nth-child(2)").text();
                    tipogloogle = $(rw+" td:nth-child(3)").text();
                    src=(tipogloogle=="non trovato")?tipoIndirizzo:tipogloogle;
                    $("#searchAdr").val(src);
                    $("#searchAdr").attr("idvt",tappa);
                    renderAffinaRicerca(src);
 }

function renderAffinaRicerca(adr){
                    geoFindAddress(adr,function (data){
                    if (data['status']=="OK") {
                                          latLog = data['result'][0].geometry.location.lat()+","+data['result'][0].geometry.location.lng();
                                          $("#searchAdr").attr("latLog",latLog);
                                          $("#searchAdr").val( data['result'][0].formatted_address );
                                          $("#addressMarker").text( data['result'][0].formatted_address );

                                          var mapOptions = {
                                                            zoom: 13,
                                                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                                                            center: data['result'][0].geometry.location
                                                            };

                                          mapB = new google.maps.Map(document.getElementById("mapB"),mapOptions);
                                          marker = new google.maps.Marker({
                                                            map:mapB,
                                                            draggable:true,
                                                            position: data['result'][0].geometry.location
                                                            });
                                          google.maps.event.addListener(marker, 'dragend', function(){
                                                            latLog = this.position.lat()+","+this.position.lng();
                                                            geoFindBylatLong(marker.getPosition(),function(result){

                                                                            if(result['status']=="OK"){
                                                                                $("#addressMarker").text(result['result'][0].formatted_address);
                                                                                $("#searchAdr").val(result['result'][0].formatted_address);
                                                                                $("#searchAdr").attr("latLog",latLog);
                                                                                }else{
                                                                                        $("#addressMarker").text("non trovato")
                                                                                        $("#searchAdr").val("non trovato");
                                                                                        }
                                                            });
                                            });
                       } else {
                                $("#addressMarker").text("non trovato")
                                $("#searchAdr").val("non trovato");
                                }


                      })
}

function fixTappeToRenderViaggio(tappe){
                    wayaddress.length=0;
                     for(i=0; i<tappe.length; i++){
                            switch (tappe[i]['ID_TIPO_TAPPA']) {
                                    case "1":
                                                da=tappe[i]['indirizzo_google'];
                                                $('#streetProd').text(da);
                                                $("#da").val(da);
                                                break;
                                    case "2":
                                                wayaddress.push(tappe[i]['indirizzo_google']);
                                                break;
                                    case "3":
                                                a=tappe[i]['indirizzo_google'];
                                                $('#streetRicup').text(a);
                                                $("#a").val(a);
                                                break;
                                    }
                      }
                      // set html tappe intermedie
                      $('.detailTappa tr').remove();
                      for(n=0; n<wayaddress.length; n++){
                            $('.detailTappa').append("<tr id='"+(n)+"'><td class=\"tbtitolo\">Tappa :</td><td class=\"gray pointappa\">"+wayaddress[n]+"</td><td>&nbsp;</td><td onclick=\"javascript:removeRow('"+(n)+"');\" class=\"cancella ui-button ui-state-default ui-corner-all\"><span class=\"ui-icon ui-icon-trash\"></span></td></tr>");
                            }
                      renderMapsByWayaddres(wayaddress);
}

function renderMapsByWayaddres(wayAdrs){
            var myOptions = {
                    zoom:7,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                    }

            map = new google.maps.Map(document.getElementById("map"),myOptions);
            directionsDisplay.setMap(map);
            var waypts = [];
            for (var n = 0; n < wayAdrs.length; n++) {
                        waypts.push({
                                  location:wayAdrs[n],
                                  stopover:true
                                  });
            }

            var request = {
                        origin: da,
                        destination: a,
                        waypoints:waypts,
                        optimizeWaypoints:false,
                        travelMode: google.maps.DirectionsTravelMode.DRIVING
                        };

             directionsService.route(request, function(response, status) {
                        totalDistance=0;
                        totalTime=0;
                        if (status == google.maps.DirectionsStatus.OK) {
                                      var rutaDetaglio = new Array();
                                      directionsDisplay.setDirections(response);

                                      var route = response.routes[0];
                                      for (var i = 0; i < route.legs.length; i++) {
                                                var star     =   route.legs[i].start_address;
                                                var end      =   route.legs[i].end_address;
                                                var distance =   route.legs[i].distance.value;
                                                var duration =   route.legs[i].duration.value;
                                                var ruta = {"partenza": star, "destino": end, "tempo": duration, "distance": distance, "costo":(costoBase*distance/1000).toFixed(3)}
                                                rutaDetaglio.push(ruta);
                                                totalDistance = totalDistance+parseInt(distance);
                                                totalTime   = totalTime+parseInt(duration);
                                                }
                                                updateViaggioByRouteResume(rutaDetaglio);
                                      }
               })

}


function updateViaggioByRouteResume(routeResume){
                    setHtml(routeResume);
                    $.post("vg_updateViaggio.php",{
                                year:anno,matriz:matriz,
                                idmov:idmov,tipoMov:tipoMov,
                                trasporto:tipoMz,idv:idv,
                                resumeRoute:routeResume,time:totalTime,
                                distance:totalDistance,
                                azione:"updateByRoute"},function(data){
                                },"json");
}

function setHtml(ruta){
                    $("#totdistance").text((totalDistance)/1000);
                    $("#totalCosto").text((costoBase*totalDistance/1000).toFixed(3));
                    price = parseFloat($("#totalCosto").text())+parseFloat($("#correttivoViaggio").text());
                    $("#totPrice").text(price.toFixed(3));

                    getTime = secondsToTime(totalTime);
                    getTime = getTime.h+" ore : "+getTime.m+" min";
                    $("#infotime").text(getTime);
                    $("#getDistance").text(totalDistance/1000+" km");
                    $('#tableRoute').find("tr:gt(0)").remove();
                    //$('#tableRoute').children( 'tr:not(:first)' ).remove();
                    for ( r=0; r<ruta.length; r++ ){
                            rtm = secondsToTime(ruta[r]['tempo']);
                            rtm = rtm.h+":"+rtm.m;
                            str='<tr><td>'+ruta[r]['partenza']+'</td><td>'+ruta[r]['destino']+'</td><td>'+(ruta[r]['distance'])/1000+'</td><td>'+rtm+'</td></tr>';
                            $('#tableRoute tr:last').after(str);
                            }

}

function getCosto(){
               var tipo = "";
               var rstcarTot = 0;
               var rstcar = 0;
               var rspCosto="";
               var costi = new Array();

               if(tipoMz!=7){
                       tipo='.carro';
                       rspCosto = '#rptaCarro';
                       }else{tipo = '.furgone'
                                rspCosto = '#rptaFurgone';
                                }

               $(tipo).each(function(index) {
                       rstcar = parseFloat($(this).val());
                       costi.push(rstcar);
                       rstcarTot = rstcarTot + rstcar;
                       });

                //Costobase � sommatoria dei costi da calcolare
                rstcarTot = rstcarTot.toFixed(3);
                $(rspCosto).text("");
                $(rspCosto).append(rstcarTot);

                $('#costBase').text("");
                $('#costBase').append(rstcarTot);
                $('#tipo').text("");

                // Get info for total cost
                var totDistance = $('#totdistance').text();

                var price = totDistance*(parseFloat($('#costBase').text()));
                price = price.toFixed(3);
                $("#totalCosto").text(price);
                price = parseFloat($('#correttivoViaggio').text())+parseFloat(price);
                $('#totPrice').text(price.toFixed(3));

$.post("vg_updateCosto.php",{
                matriz:matriz,year:anno,
                idv:idv,idmov:idmov,tipoMov:tipoMov,
                trasporto:tipoMz,listaCosti:costi,costoTotal:price,
                costoKm:rstcarTot,distanzaTotal:totDistance,
                azione:"updateCosto"
                },function(data){
                        setDetaglioTappe(data);
                },"json")

//                $.ajax({
//                        async:false,
//                        dataType:"json",
//                        type: 'POST',
//                        url: "vg_updateCosto.php",
//                        data: {matriz:matriz,year:anno,
//                                idv:idv,idmov:idmov,tipoMov:tipoMov,
//                                trasporto:tipoMz,listaCosti:costi,costoTotal:price,
//                                costoKm:rstcarTot,distanzaTotal:totDistance,
//                                azione:"updateCosto"},
//                        success: function(data) {
//                                setDetaglioTappe(data);
//                        }
//                    });

}

function setDetaglioTappe(ruta){
                    $('#tableRoute').find("tr:gt(0)").remove();
                    for(r=0;r<ruta.length;r++){
                        rtm = secondsToTime(ruta[r]['durata']);
                        rtm = rtm.h+":"+rtm.m;
                        str='<tr><td>'+ruta[r]['da']+'</td><td>'+ruta[r]['a']+'</td><td>'+((ruta[r]['distanza'])/1000)+'</td><td>'+rtm+'</td></tr>';
                        $('#tableRoute tr:last').after(str);
                        }
 }

function sethtmlByViaggio(costi){    
                    var tipoTra= (tipoMz==7)? "_f":"_c";
                    var tipiCosti = new Array("costo_gasolio","costo_autostrada","costo_rc","costo_bra","costo_conducente","costo_usura","costo_generale");

                    var C1 = costi.costo_gasolio;
                    var strg1 = "#"+tipiCosti[0]+tipoTra;
                    $(strg1).val(C1);

                    var C2 = costi.costo_autostrada;
                    var strg2 = "#"+tipiCosti[1]+tipoTra;
                    $(strg2).val(C2);

                    var C3 = costi.costo_rc;
                    var strg3 = "#"+tipiCosti[2]+tipoTra;
                    $(strg3).val(C3);

                    var C4 = costi.costo_bra;
                    var strg4 = "#"+tipiCosti[3]+tipoTra;
                    $(strg4).val(C4);

                    var C5 = costi.costo_conducente;
                    var strg5 = "#"+tipiCosti[4]+tipoTra;
                    $(strg5).val(C5);

                    var C6 = costi.costo_usura;
                    var strg6 = "#"+tipiCosti[5]+tipoTra;
                    $(strg6).val(C6);

                    var C7 = costi.costo_generale;
                    var strg7 = "#"+tipiCosti[6]+tipoTra;
                    $(strg7).val(C7);

                    var C8 = costi.costo_totale;

                    costoBase = C8;
                    // Nasconde la colonna dei costi d'accordo al tipo di trasporto
                    
                    correttivoViaggio = costi.costo_correttivo;
                    if(tipoMz==7){
                            var strg8="#rptaFurgone";
                            $("td:has(.carro)").hide();
                            $(".titoloAutocarro").hide();
                            $("#rptaCarro").hide();
                            //correttivoViaggio = costi.costo_correttivo;
                    }else{
                            var strg8="#rptaCarro";
                            $("td:has(.furgone)").hide();
                            $(".titoloFurgone").hide();
                            $("#rptaFurgone").hide();
                            //correttivoViaggio = "80.000";
                            }

                    $(strg8).text(C8);
                    $("#costBase").text(C8);

                    var C9 = costi.costo_calcolato;
                    var strg9 = "#totPrice";
                    $(strg9).text(C9);

                    // Set correttivoViaggio
                    $("#correttivoViaggio").text(correttivoViaggio);

}

function removeRow(id) {
                     var tr = document.getElementById(id);
                     if (tr) {
                               if (tr.nodeName == 'TR') {
                                          var tbl = tr; // Look up the hierarchy for TABLE
                                          while (tbl != document && tbl.nodeName != 'TABLE') {
                                                            tbl = tbl.parentNode;
                                                            }
                                          if (tbl && tbl.nodeName == 'TABLE') {
                                                  while (tr.hasChildNodes()) {
                                                       tr.removeChild( tr.lastChild );
                                                       }
                                                   tr.parentNode.removeChild( tr );
                                                   }
                                }else{
                                    alert( 'Specified document element is not a TR. id=' + id );
                                }
                      }else{
                            alert( 'Specified document element is not found. id=' + id );
                      }
}

function confermatappe(){    
                     tappeArray = new Array();
                     $(".pointappa").each(function() {
                            tappeArray.push($(this).text());
                            });
                     $.post("vg_confermaTappe.php", {
                            year:anno,matriz:matriz,idmov:idmov,tipoMov:tipoMov,
                            trasporto:tipoMz,idv:idv,
                            tappe:(tappeArray.length!=0)?tappeArray:"null",
                            da:da,a:a,azione:"conferma"},
                            function(data){
                                    if(data=="OK")
                                       renderMapsByWayaddres(tappeArray);
                            },"json");
}

function Addtappa(){
                    var numrow = $('.detailTappa tr').length;
                    //var numrow =$("#partenza").siblings().size();
                    var tappa = $('#addtappa').val();
                    if ( tappa!="" ){
                                    geoFindAddress(tappa,function(t){
                                                if ( t['status']=='OK' ){
                                                        var newrow = $("<tr></tr><tr id='"+(numrow+1)+"'><td class=\"tbtitolo\">Tappa :</td><td class=\"gray pointappa\">"+t['result'][0].formatted_address+"</td><td>&nbsp;</td><td onclick=\"javascript:removeRow('"+(numrow+1)+"');\"  class='cancella ui-button ui-state-default ui-corner-all'><span class='ui-icon ui-icon-trash'>&nbsp;</span></td></tr>");
                                                        if ( numrow==0 ){
                                                                $('.detailTappa').append("<tr id='"+(numrow+1)+"'><td class=\"tbtitolo\">Tappa :</td><td class=\"gray pointappa\">"+t['result'][0].formatted_address+"</td><td>&nbsp;</td><td onclick=\"javascript:removeRow('"+(numrow+1)+"');\"  class='cancella ui-button ui-state-default ui-corner-all'><span class='ui-icon ui-icon-trash'>&nbsp;</span></td></tr>");
                                                                $('#addtappa').val("");
                                                        } else {
                                                                $('.detailTappa tr:last').after(newrow);
                                                                $('#addtappa').val("");
                                                                }
                                                  } else {
                                                          alert(tappa+'\n Indirizzo non trovato');}
                                                })
                      } else {
                                alert("Inserire un indirizzo");
                                }
}

function secondsToTime(secs){
                    var hours = Math.floor(secs / (60 * 60));

                    var divisor_for_minutes = secs % (60 * 60);
                    var minutes = Math.floor(divisor_for_minutes / 60);

                    var divisor_for_seconds = divisor_for_minutes % 60;
                    var seconds = Math.ceil(divisor_for_seconds);

                    var obj = {
                        "h": hours,
                        "m": minutes,
                        "s": seconds
                    };
                    return obj;
}

function detaglioTappe(){
                    $('#detaglioTappe').hide();
                    $('#tableRoute').show();
}


function geoFindBylatLong(latLong,callback){
            var retorno = new Array();
            var finder  = new google.maps.Geocoder();
            if (finder) {
                    finder.geocode({'latLng':latLong}, function (results, status) {
                                if (status == google.maps.GeocoderStatus.OK) {
                                        retorno['status']="OK";
                                        retorno['result']=results;
                                } else { // Se non trova nessun risultato
                                        retorno['status']="NULL";
                                        }
                                        callback(retorno);
                    })}

}


function geoFindAddress(indirizzo,callback){
            var retorno = new Array();
            var finder = new google.maps.Geocoder();
            if (finder) {
                    finder.geocode({'address':indirizzo}, function (results, status) {
                                if ( status == google.maps.GeocoderStatus.OK ) {
                                        retorno['status']="OK";
                                        retorno['result']=results;
                                } else { // Se non trova nessun risultato
                                        retorno['status']="NULL";
                                        }
                                callback(retorno);
                    })}

}

function gup( name ){

            name        =  name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
            var regexS  =  "[\\?&]"+name+"=([^&#]*)";
            var regex   =  new RegExp( regexS );
            var results =  regex.exec( window.location.href );
            if( results == null )
                return "";
            else
                return results[1];
}

function urldecode (str) {
               // Decodes URL-encoded string
               // return decodeURIComponent((str + '').replace(/\+/g, '%20'));
            return decodeURIComponent(str.replace(/\+/g, ' '));
}

function dump(arr,level){
            var dumped_text = "";
            if(!level) level = 0;
            //The padding given at the beginning of the line.
            var level_padding = "";
            for(var j=0;j<level+1;j++) level_padding += "    ";

            if(typeof(arr) == 'object') { //Array/Hashes/Objects
                    for(var item in arr) {
                            var value = arr[item];

                            if(typeof(value) == 'object') { //If it is an array,
                                    dumped_text += level_padding + "'" + item + "' ...\n";
                                    dumped_text += dump(value,level+1);
                            } else {
                                    dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                            }
                    }
            } else { //Stings/Chars/Numbers etc.
                    dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
            }
            return dumped_text;
}


