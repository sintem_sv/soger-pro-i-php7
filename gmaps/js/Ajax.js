function ajaxObject(url, callbackFunction) {
  var that=this;      
  this.updating = false;

  this.update = function(passData,postMethod) { 
    if (that.updating==true) { 
	return false; 
    }
    that.updating=true;                       
    var AJAX = null;  
    
    if (window.XMLHttpRequest) {              
      AJAX=new XMLHttpRequest();              
    } else {                                  
      AJAX=new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    if (AJAX==null) {
      return false;                               
    } else {
	    
      AJAX.onreadystatechange = function() {  
        if (AJAX.readyState==4) {             
          that.updating=false;                
          that.callback(AJAX.responseText,AJAX.status,AJAX.responseXML);        
          delete AJAX;                                         
        }                                                      
      }
      
      var timestamp = new Date();                              
      if (postMethod=='POST') {
        var uri=urlCall+'?'+timestamp.getTime();
        AJAX.open("POST", uri, true);
        AJAX.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        AJAX.send(passData);
      } else {
        var uri=urlCall+'?'+passData+'&timestamp='+(timestamp*1); 
        AJAX.open("GET", uri, true);                             
        AJAX.send(null);                                         
      }              
      return true;                                             
    }                                                                           
  }
  var urlCall = url;        
  this.callback = callbackFunction || function () { };
}  
