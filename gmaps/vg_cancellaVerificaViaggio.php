<?php
$year           = $_POST['year'];
$matriz         = $_POST['matriz'];
$idmov          = $_POST['idmov'];
$tipoMov        = $_POST['tipoMov'];
$mz             = $_POST['trasporto'];
$azione         = $_POST['azione'];
$idv            = $_POST['idv'];
require_once("vg_viaggio.php");

if($azione=="cancellaVerifica"){
    $viaggio = new Viaggio($year,$matriz,$idmov,$tipoMov,$mz);    
    $viaggio->updatePercorsOriginale($idv);
    $viaggio->removeViaggioMovimento();
    $viaggio->cancellaViaggioByIDV($idv);
    echo json_encode("OK");
}
?>
