<?php
$matriz         = $_POST['matriz'];
$year           = $_POST['year'];
$idv            = $_POST['idv'];
$idmov          = $_POST['idmov'];
$tipoMov        = $_POST['tipoMov'];
$mz             = $_POST['trasporto'];
$azione         = $_POST['azione'];
$listaCosti     = $_POST['listaCosti'];
$costoTotal     = $_POST['costoTotal'];
$costoKm        = $_POST['costoKm'];
$distanzaTotal  = $_POST['distanzaTotal'];


require_once("vg_viaggio.php");
if($azione=="updateCosto"){
                $create = new Viaggio($year,$matriz,$idmov,$tipoMov,$mz);

                // update user_viaggio
                $create->setViaggioByID($idv);
                $create->distanza         = $distanzaTotal;
                $create->ID_MZ_TRA        = $mz;
                $create->costo_gasolio    = $listaCosti[0];
                $create->costo_autostrada = $listaCosti[1];
                $create->costo_rc         = $listaCosti[2];
                $create->costo_bra        = $listaCosti[3];
                $create->costo_conducente = $listaCosti[4];
                $create->costo_usura      = $listaCosti[5];
                $create->costo_generale   = $listaCosti[6];
                $create->costo_totale     = $costoKm;
                $create->costo_calcolato  = $costoTotal;
                $create->saveViaggio();

                // update user_viaggio_details
                $td = $create->getTappeDetailsByIdViaggio($idv);
                
                for($i=0; $i<count($td); $i++){
                            $id         = $td[$i]['ID_VTD'];
                            $distance   = $td[$i]['distanza'];
                            $newcosto   = ($distance/1000)*$costoKm;
                            $sql = "UPDATE `user_viaggi_tappe_details` SET costo =".$newcosto." WHERE ID_VTD = ".$id;
                            $create->query($sql);
                        }

                 $td = $create->getTappeDetailsByIdViaggio($idv);

                 echo json_encode($td);


              }
?>
