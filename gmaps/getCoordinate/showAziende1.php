<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel ='stylesheet' type='text/css' href='css/jquery-ui-1.8.7.custom.css'/>
        <link rel ='stylesheet' type='text/css' href='css/map.css'/>
        <script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
        <script type='text/javascript' src='js/jquery-ui-1.8.7.custom.min.js'></script>
    </head>
    <script type="text/javascript">

        var last = "";
        function getRecords(tb,rec){
             last = rec;
             $.ajax({
                        type: "POST",
                        async: false,
                        url: "getRecords.php",
                        data: {table:tb,starec:last},
                        dataType: 'json',
                        success: function(dati){

                            if(dati[0]['msn']=='ERROR'){
                            alert("Finito");
                            
                            }else{
                                      for(i=1;i<dati.length;i++){
                                               (i%2==0)?style="#ffffff":style="#F9F290";
                                               str =  "<tr class='mov' style='background-color:"+style+";color:#565656;'><td class='numMov'>"+i+"</td><td id='impP"+i+"'>"+dati[i]['impianto']+"</td>";
                                               str =  str + "<td><span class='from' id='aziendaP"+i+"'>"+dati[i]['idazienda']+"</span></td>";
                                               str =  str + "<td><span style='color:#D84116' class='fromg' id='indirizzoP"+i+"'>"+dati[i]['indirizzo']+"</span></td>";
                                               str =  str + "<td><span style='color:#D84116' class='fromg' id='comuneP"+i+"'>"+dati[i]['comune']+"</span></td>";
                                               str =  str + "<td><span style='color:#D84116' class='fromg' id='provinciaP"+i+"'>"+dati[i]['provincia']+"</span></td>";
                                               str =  str + "<td><span style='color:#D84116' class='fromg' id='nazioneP"+i+"'>"+dati[i]['nazione']+"</span></td>";
                                               str =  str + "<td><span style='color:#D84116' class='fromg' id='fulladdressP"+i+"'></span></td>";
                                               str =  str + "<td><span class='lat' id='Plat"+i+"'></span></td>";
                                               str =  str + "<td><span style='color:#D84116' class='lon' id='Plon"+i+"'></span></td>";
                                               str =  str + "<td id='Pstatus"+i+"'></td>";
                                               str =  str + "<td id='gindirizzoP"+i+"'></td>";
                                               str =  str + "<td id='gcomuneP"+i+"'></td>";
                                               str =  str + "<td id='gprovinciaP"+i+"'></td>";
                                               str =  str + "<td id='gnazioneP"+i+"'></td>";
                                               str =  str + "</tr>";
                                               $("#tbProduttori").append(str);
                                               } // END FOR
						getLatitude("Produttori");
                                        }   // END ELSE
                               }        // END FUNCTION
                            });         // END AJAX
            
        }




         function getLatitude(impianto){
                i=1;
                imptipo = "#tb"+impianto+" .mov";

                if(impianto=="Produttori")tb="P";
                if(impianto=="Destinatari")tb="D";
                if(impianto=="Trasportatori")tb="T";
                if(impianto=="Intermediari")tb="I";

                $(imptipo).each(function(){

                                 indirizzotb  = $(this).find("td").eq(3).text();
                                 comunetb     = $(this).find("td").eq(4).text();
                                 provinciatb  = $(this).find("td").eq(5).text();
                                 nazionetb    = $(this).find("td").eq(6).text();

                                 indirizzo = indirizzotb+","+comunetb+","+provinciatb+","+nazionetb;
								 $(this).find("td").eq(7).text(indirizzo);
                                 lat = "";
                                 lon = "";
                                 $.ajax({
                                        type: "POST",
                                        async: false,
                                        url: "getLatitudine.php",
                                        data: {indirizzo:indirizzo},
                                        dataType: 'json',
                                        success: function(dati){                                       
                                        if(dati['Status']['code']==200 && dati['Placemark'][0]['AddressDetails']['Accuracy']>6){

                                            precicion = dati['Placemark'][0]['AddressDetails']['Accuracy'];
                                            comune = dati['Placemark'][0]['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']['LocalityName'];
                                            provincia =dati['Placemark'][0]['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['SubAdministrativeAreaName'];
                                            country = dati['Placemark'][0]['AddressDetails']['Country']['CountryName'];

                                            addr = dati['Placemark'][0]['address'];

                                            log = dati['Placemark'][0]['Point']['coordinates'][0];
                                            latd = dati['Placemark'][0]['Point']['coordinates'][1];
//                                          alert(dump(dati['Placemark'][0]['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']['DependentLocality']['Thoroughfare']['ThoroughfareName']));
                                            //indir = dati['Placemark'][0]['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']['Thoroughfare']['ThoroughfareName'];

                                            $("#"+tb+"lat"+i).text(latd);
                                            $("#"+tb+"lon"+i).text(log);
                                            $("#"+tb+"status"+i).text("OK");
                                            $("#"+tb+"lon"+i).text(log);
                                            $("#gindirizzo"+tb+i).text(addr);
                                            $("#gcomune"+tb+i).text(comune);
                                            $("#gprovincia"+tb+i).text(provincia);
                                            $("#gnazione"+tb+i).text(country);

                                            }else{
                                                $("#"+tb+"lat"+i).text("NULL");
                                                $("#"+tb+"lon"+i).text("NULL");
                                                }
                                        }});// END AJAX CALL
                                   i=i+1;
                }); // END EACH

                updateTable("Produttori");
           }


           function updateTable(tabella){                
                imptipo = "#tb"+tabella+" .mov";
                if(tabella=="Produttori")tb="P";
                if(tabella=="Destinatari")tb="D";
                if(tabella=="Trasportatori")tb="T";
                if(tabella=="Intermediari")tb="I";
                c=1;
                $(imptipo).each(function(){
                    //alert($(this).find("td").eq(1).text());
                     status  = $(this).find("td").eq(10).text();
                     if(status=='OK'){
                             imp  = $(this).find("td").eq(1).text();
                             az   = $(this).find("td").eq(2).text();
                             lat  = $(this).find("td").eq(8).text();
                             log  = $(this).find("td").eq(9).text();
                             table = tabella;
                              $.ajax({
                                        type: "POST",
                                        async: false,
                                        url: "updateAziendeTb.php",
                                        data: {table:table,imp:imp,az:az,lat:lat,log:log},
                                        dataType: 'json',
                                        success: function(dati){
                                                    if(dati==true){
                                                        
                                                        imgs = "<img src='css/images/green.png'/>";
                                                        $(imptipo).find("#"+tb+"status"+c).append(imgs);
                                                    }else{
                                                        imgs = "<img src='css/images/blue.png'/>";
                                                        $(imptipo).find("#"+tb+"status"+c).append(imgs);
                                                        }
                                                 }
                                        });// END AJAX
                            } // END IF
                            c=c+1;
                    }); // END EACH

                    cleanTbtogetrecords("tbProduttori");

           }


           function cleanTbtogetrecords(tb){           
            tbrec   = "#"+tb+" tr:last";
            lastrec = $(tbrec).find("td").eq(2).text();
            clean   = "#"+tb+" .mov";
            $(clean).remove();            
            getRecords('user_aziende_produttori',lastrec);
         }



    function dump(arr,level){
	var dumped_text = "";
	if(!level) level = 0;
	//The padding given at the beginning of the line.
	var level_padding = "";
	for(var j=0;j<level+1;j++) level_padding += "    ";

	if(typeof(arr) == 'object') { //Array/Hashes/Objects
		for(var item in arr) {
			var value = arr[item];

			if(typeof(value) == 'object') { //If it is an array,
				dumped_text += level_padding + "'" + item + "' ...\n";
				dumped_text += dump(value,level+1);
			} else {
				dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
			}
		}
	} else { //Stings/Chars/Numbers etc.
		dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
	}
	return dumped_text;
        }

        $("document").ready(function(){




                $("#getProduttori").click(function(){
                    getLatitude("Produttori");
                });

                $("#getDestinatario").click(function(){
                    getLatitude("Destinatari");
                });

                $("#getTrasportatori").click(function(){
                    getLatitude("Trasportatori");
                });

                 $("#getIntermediari").click(function(){
                    getLatitude("Intermediari");
                });

               $("#updateDati").hide();

               $("#updateProduttori").click(function(){
                   updateTable("Produttori");
               });

               $("#updateDestinatari").click(function(){

                   updateTable("Destinatari");
               });

               $("#updateTrasportatori").click(function(){
                   updateTable("Trasportatori");
               });

                $("#updateIntermediari").click(function(){
                   updateTable("Intermediari");
               });


           function updateTable(tabella){
                alert("E' finito il caricamento delle latitude");
                imptipo = "#tb"+tabella+" .mov";
                if(tabella=="Produttori")tb="P";
                if(tabella=="Destinatari")tb="D";
                if(tabella=="Trasportatori")tb="T";
                if(tabella=="Intermediari")tb="I";
                c=0;
                $(imptipo).each(function(){
                     status  = $(this).find("td").eq(10).text();
                     if(status=='OK'){
                             imp  = $(this).find("td").eq(1).text();
                             az   = $(this).find("td").eq(2).text();
                             lat  = $(this).find("td").eq(8).text();
                             log  = $(this).find("td").eq(9).text();
                             table = tabella;
                              $.ajax({
                                        type: "POST",
                                        async: false,
                                        url: "updateAziendeTb.php",
                                        data: {table:table,imp:imp,az:az,lat:lat,log:log},
                                        dataType: 'json',
                                        success: function(dati){                                          
                                                    if(dati==true){
                                                        imgs = "<img src='css/images/green.png'/>";
                                                        $(imptipo).find("#"+tb+"status"+c).append(imgs);
                                                    }else{
                                                        imgs = "<img src='css/images/blue.png'/>";
                                                        $(imptipo).find("#"+tb+"status"+c).append(imgs);
                                                        }
                                                 }
                                        });// END AJAX
                            } // END IF
                            c=c+1;
                    }); // END EACH

                    cleanTbtogetrecords("tbProduttori");

           }
        })



    </script>

    <body onload="getRecords('user_aziende_produttori',0);">

<?php

$year = date('Y');
require_once("connessione/conectarse_db.php");
$conectarse=new DbConnector($year);
$contatore = 0;
$movAggiornati = Array();

// GET DATI PRODUTTORI

                echo "</hr><br/>AZIENDE PRODUTTORI<br/>";
                echo "<div id='getProduttori'><button>GET Cordinate AZIENDE PRODUTTORI</button></div><br/></hr>";
                echo "<div id='updateProduttori'><button>UPDATE -- tbl AZIENDE PRODUTTORI</button></div><br/></hr>";


                echo "<table id='tbProduttori'>
                            <tr style='background-color:#A5BFDD;color:#565656;' align='center'>
                                <td>N�</td>
                                <td>ID_IMP</td>
                                <td>ID_AZP</td>
                                <td>INDIRIZZO</td>
                                <td>COMUNE</td>
                                <td>PROV</td>
                                <td>NAZIONE</td>
                                <td>FULL-address</td>
                                <td>LATITUDE</td>
                                <td>LONGITUDE</td>
                                <td>STATUS</td>
                                <td>gloogeINDIRIZZO</td>
                                <td>gloogleCOMUNE</td>
                                <td>glooPROV</td>
                                <td>gloogleNAZIONE</td>
                            </tr></table>";



?>

    </body>
</html>
