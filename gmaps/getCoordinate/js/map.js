var directionDisplay;
        var year;
        var map;
        var gdir;
        var geocoder = null;
        var radioControl= 0;
        var countappa = 0;
        var actionTappa = '0';
        var kmt = 0;
        var timer = 0;
        var action = "";
        var idv = 0;
        var tipoMz = "";
        var da = "";
        var a = "";
        var idmovimento="";
        var tipoMov="";
        var waypts = new Array();
        var indirizzi = new Array();
        var responseArray = new Array();
        var logAddressIsWorking=false;
        var start="";
        var end="";
        var viaggio;
        var options;
        styleCostoRuta="";
        var points=new Array();

        $(document).ready(function(){
 
            $('.autocompletar input').css("height","40px");

            $('.seeIndirizzi').hide();

            $('.tab_costi').hide();
            $('.infoCalcolo').hide();

            $('#direcciones').hide();

            $('.costoRuta').hide();

            $('.btn').mouseover(function(){
                $(this).addClass('ui-state-hover');
            });

            $(".cancella").mouseover(function(){
                $(this).addClass('ui-state-hover');
            });

            $('.btn').mouseout(function(){
                $(this).removeClass('ui-state-hover');
            });

            $('#calcolaCosto').hide();

             $('#tableRoute').hide();

            $('table input').click(function(){
                $('#calcolaCosto').fadeIn('slow');
            });

            // Personaliza Costi
            $('.rx table :input').attr('disabled',true);

            $(function() {
                $("#radio").buttonset();
            });

            var tipoTras = gup( 'ID_MZ_TRA' );
            if(tipoTras==2){enableInput(1)}else{enableInput(2)}

            //Modifica il URL
            $("#txt").keyup(function () {
			parent.location.hash = $(this).val();
            });

            
        var num;
        var $tds;
        $("#tableRoute").each(function(i, t) {
           $tds = $("td", t);
           num = $tds.length;
           if (num % 6 == 0) {
                $tds.eq(0).css("width", "9%").end()
                    .eq(1).css("width", "20%").end()
                    .eq(2).css("width", "20%").end()
                    .eq(3).css("width", "12%").end()
                    .eq(4).css("width", "27%").end()
                    .eq(5).css("width", "6%");
            }
        });

        });

        function urldecode (str) {
            // Decodes URL-encoded string
            return decodeURIComponent((str + '').replace(/\+/g, '%20'));
        }


        function gup( name ){
            name        =  name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
            var regexS  =  "[\\?&]"+name+"=([^&#]*)";
            var regex   =  new RegExp( regexS );
            var results =  regex.exec( window.location.href );
            if( results == null )
                return "";
            else
                return results[1];
        }

       //Dump.
       function dump(arr,level){
			var dumped_text = "";
			if(!level) level = 0;
			//The padding given at the beginning of the line.
			var level_padding = "";
			for(var j=0;j<level+1;j++) level_padding += "    ";

			if(typeof(arr) == 'object') { //Array/Hashes/Objects
				for(var item in arr) {
					var value = arr[item];

					if(typeof(value) == 'object') { //If it is an array,
						dumped_text += level_padding + "'" + item + "' ...\n";
						dumped_text += dump(value,level+1);
					} else {
						dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
					}
				}
			} else { //Stings/Chars/Numbers etc.
				dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
			}
			return dumped_text;
        }


        //////////////////////////     Load the maps  /////////////////////////////////////////////////
        function test (){
                alert("da: "+da+" a: "+a);         
            }


        function load() {
          if (GBrowserIsCompatible()) {
             map = new GMap2(document.getElementById("map"));
             map.addControl(new GLargeMapControl());
             map.addControl(new GMapTypeControl());
             map.enableScrollWheelZoom();
             gdir = new GDirections(map, document.getElementById("direcciones"),{getSteps: true});
             GEvent.addListener(gdir, "load", onGDirectionsLoad);
             GEvent.addListener(gdir, "error", mostrarError);
             // Getting dei parametri che sono nella URL
             action   = gup('action');
             tipoMz   = gup('ID_MZ_TRA');
             year     = gup('anno');
             if(tipoMz!=7)tipoMz=2;
             da   = urldecode(gup('da'));
             a    = urldecode(gup('a'));
             idv  = gup('ID_VIAGGIO');
             // Definire che tipo di movimento comporta fiscale o industriale
            ID_MOV_F = gup('ID_MOV_F');
            if(ID_MOV_F!=""){
                idmovimento=ID_MOV_F;
                tipoMov = "1";
                //$('.rx').show();
                $('.costoRuta').show();
                $('.tab_costi').show();
                $('.infoCalcolo').show();
                $('.infoPercorso').css('width','922px');
            }else{
                ID_MOV = gup('ID_MOV');
                idmovimento=ID_MOV;
                tipoMov = "0";
                styleCostoRuta = "display:none;"                
                //$('#tableRoute td:nth-child(4)').hide();
                $('.infoPercorso').css('width','922px');
                }

            switch(action){
                case "create":
                    create(da,a);
                    break;
                case "show":
                    show(idv,da,a);                    
                    break;
                }
           }
       }

       function onGDirectionsLoad(){
          // Se esiste piu d' una route
          
           var rutaAr = new Array();
             if (gdir.getNumRoutes() > 0) {
                for (var i = 0; i < gdir.getNumRoutes(); i++) {
                     var star     = gdir.getRoute(i).getStartGeocode().address;
                     var end      = gdir.getRoute(i).getEndGeocode().address;
                     var distance = gdir.getRoute(i).getDistance().meters;
                     var duration = gdir.getRoute(i).getDuration().seconds;
                     var ruta = {"partenza": star, "destino": end, "tempo": duration, "distance": distance}
                     rutaAr.push(ruta);
                     }
                }

            //Get tempo e distanza del complesso viaggio
            kmt         = gdir.getDistance().meters;
            timer       = gdir.getDuration().seconds;
            var time    = rectime(timer);
            var km      = (kmt / 1000);
            document.getElementById("getDistance").innerHTML = km + " km";
            document.getElementById("infotime").innerHTML = time;
            // Set costo del complesso viaggio
            $('#totdistance').text(km);
            $('#totPrice').text('');
            var price = km*($('#costBase').text())
            price = price.toFixed(2);
            $('#totPrice').text(price);
            // Se sono modificati le tappe o costi controller aggiornara i dati nel DB
            if(actionTappa == '1') controller(rutaAr);
                model(action,timer,kmt,idmovimento,tipoMov,rutaAr);
                
        }

        function model(act,timer,kmt,idmovimento,tipoMov,rutaArray){
            switch(act){
                // Crea viaggio d'accordo alla info del URL' dopo aggiorna la URL con action show
                case "create":
                    $.post("CreaViaggio.php", {year:year,action:act, mz:tipoMz, da:da, a:a, time:timer, distance:kmt, idv:idv,idmov:idmovimento,tipomov:tipoMov,rutaAr:rutaArray},
                        function(dati){
                           var serverAdress=parent.location.href.split("?");
                           serverAdress=serverAdress[0];
                           parent.location.href=serverAdress+dati["url"];
                        },"json");
                    break;
                 case "show":
             
                    break;
            }
        }

        function create(da,a){

             $.post("CreaDefault.php", {year:year,da:da, a:a,idv:idv,mz:tipoMz,idmov:idmovimento,tipomov:tipoMov},function(dati){
//                    alert("default\n"+dump(dati));
                    verificaRigaIndirizzo(dati);
                },"json");

        }

        var numdati;
       
		function verificaRigaIndirizzo(dati){
            //Verifica la esistenza dei indirizzi per oggi tappa nelle api
            numdati=dati.length;
            var idtappa;
            idv = dati[0]['ID_VIAGGIO'];
            var geocoder = new GClientGeocoder();            
            for(var i=0; i<numdati; i++ ){
               var iString=i+'';
               geocoder.getLocations(dati[i]['indirizzo'],function(response){
               idtappa=dati[iString]['ID_TAPPA'];
               aggiungiRigaIndirizzo(response,idtappa,idv);});
               }
            }

        var TappeCounter=0;
        var TappeUnitario=0;
//
        function aggiungiRigaIndirizzo(response,id,viaggio){
            //Se e trovato il indirizzo(i) si popola il pop up
            //con un valor de indirizzo altrimenti con un vaor nontrovato
            if (TappeCounter%2==0){var stilo="style='color:#419DCF;'"}else{var stilo="style='color:#174693;'"}
            if (!response || response.Status.code != 200) {
                //$( ".addressOK" ).autocomplete({ autoFocus: true });
                $('#ScegliIndirizzo tr:last').after("<tr><td colspan='2'>&nbsp;</td></tr><tr "+stilo+"><td><label>"+response.name+"</label></td><td><span class='addressOKK' iden=\'"+response.name+"\' value='notrovato'>Non Trovato</span></td></tr>");
                seePopup(viaggio);
                } else {
                var risultati=response.Placemark.length;
                if (risultati==1){
                    TappeUnitario++                    
                    $('#ScegliIndirizzo tr:last').after("<tr><td colspan='2'>&nbsp;</td></tr><tr "+stilo+"><td><label>"+response.name+"</label></td><td><span class='addressOKK' iden=\'"+response.name+"\' value='"+response.Placemark[0]['address']+"' >"+response.Placemark[0]['address']+"</span></td></tr>");
                    }
                else{
                    for(var a=0; a<risultati; a++){
                        options  = options + "<option iden=\'"+response.name+"\' value='"+response.Placemark[a]['address']+"' >"+response.Placemark[a]['address']+"</option>"
                        }
                        $('#ScegliIndirizzo tr:last').after("<tr><td colspan='2'>&nbsp;</td></tr><tr "+stilo+"><td><label>"+response.name+"</label></td><td><select class='addressOK'>"+options+"</select></td></tr>");
                        }
                TappeCounter++;
                }
                // Se ha finito il ciclo delle tappe verifico se ha bisogno di far comparire il popup
                if(TappeCounter==numdati){
                        if(TappeUnitario==numdati){

                        var wayaddress = new Array();
                        var address = new Array();
                        var idirizzo;

                            $(".addressOKK").each(function(){
                            idirizzo = $(this).text();
                            idirizzo = idirizzo+"/"+ $(this).attr("iden");
                            wayaddress.push(idirizzo);                               
                            });

                            $.post("StartTappe.php",{year:year,uptappe:"1",idirizzi:wayaddress,idv:viaggio,idmv:idmovimento,tipomov:tipoMov},function(dati){
                                for(r=0;r<dati.length;r++){
                                    idirizzo = dati[r]['indirizzo'];
                                    address.push(idirizzo);
                                    }
                                aggiornaPercorso(address);
                            },"json");

                        }else{
                            seePopup(viaggio);

                        }
                }
         }

        function show(idv,da,a){
            $.getJSON("VisualizaViaggio.php",{year:year,action:"show",idv:idv,da:da,a:a,idmv:idmovimento,tipomov:tipoMov},function(data){
                //Gestione della tabella Ruta da visualizare
                for(r=0;r<(data[2]).length;r++){
                    var distanzaRuta = (data[2][r]["distanza"])/1000;
                    var durataRuta = rectime(data[2][r]["durata"]);
                    $('#tableRoute tr:last').after('<tr><td>'+data[2][r]["da"]+'</td><td>'+data[2][r]["a"]+'</td><td>'+distanzaRuta+'</td><td>'+durataRuta+'</td><td class="costoRuta">'+data[2][r]["costo"]+'</td></tr>');
                    }
               // $("#tableRoute tr:odd").css("background-color", "#bbbbff");

                if(tipoMz==7) var tipoTra="_f";else var tipoTra="_c";
                    //Set costi negli input C contiene il costo a caricate mentre strg determina el input che sara caricato
                    var tipiCosti = new Array("costo_gasolio","costo_autostrada","costo_rc","costo_bra","costo_conducente","costo_usura","costo_generale");

                    var C1 = data[1][0].costo_gasolio;
                    var strg1 = "#"+tipiCosti[0]+tipoTra;
                    $(strg1).val(C1);

                    var C2 = data[1][0].costo_autostrada;
                    var strg2 = "#"+tipiCosti[1]+tipoTra;
                    $(strg2).val(C2);

                    var C3 = data[1][0].costo_rc;
                    var strg3 = "#"+tipiCosti[2]+tipoTra;
                    $(strg3).val(C3);

                    var C4 = data[1][0].costo_bra;
                    var strg4 = "#"+tipiCosti[3]+tipoTra;
                    $(strg4).val(C4);

                    var C5 = data[1][0].costo_conducente;
                    var strg5 = "#"+tipiCosti[4]+tipoTra;
                    $(strg5).val(C5);

                    var C6 = data[1][0].costo_usura;
                    var strg6 = "#"+tipiCosti[5]+tipoTra;
                    $(strg6).val(C6);

                    var C7 = data[1][0].costo_generale;
                    var strg7 = "#"+tipiCosti[6]+tipoTra;
                    $(strg7).val(C7);

                    var C8 = data[1][0].costo_totale;

                    // Nasconde la colonna dei costi d'accordo al tipo di trasporto
                    if(tipoMz==7){
                        var strg8="#rptaFurgone";
                        $("td:has(.carro)").hide();
                        $(".titoloAutocarro").hide();
                        $("#rptaCarro").hide();
                    }else{
                        var strg8="#rptaCarro";
                        $("td:has(.furgone)").hide();
                        $(".titoloFurgone").hide();
                        $("#rptaFurgone").hide();
                    }

                    $(strg8).text(C8);
                    $("#costBase").text(C8);
                    var C9 = data[1][0].costo_calcolato;
                    var strg9 = "#totPrice";
                    $(strg9).text(C9);

                    //Set delle tappe
                    var n  = $(data[0]).length;
                    var addresses = new Array();
                    var ways = new Array();

                    // per ogni tappa (da, tappe, a) del db visualiza e inserire del array addresses per renderizargli
                    //$('#da').val(da);
                    //$('#a').val(a);
                    //
                    for (i=0; i<n; i++){
                        addresses.push(data[0][i].indirizzo);
                        if(i==0){
                            $('#streetProd').text(data[0][i].indirizzo);
                            start =  data[0][i].indirizzo;
                            $('#da').val(start);
                        }else{
                            if(i==n-1){
                                $('#streetRicup').text(data[0][i].indirizzo);
                                end =  data[0][i].indirizzo;
                                $('#a').val(end);
                            }else{
                                $('.detailTappa').append("<tr id='"+(i)+"'><td class=\"tbtitolo\">Tappa :</td><td class=\"gray pointappa\">"+data[0][i].indirizzo+"</td><td>&nbsp;</td><td onclick=\"javascript:removeRow('"+(i)+"');\" class=\"cancella ui-button ui-state-default ui-corner-all\"><span class=\"ui-icon ui-icon-trash\"></span></td></tr>");
                                ways.push(data[0][i].indirizzo);
                                waypts=ways;
                                }
                         }
                      }
                    gdir.loadFromWaypoints(addresses);
					confermatappe();
             });
        }

        function onroute(){
            var da = $('#da').val();
            var a =  $('#a').val();
            if(da!="" && a!="" ){
                gdir.loadFromWaypoints([da,a]);
                $('#streetProd').text(da);
                $('#streetRicup').text(a);
    //            $('#da').val('');
    //            $('#a').val('');
            }else{
                 alert("Inserire partenza la partenza e il arrivo correttamente");
                 }
        }

        function removeRow(id) {
             var tr = document.getElementById(id);
             if (tr) {
               if (tr.nodeName == 'TR') {
                  var tbl = tr; // Look up the hierarchy for TABLE
                  while (tbl != document && tbl.nodeName != 'TABLE') {
                        tbl = tbl.parentNode;
                        }
                  if (tbl && tbl.nodeName == 'TABLE') {
                      while (tr.hasChildNodes()) {
                           tr.removeChild( tr.lastChild );
                           }
                       tr.parentNode.removeChild( tr );
                       }
                }else{
                    alert( 'Specified document element is not a TR. id=' + id );
                }
              }else{
                    alert( 'Specified document element is not found. id=' + id );
              }
        }

        function confermatappe(){
            var da =  $('#streetProd').text();
            var a  =  $('#streetRicup').text();
            points.length=0;
            //Nel array points si trovano tutte le tappe che ha inserito l' utente            
            points.push(da);
            $('.pointappa').each(function(index) {
                point = $(this).text();
                points.push(point);
            });
            points.push(a);
            actionTappa = '1';
//            alert("--->"+dump(points));
            gdir.loadFromWaypoints(points);

        }

        function controller(rutaAr){
            actionTappa = '0';
            var da =  $('#streetProd').text();
            var a =  $('#streetRicup').text();
            //Nel array points si trovano tutte le tappe che ha inserito l' utente
            var points=new Array();
            points.push(da);
            $('.pointappa').each(function(index) {
            point = $(this).text();
            points.push(point);
            });
            points.push(a);

            //Nel array costi ci sono tutti i costi mentre in costTotal e la sommatoria dei costi
            if(tipoMz==7){var tipomzo="#costi input[class|=furgone]";}else{var tipomzo = "#costi input[class|=carro]";}
            var costTotal = 0;
            var costi = new Array();
            $(tipomzo).each(function(){
                costi.push($(this).val());
                costTotal = costTotal + parseFloat($(this).val());
            });
            $("#tableRoute tr:gt(0)").remove();
//            alert("dump Controller\n"+dump(points)+"\n distance"+kmt+"\n inforuta :"+dump(rutaAr)+"\n"+"\n tipomz"+tipoMz)
            $.post("controller.php",{year:year,tappe:points,idv:idv,action:action,costi:costi,costTotal:costTotal,distance:kmt,infRoute:rutaAr,idmov:idmovimento,tipomov:tipoMov,tipoMz:tipoMz},function(resultati){
                    for(r=0;r<(resultati.infRoutes).length;r++){
                        var distanzaRuta = (resultati.infRoutes[r]["distanza"])/1000;
                        var durataRuta = rectime(resultati.infRoutes[r]["durata"]);
                        if (resultati.infRoutes[r]["da"].length > 20)
                           {
                               var split = resultati.infRoutes[r]["da"].split(",");
                               if(split.length>2){
                                   var one = split[0];
                               }else{
                                   var one = split[0];
                               }
                            }else{
                                var one = resultati.infRoutes[r]["da"];
                            }

                        if (resultati.infRoutes[r]["a"].length > 20)
                           {
                               var split = resultati.infRoutes[r]["a"].split(",");
                               if(split.length>2){
                                   var two = split[0];
                               }else{
                                   var two = split[0];
                               }
                            }else{
                                var two = resultati.infRoutes[r]["a"];
                            }
                        $('#tableRoute tr:last').after('<tr><td>'+one+'</td><td>'+two+'</td><td>'+distanzaRuta+'</td><td>'+durataRuta+'</td><td style='+styleCostoRuta+'>'+resultati.infRoutes[r]["costo"]+'</td></tr>');
                    }
            },"json");
        }

       function rectime(sec) {
            var hr = Math.floor(sec / 3600);
            var min = Math.floor((sec - (hr * 3600))/60);
            sec -= ((hr * 3600) + (min * 60));
            sec += '';min += '';
            while (min.length < 2) {min = '0' + min;}
            while (sec.length < 2) {sec = '0' + sec;}
            hr = (hr)?hr+':':'';
            return hr + min + ':' + sec;
       }

       function mostrarError(){
        if (gdir.getStatus().code == G_GEO_UNKNOWN_ADDRESS)
           alert("Uno degli indirizzi indicati e errato, verificarne la correttezza.\n\n\n\n esempio: 'None, Torino'");
           else if (gdir.getStatus().code == G_GEO_SERVER_ERROR)
           alert("Non e stato possibile processare la richiesta a causa di un errore sconosciuto.");
           else if (gdir.getStatus().code == G_GEO_MISSING_QUERY)
           alert("Non e stato possibile processare la richiesta, verificare di aver immesso gli indirizzi.");
           else if (gdir.getStatus().code == G_GEO_BAD_KEY)
           alert("Non e riconosciuta la key dell' API google");
           else if (gdir.getStatus().code == G_GEO_BAD_REQUEST)
           alert("Non e possibile processare corretamente la richiesta.");
//          else {/*alert("Errore sconosciuto.");*/ create(da,a); }


//////////////
            else {

                    var strg="";
                    for(i=0;i<points.length;i++){
                        if(i==0){
                            $('#da').val(points[i]);
                            $('#streetProd').text(points[i]);
                            da = points[i];
                        }else if(i==(points.length-1)){
                            $('#a').val(points[i]);
                            var a  =  $('#streetRicup').text(points[i]);
                            a = points[i];
                            }else{
                                strg=strg+points[i]+"\n\n";
                                $('.detailTappa').append("<tr id='"+i+"'><td class=\"tbtitolo\">Tappa :</td><td class=\"gray pointappa\">"+points[i]+"</td><td>&nbsp;</td><td onclick=\"javascript:removeRow('"+i+"');\"  class='cancella ui-button ui-state-default ui-corner-all'><span class='ui-icon ui-icon-trash'>&nbsp;</span></td></tr>");
                                }
                        }

                alert("Errore sconosciuto.\n E' posibile che non sia un percorso per queste viaggio.\n Verificare la correteza dei indirizzi \n\n\n\n"+strg);
            }
                    
                
       }

       function getCosto(){
           var tipo = "";
           var rstcarTot = 0;
           var rstcar = 0;
           var rspCosto="";

           if(radioControl==1){
               tipo='.carro';
               rspCosto = '#rptaCarro';
           }else{tipo = '.furgone'
               rspCosto = '#rptaFurgone';
            }

           $(tipo).each(function(index) {
             rstcar = ($(this).val())*1;
             rstcarTot = rstcarTot + rstcar;
            });
            //Costbase � la sommatori dei costi da calcolare
            rstcarTot = rstcarTot.toFixed(2);
            $(rspCosto).text("");
            $(rspCosto).append(rstcarTot);

            $('#costBase').text("");
            $('#costBase').append(rstcarTot);
            $('#tipo').text("");

            // Get info for total cost

            //$('.infoCalcolo').hide('slow');
            var totDistance = $('#totdistance').text();
            var price = totDistance*($('#costBase').text());
            price = price.toFixed(2);
            $('#totPrice').text(price);
            //$('.infoCalcolo').show('slow');
            confermatappe();
       }

       function Addtappa(){
        var numrow = $('.detailTappa tr').length;
        //var numrow =$("#partenza").siblings().size();
        var tappa = $('#addtappa').val();
        if(tappa!=""){
           var newrow = $("<tr></tr><tr id='"+(numrow+1)+"'><td class=\"tbtitolo\">Tappa :</td><td class=\"gray pointappa\">"+tappa+"</td><td>&nbsp;</td><td onclick=\"javascript:removeRow('"+(numrow+1)+"');\"  class='cancella ui-button ui-state-default ui-corner-all'><span class='ui-icon ui-icon-trash'>&nbsp;</span></td></tr>");
            if(numrow==0){
                  $('.detailTappa').append("<tr id='"+(numrow+1)+"'><td class=\"tbtitolo\">Tappa :</td><td class=\"gray pointappa\">"+tappa+"</td><td>&nbsp;</td><td onclick=\"javascript:removeRow('"+(numrow+1)+"');\"  class='cancella ui-button ui-state-default ui-corner-all'><span class='ui-icon ui-icon-trash'>&nbsp;</span></td></tr>");
            }else{
                  $('.detailTappa tr:last').after(newrow);
                }
        }else{
           alert("Inserire un indirizzo");
           }
        }

       function obtenerRuta(desde, hasta) {
            var i=0 ;
            if(i==0){
                gdir.load("from: " + desde + " to: " + hasta,
                {"locale": "it", "travelMode" : G_TRAVEL_MODE_DRIVING});
            }else{
               gdir.loadFromWaypoints(["Torino","Genova","Milano","Roma"]);
            }
       }

        function cancellaTappa(){
            //$('.detailTappa tr:last').remove();
             $(this).addClass('ui-state-hover');
        }


        function enableInput(r){
           if(r==1){
                $('.carro').removeAttr('disabled');
                $('.carro').css('background-color','#1A457B');
                $('.carro').css('color','#ccc');
                $('.furgone').attr('disabled',true);
                $('.furgone').css('background-color','#6A91BA');
           }else{
                $('.furgone').removeAttr('disabled');
                $('.furgone').css('background-color','#1A457B');
                $('.furgone').css('color','#ccc');
                $('.carro').attr('disabled',true);
                $('.carro').css('background-color','#6A91BA');
           }
           radioControl=r;
        }

                var request = {
                    origin: start,
                    destination: end,
                    waypoints: waypts,
                    optimizeWaypoints: true
                    //travelMode: google.maps.DirectionsTravelMode.DRIVING
                };
                function tappaInformazione(){
                directionsService.route(request, function(response, status) {
                  if (status == google.maps.DirectionsStatus.OK) {
                    //directionsDisplay.setDirections(response);
                    var route = response.routes[0];
                    var summaryPanel = document.getElementById("directions_panel");
                    summaryPanel.innerHTML = "";
                    // For each route, display summary information.
                    for (var i = 0; i < route.legs.length; i++) {
                      var routeSegment = i + 1;
                      var nu = route.legs.length;
                      summaryPanel.innerHTML += "<b>Route Segment: " + routeSegment + "</b><br />";
                      summaryPanel.innerHTML += route.legs[i].start_address + " to ";
                      summaryPanel.innerHTML += route.legs[i].end_address + "<br />";
                      summaryPanel.innerHTML += route.legs[i].distance.text + "<br /><br />";
                      summaryPanel.innerHTML += route.legs[i].duration.text + "<br /><br />";
                    }
                  }
                });

          }

          function detaglioTappe(){

                $('#detaglioTappe').hide();
                $('#tableRoute').show();
                
                confermatappe();
                
          }

          function seePopup(viaggio){


                    $('.seeIndirizzi').show();
                    comboBox();
                    $(function() {
                            $( "#dialog:ui-dialog" ).dialog( "destroy" );
                            $( "#dialog-confirm" ).dialog({
                                resizable: true,
                                show: "slide",
                                hide: "clip",
                                height:340,
                                width:640,
                                modal: true,
                                position: 'top',
                                buttons: {
                                        "Invio": function() {
                                            var wayaddress = new Array();
                                            var address = new Array();
                                            var idirizzo;
                                            //$('input:radio').each(function(){
                                                $(".addressOK").each(function(){
                                                    idirizzo = $(this).find('option:selected').text();
                                                    idirizzo =idirizzo+"/"+ $(this).find('option:selected').attr("iden");
                                                    wayaddress.push(idirizzo);
                                                       //alert('opcion '+$(this).text()+' valor '+ $(this).attr('value'))
                                                    });

                                                $(".addressOKK").each(function(){
                                                    idirizzo = $(this).text();
                                                    idirizzo =idirizzo+"/"+ $(this).attr("iden");
                                                    wayaddress.push(idirizzo);
                                                       //alert('opcion '+$(this).text()+' valor '+ $(this).attr('value'))
                                                    });

                                                $.post("StartTappe.php",{year:year,uptappe:"1",idirizzi:wayaddress,idv:viaggio,idmov:idmovimento,tipomov:tipoMov},function(dati){

                                                    for(r=0;r<dati.length;r++){
                                                        idirizzo = dati[r]['indirizzo'];
                                                        address.push(idirizzo);
                                                    }

                                                   aggiornaPercorso(address);
                                                },"json");

                                            $( this ).dialog( "close" );
                                        },
                                        Cancel: function() {
                                                $( this ).dialog( "close" );
                                         }
                                    }
                            });
                    })

          }
          function aggiornaPercorso(indirizzi){

//alert(indirizzi);
points =indirizzi;
                        var n=indirizzi.length;
                        for(i=0;i<n;i++){
                            if(i==0)
                                da=indirizzi[i];
                            //alert("sono da"+da)
                            if(i==(n-1))
                                a=indirizzi[i];
                        }
                gdir.loadFromWaypoints(indirizzi);
          }

          function comboBox(){
              (function($) {
		$.widget("ui.combobox", {
			_create: function() {
				var self = this;
				var select = this.element.hide();
				var input = $("<input>")
					.insertAfter(select)
					.autocomplete({
						source: function(request, response) {
							var matcher = new RegExp(request.term, "i");
							response(select.children("option").map(function() {
								var text = $(this).text();
								if (!request.term || matcher.test(text))
									return {
										id: $(this).val(),
										label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + request.term.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1") + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"),
										value: text
									};
							}));
						},
						delay: 0,
						select: function(e, ui) {
							if (!ui.item) {
								// remove invalid value, as it didn't match anything
								//$(this).val("");
								return false;
							}
							$(this).focus();
							select.val(ui.item.id);
							self._trigger("selected", null, {
								item: select.find("[value='" + ui.item.id + "']")
							});

						},
						minLength: 0
					})
					.addClass("ui-widget ui-widget-content ui-corner-left");

                                        

				$("<button>&nbsp;</button>")
				.insertAfter(input)
				.button({
					icons: {
						primary: "ui-icon-triangle-1-s"
					},
					text: false
				}).removeClass("ui-corner-all")
				.addClass("ui-corner-right ui-button-icon")
				.position({
					my: "left center",
					at: "right center",
					of: input,
					offset: "-1 0"
				}).css({top: "4px",height:"18px",left:"-1px"})
                                //.css({backgroundColor: '#ffe', borderLeft: '5px solid #ccc'})
				.click(function() {
					// close if already visible
					if (input.autocomplete("widget").is(":visible")) {
						input.autocomplete("close");
						return;
					}
					// pass empty string as value to search for, displaying all results
					input.autocomplete("search", "");
					input.focus();
				});
			}
		});

	})(jQuery);

	$(function() {
		$("select").combobox();
	});

          }