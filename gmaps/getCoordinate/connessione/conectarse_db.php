
<?php ////////////////////////////////////////////////////////////////////////////////////////
// Class: DbConnector
// Purpose: Connect to a database,MySQLversion
///////////////////////////////////////////////////////////////////////////////////////

require_once 'sistema_coneccion.php';
class DbConnector extends SystemComponent {
	var $theQuery;
	var $link;
	var $num;
	var $result;
        
//*** Function: DbConnector, Purpose: Connect to the database ***




	function __construct($year){
		// Load settings from parent class
		$settings = SystemComponent::getSettings($year);
		// Get the main settings from the array we just loaded
		$host = $settings['dbhost'];
		$db   = $settings['dbname'];
		$user = $settings['dbusername'];
		$pass = $settings['dbpassword'];
		// Connect to the database
		$this->link = mysqli_connect($host, $user, $pass);
                mysqli_set_charset($this->link,'utf8');
		mysqli_select_db($this->link,$db);
		register_shutdown_function(array(&$this, 'close'));
	}

	//*** Function: query, Purpose: Execute a database query ***
	function query($query) {
		$this->theQuery = $query;
		return mysqli_query($this->link,$query);
	}
	//*** Function: getQuery, Purpose: Returns the last database query, for
	//debugging ***
	function getQuery() {
		return $this->theQuery;
	}
	//*** Function: getNumRows, Purpose: Return row count, MySQL version
	//***


	function getNumRows($result){
	//$this->result;
		return mysqli_num_rows(mysqli_query($this->link,$result));
	}
	//*** Function: fetchArray, Purpose: Get array of query results *** function
	function fetchArray($result) {
		return mysqli_fetch_array($result);
	}
	//*** Function: fetchArray, Purpose: Get associative array of query results *** function
	function fetchAssoc($result) {
		return mysqli_fetch_assoc($result);
	}
	//*** Function: close, Purpose: Close the connection ***
	function close() { 
		return mysqli_close($this->link); 
	}
        function ConvertitoreData($data){
        $separa=explode ("/",$data);
        $a=$separa[0];
        $b=$separa[1];
        $c=$separa[2];
        $data_convertita="$c-$b-$a";
        return $data_convertita;
}
	
}
//*** Function: Extrae el txt  ***
        function ExtraeTextoDate($str,$n){
                if (strlen($str)>$n){
                        $exp = explode(" ", $str);
                        $res=implode($exp);
                        $nv=strlen($res);
                        if($nv > $n){
                                $res= substr($res, 0, $n);
                                return $res."...";
                        }else{
                                return $str."...";
                        }
                }else
                return $str."...";
        }
        function ExtraeTexto($str,$n){
                if (strlen($str)>$n){
                                $res= substr($str, 0, $n);
                                $str=$res;
                                return $str."...";
                }
                return $str."...";
        }



?>

