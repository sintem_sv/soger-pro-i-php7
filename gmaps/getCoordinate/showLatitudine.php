<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel ='stylesheet' type='text/css' href='css/jquery-ui-1.8.7.custom.css'/>
        <link rel ='stylesheet' type='text/css' href='css/map.css'/>
        <script type='text/javascript' src='js/jquery-1.4.4.min.js'></script>
        <script type='text/javascript' src='js/jquery-ui-1.8.7.custom.min.js'></script>
    </head>
    <script type="text/javascript">

    function dump(arr,level){
	var dumped_text = "";
	if(!level) level = 0;
	//The padding given at the beginning of the line.
	var level_padding = "";
	for(var j=0;j<level+1;j++) level_padding += "    ";

	if(typeof(arr) == 'object') { //Array/Hashes/Objects
		for(var item in arr) {
			var value = arr[item];

			if(typeof(value) == 'object') { //If it is an array,
				dumped_text += level_padding + "'" + item + "' ...\n";
				dumped_text += dump(value,level+1);
			} else {
				dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
			}
		}
	} else { //Stings/Chars/Numbers etc.
		dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
	}
	return dumped_text;
        }

        $("document").ready(function(){




                $("#getProduttori").click(function(){
                    getLatitude("Produttori");
                });

                $("#getDestinatario").click(function(){
                    getLatitude("Destinatari");
                });

                $("#getTrasportatori").click(function(){
                    getLatitude("Trasportatori");
                });

                 $("#getIntermediari").click(function(){
                    getLatitude("Intermediari");
                });

               $("#updateDati").hide();

               $("#updateProduttori").click(function(){
                   updateTable("Produttori");
               });

               $("#updateDestinatari").click(function(){

                   updateTable("Destinatari");
               });

               $("#updateTrasportatori").click(function(){
                   updateTable("Trasportatori");
               });

                $("#updateIntermediari").click(function(){
                   updateTable("Intermediari");
               });


           function updateTable(tabella){

                imptipo = "#tb"+tabella+" .mov";
                if(tabella=="Produttori")tb="P";
                if(tabella=="Destinatari")tb="D";
                if(tabella=="Trasportatori")tb="T";
                if(tabella=="Intermediari")tb="I";
                c=0;
                $(imptipo).each(function(){
                     status  = $(this).find("td").eq(10).text();
                     if(status=='OK'){
                             uim  = $(this).find("td").eq(1).text();
                             az   = $(this).find("td").eq(2).text();
                             lat  = $(this).find("td").eq(8).text();
                             log  = $(this).find("td").eq(9).text();
                             table = tabella;
                              $.ajax({
                                        type: "POST",
                                        async: false,
                                        url: "updateLatitudine.php",
                                        data: {table:table,uim:uim,az:az,lat:lat,log:log},
                                        dataType: 'json',
                                        success: function(dati){                                            
                                                    if(dati==true){
                                                        imgs = "<img src='css/images/green.png'/>";
                                                        $(imptipo).find("#"+tb+"status"+c).append(imgs);
                                                    }else{
                                                        imgs = "<img src='css/images/blue.png'/>";
                                                        $(imptipo).find("#"+tb+"status"+c).append(imgs);
                                                        }
                                                 }
                                        });// END AJAX
                            } // END IF
                            c=c+1;
                    }); // END EACH
           }


         function getLatitude(impianto){
                i=0;
                imptipo = "#tb"+impianto+" .mov";

                if(impianto=="Produttori")tb="P";
                if(impianto=="Destinatari")tb="D";
                if(impianto=="Trasportatori")tb="T";
                if(impianto=="Intermediari")tb="I";



                $(imptipo).each(function(){
                                 
                                 indirizzotb  = $(this).find("td").eq(3).text();
                                 comunetb     = $(this).find("td").eq(4).text();
                                 provinciatb  = $(this).find("td").eq(5).text();
                                 nazionetb    = $(this).find("td").eq(6).text();

                                 indirizzo = indirizzotb+","+comunetb+","+provinciatb+","+nazionetb
                                 $(this).find("td").eq(7).text(indirizzo);
                                 lat = "";
                                 lon = "";
                                 $.ajax({
                                        type: "POST",
                                        async: false,
                                        url: "getLatitudine.php",
                                        data: {indirizzo:indirizzo},
                                        dataType: 'json',
                                        success: function(dati){
                                        
                                        if(dati['Status']['code']==200 && dati['Placemark'][0]['AddressDetails']['Accuracy']>6){

                                            precicion = dati['Placemark'][0]['AddressDetails']['Accuracy'];
                                            comune = dati['Placemark'][0]['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']['LocalityName'];
                                            provincia =dati['Placemark'][0]['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['SubAdministrativeAreaName'];
                                            country = dati['Placemark'][0]['AddressDetails']['Country']['CountryName'];
                                            
                                            addr = dati['Placemark'][0]['address'];
                                            
                                            log = dati['Placemark'][0]['Point']['coordinates'][0];
                                            latd = dati['Placemark'][0]['Point']['coordinates'][1];
//                                          alert(dump(dati['Placemark'][0]['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']['DependentLocality']['Thoroughfare']['ThoroughfareName']));                                            
                                            indir = dati['Placemark'][0]['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']['Thoroughfare']['ThoroughfareName'];
                                            
                                            $("#"+tb+"lat"+i).text(latd);
                                            $("#"+tb+"lon"+i).text(log);
                                            $("#"+tb+"status"+i).text("OK");
                                            $("#"+tb+"lon"+i).text(log);
                                            $("#gindirizzo"+tb+i).text(addr);
                                            $("#gcomune"+tb+i).text(comune);
                                            $("#gprovincia"+tb+i).text(provincia);
                                            $("#gnazione"+tb+i).text(country);

                                            }else{
                                                $("#"+tb+"lat"+i).text("NULL");
                                                $("#"+tb+"lon"+i).text("NULL");
                                                }
                                        }});// END AJAX CALL
                                   i=i+1;
                }); // END EACH
           }


        })



    </script>

    <body>

<?php

$year = date('Y');
require_once("connessione/conectarse_db.php");
$conectarse=new DbConnector($year);
$contatore = 0;
$movAggiornati = Array();

// GET DATI PRODUTTORI

                echo "</hr><br/>IMPIANTI PRODUTTORI<br/>";
                echo "<div id='getProduttori'><button>GET Cordinate PRODUTTORI</button></div><br/></hr>";
                echo "<div id='updateProduttori'><button>UPDATE -- tbl PRODUTTORI</button></div><br/></hr>";


                echo "<table id='tbProduttori'>
                            <tr style='background-color:#A5BFDD;color:#565656;' align='center'>
                                <td>N�</td>
                                <td>ID_UIMP</td>
                                <td>ID_AZP</td>
                                <td>INDIRIZZO</td>
                                <td>COMUNE</td>
                                <td>PROV</td>
                                <td>NAZIONE</td>
                                <td>FULL-address</td>
                                <td>LATITUDE</td>
                                <td>LONGITUDE</td>
                                <td>STATUS</td>
                                <td>gloogeINDIRIZZO</td>
                                <td>gloogleCOMUNE</td>
                                <td>glooPROV</td>
                                <td>gloogleNAZIONE</td>
                            </tr>";
                   
//                    $sqlP = "SELECT c.description as comune,c.shdes_prov as provincia,c.nazione,i.description
//                    as indirizzo,i.ID_UIMP as impiantoP,i.ID_AZP as aziendaP FROM lov_comuni_istat c
//                    JOIN user_impianti_produttori i ON c.ID_COM = i.ID_COM limit 19421,600";
//
                    $sqlP = "SELECT c.description as comune,c.shdes_prov as provincia,c.nazione,i.description
                    as indirizzo,i.ID_UIMP as impiantoP,i.ID_AZP as aziendaP FROM lov_comuni_istat c
                    JOIN user_impianti_produttori i ON c.ID_COM = i.ID_COM LIMIT 0,1";


                    $sqlP = $conectarse->query($sqlP);
                    $n=0;
                    while($matriz=$conectarse->fetchArray($sqlP)){
                        
                        ($n%2==0)?$style="#ffffff":$style="#F9F290";
                           echo "<tr class='mov' style='background-color:".$style.";color:#565656;'><td class='numMov'>".$n."</td><td id='impP".$n."'>".$matriz['impiantoP']."</td>";
                           echo "<td><span class='from' id='aziendaP".$n."'>".$matriz['aziendaP']."</span></td>";
                           echo "<td><span style='color:#D84116' class='fromg' id='indirizzoP".$n."'>".$matriz["indirizzo"]."</span></td>";
                           echo "<td><span style='color:#D84116' class='fromg' id='comuneP".$n."'>".$matriz["comune"]."</span></td>";
                           echo "<td><span style='color:#D84116' class='fromg' id='provinciaP".$n."'>".$matriz["provincia"]."</span></td>";
                           echo "<td><span style='color:#D84116' class='fromg' id='nazioneP".$n."'>".$matriz["nazione"]."</span></td>";
                           echo "<td><span style='color:#D84116' class='fromg' id='fulladdressP".$n."'></span></td>";
                           echo "<td><span class='lat' id='Plat".$n."'></span></td>";
                           echo "<td><span style='color:#D84116' class='lon' id='Plon".$n."'></span></td>";
                           echo "<td id='Pstatus".$n."'></td>";
                           echo "<td id='gindirizzoP".$n."'></td>";
                           echo "<td id='gcomuneP".$n."'></td>";
                           echo "<td id='gprovinciaP".$n."'></td>";
                           echo "<td id='gnazioneP".$n."'></td>";
                           echo "</tr>";
                        $n=$n+1;
                    }
                echo "</table>";
                echo "</hr><br/>IMPIANTI DESTINATARI <br/>";
                echo "<div id='getDestinatario'><button>GET Cordinate DESTINATARIO</button></div><br/></hr>";
                echo "<div id='updateDestinatari'><button>UPDATE -- tbl DESTINATARI</button></div><br/></hr>";
                // GET DATI DESTINATARI
                echo "<table id='tbDestinatari'>
                            <tr style='background-color:#565656;color:#A5BFDD;' align='center'>
                                <td>N�</td>
                                <td>ID_UIMD</td>
                                <td>ID_AZD</td>
                                <td>INDIRIZZO</td>
                                <td>COMUNE</td>
                                <td>PROV</td>
                                <td>NAZIONE</td>
                                <td>FULL-address</td>
                                <td>LATITUDE</td>
                                <td>LONGITUDE</td>
                                <td>STATUS</td>
                                <td>gloogeINDIRIZZO</td>
                                <td>gloogleCOMUNE</td>
                                <td>glooPROV</td>
                                <td>gloogleNAZIONE</td>
                            </tr>";

                    $sqlD = "SELECT c.description as comune,c.shdes_prov as provincia,c.nazione,i.description
                    as indirizzo,i.ID_UIMD as impiantoD,i.ID_AZD as aziendaD FROM lov_comuni_istat c
                    JOIN user_impianti_destinatari i ON c.ID_COM = i.ID_COM LIMIT 0,1";
                    $sqlD = $conectarse->query($sqlD);
                    $n=0;
                    while($matriz=$conectarse->fetchArray($sqlD)){
                        ($n%2==0)?$style="#156AAF":$style="#565656";
                           echo "<tr class='mov' style='background-color:".$style.";color:#FFFFFF;'><td class='numMov'>".$n."</td><td id='impD".$n."'>".$matriz['impiantoD']."</td>";

                           echo "<td><span class='from' id='aziendaD".$n."'>".$matriz['aziendaD']."</span></td>";
                           echo "<td><span style='color:#FFFFFF' class='fromg' id='indirizzoD".$n."'>".$matriz["indirizzo"]."</span></td>";
                           echo "<td><span style='color:#FFFFFF' class='fromg' id='comuneD".$n."'>".$matriz["comune"]."</span></td>";
                           echo "<td><span style='color:#FFFFFF' class='fromg' id='provinciaD".$n."'>".$matriz["provincia"]."</span></td>";
                           echo "<td><span style='color:#FFFFFF' class='fromg' id='nazioneD".$n."'>".$matriz["nazione"]."</span></td>"; 
    
                           echo "<td><span style='color:#D84116' class='fromg' id='fulladdressD".$n."'></span></td>";
                           echo "<td><span class='lat' id='Dlat".$n."'></span></td>";
                           echo "<td><span style='color:#D84116' class='lon' id='Dlon".$n."'></span></td>";
                           echo "<td id='Dstatus".$n."'></td>";
                           echo "<td id='gindirizzoD".$n."'></td>";
                           echo "<td id='gcomuneD".$n."'></td>";
                           echo "<td id='gprovinciaD".$n."'></td>";
                           echo "<td id='gnazioneD".$n."'></td>";                                                
                           echo "</tr>";
                        $n=$n+1;
                    }                                        
                echo "</table>";


                echo "</hr><br/>IMPIANTI Trasportatori <br/>";
                echo "<div id='getTrasportatori'><button>GET Cordinate TRASPORTATORI</button></div><br/></hr>";
                echo "<div id='updateTrasportatori'><button>UPDATE -- tbl TRASPORTATORI</button></div><br/></hr>";
                // GET DATI TRASPORTATORI
                echo "<table id='tbTrasportatori'>
                            <tr style='background-color:#565656;color:#A5BFDD;' align='center'>
                                <td>N�</td>
                                <td>ID_UIMT</td>
                                <td>ID_AZT</td>
                                <td>INDIRIZZO</td>
                                <td>COMUNE</td>
                                <td>PROV</td>
                                <td>NAZIONE</td>
                                <td>FULL-address</td>
                                <td>LATITUDE</td>
                                <td>LONGITUDE</td>
                                <td>STATUS</td>
                                <td>gloogeINDIRIZZO</td>
                                <td>gloogleCOMUNE</td>
                                <td>glooPROV</td>
                                <td>gloogleNAZIONE</td>
                            </tr>";

                    $sqlD = "SELECT c.description as comune,c.shdes_prov as provincia,c.nazione,i.description
                    as indirizzo,i.ID_UIMT as impiantoT,i.ID_AZT as aziendaT FROM lov_comuni_istat c
                    JOIN user_impianti_trasportatori i ON c.ID_COM = i.ID_COM AND ID_UIMT>0038229 LIMIT 0,1";
                    $sqlD = $conectarse->query($sqlD);
                    $n=0;
                    while($matriz=$conectarse->fetchArray($sqlD)){
                        ($n%2==0)?$style="#156AAF":$style="#565656";
                           echo "<tr class='mov' style='background-color:".$style.";color:#FFFFFF;'><td class='numMov'>".$n."</td><td id='impT".$n."'>".$matriz['impiantoT']."</td>";

                           echo "<td><span class='from' id='aziendaT".$n."'>".$matriz['aziendaT']."</span></td>";
                           echo "<td><span style='color:#FFFFFF' class='fromg' id='indirizzoT".$n."'>".$matriz["indirizzo"]."</span></td>";
                           echo "<td><span style='color:#FFFFFF' class='fromg' id='comuneT".$n."'>".$matriz["comune"]."</span></td>";
                           echo "<td><span style='color:#FFFFFF' class='fromg' id='provinciaT".$n."'>".$matriz["provincia"]."</span></td>";
                           echo "<td><span style='color:#FFFFFF' class='fromg' id='nazioneT".$n."'>".$matriz["nazione"]."</span></td>";

                           echo "<td><span style='color:#D84116' class='fromg' id='fulladdressT".$n."'></span></td>";
                           echo "<td><span class='lat' id='Tlat".$n."'></span></td>";
                           echo "<td><span style='color:#D84116' class='lon' id='Tlon".$n."'></span></td>";
                           echo "<td id='Tstatus".$n."'></td>";
                           echo "<td id='gindirizzoT".$n."'></td>";
                           echo "<td id='gcomuneT".$n."'></td>";
                           echo "<td id='gprovinciaT".$n."'></td>";
                           echo "<td id='gnazioneT".$n."'></td>";
                           echo "</tr>";
                        $n=$n+1;
                    }
                    echo "</table>";



                echo "</hr><br/>IMPIANTI Intermediari <br/>";
                echo "<div id='getIntermediari'><button>GET Cordinate INTERMEDIARI</button></div><br/></hr>";
                echo "<div id='updateIntermediari'><button>UPDATE -- tbl INTERMEDIARI</button></div><br/></hr>";
                // GET DATI INTERMEDIARI
                echo "<table id='tbIntermediari'>
                            <tr style='background-color:#565656;color:#A5BFDD;' align='center'>
                                <td>N�</td>
                                <td>ID_UIMI</td>
                                <td>ID_AZI</td>
                                <td>INDIRIZZO</td>
                                <td>COMUNE</td>
                                <td>PROV</td>
                                <td>NAZIONE</td>
                                <td>FULL-address</td>
                                <td>LATITUDE</td>
                                <td>LONGITUDE</td>
                                <td>STATUS</td>
                                <td>gloogeINDIRIZZO</td>
                                <td>gloogleCOMUNE</td>
                                <td>glooPROV</td>
                                <td>gloogleNAZIONE</td>
                            </tr>";

                    $sqlD = "SELECT c.description as comune,c.shdes_prov as provincia,c.nazione,i.description
                    as indirizzo,i.ID_UIMI as impiantoI,i.ID_AZI as aziendaI FROM lov_comuni_istat c
                    JOIN user_impianti_intermediari i ON c.ID_COM = i.ID_COM  LIMIT 0,5000";
                    $sqlD = $conectarse->query($sqlD);
                    $n=0;
                    while($matriz=$conectarse->fetchArray($sqlD)){
                        ($n%2==0)?$style="#156AAF":$style="#565656";
                           echo "<tr class='mov' style='background-color:".$style.";color:#FFFFFF;'><td class='numMov'>".$n."</td><td id='impI".$n."'>".$matriz['impiantoI']."</td>";

                           echo "<td><span class='from' id='aziendaI".$n."'>".$matriz['aziendaI']."</span></td>";
                           echo "<td><span style='color:#FFFFFF' class='fromg' id='indirizzoI".$n."'>".$matriz["indirizzo"]."</span></td>";
                           echo "<td><span style='color:#FFFFFF' class='fromg' id='comuneI".$n."'>".$matriz["comune"]."</span></td>";
                           echo "<td><span style='color:#FFFFFF' class='fromg' id='provinciaI".$n."'>".$matriz["provincia"]."</span></td>";
                           echo "<td><span style='color:#FFFFFF' class='fromg' id='nazioneI".$n."'>".$matriz["nazione"]."</span></td>";

                           echo "<td><span style='color:#D84116' class='fromg' id='fulladdressI".$n."'></span></td>";
                           echo "<td><span class='lat' id='Ilat".$n."'></span></td>";
                           echo "<td><span style='color:#D84116' class='lon' id='Ilon".$n."'></span></td>";
                           echo "<td id='Istatus".$n."'></td>";
                           echo "<td id='gindirizzoI".$n."'></td>";
                           echo "<td id='gcomuneI".$n."'></td>";
                           echo "<td id='gprovinciaI".$n."'></td>";
                           echo "<td id='gnazioneI".$n."'></td>";
                           echo "</tr>";
                        $n=$n+1;
                    }
                    echo "</table>";


?>

    </body>
</html>
