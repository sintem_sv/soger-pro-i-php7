<?php
require_once("vg_movimenti.php");

class Viaggio extends Movimenti
{

        var $ID_VIAGGIO;
        var $durata;
        var $distanza;
        var $ID_MZ_TRA;
        var $costo_gasolio;
        var $costo_autostrada;
        var $costo_rc;
        var $costo_bra;
        var $costo_conducente;
        var $costo_usura;
        var $costo_generale;
        var $costo_totale;
        var $costo_calcolato;
        var $costo_correttivo;        


        function __construct($year, $matriz, $idmov, $tipoMov, $mz){
                    $conn = parent::__construct($year, $matriz, $idmov, $tipoMov);
                    $this->ID_MZ_TRA = $mz;
                    }
       
        function createViaggio(){            
                    $sql = "INSERT INTO `user_viaggi` (`ID_MZ_TRA`) VALUES (".$this->ID_MZ_TRA.")";
                    $sql = DbConnector::query($sql);                    
                    //die(var_dump($sql));
                    $this->ID_VIAGGIO = mysqli_insert_id( DbConnector::getLink() );
                    return $this->ID_VIAGGIO;
        }


        function setViaggioByID($idv){
                    $row = $this->getViaggioByID($idv);
                    $this->ID_VIAGGIO   = $row['ID_VIAGGIO'];
                    $this->durata       = $row['durata'];
                    $this->distanza     = $row['distanza'];
                    $this->ID_MZ_TRA    = $row['ID_MZ_TRA'];
                    $this->costo_gasolio    = $row['costo_gasolio'];
                    $this->costo_autostrada = $row['costo_autostrada'];
                    $this->costo_rc         = $row['costo_rc'];
                    $this->costo_bra        = $row['costo_bra'];
                    $this->costo_conducente = $row['costo_conducente'];
                    $this->costo_usura      = $row['costo_usura'];
                    $this->costo_generale   = $row['costo_generale'];
                    $this->costo_totale     = $row['costo_totale'];
                    $this->costo_calcolato  = $row['costo_calcolato'];
                    $this->costo_correttivo = $row['costo_correttivo'];
        }




        function setTappeAndPercorsoByConferma($idViaggio,$tappe,$da,$a){

            if($this->deleteViaggiTappaByIDV($idViaggio)==true)
            $sql=  "INSERT into `user_viaggi_tappe`";
            $sql.= "(`ID_VIAGGIO`,`indirizzo`,`indirizzo_google`,`ID_TIPO_TAPPA`)";
            $sql.= " values (".$idViaggio.",'".addslashes($this->indirizzoP)."','".$da."',1);";
            DbConnector::query($sql);

            for($i=0;$i<count($tappe); $i++){
                    $sqlt =  "INSERT into `user_viaggi_tappe`";
                    $sqlt.=  "(`ID_VIAGGIO`,`indirizzo`,`indirizzo_google`,`ID_TIPO_TAPPA`)";
                    $sqlt.=  "values (".$idViaggio.",'".$tappe[$i]."','".$tappe[$i]."',2);";
                    DbConnector::query($sqlt);
                    }
                       
            $sqld=  "INSERT into `user_viaggi_tappe`";
            $sqld.=  "(`ID_VIAGGIO`,`indirizzo`,`indirizzo_google`,`ID_TIPO_TAPPA`)";
            $sqld.=  "values (".$idViaggio.",'".addslashes($this->indirizzoD)."','".$a."',3);";            
            DbConnector::query($sqld);       
        }

        function getTappeDetailsByIdViaggio($id){
                 $return = array();
                 $sql = "SELECT *FROM user_viaggi_tappe_details where ID_VIAGGIO = ".$id;
                 $sql = parent::query($sql);
                 while($row =  parent::fetchArray($sql)){
                        array_push($return, $row);
                        }
                 return $return;
                  
        }



        function updateTappaDetailsByResumeRoute($idv,$resumeRoute){
                       
        for($r=0;$r<count($resumeRoute);$r++){
                $sql = "INSERT into user_viaggi_tappe_details (`ID_VIAGGIO`,`da`,`a`,`durata`,`distanza`,`costo`)
                                    VALUE(".$idv.",'".$resumeRoute[$r]['partenza']."','".$resumeRoute[$r]['destino']."',".$resumeRoute[$r]['tempo'].",".$resumeRoute[$r]['distance'].",".$resumeRoute[$r]['costo'].")";
                DbConnector::query($sql);
                }            
        }


        function saveViaggio(){

            $sql = "UPDATE  `user_viaggi` set
                            `durata` =".$this->durata.",
                            `distanza` =".$this->distanza.",
                            `ID_MZ_TRA` =".$this->ID_MZ_TRA.",
                            `costo_gasolio` =".$this->costo_gasolio.",
                            `costo_autostrada` =".$this->costo_autostrada.",
                            `costo_rc` =".$this->costo_rc.",
                            `costo_bra` =".$this->costo_bra.",
                            `costo_conducente` =".$this->costo_conducente.",
                            `costo_usura` =".$this->costo_usura.",
                            `costo_generale` =".$this->costo_generale.",
                            `costo_totale` =".$this->costo_totale.",
                            `costo_calcolato` =".$this->costo_calcolato.",
                            `costo_correttivo` =".$this->costo_correttivo."
                             WHERE id_viaggio =".$this->ID_VIAGGIO;

            DbConnector::query($sql);
        }


        function getViaggioByID($id){
                    $sql   = "SELECT *FROM user_viaggi WHERE ID_VIAGGIO = ".$id;
                    $sql   = DbConnector::query($sql);
                    $row   = DbConnector::fetchArray($sql);
                    return $row;
                    }


        private function createTappeFromByIdViaggio($id){ // create tappe partenza
            $from  = parent::getProduttore(); // array (status,result,coordinate(array(latitude,longitude)),default)
            if($from['status']=='OK'){
                $sql="INSERT into `user_viaggi_tappe`
                (`ID_VIAGGIO`, `indirizzo`, `comune`, `provincia`, `cap`,
                `nazione`,`indirizzo_google`,`ID_TIPO_TAPPA`)
                 values (".$id.",'".addslashes($this->indirizzoP)."','".addslashes($this->com_prod)."','".addslashes($this->regione_prod)."','".addslashes($this->CAP_prod)."','".addslashes($this->nazione_prod)."','".addslashes($from['result'])."',1)";
                 } else {
                            $sql="INSERT into `user_viaggi_tappe`
                            (`ID_VIAGGIO`, `indirizzo`, `comune`, `provincia`, `cap`,
                            `nazione`,`indirizzo_google`,`ID_TIPO_TAPPA`)
                             values (".$id.",'".addslashes($this->indirizzoP)."','".addslashes($this->com_prod)."','".addslashes($this->regione_prod)."','".addslashes($this->CAP_prod)."','".addslashes($this->nazione_prod)."','No trovato',1)";
                             }

              parent::query($sql);
            }

        private function createTappeToByIdViaggio($id){ // create tappe destino
            $to  = parent::getDestinatario(); // array (status,result,coordinate(array(latitude,longitude)),default)
            if($to['status']=='OK'){
                $sql="INSERT into `user_viaggi_tappe`
                (`ID_VIAGGIO`, `indirizzo`, `comune`, `provincia`, `cap`,
                `nazione`,`indirizzo_google`,`ID_TIPO_TAPPA`)
                 values (".$id.",'".addslashes($this->indirizzoD)."','".addslashes($this->com_dest)."','".addslashes($this->regione_dest)."','".addslashes($this->CAP_dest)."','".addslashes($this->nazione_dest)."','".addslashes($to['result'])."',3)";
                 } else {
                            $sql="INSERT into `user_viaggi_tappe`
                            (`ID_VIAGGIO`, `indirizzo`, `comune`, `provincia`, `cap`,
                            `nazione`,`indirizzo_google`,`ID_TIPO_TAPPA`)
                             values (".$id.",'".addslashes($this->indirizzoD)."','".addslashes($this->com_dest)."','".addslashes($this->regione_dest)."','".addslashes($this->CAP_dest)."','".addslashes($this->nazione_dest)."','No trovato',3)";
                             }

            parent::query($sql);                 
            }

         private function createTappeIntermedieByIdViaggio($id){                
                $t = Movimenti::getTappeDetailsByApi();

                for($i=0;$i<count($t); $i++){
                            if($t[$i]['status']=='OK'){
                                    $sql="INSERT into `user_viaggi_tappe`
                                    (`ID_VIAGGIO`,`indirizzo`,`indirizzo_google`,`ID_TIPO_TAPPA`)
                                     values (".$id.",'".addslashes($t[$i]['default'])."','".addslashes($t[$i]['results']['address'])."',2)";
                                    DbConnector::query($sql);
                                    }else{
                                            $sql="INSERT into `user_viaggi_tappe`
                                            (`ID_VIAGGIO`,`indirizzo`,`indirizzo_google`,`ID_TIPO_TAPPA`)
                                             values (".$id.",'".addslashes($t[$i]['default'])."','non trovato',2)";
                                            DbConnector::query($sql);
                                            }
                }

                
                
         }

         
         public function createAllTappeByIdViaggio($id){

             $this->createTappeFromByIdViaggio($id);
             $this->createTappeIntermedieByIdViaggio($id);
             $this->createTappeToByIdViaggio($id);
         }

         public function getTappeOrdinateFromViaggiTappeByIdViaggio($id){
                 $return = array();
                 $sql = "SELECT *FROM user_viaggi_tappe where ID_VIAGGIO = ".$id." ORDER BY ID_TIPO_TAPPA,ID_TAPPA";
                 $sql = DbConnector::query($sql);
                 while($row = DbConnector::fetchArray($sql)){
                        array_push($return,$row);
                        }
                 return $return;
         }

         function getCostoDefaultByMz($mz){
             $sql = "SELECT *FROM `lov_costi_trasposto` WHERE ID_MZ_TRA = ".$this->ID_MZ_TRA;             
             $sql = parent::query($sql);
             return parent::fetchArray($sql);             
         }

         function getCostoDefaultByImpianto(){
                  $sql = "SELECT *FROM `user_viaggi_impianti` WHERE ID_IMP= '".$this->ID_IMP."' AND ID_MZ_TRA = ".$this->ID_MZ_TRA;                                    
                                      
                  if((parent::query($sql)==true) && (parent::getNumRows($sql)>0)){// && parent::getNumRows($sql)>0){
                        return parent::fetchArray(parent::query($sql));
                        }else{
                                return $this->getCostoDefaultByMz($this->ID_MZ_TRA);
                                }                  
         }

         function setCostoViaggiDefault(){
                    $costo = $this->getCostoDefaultByImpianto();                    
                    $this->costo_gasolio    = $costo['costo_gasolio'];
                    $this->costo_autostrada = $costo['costo_autostrada'];
                    $this->costo_rc         = $costo['costo_rc'];
                    $this->costo_bra        = $costo['costo_bra'];
                    $this->costo_conducente = $costo['costo_conducente'];
                    $this->costo_usura      = $costo['costo_usura'];
                    $this->costo_generale   = $costo['costo_generale'];
                    $this->costo_correttivo = $costo['costo_correttivo'];
                    // set Costo Total
                    $costoTotale  = $this->costo_gasolio;
                    $costoTotale += $this->costo_autostrada;
                    $costoTotale += $this->costo_rc;
                    $costoTotale +=$this->costo_bra;
                    $costoTotale +=$this->costo_conducente;
                    $costoTotale +=$this->costo_usura;
                    $costoTotale +=$this->costo_generale;
                    $this->costo_totale = $costoTotale;

             }
            function updatePercorsOriginale($idv){
                    
            $clean  = "UPDATE ".$this->tabellaMov." SET percorso = '' WHERE ".$this->tipoMov." = ".$this->mov;
            DbConnector::query($clean);            
            $tappe = $this->getTappeIntermedieByIDV($idv);
            for($i=0;$i<count($tappe); $i++){

                    if ($i == 0 ){
                            $sql  =   "UPDATE `".$this->tabellaMov."` SET `percorso` = CONCAT(percorso,'".$tappe[$i]['indirizzo']."') where ".$this->tipoMov." =  ".$this->mov.";";
                            }else{
                                    $sql  =   "UPDATE `".$this->tabellaMov."` SET `percorso` = CONCAT(percorso,'-".$tappe[$i]['indirizzo']."') where ".$this->tipoMov." =  ".$this->mov.";";
                                    }
                                
                    $sql   = DbConnector::query($sql);
                    }                    
            }




            function getTappeIntermedieByIDV($idv){
                    $return = array();
                    $sql = "SELECT  indirizzo
                                    FROM `user_viaggi_tappe`
                                    WHERE ID_TIPO_TAPPA = 2 AND ID_VIAGGIO = ".$idv." ORDER BY ID_TAPPA";
                    $sql = DbConnector::query($sql);
                    while($row = DbConnector::fetchArray($sql)){
                        array_push($return, $row);
                        }
                    return $return;
            }

             function updateCostoViaggioDefault(){
                    $this->setCostoViaggiDefault();

                    $sql = "UPDATE  `user_viaggi` set
                                    `costo_gasolio` =".$this->costo_gasolio.",
                                    `costo_autostrada` =".$this->costo_autostrada.",
                                    `costo_rc` =".$this->costo_rc.",
                                    `costo_bra` =".$this->costo_bra.",
                                    `costo_conducente` =".$this->costo_conducente.",
                                    `costo_usura` =".$this->costo_usura.",
                                    `costo_generale` =".$this->costo_generale.",
                                    `costo_totale` =".$this->costo_totale.",
                                    `costo_correttivo` =".$this->costo_correttivo."
                                     WHERE id_viaggio =".$this->ID_VIAGGIO;

                  return DbConnector::query($sql);

             }

          function updateIndirizzoGloogleTVByIDT($idt,$adrGloogle){
                    $sql = "UPDATE `user_viaggi_tappe`
                                    SET `indirizzo_google` = '".addslashes($adrGloogle)."'
                                         WHERE `user_viaggi_tappe`.`ID_TAPPA` = ".$idt;
                   return parent::query($sql);
          }

          function getViaggioTappeByIDT($idt){
                    $sql = "SELECT *
                                    FROM `user_viaggi_tappe`ID_TAPPA
                                    WHERE ID_TAPPA = ".$idt;                    
                    return parent::fetchArray(parent::query($sql));
          }
          function getTipoTappeByIDT($idt){
                    $sql = "SELECT ID_TIPO_TAPPA
                                    FROM `user_viaggi_tappe`ID_TAPPA
                                    WHERE ID_TAPPA = ".$idt;
                    $tipo = parent::fetchArray(parent::query($sql));
                    return $tipo['ID_TIPO_TAPPA'];
                    }
          function getIndirizziGloogleFromTappeByTipoTappeByIDV($tipoTappe,$idViaggio){
                    $return = array();
                    $sql = "SELECT  *
                                    FROM `user_viaggi_tappe`
                                    WHERE ID_TIPO_TAPPA = ".$tipoTappe." AND ID_VIAGGIO = ".$idViaggio." ORDER BY ID_TAPPA";
                    $sql = DbConnector::query($sql);
                    while($row = DbConnector::fetchArray($sql)){
                        array_push($return, $row);
                        }
                    return $return;

          }

    function deleteViaggiByIDV($idv){
            $sql = "DELETE FROM `user_viaggi` WHERE `user_viaggi`.`ID_VIAGGIO` = ".$idv;
            return DbConnector::query($sql);    
    }

    function deleteViaggiTappaByIDV($idv){
            $sql= "DELETE FROM `user_viaggi_tappe` WHERE `ID_VIAGGIO`=".$idv.";";
            return DbConnector::query($sql);
    }

    function deleteViaggiTappaDetailsByIDV($idv){
            $sql = "DELETE FROM `user_viaggi_tappe_details` WHERE `ID_VIAGGIO` = ".$idv;
            return DbConnector::query($sql);
    }


    function cancellaViaggioByIDV($idv){
            $this->deleteViaggiByIDV($idv);
            $this->deleteViaggiTappaByIDV($idv);
            $this->deleteViaggiTappaDetailsByIDV($idv);
    }

    function updatePercorsoFromTappeAndIDV($tappe,$idv){

            $clean  = "UPDATE ".$this->tabellaMov." SET percorso = '' WHERE ".$this->tipoMov." = ".$this->mov;
            DbConnector::query($clean);
            for($i=0;$i<count($tappe); $i++){
                    if ($i == 0 ){
                            $sql  =   "UPDATE `".$this->tabellaMov."` SET `percorso` = CONCAT(percorso,'".$tappe[$i]."') where ".$this->tipoMov." =  ".$this->mov.";";
                            }else{
                                    $sql  =   "UPDATE `".$this->tabellaMov."` SET `percorso` = CONCAT(percorso,'-".$tappe[$i]."') where ".$this->tipoMov." =  ".$this->mov.";";
                                    }                                
                    $sql   = DbConnector::query($sql);
                    }

    }

    function updatePercorsoFromTblTappeByIDV($idv){  // UPDATE CAMPO PERCORSO BY USER_VIAGGI_TAPPE

            $clean  = "UPDATE ".$this->tabellaMov." SET percorso = '' WHERE ".$this->tipoMov." = ".$this->mov;
            DbConnector::query($clean);
            $tappe = $this->getIndirizziGloogleFromTappeByTipoTappeByIDV(2, $idv);
            for($i=0;$i<count($tappe); $i++){

                    if ($i == 0 ){
                            $sql  =  ($tappe[$i]['indirizzo_google']!='non trovato')? "UPDATE `".$this->tabellaMov."` SET `percorso` = CONCAT(percorso,'".$tappe[$i]['indirizzo_google']."') where ".$this->tipoMov." =  ".$this->mov.";":
                                                                                      "UPDATE `".$this->tabellaMov."` SET `percorso` = CONCAT(percorso,'".$tappe[$i]['indirizzo']."') where ".$this->tipoMov." =  ".$this->mov.";";
                             
                            }else{
                                    $sql  =  ($tappe[$i]['indirizzo_google']!='non trovato')? "UPDATE `".$this->tabellaMov."` SET `percorso` = CONCAT(percorso,'-".$tappe[$i]['indirizzo_google']."') where ".$this->tipoMov." =  ".$this->mov.";" :
                                                                                              "UPDATE `".$this->tabellaMov."` SET `percorso` = CONCAT(percorso,'-".$tappe[$i]['indirizzo']."') where ".$this->tipoMov." =  ".$this->mov.";";
                                    }
                                
                    $sql   = DbConnector::query($sql);
                    }
   }


   
}


?>
