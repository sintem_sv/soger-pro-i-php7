<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
 <HEAD>
  <TITLE> OFFERTA CLIENTI ARS </TITLE>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <META NAME="Generator" CONTENT="EditPlus">
  <META NAME="Author" CONTENT="">
  <META NAME="Keywords" CONTENT="">
  <META NAME="Description" CONTENT="">
  <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="ars60.js"></script>
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-41708282-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

 </HEAD>

 <BODY style="background-color:#dce9be;margin:0;padding:0;">


	<div id="external" style="border-radius: 15px; -moz-border-radius: 15px; border: 2px solid #b4be9b;width:900px;margin-top:60px;left:50%;position: absolute;margin-left:-450px;">

		<img src="ars60.gif" alt="SDS GRATIS 60 GIORNI" title="SDS GRATIS 60 GIORNI" style="position:absolute;margin:-60px;" />

		<div id="top" style="margin-top:2px;margin-right:2px;margin-left:2px;background-color:#473c4d;border-top-left-radius:13px; border-top-right-radius:13px;height:160px;background-image:url('sds_logo.png');background-repeat:no-repeat;background-position:200px 20px;">
		
			<span style="display: inline-block;margin-top:90px;margin-left:200px;color:#fff;font-family: Arial,Tahoma,Verdana,sans-serif;font-size: 20px;font-weight: normal;letter-spacing: -0.05em;">
			La gestione delle Schede di Sicurezza per la valutazione del Rischio Chimico
			</span>

		</div>

		<div id="bottom" style="margin-bottom:2px;margin-right:2px;margin-left:2px;background-color:#fff;border-bottom-left-radius:13px; border-bottom-right-radius:13px;height:650px;position:relative;">

			<div>
				<div style="float:left;width:30%;">
	
				<div>
					<h2 style="color:#8A3C00;padding-left:10px;font-family: Arial,Tahoma,Verdana,sans-serif;">Scopri SDS Archive</h2>

					<p style="padding-left:10px;padding-right:25px;font-family: Arial,Tahoma,Verdana,sans-serif; font-size: 12px;">
					Con <b>SDS Archive</b> gestisci facilmente le Schede di Sicurezza dei prodotti usati in Azienda, ne verifichi la conformità e la rispondenza alle normative vigenti e raccogli i dati per la valutazione del Rischio Chimico.
					
					<br /><br />
                                        <div style="font-size: 12px; font-family: Arial,Tahoma,Verdana,sans-serif; padding: 15px; background-color: #F6BA5B">
					Maggiori informazioni sul software <b>SDS Archive</b> sono disponibili sul sito</p>
                                        </div>
                                        <a href="http://www.sdsarchive.it" target="_blank" style="text-decoration:none;color:#8A3C00;">
					<div style="background-color: #E2A22F; text-align: center; padding:15px;font-family: Arial,Tahoma,Verdana,sans-serif; font-size: 16px;">					
                                            <b>www.sdsarchive.it</b>
					</div>
                                        </a>
				</div>
				
				<div>
					<h2 style="color:#8A3C00;padding-left:10px;font-family: Arial,Tahoma,Verdana,sans-serif;">Sei Cliente ARS?</h2>

					<p style="padding-left:10px;padding-right:25px;font-family: Arial,Tahoma,Verdana,sans-serif; font-size: 12px;">
					In esclusiva per i lettori della newsletter di ARS Edizioni Informatiche, proponiamo un <b>abbonamento gratuito</b> della durata di 60 giorni per l'utilizzo del software <b>SDS Archive</b>.
					</p>
				</div>
				
				</div>

				<div id="MainContent" style="float:right;width:70%;">
				<h2 style="color:#8A3C00;padding-left:10px;font-family: Arial,Tahoma,Verdana,sans-serif;">Richiedi 60 giorni di prova gratuita</h2>

				<p id="IntroText" style="padding-left:10px;padding-right:15px;font-family: Arial,Tahoma,Verdana,sans-serif; font-size: 12px;">
				Compila in tutti i suoi campi il modulo sottostante, entro 24 ore lavorative verrai contattato da SINTEM S.R.L. che ti fornirà le credenziali di accesso gratuito a <b>SDS Archive</b> per 60 giorni.<br /><br />

				Problemi con la registrazione? Contattaci all'indirizzo mail <a href="mailto:amministrazione@sintem.it" style="text-decoration:none;color:#8A3C00;">amministrazione@sintem.it</a>
				</p>
				
				<form style="margin-top:25px;" id="FormRichiesta">
					<fieldset style="position:relative;background-color:#DCE9BE;border: 1px solid #000;margin-right:10px;padding-top:20px;padding-bottom:30px;">
						
						<legend style="position:absolute;top: -.5em;left: .5em;border:1px solid #000;background-color:#fff;font-family: Arial,Tahoma,Verdana,sans-serif;font-size:12px;font-weight:bold;"><span style="display: inline-block;padding:3px 20px;width:200px;">DATI AZIENDA</span></legend>
						
						<div style="margin-bottom:5px;margin-top:30px;">
							<label style="margin-left:30px;margin-right:1em;font-family: Arial,Tahoma,Verdana,sans-serif;font-size:11px;display:inline-block;width:150px;text-align:right;font-weight:bold;">Ragione sociale:</label><input id="Ragione_sociale" name="Ragione_Sociale" type="text" style="width:300px;">
						</div>

						<div style="margin-bottom:5px;">
							<label style="margin-left:30px;margin-right:1em;font-family: Arial,Tahoma,Verdana,sans-serif;font-size:11px;display:inline-block;width:150px;text-align:right;font-weight:bold;">Codice fiscale: </label><input id="Codice_fiscale" name="Codice_fiscale" type="text" style="width:300px;">
						</div>

						<div>
							<label style="margin-left:30px;margin-right:1em;font-family: Arial,Tahoma,Verdana,sans-serif;font-size:11px;display:inline-block;width:150px;text-align:right;font-weight:bold;">Indirizzo stabilimento: </label><input id="Indirizzo_stabilimento" name="Indirizzo_stabilimento" type="text" style="width:300px;">
						</div>
					
					</fieldset>

					<fieldset style="position:relative;background-color:#DCE9BE;border: 1px solid #000;margin-top:30px;margin-right:10px;padding-top:20px;padding-bottom:30px;">
						
						<legend style="position:absolute;top: -.5em;left: .5em;border:1px solid #000;background-color:#fff;font-family: Arial,Tahoma,Verdana,sans-serif;font-size:12px;font-weight:bold;">
						<span style="display: inline-block;padding:3px 20px;width:200px;">PERSONA DI RIFERIMENTO</span></legend>
						
						<div style="margin-bottom:5px;margin-top:30px;">
							<label style="margin-left:30px;margin-right:1em;font-family: Arial,Tahoma,Verdana,sans-serif;font-size:11px;display:inline-block;width:150px;text-align:right;font-weight:bold;">Nominativo: </label><input type="text" id="Nominativo" name="Nominativo" style="width:300px;">
						</div>

						<div style="margin-bottom:5px;">
							<label style="margin-left:30px;margin-right:1em;font-family: Arial,Tahoma,Verdana,sans-serif;font-size:11px;display:inline-block;width:150px;text-align:right;font-weight:bold;">Telefono: </label><input type="text" id="Telefono" name="Telefono" style="width:300px;">
						</div>

						<div>
							<label style="margin-left:30px;margin-right:1em;font-family: Arial,Tahoma,Verdana,sans-serif;font-size:11px;display:inline-block;width:150px;text-align:right;font-weight:bold;">Email: </label><input type="text" id="Email" name="Email" style="width:300px;">
						</div>
					</fieldset>

					
					<div style="text-align:right;padding-top:25px;">
						<input id="SubmitButton" style="margin-right:10px;cursor:pointer;border:1px solid #000; color:#000; background-color:#DCE9BE;padding:8px 20px;" type="button" value="Invia richiesta" />
					</div>


				</form>
				
				</div>


			</div>

			<div id="footer" style="clear:both;border-top:1px solid #dddedc;position:absolute;bottom:0;width:100%;text-align:center;font-family: Arial,Tahoma,Verdana,sans-serif; font-size: 11px;padding:10px;">
				<img src="mini_sintem.png" alt="SINTEM S.R.L." title="SINTEM S.R.L." /><br /><br />
				<b>SDS Archive</b> è un prodotto 
				<a href="http://www.sintem.it" target="_blank" style="text-decoration:none;color:#8A3C00;">SINTEM S.R.L.</a>
				- Tutti i diritti riservati

			</div>

		</div>

	</div>
  
 </BODY>
</HTML>
