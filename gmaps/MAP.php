<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php require_once("head.php"); ?>
<script type="text/javascript" src="js/map3.js"></script>
<script type="text/javascript" src="js/jquery.ui.autocomplete.js"></script>
<style>
    .btnRefresh{
    cursor: pointer;
    margin: 1px;
    padding: 4px 0;
    float: left;position: relative;}
    .btnAggiornaDefault{float: left;
    margin: 0 4px;}
</style>
</head>
  <body onload="initialize()">

    <div class="containerMap">
		<div class="cols">
			<div class="lx">
				<div class="boxmap ui-corner-all">
					<div id="map" class="imgMap"></div>
				</div>
				<div class="tapa ui-corner-all">

					<div class="tbTitolo" style="margin-left:20px;margin-top:20px;">Indicare punto di partenza e di arrivo</div>
					<form action="#" name="form_ruta" onsubmit="obtenerRuta(this.desde.value, this.hasta.value); return false">
						<table id="TappeTable">
							<tr>
								<td>Da</td>
								<td><input readonly="readonly"  class="ui-corner-all" type="text" name="partenza" id="da" value="" <?php if(isset($_GET['da'])) echo " disabled "; ?>/></td>
								<td>A</td>
								<td><input readonly="readonly"  class="ui-corner-all" type="text" name="arrivo" id="a" value="" <?php if(isset($_GET['a'])) echo " disabled "; ?>/></td>
                                                                <td><div id="btnGetpercorso" class="ui-state-default ui-corner-all ui-button btn" onclick="ricalcola()">&nbsp;Verifica&nbsp;</div></td>
							</tr>
							<tr>
								<td colspan="3">Aggiungi una tappa intermedia</td>
								<td><input class="ui-corner-all" id="addtappa" type="text"/></td>
								<td><div id="BtnAddtappa" class="ui-button ui-corner-all ui-state-default btn" onclick="Addtappa()">Aggiungi</div></td>
							</tr>
						</table>
					</form>

				</div>

			</div>

			<div class="rx">

				<div class="tab_costi ui-corner-all">
                                    <div class="titoloTab" id="testa">COSTI KILOMETRICI</div>
					<table id="costi">
						<tr>
							<th style="color:#F1F264;">Descrizione</th>
							<th class="titoloAutocarro" style="color:#F1F264;width:60px;text-align:center;">Autocarro</th>
							<th class="titoloFurgone" style="color:#F1F264;width:60px;text-align:center;">Furgone</th>
						</tr>
						<tr>
							<td class="white">Gasolio</td>
                                                        <td align="center"><input class="carro" type="text" value="" id="costo_gasolio_c"/></td>
							<td align="center"><input class="furgone" type="text" value="" id="costo_gasolio_f"/></td>

						</tr>
						<tr>
							<td class="white">Autostrada</td>
							<td align="center"><input class="carro" type="text" value="" id="costo_autostrada_c"/></td>
							<td align="center"><input class="furgone" type="text" value="" id="costo_autostrada_f"/></td>

						</tr>
						<tr>
							<td class="white">Assicurazione RCA</td>
							<td align="center"><input class="carro" type="text" value="" id="costo_rc_c"/></td>
							<td align="center"><input class="furgone" type="text" value="" id="costo_rc_f"/></td>

						</tr>
						<tr>
							<td class="white">Bollo, revisione, etc.</td>
							<td align="center"><input class="carro" type="text" value="" id="costo_bra_c"/></td>
							<td align="center"><input class="furgone" type="text" value="" id="costo_bra_f"/></td>

						</tr>
						<tr>
							<td class="white">Salario conducente</td>
							<td align="center"><input class="carro" type="text" value="" id="costo_conducente_c"/></td>
							<td align="center"><input class="furgone" type="text" value="" id="costo_conducente_f"/></td>

						</tr>
						<tr>
							<td class="white">Manutenzione</td>
							<td align="center"><input class="carro" type="text" value="" id="costo_usura_c"/></td>
							<td align="center"><input class="furgone" type="text" value="" id="costo_usura_f"/></td>

						</tr>
						<tr>
							<td class="white">Spese generali</td>
							<td align="center"><input class="carro" type="text" value="" id="costo_generale_c"/></td>
							<td align="center"><input class="furgone" type="text" value="" id="costo_generale_f"/></td>

						</tr>
						<tr>
							<td class="tbtitolo"><p>TOTALE costo kilometrico</p></td>
							<td align="center" id="rptaCarro" class="rspta"></td>
							<td align="center" id="rptaFurgone"class="rspta"></td>

						</tr>
					</table>
                                <div style="margin-top: 20px">
                                        <span id="calcolaCosto" class="ui-state-default ui-corner-all ui-button btn" onclick="getCosto()">Aggiorna costo</span>
                                </div>
				</div>


				<div class="infoCalcolo ui-corner-all">
                                    <table>
                                        <thead><div style="font-weight:bold;color:#F1F264;margin-top:5px;">Costo vivo del viaggio stimato<span id="tipo"></span></div></thead>
                                        <tbody>
                                            <tr>
                                                <td>Kilometri Percorsi (<span class="orange"> km</span>)</td>
                                                <td><span class="white"><span style="float: right;"id="totdistance"></span></span></td>
                                            </tr>
                                            <tr>
                                                <td>Costo Kilometrico (<span class="orange"> &euro; </span>)</td>
                                                <td><span style="float: right;" id="costBase" class="white"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Costo Totale (<span class="orange"> &euro; </span>)</td>
                                                <td><span style="float: right;" class="white" id="totalCosto"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Correttivo Viaggio (<span class="orange"> &euro; </span>)</td>
                                                <td><span style="float: right;" class="white" id="correttivoViaggio"></span></td>
                                            </tr>
                                            <tr>
                                                <td> TOTALE (<span class="orange"> &euro; </span>) </td>
                                                <td><span class="orange" style="font-size:large; float: right;" id="totPrice"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>										                                         
				</div>

				<?php if(!isset($_GET['ID_MZ_TRA'])){ ?>
				<div class="confCost">
					<p>Seleziona la tipologia di mezzo impiegato</p>
					<div id="radio">
						<input type="radio" id="radio1" name="radio" onclick="enableInput(1)" /><label for="radio1">Autocarro</label>
						<input type="radio" id="radio2" name="radio" onclick="enableInput(2)" /><label for="radio2">Furgone</label>
					</div>
				</div>
				<?php } ?>
			</div>
    </div>

    <div>
            <div class="infoPercorso ui-corner-all">
<!--                Info e gestione delle tappe                 -->
                <div class="gestioneTappe">
                    <div class="tbTitolo" style="margin-bottom:20px;">Dettaglio del percorso pianificato</div>
                    <div id="direcciones"></div>
                    <div>
                            <span class="white">Durata: </span><span class="blue" id="infotime"></span><br/>
                            <span class="white">Distanza: </span><span id="getDistance" class="blue" id="infokm">km</span>
                    </div>
                    <div>
                            <span class="white">Luogo produzione rifiuto: </span><br/><span class="blue" id="streetProd"></span>
                    </div>
                    <table class="detailTappa">

                    </table>
                    <div>
                        <span class="white">Impianto di smaltimento: </span><br/><span class="blue" id="streetRicup"></span>
                    </div>
                    <p>&nbsp;</p>
                    <span id="confermaRoute" class="ui-state-default ui-corner-all ui-button btn" onclick="confermatappe()">Conferma Tappe</span>&nbsp;&nbsp;
 <!--               <span id="removeRoute" class="ui-state-default ui-corner-all ui-button btn" onclick="cancellaTappa()">Cancella Tappe</span>-->
                </div>

<!--                Info delle Rute                 -->
                   <div class="detailRoute">
                        <table id="tableRoute">
                            <tr class="tbTitolo">
<!--                            <td class="ruta">Ruta</td>-->
                                <td class="ruta">Da</td>
                                <td class="ruta">A</td>
                                <td class="ruta">Distanza(<span class="orange">km</span>)</td>
                                <td class="ruta">Tempo</td>
<!--                                <td class="costoRuta ruta">Costo</td>-->
                            </tr>
                        </table>
                    </div>
                <div id="detaglioTappe" class="ui-state-default ui-button btn ui-corner-all" onclick="detaglioTappe()">Visualiza descrizione delle Tappe</div>
                
<!--                 SEE POPUP
                <div id="dialog-confirm" title="Scegli l' indirizzo corretto">
                        <div class="seeIndirizzi">
                            <div id="elencoIndirizzi">
                                <table id="ScegliIndirizzo">
                                    <tr class="orange">
                                        <td>Indirizzo fornitore</td>
                                        <td>Indirizzo Google Maps</td>
                                    </tr>

                                </table>
                            </div>

                        </div>
                </div>-->
<!--                 Popup default
                <div id="correzioneDialog" title="Indirizzi API">
                    <table style="margin-top: 20px ">
                        <tr>
                            <td>Indirizzo del PRODUTTORE : </td>
                            <td><span style="color: #D0622B" id="ind_p_default"></span></td>
                            <td><span id="escludeP">Ignora INDIRIZZO</span></td>
                        </tr>
                        <tr>
                            <td>Gloogle Maps ha trovato :</td>
                            <td><select id="cboxda"></select></td>
                            <td><div class="btnRefresh ui-state-hover ui-corner-all"><span id="aggProd" class='btnAggiornaDefault ui-icon ui-icon-arrowrefresh-1-e'></span></div></td>
                        </tr>
                        <tr><td colspan="2" style="text-align:center; color: #A6C9E2"><br/><hr/><br/></td></tr>
                        <tr>
                            <td>Indirizzo del DESTINATARIO : </td>
                            <td><span style="color: #D0622B" id="ind_d_default"></span></td>
                            <td><span id="escludeD">Ignora INDIRIZZO</span></td>
                        </tr>
                        <tr>
                            <td>Gloogle Maps ha trovato :</td>
                            <td><select id="cboxa"></select></td>
                            <td><div class="btnRefresh ui-state-hover ui-corner-all"><span id="aggDest" class='btnAggiornaDefault ui-icon ui-icon-arrowrefresh-1-e'></span></div></td>
                        </tr>
                    </table>
                </div>End Popup default -->
            </div>
    </div>
</div>


      <!-- boxs verifica tappe -->
      <div id="VerificaTappeViaggio" title="Verifica le Tappe dei Viaggio">

      <table id="tbTappeErrate">
          <tr class="ui-state-highlight" >
              <td>Tipo</td>
              <td>Indirizzi( da Anagrafica )</td>
              <td>Indirizzi ( API Google )</td>
              <td>Affina ricerca</td>
          </tr>
      </table>
          <div id="boxSearching">
          <hr/>
          <input latLog="" size="60" style="color: #FCFDFD" type="text" id="searchAdr"/>
          &nbsp;<span id="searchByinput" class="ui-state-default ui-corner-all ui-button btn">Cerca</span>
          &nbsp;&nbsp;<span id="salvaRicerca" class="ui-state-default ui-corner-all ui-button btn"> Salva </span>
          <div style="margin: 4px" id="addressMarker"></div><br/>
          
          <br/>
          <br/>
          </div>

          <div id="mapB" class="imgMap"></div>


      </div>
      

      <!-- end dialog boxs-->








</body>
</html>