<?php
session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__libs/SQLFunct.php");
require_once("../__includes/COMMON_wakeForgEdit.php");

$TableName=$_GET['TableName'];

if($_GET["IDautorizzazione"]!="garbage") {
	$tableInfoBuffer = $FEDIT->FGE_TableInfo;
	$FEDIT->FGE_FLushTableInfo();
	switch($_GET["AppyTo"]) {
		case "destinatari":
			$FEDIT->FGE_UseTables(array("user_autorizzazioni_dest"));
			$FEDIT->FGE_SetSelectFields(array("ID_AUTHD","ID_OP_RS","ID_TRAT","rilascio","scadenza","ID_ORIGINE_DATI"),"user_autorizzazioni_dest");
			$FEDIT->FGE_DescribeFields();
			$FEDIT->FGE_SetFilter("ID_AUTHD", $_GET["IDautorizzazione"],"user_autorizzazioni_dest");
			$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
			$tmp = date(FGE_SCREEN_DATE, strtotime($FEDIT->DbRecordSet[0]["user_autorizzazioni_destrilascio"]));
			$tmp.= "&&".$FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_TRAT"];
			$tmp.= "|";
			if($FEDIT->DbRecordSet[0]["user_autorizzazioni_destscadenza"]!="0000-00-00") {
				$dataDbConvertita = strtotime($FEDIT->DbRecordSet[0]["user_autorizzazioni_destscadenza"]);
				$tmp .= date(FGE_SCREEN_DATE, $dataDbConvertita);
				$tmp .= "|" . dateDiff("d",date("m/d/Y",$dataDbConvertita),date("m/d/Y"));				
			} else {
				$tmp .= "0000-00-00|0";	
			}
			$tmp.="|".$FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_ORIGINE_DATI"];
			#
			$FEDIT->FGE_TableInfo = $tableInfoBuffer;
			$FEDIT->FGE_SetValue("ID_OP_RS",$FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_OP_RS"],$TableName);		
			//$FEDIT->FGE_SetValue("ID_TRAT",$FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_TRAT"],$TableName);
			$tableInfoBuffer = $FEDIT->FGE_TableInfo;	
		break;
		case "trasportatori":
			$FEDIT->FGE_UseTables(array("user_autorizzazioni_trasp"));
			$FEDIT->FGE_SetSelectFields(array("ID_AUTHT","rilascio","scadenza","ID_ORIGINE_DATI"),"user_autorizzazioni_trasp");
			$FEDIT->FGE_DescribeFields();
			$FEDIT->FGE_SetFilter("ID_AUTHT", $_GET["IDautorizzazione"],"user_autorizzazioni_trasp");
			$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
			$tmp = date(FGE_SCREEN_DATE, strtotime($FEDIT->DbRecordSet[0]["user_autorizzazioni_trasprilascio"])) . "|";		
			if($FEDIT->DbRecordSet[0]["user_autorizzazioni_traspscadenza"]!="0000-00-00") {
				$dataDbConvertita = strtotime($FEDIT->DbRecordSet[0]["user_autorizzazioni_traspscadenza"]);
				$tmp .= date(FGE_SCREEN_DATE, $dataDbConvertita);
				$tmp .= "|" . dateDiff("d",date("m/d/Y",$dataDbConvertita),date("m/d/Y"));				
			} else {
				$tmp .= "0000-00-00|0";	
			}
			$tmp.="|".$FEDIT->DbRecordSet[0]["user_autorizzazioni_traspID_ORIGINE_DATI"];
		break;
		case "intermediari":
			$FEDIT->FGE_UseTables(array("user_autorizzazioni_interm"));
			$FEDIT->FGE_SetSelectFields(array("ID_AUTHI","rilascio","scadenza","ID_ORIGINE_DATI"),"user_autorizzazioni_interm");
			$FEDIT->FGE_DescribeFields();
			$FEDIT->FGE_SetFilter("ID_AUTHI", $_GET["IDautorizzazione"],"user_autorizzazioni_interm");
			$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
			$tmp = date(FGE_SCREEN_DATE, strtotime($FEDIT->DbRecordSet[0]["user_autorizzazioni_intermrilascio"])) . "|";		
			if($FEDIT->DbRecordSet[0]["user_autorizzazioni_intermscadenza"]!="0000-00-00") {
				$dataDbConvertita = strtotime($FEDIT->DbRecordSet[0]["user_autorizzazioni_intermscadenza"]);
				$tmp .= date(FGE_SCREEN_DATE, $dataDbConvertita);
				$tmp .= "|" . dateDiff("d",date("m/d/Y",$dataDbConvertita),date("m/d/Y"));				
			} else {
				$tmp .= "0000-00-00|0";	
			}
			$tmp.="|".$FEDIT->DbRecordSet[0]["user_autorizzazioni_intermID_ORIGINE_DATI"];
		break;
	}
	$FEDIT->FGE_TableInfo = $tableInfoBuffer;
	echo $tmp;

} else {
	switch($_GET["AppyTo"]) {
		case "destinatari":
		$FEDIT->FGE_SetValue("ID_OP_RS","",$TableName);		
		//$FEDIT->FGE_SetValue("ID_TRAT","",$TableName);
		$tableInfoBuffer = $FEDIT->FGE_TableInfo;	
	}
	echo "||";
	
}

require_once("../__includes/COMMON_sleepForgEdit.php");
?>
