<?php
$FEDIT->FGE_HideFields(array("ID_IMP","originalID_RIF","TIPO","produttore","trasportatore","destinatario","intermediario","FKEpesospecifico"),$TableName);
$FEDIT->FGE_DisableFields(array("NMOV","FKESF","FKEumis","pesoN","FKEdisponibilita"),$TableName);
$FEDIT->FGE_DisableFields(array("FKErilascioD","FKErilascioT","FKErilascioI"),$TableName);
$FEDIT->FGE_HideFields(array("FKEgiacINI","FKElastCarico","qta_hidden","FKEscadenzaT","FKEscadenzaD","FKEscadenzaI"),$TableName);
$FEDIT->FGE_HideFields(array("NPER","ID_TRAT","NumAlboAutotraspProprio","NumAlboAutotrasp"),$TableName);
$FEDIT->FGE_HideFields(array("SIS_OK","SIS_OK_SCHEDA"),$TableName);
$FEDIT->FGE_DisableFields(array("FKEcfiscP","FKEcfiscD","FKEcfiscT","FKEcfiscI"),$TableName);
$FEDIT->FGE_DisableFields(array("adr"),$TableName);

if(isset($_GET["table"]) && isset($_GET["pri"]) && isset($_GET["filter"])) {
	$sql	= "SELECT adr FROM ".$_GET["table"]." WHERE ".$_GET['pri']."=".$_GET['filter'].";";
	$FEDIT->SDBRead($sql,"DbRecordSetADRblock",true,false);
	$ADR	= $FEDIT->DbRecordSetADRblock[0]['adr'];
	if($ADR=='0'){
		$FEDIT->FGE_DisableFields(array("QL","ID_ONU"),$_GET["table"]);
		}
	}
else{
	$FEDIT->FGE_DisableFields(array("QL","ID_ONU"),$TableName);
	}

$FEDIT->FGE_DisableFields(array("Transfrontaliero"),$TableName);

# questo if serve a nascondere trasportatore e destinatario nel caso in cui io sia un produttore
if($this->UserData["workmode"]=="produttore"){	
	$FEDIT->FGE_HideFields(array("N_ANNEX_VII","adr", "QL","ID_ONU","ID_AZD","FKEcfiscD","ID_UIMD","Transfrontaliero","ID_AUTHD","FKErilascioD","FKEscadenzaD","ID_AZT","FKEcfiscT","ID_UIMT","ID_AUTHT","FKErilascioT","FKEscadenzaT","ID_AZI","FKEcfiscI","ID_UIMI","ID_AUTHI","FKErilascioI","FKEscadenzaI"),$TableName);
	$FEDIT->FGE_SetValue("ID_AZD","0",$TableName);
	$FEDIT->FGE_SetValue("FKEcfiscD","",$TableName);
	$FEDIT->FGE_SetValue("ID_UIMD","0",$TableName);
	$FEDIT->FGE_SetValue("Transfrontaliero","0",$TableName);
	$FEDIT->FGE_SetValue("ID_AUTHD","0",$TableName);
	$FEDIT->FGE_SetValue("FKErilascioD","0000-00-00",$TableName);
	$FEDIT->FGE_SetValue("FKEscadenzaD","0000-00-00",$TableName);
	$FEDIT->FGE_SetValue("ID_AZT","0",$TableName);
	$FEDIT->FGE_SetValue("FKEcfiscT","",$TableName);
	$FEDIT->FGE_SetValue("ID_UIMT","0",$TableName);
	$FEDIT->FGE_SetValue("ID_AUTHT","0",$TableName);
	$FEDIT->FGE_SetValue("FKErilascioT","0000-00-00",$TableName);
	$FEDIT->FGE_SetValue("FKEscadenzaT","0000-00-00",$TableName);
	$FEDIT->FGE_SetValue("ID_AZI","0",$TableName);
	$FEDIT->FGE_SetValue("FKEcfiscI","",$TableName);
	$FEDIT->FGE_SetValue("ID_UIMI","0",$TableName);
	$FEDIT->FGE_SetValue("ID_AUTHI","0",$TableName);
	$FEDIT->FGE_SetValue("FKErilascioI","0000-00-00",$TableName);
	$FEDIT->FGE_SetValue("FKEscadenzaI","0000-00-00",$TableName);
	$FEDIT->FGE_SetValue("adr","0",$TableName);
	$FEDIT->FGE_SetValue("QL","0",$TableName);
	$FEDIT->FGE_SetValue("ID_ONU","0",$TableName);
	$FEDIT->FGE_SetValue("FANGHI",0,$TableName);
	}

$FEDIT->FGE_HideFields("ID_USR",$TableName);
$FEDIT->FGE_HideFields("UniqueString",$TableName);


?>