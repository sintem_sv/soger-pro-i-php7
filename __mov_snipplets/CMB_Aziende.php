<?php
session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__libs/SQLFunct.php");
require_once("../__includes/COMMON_wakeForgEdit.php");

$TableName=$_GET['TableName'];

if($_GET["IDazienda"]!="garbage") {
	switch($_GET["AppyTo"]) {
		case "produttori":
//{{{
			#
			#	codice fiscale azienda
			#
			$buffer = $FEDIT->FGE_TableInfo;
			$FEDIT->FGE_FlushTableInfo();
			$FEDIT->FGE_UseTables(array("user_aziende_produttori"));
			$FEDIT->FGE_SetSelectFields(array("ID_AZP","codfisc"),"user_aziende_produttori");
			$FEDIT->FGE_DescribeFields();
			$FEDIT->FGE_SetFilter("ID_AZP",$_GET["IDazienda"],"user_aziende_produttori");
			$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
			$reply = $FEDIT->DbRecordSet[0]["user_aziende_produttoricodfisc"];
			#
			#	cerca impianto univoco
			#
			$FEDIT->FGE_FlushTableInfo();
			$FEDIT->FGE_UseTables(array("user_impianti_produttori"));
			$FEDIT->FGE_SetSelectFields(array("ID_UIMP","ID_AZP","approved"),"user_impianti_produttori");
			$FEDIT->FGE_DescribeFields();
			$FEDIT->FGE_SetFilter("ID_AZP",$_GET["IDazienda"],"user_impianti_produttori");
                        $FEDIT->FGE_SetFilter("approved","1","user_impianti_produttori");
			$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
                        if($FEDIT->DbRecordSet){
                            if($FEDIT->DbRecsNum==1) {
                                $ID_UIMP = $FEDIT->DbRecordSet[0]["user_impianti_produttoriID_UIMP"];
                            }
                        }
			$reply .= "|||";
			//}}}
		break;
		case "destinatari":
//{{{
			#
			#	codice fiscale azienda
			#
			$buffer = $FEDIT->FGE_TableInfo;
			$FEDIT->FGE_FlushTableInfo();
			$FEDIT->FGE_UseTables("user_aziende_destinatari");
			$FEDIT->FGE_SetSelectFields(array("ID_AZD","codfisc"),"user_aziende_destinatari");
			$FEDIT->FGE_DescribeFields();
			$FEDIT->FGE_SetFilter("ID_AZD",$_GET["IDazienda"],"user_aziende_destinatari");
			$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
			$reply = $FEDIT->DbRecordSet[0]["user_aziende_destinataricodfisc"];
			#
			#	cerca impianto / autorizzazione univoca
			#
			$FEDIT->FGE_FlushTableInfo();
			$FEDIT->FGE_UseTables(array("user_impianti_destinatari","user_autorizzazioni_dest"));
			$FEDIT->FGE_SetSelectFields(array("ID_UIMD","IN_ITALIA","approved"),"user_impianti_destinatari");
			$FEDIT->FGE_SetSelectFields(array("ID_AUTHD","rilascio","scadenza","ID_OP_RS","ID_TRAT","ID_RIF","ID_ORIGINE_DATI","approved"),"user_autorizzazioni_dest");
			$FEDIT->FGE_DescribeFields();
			$FEDIT->FGE_SetFilter("ID_AZD",$_GET["IDazienda"],"user_impianti_destinatari");
                        $FEDIT->FGE_SetFilter("approved","1","user_impianti_destinatari");
			$FEDIT->FGE_SetFilter("approved","1","user_autorizzazioni_dest");
			$FEDIT->FGE_SetFilter("ID_RIF",$_GET["ID_RIF"],"user_autorizzazioni_dest");
                        $FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
			$multipleUIMD = false;
			$multipleAUTHD = false;
                        if($FEDIT->DbRecordSet){
                            if($FEDIT->DbRecsNum==1) {
                                    $ID_UIMD = $FEDIT->DbRecordSet[0]["user_impianti_destinatariID_UIMD"];
                                    $ID_AUTHD = $FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_AUTHD"];
                                    $ID_OP_RS = $FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_OP_RS"];
                                    $ID_TRAT = $FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_TRAT"];
                                    $ID_ORIGINE_DATI_D = $FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_ORIGINE_DATI"];
                                    $reply .= "|" . date(FGE_SCREEN_DATE, strtotime($FEDIT->DbRecordSet[0]["user_autorizzazioni_destrilascio"]));
                                    $reply .= "&&".$ID_TRAT;
                                    $reply .= "|" . DateScreenConvert($FEDIT->DbRecordSet[0]["user_autorizzazioni_destscadenza"]);
                                    $reply .= "|" . $FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_ORIGINE_DATI"];
                                    $reply .= "|" . $FEDIT->DbRecordSet[0]["user_impianti_destinatariIN_ITALIA"];
                            } else {
                                    foreach($FEDIT->DbRecordSet as $k=>$dati) {
                                            #
                                            if(!isset($tmpID_AUTHD)) {
                                                    $tmpID_AUTHD = $dati["user_autorizzazioni_destID_AUTHD"];
                                                    $tmpID_OP_RS = $dati["user_autorizzazioni_destID_OP_RS"];
                                                    $tmpID_TRAT = $dati["user_autorizzazioni_destID_TRAT"];
                                            } else {
                                                    if($tmpID_AUTHD!=$dati["user_autorizzazioni_destID_AUTHD"]) {
                                                            $multipleAUTHD = true;
                                                    }
                                            }
                                            #
                                            if(!isset($tmpID_UIMD)) {
                                                    $tmpID_UIMD = $dati["user_impianti_destinatariID_UIMD"];
                                            } else {
                                                    if($tmpID_UIMD!=$dati["user_impianti_destinatariID_UIMD"]) {
                                                            $multipleUIMD = true;
                                                    }
                                            }
                                    }
                            }
                        }
			#
			#	REPLY ARRAY 2-4 (Destinatari: rilascio e scadenza autorizzazione)
			#

                        if(!$multipleUIMD && $FEDIT->DbRecsNum>1) {
				$ID_UIMD = $tmpID_UIMD;
			}
			if(!$multipleAUTHD && $FEDIT->DbRecsNum>1) {
				$ID_OP_RS = $tmpID_OP_RS;
				$ID_TRAT = $tmpID_TRAT;
				$ID_AUTHD = $tmpID_AUTHD;
				$reply .= "|" . date(FGE_SCREEN_DATE, strtotime($dati["user_autorizzazioni_destrilascio"]));
				$reply .= "&&".$ID_TRAT;
				$reply .= "|" . DateScreenConvert($dati["user_autorizzazioni_destscadenza"]);
			} else {
				$reply .= "|||";
			}
			#}//}}}
		break;
		case "trasportatori":
//{{{
		#
		#	codice fiscale azienda
		#
		$buffer = $FEDIT->FGE_TableInfo;
		$FEDIT->FGE_FlushTableInfo();
		$FEDIT->FGE_UseTables("user_aziende_trasportatori");
		$FEDIT->FGE_SetSelectFields(array("ID_AZT","codfisc","NumAlboAutotraspProprio","NumAlboAutotrasp"),"user_aziende_trasportatori");
		$FEDIT->FGE_DescribeFields();
		$FEDIT->FGE_SetFilter("ID_AZT",$_GET["IDazienda"],"user_aziende_trasportatori");
		$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
		$reply = $FEDIT->DbRecordSet[0]["user_aziende_trasportatoricodfisc"] . "&&" . $FEDIT->DbRecordSet[0]["user_aziende_trasportatoriNumAlboAutotraspProprio"] . "&&" . $FEDIT->DbRecordSet[0]["user_aziende_trasportatoriNumAlboAutotrasp"];
		#
		#	cerca impianto / autorizzazione univoca
		#
		$FEDIT->FGE_FlushTableInfo();
		$FEDIT->FGE_UseTables(array("user_impianti_trasportatori","user_autorizzazioni_trasp"));
		$FEDIT->FGE_SetSelectFields(array("ID_UIMT","approved"),"user_impianti_trasportatori");
		$FEDIT->FGE_SetSelectFields(array("ID_AUTHT","rilascio","scadenza","ID_RIF","ID_ORIGINE_DATI","approved"),"user_autorizzazioni_trasp");
		$FEDIT->FGE_DescribeFields();
		$FEDIT->FGE_SetFilter("ID_AZT",$_GET["IDazienda"],"user_impianti_trasportatori");
                $FEDIT->FGE_SetFilter("approved","1","user_impianti_trasportatori");
		$FEDIT->FGE_SetFilter("approved","1","user_autorizzazioni_trasp");
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["ID_RIF"],"user_autorizzazioni_trasp");
		$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
		$multipleUIMT = false;
		$multipleAUTHT = false;
                if($FEDIT->DbRecordSet){
                    if($FEDIT->DbRecsNum==1) {
                            $ID_UIMT = $FEDIT->DbRecordSet[0]["user_impianti_trasportatoriID_UIMT"];
                            $ID_AUTHT = $FEDIT->DbRecordSet[0]["user_autorizzazioni_traspID_AUTHT"];
                            $ID_ORIGINE_DATI = $FEDIT->DbRecordSet[0]["user_autorizzazioni_traspID_ORIGINE_DATI"];
                            $reply .= "|" . date(FGE_SCREEN_DATE, strtotime($FEDIT->DbRecordSet[0]["user_autorizzazioni_trasprilascio"]));
                            $reply .= "|" . DateScreenConvert($FEDIT->DbRecordSet[0]["user_autorizzazioni_traspscadenza"]);
                            $reply .= "|" . $FEDIT->DbRecordSet[0]["user_autorizzazioni_traspID_ORIGINE_DATI"];
                    } else {
                            foreach($FEDIT->DbRecordSet as $k=>$dati) {
                                    #
                                    if(!isset($tmpID_AUTHT)) {
                                            $tmpID_AUTHT = $dati["user_autorizzazioni_traspID_AUTHT"];
                                    } else {
                                            if($tmpID_AUTHT!=$dati["user_autorizzazioni_traspID_AUTHT"]) {
                                                    $multipleAUTHT = true;
                                            }
                                    }
                                    #
                                    if(!isset($tmpID_UIMT)) {
                                            $tmpID_UIMT = $dati["user_impianti_trasportatoriID_UIMT"];
                                    } else {
                                            if($tmpID_UIMT!=$dati["user_impianti_trasportatoriID_UIMT"]) {
                                                    $multipleUIMT = true;
                                            }
                                    }
                            }
                    }
                }
		#
		#	REPLY ARRAY 2-4 (Trasportatori: rilascio e scadenza autorizzazione)
		#
		if(!$multipleUIMT && $FEDIT->DbRecsNum>1) {
			$ID_UIMT = $tmpID_UIMT;
		}
		if(!$multipleAUTHT && $FEDIT->DbRecsNum>1) {
			$ID_AUTHT = $tmpID_AUTHT;
			$reply .= "|" . date(FGE_SCREEN_DATE, strtotime($dati["user_autorizzazioni_trasprilascio"]));
			$reply .= "|" . DateScreenConvert($dati["user_autorizzazioni_traspscadenza"]);
		} else {
			$reply .= "|||";
		}
		//}}}
		break;




		case "intermediari":
//{{{
		#
		#	codice fiscale azienda
		#
		$buffer = $FEDIT->FGE_TableInfo;
		$FEDIT->FGE_FlushTableInfo();
		$FEDIT->FGE_UseTables("user_aziende_intermediari");
		$FEDIT->FGE_SetSelectFields(array("ID_AZI","codfisc"),"user_aziende_intermediari");
		$FEDIT->FGE_DescribeFields();
		$FEDIT->FGE_SetFilter("ID_AZI",$_GET["IDazienda"],"user_aziende_intermediari");
		$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
		$reply = $FEDIT->DbRecordSet[0]["user_aziende_intermediaricodfisc"];
		#
		#	cerca impianto / autorizzazione univoca
		#
		$FEDIT->FGE_FlushTableInfo();
		$FEDIT->FGE_UseTables(array("user_impianti_intermediari","user_autorizzazioni_interm"));
		$FEDIT->FGE_SetSelectFields(array("ID_UIMI","approved"),"user_impianti_intermediari");
		$FEDIT->FGE_SetSelectFields(array("ID_AUTHI","rilascio","scadenza","ID_RIF","ID_ORIGINE_DATI","approved"),"user_autorizzazioni_interm");
		$FEDIT->FGE_DescribeFields();
		$FEDIT->FGE_SetFilter("ID_AZI",$_GET["IDazienda"],"user_impianti_intermediari");
                $FEDIT->FGE_SetFilter("approved","1","user_impianti_intermediari");
		$FEDIT->FGE_SetFilter("approved","1","user_autorizzazioni_interm");
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["ID_RIF"],"user_autorizzazioni_interm");
		$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
		$multipleUIMI = false;
		$multipleAUTHI = false;
                if($FEDIT->DbRecordSet){
                    if($FEDIT->DbRecsNum==1) {
                            $ID_UIMI = $FEDIT->DbRecordSet[0]["user_impianti_intermediariID_UIMI"];
                            $ID_AUTHI = $FEDIT->DbRecordSet[0]["user_autorizzazioni_intermID_AUTHI"];
                            $ID_ORIGINE_DATI = $FEDIT->DbRecordSet[0]["user_autorizzazioni_intermID_ORIGINE_DATI"];
                            $reply .= "|" . date(FGE_SCREEN_DATE, strtotime($FEDIT->DbRecordSet[0]["user_autorizzazioni_intermrilascio"]));
                            $reply .= "|" . DateScreenConvert($FEDIT->DbRecordSet[0]["user_autorizzazioni_intermscadenza"]);
                            $reply .= "|" . $FEDIT->DbRecordSet[0]["user_autorizzazioni_intermID_ORIGINE_DATI"];
                    } else {
                            foreach($FEDIT->DbRecordSet as $k=>$dati) {
                                    #
                                    if(!isset($tmpID_AUTHI)) {
                                            $tmpID_AUTHI = $dati["user_autorizzazioni_intermID_AUTHI"];
                                    } else {
                                            if($tmpID_AUTHI!=$dati["user_autorizzazioni_intermID_AUTHI"]) {
                                                    $multipleAUTHI = true;
                                            }
                                    }
                                    #
                                    if(!isset($tmpID_UIMI)) {
                                            $tmpID_UIMI = $dati["user_impianti_trasportatoriID_UIMI"];
                                    } else {
                                            if($tmpID_UIMI!=$dati["user_impianti_trasportatoriID_UIMI"]) {
                                                    $multipleUIMI = true;
                                            }
                                    }
                            }
                    }
                }
		#
		#	REPLY ARRAY 2-4 (Intermediari: rilascio e scadenza autorizzazione)
		#
		if(!$multipleUIMI && $FEDIT->DbRecsNum>1) {
			$ID_UIMI = $tmpID_UIMI;
		}
		if(!$multipleAUTHI && $FEDIT->DbRecsNum>1) {
			$ID_AUTHI = $tmpID_AUTHI;
			$reply .= "|" . date(FGE_SCREEN_DATE, strtotime($dati["user_autorizzazioni_intermrilascio"]));
			$reply .= "|" . DateScreenConvert($dati["user_autorizzazioni_intermscadenza"]);
		} else {
			$reply .= "|||";
		}
		//}}}
		break;
	}



	#
	echo $reply;
	#
	$FEDIT->FGE_TableInfo = $buffer;
	#
	# Applicazione filtri: PRODUTTORI
	#
	switch($_GET["AppyTo"]) {
		case "produttori":
		$FEDIT->FGE_LookUpCFG("ID_UIMP",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_AZP",$_GET["IDazienda"],"user_impianti_produttori");
		$FEDIT->FGE_LookUpDone();
		break;
		#
		# Applicazione filtri: DESTINATARI
		#
		case "destinatari":
	//{{{
		$FEDIT->FGE_LookUpCFG("ID_UIMD",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_AZD",$_GET["IDazienda"],"user_impianti_destinatari");
		$FEDIT->FGE_LookUpDone();
		#
		$FEDIT->FGE_LookUpCFG("ID_AUTHD",$TableName);
                if(!$multipleUIMD && $ID_UIMD) {
                    $FEDIT->FGE_SetFilter("ID_UIMD",$ID_UIMD,"user_autorizzazioni_dest");
                } else {
                    $FEDIT->FGE_SetFilter("ID_UIMD","###","user_autorizzazioni_dest");
                }
		$FEDIT->FGE_LookUpDone();
		#
		if(!$multipleUIMD && $ID_UIMD) {
                    $FEDIT->FGE_SetValue("ID_UIMD",$ID_UIMD,$TableName);
		} else {
                    $FEDIT->FGE_SetValue("ID_UIMD",0,$TableName);
		}
		#

		if($_GET["Tipo"]=="S" || $_SESION['workmode']!='produttore') {
			if(!$multipleAUTHD) {
				$FEDIT->FGE_SetValue("ID_OP_RS",$ID_OP_RS,$TableName);
				$FEDIT->FGE_SetValue("ID_AUTHD",$ID_AUTHD,$TableName);
			} else {
				$FEDIT->FGE_SetValue("ID_OP_RS",0,$TableName);
				$FEDIT->FGE_SetValue("ID_AUTHD",0,$TableName);
			}
		}
		break;
		//}}}
		#
		# Applicazione filtri: TRASPORTATORI
		#
		case "trasportatori":
	//{{{
		$FEDIT->FGE_LookUpCFG("ID_UIMT",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_AZT",$_GET["IDazienda"],"user_impianti_trasportatori");
		$FEDIT->FGE_LookUpDone();
		$FEDIT->FGE_LookUpCFG("ID_AUTHT",$TableName);
                if(!$multipleUIMT && $ID_UIMT) {
                    $FEDIT->FGE_SetFilter("ID_UIMT",$ID_UIMT,"user_autorizzazioni_trasp");
                } else {
                    $FEDIT->FGE_SetFilter("ID_UIMT","###","user_autorizzazioni_trasp");
                }
		$FEDIT->FGE_LookUpDone();
		if(!$multipleUIMT && $ID_UIMT) {
                    $FEDIT->FGE_SetValue("ID_UIMT",$ID_UIMT,$TableName);
		} else {
                    $FEDIT->FGE_SetValue("ID_UIMT",0,$TableName);
		}
		if(!$multipleAUTHT) {
			$FEDIT->FGE_SetValue("ID_AUTHT",$ID_AUTHT,$TableName);
		} else {
			$FEDIT->FGE_SetValue("ID_AUTHT",0,$TableName);
		}
		//}}}
		break;


		#
		# Applicazione filtri: INTERMEDIARI
		#
		case "intermediari":
	//{{{
		$FEDIT->FGE_LookUpCFG("ID_UIMI",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_AZI",$_GET["IDazienda"],"user_impianti_intermediari");
		$FEDIT->FGE_LookUpDone();
		$FEDIT->FGE_LookUpCFG("ID_AUTHI",$TableName);
                if(!$multipleUIMI && $ID_UIMI) {
                    $FEDIT->FGE_SetFilter("ID_UIMI",$ID_UIMI,"user_autorizzazioni_interm");
                } else {
                    $FEDIT->FGE_SetFilter("ID_UIMI","###","user_autorizzazioni_interm");
                }
		$FEDIT->FGE_LookUpDone();
		if(!$multipleUIMI && $ID_UIMI) {
                    $FEDIT->FGE_SetValue("ID_UIMI",$ID_UIMI,$TableName);
		} else {
                    $FEDIT->FGE_SetValue("ID_UIMI",0,$TableName);
		}
		if(!$multipleAUTHI) {
			$FEDIT->FGE_SetValue("ID_AUTHI",$ID_AUTHI,$TableName);
		} else {
			$FEDIT->FGE_SetValue("ID_AUTHI",0,$TableName);
		}
		//}}}
		break;

	}
} else {
	switch($_GET["AppyTo"]) {
		case "produttori":
			$FEDIT->FGE_LookUpCFG("ID_UIMP",$TableName,false,true);
			$FEDIT->FGE_SetFilter("ID_AZP","###","user_impianti_produttori");
			$FEDIT->FGE_LookUpDone();
			break;
		case "destinatari":
			$FEDIT->FGE_LookUpCFG("ID_UIMD",$TableName,false,true);
			$FEDIT->FGE_SetFilter("ID_AZD","###","user_impianti_destinatari");
			$FEDIT->FGE_LookUpDone();
			$FEDIT->FGE_LookUpCFG("ID_AUTHD",$TableName,false,true);
			$FEDIT->FGE_SetFilter("ID_UIMD","###","user_autorizzazioni_dest");
			$FEDIT->FGE_LookUpDone();
			$FEDIT->FGE_SetValue("ID_OP_RS",0,$TableName);
			break;
		case "trasportatori":
			$FEDIT->FGE_LookUpCFG("ID_UIMT",$TableName,false,true);
			$FEDIT->FGE_SetFilter("ID_AZT","###","user_impianti_trasportatori");
			$FEDIT->FGE_LookUpDone();
			$FEDIT->FGE_LookUpCFG("ID_AUTHT",$TableName,false,true);
			$FEDIT->FGE_SetFilter("ID_UIMT","###","user_autorizzazioni_trasp");
			$FEDIT->FGE_LookUpDone();
			break;
		case "intermediari":
			$FEDIT->FGE_LookUpCFG("ID_UIMI",$TableName,false,true);
			$FEDIT->FGE_SetFilter("ID_AZI","###","user_impianti_intermediari");
			$FEDIT->FGE_LookUpDone();
			$FEDIT->FGE_LookUpCFG("ID_AUTHI",$TableName,false,true);
			$FEDIT->FGE_SetFilter("ID_UIMI","###","user_autorizzazioni_interm");
			$FEDIT->FGE_LookUpDone();
			break;
	}
	echo "||||";
}
require_once("../__includes/COMMON_sleepForgEdit.php");
?>
