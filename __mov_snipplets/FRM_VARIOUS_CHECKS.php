<?php
session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
global $SOGER;


function checkIfEditable($ID_MOV_F, $GGEDIT){
	// Posso modificare il carico precedente se:
	// 1- non � FISCALE=1
	// 2- idSis IS NULL
	// 3- DTMOV > oggi - ggmodificabili
	global $FEDIT;
	$sql		= "SELECT idSIS, FISCALE, DTMOV, NFORM FROM user_movimenti_fiscalizzati WHERE ID_MOV_F=".$ID_MOV_F.";";
	$FEDIT->SDBRead($sql,"DbRecordSet",'1','0');
	$FISCALE	= $FEDIT->DbRecordSet[0]['FISCALE'];
	$idSIS		= $FEDIT->DbRecordSet[0]['idSIS'];
	$DTMOV		= $FEDIT->DbRecordSet[0]['DTMOV'];
        $NFORM		= $FEDIT->DbRecordSet[0]['NFORM'];
	$oggi			= mktime(0, 0, 0, date('m'), date('d'), date('Y'));
	$movimento		= explode('-', $DTMOV);
	$movimento_dt	= mktime(0, 0, 0, $movimento[1], $movimento[2], $movimento[0]);
	$differenza		= ($oggi - $movimento_dt)/(60*60*24);
        $CaricoIsFir = (($SOGER->UserData['workmode']!='produttore' AND !is_null($NFORM) && trim($NFORM)!='')? true:false);
	return ($FISCALE=='1' || !is_null($idSIS) || $differenza>$GGEDIT || $CaricoIsFir) ? 0 : 1;
	}


$TableName	= $_GET['TableName'];
$pri		= $_GET['pri'];
$filter		= $_GET['filter'];
$TIPO		= $_GET['TIPO'];
$NMOV		= $_GET['NMOV'];
$NFORM		= $_GET['NFORM'];
$ID_RIF		= $_GET['ID_RIF'];
list($giorno, $mese, $anno) = explode('/', trim($_GET['DTMOV']));
$DTMOV		= $anno."-".$mese."-".$giorno;

$OK_DTMOV	= true; 
$OK_NFORM	= true;


// VERIFICO SE ULTIMO MOVIMENTO DI CARICO E' EDITABILE
$C_EDITABLE = 1;
$sql = "SELECT ID_MOV_F FROM user_movimenti_fiscalizzati WHERE NMOV<".$NMOV." AND ".$SOGER->UserData['workmode']."=1 AND ID_IMP='".$SOGER->UserData['core_impiantiID_IMP']."' AND TIPO='C' AND ID_RIF=".$ID_RIF." ORDER BY NMOV DESC LIMIT 0,1;";
$FEDIT->SDBRead($sql,"DbRecordSet",'1','0');
if(!is_null($FEDIT->DbRecordSet[0]['ID_MOV_F'])){
	$C_EDITABLE = checkIfEditable($FEDIT->DbRecordSet[0]['ID_MOV_F'], $SOGER->UserData['core_usersGGedit_'.$SOGER->UserData['workmode']]);
	}
else{
	$C_EDITABLE = 0;
	}

// VERIFICO SE E' RISPETTATO ORDINE CRONOLOGICO REGISTRAZIONI
if($SOGER->UserData['core_usersCHECK_MOV_ORDER']==1 && $DTMOV!="--"){
	// controllo DTMOV	
	$sql ="SELECT DTMOV FROM ".$TableName." WHERE ";
	if($filter!="")
		$sql.=$pri."<>".$filter." AND ";
	$sql.="ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' ";
	$sql.="AND DTMOV>'".$DTMOV."' AND NMOV<".$NMOV;
	include("../__includes/SOGER_DirectProfilo.php");
	$FEDIT->SDBRead($sql,"DbRecDTMOV",true,false);
	if(!is_null($FEDIT->DbRecDTMOV)) $OK_DTMOV=false;
	}

// VERIFICO SE IL NUMERO DI FORMULARIO INSERITO E' UNIVOCO
if($SOGER->UserData['core_usersCHECK_DOUBLE_FIR']==1 && trim($NFORM)!=""){
	// controllo NFORM
	$sql ="SELECT NFORM FROM ".$TableName." WHERE ";
	if($filter!="")
		$sql.=$pri."<>".$filter." AND ";
	$sql.="ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' ";
	$sql.="AND NFORM LIKE '%".$NFORM."%' ";
	$sql.="AND TIPO='".$TIPO."' ";
	include("../__includes/SOGER_DirectProfilo.php");
	$FEDIT->SDBRead($sql,"DbRecNFORM",true,false);
	if(!is_null($FEDIT->DbRecNFORM)) $OK_NFORM=false;
	}
	

if($OK_DTMOV && $OK_NFORM){
	// ok liscio..
	$check		= 1;
	$response	= "";
	}
else{
	$response="Impossibile salvare il movimento:";
	if(!$OK_DTMOV) $response.="\n - la data del movimento indicata e' precedente ad altre gia' inserite";
	if(!$OK_NFORM) $response.="\n - esiste gia' un movimento col numero di formulario indicato";
	$check=0;
	}

echo $check."|".$response."|".$C_EDITABLE;

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");

?>