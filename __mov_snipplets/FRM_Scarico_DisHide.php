<?php
	
$FEDIT->FGE_HideFields(array("ID_IMP","originalID_RIF","TIPO","TIPO_S_SCHEDA","TIPO_S_INTERNO","produttore","trasportatore","destinatario","intermediario","percorso","FKEpesospecifico"),$TableName);
$FEDIT->FGE_DisableFields(array("NMOV","FKESF","FKEumis","pesoN"),$TableName);

if($IS_SCHEDA_SISTRI && $IS_SCHEDA_SISTRI_OPEN)
	$FEDIT->FGE_HideFields("NMOV",$TableName);

$FEDIT->FGE_HideFields(array("qta_hidden","StampaAnnua"),$TableName);
$FEDIT->FGE_HideFields(array("FKEgiacINI","FKElastCarico","FKErifCarico"),$TableName);
$FEDIT->FGE_HideFields(array("FKEscadenzaT","FKEscadenzaD","FKEscadenzaI"),$TableName);

$FEDIT->FGE_HideFields(array("NPER","ID_TRAT","NumAlboAutotraspProprio","NumAlboAutotrasp"),$TableName);

if(isset($_GET["table"]) && isset($_GET["pri"]) && isset($_GET["filter"])) {
	$sql	= "SELECT adr FROM ".$_GET["table"]." WHERE ".$_GET['pri']."=".$_GET['filter'].";";
	$FEDIT->SDBRead($sql,"DbRecordSetADRblock",true,false);
	$ADR	= $FEDIT->DbRecordSetADRblock[0]['adr'];
	if($ADR=='0'){
		$FEDIT->FGE_DisableFields(array("QL"),$_GET["table"]);
		}
	}
else{
	$FEDIT->FGE_DisableFields(array("QL"),$TableName);
	}
$FEDIT->FGE_DisableFields(array("ID_ONU"),$TableName);
$FEDIT->FGE_HideFields(array("CAR_NumDocumento","CAR_Laboratorio","CAR_DataAnalisi","prescrizioni_mov"),$TableName);
$FEDIT->FGE_HideFields(array("ID_COMM","ID_CAR"),$TableName);
$FEDIT->FGE_HideFields(array("USER_telefono","USER_email"),$TableName);
$FEDIT->FGE_HideFields(array("SIS_OK","SIS_OK_SCHEDA"),$TableName);
$FEDIT->FGE_DisableFields(array("FKErilascioD","FKErilascioT","FKErilascioI","FKEdisponibilita","adr"),$TableName);
$FEDIT->FGE_DisableFields(array("FKEcfiscP","FKEcfiscD","FKEcfiscT","FKEcfiscI"),$TableName);
$FEDIT->FGE_DisableFields(array("Transfrontaliero"),$TableName);

if($SOGER->UserData['core_impiantiMODULO_SIS']==1 && $IS_SCHEDA_SISTRI){
	if(isset($_GET["table"]) && isset($_GET["pri"]) && isset($_GET["filter"])) {
		$sql	= "SELECT ID_TIPO_IMBALLAGGIO FROM ".$_GET["table"]." WHERE ".$_GET['pri']."=".$_GET['filter'].";";
		$FEDIT->SDBRead($sql,"DbRecordSetTIPO_IMBALLAGGIO",true,false);
		$TIPO_IMBALLAGGIO	= $FEDIT->DbRecordSetTIPO_IMBALLAGGIO[0]['ID_TIPO_IMBALLAGGIO'];
		if($TIPO_IMBALLAGGIO!='2090'){
			$FEDIT->FGE_DisableFields(array("ALTRO_TIPO_IMBALLAGGIO"),$TableName);
			}
		}
	else{
		$FEDIT->FGE_DisableFields(array("ALTRO_TIPO_IMBALLAGGIO"),$TableName);
		}
	}

if( ($SOGER->UserData["core_usersFRM_LAYOUT_DEST"]==1 AND $SOGER->UserData['workmode']!='produttore') || $IS_SCARICO_INTERNO){
	$FEDIT->FGE_HideFields(array("NFORM","DTFORM", "VER_DESTINO"),$TableName);
	$FEDIT->FGE_HideFields(array("adr","QL","ID_ONU"),$TableName);
	$FEDIT->FGE_HideFields(array("ID_AZP","FKEcfiscP","ID_UIMP"),$TableName);
	$FEDIT->FGE_HideFields(array("ID_AZT","FKEcfiscT","ID_UIMT","ID_AUTHT","FKErilascioT","FKEscadenzaT","NumAlboAutotraspProprio","NumAlboAutotrasp","NPER","SenzaTrasporto"),$TableName);
	$FEDIT->FGE_HideFields(array("ID_AZI","FKEcfiscI","ID_UIMI","ID_AUTHI","FKErilascioI","FKEscadenzaI"),$TableName);
	}

$FEDIT->FGE_HideFields("ID_USR",$TableName);
$FEDIT->FGE_HideFields("UniqueString",$TableName);

?>