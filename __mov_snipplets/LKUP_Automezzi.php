<?php
	$FEDIT->FGE_LookUpCFG("ID_AUTO",$TableName,false,$autoTRADEST);
	$FEDIT->FGE_UseTables(array("user_automezzi"));
	$FEDIT->FGE_SetSelectFields(array("description","ID_AUTHT","adr","ID_ORIGINE_DATI"),"user_automezzi");
	$FEDIT->FGE_SetFilter("ID_AUTHT",$AuthTRA,"user_automezzi");
		
	if($IsAdr=='1' && $SOGER->UserData['core_usersck_ADR']=='1') {
		$FEDIT->FGE_SetFilter("adr",1,"user_automezzi");	
	}

	$FEDIT->FGE_SetOrder("user_automezzi:description");
	$FEDIT->FGE_HideFields(array("ID_AUTHT","adr","ID_ORIGINE_DATI"),"user_automezzi");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
?>