<?php
	$FEDIT->FGE_LookUpCFG("ID_AUTST",$TableName,false,false);
	$FEDIT->FGE_UseTables(array("user_autisti"));
	$FEDIT->FGE_SetSelectFields(array("description","nome","ID_UIMT","adr","scadenza"),"user_autisti");
	$FEDIT->FGE_SetFilter("ID_UIMT",$ID_UIMT,"user_autisti");
	
	if($IsAdr=='1' && $SOGER->UserData['core_usersck_ADR']=='1') {
		$FEDIT->FGE_SetFilter("adr",1,"user_autisti");	
	}
	
	$FEDIT->FGE_HideFields(array("ID_UIMT","adr","scadenza"),"user_autisti");
	$FEDIT->FGE_SetOrder("user_autisti:description");	
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
?>