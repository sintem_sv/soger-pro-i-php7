<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");

$msg="";
$estensioni=array('.doc', '.docx', '.rtf', '.pdf', '.tiff','.DOC', '.DOCX', '.RTF', '.PDF', '.TIFF');

//die(var_dump($_FILES));

do {
  if (is_uploaded_file($_FILES['doc_allegato']['tmp_name'])) {
    // Controllo che il file non superi 10 Mb
	$MbAllowed=10;
    if ($_FILES['doc_allegato']['size'] > (1024000 * $MbAllowed) ) {
      $msg = "Dimensioni del file non valide: superato il limite massimo di ".$MbAllowed." Mb. \\n";
      break;
    }
    // Controllo che il file sia in uno dei formati consentiti
	$estensione = substr($_FILES['doc_allegato']['name'],strripos($_FILES['doc_allegato']['name'],'.'));
	if(!in_array($estensione, $estensioni)){
      $msg = "Formato del file non valido: estensione diversa da .tiff, .doc, .rtf o .pdf. \\n";
      break;
    }
	switch($_POST['soggetto']){
		case "user_aziende_produttori":
			$directory="produttore/";
			break;
		case "user_aziende_trasportatori":
			$directory="trasportatore/";
			break;
		case "user_aziende_destinatari":
			$directory="destinatario/";
			break;
		case "user_aziende_intermediari":
			$directory="intermediario/";
			break;
		}
	/*
	$string='../__upload/allegati/'.$directory.'['.$_POST["id"].']'.$_FILES['doc_allegato']['name'];
	echo $string;
	echo "<hr>";
	$encoded = utf8_encode($string);
	echo $encoded;
	$decoded = $encoded);
	echo "<hr>";
	echo $decoded;
	die();
	*/
    // Sposto il file nella cartella da me desiderata
    if (!move_uploaded_file($_FILES['doc_allegato']['tmp_name'], '../__upload/allegati/'.$directory.'['.$_POST["id"].']'.$_FILES['doc_allegato']['name'])) {
		
      $msg = "Errore nel caricamento, verificare i permessi per la cartella 'allegati'. \\n";
      break;
    }
  }
} while (false);

if($msg!=""){
	echo "<script type=\"text/javascript\">\n";
	echo "window.alert('Si sono verificati i seguenti errori: \\n ".$msg." ');";
	echo "history.go(-1);";
	echo "</script>";
	}
else{
	echo "<script type=\"text/javascript\">\n";
	echo "window.alert('Documento caricato correttamente');";
	echo "history.go(-1);";
	echo "</script>";
	}

require_once("../__includes/COMMON_sleepForgEdit.php");
require_once("../__includes/COMMON_sleepSoger.php");

?>