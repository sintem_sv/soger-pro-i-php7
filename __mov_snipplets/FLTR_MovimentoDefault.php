<?php
	#
	#	produttore (azienda)
	#
	$FEDIT->FGE_LookUpCFG("ID_AZP",$TableName);
	$FEDIT->FGE_SetFilter("ID_AZP","###","user_aziende_produttori");
	$FEDIT->FGE_LookUpDone();	
	#
	#	produttore (impianto)
	#
	$FEDIT->FGE_LookUpCFG("ID_UIMP",$TableName);
	$FEDIT->FGE_SetFilter("ID_AZP","###","user_impianti_produttori");
	$FEDIT->FGE_LookUpDone();	
	#
	#	destinatario (impianto)
	#
	$FEDIT->FGE_LookUpCFG("ID_UIMD",$TableName);
	$FEDIT->FGE_SetFilter("ID_AZD","###","user_impianti_destinatari");
	$FEDIT->FGE_SetFilter("ID_RIF","###","user_autorizzazioni_dest");
	$FEDIT->FGE_LookUpDone();	
	#
	# destinatario (azienda)
	#
	$FEDIT->FGE_LookUpCFG("ID_AZD",$TableName);
	$FEDIT->FGE_SetFilter("ID_RIF","###","user_autorizzazioni_dest");
	$FEDIT->FGE_LookUpDone();   
	#
	#	destinatario (autorizzazioni)
	#
	$FEDIT->FGE_LookUpCFG("ID_AUTHD",$TableName);	
	$FEDIT->FGE_SetFilter("ID_UIMD","###","user_autorizzazioni_dest");
	$FEDIT->FGE_SetFilter("ID_RIF","###","user_autorizzazioni_dest");
	$FEDIT->FGE_LookUpDone();	
	#
	#	trasportatore (azienda)
	#
	$FEDIT->FGE_LookUpCFG("ID_AZT",$TableName);
	$FEDIT->FGE_SetFilter("ID_RIF","###","user_autorizzazioni_trasp");
	$FEDIT->FGE_LookUpDone();
	#
	#	trasportatore (impianto)
	#
	$FEDIT->FGE_LookUpCFG("ID_UIMT",$TableName);
	$FEDIT->FGE_SetFilter("ID_AZT","###","user_impianti_trasportatori");
	$FEDIT->FGE_SetFilter("ID_RIF","###","user_autorizzazioni_trasp");
	$FEDIT->FGE_LookUpDone();	
	#
	#	trasportatore (autorizzazioni)
	#
	$FEDIT->FGE_LookUpCFG("ID_AUTHT",$TableName);	
	$FEDIT->FGE_SetFilter("ID_UIMT","###","user_autorizzazioni_trasp");
	$FEDIT->FGE_SetFilter("ID_RIF","###","user_autorizzazioni_trasp");
	$FEDIT->FGE_LookUpDone();
	#
	#	intermediario (azienda)
	#
	$FEDIT->FGE_LookUpCFG("ID_AZI",$TableName);
	$FEDIT->FGE_SetFilter("ID_RIF","###","user_autorizzazioni_interm");
	$FEDIT->FGE_LookUpDone();
	#
	#	intermediario (impianto)
	#
	$FEDIT->FGE_LookUpCFG("ID_UIMI",$TableName);
	$FEDIT->FGE_SetFilter("ID_AZI","###","user_impianti_intermediari");
	$FEDIT->FGE_SetFilter("ID_RIF","###","user_autorizzazioni_interm");
	$FEDIT->FGE_LookUpDone();	
	#
	#	intermediario (autorizzazioni)
	#
	$FEDIT->FGE_LookUpCFG("ID_AUTHI",$TableName);	
	$FEDIT->FGE_SetFilter("ID_UIMI","###","user_autorizzazioni_interm");
	$FEDIT->FGE_SetFilter("ID_RIF","###","user_autorizzazioni_interm");
	$FEDIT->FGE_LookUpDone();
?>
