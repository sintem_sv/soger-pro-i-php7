<?php
session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");

$msg="";
//$estensione=explode(".", $_FILES['immagine']['name']);
//$ext=$estensione[count($estensione)-1];

list($width, $height, $type, $attr) = getimagesize($_FILES['immagine']['tmp_name']);

do {
  if (is_uploaded_file($_FILES['immagine']['tmp_name'])) {
    // Controllo che il file non superi i 1048576 Byte = 1 Megabyte
    if ($_FILES['immagine']['size'] > 1048576) {
      $msg = "Dimensioni del file non valide: superato il limite massimo di 1 Mb. \\n";
      break;
    }
    // Controllo che le dimensioni (in pixel) non superino 600x600
   if (($width > 600) || ($height > 600)) {
      $msg = "Dimensioni del file non valide: superato il limite massimo di 600px per lato. \\n";
      break;
    }
    // Controllo che il file sia in JPG
    if ($type!=2) {
      $msg = "Formato del file non valido: estensione diversa da .jpg, .JPG, .jpeg o .JPEG. \\n";
      break;
    }
    // Sposto il file nella cartella da me desiderata
    if (!move_uploaded_file($_FILES['immagine']['tmp_name'], '../__upload/'.$_POST['folder'].'/'.$_POST['ID_RIF'].".jpg")) {
      $msg = "Errore nel caricamento, verificare i permessi per la cartella __upload. \\n";
      break;
    }
  }
} while (false);


/*
$type=1 -> .gif
$type=2 -> .jpg/.jpeg
$type=3 -> .png
*/


if($msg!=""){
	echo "<script type=\"text/javascript\">\n";
	echo "window.alert('Si sono verificati i seguenti errori: \\n ".$msg." ');";
	echo "history.go(-1);";
	echo "</script>";
	}
else{
	echo "<script type=\"text/javascript\">\n";
	echo "window.alert('Immagine caricata correttamente');";
	echo "history.go(-1);";
	echo "</script>";
	}


	
require_once("../__includes/COMMON_sleepForgEdit.php");
require_once("../__includes/COMMON_sleepSoger.php");

?>