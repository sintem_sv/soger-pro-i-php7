<?php
session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
global $SOGER;

$TableName=$_GET['TableName'];
$pri=$_GET['pri'];
$filter=$_GET['filter'];
$TIPO=$_GET['TIPO'];
$NMOV=$_GET['NMOV'];
$NFORM=$_GET['NFORM'];
// 01/01/2010 -> 2010-01-01
list($giorno, $mese, $anno) = explode('/', trim($_GET['DTMOV']));
$DTMOV=$anno."-".$mese."-".$giorno;

if($TableName=="user_movimenti_fiscalizzati"){

	if(trim($NFORM)!=""){
		// controllo NFORM
		$sql ="SELECT NFORM FROM ".$TableName." WHERE ";
		if($filter!="")
			$sql.=$pri."<>".$filter." AND ";
		$sql.="ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' ";
		$sql.="AND NFORM LIKE '%".$NFORM."%' ";
		include("../__includes/SOGER_DirectProfilo.php");
		$FEDIT->SDBRead($sql,"DbRecNFORM",true,false);
		}

	if($DTMOV!="--"){
		// controllo DTMOV	
		$sql ="SELECT DTMOV FROM ".$TableName." WHERE ";
		if($filter!="")
			$sql.=$pri."<>".$filter." AND ";
		$sql.="ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' ";
		$sql.="AND DTMOV>'".$DTMOV."' AND NMOV<".$NMOV;
		include("../__includes/SOGER_DirectProfilo.php");
		$FEDIT->SDBRead($sql,"DbRecDTMOV",true,false);
		}
	
	}

if(is_null($FEDIT->DbRecNFORM) && is_null($FEDIT->DbRecDTMOV)){
	// ok liscio..
	$check=1;
	$response="";
	}
else{
	$response="Impossibile salvare il movimento:";
	if(!is_null($FEDIT->DbRecNFORM)) $response.="\n - esiste gia' un movimento col numero di formulario indicato";
	if(!is_null($FEDIT->DbRecDTMOV)) $response.="\n - la data del movimento indicata e' precedente ad altre gia' inserite";
	$check=0;
	}

echo $check."|".$response;

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");

?>