<?php
session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");

##
$ID_IMP		= $SOGER->UserData['core_usersID_IMP'];
$workmode	= $SOGER->UserData['workmode'];
$GG			= $SOGER->UserData['core_usersGGedit_'.$workmode];
$MINDATE	= date ( 'Y-m-d', strtotime ( '-'.$GG.' day' . date('Y-m-d') ) );
$SQL = "SELECT MIN(NMOV) AS MINNMOV FROM user_movimenti_fiscalizzati WHERE ID_IMP='".$ID_IMP."' AND ".$workmode."=1 AND DTMOV>='".$MINDATE."';";
$FEDIT->SDBRead($SQL,"DbRecordSetNMOV", true, false);

if($FEDIT->DbRecordSetNMOV[0]['MINNMOV']){

    $MINNMOV	= $FEDIT->DbRecordSetNMOV[0]['MINNMOV'];
    $SQL		= "SELECT ID_MOV_F, NMOV FROM user_movimenti_fiscalizzati";
	$SQL		.=" WHERE ID_IMP='".$ID_IMP."' AND ".$workmode."=1 AND NMOV<>9999999 AND NMOV<>0 AND NMOV>=".$MINNMOV;
	$SQL		.=" ORDER BY DTMOV ASC, PerRiclassificazione ASC,";
	$SQL		.=" CASE PerRiclassificazione WHEN 0 THEN TIPO END ASC, CASE PerRiclassificazione WHEN 1 THEN TIPO END DESC, ";
	$SQL		.=" NMOV ASC, ID_MOV_F;";

    $FEDIT->SdbRead($SQL,"DbRecordSet",true,false);

    for($m=0;$m<count($FEDIT->DbRecordSet);$m++){
        $nmov	= $m + $MINNMOV;
        $sql	= "UPDATE user_movimenti_fiscalizzati SET NMOV=".$nmov." WHERE ID_MOV_F=".$FEDIT->DbRecordSet[$m]['ID_MOV_F'];
        $FEDIT->SDBWrite($sql,true,false);
    }
}

## aggiorno riferimenti carico
$LegamiSISTRI	= false;
$TableName = 'user_movimenti_fiscalizzati';
require("../__scripts/MovimentiRifMovCarico.php");


echo "Operazione ultimata, la pagina verra' ora ricaricata.";
	
require_once("../__includes/COMMON_sleepForgEdit.php");
require_once("../__includes/COMMON_sleepSoger.php");

//header("location: ../");
?>