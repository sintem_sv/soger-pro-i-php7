<?php
$FEDIT->FGE_LookUpCFG("ID_AZP",$TableName,false,true);	
$FEDIT->FGE_UseTables(array("user_aziende_produttori", "user_impianti_produttori"));
$FEDIT->FGE_SetSelectFields(array("ID_AZP","ID_IMP","description","produttore","destinatario","trasportatore","approved"),"user_aziende_produttori");
$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_aziende_produttori");
$ProfiloFields = "user_aziende_produttori";
include("SOGER_FiltriProfilo.php");
$FEDIT->FGE_GroupBy("ID_AZP","user_aziende_produttori");
$FEDIT->FGE_SetOrder("user_aziende_produttori:description");
$FEDIT->FGE_HideFields(array("ID_AZP","ID_IMP","produttore","destinatario","trasportatore","approved"),"user_aziende_produttori");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();
?>