<?php
$FEDIT->FGE_LookUpCFG("ID_AZD",$TableName,false,$autoTRADEST);
$FEDIT->FGE_UseTables(array("user_aziende_destinatari","user_impianti_destinatari","user_autorizzazioni_dest"));
$FEDIT->FGE_SetSelectFields(array("ID_AZD","ID_IMP","description","produttore","destinatario","trasportatore","approved"),"user_aziende_destinatari");
$FEDIT->FGE_SetSelectFields(array("ID_RIF"),"user_autorizzazioni_dest");
$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_aziende_destinatari");

if(isset($_GET['table']) AND $PerRiclassificazione==1)
	$FEDIT->FGE_SetFilter("ID_AZD",0,"user_aziende_destinatari");

$ProfiloFields = "user_aziende_destinatari";
include("SOGER_FiltriProfilo.php");
$FEDIT->FGE_GroupBy("ID_AZD","user_aziende_destinatari");
$FEDIT->FGE_SetOrder("user_aziende_destinatari:description");
$FEDIT->FGE_HideFields(array("ID_RIF"),"user_autorizzazioni_dest");
$FEDIT->FGE_HideFields(array("ID_AZD","ID_IMP","produttore","destinatario","trasportatore","approved"),"user_aziende_destinatari");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();
?>