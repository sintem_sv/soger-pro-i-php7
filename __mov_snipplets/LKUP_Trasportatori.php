<?php
$FEDIT->FGE_LookUpCFG("ID_AZT",$TableName,false,$autoTRADEST);		
$FEDIT->FGE_UseTables(array("user_aziende_trasportatori","user_impianti_trasportatori","user_autorizzazioni_trasp"));
$FEDIT->FGE_SetSelectFields(array("ID_AZT","ID_IMP","description","produttore","destinatario","trasportatore","approved"),"user_aziende_trasportatori");
$FEDIT->FGE_SetSelectFields(array("ID_RIF"),"user_autorizzazioni_trasp");
$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_aziende_trasportatori");
$ProfiloFields = "user_aziende_trasportatori";
include("SOGER_FiltriProfilo.php");
$FEDIT->FGE_GroupBy("ID_AZT","user_aziende_trasportatori");
$FEDIT->FGE_SetOrder("user_aziende_trasportatori:description");
$FEDIT->FGE_HideFields(array("ID_RIF"),"user_autorizzazioni_trasp");
$FEDIT->FGE_HideFields(array("ID_AZT","ID_IMP","produttore","destinatario","trasportatore","approved"),"user_aziende_trasportatori");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();
?>