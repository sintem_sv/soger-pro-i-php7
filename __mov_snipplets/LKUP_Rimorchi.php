<?php

	$FEDIT->FGE_LookUpCFG("ID_RMK",$TableName,false,false);
	$FEDIT->FGE_UseTables(array("user_rimorchi"));
	$FEDIT->FGE_SetSelectFields(array("ID_RMK","description","ID_AUTHT","adr","ID_ORIGINE_DATI"),"user_rimorchi");
	$FEDIT->FGE_GroupBy("ID_RMK","user_rimorchi");
	$FEDIT->FGE_SetFilter("ID_AUTHT",$AuthTRA,"user_rimorchi");
	
	if($IsAdr=='1' && $SOGER->UserData['core_usersck_ADR']=='1') {
		$FEDIT->FGE_SetFilter("adr",1,"user_rimorchi");	
	}
	$FEDIT->FGE_SetOrder("user_rimorchi:description");
	$FEDIT->FGE_HideFields(array("ID_RMK","ID_AUTHT","adr","ID_ORIGINE_DATI"),"user_rimorchi");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
?>
