<?php
session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__libs/SQLFunct.php");
require_once("../__includes/COMMON_wakeForgEdit.php");

$TableName=$_GET['TableName'];

if($_GET["IDimpianto"]!="garbage") {
		$buffer = $FEDIT->FGE_TableInfo;
		switch($_GET["AppyTo"]) {
			case "destinatari":
				#
				#	cerca autorizzazione univoca
				#
				$FEDIT->FGE_FlushTableInfo();
				$FEDIT->FGE_UseTables(array("user_autorizzazioni_dest","user_impianti_destinatari"));
				$FEDIT->FGE_SetSelectFields(array("ID_AUTHD","rilascio","scadenza","ID_OP_RS","ID_TRAT","ID_UIMD","ID_RIF","ID_ORIGINE_DATI","approved"),"user_autorizzazioni_dest");
				$FEDIT->FGE_SetSelectFields(array("IN_ITALIA"),"user_impianti_destinatari");
				$FEDIT->FGE_DescribeFields();
				$FEDIT->FGE_SetFilter("ID_UIMD",$_GET["IDimpianto"],"user_autorizzazioni_dest");
				$FEDIT->FGE_SetFilter("ID_RIF",$_GET["ID_RIF"],"user_autorizzazioni_dest");
				$FEDIT->FGE_SetFilter("approved","1","user_autorizzazioni_dest");
				$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
				if($FEDIT->DbRecsNum==1) {
					$ID_AUTHD = $FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_AUTHD"];
					$ID_OP_RS = $FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_OP_RS"];
					$ID_TRAT = $FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_TRAT"];
					$reply  = date(FGE_SCREEN_DATE, strtotime($FEDIT->DbRecordSet[0]["user_autorizzazioni_destrilascio"]));
					$reply .= "&&".$ID_TRAT;
					$reply .= "|" . DateScreenConvert($FEDIT->DbRecordSet[0]["user_autorizzazioni_destscadenza"]);
					$reply .= "|" . $FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_ORIGINE_DATI"];
					$reply .= "|" . $FEDIT->DbRecordSet[0]["user_impianti_destinatariIN_ITALIA"];
				} else {
					$reply = "|||";	
				}
			break;
			case "trasportatori":
				#
				#	cerca autorizzazione univoca
				#
	//{{{ 
				$FEDIT->FGE_FlushTableInfo();
				$FEDIT->FGE_UseTables(array("user_autorizzazioni_trasp"));
				$FEDIT->FGE_SetSelectFields(array("ID_AUTHT","rilascio","scadenza","ID_UIMT","ID_RIF","ID_ORIGINE_DATI","approved"),"user_autorizzazioni_trasp");
				$FEDIT->FGE_DescribeFields();
				$FEDIT->FGE_SetFilter("ID_UIMT",$_GET["IDimpianto"],"user_autorizzazioni_trasp");
				$FEDIT->FGE_SetFilter("ID_RIF",$_GET["ID_RIF"],"user_autorizzazioni_trasp");
				$FEDIT->FGE_SetFilter("approved","1","user_autorizzazioni_trasp");
				$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
				if($FEDIT->DbRecsNum==1) {
					$ID_AUTHT = $FEDIT->DbRecordSet[0]["user_autorizzazioni_traspID_AUTHT"];
					$reply = date(FGE_SCREEN_DATE, strtotime($FEDIT->DbRecordSet[0]["user_autorizzazioni_trasprilascio"]));
					$reply .= "|" . DateScreenConvert($FEDIT->DbRecordSet[0]["user_autorizzazioni_traspscadenza"]);
					$reply .= "|" . $FEDIT->DbRecordSet[0]["user_autorizzazioni_traspID_ORIGINE_DATI"];
				} else {
					$reply = "||";	
				} //}}}
			break;
			case "intermediari":
				#
				#	cerca autorizzazione univoca
				#
	//{{{ 
				$FEDIT->FGE_FlushTableInfo();
				$FEDIT->FGE_UseTables(array("user_autorizzazioni_interm"));
				$FEDIT->FGE_SetSelectFields(array("ID_AUTHI","rilascio","scadenza","ID_UIMI","ID_RIF","ID_ORIGINE_DATI","approved"),"user_autorizzazioni_interm");
				$FEDIT->FGE_DescribeFields();
				$FEDIT->FGE_SetFilter("ID_UIMI",$_GET["IDimpianto"],"user_autorizzazioni_interm");
				$FEDIT->FGE_SetFilter("approved","1","user_autorizzazioni_interm");
				$FEDIT->FGE_SetFilter("ID_RIF",$_GET["ID_RIF"],"user_autorizzazioni_interm");
				$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
				if($FEDIT->DbRecsNum==1) {
					$ID_AUTHI = $FEDIT->DbRecordSet[0]["user_autorizzazioni_intermID_AUTHI"];
					$reply = date(FGE_SCREEN_DATE, strtotime($FEDIT->DbRecordSet[0]["user_autorizzazioni_intermrilascio"]));
					$reply .= "|" . DateScreenConvert($FEDIT->DbRecordSet[0]["user_autorizzazioni_intermscadenza"]);
					$reply .= "|" . $FEDIT->DbRecordSet[0]["user_autorizzazioni_intermID_ORIGINE_DATI"];
				} else {
					$reply = "||";	
				} //}}}
			break;
		}
		$FEDIT->FGE_TableInfo = $buffer;
	
	switch($_GET["AppyTo"]) {
	#
	#	APPLICA FILTRI - DESTINATARI
	#	
	case "destinatari":
		$FEDIT->FGE_LookUpCFG("ID_AUTHD",$TableName);
		$FEDIT->FGE_SetFilter("ID_UIMD",$_GET["IDimpianto"],"user_autorizzazioni_dest");
		$FEDIT->FGE_LookUpDone();
		if($_GET["Tipo"]=="S") {
			if(isset($ID_OP_RS)) {
				$FEDIT->FGE_SetValue("ID_OP_RS",$ID_OP_RS,$TableName);
				}
			//if(isset($ID_TRAT)) {
			//	$FEDIT->FGE_SetValue("ID_TRAT",$ID_TRAT,$TableName);
			//}
		}
	break;
	#
	#	APPLICA FILTRI - TRASPORTATORI
	#
	case "trasportatori":
	$FEDIT->FGE_LookUpCFG("ID_AUTHT",$TableName);
	$FEDIT->FGE_SetFilter("ID_UIMT",$_GET["IDimpianto"],"user_autorizzazioni_trasp");
	$FEDIT->FGE_LookUpDone();
	break;
	#
	#	APPLICA FILTRI - INTERMEDIARI
	#
	case "intermediari":
	$FEDIT->FGE_LookUpCFG("ID_AUTHI",$TableName);
	$FEDIT->FGE_SetFilter("ID_UIMI",$_GET["IDimpianto"],"user_autorizzazioni_interm");
	$FEDIT->FGE_LookUpDone();
	break;
	}
} else {
	
	switch($_GET["AppyTo"]) {
		case "destinatari":
			$FEDIT->FGE_SetValue("ID_OP_RS",0,$TableName);
			//$FEDIT->FGE_SetValue("ID_TRAT",0,$TableName);
			$FEDIT->FGE_LookUpCFG("ID_AUTHD",$TableName);
			$FEDIT->FGE_SetFilter("ID_UIMD","###","user_autorizzazioni_dest");
			$FEDIT->FGE_LookUpDone();	
		break;
		case "trasportatori":
			$FEDIT->FGE_LookUpCFG("ID_AUTHT",$TableName);
			$FEDIT->FGE_SetFilter("ID_UIMT","###","user_autorizzazioni_trasp");
			$FEDIT->FGE_LookUpDone();
		break;
		case "intermediari":
			$FEDIT->FGE_LookUpCFG("ID_AUTHI",$TableName);
			$FEDIT->FGE_SetFilter("ID_UIMI","###","user_autorizzazioni_interm");
			$FEDIT->FGE_LookUpDone();
		break;
	}
	$reply = "||";
}

echo $reply;
require_once("../__includes/COMMON_sleepForgEdit.php");
?>
