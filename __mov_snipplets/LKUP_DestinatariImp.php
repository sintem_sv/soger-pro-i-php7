<?php
$FEDIT->FGE_LookUpCFG("ID_UIMD",$TableName,false,$autoTRADEST);
$FEDIT->FGE_UseTables(array("user_impianti_destinatari","user_autorizzazioni_dest"));
$FEDIT->FGE_SetSelectFields(array("ID_UIMD","description","ID_AZD","approved"),"user_impianti_destinatari");
$FEDIT->FGE_SetSelectFields(array("ID_RIF"),"user_autorizzazioni_dest");
$FEDIT->FGE_GroupBy("ID_UIMD","user_impianti_destinatari");
$FEDIT->FGE_SetOrder("user_impianti_destinatari:description");
$FEDIT->FGE_HideFields(array("ID_RIF"),"user_autorizzazioni_dest");
$FEDIT->FGE_HideFields(array("ID_UIMD","ID_AZD","approved"),"user_impianti_destinatari");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();
?>
