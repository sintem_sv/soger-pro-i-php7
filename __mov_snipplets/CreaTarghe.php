<?php
session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");

$OrigineDati = ($_GET['albo']==1)? 4:1;
$MezzoTrasporto = $_GET['ID_AUTO_TYPE'];
$Rimorchio = $_GET['ID_RMK_TYPE'];

if($_GET['automezzo']){
    $sql ="INSERT INTO user_automezzi \n";
    $sql.="( `ID_AUTO` , `description` , `PREV_description` , `adr` , `BlackBox` , `ID_AUTHT` , `ID_ORIGINE_DATI` , `ID_LIM`, `ID_MZ_TRA`, `inherit_edits`, `COD_IMPORT` ) \n";
    $sql.="VALUES ( NULL , '".addslashes($_GET['automezzo'])."', '".addslashes($_GET['automezzo'])."', '".$_GET['adr_automezzo']."', '".$_GET['BlackBox']."', '".$_GET['autorizzazione']."', '".$OrigineDati."', '1', '".$MezzoTrasporto."', 0, 0 );\n";
    $FEDIT->SDBWrite($sql,true,false);

    $log ="INSERT INTO core_logs \n";
    $log.="( `table` , `pri_key` , `ID_USR` , `ID_IMP` , `sqlType` , `sqlCommand` ) \n";
    $log.="VALUES ('user_automezzi','--','".$SOGER->UserData["core_usersID_USR"]."', '".$SOGER->UserData["core_usersID_IMP"]."', 'creazione', '".addslashes($sql)."');";
    $FEDIT->SDBWrite($log,true,false);
}

if($_GET['rimorchio']){
    $sql ="INSERT INTO user_rimorchi \n";
    $sql.="( `ID_RMK` , `description` , `PREV_description` , `adr` , `ID_AUTHT` , `ID_ORIGINE_DATI` , `ID_LIM`, `ID_MZ_RMK`, `inherit_edits`, `COD_IMPORT` ) \n";
    $sql.="VALUES ( NULL , '".addslashes($_GET['rimorchio'])."', '".addslashes($_GET['rimorchio'])."', '".$_GET['adr_rimorchio']."', '".$_GET['autorizzazione']."', '".$OrigineDati."', '1', '".$Rimorchio."', 0, 0 );\n";
    $FEDIT->SDBWrite($sql,true,false);

    $log ="INSERT INTO core_logs \n";
    $log.="( `table` , `pri_key` , `ID_USR` , `ID_IMP` , `sqlType` , `sqlCommand` ) \n";
    $log.="VALUES ('user_rimorchi','--','".$SOGER->UserData["core_usersID_USR"]."', '".$SOGER->UserData["core_usersID_IMP"]."', 'creazione', '".addslashes($sql)."');";
    $FEDIT->SDBWrite($log,true,false);
}

require_once("../__includes/COMMON_sleepForgEdit.php");
require_once("../__includes/COMMON_sleepSoger.php");

?>