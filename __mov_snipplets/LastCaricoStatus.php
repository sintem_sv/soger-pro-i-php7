<?php
session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
global $SOGER;

function checkIfEditable($ID_MOV_F, $GGEDIT){
	// Posso modificare il carico precedente se:
	// 1- non � FISCALE=1
	// 2- idSis IS NULL
	// 3- DTMOV > oggi - ggmodificabili
	global $FEDIT;
	$sql		= "SELECT idSIS, FISCALE, DTMOV FROM user_movimenti_fiscalizzati WHERE ID_MOV_F=".$ID_MOV_F.";";
	$FEDIT->SDBRead($sql,"DbRecordSet",'1','0');
	$FISCALE	= $FEDIT->DbRecordSet[0]['FISCALE'];
	$idSIS		= $FEDIT->DbRecordSet[0]['idSIS'];
	$DTMOV		= $FEDIT->DbRecordSet[0]['DTMOV'];
	$oggi			= mktime(0, 0, 0, date('m'), date('d'), date('Y'));
	$movimento		= explode('-', $DTMOV);
	$movimento_dt	= mktime(0, 0, 0, $movimento[1], $movimento[2], $movimento[0]);
	$differenza		= ($oggi - $movimento_dt)/(60*60*24);
	return ($FISCALE=='1' || !is_null($idSIS) || $differenza>$GGEDIT) ? '0' : '1';
	}

$editable	= '1';
$sql		= "SELECT FKErifCarico FROM ".$_GET['TableName']." WHERE ".$_GET['pri']."='".$_GET['filter']."'";
$FEDIT->SDBRead($sql,"DbRecordSet",'1','0');


// HO RIFERIMENTO A MOV. DI CARICO
if(!is_null($FEDIT->DbRecordSet[0]['FKErifCarico'])){	
	$LastCarico = end(explode(",", $FEDIT->DbRecordSet[0]['FKErifCarico']));
	if(stripos($LastCarico,'giac') !== false) {
		$sql = "SELECT ID_MOV_F FROM user_movimenti_fiscalizzati WHERE NMOV='".$LastCarico."' AND ".$SOGER->UserData['workmode']."=1 AND ID_IMP='".$SOGER->UserData['core_impiantiID_IMP']."';";
		$FEDIT->SDBRead($sql,"DbRecordSet",'1','0');
		$editable = checkIfEditable($FEDIT->DbRecordSet[0]['ID_MOV_F'], $SOGER->UserData['core_usersGGedit_'.$SOGER->UserData['workmode']]);
		}
	else{
		$editable = '0';
		}
	}

// NON HO RIFERIMENTO A MOV. DI CARICO
// � un nuovo scarico oppure pu� essere NULL se lo scarico era stato salvato a zero e lo si sta incrementando.
// devo verificare lo stato del carico immediatamente precedente per lo stesso rifiuto
else{
	$sql = "SELECT ID_MOV_F FROM user_movimenti_fiscalizzati WHERE NMOV<".$_GET['NMOV']." AND ".$SOGER->UserData['workmode']."=1 AND ID_IMP='".$SOGER->UserData['core_impiantiID_IMP']."' AND TIPO='C' AND ID_RIF=".$_GET['ID_RIF']." ORDER BY NMOV DESC LIMIT 0,1;";
	$FEDIT->SDBRead($sql,"DbRecordSet",'1','0');
	if(!is_null($FEDIT->DbRecordSet[0]['ID_MOV_F'])){
		$editable = checkIfEditable($FEDIT->DbRecordSet[0]['ID_MOV_F'], $SOGER->UserData['core_usersGGedit_'.$SOGER->UserData['workmode']]);
		}
	else{
		$editable = '0';
		}
	}

echo $editable;

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");

?>