<?php
session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");

$msg="";
$estensioni=array('.pdf', '.PDF');

do {
  if (is_uploaded_file($_FILES['analisi']['tmp_name'])) {
    // Controllo che il file non superi 1 Mb // Paolo 09/03/2010
    // Controllo che il file non superi 2 Mb // Paolo 01/07/2014
	// Controllo che il file non superi 5 Mb // Tiziana 19/12/2014
    if ($_FILES['analisi']['size'] > 5242880) {
      $msg = "Dimensioni del file non valide: superato il limite massimo di 2 Mb. \\n";
      break;
    }
    // Controllo che il file sia in uno dei formati RTF o PDF
	$estensione = substr($_FILES['analisi']['name'],strripos($_FILES['analisi']['name'],'.'));
	if(!in_array($estensione, $estensioni)){
	//if($fileChunks[1] != 'doc' && $fileChunks[1] != 'rtf' && $fileChunks[1] != 'pdf'){
      $msg = "Formato del file non valido: estensione diversa da .pdf. \\n";
      break;
    }
    // Sposto il file nella cartella da me desiderata
    if (!move_uploaded_file($_FILES['analisi']['tmp_name'], '../__upload/analisi/'.$_POST['ID_RIF'].strtolower($estensione))) {
      $msg = "Errore nel caricamento, verificare i permessi per la cartella __upload/analisi. \\n";
      break;
    }
  }
} while (false);

if($msg!=""){
	echo "<script type=\"text/javascript\">\n";
	echo "window.alert('Si sono verificati i seguenti errori: \\n ".$msg." ');";
	echo "history.go(-1);";
	echo "</script>";
	}
else{
	echo "<script type=\"text/javascript\">\n";
	echo "window.alert('Analisi caricata correttamente');";
	echo "history.go(-1);";
	echo "</script>";
	}

require_once("../__includes/COMMON_sleepForgEdit.php");
require_once("../__includes/COMMON_sleepSoger.php");

?>