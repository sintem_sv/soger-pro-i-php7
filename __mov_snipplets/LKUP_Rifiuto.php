<?php
$FEDIT->FGE_LookUpCFG("ID_RIF",$TableName);
$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
$FEDIT->FGE_SetSelectFields(array("cod_pro","descrizione","ID_CER","ID_IMP","produttore","destinatario","trasportatore","approved"),"user_schede_rifiuti");
$ProfiloFields = "user_schede_rifiuti";
include("SOGER_FiltriProfilo.php");
$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");

if($SOGER->UserData['core_impiantiMODULO_SIS']==1 AND $SOGER->UserData['workmode']=='produttore'){
	
	$AppLocationToApplyFilter = array("UserNuovoMovScarico", "UserNuovoMovScaricoF", "UserNuovoMovScaricoFiscale", "UserNuovaSchedaSistri", "UserNuovaSchedaSistriF", "UserNuovaSchedaSistriFiscale");

	if(in_array($SOGER->AppLocation, $AppLocationToApplyFilter)){ 
		
		// NUOVO SCARICO / FIR oppure VISUALIZZAZIONE SCHEDA SISTRI
		if(strpos($SOGER->AppLocation, "SchedaSistri")===false){
			if(!$IS_SCHEDA_SISTRI){
				if($SOGER->UserData['core_usersFRM_pericolosi']=='0')
					$FEDIT->FGE_SetFilter("pericoloso",0,"user_schede_rifiuti");
				}
			else
				$FEDIT->FGE_SetFilter("pericoloso",1,"user_schede_rifiuti");
			}
		// NUOVA SCHEDA SISTRI
		else{
			$FEDIT->FGE_SetFilter("pericoloso",1,"user_schede_rifiuti");
			}
		}

	}

$FEDIT->FGE_SetOrder("lov_cer:COD_CER");
$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","destinatario","trasportatore","approved"),"user_schede_rifiuti");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();
?>