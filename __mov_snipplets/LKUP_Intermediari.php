<?php
$FEDIT->FGE_LookUpCFG("ID_AZI",$TableName,false,false);		
$FEDIT->FGE_UseTables(array("user_aziende_intermediari","user_impianti_intermediari","user_autorizzazioni_interm"));
$FEDIT->FGE_SetSelectFields(array("ID_AZI","ID_IMP","description","produttore","destinatario","trasportatore","approved"),"user_aziende_intermediari");
$FEDIT->FGE_SetSelectFields(array("ID_RIF"),"user_autorizzazioni_interm");
$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_aziende_intermediari");
$ProfiloFields = "user_aziende_intermediari";
include("SOGER_FiltriProfilo.php");
$FEDIT->FGE_GroupBy("ID_AZI","user_aziende_intermediari");
$FEDIT->FGE_SetOrder("user_aziende_intermediari:description");
$FEDIT->FGE_HideFields(array("ID_RIF"),"user_autorizzazioni_interm");
$FEDIT->FGE_HideFields(array("ID_AZI","ID_IMP","produttore","destinatario","trasportatore","approved"),"user_aziende_intermediari");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();
?>
