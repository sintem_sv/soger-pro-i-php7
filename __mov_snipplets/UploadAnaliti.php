<?php
session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");

$msg="";
$estensioni=array('.doc', '.rtf', '.pdf');

do {
  if (is_uploaded_file($_FILES['analiti']['tmp_name'])) {
    // Controllo che il file non superi i 5 Mb
    if ($_FILES['analiti']['size'] > 5242880) {
      $msg = "Dimensioni del file non valide: superato il limite massimo di 5 Mb. \\n";
      break;
    }
    // Controllo che il file sia in uno dei formati RTF o PDF
	$estensione = substr($_FILES['analiti']['name'],strripos($_FILES['analiti']['name'],'.'));
	if(!in_array($estensione, $estensioni)){
	//if($fileChunks[1] != 'doc' && $fileChunks[1] != 'rtf' && $fileChunks[1] != 'pdf'){
      $msg = "Formato del file non valido: estensione diversa da .doc, .rtf o .pdf. \\n";
      break;
    }
    // Sposto il file nella cartella da me desiderata
    if (!move_uploaded_file($_FILES['analiti']['tmp_name'], '../__upload/liste_analiti/'.$_POST['ID_RIF'].$estensione)) {
      $msg = "Errore nel caricamento, verificare i permessi per la cartella __upload/liste_analiti. \\n";
      break;
    }
  }
} while (false);

if($msg!=""){
	echo "<script type=\"text/javascript\">\n";
	echo "window.alert('Si sono verificati i seguenti errori: \\n ".$msg." ');";
	echo "history.go(-1);";
	echo "</script>";
	}
else{
	echo "<script type=\"text/javascript\">\n";
	echo "window.alert('Lista analiti caricata correttamente');";
	echo "history.go(-1);";
	echo "</script>";
	}

require_once("../__includes/COMMON_sleepForgEdit.php");
require_once("../__includes/COMMON_sleepSoger.php");

?>