<?php
session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__libs/SQLFunct.php");
$tmp="";

$MovToSave=count($_GET);
for($m=1;$m<=$MovToSave;$m++){
    
	$dati=explode("|",$_GET[$m]);
	$MovID=$dati[0];
	$PesoDestino=$dati[1];
	$DataRiconsegna=$dati[2];
        $DestArrivo=$dati[3];
        $DestAccRifiuto=$dati[4];

	#
	#	SOGER ECO: aggiorna costi (peso a destino)
	#
	if($SOGER->UserData["core_usersG3"] == "1") {
		$impianto = $SOGER->UserData["core_impiantiID_IMP"];
		$sql = "SELECT PS_DESTINO FROM user_movimenti_fiscalizzati WHERE ID_MOV_F='" . $MovID . "'";
		$FEDIT->SDBRead($sql,"DBRecordSet",true,false);	
		if($FEDIT->DBRecordSet[0]["PS_DESTINO"]!=$PesoDestino) {
			if($PesoDestino=='') $PesoDestino=0;
			$sql = "UPDATE user_movimenti_costi SET euro=" . $PesoDestino . "*FKE_costoUM WHERE ID_MOV_F=$MovID AND FKE_UM='KGD'";
			$FEDIT->SDBWrite($sql,true,false);
			if($FEDIT->DbInsEdtNum!=0) {
				$updCosti = true;	
			}
		}
	}

	$sql = "UPDATE user_movimenti_fiscalizzati SET ";
	
	## PESO A DESTINO
	if($PesoDestino=="")
            $sql .= " PS_DESTINO=NULL,";            
	else
            $sql .= " PS_DESTINO='" . $PesoDestino . "',";            

	## DATA RICONSEGNA
	if($DataRiconsegna!="") {
            $items = explode("/",$DataRiconsegna);
            $data = $items[2] . "-" . $items[1] . "-" . $items[0];  				
            $sql .= " DT_FORM='" . $data . "',";
            }
        else
            $sql .= " DT_FORM = NULL,";
	
        ## DATA ARRIVO
	if($DestArrivo!="") {
            $dt = explode(" ",$DestArrivo);
            $data = explode("/", $dt[0]);
            $date = $data[2] . "-" . $data[1] . "-" . $data[0];
            $sql .= " DEST_ARRIVO='" . $date . " " . $dt[1] . "',";
            }

	## ACCETTATAZIONE RIFIUTO
        $sql .= " DEST_ACC_RIFIUTO=".$DestAccRifiuto;

	$sql .= " WHERE ID_MOV_F='" . $MovID . "'";
        
	$FEDIT->SDBWrite($sql,true,false);

	$LogSql  = "INSERT INTO core_logs ";
        $LogSql .= "(`table` , `pri_key` , `ID_USR` , `ID_IMP` , `sqlType` , `sqlCommand` ) ";
        $LogSql .= "VALUES ";
	$LogSql .= "('user_movimenti_fiscalizzati','" . $MovID . "','";
	$LogSql .= $SOGER->UserData["core_usersID_USR"] . "','";
	$LogSql .= $SOGER->UserData["core_usersID_IMP"] . "','modifica','".addslashes($sql)."')";
	$FEDIT->SDBWrite($LogSql,true,false);

	}


$SOGER->AppLocation = "UserVediMovimentiIVcopie";


if(!isset($updCosti)) {
		$SOGER->SetFeedback("Record Salvati",2);
} else {
	$SOGER->SetFeedback("Attenzione: il computo economico � stato aggiornato",3);
}

require_once("../__includes/COMMON_sleepForgEdit.php");
require_once("../__includes/COMMON_sleepSoger.php");

header("location: ../");
?>
