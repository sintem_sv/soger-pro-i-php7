<?php
session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");

## al momento non fa distinzione tra movimenti gi� stampati a registro o meno

$LAST_BAT_SQL="SELECT MAX(ID_BAT) AS ID_BAT FROM batch WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND workmode='".$SOGER->UserData['workmode']."'";
$FEDIT->SDBRead($LAST_BAT_SQL,"DbRecordSetBatch", true, false);

//die($FEDIT->DbRecordSetBatch);

if(!is_null($FEDIT->DbRecordSetBatch)) {

	$Giorni = 14;
	$DataGiorniFa = date ( 'Y-m-d', strtotime ( '-'.$Giorni.' day' . date('Y-m-d') ) );

	$sql_giorni="SELECT DTMOV, idSIS, idSIS_scheda FROM user_movimenti_fiscalizzati WHERE ID_BAT='".$FEDIT->DbRecordSetBatch[0]['ID_BAT']."' AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND  ".$SOGER->UserData['workmode']."=1";
	$sql_giorni.=" AND (DTMOV<'".$DataGiorniFa."' ";
	$sql_giorni.=" AND NMOV<>9999999 ";
	$sql_giorni.=" OR (idSIS IS NOT NULL OR idSIS_scheda IS NOT NULL)) ";

	$FEDIT->SDBRead($sql_giorni,"DbRecordSetGiorni", true, false);

	$Got_idSIS = false;
	$Got_idSIS_scheda = false;

	for($m=0;$m<count($FEDIT->DbRecordSetGiorni);$m++){
		if($FEDIT->DbRecordSetGiorni[$m]['idSIS']) $Got_idSIS = true;
		if($FEDIT->DbRecordSetGiorni[$m]['idSIS_scheda']) $Got_idSIS_scheda = true;
		}


	if( is_null($FEDIT->DbRecordSetGiorni) | ($SOGER->UserData['core_usersusr']=='sintem' AND !$Got_idSIS AND !$Got_idSIS_scheda) ){
	
	
		## elimino record da reg. fiscale
		$sql="DELETE FROM user_movimenti_fiscalizzati WHERE ID_BAT='".$FEDIT->DbRecordSetBatch[0]['ID_BAT']."' AND ".$SOGER->UserData['workmode']."=1 AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."'";
		$FEDIT->SDBWrite($sql,true,false);
			
	
		## rendo nuovamente fiscalizzabili record reg. industriale e elimino riferimento a batch e FKErifFisc
		$sql="UPDATE user_movimenti SET FISCALIZZATO=0, ID_BAT=NULL, FKErifFisc=NULL WHERE ID_BAT='".$FEDIT->DbRecordSetBatch[0]['ID_BAT']."' AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1 ";
		$FEDIT->SDBWrite($sql,true,false);
		
		
		## elimino record da tabella batch
		$sql="DELETE FROM batch WHERE ID_BAT='".$FEDIT->DbRecordSetBatch[0]['ID_BAT']."'";
		$FEDIT->SDBWrite($sql,true,false);

		echo "Fiscalizzazione annullata correttamente";
		}
	else{
		echo "Impossibile annullare l'ultima fiscalizzazione, comprende movimenti pi� vecchi di ".$Giorni." giorni o movimenti che sono stati inviati a SISTRI.";
		}

	}
else{
	echo "Nessuna fiscalizzazione eseguita, impossibile annullare";
	}
	
require_once("../__includes/COMMON_sleepForgEdit.php");
require_once("../__includes/COMMON_sleepSoger.php");

//header("location: ../");
?>