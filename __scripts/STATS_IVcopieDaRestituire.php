<?php
setlocale(LC_TIME, 'ita', 'it_IT');

$id_uimp_buffer = $FEDIT->DbRecordSet[0]['ID_UIMP'];

$sqlComuneImpianto = "SELECT lov_comuni_istat.description AS coumne, core_impianti.fax, core_impianti.description AS impianto FROM lov_comuni_istat JOIN core_impianti ON core_impianti.ID_COM=lov_comuni_istat.ID_COM WHERE core_impianti.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."'";
$FEDIT->SdbRead($sqlComuneImpianto,"DbRecordSetComuneImpianto",true,false);
$ComuneImpianto = $FEDIT->DbRecordSetComuneImpianto[0]['coumne'];
$FaxImpianto	= $FEDIT->DbRecordSetComuneImpianto[0]['fax'];
$NomeImpianto	= $FEDIT->DbRecordSetComuneImpianto[0]['impianto'];
$MailUtente		= $SOGER->UserData['core_usersemail'];
$NomeUtente		= $SOGER->UserData['core_usersnome'];
$CognomeUtente	= $SOGER->UserData['core_userscognome'];
$AnalyzedID_UIMP = array();

// distinct page per company
for($f=0;$f<count($FEDIT->DbRecordSet);$f++){

	if($FEDIT->DbRecordSet[$f]['ID_UIMP']<>$id_uimp_buffer){
		// add a new page
		$FEDIT->FGE_PdfBuffer->AddPage();
		}
		
	// top-margin for company header
	$Ypos=40;

	// luogo e data
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);	
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(170,4,$ComuneImpianto.",     ".strftime("%d %B %Y"),0,"L");

	// destinatario
	$Ypos=55;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);	
	$FEDIT->FGE_PdfBuffer->SetXY(110,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(80,4,"Spett.le",0,"L");
	$Ypos=60;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);	
	$FEDIT->FGE_PdfBuffer->SetXY(110,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(80,4,$FEDIT->DbRecordSet[$f]['PROD_RAGSOC'],0,"L");
	$Ypos=65;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);	
	$FEDIT->FGE_PdfBuffer->SetXY(110,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(80,4,$FEDIT->DbRecordSet[$f]['PROD_INDIRIZZO1'],0,"L");
	$Ypos=70;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);	
	$FEDIT->FGE_PdfBuffer->SetXY(110,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(80,4,$FEDIT->DbRecordSet[$f]['PROD_INDIRIZZO2'],0,"L");

	// oggetto
	$Ypos=120;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);	
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"OGGETTO: ",0,"L");
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);	
	$FEDIT->FGE_PdfBuffer->SetXY(40,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(70,4,"Restituzione IV copie dei formulari",0,"L");

	// formulari
	for($t=$f;$t<count($FEDIT->DbRecordSet);$t++){
		if(!in_array($FEDIT->DbRecordSet[$t]['ID_UIMP'], $AnalyzedID_UIMP) AND $FEDIT->DbRecordSet[$t]['ID_UIMP'] == $FEDIT->DbRecordSet[$f]['ID_UIMP']){
			$DTFORM = explode('-',$FEDIT->DbRecordSet[$t]['DTFORM']);
			$DTFORM = $DTFORM[2].'/'.$DTFORM[1].'/'.$DTFORM[0];
			$formulari[] = array('NFORM'=>$FEDIT->DbRecordSet[$t]['NFORM'], 'DTFORM'=>$DTFORM);
			}
		}
	$Ypos=115;
	for($i=0;$i<count($formulari);$i++){
		$Ypos+=5;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);	
		$FEDIT->FGE_PdfBuffer->SetXY(110,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(60,4,$formulari[$i]['NFORM']."   del   ".$formulari[$i]['DTFORM'],0,"L");
		}
	// reset formulari
	$formulari = array();
	$AnalyzedID_UIMP[] = $FEDIT->DbRecordSet[$f]['ID_UIMP'];

	// static text abt 10 fir 90+(5*10)
	$Ypos=180;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);	
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(170,4,"Allegata alle presente Vi inviamo copia di Vs. pertinenza dei Formulari Identificativi dei Rifiuti di cui all'oggetto.",0,"L");
	$Ypos=185;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);	
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(170,4,"Vi preghiamo di farci pervenire la presente via fax (".$FaxImpianto.") o mail (".$MailUtente.") debitamente firmata per ricevuta.",0,"L");
	$Ypos=195;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);	
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(170,4,"A Vs. disposizione per ogni ulteriore chiarimento, cogliamo l'occasione per ben distintamente salutarVi.",0,"L");

	// azienda e firma mittente
	$Ypos=210;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);	
	$FEDIT->FGE_PdfBuffer->SetXY(140,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,4,$NomeImpianto,0,"L");
	$Ypos=215;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);	
	$FEDIT->FGE_PdfBuffer->SetXY(140,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,4,$NomeUtente." ".$CognomeUtente,0,"L");

	// data e firma per ricevuta
	$Ypos=230;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);	
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(170,4,"Data e firma per ricevuta",0,"L");
	$Ypos=245;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);	
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(170,4,"_________________________",0,"L");

	$id_uimp_buffer = $FEDIT->DbRecordSet[$f]['ID_UIMP'];
	}

?>