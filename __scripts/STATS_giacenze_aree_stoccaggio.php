<?php

$Ypos = 60;
$AreaStoccaggio_buffer=0;
$actual_stock = 0;

for($a=0;$a<count($FEDIT->DbRecordSet);$a++){

	if($FEDIT->DbRecordSet[$a]['ID_ASD']!=$AreaStoccaggio_buffer){
		$actual_stock = 0;
		$Ypos += 15;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(150,5,$FEDIT->DbRecordSet[$a]['AreaStoccaggio'],"B","L");
		$FEDIT->FGE_PdfBuffer->SetXY(160,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,5,$FEDIT->DbRecordSet[$a]['max_stock']." kg","B","R");
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		}

	$actual_stock += $FEDIT->DbRecordSet[$a]['giacenza'];

	$Ypos += 8;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(140,5,$FEDIT->DbRecordSet[$a]['COD_CER']." ".$FEDIT->DbRecordSet[$a]['descrizione'],"","L");
	$FEDIT->FGE_PdfBuffer->SetXY(160,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(40,5,$FEDIT->DbRecordSet[$a]['giacenza']." kg","","R");

	// total
	if( ($a==count($FEDIT->DbRecordSet)-1) || $FEDIT->DbRecordSet[$a+1]['ID_ASD']!=$FEDIT->DbRecordSet[$a]['ID_ASD'] ){
		$Ypos += 8;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(150,5,"Saldo acquistabile","T","L");
		$FEDIT->FGE_PdfBuffer->SetXY(160,$Ypos);
		$SaldoAcquistabile = $FEDIT->DbRecordSet[$a]['max_stock'] - $actual_stock; 
		$FEDIT->FGE_PdfBuffer->MultiCell(40,5,$SaldoAcquistabile." kg","T","R");
		}

	$AreaStoccaggio_buffer = $FEDIT->DbRecordSet[$a]['ID_ASD'];
	}

?>