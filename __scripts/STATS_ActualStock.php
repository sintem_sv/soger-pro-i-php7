<?php

	$TotC = 0;
	$TotS = 0;
	$TotM3 = 0;
	$TotC_general = 0;
	$TotS_general = 0;
	$TotGiacIni = 0;
	$TotC_P = 0;
	$TotS_P = 0;
	$TotM3_P = 0;
	$TotC_general_P = 0;
	$TotS_general_P = 0;
	$TotGiacIni_P = 0;
	$TotC_NP = 0;
	$TotS_NP = 0;
	$TotM3_NP = 0;
	$TotC_general_NP = 0;
	$TotS_general_NP = 0;
	$TotGiacIni_NP = 0;
	$RIFbuf = "";
	$counter = 0;
	$Ypos = 33;
	$NPfound = false;
	$NPTotM3 = false;
	$NPTotL = false;
	$NPTotKg = false;
	$Pfound = false;
	$PTotM3 = false;
	$PTotL = false;
	$PTotKg = false;

	$Ypos+=30;

	CheckYPos($Ypos,$PdfObj,$asCSV);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	
	$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,"Cer",0,"C");
	
	$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(60,5,"Descrizione",0,"C");
	
	$FEDIT->FGE_PdfBuffer->SetXY(85,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(60,5,"Stato fisico",0,"C");

	//$FEDIT->FGE_PdfBuffer->SetXY(145,$Ypos);
	//$FEDIT->FGE_PdfBuffer->MultiCell(10,5,"U.M.",0,"C");
	
	$FEDIT->FGE_PdfBuffer->SetXY(145,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,3.5,"Stoc. Max",0,"C");
	if($FEDIT->DbRecordSet[0]['ID_GDEP']=='1'){
		$FEDIT->FGE_PdfBuffer->SetXY(165,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,3.5,"gg giacenza",0,"C");
		}

	$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(45,5,"Stoccaggio",0,"C");
	
	$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos+5);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,"Kg.",0,"C");
	
	$FEDIT->FGE_PdfBuffer->SetXY(195,$Ypos+5);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,"Litri",0,"C");
	
	$FEDIT->FGE_PdfBuffer->SetXY(210,$Ypos+5);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,"Mc.",0,"C");
	
	$FEDIT->FGE_PdfBuffer->SetXY(225,$Ypos);

	$YEAR = substr($FEDIT->DbServerData["db"], -4);
	$ShowClassiH = $YEAR<2016? true:false;
    if($ShowClassiH){
		$FEDIT->FGE_PdfBuffer->MultiCell(30,5,"Classi H di pericolo ",0,"C");
		$FEDIT->FGE_PdfBuffer->SetXY(255,$Ypos);
		}

	$FEDIT->FGE_PdfBuffer->MultiCell(30,5,"Classi HP di pericolo ",0,"C");

	$Ypos +=12;
	CheckYPos($Ypos,$PdfObj,$asCSV);
	
	foreach($FEDIT->DbRecordSet as $k=>$dati) {
		if($dati["RifID"]!=$RIFbuf) {
			if($counter>0) {
				StampaRifiuto($datiRif,$FEDIT->FGE_PdfBuffer,$Ypos,$NPfound,$Pfound,$TotM3,$NPTotM3,$PTotM3,$TotKg,$NPTotKg,$PTotKg,$TotL,$NPTotL,$PTotL,$TotKg_P,$TotKg_NP,$TotL_P,$TotL_NP,$TotM3_P,$TotM3_NP);
			}
			$RIFbuf = $dati["RifID"];
			$SQL="SELECT id FROM user_movimenti_giacenze_iniziali WHERE ID_RIF=".$RIFbuf;
			$FEDIT->SDBRead($SQL,"DbRecordSetGiacenzaIniziale",true,false);
			$giac_ini = ($FEDIT->DbRecsNum>0? 0:$dati["giac_ini"]);
			$datiRif = array(
				"PER"=>$dati["PER"],
				"CER"=>$dati["COD_CER"],
				"RifID"=>$dati["RifID"],
				"originalID_RIF"=>$dati["originalID_RIF"],
				"COD"=>$dati["cod_pro"],
				"DES"=>$dati["descrizione"],
				"ADR"=>$dati["adr"],
				"PSPEC"=>$dati["peso_spec"],
				"stato_fisico"=>$dati["stato_fisico"],
				"IDUM"=>$dati["ID_UMIS"],
				"UM"=>$dati["UnMis"],
				"H1"=>$dati["H1"],
				"H2"=>$dati["H2"],
				"H3A"=>$dati["H3A"],
				"H3B"=>$dati["H3B"],
				"H4"=>$dati["H4"],
				"H5"=>$dati["H5"],
				"H6"=>$dati["H6"],
				"H7"=>$dati["H7"],
				"H8"=>$dati["H8"],
				"H9"=>$dati["H9"],
				"H10"=>$dati["H10"],
				"H11"=>$dati["H11"],
				"H12"=>$dati["H12"],
				"H13"=>$dati["H13"],
				"H14"=>$dati["H14"],
				"H15"=>$dati["H15"],
				"HP1"=>$dati["HP1"],
				"HP2"=>$dati["HP2"],
				"HP3"=>$dati["HP3"],
				"HP4"=>$dati["HP4"],
				"HP5"=>$dati["HP5"],
				"HP6"=>$dati["HP6"],
				"HP7"=>$dati["HP7"],
				"HP8"=>$dati["HP8"],
				"HP9"=>$dati["HP9"],
				"HP10"=>$dati["HP10"],
				"HP11"=>$dati["HP11"],
				"HP12"=>$dati["HP12"],
				"HP13"=>$dati["HP13"],
				"HP14"=>$dati["HP14"],
				"HP15"=>$dati["HP15"],
				"plus"=>0,
				"giac_ini"=>$giac_ini,
				"minus"=>0,
				"QLIM"=>$dati["q_limite"],
				"TLIM"=>$dati["t_limite"],
				"ID_GDEP"=>$dati["ID_GDEP"],
				"MAXVAR"=>$dati["MaxVar"]
			);
		}
		
		$TotGiacIni	  += $datiRif["giac_ini"];
		if($dati["PER"]=='1')
			$TotGiacIni_P += $datiRif["giac_ini"];
		else
			$TotGiacIni_NP += $datiRif["giac_ini"];

		# lettura sulla tabella dei movimenti e delle giacenze iniziali automatiche
		$sql = "SELECT 0 AS isGiacIniAuto, quantita, TIPO, ID_RIF, DTMOV, NMOV ";
		$sql.= "FROM ".$MovTable." ";
		$sql.= "WHERE ".$MovTable.".ID_RIF='".$dati["RifID"]."' AND NMOV<>9999999 ";
		$sql.= "AND ".$MovTable.".ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' ";
		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");

		$sql2 = "SELECT 1 AS isGiacIniAuto, quantita, TIPO, ID_RIF, DTMOV, NMOV ";
		$sql2.= "FROM user_movimenti_giacenze_iniziali ";
		$sql2.= "WHERE ID_RIF='".$dati["RifID"]."'";
		$sql2.= "AND user_movimenti_giacenze_iniziali.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' ";

		$sqlUnion = "(".$sql.") UNION (".$sql2.") ";
		$sqlUnion.= "ORDER BY isGiacIniAuto DESC, NMOV ASC, DTMOV ASC";

		$FEDIT->SdbRead($sqlUnion,"DbRecordSetTmp",true,false);
		//print_r($FEDIT->DbRecordSetTmp);
		if(isset($FEDIT->DbRecordSetTmp)){
			for($m=0;$m<count($FEDIT->DbRecordSetTmp);$m++){				
				if($FEDIT->DbRecordSetTmp[$m]["TIPO"]=="C") {
					$datiRif["carico"][] = array('qta' => $FEDIT->DbRecordSetTmp[$m]["quantita"], 'num' => $FEDIT->DbRecordSetTmp[$m]["NMOV"], 'data' => $FEDIT->DbRecordSetTmp[$m]["DTMOV"]);
					$datiRif["plus"] += $FEDIT->DbRecordSetTmp[$m]["quantita"];	
				} else {
					$datiRif["minus"] += $FEDIT->DbRecordSetTmp[$m]["quantita"];
				}
			}
			$TotC_general += $datiRif["plus"];
			if($dati["PER"]=='1')
				$TotC_general_P += $datiRif["plus"];
			else
				$TotC_general_NP += $datiRif["plus"];
			
			
			$TotS_general += $datiRif["minus"];
			if($dati["PER"]=='1')
				$TotS_general_P += $datiRif["minus"];
			else
				$TotS_general_NP += $datiRif["minus"];

			}
		$counter++;	
		}




	StampaRifiuto($datiRif,$FEDIT->FGE_PdfBuffer,$Ypos,$NPfound,$Pfound,$TotM3,$NPTotM3,$PTotM3,$TotKg,$NPTotKg,$PTotKg,$TotL,$NPTotL,$PTotL,$TotKg_P,$TotKg_NP,$TotL_P,$TotL_NP,$TotM3_P,$TotM3_NP,$asCSV);

	$Ypos -= 2;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
	//$FEDIT->FGE_PdfBuffer->SetXY(163.5,$Ypos);

	$FEDIT->FGE_PdfBuffer->SetXY(135,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(45,5,"Totale ","TLRB","R");
	$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,round($TotKg,2),"TLRB","C");
	$FEDIT->FGE_PdfBuffer->SetXY(195,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,round($TotL,2),"TLRB","C");
	$FEDIT->FGE_PdfBuffer->SetXY(210,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,round($TotM3,2),"TLRB","C");

	$Ypos+=7;

	$FEDIT->FGE_PdfBuffer->SetXY(135,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(45,5,"Totale rifiuti pericolosi","TLRB","R");
	$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,round($TotKg_P,2),"TLRB","C");
	$FEDIT->FGE_PdfBuffer->SetXY(195,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,round($TotL_P,2),"TLRB","C");
	$FEDIT->FGE_PdfBuffer->SetXY(210,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,round($TotM3_P,2),"TLRB","C");

	$Ypos+=7;

	$FEDIT->FGE_PdfBuffer->SetXY(135,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(45,5,"Totale rifiuti non pericolosi","TLRB","R");
	$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,round($TotKg_NP,2),"TLRB","C");
	$FEDIT->FGE_PdfBuffer->SetXY(195,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,round($TotL_NP,2),"TLRB","C");
	$FEDIT->FGE_PdfBuffer->SetXY(210,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,round($TotM3_NP,2),"TLRB","C");

	
	$PTotM3 = true;
	$TotM3 = 0;
	$PTotKg = true;
	$TotKg = 0;
	$PTotL = true;
	$TotL = 0;
	
	$TotM3_P = 0;
	$TotKg_P = 0;
	$TotL_P = 0;
	
	$TotM3_NP = 0;
	$TotKg_NP = 0;	
	$TotL_NP = 0;


function YN($in) {
	if($in=="1") {
		return "si";
	} else {
		return "no";	
	}
}

function YNimg($in) {
	if($in=="1") {
		return "../__css/CHECKY.png";
	} else {
		return "../__css/CHECKN.png";
	}
}


function StampaRifiuto(&$dati,&$PdfObj,&$Ypos,&$NPfound,&$Pfound,&$TotM3,&$NPTotM3,&$PTotM3,&$TotKg,&$NPTotKg,&$PTotKg,&$TotL,&$NPTotL,&$PTotL,&$TotKg_P,&$TotKg_NP,&$TotL_P,&$TotL_NP,&$TotM3_P,&$TotM3_NP){

	CheckYPos($Ypos,$PdfObj,$asCSV);

	# CER
	$PdfObj->SetFont('Helvetica','',8);
	$PdfObj->SetXY(10,$Ypos);
	$PdfObj->MultiCell(15,5,$dati["CER"],0,"C");

	# DESCRIZIONE
	$PdfObj->SetXY(25,$Ypos+0.6);
	if(strlen($dati["DES"])>110) {
		$PdfObj->MultiCell(60,3.5,substr($dati["COD"]." ".$dati["DES"],0,-25)." [..]",0,"L");
		} 
	else {
		$PdfObj->MultiCell(60,3.5,$dati["COD"]." ".$dati["DES"],0,"L");
		}

	# STATO FISICO
	$PdfObj->SetXY(85,$Ypos);
	$PdfObj->SetFont('Helvetica','',8);
	$PdfObj->MultiCell(60,5,$dati["stato_fisico"],0,"L");

	# UNITA' MISURA
	//$PdfObj->SetXY(145,$Ypos);
	//$PdfObj->MultiCell(10,5,$dati["UM"],0,"C");
	
	# STOCK MAX
	$PdfObj->SetXY(145,$Ypos);
	switch($dati['ID_GDEP']){
		case '1':	//temporale
			if(!is_null($dati["TLIM"]))
				$PdfObj->MultiCell(20,5,$dati["TLIM"] . " gg",0,"C");
			break;
		case '2':	//volumetrico
			if(!is_null($dati["QLIM"]))
				$PdfObj->MultiCell(20,5,$dati["QLIM"] . " m3",0,"C");
			break;
			}

	# GG GIACENZA
	$CarichiRifiuto = array();
	$datiToCheck['totS'] = $dati['minus'];
	$datiToCheck['totC'] = $dati['plus'];
	$datiToCheck['RifID'] = $dati['RifID'];
	$datiToCheck['giac_ini'] = $dati['giac_ini'];
	
	if(($datiToCheck['totC']+$dati['giac_ini'])>$datiToCheck['totS']){
		$datiToCheck['carico'] = $dati['carico'];
		$string=CheckCarichi($datiToCheck, $CarichiRifiuto);
		$result=explode("|",$string);

		if((float)$datiToCheck['giac_ini']==0 OR (float)$datiToCheck['totS']>=(float)$datiToCheck['giac_ini']){
			$ggGiacenza=dateDiff("d", $result[1], date('Y-m-d'))." gg";
			}
		else
			$ggGiacenza="Giacenza iniziale";
		}
	else
		$ggGiacenza='-';



	if($dati['ID_GDEP']=='1'){
		$PdfObj->SetXY(165,$Ypos);
		$PdfObj->MultiCell(15,5,$ggGiacenza,0,"C");
		}

	###
	$Qtot = $dati["plus"]+$dati["giac_ini"]-$dati["minus"];
	$TotC = $dati["plus"];
	$TotS = $dati["minus"];

	switch($dati["IDUM"]) {
		case "1":	# KG
		$KG = $Qtot;  
		$M3 = KgQConvert($Qtot,"M3",$dati["PSPEC"]);
		$L = KgQConvert($Qtot,"Litri",$dati["PSPEC"]);
		$BKg='B';
		$BL ='';
		$BM3='';
		break;
		case "2":	# LT
		$L = $Qtot;
		if($Qtot!=0) {
			$KG = round($Qtot*$dati["PSPEC"],2);
		} else {
			$KG = 0;
		}
		if($KG!=0) {
			$M3 = round(KgQConvert($KG,"M3",$dati["PSPEC"]),2);
		} else {
			$M3 = 0;
		}
		$BKg='';
		$BL ='B';
		$BM3='';
		break;
		case "3":	# METRI CUBI
			$M3 = $Qtot; 
			$KG = round($Qtot*$dati["PSPEC"]*1000,2);
			$L = round($Qtot*$dati["PSPEC"],2);
		$BKg='';
		$BL ='';
		$BM3='B';
		break;		
	}
	
	if($dati["PER"]=="1"){ 
		$TotM3_P += round($M3,2);
		$TotKg_P += round($KG,2);
		$TotL_P  += round($L,2);
		$TotC_general_P += $TotC;
		$TotS_general_P += $TotS;
		$TotGiacIni_P   += $dati["giac_ini"];
		}
	else{
		$TotM3_NP += round($M3,2);
		$TotKg_NP += round($KG,2);
		$TotL_NP  += round($L,2);
		$TotC_general_NP += $TotC;
		$TotS_general_NP += $TotS;
		$TotGiacIni_NP   += $dati["giac_ini"];
		}

	$TotM3 += round($M3,2);
	$TotKg += round($KG,2);
	$TotL  += round($L,2);
	$TotC_general += $TotC;
	$TotS_general += $TotS;
	$TotGiacIni   += $dati["giac_ini"];

	# STOCCAGGIO KG, LITRI, MC
	$PdfObj->SetXY(180,$Ypos);
	$PdfObj->SetFont('Helvetica',$BKg,8);
	$PdfObj->MultiCell(15,5,round($KG,2),0,"C");
	$PdfObj->SetXY(195,$Ypos);
	$PdfObj->SetFont('Helvetica',$BL,8);
	$PdfObj->MultiCell(15,5,round($L,2),0,"C");
	$PdfObj->SetXY(210,$Ypos);
	$PdfObj->SetFont('Helvetica',$BM3,8);
	$PdfObj->MultiCell(15,5,round($M3,2),0,"C");
	
	$PdfObj->SetXY(225,$Ypos);
	# CLASSI H
	$YEAR = substr($_SESSION["DbInUse"], -4);
	$ShowClassiH = $YEAR<2016? true:false;
	if($ShowClassiH){		
		if($dati["H1"]=="1")  $ClassiH.="H1 ";
		if($dati["H2"]=="1")  $ClassiH.="H2 ";
		if($dati["H3A"]=="1") $ClassiH.="H3A ";
		if($dati["H3B"]=="1") $ClassiH.="H3B ";
		if($dati["H4"]=="1")  $ClassiH.="H4 ";
		if($dati["H5"]=="1")  $ClassiH.="H5 ";
		if($dati["H6"]=="1")  $ClassiH.="H6 ";
		if($dati["H7"]=="1")  $ClassiH.="H7 ";
		if($dati["H8"]=="1")  $ClassiH.="H8 ";
		if($dati["H9"]=="1")  $ClassiH.="H9 ";
		if($dati["H10"]=="1") $ClassiH.="H10 ";
		if($dati["H11"]=="1") $ClassiH.="H11 ";
		if($dati["H12"]=="1") $ClassiH.="H12 ";
		if($dati["H13"]=="1") $ClassiH.="H13 ";
		if($dati["H14"]=="1") $ClassiH.="H14 ";
		if($dati["H15"]=="1") $ClassiH.="H15 ";
		$PdfObj->MultiCell(30,5,$ClassiH,"","L");
		$PdfObj->SetXY(255,$Ypos);
		}

	# CLASSI HP
	if($dati["HP1"]=="1")  $ClassiHP.="HP1 ";
	if($dati["HP2"]=="1")  $ClassiHP.="HP2 ";
	if($dati["HP3"]=="1")  $ClassiHP.="HP3 ";
	if($dati["HP4"]=="1")  $ClassiHP.="HP4 ";
	if($dati["HP5"]=="1")  $ClassiHP.="HP5 ";
	if($dati["HP6"]=="1")  $ClassiHP.="HP6 ";
	if($dati["HP7"]=="1")  $ClassiHP.="HP7 ";
	if($dati["HP8"]=="1")  $ClassiHP.="HP8 ";
	if($dati["HP9"]=="1")  $ClassiHP.="HP9 ";
	if($dati["HP10"]=="1") $ClassiHP.="HP10 ";
	if($dati["HP11"]=="1") $ClassiHP.="HP11 ";
	if($dati["HP12"]=="1") $ClassiHP.="HP12 ";
	if($dati["HP13"]=="1") $ClassiHP.="HP13 ";
	if($dati["HP14"]=="1") $ClassiHP.="HP14 ";
	if($dati["HP15"]=="1") $ClassiHP.="HP15 ";
	$PdfObj->MultiCell(30,5,$ClassiHP,"","L");

	
	$Ypos +=12;	
	CheckYPos($Ypos,$PdfObj,$asCSV);
	}

?>