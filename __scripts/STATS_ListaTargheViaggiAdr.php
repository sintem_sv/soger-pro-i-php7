<?php

//print_r($FEDIT->DbRecordSet);

$AZTbuffer="";
$TARGAbuffer="00000";
$RIFbuffer="";
$counterTr=-1;
$counterTa=0;
$counterRif=0;
$counterFir=0;

for($t=0;$t<count($FEDIT->DbRecordSet);$t++){
	if($FEDIT->DbRecordSet[$t]['trasportatore']!=$AZTbuffer){
		$counterTr++;
		$counterTa=0;
		$counterRif=0;
		$counterFir=0;
		$Trasportatori[$counterTr]['trasportatore']=$FEDIT->DbRecordSet[$t]['trasportatore'];
		$Trasportatori[$counterTr]['targhe'][$counterTa]['automezzo']=$FEDIT->DbRecordSet[$t]['targa'];
		$Trasportatori[$counterTr]['targhe'][$counterTa]['rifiuti'][$counterRif]['rifiuto']=$FEDIT->DbRecordSet[$t]['cer']." - ".$FEDIT->DbRecordSet[$t]['rifiuto']."||".$FEDIT->DbRecordSet[$t]['onu'];
		$Trasportatori[$counterTr]['targhe'][$counterTa]['rifiuti'][$counterRif]['formulari'][$counterFir]['fir']=$FEDIT->DbRecordSet[$t]['formulario']."||".$FEDIT->DbRecordSet[$t]['um']."||".$FEDIT->DbRecordSet[$t]['quantita'];
		$AZTbuffer=$FEDIT->DbRecordSet[$t]['trasportatore'];
		if($FEDIT->DbRecordSet[$t]['targa']!="") $TARGAbuffer=$FEDIT->DbRecordSet[$t]['targa']; else $TARGAbuffer="00000";
		$RIFbuffer=$FEDIT->DbRecordSet[$t]['cer']." - ".$FEDIT->DbRecordSet[$t]['rifiuto']."||".$FEDIT->DbRecordSet[$t]['onu'];
		}
	else{ 
		// stesso trasportatore
		// stessa targa?
		if($FEDIT->DbRecordSet[$t]['targa']!=$TARGAbuffer){
			$counterTa++;
			$counterRif=0;
			$counterFir=0;
			$Trasportatori[$counterTr]['targhe'][$counterTa]['automezzo']=$FEDIT->DbRecordSet[$t]['targa'];
			$Trasportatori[$counterTr]['targhe'][$counterTa]['rifiuti'][$counterRif]['rifiuto']=$FEDIT->DbRecordSet[$t]['cer']." - ".$FEDIT->DbRecordSet[$t]['rifiuto']."||".$FEDIT->DbRecordSet[$t]['onu'];
			$Trasportatori[$counterTr]['targhe'][$counterTa]['rifiuti'][$counterRif]['formulari'][$counterFir]['fir']=$FEDIT->DbRecordSet[$t]['formulario']."||".$FEDIT->DbRecordSet[$t]['um']."||".$FEDIT->DbRecordSet[$t]['quantita'];
			if($FEDIT->DbRecordSet[$t]['targa']!="") $TARGAbuffer=$FEDIT->DbRecordSet[$t]['targa']; else $TARGAbuffer="00000";
			$RIFbuffer=$FEDIT->DbRecordSet[$t]['cer']." - ".$FEDIT->DbRecordSet[$t]['rifiuto']."||".$FEDIT->DbRecordSet[$t]['onu'];
			}
		else{	
			// targa uguale
			// rifiuto uguale?
			if($FEDIT->DbRecordSet[$t]['cer']." - ".$FEDIT->DbRecordSet[$t]['rifiuto']."||".$FEDIT->DbRecordSet[$t]['onu']!=$RIFbuffer){
				$counterRif++;
				$counterFir=0;
				$Trasportatori[$counterTr]['targhe'][$counterTa]['rifiuti'][$counterRif]['rifiuto']=$FEDIT->DbRecordSet[$t]['cer']." - ".$FEDIT->DbRecordSet[$t]['rifiuto']."||".$FEDIT->DbRecordSet[$t]['onu'];
				$Trasportatori[$counterTr]['targhe'][$counterTa]['rifiuti'][$counterRif]['formulari'][$counterFir]['fir']=$FEDIT->DbRecordSet[$t]['formulario']."||".$FEDIT->DbRecordSet[$t]['um']."||".$FEDIT->DbRecordSet[$t]['quantita'];
				$RIFbuffer=$FEDIT->DbRecordSet[$t]['cer']." - ".$FEDIT->DbRecordSet[$t]['rifiuto']."||".$FEDIT->DbRecordSet[$t]['onu'];
				}
			else{ //rifiuto uguale
				$counterFir++;
				$Trasportatori[$counterTr]['targhe'][$counterTa]['rifiuti'][$counterRif]['formulari'][$counterFir]['fir']=$FEDIT->DbRecordSet[$t]['formulario']."||".$FEDIT->DbRecordSet[$t]['um']."||".$FEDIT->DbRecordSet[$t]['quantita'];
				}
			}
		}	
	}

//print_r($Trasportatori);

$Xpos = 20;
$Ypos = 50;


for($t=0;$t<count($Trasportatori);$t++){
	$Xpos = 20;
	$Ypos+= 10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(260,4,$Trasportatori[$t]['trasportatore'],"BT","C");
	$Ypos+=6;

	//intestazione
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',6);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);	
	$FEDIT->FGE_PdfBuffer->MultiCell(20,2,"Targa","","C");
	$Xpos+=20;
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);	
	$FEDIT->FGE_PdfBuffer->MultiCell(25,2,"N� movimenti effettuati","","C");
	$Xpos+=25;
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);	
	$FEDIT->FGE_PdfBuffer->MultiCell(150,2,"Rifiuto","","C");
	$Xpos+=150;
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);	
	$FEDIT->FGE_PdfBuffer->MultiCell(20,2,"Quantit�","","C");
	$Xpos+=20;
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);	
	$FEDIT->FGE_PdfBuffer->MultiCell(15,2,"N. ONU","","C");
	$Xpos+=15;
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);	
	$FEDIT->FGE_PdfBuffer->MultiCell(30,2,"Formulari","","C");

	$Ypos+=6;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	for($a=0;$a<count($Trasportatori[$t]['targhe']);$a++){
		$Xpos = 20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

		//targa
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',6);
		if($Trasportatori[$t]['targhe'][$a]['automezzo']!="") 
			$automezzo=$Trasportatori[$t]['targhe'][$a]['automezzo']; 
		else 
			$automezzo="non indicato";
		$FEDIT->FGE_PdfBuffer->MultiCell(20,3,$automezzo,"","C");
		$Xpos+=45;

		for($r=0;$r<count($Trasportatori[$t]['targhe'][$a]['rifiuti']);$r++){
			//rifiuto
			$nmov=0;
			$qta=0;
			$Xpos=65;
			$rif=explode("||",$Trasportatori[$t]['targhe'][$a]['rifiuti'][$r]['rifiuto']);
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',6);
			$FEDIT->FGE_PdfBuffer->MultiCell(150,3,$rif[0],"","L");
			$Xpos+=170;

			//onu
			if($rif[1]=="") $rif[1]="n.d.";
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',6);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,3,$rif[1],"","C");

			//nmov			
			$nmov=count($Trasportatori[$t]['targhe'][$a]['rifiuti'][$r]['formulari']);
			$FEDIT->FGE_PdfBuffer->SetXY(40,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',6);
			$FEDIT->FGE_PdfBuffer->MultiCell(25,3,$nmov,"","C");
		
			//qta in KG
			$qta=0;
			for($f=0;$f<$nmov;$f++){
				$fir_um_ps=explode("||",$Trasportatori[$t]['targhe'][$a]['rifiuti'][$r]['formulari'][$f]['fir']);
				$qta+=$fir_um_ps[2];
				}
			$Xpos=215;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',6);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,3, $qta." Kg","","R");
			$qta=0;

			$Xpos=250;
			for($f=0;$f<$nmov;$f++){
				if($f==0) $PageStart=$FEDIT->FGE_PdfBuffer->PageNo();

				//formulario
				$fir_um_ps=explode("||",$Trasportatori[$t]['targhe'][$a]['rifiuti'][$r]['formulari'][$f]['fir']);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',6);
				$FEDIT->FGE_PdfBuffer->MultiCell(30,3,$fir_um_ps[0],"","C");
				$Ypos+=3;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				}
			$nmov=0;
			
			if($FEDIT->FGE_PdfBuffer->PageNo()==$PageStart){
				$Ypos-=$up;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				}

			}
		
		$Ypos+=6;
		}
	
	}

?>