<?php
session_start();
require_once("Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");


if(isset($_POST['filter'])){
	if(isset($_SESSION['SearchingFilter'])) unset($_SESSION['SearchingFilter']);
	$_SESSION['SearchingFilter']=$_POST['filter'];
	}

else{

	if(!isset($_GET["area"])) {	
		$SOGER->AppLocation		= $SOGER->AppBackStatus;
		$_SESSION['PressBack']	= true;
		} 
	else {
		if($_GET["area"]=="SOGER_logoff") {

			if(!isset($_GET['SessionExpired'])){
				# log access
				$LogSql  = "INSERT INTO core_logs (`table` , `pri_key` , `ID_USR` , `ID_IMP` , `sqlType` , `sqlCommand` ) ";
				$LogSql .= "VALUES ('login-logout','--','".$SOGER->UserData["core_usersID_USR"]."','".$SOGER->UserData["core_usersID_IMP"]."','logout','--')";
				$FEDIT->SDBWrite($LogSql,true,false);
				}
			
			session_unset();
			session_destroy();
			header("location: ../");
		} else {
			switch($_GET["area"]) {

				case "UserMainMenu":
				case "ResponsabileMenu":
				case "AdminMenu":
					unset($_SESSION['PrevAppLocation']);
					break;


### OPERATORE
				case "UserStatsCfg":
					if($_GET['Stat']=='SoggCER' && $SOGER->UserData["core_usersO3"]=="0"){
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per la stampa della lista fornitori [cod. O3]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ../");
						}
					break;
				case "UserSchedeRifiutiRicCS":
					if($SOGER->UserData["core_usersO4"]=="0") {
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per effettuare richieste di carico / scarico [cod. O4]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ../");
					}
					break;
				case "UserNuovoMovCarico":
				case "UserNuovoMovScarico":
				case "UserNuovoMovCaricoF":
				case "UserNuovoMovScaricoF":
				case "UserNuovaSchedaSistri":
				case "UserNuovaSchedaSistriF":
				case "UserNuovoMovScaricoInternoF":
					if($SOGER->UserData["core_usersO5"]=="0") {
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per la creazione/modifica dei movimenti [cod. O5]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ../");
					} else {

						## PER CARICO/SCARICO DA SCHEDA RIF E AVVISI
						if(isset($_SESSION['MovimentoRIF'])){
							unset($_SESSION["MovimentoRIF"]);
							}
						if(isset($_SESSION['MovimentoRIF_quantita'])){
							unset($_SESSION["MovimentoRIF_quantita"]);
							}
						$_SESSION["MovimentoRIF"]=$_GET['ID_RIF'];
						$_SESSION["MovimentoRIF_quantita"]=$_GET['MovimentoRIF_quantita'];

						include("../__includes/USER_MovFlushSessionVars.php");
						$SOGER->AppLocation=$_GET["area"];
					}
					break;
				case "UserInformazioni":
					if($SOGER->UserData["core_usersO8"]=="0") {
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per la generazione delle statistiche [cod. O8]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ../");
					}
					break;
				case "UserDB":
					if($SOGER->UserData["core_usersO9"]=="0"){
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere al pannello \"database\" [cod. O9]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ".$_SERVER['HTTP_REFERER']);
						die();
						}
					break;
				case "UserRegistrazione":
					if($SOGER->UserData["core_usersO10"]=="0"){
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere al pannello \"dati utente\" [cod. O10]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ".$_SERVER['HTTP_REFERER']);
						die();
						}
					break;
				case "UserConfigStampe":
					if($SOGER->UserData["core_usersO11"]=="0"){
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere al pannello \"stampe\" [cod. O11]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ".$_SERVER['HTTP_REFERER']);
						die();
						}
					break;

				case "UserVediMovimenti":
					include("../__includes/USER_MovFlushSessionVars.php");		
					$SOGER->AppLocation=$_GET["area"];
					break;
				case "UserVediMovimentiFanghi":
					include("../__includes/USER_MovFlushSessionVars.php");		
					$SOGER->AppLocation=$_GET["area"];
					break;
### GESTORE
				case "UserBudgetRif":
					if(!$SOGER->UserData["core_impiantiMODULO_ECO"]=="1" | $SOGER->UserData["core_usersG3"]=="0") {
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere alla gestione economica [cod. G3]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ../");
					}
					break;
				case "UserSchedeSicurezza":
					if($SOGER->UserData["core_usersG6"]=="0") {
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per creare le procedure di sicurezza [cod. G6]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ../");
					}
					break;
				case "UserGestioneDeposito_organizza":
					if($SOGER->UserData["core_usersG8"]=="0"){
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere all'organizzazione del deposito [cod. G8]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ../");
						}
					break;

				case "SDG_index":
					if($SOGER->UserData["core_usersG11"]=="0"){
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere alla visualizzazione degli indici di gestione [cod. G11]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ../");
						}
					break;
				case "UserStatsCfg":
					if($_GET['Stat']=='ControlloDeposito' && $SOGER->UserData["core_usersG12"]=="0"){
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per la stampa della statistica di controllo del deposito [cod. G12]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ../");
						}
					break;

				case "UserSchedaRifiutoCaratt":
					if($SOGER->UserData["core_usersG13"]=="0"){
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per la generazione delle schede di caratterizzazione [cod. G13]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ../");
						}
					break;

### DELEGATO
				case "RegistroFiscale":
				case "RegistroUnicoPD":
					if($SOGER->UserData["core_usersD4"]=="0") {
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per la stampa del Registro di Carico/Scarico fiscale [cod. D4]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ../");
					}
					break;

				case "UserRegistrazioneIntestatario":
					if($SOGER->UserData["core_usersD6"]=="0"){
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per l'accesso al pannello Registrazione [cod. D6]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ../");
						}
					break;

### RESPONSABILE
				case "SDG_cea":
					if($SOGER->UserData["core_usersR1"]=="0"){
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere alla visualizzazione delle classi di efficienza [cod. R1]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ../");
						}
					break;

				case "UserControlPack":
					if($SOGER->UserData["core_usersR2"]=="0"){
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere alle statistiche di controllo movimentazione [cod. R2]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ../");
						}
					break;

				case "ISO14001":
					if($SOGER->UserData["core_usersR3"]=="0"){
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere al Registro adempimenti [cod. R3]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ../");
						}
					break;

				case "ConfigNonConformita":
					if($SOGER->UserData["core_usersR4"]=="0"){
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere alla Configurazione delle non conformitą [cod. R4]","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ../");
						}
					break;

				case "UserConfigControlli":
					if($SOGER->UserData["core_usersR5"]=="0") {
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per gestire i controlli [cod. R5]","1");
						//require_once("../__includes/COMMON_sleepSoger.php");
						//header("location: ../");
					}				
					break;

				case "UserConfigAllarmi":
					if($SOGER->UserData["core_usersR6"]=="0") {
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per gestire gli allarmi [cod. R6]","1");
						//require_once("../__includes/COMMON_sleepSoger.php");
						//header("location: ../");
					}				
					break;

				case "UserConfigCostiViaggio":
					if($SOGER->UserData["core_usersR7"]=="0") {
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per gestire i costi viaggio [cod. R7]","1");
						//require_once("../__includes/COMMON_sleepSoger.php");
						//header("location: ../");
					}				
					break;



				case "ConferimentoRifiuto":
					if($SOGER->UserData["core_usersO4"]=="0") {
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per richiedere il conferimento di un rifiuto","1");
						require_once("../__includes/COMMON_sleepSoger.php");
						header("location: ../");
					}
					if(isset($_SESSION['CR_ID_IRF'])) unset($_SESSION['CR_ID_IRF']);
					$_SESSION['CR_ID_IRF']=$_GET['filter'];
					break;
			}
			
			$SOGER->AppLocation=$_GET["area"];
		}	
	}

} // chiude if isset $_POST['filter']

require_once("../__includes/COMMON_sleepSoger.php");

if(isset($_GET["action"]) | isset($_GET["FGE_action"])) {
	header("location: ../?" . $_SERVER["QUERY_STRING"]);
} else {
	header("location: ../");
}
?>
