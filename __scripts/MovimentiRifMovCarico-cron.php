<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

//session_start();
require_once("ForgEdit_includes.inc");
require_once("../__classes/SogerMailer.php");

## CONNECTION TO DATABASE
$FEDIT=new DbLink();
$FEDIT->DbConn('soger'.date('Y'));

$debugMode		= false;
$UpdSQL			= array();
$TableName		= "user_movimenti_fiscalizzati";

$sql = "SELECT DISTINCT ID_RIF from ".$TableName." ";
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
$rifs = array();    
if(isset($FEDIT->DbRecordSet)){
	foreach($FEDIT->DbRecordSet as $k=>$v){
		RebuildRefs($debugMode,$v["ID_RIF"],$FEDIT,$UpdSQL,$TableName);
            }
	}
        
foreach($UpdSQL as $k=>$query) {
    if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
        echo "<li>$query";
        }
    $FEDIT->SDBWrite($query,"DbRecordSet",false,false);
    }



function RebuildRefs($debugMode,$ID_RIF,&$OBJ,&$UpdSQL,$TableName) {

    $pri            = ($TableName=="user_movimenti"? "ID_MOV":"ID_MOV_F");
    $Carichi        = array();
    $refComplexSQL_GiacenzeAnnoPrecedente  = array();
    $refComplexSQL_CarichiAnnoCorrente  = array();
    $SubTot         = 0;
    $DaScaricare    = 0;
    
    // giacenze iniziali
    $sql ="SELECT NMOV AS ".$TableName."NMOV, 1 AS isGiacenza, quantita AS ".$TableName."quantita, pesoN AS ".$TableName."pesoN, TIPO AS ".$TableName."TIPO, ID_MOV_F AS ".$TableName."ID_MOV_F, idSIS AS ".$TableName."idSIS, user_schede_rifiuti.pericoloso AS user_schede_rifiutipericoloso ";
    $sql.=" FROM user_movimenti_giacenze_iniziali ";
    $sql.=" JOIN user_schede_rifiuti ON user_movimenti_giacenze_iniziali.ID_RIF=user_schede_rifiuti.ID_RIF ";
    $sql.=" WHERE user_movimenti_giacenze_iniziali.ID_RIF='$ID_RIF' AND user_movimenti_giacenze_iniziali.quantita>0 ";
    $sql.=" ORDER BY NMOV ASC";
    $OBJ->SDBRead($sql,"DbRecordSetGiacenzeIniziali",true,false);
    $giacenzeIniziali_counter = $OBJ->DbRecsNum;
    $giacenzeIniziali = ($giacenzeIniziali_counter>0? $OBJ->DbRecordSetGiacenzeIniziali:array());

    // giacenze iniziali manuali
    $giacenzeInizialiManuali = array();
    if($giacenzeIniziali_counter==0){
            $sql = "SELECT giac_ini, pericoloso, peso_spec, ID_UMIS FROM user_schede_rifiuti WHERE ID_RIF=".$ID_RIF.";";
            $OBJ->SDBRead($sql,"DbRecordSet",true,false);
            if($OBJ->DbRecordSet[0]["giac_ini"]>0) {
                    switch($OBJ->DbRecordSet[0]["ID_UMIS"]){
                            // kg
                            case 1: 
                                    $pesoN = $OBJ->DbRecordSet[0]["giac_ini"];
                                    break;
                            // litri
                            case 2:
                                    $pesoN = $OBJ->DbRecordSet[0]["giac_ini"] / $OBJ->DbRecordSet[0]["peso_spec"];
                                    break;
                            // mc
                            case 3:
                                    $pesoN = $OBJ->DbRecordSet[0]["giac_ini"] / $OBJ->DbRecordSet[0]["peso_spec"] / 1000;
                                    break;
                            }
                    $giacenzeInizialiManuali[0] = array($TableName."NMOV"=>'0', 
                                                                                    "isGiacenza"=>'0',
                                                                                    $TableName."quantita"=>$OBJ->DbRecordSet[0]["giac_ini"],
                                                                                    $TableName."pesoN"=>$pesoN,
                                                                                    $TableName."TIPO"=>'C',
                                                                                    $TableName.$pri=>'0',
                                                                                    $TableName."idSIS"=>'0',
                                                                                    $TableName."pericoloso"=>$OBJ->DbRecordSet[0]["pericoloso"],
                                                                                    );
                    }
            }

    // movimentazioni
    $sql ="SELECT ".$TableName.".NMOV AS ".$TableName."NMOV, 0 AS isGiacenza, ".$TableName.".quantita AS ".$TableName."quantita, ".$TableName.".pesoN AS ".$TableName."pesoN, ".$TableName.".TIPO AS ".$TableName."TIPO, ".$TableName.".".$pri." AS ".$TableName.$pri.", ".$TableName.".idSIS AS ".$TableName."idSIS, user_schede_rifiuti.pericoloso AS user_schede_rifiutipericoloso ";
    $sql.=" FROM ".$TableName." ";
    $sql.=" JOIN user_schede_rifiuti ON ".$TableName.".ID_RIF=user_schede_rifiuti.ID_RIF ";	
    $sql.=" WHERE ".$TableName.".ID_RIF='$ID_RIF' AND ".$TableName.".quantita>0 ";
    $sql.=" ORDER BY NMOV ASC";
    $OBJ->SDBRead($sql,"DbRecordSetMovimentazioni",true,false);
    $movimentazioni_counter = $OBJ->DbRecsNum;
    $movimentazioni = ($movimentazioni_counter>0? $OBJ->DbRecordSetMovimentazioni:array());
	    
    $record = array_merge($giacenzeIniziali, $giacenzeInizialiManuali, $movimentazioni);
    foreach($record as $k=>$dati) {

        if($dati[$TableName."TIPO"]=="C"){
            $Carichi[$dati[$TableName.$pri]] = round($dati[$TableName."quantita"], 2);
            if($dati['isGiacenza']=='1')
                $refComplexSQL_GiacenzeAnnoPrecedente[] = $dati[$TableName.'NMOV'];
            else
                $refComplexSQL_CarichiAnnoCorrente[] = $dati[$TableName.'NMOV'];

            $SubTot += round($dati[$TableName."quantita"], 2);
            }
        else {
            $DaScaricare += round($dati[$TableName."quantita"], 2);
            // sta scaricando tutta la mia giacenza
            if(round($dati[$TableName."quantita"], 2)==round($SubTot, 2)) {
                $GiacenzeAnnoPrecedente = array();
                $CarichiAnnoCorrente = array();
                foreach(array_keys($Carichi) AS $Id){
                    if($Id>0){
                        $sql = "(SELECT NMOV, 0 AS isGiacenza FROM ".$TableName." WHERE ".$pri."=".$Id.") UNION (SELECT NMOV, 1 AS isGiacenza FROM user_movimenti_giacenze_iniziali WHERE ".$pri."=".$Id.")";
                        $OBJ->SDBRead($sql,"DbRecordSet",true,false);
                        if($OBJ->DbRecordSet[0]['isGiacenza']=='1')
                            $GiacenzeAnnoPrecedente[] = $OBJ->DbRecordSet[0]['NMOV']=='0'? 'Giac.Iniz.':$OBJ->DbRecordSet[0]['NMOV'];
                        else
                            $CarichiAnnoCorrente[] = $OBJ->DbRecordSet[0]['NMOV'];
                    }
                    else{
                        // scarico giacenza manuale
                        $CarichiAnnoCorrente[] = 'Giac.Iniz.';
                    }
                }
                $prevYear = date("Y",strtotime("-1 year"));
                $NMOVs = array();
                if($GiacenzeAnnoPrecedente)
                        $NMOVs[] = 'Giac.Iniz. (mov.'.implode(", ",$GiacenzeAnnoPrecedente).' del reg.'.$prevYear.')';
                if($CarichiAnnoCorrente)
                        $NMOVs[] = implode(", ",$CarichiAnnoCorrente);
                $UpdSQL[] = "UPDATE ".$TableName." SET FKErifCarico='" . implode(", ",$NMOVs) . "' WHERE NMOV='" . $dati[$TableName."NMOV"] . "' AND ".$TableName.".".$pri."='" . $dati[$TableName.$pri] . "' LIMIT 1";
                $refComplexSQL_GiacenzeAnnoPrecedente  = array();
                $refComplexSQL_CarichiAnnoCorrente  = array();
                $NMOVs = array();
                $GiacenzeAnnoPrecedente = array();
                $CarichiAnnoCorrente = array();
                $Carichi = array();
                $SubTot = 0;
                }

            // resta qualcosa in giacenza
            else {
                $SubTot -= round($dati[$TableName."quantita"], 2);
                $Carichi = reCheckCarichi($debugMode,$Carichi,round($dati[$TableName."quantita"], 2),$UpdSQL,$dati[$TableName."NMOV"],$dati[$TableName.$pri],$dati[$TableName.'idSIS'],$TableName,$OBJ,$dati['user_schede_rifiutipericoloso']);
                }
            }
        }
    }

function reCheckCarichi($debugMode,$arrayCarichi,$qScarico,&$SQLs,$scaricoNmov,$scaricoID_MOV,$scaricoid_SIS,$TableName,&$OBJ,$pericoloso) {
	
    if($TableName=="user_movimenti") $pri="ID_MOV"; else $pri="ID_MOV_F";
    $fullyScaricato	= false;
    $AltfullyScaricato	= false;
    $newArrayCarichi	= array();
	$refComplexSQL_GiacenzeAnnoPrecedente  = array();
	$refComplexSQL_CarichiAnnoCorrente  = array();
    $isGiacIni		= false;
    $ancoraDaScaricare	= $qScarico;
    $Scaricati		= 0;

    foreach($arrayCarichi as $idMov=>$quantitaKg) {
        if($fullyScaricato) {
            # se abbiamo gi� scaricato tutto, aggiungo movimenti senza fare altro (OK)
            $newArrayCarichi[$idMov] = $quantitaKg;
            } 
        else {

			$GiacenzeAnnoPrecedente = array();
			$CarichiAnnoCorrente = array();

			if($idMov>0){
                $sql = "(SELECT NMOV, 1 AS isGiacenza FROM user_movimenti_giacenze_iniziali WHERE ".$pri."=".$idMov.")";
                $sql.= " UNION ";
                $sql.= "(SELECT NMOV, 0 AS isGiacenza FROM ".$TableName." WHERE ".$pri."=".$idMov.")";
                $OBJ->SDBRead($sql,"DbRecordSet",true,false);
                if($OBJ->DbRecordSet[0]['isGiacenza']=='1')
					$refComplexSQL_GiacenzeAnnoPrecedente[] = $OBJ->DbRecordSet[0]['NMOV']=='0'? 'Giac.Iniz.':$OBJ->DbRecordSet[0]['NMOV'];
				else
					$refComplexSQL_CarichiAnnoCorrente[] = $OBJ->DbRecordSet[0]['NMOV'];              
                }
            else{
                // scarico giacenza manuale
                $refComplexSQL_CarichiAnnoCorrente[] = 'Giac.Iniz.';
                }

            if($Scaricati<=$qScarico) {
                $Residuo = $quantitaKg - $ancoraDaScaricare; if($Residuo<0) $Residuo=0;
                if($Residuo==0) $Prelevati=$quantitaKg; else $Prelevati=$ancoraDaScaricare;

                if(!$fullyScaricato && ($ancoraDaScaricare - $quantitaKg>=0)) {
                    $ancoraDaScaricare -= $quantitaKg;
                    if($ancoraDaScaricare==0) $fullyScaricato=true; 						
                    } 
                else {
                    if(!$fullyScaricato) {
                        $newArrayCarichi[$idMov] = $quantitaKg-$ancoraDaScaricare;
                        $fullyScaricato = true;
                        } 
                    else {
                        $newArrayCarichi[$idMov] = $quantitaKg;
                        }
                    }
                $Scaricati += $quantitaKg;
                } 
            }
        }
	$prevYear = date("Y",strtotime("-1 year"));
	$NMOVs = array();
	if($refComplexSQL_GiacenzeAnnoPrecedente)
		$NMOVs[] = 'Giac.Iniz. (mov.'.implode(", ",$refComplexSQL_GiacenzeAnnoPrecedente).' del reg.'.$prevYear.')';
	if($refComplexSQL_CarichiAnnoCorrente)
		$NMOVs[] = implode(", ",$refComplexSQL_CarichiAnnoCorrente);
    $SQLs[] = "UPDATE ".$TableName." SET FKErifCarico='" . implode(", ",$NMOVs) . "' WHERE NMOV='" . $scaricoNmov . "' AND ".$pri."='" .$scaricoID_MOV. "' LIMIT 1";
    $refComplexSQL_GiacenzeAnnoPrecedente  = array();
	$refComplexSQL_CarichiAnnoCorrente  = array();
	$NMOVs = array();
	$GiacenzeAnnoPrecedente = array();
	$CarichiAnnoCorrente = array();
    #print_r($SQLs);
    $defArray = array();
    foreach($newArrayCarichi as $k=>$v) {
        if($v!=0) {
            $defArray[$k] = $v;			
            }
        }
    return $defArray; 
    }




## SEND NOTIFICATION MAIL TO USER
$mail = new SogerMailer();
$mail->isHTML(true);
$mail->setFrom('cron@sogerpro.it', 'So.Ge.R. PRO');
$mail->addAddress('programmazione@sintem.it');
$mail->Subject  = 'Script ricalcolo legami carico-scarico '.date('d-m-Y');
$message = "Operazione ultimata";
//$message  = utf8_encode($message);
$mail->Body = $message;
$mail->send();

?>