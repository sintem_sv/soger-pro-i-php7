<?php
session_start();
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeForgEdit.php");

$BF = $FEDIT->FGE_TableInfo;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables($_GET["table"]);
$FEDIT->FGE_DescribeFields(true);
$FEDIT->FGE_SelectAllFields($_GET["table"]);
$FEDIT->FGE_SetFilter($_GET["field"], $_GET["filter"],$_GET["table"]);
$sql = $FEDIT->FGE_SQL_MakeSelect();
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
foreach($FEDIT->FGE_TableInfo[$_GET["table"]] as $FName=>$FInfo) {
	if($FInfo["FGE_Type"]!="FGEprimary") {
		echo "<b>" . utf8_encode($FInfo["FGE_Label"]) . "</b>: ";
		echo utf8_encode($FEDIT->DbRecordSet[0][$_GET["table"].$FInfo["Field"]]) . " ";
	}
}
//echo $sql;


$FEDIT->FGE_TableInfo = $BF;

require_once("../__includes/COMMON_sleepForgEdit.php");
?>
