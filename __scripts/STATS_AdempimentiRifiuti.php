<?php
session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/fpdf.php");
include("../__libs/SQLFunct.php");
require_once("STATS_funct.php");


	$sql ="SELECT user_schede_rifiuti.ID_RIF, lov_cer.COD_CER, descrizione, ID_FONTE_RIF, CAR_approved, CLASS_approved, et_last_print, et_approved, user_schede_sicurezza.approved AS sic_approved ";
	$sql.="FROM user_schede_rifiuti ";
	$sql.="LEFT JOIN user_schede_sicurezza ON user_schede_rifiuti.ID_RIF=user_schede_sicurezza.ID_RIF ";
	$sql.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
	$sql.="WHERE user_schede_rifiuti.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND user_schede_rifiuti.".$SOGER->UserData['workmode']."=1 ";
	$sql.="ORDER BY COD_CER ASC, descrizione ASC, ID_RIF ASC ";
	$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

	//print_r($FEDIT->DbRecordSet);die();

	$orientation="L";
	$statTitle = "ADEMPIMENTI RIFIUTI";
	$DcName = date("d/m/Y") . "--Adempimenti_Rifiuti";
	
	$PrintImpianto = true;
	$PrintCER = false;
	$SkipStandardNoRecs = true;

	$um="mm";
	$Format = array(210,297);
	$ZeroMargin = true;
	$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin,$DcName);

	## stats title
	$Ypos+=10;
	$FEDIT->FGE_PdfBuffer->SetXY(0,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',14);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,15,$statTitle,0,"C"); 


	## stat desc
	if(!isset($statDesc)) $statDesc=" Prospetto riepilogativo degli adempimenti inerenti la documentazione relativa ai rifiuti prodotti.";
	$Xpos=20;
	$Ypos+=15;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Descrizione:",0,"L");
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(40,25);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$statDesc,0,"L");

	## intestatario
	$intestatario = strtolower($SOGER->UserData["core_impiantidescription"]) . " (" . $SOGER->UserData["workmode"] . ")";
	$Ypos+=7;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Intestatario:",0,"L");
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(40,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$intestatario,0,"L");

	
	$Ypos+=20;


	# intestazione dati
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,10,"CER","TLBR","C");
	$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,10,"Descrizione","TLBR","L");
	$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Caratterizzazione","TLR","C");
	$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(35,5,"Classificazione","TLR","C");
	$FEDIT->FGE_PdfBuffer->SetXY(210,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(35,5,"Etichetta","TLR","C");
	$FEDIT->FGE_PdfBuffer->SetXY(245,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Procedura di sicurezza","TLR","C");
	$Ypos += 5;
	
	#caratterizzazione
	$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,"Fatta","TBLR","C");
	$FEDIT->FGE_PdfBuffer->SetXY(140,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,"Approvata","TBLR","C");
	$FEDIT->FGE_PdfBuffer->SetXY(157.5,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,"Caricata","TBLR","C");
	#classificazione
	$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,"Approvata","TBLR","C");
	$FEDIT->FGE_PdfBuffer->SetXY(192.5,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,"Caricata","TBLR","C");
	#etichetta
	$FEDIT->FGE_PdfBuffer->SetXY(210,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,"Approvata","TBLR","C");
	$FEDIT->FGE_PdfBuffer->SetXY(227.5,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,"Stampata","TBLR","C");
	#proc. sicurezza
	$FEDIT->FGE_PdfBuffer->SetXY(245,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,"Fatta","TBLR","C");
	$FEDIT->FGE_PdfBuffer->SetXY(260,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,"Approvata","TBLR","C");
	$FEDIT->FGE_PdfBuffer->SetXY(277.5,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,"Caricata","TBLR","C");
	$Ypos += 8; 

	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);

	for($r=0;$r<count($FEDIT->DbRecordSet);$r++){

		$ID_RIF = $FEDIT->DbRecordSet[$r]['ID_RIF'];

		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
		$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,5,$FEDIT->DbRecordSet[$r]['COD_CER'],0,"C");
		$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$FEDIT->DbRecordSet[$r]['descrizione'],0,"L");
		
		#caratterizzazione
		$caratterizzazione_doc = "../__upload/caratterizzazioni/".$ID_RIF.".doc";
		$caratterizzazione_rtf = "../__upload/caratterizzazioni/".$ID_RIF.".rtf";
		$caratterizzazione_pdf = "../__upload/caratterizzazioni/".$ID_RIF.".pdf";
		$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
		if($FEDIT->DbRecordSet[$r]['ID_FONTE_RIF']!=0 | file_exists($caratterizzazione_doc) | file_exists($caratterizzazione_rtf) | file_exists($caratterizzazione_pdf))
			$FEDIT->FGE_PdfBuffer->MultiCell(15,5,utf8_decode("s�"),0,"C");
		else
			$FEDIT->FGE_PdfBuffer->MultiCell(15,5,"no",0,"C");

		$FEDIT->FGE_PdfBuffer->SetXY(140,$Ypos);
		if($FEDIT->DbRecordSet[$r]['CAR_approved']==1)
			$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,utf8_decode("s�"),0,"C");
		else
			$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,"no",0,"C");

		$FEDIT->FGE_PdfBuffer->SetXY(157.5,$Ypos);					
		if(file_exists($caratterizzazione_doc) | file_exists($caratterizzazione_rtf) | file_exists($caratterizzazione_pdf))
			$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,utf8_decode("s�"),0,"C");
		else
			$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,"no",0,"C");

		#classificazione
		$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
		if($FEDIT->DbRecordSet[$r]['CLASS_approved']==1)
			$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,utf8_decode("s�"),0,"C");
		else
			$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,"no",0,"C");

		$FEDIT->FGE_PdfBuffer->SetXY(192.5,$Ypos);
		$classificazione_doc = "../__upload/classificazioni/".$ID_RIF.".doc";
		$classificazione_rtf = "../__upload/classificazioni/".$ID_RIF.".rtf";
		$classificazione_pdf = "../__upload/classificazioni/".$ID_RIF.".pdf";
		if(file_exists($classificazione_doc) | file_exists($classificazione_rtf) | file_exists($classificazione_pdf))
			$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,utf8_decode("s�"),0,"C");
		else
			$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,"no",0,"C");

		#etichetta
		$FEDIT->FGE_PdfBuffer->SetXY(210,$Ypos);
		if($FEDIT->DbRecordSet[$r]['et_approved']==1)
			$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,utf8_decode("s�"),0,"C");
		else
			$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,"no",0,"C");

		$FEDIT->FGE_PdfBuffer->SetXY(227.5,$Ypos);
		if($FEDIT->DbRecordSet[$r]['et_last_print']!="")
			$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,utf8_decode("s�"),0,"C");
		else
			$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,"no",0,"C");

		#proc. sicurezza
		$FEDIT->FGE_PdfBuffer->SetXY(245,$Ypos);
		if($FEDIT->DbRecordSet[$r]['sic_approved']!="")
			$FEDIT->FGE_PdfBuffer->MultiCell(15,5,utf8_decode("s�"),0,"C");
		else
			$FEDIT->FGE_PdfBuffer->MultiCell(15,5,"no",0,"C");

		$FEDIT->FGE_PdfBuffer->SetXY(260,$Ypos);
		if($FEDIT->DbRecordSet[$r]['sic_approved']==1)
			$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,utf8_decode("s�"),0,"C");
		else
			$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,"no",0,"C");

		$FEDIT->FGE_PdfBuffer->SetXY(277.5,$Ypos);
		$scheda_doc = "../__upload/sicurezza/".$ID_RIF.".doc";
		$scheda_rtf = "../__upload/sicurezza/".$ID_RIF.".rtf";
		$scheda_pdf = "../__upload/sicurezza/".$ID_RIF.".pdf";
		if(file_exists($scheda_doc) | file_exists($scheda_rtf) | file_exists($scheda_pdf))
			$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,utf8_decode("s�"),0,"C");
		else
			$FEDIT->FGE_PdfBuffer->MultiCell(17.5,5,"no",0,"C");
		$Ypos += 8;
		}

	
	
	PDFOut($FEDIT->FGE_PdfBuffer,$DcName);

?>