<?php

session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/fpdf.php");
require_once("STATS_funct.php");
require_once("../__libs/SQLFunct.php");


$SQL ="SELECT department.descr AS Reparto, product.id AS ID_PRO, product.code AS Matricola, product.descr AS DescrizioneBaan, SUM(quantity) AS quantita, product.um AS UnitaMisura ";
$SQL.="FROM warehouse_register ";
$SQL.="JOIN department ON warehouse_register.department_id=department.id ";
$SQL.="JOIN product ON warehouse_register.product_id = product.id ";
$SQL.="WHERE department.plant_id=1 AND warehouse_register.date>='2013-03-01' ";
$SQL.="GROUP BY product.id ";
$SQL.="ORDER BY Matricola ASC";

$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);

for($c=0;$c<count($FEDIT->DbRecordSet);$c++){

	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$c]['Reparto']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$c]['Matricola']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$c]['DescrizioneBaan']);

	# DATI SDS
	$SQL ="SELECT sds.id AS ID_SDS, sds.descr AS NomeCommerciale FROM sds JOIN j_product_sds ON j_product_sds.id=sds.id WHERE j_product_sds.product_id=".$FEDIT->DbRecordSet[$c]['ID_PRO'];
	$FEDIT->SdbRead($SQL,"DbRecordSetSDS",true,false);

	if(count($FEDIT->DbRecordSetSDS)>0){
	
		# NOME COMMERCIALE
		$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSetSDS[0]['NomeCommerciale']);
	
		# CERCO SIMBOLI
		$SQL ="SELECT data_symbol.code AS SIMBOLO FROM data_symbol JOIN j_sds_symbol ON data_symbol.id = j_sds_symbol.id WHERE j_sds_symbol.sds_id =".$FEDIT->DbRecordSetSDS[0]['ID_SDS'].";";
		$FEDIT->SdbRead($SQL,"DbRecordSetSimboli",true,false);
		$Simboli='';
		for($s=0;$s<count($FEDIT->DbRecordSetSimboli);$s++){
			$Simboli.=$FEDIT->DbRecordSetSimboli[$s]['SIMBOLO']." ";
			}
		$CSVbuf .= AddCSVcolumn($Simboli);

		# CERCO FRASI R
		$SQL ="SELECT data_phrase_r.code AS FRASE FROM data_phrase_r JOIN j_sds_phraser ON data_phrase_r.id = j_sds_phraser.id WHERE j_sds_phraser.sds_id =".$FEDIT->DbRecordSetSDS[0]['ID_SDS'].";";
		$FEDIT->SdbRead($SQL,"DbRecordSetR",true,false);
		$FrasiR='';
		for($r=0;$r<count($FEDIT->DbRecordSetR);$r++){
			$FrasiR.=$FEDIT->DbRecordSetR[$r]['FRASE']." ";
			}
		$CSVbuf .= AddCSVcolumn($FrasiR);
		}
	else{
		$CSVbuf .= AddCSVcolumn("Nessuna SDS associata");
		$CSVbuf .= AddCSVcolumn("Nessuna SDS associata");
		$CSVbuf .= AddCSVcolumn("Nessuna SDS associata");
		}

	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$c]['quantita']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$c]['UnitaMisura']);
	$CSVbuf .= AddCSVRow();

	}

CSVOut($CSVbuf,"REPORT_1");

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>