<?php
	$FEDIT->FGE_FLushTableInfo();
	$FEDIT->FGE_UseTables(array($TableName,"user_schede_rifiuti"));
	$FEDIT->FGE_SetSelectFields(array($IDMov,"ID_RIF","pesoN","TIPO","ID_IMP","StampaAnnua","FISCALE"),$TableName);
	$FEDIT->FGE_SetSelectFields(array("giac_ini","ID_IMP","giac_min","ID_UMIS","peso_spec"),"user_schede_rifiuti");
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_SetFilter("ID_RIF", $IDRIF,$TableName);
	if($IMP_FILTER){
		$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],$TableName);
		$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
		}
	$SQL	= $FEDIT->FGE_SQL_MakeSelect();
	if($EXCLUDE_9999999)
		$SQL .=" AND ".$TableName.".NMOV<>9999999 ";
	$FEDIT->SDBRead($SQL,"DbRecordSet",true,false);
	#

	if($FEDIT->DbRecsNum>0) {
		if(is_null($FEDIT->DbRecordSet[0]["user_schede_rifiutigiac_ini"])) {
			$GiacINI = 0;	
		} else {
			$GiacINI = 0 + $FEDIT->DbRecordSet[0]["user_schede_rifiutigiac_ini"];
		}
		$GiacenzaPlus = 0;
		$GiacenzaMinus = 0;
		$LastCarico = null;
		foreach($FEDIT->DbRecordSet as $k=>$dati) {
			if($FEDIT->DbRecordSet[$k][$TableName."TIPO"] == "C") {
				if($FEDIT->DbRecordSet[$k][$TableName.$IDMov]!=$LastCarico) {
					$LastCarico = $FEDIT->DbRecordSet[$k][$TableName.$IDMov] ;
					$StampaAnnua = $FEDIT->DbRecordSet[$k][$TableName."FISCALE"] ;	
				}
				$GiacenzaPlus += $FEDIT->DbRecordSet[$k][$TableName."pesoN"]; 
			} else {
				# la qta dello scarico corrente non va sottratta nella visualizzazione della disponibilit�!
				if($FEDIT->DbRecordSet[$k][$TableName.$IDMov]!=@$_GET[$IDMov])
					$GiacenzaMinus += $FEDIT->DbRecordSet[$k][$TableName."pesoN"];
			}
		}
		if(is_null($LastCarico)) {
			$LastCarico = 0;
		}
		
		// se non � in Kg, $GiacINI va convertito

		switch($FEDIT->DbRecordSet[0]["user_schede_rifiutiID_UMIS"]){
			case '1':
				$GiacINIperDispo=$GiacINI;
				break;
			case '2':
				$GiacINIperDispo=$GiacINI*$FEDIT->DbRecordSet[0]["user_schede_rifiutipeso_spec"];
				break;
			case '3':
				$GiacINIperDispo=$GiacINI*$FEDIT->DbRecordSet[0]["user_schede_rifiutipeso_spec"]*1000;
				break;
			}

		$Disponibilita = (number_format($GiacenzaPlus, 2, '.', '') + number_format($GiacINIperDispo, 2, '.', '')) - (number_format($GiacenzaMinus, 2, '.', ''));
		$Disponibilita = round($Disponibilita,2);

		if($DISPO_PRINT_OUT) {
			if($Disponibilita<0) $Disponibilita=0;
			return "|$Disponibilita|$GiacINI|$LastCarico|$StampaAnnua";
		}
	} else {
		# nessun record: manca referenza tabella movimenti -> estraggo solo i dati rifiuto
		$FEDIT->FGE_FLushTableInfo();
		$FEDIT->FGE_UseTables(array("user_schede_rifiuti"));
		$FEDIT->FGE_SetSelectFields(array("giac_ini","ID_IMP","ID_RIF","ID_UMIS","peso_spec"),"user_schede_rifiuti");
		$FEDIT->FGE_DescribeFields();
		$FEDIT->FGE_SetFilter("ID_RIF", $IDRIF,"user_schede_rifiuti");
		if($IMP_FILTER)
			$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
		$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
		if(is_null($FEDIT->DbRecordSet[0]["user_schede_rifiutigiac_ini"])) {
			$GiacINI = 0;	
		} else {
			$GiacINI = $FEDIT->DbRecordSet[0]["user_schede_rifiutigiac_ini"];
		}	
		
		// se non � in Kg, $GiacINI va convertito
		/*
		switch($FEDIT->DbRecordSet[0]["user_schede_rifiutiID_UMIS"]){
			case '2':
				$GiacINI=$GiacINI*$FEDIT->DbRecordSet[0]["user_schede_rifiutipeso_spec"];
				break;
			case '3':
				$GiacINI=$GiacINI*$FEDIT->DbRecordSet[0]["user_schede_rifiutipeso_spec"]*1000;
				break;
			}
		*/
		// LA GIACENZA INIZIALE VA GESTITA NELLA UM DEL RIFIUTO, NON VA CONVERTITA
		// LA DISPONIBILITA INVECE VA CONVERTITA!
		$Disponibilita = $GiacINI;
		switch($FEDIT->DbRecordSet[0]["user_schede_rifiutiID_UMIS"]){
			case '1':
				$Disponibilita=$GiacINI;
				break;
			case '2':
				$Disponibilita=$GiacINI*$FEDIT->DbRecordSet[0]["user_schede_rifiutipeso_spec"];
				break;
			case '3':
				$Disponibilita=$GiacINI*$FEDIT->DbRecordSet[0]["user_schede_rifiutipeso_spec"]*1000;
				break;
			}
		if($DISPO_PRINT_OUT) {
			return "|$Disponibilita|$GiacINI|0|0";
		}
	}
?>