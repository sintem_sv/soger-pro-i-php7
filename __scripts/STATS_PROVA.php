<?php

function ordina_pericoloso($a, $b){

	if($a['pericoloso'] == $b['pericoloso']){
		if($a['COD_CER'] > $b['COD_CER'])
			return 1;
		else
			return 0;
		}
	else{
		if($a['pericoloso'] > $b['pericoloso'])
			return 1;
		else
			return 0;
		}
	}

function ordina_sf($a, $b){

	if($a['ID_SF'] == $b['ID_SF']){
		if($a['COD_CER'] > $b['COD_CER'])
			return 1;
		else
			return 0;
		}
	else{
		if($a['ID_SF'] > $b['ID_SF'])
			return 1;
		else
			return 0;
		}
	}

function ordina_spec($a, $b){

	if($a['ID_SPEC'] == $b['ID_SPEC']){
		if($a['COD_CER'] > $b['COD_CER'])
			return 1;
		else
			return 0;
		}
	else{
		if($a['ID_SPEC'] > $b['ID_SPEC'])
			return 1;
		else
			return 0;
		}
	}


usort($FEDIT->DbRecordSet, "ordina_pericoloso");
$RifPER		= $FEDIT->DbRecordSet;
usort($FEDIT->DbRecordSet, "ordina_sf");
$RifSF		= $FEDIT->DbRecordSet;
usort($FEDIT->DbRecordSet, "ordina_spec");
$RifSPEC	= $FEDIT->DbRecordSet;

$PERbuffer	= '';
$SFbuffer	= '';
$SPECbuffer	= '';

$Pericolosi		= array();
$StatoFisico	= array();
$Specifici		= array();



## PERICOLOSI vs NON PERICOLOSI
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFillColor(242,236,60);
$FEDIT->FGE_PdfBuffer->MultiCell(265,7,"Pericolosi / Non pericolosi","TLBR","C",1);

for($r=0;$r<count($RifPER);$r++){
	if($RifPER[$r]['pericoloso']!=$PERbuffer){
		$FIRPER		= 0;
		$KGPER		= 0;
		$Ypos+=10;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFillColor(252,249,171);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
		$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,7,"CER","TLBR","C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(30,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(120,7,"Descrizione","TLBR","C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(150,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,7,"Stato fisico","TLBR","C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(190,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(10,7,"FIR","TLBR","C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(200,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,7,"kg conferiti","TLBR","C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(225,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,7,"kg medi","TLBR","C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,7,"PericolositÓ","TLBR","C",1);
		$Ypos+=7;
		}	
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,$RifPER[$r]['COD_CER'],"TLBR","C");
	$FEDIT->FGE_PdfBuffer->SetXY(30,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(120,5,$RifPER[$r]['descrizione'],"TLBR","L");
	$FEDIT->FGE_PdfBuffer->SetXY(150,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(40,5,$RifPER[$r]['SF'],"TLBR","L");
	$FEDIT->FGE_PdfBuffer->SetXY(190,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(10,5,$RifPER[$r]['FIR'],"TLBR","R");
	$FEDIT->FGE_PdfBuffer->SetXY(200,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(25,5,number_format($RifPER[$r]['KG'], 2, ',', '.'),"TLBR","R");
	$FEDIT->FGE_PdfBuffer->SetXY(225,$Ypos);
	$CMtmp	= $RifPER[$r]['KG'] / $RifPER[$r]['FIR'];
	$CM		= number_format($CMtmp, 2, ',', '.');
	$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$CM,"TLBR","R");
	$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
	if($RifPER[$r]['pericoloso']==1) $P='Pericoloso'; else $P='Non pericoloso';
	$FEDIT->FGE_PdfBuffer->MultiCell(30,5,$P,"TLBR","C");

	$PERbuffer = $RifPER[$r]['pericoloso'];
	$Pericolosi[$RifPER[$r]['pericoloso']]['KG']+=$RifPER[$r]['KG'];
	$Pericolosi[$RifPER[$r]['pericoloso']]['FIR']+=$RifPER[$r]['FIR'];

	if( ($RifPER[$r+1]['pericoloso']!=$PERbuffer AND $PERbuffer!='') | $r==(count($RifPER)-1) ){
		$Ypos+=5; 
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetXY(190,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(10,5,$Pericolosi[$RifPER[$r]['pericoloso']]['FIR'],"TLBR","R");
		$FEDIT->FGE_PdfBuffer->SetXY(200,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,5,number_format($Pericolosi[$RifPER[$r]['pericoloso']]['KG'], 2, ',', '.'),"TLBR","R");
		}

	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	}



## STATO FISICO
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFillColor(61,191,244);
$FEDIT->FGE_PdfBuffer->MultiCell(265,7,"Stato fisico","TLBR","C",1);

for($r=0;$r<count($RifSF);$r++){
	if($RifSF[$r]['SF']!=$SFbuffer){
		$FIRSF		= 0;
		$KGSF		= 0;
		$Ypos+=10;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFillColor(171,221,252);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
		$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,7,"CER","TLBR","C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(30,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(120,7,"Descrizione","TLBR","C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(150,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,7,"Stato fisico","TLBR","C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(190,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(10,7,"FIR","TLBR","C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(200,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,7,"kg conferiti","TLBR","C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(225,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,7,"kg medi","TLBR","C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,7,"PericolositÓ","TLBR","C",1);
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		}	
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,$RifSF[$r]['COD_CER'],"TLBR","C");
	$FEDIT->FGE_PdfBuffer->SetXY(30,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(120,5,$RifSF[$r]['descrizione'],"TLBR","L");
	$FEDIT->FGE_PdfBuffer->SetXY(150,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(40,5,$RifSF[$r]['SF'],"TLBR","L");
	$FEDIT->FGE_PdfBuffer->SetXY(190,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(10,5,$RifSF[$r]['FIR'],"TLBR","R");
	$FEDIT->FGE_PdfBuffer->SetXY(200,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(25,5,number_format($RifSF[$r]['KG'], 2, ',', '.'),"TLBR","R");
	$FEDIT->FGE_PdfBuffer->SetXY(225,$Ypos);
	$CMtmp	= $RifSF[$r]['KG'] / $RifSF[$r]['FIR'];
	$CM		= number_format($CMtmp, 2, ',', '.');
	$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$CM,"TLBR","R");
	$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
	if($RifSF[$r]['pericoloso']==1) $P='Pericoloso'; else $P='Non pericoloso';
	$FEDIT->FGE_PdfBuffer->MultiCell(30,5,$P,"TLBR","C");
	
	$SFbuffer = $RifSF[$r]['SF'];
	$StatoFisico[$RifSF[$r]['SF']]['KG']+=$RifSF[$r]['KG'];
	$StatoFisico[$RifSF[$r]['SF']]['FIR']+=$RifSF[$r]['FIR'];
	
	if( ($RifSF[$r+1]['SF']!=$SFbuffer AND $SFbuffer!='') | $r==(count($RifSF)-1) ){
		$Ypos+=5; 
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetXY(190,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(10,5,$StatoFisico[$RifSF[$r]['SF']]['FIR'],"TLBR","R");
		$FEDIT->FGE_PdfBuffer->SetXY(200,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,5,number_format($StatoFisico[$RifSF[$r]['SF']]['KG'], 2, ',', '.'),"TLBR","R");
		}

	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	}


## SPECIFICI vs A-SPECIFICI
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFillColor(173,232,97);
$FEDIT->FGE_PdfBuffer->MultiCell(265,7,"Specifici / A-Specifici","TLBR","C",1);

for($r=0;$r<count($RifSPEC);$r++){
	if($RifSPEC[$r]['SPEC']!=$SPECbuffer){
		$Ypos+=10;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFillColor(201,244,144);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
		$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,7,"CER","TLBR","C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(30,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(120,7,"Descrizione","TLBR","C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(150,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,7,"Stato fisico","TLBR","C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(190,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(10,7,"FIR","TLBR","C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(200,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,7,"kg conferiti","TLBR","C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(225,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,7,"kg medi","TLBR","C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,7,"SpecificitÓ","TLBR","C",1);
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		}	
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,$RifSPEC[$r]['COD_CER'],"TLBR","C");
	$FEDIT->FGE_PdfBuffer->SetXY(30,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(120,5,$RifSPEC[$r]['descrizione'],"TLBR","L");
	$FEDIT->FGE_PdfBuffer->SetXY(150,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(40,5,$RifSPEC[$r]['SF'],"TLBR","L");
	$FEDIT->FGE_PdfBuffer->SetXY(190,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(10,5,$RifSPEC[$r]['FIR'],"TLBR","R");
	$FEDIT->FGE_PdfBuffer->SetXY(200,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(25,5,number_format($RifSPEC[$r]['KG'], 2, ',', '.'),"TLBR","R");
	$FEDIT->FGE_PdfBuffer->SetXY(225,$Ypos);
	$CMtmp	= $RifSPEC[$r]['KG'] / $RifSPEC[$r]['FIR'];
	$CM		= number_format($CMtmp, 2, ',', '.');
	$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$CM,"TLBR","R");
	$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,5,$RifSPEC[$r]['SPEC'],"TLBR","C");
	
	$SPECbuffer = $RifSPEC[$r]['SPEC'];
	$Specifici[$RifSPEC[$r]['SPEC']]['KG']+=$RifSPEC[$r]['KG'];
	$Specifici[$RifSPEC[$r]['SPEC']]['FIR']+=$RifSPEC[$r]['FIR'];
	
	if( ($RifSPEC[$r+1]['SPEC']!=$SPECbuffer AND $SPECbuffer!='') | $r==(count($RifSPEC)-1) ){
		$Ypos+=5; 
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetXY(190,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(10,5,$Specifici[$RifSPEC[$r]['SPEC']]['FIR'],"TLBR","R");
		$FEDIT->FGE_PdfBuffer->SetXY(200,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,5,number_format($Specifici[$RifSPEC[$r]['SPEC']]['KG'], 2, ',', '.'),"TLBR","R");
		}
	
	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	}


## TABELLE %
$FEDIT->FGE_PdfBuffer->AddPage();

$Ypos=15;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);


# PERICOLOSI vs NON PERICOOSI

$PERTOTKG = $Pericolosi[0]['KG'] + $Pericolosi[1]['KG'];
$PERC_PERKG = 100 / $PERTOTKG * $Pericolosi[0]['KG'];
$PERC_NPERKG = 100 / $PERTOTKG * $Pericolosi[1]['KG'];

$PERTOTFIR = $Pericolosi[0]['FIR'] + $Pericolosi[1]['FIR'];
$PERC_PERFIR = 100 / $PERTOTFIR * $Pericolosi[0]['FIR'];
$PERC_NPERFIR = 100 / $PERTOTFIR * $Pericolosi[1]['FIR'];

$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFillColor(242,236,60);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(120,7,"Pericolosi / Non pericolosi","TLBR","C",1);

$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(60,7,"Pericolosi","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($Pericolosi[0]['KG'], 2, ',', '.')." kg","TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERC_PERKG, 2, ',', '.')." %","TLBR","R");

$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(60,7,"Non pericolosi","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($Pericolosi[1]['KG'], 2, ',', '.')." kg","TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERC_NPERKG, 2, ',', '.')." %","TLBR","R");

$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERTOTKG, 2, ',', '.')." kg","TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,"100 %","TLBR","R");

$Ypos=15;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(160,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFillColor(242,236,60);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(120,7,"Pericolosi / Non pericolosi","TLBR","C",1);

$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(160,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(60,7,"Pericolosi","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(220,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,$Pericolosi[0]['FIR'],"TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERC_PERFIR, 2, ',', '.')." %","TLBR","R");

$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(160,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(60,7,"Non pericolosi","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(220,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,$Pericolosi[1]['FIR'],"TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERC_NPERFIR, 2, ',', '.')." %","TLBR","R");

$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(220,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,$PERTOTFIR,"TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,"100 %","TLBR","R");






# STATO FISICO

if (!array_key_exists("Solido non pulverulento", $StatoFisico)) {
	$StatoFisico["Solido non pulverulento"]["KG"]=0;
	$StatoFisico["Solido non pulverulento"]["FIR"]=0;
	}
if (!array_key_exists("Solido pulverulento", $StatoFisico)) {
	$StatoFisico["Solido pulverulento"]["KG"]=0;
	$StatoFisico["Solido pulverulento"]["FIR"]=0;
	}
if (!array_key_exists("Fangoso palabile", $StatoFisico)) {
	$StatoFisico["Fangoso palabile"]["KG"]=0;
	$StatoFisico["Fangoso palabile"]["FIR"]=0;
	}
if (!array_key_exists("Liquido", $StatoFisico)) {
	$StatoFisico["Liquido"]["KG"]=0;
	$StatoFisico["Liquido"]["FIR"]=0;
	}


$SFTOTKG = $StatoFisico["Solido non pulverulento"]["KG"] + $StatoFisico["Solido pulverulento"]["KG"] + $StatoFisico["Fangoso palabile"]["KG"] + $StatoFisico["Liquido"]["KG"];
$PERC_SNPKG = 100 / $SFTOTKG * $StatoFisico["Solido non pulverulento"]["KG"];
$PERC_SPKG = 100 / $SFTOTKG * $StatoFisico["Solido pulverulento"]["KG"];
$PERC_FKG = 100 / $SFTOTKG * $StatoFisico["Fangoso palabile"]["KG"];
$PERC_LKG = 100 / $SFTOTKG * $StatoFisico["Liquido"]["KG"];

$SFTOTFIR = $StatoFisico["Solido non pulverulento"]["FIR"] + $StatoFisico["Solido pulverulento"]["FIR"] + $StatoFisico["Fangoso palabile"]["FIR"] + $StatoFisico["Liquido"]["FIR"];
$PERC_SNPFIR = 100 / $PERTOTFIR * $StatoFisico["Solido non pulverulento"]["FIR"];
$PERC_SPFIR = 100 / $PERTOTFIR * $StatoFisico["Solido pulverulento"]["FIR"];
$PERC_FFIR = 100 / $PERTOTFIR * $StatoFisico["Fangoso palabile"]["FIR"];
$PERC_LFIR = 100 / $PERTOTFIR * $StatoFisico["Liquido"]["FIR"];



$Ypos+=20;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFillColor(61,191,244);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(120,7,"Stato fisico","TLBR","C",1);

// solido np
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(60,7,"Solido non pulverulento","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($StatoFisico["Solido non pulverulento"]['KG'], 2, ',', '.')." kg","TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERC_SNPKG, 2, ',', '.')." %","TLBR","R");

// solido p
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(60,7,"Solido pulverulento","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($StatoFisico["Solido pulverulento"]['KG'], 2, ',', '.')." kg","TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERC_SPKG, 2, ',', '.')." %","TLBR","R");

// fangoso
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(60,7,"Fangoso palabile","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($StatoFisico["Fangoso palabile"]['KG'], 2, ',', '.')." kg","TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERC_FKG, 2, ',', '.')." %","TLBR","R");

// liquido
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(60,7,"Liquido","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($StatoFisico["Liquido"]['KG'], 2, ',', '.')." kg","TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERC_LKG, 2, ',', '.')." %","TLBR","R");

// totale
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($SFTOTKG, 2, ',', '.')." kg","TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,"100 %","TLBR","R");

$Ypos-=35;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(160,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFillColor(61,191,244);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(120,7,"Stato fisico","TLBR","C",1);

// solido np
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(160,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(60,7,"Solido non pulverulento","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(220,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,$StatoFisico["Solido non pulverulento"]['FIR'],"TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERC_SNPFIR, 2, ',', '.')." %","TLBR","R");

// solido p
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(160,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(60,7,"Solido pulverulento","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(220,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,$StatoFisico["Solido pulverulento"]['FIR'],"TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERC_SPFIR, 2, ',', '.')." %","TLBR","R");

// fangoso
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(160,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(60,7,"Fangoso palabile","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(220,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,$StatoFisico["Fangoso palabile"]['FIR'],"TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERC_FFIR, 2, ',', '.')." %","TLBR","R");

// liquido
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(160,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(60,7,"Liquido","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(220,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,$StatoFisico["Liquido"]['FIR'],"TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERC_LFIR, 2, ',', '.')." %","TLBR","R");

// totale
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(220,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,$SFTOTFIR,"TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,"100 %","TLBR","R");


# SPECIFICI / A-SPECIFICI

if (!array_key_exists("Non determinata", $Specifici)) {
	$Specifici["Non determinata"]["KG"]=0;
	$Specifici["Non determinata"]["FIR"]=0;
	}
if (!array_key_exists("Specifico", $Specifici)) {
	$Specifici["Specifico"]["KG"]=0;
	$Specifici["Specifico"]["FIR"]=0;
	}
if (!array_key_exists("A-specifico", $Specifici)) {
	$Specifici["A-specifico"]["KG"]=0;
	$Specifici["A-specifico"]["FIR"]=0;
	}


$STOTKG = $Specifici["Non determinata"]["KG"] + $Specifici["Specifico"]["KG"] + $Specifici["A-specifico"]["KG"];
$PERC_NKG = 100 / $STOTKG * $Specifici["Non determinata"]["KG"];
$PERC_SKG = 100 / $STOTKG * $Specifici["Specifico"]["KG"];
$PERC_AKG = 100 / $STOTKG * $Specifici["A-specifico"]["KG"];

$STOTFIR = $Specifici["Non determinata"]["FIR"] + $Specifici["Specifico"]["FIR"] + $Specifici["A-specifico"]["FIR"];
$PERC_NFIR = 100 / $STOTFIR * $Specifici["Non determinata"]["FIR"];
$PERC_SFIR = 100 / $STOTFIR * $Specifici["Specifico"]["FIR"];
$PERC_AFIR = 100 / $STOTFIR * $Specifici["A-specifico"]["FIR"];


$Ypos+=20;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFillColor(173,232,97);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(120,7,"Specifici / A-specifici","TLBR","C",1);

// non determinata
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(60,7,"Non determinata","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($Specifici["Non determinata"]['KG'], 2, ',', '.')." kg","TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERC_NKG, 2, ',', '.')." %","TLBR","R");

// specifici
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(60,7,"Specifici","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($Specifici["Specifico"]['KG'], 2, ',', '.')." kg","TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERC_SKG, 2, ',', '.')." %","TLBR","R");

// a-specifici
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(60,7,"A-specifici","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($Specifici["A-specifico"]['KG'], 2, ',', '.')." kg","TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERC_AKG, 2, ',', '.')." %","TLBR","R");

// totale
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($STOTKG, 2, ',', '.')." kg","TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,"100 %","TLBR","R");

$Ypos-=28;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(160,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFillColor(173,232,97);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(120,7,"Specifici / A-specifici","TLBR","C",1);

// non determinata
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(160,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(60,7,"Non determinata","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(220,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,$Specifici["Non determinata"]['FIR'],"TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERC_NFIR, 2, ',', '.')." %","TLBR","R");

// specifici
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(160,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(60,7,"Specifici","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(220,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,$Specifici["Specifico"]['FIR'],"TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERC_SFIR, 2, ',', '.')." %","TLBR","R");

// a-specifici
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(160,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(60,7,"A-specifici","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(220,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,$Specifici["A-specifico"]['FIR'],"TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,number_format($PERC_AFIR, 2, ',', '.')." %","TLBR","R");

// totale
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(220,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,$SFTOTFIR,"TLBR","R");
$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,7,"100 %","TLBR","R");

?>