<?php
session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__libs/SQLFunct.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");
# legge db, crea array con i valori
global $SOGER;
$ReplacedWith = array();
$ToBeReplaced = array(
	0=>"[DATA]",
	1=>"[DES_RIFIUTO]",
	2=>"[COD_CER]",
	3=>"[DES_CER]",
	4=>"[U_MIS]",
	5=>"[RICHIEDENTE]",
	6=>"[PROCEDURA_ISO]",
	);
	
$sql = "SELECT descrizione,COD_CER,lov_cer.description,lov_misure.description AS U_MISdes FROM user_schede_rifiuti JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER JOIN lov_misure ON user_schede_rifiuti.ID_UMIS=lov_misure.ID_UMIS WHERE ID_RIF='" . $_GET["filter"] . "'";
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
$FileName = date("d-m-Y") . "---Cer";
$ReplacedWith[0] = date("d/m/Y");
foreach($FEDIT->DbRecordSet[0] as $k=>$values) {
	if($k=="COD_CER") {
		$FileName .= $values;
	}
	$ReplacedWith[] = $values;		
}
$ReplacedWith[5] = $SOGER->UserData["core_usersdescription"];



## PROCEDURA ISO - ID_ISOF=12
$SQL="SELECT procedura FROM core_impianti_iso WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1 AND ID_ISOF=12;";
$FEDIT->SdbRead($SQL, "DbRecordSet");
$PROCEDURA=$FEDIT->DbRecordSet[0]["procedura"];
if(!is_null($PROCEDURA) && $PROCEDURA!='')
	$ReplacedWith[6]="Documento elaborato in ottemperanza alla procedura cliente: ".$PROCEDURA;
else
	$ReplacedWith[6]="";


#
if($_GET["FGE_action"]=="carico") {
	$FileName .= "---Richiesta.di.Carico.rtf";
	$template = "RIC_CARICO.rtf";
	} 
else {
	$FileName .= "---Richiesta.di.Scarico.rtf";
	$template = "RIC_SCARICO.rtf";
	}
#
StreamOut($template,$FileName,$ToBeReplaced,$ReplacedWith);

require_once("../__includes/COMMON_sleepForgEdit.php");
?>
