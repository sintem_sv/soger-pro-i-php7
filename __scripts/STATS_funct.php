<?php
function PDFOut(&$PdfObj,$DcName) {
//{{{
	$PdfObj->Output($DcName.".pdf","D");
	unset($PdfObj);
	die();	 //}}}
}




function CheckCarichi($rif, $CarichiRifiuto){

	if(isset($rif['totS'])) $TotS=$rif['totS']; else $TotS=0;
	$giacOK=false;
	if(($rif['totC']+$rif['giac_ini'])>$rif['totS']){

		# 0 - giacenza iniziale
		if($rif['giac_ini']>0 && !$giacOK){
			$CarichiRifiuto[$rif['RifID']][0]['qta']=$rif['giac_ini'];
			$CarichiRifiuto[$rif['RifID']][0]['num']=0;
			//$CarichiRifiuto[$rif['RifID']][0]['data']="0000-00-00";
			$CarichiRifiuto[$rif['RifID']][0]['data']=date("Y")."-01-01";
			$giacOK=true;
			}
		# 1 - array carichi rifiuto
		for($k=0;$k<count($rif['carico']);$k++){
				if(isset($CarichiRifiuto[$rif['RifID']])) $index=count($CarichiRifiuto[$rif['RifID']]); else $index=0;
				$CarichiRifiuto[$rif['RifID']][$index]['qta']=$rif['carico'][$k]['qta'];
				$CarichiRifiuto[$rif['RifID']][$index]['num']=$rif['carico'][$k]['num'];
				$CarichiRifiuto[$rif['RifID']][$index]['data']=$rif['carico'][$k]['data'];
			}
		# 2 - sottraggo qta da scaricato fino a quando scaricato resta positivo
		for($i=0;$i<count($CarichiRifiuto[$rif['RifID']]);$i++){
//			print_r("TotS=".$TotS." tolgo ".$CarichiRifiuto[$rif['RifID']][$i]['qta']."<br />");
			if($TotS>=0){
				$TotS=round($TotS, 2)-round($CarichiRifiuto[$rif['RifID']][$i]['qta'], 2);
//			print_r("TotS=".$TotS."<br />");
				if($TotS<0)
					$lastCarico=$CarichiRifiuto[$rif['RifID']][$i]['num'];
					$lastCaricoData=$CarichiRifiuto[$rif['RifID']][$i]['data'];
				}
			}


		if(isset($lastCarico) && !is_null($lastCarico)){
			return(@$lastCarico."|".@$lastCaricoData."|".abs(@$TotS)."|".$rif["COD_CER"] . " - " . $rif["descrizione"]."|".$rif["t_limite"]."|".$rif["RifID"]);
			//print_r("RIF ".$rif['RifID'].", devo scaricare dal carico: ".$lastCarico. " Kg ".abs($TotS)."<br/>\n\r");
			}
		}
	}




function RemoveCarichiScaricati($rif, $CarichiRifiuto){

if(isset($rif['totS'])) $TotS=$rif['totS']; else $TotS=0;

$giacOK = ($rif['isGiacenzaIniziale']=='1'? true:false);
$PrimoDaScaricare=0;

    # 0 - giacenza iniziale
    if($rif['giac_ini']>0 && !$giacOK){
        $CarichiRifiuto[$rif['RifID']][0]['qta']=$rif['giac_ini'];
        $CarichiRifiuto[$rif['RifID']][0]['num']=0;
        $CarichiRifiuto[$rif['RifID']][0]['ID_MOV_F']=0;
        $CarichiRifiuto[$rif['RifID']][0]['idSIS']=0;
        $CarichiRifiuto[$rif['RifID']][0]['FKEpesospecifico']=$rif['FKEpesospecifico'];
        $CarichiRifiuto[$rif['RifID']][0]['FKEumis']=$rif['FKEumis'];
        $CarichiRifiuto[$rif['RifID']][0]['ID_IMP']=$rif['ID_IMP'];
        $CarichiRifiuto[$rif['RifID']][0]['workmode']=$rif['workmode'];
        $CarichiRifiuto[$rif['RifID']][0]['data']="0000-00-00";
        $CarichiRifiuto[$rif['RifID']][0]['CER']=$rif['COD_CER'];
        $CarichiRifiuto[$rif['RifID']][0]['desc']=$rif['descrizione'];
        $giacOK=true;
        }

    # 1 - array carichi rifiuto
    for($k=0;$k<count($rif['carico']);$k++){
        if(isset($CarichiRifiuto[$rif['RifID']])) $index=count($CarichiRifiuto[$rif['RifID']]); else $index=0;
        $CarichiRifiuto[$rif['RifID']][$index]['qta']=$rif['carico'][$k]['qta'];
        $CarichiRifiuto[$rif['RifID']][$index]['num']=$rif['carico'][$k]['num'];
        $CarichiRifiuto[$rif['RifID']][$index]['ID_MOV_F']=$rif['carico'][$k]['ID_MOV_F'];
        $CarichiRifiuto[$rif['RifID']][$index]['idSIS']=$rif['carico'][$k]['idSIS'];
        $CarichiRifiuto[$rif['RifID']][$index]['FKEpesospecifico']=$rif['FKEpesospecifico'];
        $CarichiRifiuto[$rif['RifID']][$index]['FKEumis']=$rif['FKEumis'];
        $CarichiRifiuto[$rif['RifID']][$index]['ID_IMP']=$rif['ID_IMP'];
        $CarichiRifiuto[$rif['RifID']][$index]['workmode']=$rif['workmode'];
        $CarichiRifiuto[$rif['RifID']][$index]['data']=$rif['carico'][$k]['data'];
        $CarichiRifiuto[$rif['RifID']][$index]['CER']=$rif['COD_CER'];
        $CarichiRifiuto[$rif['RifID']][$index]['desc']=$rif['descrizione'];
        }

    # 2 - sottraggo qta da scaricato fino a quando scaricato resta positivo
//    $i = 0;
//    while ($i<count($CarichiRifiuto[$rif['RifID']]) && $TotS>0){
//        $TotS-=$CarichiRifiuto[$rif['RifID']][$i]['qta'];
//        $PrimoDaScaricare=$i;
//        $i++;
//        }
    for($i=0;$i<count($CarichiRifiuto[$rif['RifID']]);$i++){
        if($TotS>=0){
            $TotS-=$CarichiRifiuto[$rif['RifID']][$i]['qta'];
            $PrimoDaScaricare=$i;
            }
        }
    if($TotS==0) $PrimoDaScaricare++;

//    echo "Rifiuto ".$rif['COD_CER']." ".$rif['descrizione']."<br />";
//    echo "Totale da scaricare: ".abs($TotS).", a partire dal carico ".$CarichiRifiuto[$rif['RifID']][$PrimoDaScaricare]['num']." (index ".$PrimoDaScaricare." di ". (count($CarichiRifiuto[$rif['RifID']])-1) .")<br />";
//    echo "Totale caricato: ".$rif['totC']."<br />";
//    echo "<hr>";
//    die();

//    for($i=0;$i<count($CarichiRifiuto[$rif['RifID']]);$i++){
//        if($TotS>=0){
//            $TotS-=$CarichiRifiuto[$rif['RifID']][$i]['qta'];
//            //print_r("<br />Rifiuto ".$rif['RifID'].", elimino il carico ".$CarichiRifiuto[$rif['RifID']][$i]['num']." ( ".$i." ) restano da scaricare: ".$TotS."<br /><hr>");
//            $CarichiRifiuto[$rif['RifID']][$i]['qta']=abs($TotS);
//            if($TotS<0){
//                $PrimoDaScaricare=$i;
//                //print_r("Primo da scaricare: ".$i."<br />");
//                }
//            }
//        }

    if($PrimoDaScaricare==count($CarichiRifiuto[$rif['RifID']]))
        $CarichiRifiuto = null;
    else
        $CarichiRifiuto[$rif['RifID']] = array_slice($CarichiRifiuto[$rif['RifID']], $PrimoDaScaricare);

//    $count=count($CarichiRifiuto[$rif['RifID']]);
//    for($i=0;$i<$count;$i++){
//        //print_r("<br />Indice: ".$i." elimino se minore di ".$PrimoDaScaricare." ( count=".count($CarichiRifiuto[$rif['RifID']])." )");
//        if($i<$PrimoDaScaricare){
//            unset($CarichiRifiuto[$rif['RifID']][$i]);
//            }
//        }
//
//    $CarichiRifiuto[$rif['RifID']]=array_merge($CarichiRifiuto[$rif['RifID']]);

    return($CarichiRifiuto);

    }



function GetCerName($RifIDs,&$DbObj,&$Lines) {
	//{{{
	$sql = "SELECT descrizione,COD_CER FROM user_schede_rifiuti JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
	$sql .= " WHERE user_schede_rifiuti.ID_RIF $RifIDs ORDER BY COD_CER ASC";
	$DbObj->SdbRead($sql,"DbRecordSet",true,false);
	$tmp = "";
	foreach($DbObj->DbRecordSet as $k=>$des) {
		$tmp .= $des["COD_CER"] . " - " .$des["descrizione"] . "\n";
		$Lines++;
	}
	return substr($tmp,0,-1); //}}}
}
function GetAziendeName($AzTable,$AzKey,$AzIDs,&$DbObj) {
//{{{
	$sql = "SELECT description FROM $AzTable WHERE $AzKey $AzIDs ORDER BY description ASC";
	$DbObj->SdbRead($sql,"DbRecordSet",true,false);
	$tmp = "";
	foreach($DbObj->DbRecordSet as $k=>$des) {
		$tmp .= $des["description"] . ", ";
	}
	return substr($tmp,0,-2); //}}}
}

function SommaMovimenti(&$Ypos,&$TotCarico,&$TotScarico,&$TotPesoDestino,&$TotTara,&$TotLordo,&$TotCosto,&$VarMedia,$PdfObj,$AsCSV,&$CSVbuf) {
//{{{

global $SOGER;

	if(!$AsCSV) {
		$Ypos += 1;
		CheckYPos($Ypos,$PdfObj);
		$PdfObj->Line(5,$Ypos,200,$Ypos);
		$Ypos += 1;
		CheckYPos($Ypos,$PdfObj);
		$PdfObj->SetXY(35,$Ypos);
		$PdfObj->SetFont('Helvetica','B',8);
		$PdfObj->MultiCell(20,4,"Totali",0,"C");
		$PdfObj->SetFont('Helvetica','',8);
		$PdfObj->SetXY(55,$Ypos);
		$PdfObj->MultiCell(15,4,round($TotCarico,2),0,"R");
		$PdfObj->SetXY(70,$Ypos);
		$PdfObj->MultiCell(15,4,round($TotScarico,2),0,"R");
		$PdfObj->SetXY(85,$Ypos);
		$PdfObj->MultiCell(25,4,round($TotPesoDestino,2),0,"R");
		$PdfObj->SetXY(110,$Ypos);
		$PdfObj->MultiCell(15,4,round($TotTara,2),0,"R");
		$PdfObj->SetXY(125,$Ypos);
		$PdfObj->MultiCell(15,4,round($TotLordo,2),0,"R");

		//$PdfObj->SetXY(125,$Ypos);
		//$PdfObj->MultiCell(20,4,"� " . round($VarMedia,2) . "%",0,"R");
		$PdfObj->SetXY(140,$Ypos);
		$PdfObj->MultiCell(15,4,round(($TotCarico-$TotScarico),2),0,"R");

		if($SOGER->UserData["core_impiantiMODULO_ECO"]=="1" AND $SOGER->UserData['core_usersG3']=="1"){
			$PdfObj->SetXY(180,$Ypos);
			$PdfObj->MultiCell(20,4,number_format($TotCosto, 2, ",", ""),0,"R");
			}

		$TotCarico = 0;
		$TotScarico = 0;
		$TotTara = 0;
		$TotLordo = 0;
		$TotPesoDestino = 0;
		$TotCosto = 0;
		$Ypos += 6;
		}
	else {
		$CSVbuf .= AddCSVRow();
		$CSVbuf .= AddCSVcolumn("");
		$CSVbuf .= AddCSVcolumn("");
		$CSVbuf .= AddCSVcolumn("Totali");
		$CSVbuf .= AddCSVcolumn($TotCarico);
		$CSVbuf .= AddCSVcolumn($TotScarico);
		$CSVbuf .= AddCSVcolumn($TotPesoDestino);
		$CSVbuf .= AddCSVcolumn($TotTara);
		$CSVbuf .= AddCSVcolumn($TotLordo);
		//$CSVbuf .= AddCSVcolumn($VarMedia);
		$CSVbuf .= AddCSVcolumn(round(($TotCarico-$TotScarico),2));
		if($SOGER->UserData["core_impiantiMODULO_ECO"]=="1" AND $SOGER->UserData['core_usersG3']){
			$CSVbuf .= AddCSVcolumn($TotCosto);
			}

		$TotCarico = 0;
		$TotScarico = 0;
		$TotTara = 0;
		$TotLordo = 0;
		$TotPesoDestino = 0;
		$TotCosto = 0;
		$CSVbuf .= AddCSVRow();
	}
}

function formatTime($secs) {
   $times = array(3600, 60, 1);
   $time = '';
   $tmp = '';
   for($i = 0; $i < 3; $i++) {
      $tmp = floor($secs / $times[$i]);
      if($tmp < 1) {
         $tmp = '00';
      }
      elseif($tmp < 10) {
         $tmp = '0' . $tmp;
      }
      $time .= $tmp;
      if($i < 2) {
         $time .= ':';
      }
      $secs = $secs % $times[$i];
   }
   return $time;
}

function SommaViaggi(&$Ypos,&$TotScarico,&$TotCosto,&$TotKm,&$TotTime,$PdfObj,$AsCSV,&$CSVbuf) {
//{{{
	if(!$AsCSV) {
		$Ypos += 1;
		CheckYPos($Ypos,$PdfObj);
		$PdfObj->Line(5,$Ypos,200,$Ypos);
		$Ypos += 1;
		CheckYPos($Ypos,$PdfObj);
		$PdfObj->SetXY(35,$Ypos);
		$PdfObj->SetFont('Helvetica','B',8);
		$PdfObj->MultiCell(20,4,"Totali",0,"C");
		$PdfObj->SetFont('Helvetica','',8);
		$PdfObj->SetXY(70,$Ypos);
		$PdfObj->MultiCell(20,4,round($TotScarico,2),0,"R");
		$PdfObj->SetXY(90,$Ypos);
		$TotKm=$TotKm/1000;
		$PdfObj->MultiCell(20,4,number_format($TotKm,"2",",","")." Km",0,"R");
		$PdfObj->SetXY(110,$Ypos);
		$PdfObj->MultiCell(20,4,formatTime($TotTime),0,"R");
		$PdfObj->SetXY(140,$Ypos);
		$PdfObj->MultiCell(15,4,round(($TotCarico-$TotScarico),2),0,"R");
		$PdfObj->SetXY(180,$Ypos);
		$PdfObj->MultiCell(20,4,number_format($TotCosto, 2, ",", ""),0,"R");
		$TotScarico = 0;
		$TotCosto = 0;
		$TotKm = 0;
		$TotTime = 0;
		$Ypos += 6;
		}
	else {
		$CSVbuf .= AddCSVRow();
		$CSVbuf .= AddCSVcolumn("");
		$CSVbuf .= AddCSVcolumn("");
		$CSVbuf .= AddCSVcolumn("Totali");
		$CSVbuf .= AddCSVcolumn($TotScarico);
		$CSVbuf .= AddCSVcolumn(round(($TotCarico-$TotScarico),2));
		$CSVbuf .= AddCSVcolumn($TotCosto);
		$TotScarico = 0;
		$TotCosto = 0;
		$TotKm = 0;
		$TotTime = 0;
		$CSVbuf .= AddCSVRow();
	}
}

function SommaMovimentiCP(&$Ypos,&$TotCarico,&$TotScarico,&$TotPesoDestino,&$VarMedia,$PdfObj,$AsCSV,&$CSVbuf ) {
	if(!$AsCSV) {
		$Ypos += 1;
		CheckYPos($Ypos,$PdfObj);
		$PdfObj->Line(45,$Ypos,163,$Ypos);
		$Ypos += 1;
		CheckYPos($Ypos,$PdfObj);
		$PdfObj->SetXY(45,$Ypos);
		$PdfObj->SetFont('Helvetica','B',8);
		$PdfObj->MultiCell(20,4,"Totali",0,"C");
		$PdfObj->SetFont('Helvetica','',8);
		$PdfObj->SetXY(60,$Ypos);
		$PdfObj->SetXY(80,$Ypos);
		$PdfObj->MultiCell(20,4,round($TotScarico,2),0,"R");
		$PdfObj->SetXY(105,$Ypos);
		$PdfObj->MultiCell(20,4,round($TotPesoDestino,2),0,"R");
		$PdfObj->SetXY(125,$Ypos);
		$PdfObj->MultiCell(20,4,"� " . round($VarMedia,2) . "%",0,"R");
		$PdfObj->SetXY(145,$Ypos);
		$PdfObj->MultiCell(17,4,round(($TotCarico-$TotScarico),2),0,"R");
		$TotCarico = 0;
		$TotScarico = 0;
		$TotPesoDestino = 0;
		$Ypos += 6;
		}
	else {
		$CSVbuf .= AddCSVRow();
		$CSVbuf .= AddCSVcolumn("");
		$CSVbuf .= AddCSVcolumn("");
		$CSVbuf .= AddCSVcolumn("Totali");
		$CSVbuf .= AddCSVcolumn($TotCarico);
		$CSVbuf .= AddCSVcolumn($TotScarico);
		$CSVbuf .= AddCSVcolumn($TotPesoDestino);
		$CSVbuf .= AddCSVcolumn($VarMedia);
		$CSVbuf .= AddCSVcolumn(round(($TotCarico-$TotScarico),2));
		$TotCarico = 0;
		$TotScarico = 0;
		$TotPesoDestino = 0;
		$CSVbuf .= AddCSVRow();
	}
}
function CheckYPos(&$Ypos,&$PdfObj,$asCSV=false) {
//{{{
if($asCSV) {
	return;
}
if($PdfObj->CurOrientation=="P") {
	if($Ypos>260) {
		$PdfObj->addpage();
		$Ypos = 10;
	}
} else {
	if($Ypos>170) {
		$PdfObj->addpage();
		$Ypos = 10;
	}
}
	//}}}
}
function GetTotaliGlobali(&$TotC,&$TotS,&$TotPD,&$TotVar,&$DbObj,$ID_CER,$ID_IMP,$AzID,$AzKey,$DataDa='',$DataA='',$MovTable,$UM = 'Kg.', $peso_spec = 1, $Stat) {
//{{{
	$sql = "SELECT SUM(quantita) AS QC FROM ".$MovTable." WHERE ID_RIF='$ID_CER' AND TIPO='C' AND ID_IMP='$ID_IMP'";
	$sql .= " AND $AzKey='$AzID'";

	if($DataDa!="") {
		$sql .= " AND (DTMOV>='" . $DataDa . "') ";
		}

	if($DataA!="") {
		$sql .= " AND (DTMOV<='" . $DataA . "') ";
		}

	if($Stat=='GlDES'){
		# AMGA registra i formulari in ingresso come scarichi, il programma fa il carico automatico senza destinatario, ma quando loro entrano per fare modifiche, soger propone il primo destinatario autorizzato, loro non lo deselezionano e salvano... viene popolato solamente ID_AZD, posso aggiungere qualche condizione su codice fiscale o ID_UIMD
		$sql .= " AND TRIM(FKEcfiscD)<>'' ";
		}

	$sql.= "AND ".$MovTable.".NMOV<>9999999 ";

	require("../__includes/SOGER_DirectProfilo.php");
	$sql .= " GROUP BY ID_RIF";
	$DbObj->SdbRead($sql,"AuxRecordSet",true,false);
	if($DbObj->DbRecsNum!=0) {
		$TotC = round($DbObj->AuxRecordSet[0]["QC"],2);
	} else {
		$TotC = 0;
	}

	#

	$sql = "SELECT SUM(quantita) AS QS, SUM(IF((PS_DESTINO<>0 OR DEST_ACC_RIFIUTO=3),PS_DESTINO,quantita)) AS QPD FROM ".$MovTable." WHERE ID_RIF='$ID_CER' AND TIPO='S' AND ID_IMP='$ID_IMP'";
	$sql .= " AND $AzKey='$AzID'";

	if($DataDa!="") {
		$sql .= " AND (DTMOV>='" . $DataDa . "') ";
		}

	if($DataA!="") {
		$sql .= " AND (DTMOV<='" . $DataA . "') ";
		}

	$sql.= "AND ".$MovTable.".NMOV<>9999999 ";

	require("../__includes/SOGER_DirectProfilo.php");
	$sql .= " GROUP BY ID_RIF";
	$DbObj->SdbRead($sql,"AuxRecordSet",true,false);
	if($DbObj->DbRecsNum!=0) {

		switch($UM){
			case 'Kg.':
				$QS	= $DbObj->AuxRecordSet[0]["QS"];
				break;
			case 'Litri':
				$QS = $DbObj->AuxRecordSet[0]["QS"]*$peso_spec;
				break;
			case 'Mc.':
				$QS = $DbObj->AuxRecordSet[0]["QS"]*$peso_spec*1000;
				break;
			}
		//echo $ID_CER." SCARICO: ".$QS." DESTINO: ".$DbObj->AuxRecordSet[0]["QPD"]." UM: ".$UM." PS: ".$peso_spec." QS: ".$QS."<hr>";
		$percDes = $QS>0? $DbObj->AuxRecordSet[0]["QPD"]*100/$QS:0;


		$TotVar+=round(abs(100-$percDes),2);

		$TotS = round($DbObj->AuxRecordSet[0]["QS"],2);
		if(is_null($DbObj->AuxRecordSet[0]["QPD"])) {
			$TotPD = 0;
			}
		else {
			$TotPD = round($DbObj->AuxRecordSet[0]["QPD"],2);
			}
		}
	else {
		$TotS = 0;
		$TotPD = 0;
		$TotVar = 0;
		}
	}

function AddCSVcolumn($valueIN) {
	return "\"" . $valueIN . "\"\t";
}
function AddCSVRow() {
	return "\n";
}
function CSVOut($data,$DcName) {
	$DcName .= ".csv";
	header('Content-Type: application/csv');
	header('Content-Length: '.strlen($data));
	if(isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'],'MSIE')) {
		header('Content-Type: application/force-download');
	} else {
		header('Content-Type: application/octet-stream');
	}

	// Under HTTPS and IE8, those headers fix the download problem:
	// http://stackoverflow.com/questions/1242900/problems-with-header-when-displaying-a-pdf-file-in-ie8
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Pragma: public");

	header('Content-disposition: attachment; filename="'.$DcName.'"');
	echo $data;
	die();
}
?>
