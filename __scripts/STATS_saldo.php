<?php
$Ypos = 66;
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
$FEDIT->FGE_PdfBuffer->SetXY(0,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Saldo finale registro al 31/12/".substr($_SESSION["DbInUse"], -4)." - Chiusura registrazioni",0,"C");
$Ypos += 7;
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(15,4,"CER",0,"C");
$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(100,4,"Descrizione",0,"L");
$FEDIT->FGE_PdfBuffer->SetXY(170,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(15,3,"Giacenza",0,"C");	
$Ypos += 7;
$counter = 0;
$Giacenze = array();
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);

//print_r($FEDIT->DbRecordSet);

$rif=array();
$DbRecordSet=$FEDIT->DbRecordSet;

for($r=0;$r<count($DbRecordSet);$r++){
	$rif[$DbRecordSet[$r]['ID_RIF']]['cer']=$DbRecordSet[$r]['COD_CER'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['des']=$DbRecordSet[$r]['descrizione'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['um']= $DbRecordSet[$r]['UnMis'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['ini']=$DbRecordSet[$r]['giac_ini'];
	# lettura sulla tabella dei movimenti
		$sql = "SELECT quantita, TIPO ";
		$sql.= "FROM ".$MovTable." ";
		$sql.= "WHERE ".$MovTable.".ID_RIF='".$DbRecordSet[$r]['ID_RIF']."' ";
		$sql .= " AND ".$MovTable.".NMOV<>9999999 ";
		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		$FEDIT->SdbRead($sql,"DbRecordSet",true,false);
		$TotC=0;$TotS=0;
		if(isset($FEDIT->DbRecordSet)){
			for($m=0;$m<count($FEDIT->DbRecordSet);$m++){
				if($FEDIT->DbRecordSet[$m]['TIPO']=="C")
					$TotC+=$FEDIT->DbRecordSet[$m]['quantita'];
				else
					$TotS+=$FEDIT->DbRecordSet[$m]['quantita'];
				}
			}
		$rif[$DbRecordSet[$r]['ID_RIF']]['c']=$TotC;
		$rif[$DbRecordSet[$r]['ID_RIF']]['s']=$TotS;
	}

//print_r($rif);


for($r=0;$r<count($DbRecordSet);$r++){
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,4,$rif[$DbRecordSet[$r]['ID_RIF']]['cer'],0,"C");
	$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,3,$rif[$DbRecordSet[$r]['ID_RIF']]["des"],0,"L");
	$FEDIT->FGE_PdfBuffer->SetXY(170,$Ypos);	$FEDIT->FGE_PdfBuffer->MultiCell(15,3,round(($rif[$DbRecordSet[$r]['ID_RIF']]["c"]+$rif[$DbRecordSet[$r]['ID_RIF']]["ini"]-$rif[$DbRecordSet[$r]['ID_RIF']]["s"]),2) . " " . $rif[$DbRecordSet[$r]['ID_RIF']]["um"],0,"R");
		
	$Ypos += 6;
}

?>