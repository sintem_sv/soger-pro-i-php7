<?php
session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/fpdf.php");
include("../__libs/SQLFunct.php");
require_once("STATS_funct.php");


	$sql ="SELECT user_schede_rifiuti.ID_RIF, lov_cer.COD_CER, descrizione, CAR_approved, CLASS_approved, CAR_Analisi_approved, et_approved, sk_approved, CAR_Analisi_approved_start, CAR_approved_start, CLASS_approved_start, et_approved_start, sk_approved_start, CAR_Analisi_approved_end, CAR_approved_end, CLASS_approved_end, et_approved_end, sk_approved_end ";
	$sql.="FROM user_schede_rifiuti ";
	$sql.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
	$sql.="WHERE user_schede_rifiuti.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND user_schede_rifiuti.".$SOGER->UserData['workmode']."=1 ";
	$sql.="ORDER BY COD_CER ASC, descrizione ASC, ID_RIF ASC ";
	$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

	$orientation="L";
	$statTitle = "SCADENZA APPROVAZIONI";
	$statDesc  = "Lista scadenze dei documenti approvati";
	$DcName = date("d/m/Y") . "--Scadenza_Approvazioni";
	
	$PrintImpianto = true;
	$PrintCER = false;
	$SkipStandardNoRecs = true;

	$um="mm";
	$Format = array(210,297);
	$ZeroMargin = true;
	$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin,$DcName);

	## stats title
	$Ypos+=10;
	$FEDIT->FGE_PdfBuffer->SetXY(0,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',14);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,15,$statTitle,0,"C"); 


	## stat desc
	if(!isset($statDesc)) $statDesc=" -- ";
	$Xpos=20;
	$Ypos+=15;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Descrizione:",0,"L");
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(40,25);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$statDesc,0,"L");

	## intestatario
	$intestatario = strtolower($SOGER->UserData["core_impiantidescription"]) . " (" . $SOGER->UserData["workmode"] . ")";
	$Ypos+=7;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Intestatario:",0,"L");
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(40,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$intestatario,0,"L");

	
	$Ypos+=20;


	# intestazione dati
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,10,"CER","TLBR","C");
	$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,10,"Descrizione","TLBR","L");
	$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"Caratterizzazione","TLBR","C");
	$FEDIT->FGE_PdfBuffer->SetXY(155,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"Classificazione","TLBR","C");
	$FEDIT->FGE_PdfBuffer->SetXY(185,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"Etichetta","TLBR","C");
	$FEDIT->FGE_PdfBuffer->SetXY(215,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"Proc. sicurezza","TLBR","C");
	$FEDIT->FGE_PdfBuffer->SetXY(245,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"Analisi","TLBR","C");
	
	$Ypos += 15;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);

	for($r=0;$r<count($FEDIT->DbRecordSet);$r++){
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
		$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,10,$FEDIT->DbRecordSet[$r]['COD_CER'],"B","C");
		$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(100,10,$FEDIT->DbRecordSet[$r]['descrizione'],"B","L");

		#caratterizzazione
		$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
				$gotCaratterizzazione=false;
		if(	file_exists('../__upload/caratterizzazioni/'.$FEDIT->DbRecordSet[$r]["ID_RIF"].'.pdf') | file_exists('../__upload/caratterizzazioni/'.$FEDIT->DbRecordSet[$r]["ID_RIF"].'.rtf') | file_exists('../__upload/caratterizzazioni/'.$FEDIT->DbRecordSet[$r]["ID_RIF"].'.doc') ) 
			$gotCaratterizzazione=true;
		if($gotCaratterizzazione){
			if($FEDIT->DbRecordSet[$r]['CAR_approved']==1){
				list($anno, $mese, $giorno) = explode("-", $FEDIT->DbRecordSet[$r]['CAR_approved_start']);
				$FEDIT->FGE_PdfBuffer->MultiCell(30,5,"App.: ".$giorno."/".$mese."/".$anno,0,"C");
				$Ypos += 5;
				$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
				list($anno, $mese, $giorno) = explode("-", $FEDIT->DbRecordSet[$r]['CAR_approved_end']);
				$FEDIT->FGE_PdfBuffer->MultiCell(30,5,"Scad.: ".$giorno."/".$mese."/".$anno ,"B","C");
				$Ypos -= 5;
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"Non approvato","B","C");
			}
		else
			$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"Nessun doc.","B","C");

		#classificazione
		$FEDIT->FGE_PdfBuffer->SetXY(155,$Ypos);
		$gotClassificazione=false;
		if(	file_exists('../__upload/classificazioni/'.$FEDIT->DbRecordSet[$r]["ID_RIF"].'.pdf') | file_exists('../__upload/classificazioni/'.$FEDIT->DbRecordSet[$r]["ID_RIF"].'.rtf') | file_exists('../__upload/classificazioni/'.$FEDIT->DbRecordSet[$r]["ID_RIF"].'.doc') ) 
			$gotClassificazione=true;
		if($gotClassificazione){
			if($FEDIT->DbRecordSet[$r]['CLASS_approved']==1){
				list($anno, $mese, $giorno) = explode("-", $FEDIT->DbRecordSet[$r]['CLASS_approved_start']);
				$FEDIT->FGE_PdfBuffer->MultiCell(30,5,"App.: ".$giorno."/".$mese."/".$anno,0,"C");
				$Ypos += 5;
				$FEDIT->FGE_PdfBuffer->SetXY(155,$Ypos);
				list($anno, $mese, $giorno) = explode("-", $FEDIT->DbRecordSet[$r]['CLASS_approved_end']);
				$FEDIT->FGE_PdfBuffer->MultiCell(30,5,"Scad.: ".$giorno."/".$mese."/".$anno ,"B","C");
				$Ypos -= 5;
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"Non approvato","B","C");
			}
		else
			$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"Nessun doc.","B","C");

		#etichetta
		$FEDIT->FGE_PdfBuffer->SetXY(185,$Ypos);
		if($FEDIT->DbRecordSet[$r]['et_approved']==1){
			list($anno, $mese, $giorno) = explode("-", $FEDIT->DbRecordSet[$r]['et_approved_start']);
			$FEDIT->FGE_PdfBuffer->MultiCell(30,5,"App.: ".$giorno."/".$mese."/".$anno,0,"C");
			$Ypos += 5;
			$FEDIT->FGE_PdfBuffer->SetXY(185,$Ypos);
			list($anno, $mese, $giorno) = explode("-", $FEDIT->DbRecordSet[$r]['et_approved_end']);
			$FEDIT->FGE_PdfBuffer->MultiCell(30,5,"Scad.: ".$giorno."/".$mese."/".$anno ,"B","C");
			$Ypos -= 5;
			}
		else
			$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"Non approvato","B","C");

		#sicurezza
		$FEDIT->FGE_PdfBuffer->SetXY(215,$Ypos);
				$gotSicurezza=false;
		if(	file_exists('../__upload/sicurezza/'.$FEDIT->DbRecordSet[$r]["ID_RIF"].'.pdf') | file_exists('../__upload/sicurezza/'.$FEDIT->DbRecordSet[$r]["ID_RIF"].'.rtf') | file_exists('../__upload/sicurezza/'.$FEDIT->DbRecordSet[$r]["ID_RIF"].'.doc') ) 
			$gotSicurezza=true;
		if($gotSicurezza){
			if($FEDIT->DbRecordSet[$r]['sk_approved']==1){
				list($anno, $mese, $giorno) = explode("-", $FEDIT->DbRecordSet[$r]['sk_approved_start']);
				$FEDIT->FGE_PdfBuffer->MultiCell(30,5,"App.: ".$giorno."/".$mese."/".$anno,0,"C");
				$Ypos += 5;
				$FEDIT->FGE_PdfBuffer->SetXY(205,$Ypos);
				list($anno, $mese, $giorno) = explode("-", $FEDIT->DbRecordSet[$r]['sk_approved_end']);
				$FEDIT->FGE_PdfBuffer->MultiCell(30,5,"Scad.: ".$giorno."/".$mese."/".$anno ,"B","C");
				$Ypos -= 5;
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"Non approvato","B","C");
			}
		else
			$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"Nessun doc.","B","C");
		
		#analisi
		$FEDIT->FGE_PdfBuffer->SetXY(245,$Ypos);
		$gotAnalisi=false;
		if(file_exists('../__upload/analisi/'.$FEDIT->DbRecordSet[$r]["ID_RIF"].'.pdf')) $gotAnalisi=true;
		if($gotAnalisi){
			if($FEDIT->DbRecordSet[$r]['CAR_Analisi_approved']==1){
				list($anno, $mese, $giorno) = explode("-", $FEDIT->DbRecordSet[$r]['CAR_Analisi_approved_start']);
				$FEDIT->FGE_PdfBuffer->MultiCell(30,5,"App.: ".$giorno."/".$mese."/".$anno,0,"C");
				$Ypos += 5;
				$FEDIT->FGE_PdfBuffer->SetXY(245,$Ypos);
				list($anno, $mese, $giorno) = explode("-", $FEDIT->DbRecordSet[$r]['CAR_Analisi_approved_end']);
				$FEDIT->FGE_PdfBuffer->MultiCell(30,5,"Scad.: ".$giorno."/".$mese."/".$anno ,"B","C");
				$Ypos -= 5;
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"Non approvato","B","C");
			}
		else
			$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"Nessun doc.","B","C");
		
		$Ypos += 11;
		}

	
	
	PDFOut($FEDIT->FGE_PdfBuffer,$DcName);

?>