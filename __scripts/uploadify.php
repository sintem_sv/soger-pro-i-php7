<?php
/*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/

$targetFolder = $_POST['DestinationFolder']; // Relative to the root
$verifyToken = md5('unique_salt' . $_POST['timestamp']);

$LOG ="";

if (!empty($_FILES) && $_POST['token'] == $verifyToken) {

	if(move_uploaded_file($_FILES['Filedata']['tmp_name'], $targetFolder."/".$_FILES['Filedata']['name'])){

		// Copio il contenuto di certificato.cer
		$handle1	= fopen($targetFolder."/"."certificato.cer","r"); 
		$certificato= fread($handle1, filesize($targetFolder."/"."certificato.cer")); 
		fclose ($handle1);

		// Copio il contenuto della private key
		$handle2	= fopen($targetFolder."/".$_FILES['Filedata']['name'],"r"); 
		$privatekey = fread($handle2, filesize($targetFolder."/".$_FILES['Filedata']['name'])); 
		fclose ($handle2);

		// Creo nuovo file concatenazione di certificato e private key
		$filename	= "crt_key.pem";
		if(file_exists($targetFolder."/".$filename)) unlink($targetFolder."/".$filename);
		$handle3	= fopen($targetFolder."/".$filename, 'a');
		fwrite($handle3, $certificato."\r\n\r\n".$privatekey);

		$LOG.="Private key caricata correttamente sul server.\r\n";
		$LOG.="E' ora possibile chiudere la finestra.\r\n";
		}
	else{
		$LOG.="Attenzione! \r\n";
		$LOG.="Si e' verificato un errore in fase di caricamento del file. Riprovare e, se l'errore dovesse ripetersi, contattare l'assistenza tecnica.";
		/*
		$LOG.="TMP_NAME: ".$_FILES['Filedata']['tmp_name']."\r\n";
		$LOG.="TARGET: ".$targetFolder."\r\n";
		$LOG.="CURRENT: ".getcwd()."\r\n";
		$LOG.="NAME: ".$_FILES['Filedata']['name'];
		*/
		}
	}
else{
	$LOG.="Attenzione! \r\n";
	$LOG.="Token mismatch: contattare l'assistenza tecnica.";
	}

echo $LOG;


?>