<?php
	$Ypos +=10;
	require("STATS_palette.php");
	$ToScreen = array();
	$QuantitaTotale = 0;
	$ColPointer = 0;
	foreach($FEDIT->DbRecordSet as $k=>$dati) {
			$ToScreen[] = array(
				"CER" => $dati["COD_CER"],
				"DES" => $dati["CERdes"],
				"TOT" => $dati["Totale"],
				"COL" => $palette[$ColPointer]
			);
			$QuantitaTotale += $dati["Totale"];
			$ColPointer++;		
			if($ColPointer==count($palette)) {
				$ColPointer = 0;
			}
	}
	#
	#	RICAVA GIACENZE INIZIALI
	#
	//if($_POST["Stat"]=="DistCER") {
	if($_POST['MovType']=='C'){
		$sql = " SELECT COD_CER,SUM(giac_ini) AS GINI FROM user_schede_rifiuti JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER"; 
		$sql .= " WHERE user_schede_rifiuti.ID_RIF ";
		$sql .= " IN (";
		$tmpRif = "";
		foreach($_POST["RifIDs"] as $k=>$ID_RIF) {
			$tmpRif .= "$ID_RIF,";
		}
		$tmpRif = substr($tmpRif,0,-1) . ") GROUP BY user_schede_rifiuti.ID_CER";
		$sql .= $tmpRif;
		$FEDIT->SdbRead($sql,"DbRecordSet",true,false);
		foreach($FEDIT->DbRecordSet as $k=>$dati) {
			for($c=0;$c<count($ToScreen);$c++) {
				if($ToScreen[$c]["CER"]==$dati["COD_CER"]) {
					$ToScreen[$c]["GINI"] = $dati["GINI"];
					$ToScreen[$c]["TOT"] += $dati["GINI"];
					$QuantitaTotale += $dati["GINI"];
				}
			}
		}
	}
	#
	#	RICAVA PERCENTUALE
	#
	for($c=0;$c<count($ToScreen);$c++) {
		if($QuantitaTotale!=0) {
			$ToScreen[$c]["PERC"] = round((($ToScreen[$c]["TOT"]*100)/$QuantitaTotale),2);
		} else {
			$ToScreen[$c]["PERC"] = 0;
		}
	}
	#
	#	DISEGNA GRAFICO
	#
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	foreach($ToScreen as $k=>$dati) {
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,3,$dati["CER"] . " - " . $dati["DES"],0,"L");
		$Ypos +=7;
		#
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFillColor($dati["COL"][0],$dati["COL"][1],$dati["COL"][2]);
		$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
		if($dati["PERC"]!=0) {
			$Lunghezza = round(((150/100)*$dati["PERC"]),2);
		} else {
			$Lunghezza = 0.1;
		}
		$FEDIT->FGE_PdfBuffer->MultiCell($Lunghezza,5,"",0,"L",1);
		#
		$FEDIT->FGE_PdfBuffer->SetXY($Lunghezza+15,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,5,round($dati["TOT"],2) . " Kg. / " . $dati["PERC"] . "%",0,"L");
		$Ypos +=10;
	}
?>
