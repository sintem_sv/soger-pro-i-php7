<?php
$Ypos = 73;
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(15,4,"CER",0,"C");
$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(100,4,"Descrizione",0,"L");
$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(40,3,"Stato fisico",0,"L");	
$FEDIT->FGE_PdfBuffer->SetXY(165,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(45,5,"Giacenza attuale",0,"C");
$FEDIT->FGE_PdfBuffer->SetXY(165,$Ypos+5);
$FEDIT->FGE_PdfBuffer->MultiCell(15,5,"Kg.",0,"C");
$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos+5);
$FEDIT->FGE_PdfBuffer->MultiCell(15,5,"Litri",0,"C");
$FEDIT->FGE_PdfBuffer->SetXY(195,$Ypos+5);
$FEDIT->FGE_PdfBuffer->MultiCell(15,5,"Mc.",0,"C");

$FEDIT->FGE_PdfBuffer->SetXY(210,$Ypos);
//$FEDIT->FGE_PdfBuffer->MultiCell(35,5,"Classi di pericolo H",0,"C");		
//$FEDIT->FGE_PdfBuffer->SetXY(245,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(35,5,"Classi di pericolo HP",0,"C");		

$Ypos += 12;
$counter = 0;
$Giacenze = array();
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);

//print_r($FEDIT->DbRecordSet);

$rif=array();
$DbRecordSet=$FEDIT->DbRecordSet;

for($r=0;$r<count($DbRecordSet);$r++){
	$rif[$DbRecordSet[$r]['ID_RIF']]['cer']			= $DbRecordSet[$r]['COD_CER'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['PERICOLOSO']	= $DbRecordSet[$r]['PERICOLOSO'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['des']			= $DbRecordSet[$r]['descrizione'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['um']			= $DbRecordSet[$r]['UnMis'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['ini']			= $DbRecordSet[$r]['giac_ini'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['StatoFisico']	= $DbRecordSet[$r]['StatoFisico'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['peso_spec']	= $DbRecordSet[$r]['peso_spec'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['H1']			= $DbRecordSet[$r]['H1'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['H2']			= $DbRecordSet[$r]['H2'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['H3A']			= $DbRecordSet[$r]['H3A'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['H3B']			= $DbRecordSet[$r]['H3B'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['H4']			= $DbRecordSet[$r]['H4'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['H5']			= $DbRecordSet[$r]['H5'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['H6']			= $DbRecordSet[$r]['H6'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['H7']			= $DbRecordSet[$r]['H7'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['H8']			= $DbRecordSet[$r]['H8'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['H9']			= $DbRecordSet[$r]['H9'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['H10']			= $DbRecordSet[$r]['H10'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['H11']			= $DbRecordSet[$r]['H11'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['H12']			= $DbRecordSet[$r]['H12'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['H13']			= $DbRecordSet[$r]['H13'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['H14']			= $DbRecordSet[$r]['H14'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['H15']			= $DbRecordSet[$r]['H15'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['HP1']			= $DbRecordSet[$r]['HP1'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['HP2']			= $DbRecordSet[$r]['HP2'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['HP3']			= $DbRecordSet[$r]['HP3'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['HP4']			= $DbRecordSet[$r]['HP4'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['HP5']			= $DbRecordSet[$r]['HP5'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['HP6']			= $DbRecordSet[$r]['HP6'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['HP7']			= $DbRecordSet[$r]['HP7'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['HP8']			= $DbRecordSet[$r]['HP8'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['HP9']			= $DbRecordSet[$r]['HP9'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['HP10']		= $DbRecordSet[$r]['HP10'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['HP11']		= $DbRecordSet[$r]['HP11'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['HP12']		= $DbRecordSet[$r]['HP12'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['HP13']		= $DbRecordSet[$r]['HP13'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['HP14']		= $DbRecordSet[$r]['HP14'];
	$rif[$DbRecordSet[$r]['ID_RIF']]['HP15']		= $DbRecordSet[$r]['HP15'];

	# lettura sulla tabella dei movimenti
	$sql = "SELECT quantita, TIPO ";
	$sql.= "FROM ".$MovTable." ";
	$sql.= "WHERE ".$MovTable.".ID_RIF='".$DbRecordSet[$r]['ID_RIF']."' ";
	$sql.= "AND ".$MovTable.".NMOV<>9999999 ";
	$TableSpec = $MovTable.".";
	require("../__includes/SOGER_DirectProfilo.php");
	$FEDIT->SdbRead($sql,"DbRecordSet",true,false);
	$TotC=0;$TotS=0;
	if(isset($FEDIT->DbRecordSet)){
		for($m=0;$m<count($FEDIT->DbRecordSet);$m++){
			if($FEDIT->DbRecordSet[$m]['TIPO']=="C")
				$TotC+=$FEDIT->DbRecordSet[$m]['quantita'];
			else
				$TotS+=$FEDIT->DbRecordSet[$m]['quantita'];
			}
		}
	$rif[$DbRecordSet[$r]['ID_RIF']]['c']=$TotC;
	$rif[$DbRecordSet[$r]['ID_RIF']]['s']=$TotS;
	}

for($r=0;$r<count($DbRecordSet);$r++){
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,$rif[$DbRecordSet[$r]['ID_RIF']]['cer'],0,"C");
	$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$rif[$DbRecordSet[$r]['ID_RIF']]["des"],0,"L");
	$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(40,5,$rif[$DbRecordSet[$r]['ID_RIF']]["StatoFisico"],0,"L");

	# giacenza
	$GiacenzaUM = round( ($rif[$DbRecordSet[$r]['ID_RIF']]["c"]+$rif[$DbRecordSet[$r]['ID_RIF']]["ini"]-$rif[$DbRecordSet[$r]['ID_RIF']]["s"]),2);
	
	switch($rif[$DbRecordSet[$r]['ID_RIF']]['um']) {
		case "Kg.":	# KG
			$KG = $GiacenzaUM;  
			$M3 = round(KgQConvert($GiacenzaUM,"M3",$rif[$DbRecordSet[$r]['ID_RIF']]['peso_spec']),2);
			$L = round(KgQConvert($GiacenzaUM,"Litri",$rif[$DbRecordSet[$r]['ID_RIF']]['peso_spec']),2);
			break;
		
		case "Litri":	# LT
			$L = $GiacenzaUM;
			$KG = round($GiacenzaUM*$rif[$DbRecordSet[$r]['ID_RIF']]['peso_spec'],2);
			$M3 = round(KgQConvert($KG,"M3",$rif[$DbRecordSet[$r]['ID_RIF']]['peso_spec']),2);
			break;
		
		case "Mc.":	# METRI CUBI
			$M3 = $GiacenzaUM; 
			$KG = round($GiacenzaUM*$rif[$DbRecordSet[$r]['ID_RIF']]['peso_spec']*1000,2);
			$L = round($GiacenzaUM*$rif[$DbRecordSet[$r]['ID_RIF']]['peso_spec'],2);
			break;		
		}


	// aggiorno totali
	if($rif[$DbRecordSet[$r]['ID_RIF']]['PERICOLOSO']=="1"){ 
		$TotM3_P += round($M3,2);
		$TotKg_P += round($KG,2);
		$TotL_P  += round($L,2);
		}
	else{
		$TotM3_NP += round($M3,2);
		$TotKg_NP += round($KG,2);
		$TotL_NP  += round($L,2);
		}

	$TotM3 += round($M3,2);
	$TotKg += round($KG,2);
	$TotL  += round($L,2);

	// kg
	$kg = $TotCarico+$gINI-$TotScarico;
	$FEDIT->FGE_PdfBuffer->SetXY(165,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,$KG,0,"C");

	// lt
	$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,$L,0,"C");

	// m3
	$FEDIT->FGE_PdfBuffer->SetXY(195,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,number_format($M3, 2),0,"C");
	

	# classi h
//	$ClassiH='';
//	if($rif[$DbRecordSet[$r]['ID_RIF']]["H1"]=="1")  $ClassiH.="H1 ";
//	if($rif[$DbRecordSet[$r]['ID_RIF']]["H2"]=="1")  $ClassiH.="H2 ";
//	if($rif[$DbRecordSet[$r]['ID_RIF']]["H3A"]=="1") $ClassiH.="H3A ";
//	if($rif[$DbRecordSet[$r]['ID_RIF']]["H3B"]=="1") $ClassiH.="H3B ";
//	if($rif[$DbRecordSet[$r]['ID_RIF']]["H4"]=="1")  $ClassiH.="H4 ";
//	if($rif[$DbRecordSet[$r]['ID_RIF']]["H5"]=="1")  $ClassiH.="H5 ";
//	if($rif[$DbRecordSet[$r]['ID_RIF']]["H6"]=="1")  $ClassiH.="H6 ";
//	if($rif[$DbRecordSet[$r]['ID_RIF']]["H7"]=="1")  $ClassiH.="H7 ";
//	if($rif[$DbRecordSet[$r]['ID_RIF']]["H8"]=="1")  $ClassiH.="H8 ";
//	if($rif[$DbRecordSet[$r]['ID_RIF']]["H9"]=="1")  $ClassiH.="H9 ";
//	if($rif[$DbRecordSet[$r]['ID_RIF']]["H10"]=="1") $ClassiH.="H10 ";
//	if($rif[$DbRecordSet[$r]['ID_RIF']]["H11"]=="1") $ClassiH.="H11 ";
//	if($rif[$DbRecordSet[$r]['ID_RIF']]["H12"]=="1") $ClassiH.="H12 ";
//	if($rif[$DbRecordSet[$r]['ID_RIF']]["H13"]=="1") $ClassiH.="H13 ";
//	if($rif[$DbRecordSet[$r]['ID_RIF']]["H14"]=="1") $ClassiH.="H14 ";
//	if($rif[$DbRecordSet[$r]['ID_RIF']]["H15"]=="1") $ClassiH.="H15 ";
//	$FEDIT->FGE_PdfBuffer->SetXY(210,$Ypos);
//	$FEDIT->FGE_PdfBuffer->MultiCell(35,5,$ClassiH,0,"L");
//	unset($ClassiH);

	# classi HP
	$ClassiHP='';
	if($rif[$DbRecordSet[$r]['ID_RIF']]["HP1"]=="1")  $ClassiHP.="HP1 ";
	if($rif[$DbRecordSet[$r]['ID_RIF']]["HP2"]=="1")  $ClassiHP.="HP2 ";
	if($rif[$DbRecordSet[$r]['ID_RIF']]["HP3"]=="1")  $ClassiHP.="HP3 ";
	if($rif[$DbRecordSet[$r]['ID_RIF']]["HP4"]=="1")  $ClassiHP.="HP4 ";
	if($rif[$DbRecordSet[$r]['ID_RIF']]["HP5"]=="1")  $ClassiHP.="HP5 ";
	if($rif[$DbRecordSet[$r]['ID_RIF']]["HP6"]=="1")  $ClassiHP.="HP6 ";
	if($rif[$DbRecordSet[$r]['ID_RIF']]["HP7"]=="1")  $ClassiHP.="HP7 ";
	if($rif[$DbRecordSet[$r]['ID_RIF']]["HP8"]=="1")  $ClassiHP.="HP8 ";
	if($rif[$DbRecordSet[$r]['ID_RIF']]["HP9"]=="1")  $ClassiHP.="HP9 ";
	if($rif[$DbRecordSet[$r]['ID_RIF']]["HP10"]=="1") $ClassiHP.="HP10 ";
	if($rif[$DbRecordSet[$r]['ID_RIF']]["HP11"]=="1") $ClassiHP.="HP11 ";
	if($rif[$DbRecordSet[$r]['ID_RIF']]["HP12"]=="1") $ClassiHP.="HP12 ";
	if($rif[$DbRecordSet[$r]['ID_RIF']]["HP13"]=="1") $ClassiHP.="HP13 ";
	if($rif[$DbRecordSet[$r]['ID_RIF']]["HP14"]=="1") $ClassiHP.="HP14 ";
	if($rif[$DbRecordSet[$r]['ID_RIF']]["HP15"]=="1") $ClassiHP.="HP15 ";
	//$FEDIT->FGE_PdfBuffer->SetXY(245,$Ypos);
        $FEDIT->FGE_PdfBuffer->SetXY(210,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(35,5,$ClassiHP,0,"L");
	unset($ClassiHP);

	$Ypos += 10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	}

$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(40,5,"Totale ","TLRB","R");
$FEDIT->FGE_PdfBuffer->SetXY(165,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(15,5,round($TotKg,2),"TLRB","C");
$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(15,5,round($TotL,2),"TLRB","C");
$FEDIT->FGE_PdfBuffer->SetXY(195,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(15,5,round($TotM3,2),"TLRB","C");

$Ypos+=7;

$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(40,5,"Totale rifiuti pericolosi","TLRB","R");
$FEDIT->FGE_PdfBuffer->SetXY(165,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(15,5,round($TotKg_P,2),"TLRB","C");
$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(15,5,round($TotL_P,2),"TLRB","C");
$FEDIT->FGE_PdfBuffer->SetXY(195,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(15,5,round($TotM3_P,2),"TLRB","C");

$Ypos+=7;

$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(40,5,"Totale rifiuti non pericolosi","TLRB","R");
$FEDIT->FGE_PdfBuffer->SetXY(165,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(15,5,round($TotKg_NP,2),"TLRB","C");
$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(15,5,round($TotL_NP,2),"TLRB","C");
$FEDIT->FGE_PdfBuffer->SetXY(195,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(15,5,round($TotM3_NP,2),"TLRB","C");


$PTotM3 = true;
$TotM3 = 0;
$PTotKg = true;
$TotKg = 0;
$PTotL = true;
$TotL = 0;

$TotM3_P = 0;
$TotKg_P = 0;
$TotL_P = 0;

$TotM3_NP = 0;
$TotKg_NP = 0;	
$TotL_NP = 0;

?>