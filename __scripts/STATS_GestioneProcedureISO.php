<?php
session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/fpdf.php");
include("../__libs/SQLFunct.php");
require_once("STATS_funct.php");
#
global $SOGER;


$orientation="L";
$statTitle = "GESTIONE PROCEDURE ISO14001";
$statDesc = "Riepilogo delle procedure ISO14001 adottate e normativa di riferimento.";
$DcName = date("d/m/Y") . "--Gestione_Procedure_ISO14001";

$PrintImpianto = true;
$PrintCER = false;
$SkipStandardNoRecs = true;

$um="mm";
$Format = array(210,297);
$ZeroMargin = true;
$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin,$DcName);

## stats title
$Ypos+=10;
$FEDIT->FGE_PdfBuffer->SetXY(0,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',14);
$FEDIT->FGE_PdfBuffer->MultiCell(0,15,$statTitle,0,"C"); 


## stat desc
if(!isset($statDesc)) $statDesc=" -- ";
$Xpos=20;
$Ypos+=15;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Descrizione:",0,"L");
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(40,25);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$statDesc,0,"L");

## intestatario
$intestatario = strtolower($SOGER->UserData["core_impiantidescription"]) . " (" . $SOGER->UserData["workmode"] . ")";
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Intestatario:",0,"L");
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(40,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$intestatario,0,"L");


$Ypos+=20;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

# header
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',7);
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(60,10,"Normativa di riferimento","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(70,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(60,10,"Norma ISO14001","TLBR","L");
$FEDIT->FGE_PdfBuffer->SetXY(130,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(50,10,"Funzione SO.Ge.R. PRO","TLBR","C");
$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(50,10,"Procedura aziendale","TLBR","C");
$FEDIT->FGE_PdfBuffer->SetXY(230,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(50,10,"Utenze autorizzate","TLBR","C");
$Ypos += 10; //62

$MyY=62;
$MyYprev=$MyY;

$sql ="SELECT core_impianti_iso.ID_ISOF, procedura, normaRifiuti, normaISO, funzionePRO, requirementsPRO FROM core_impianti_iso ";
$sql.="JOIN lov_iso_funct ON core_impianti_iso.ID_ISOF=lov_iso_funct.ID_ISOF ";
$sql.="WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1;";
$FEDIT->SDBRead($sql,"DbRecordSet");

for($c=0;$c<count($FEDIT->DbRecordSet);$c++){
	# rows
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(60,5,$FEDIT->DbRecordSet[$c]['normaRifiuti'],"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(70,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(60,5,$FEDIT->DbRecordSet[$c]['normaISO'],"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(130,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,5,$FEDIT->DbRecordSet[$c]['funzionePRO'],"T","C");
	$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,5,$FEDIT->DbRecordSet[$c]['procedura'],"T","C");
		
	$SQL ="SELECT nome, cognome, usr FROM core_users WHERE ".$SOGER->UserData['workmode']."=1 AND approved=1 AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$FEDIT->DbRecordSet[$c]['requirementsPRO']."=1 ";
	// Andrebbe aggiunto un campo del tipo "isSintem" o "isAssistenza"
	$SQL.="AND usr <>'sintem' ";
	$FEDIT->SDBRead($SQL,"AllowedUsers");
	$Users="";
	for($u=0;$u<count($FEDIT->AllowedUsers);$u++){
		$Users.=$FEDIT->AllowedUsers[$u]['nome']." ".$FEDIT->AllowedUsers[$u]['cognome'].", ";
		}
	$FEDIT->FGE_PdfBuffer->SetXY(230,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,5,$Users,"T","C");
	$Ypos += 30;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	}

PDFOut($FEDIT->FGE_PdfBuffer,$DcName);

?>