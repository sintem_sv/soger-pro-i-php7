<?php
$counter = 0;

//print_r($FEDIT->DbRecordSet);

foreach($FEDIT->DbRecordSet as $k=>$dati) {
	
	if(!is_null($dati["PS_DESTINO"])){

		switch($dati["UnMis"]){
			case 'Kg.':
				$QS	= $dati["quantita"];
				break;
			case 'Litri':
				$QS = $dati["quantita"]*$dati["peso_spec"];
				break;
			case 'Mc.':
				$QS = $dati["quantita"]*$dati["peso_spec"]*1000;
				break;
			}

		$percDes = @round($dati["PS_DESTINO"]*100/$QS,2);

		//$percDes = @round($dati["PS_DESTINO"]*100/$dati["quantita"],2);
		$percDesAbs=abs(100-$percDes);
		if($percDesAbs>$dati["MaxVar"]){

			if($dati["AZref"]!=$tmpAZ) {
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
				# dati azienda
				$Ypos += 10;
				$tmpAZ =$dati["AZref"];
				$tmpIMP = "";
				$tmpCER = "";
		//{{{ 
				if(!$asCSV) {
					$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
					$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$dati["AZdes"],0,"L");
					$Ypos += 4;
					$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
					$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
					$AzAddr = $dati["AZind"] . " - " . $dati["AZcom"] . " (" . $dati["AZshprov"] . ") - " . $dati["AZnaz"];    
					$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$AzAddr,0,"L");
					$Ypos += 4; 
				} else {
					$azienda = $dati["AZdes"] . " " . $dati["AZind"] . " - " . $dati["AZcom"] . " (" . $dati["AZshprov"] . ") - " . $dati["AZnaz"];
					$CSVbuf .= AddCSVcolumn($azienda);
					$CSVbuf .= AddCSVRow();
				}
				//}}}
			}
			if($PrintImpianto) {
				if($dati["IMPref"]!=$tmpIMP) {
					CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
					$tmpIMP = $dati["IMPref"];
					$tmpCER = "";
					# dati impianto
			//{{{ 
					if(!$asCSV) {
						$Ypos += 3;
						$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
						$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
						$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Impianto di:",0,"L");
						$ImpAddr = $dati["IMPdes"] . " - " . $dati["IMPcom"] . " (" . $dati["IMPshprov"] . ") - " . $dati["IMPnaz"];
						$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
						$FEDIT->FGE_PdfBuffer->SetXY(32,$Ypos);
						$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$ImpAddr,0,"L");
						$Ypos += 4;
					} else {
						$impianto = "Impianto di: " .  $dati["IMPdes"] . " - " . $dati["IMPcom"] . " (" . $dati["IMPshprov"] . ") - " . $dati["IMPnaz"];
						$CSVbuf .= AddCSVcolumn($impianto);
						$CSVbuf .= AddCSVRow();
						$CSVbuf .= AddCSVRow();
					}
					//}}}
				}
			}
			if($tmpCER!=$dati["CERref"]) {
				$counterVar = 0;
				$TotVar = 0;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
				$tmpCER = $dati["CERref"];
				$CERdes = $dati["COD_CER"] . " - " . $dati["descrizione"];
				# dati cer
		//{{{ 
				if(!$asCSV) {
					$Ypos += 1;
					$FEDIT->FGE_PdfBuffer->Line(17,$Ypos,180, $Ypos);
					$Ypos += 1;
					$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
					$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
					$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$CERdes,0,"L");
					$Ypos += 6; 
				} else {
					$CSVbuf .= AddCSVRow();
					$CSVbuf .= AddCSVcolumn($CERdes);			
				}
				
				//}}}
				# intestazione movimenti + Giacenza Iniziale
		//{{{ 
				if(is_null($dati["giac_ini"])) {
					$giacINI = 0;	
				} else {
					$giacINI = $dati["giac_ini"];
				}
				if(!$asCSV) {
					CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
					$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
					$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Movimento",0,"C");
					$FEDIT->FGE_PdfBuffer->SetXY(35,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(10,4,"Tipo",0,"C");
					$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Data Mov.",0,"C");
					$FEDIT->FGE_PdfBuffer->SetXY(65,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Peso stimato",0,"C");
					$FEDIT->FGE_PdfBuffer->SetXY(85,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(25,4,"Peso a Destino",0,"C");
					$FEDIT->FGE_PdfBuffer->SetXY(110,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(17,4,"Variazione %",0,"C");
					$FEDIT->FGE_PdfBuffer->SetXY(130,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Scarto ammesso",0,"C");
					$FEDIT->FGE_PdfBuffer->SetXY(147,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(25,4,"Formulario",0,"C");
					$FEDIT->FGE_PdfBuffer->SetXY(172,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(10,4,"ADR",0,"C");
					$Ypos += 4;
					$FEDIT->FGE_PdfBuffer->SetXY(64,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(22,4,$dati["UnMis"],0,"C");
					$FEDIT->FGE_PdfBuffer->SetXY(85,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(25,4,"Kg.",0,"C");
					$Ypos += 4;
				} else {
					$CSVbuf .= AddCSVRow();
					$CSVbuf .= AddCSVcolumn("Movimento");
					$CSVbuf .= AddCSVcolumn("Tipo");
					$CSVbuf .= AddCSVcolumn("Data Movimento");			
					$CSVbuf .= AddCSVcolumn("Peso stimato");			
					$CSVbuf .= AddCSVcolumn("Peso a Destino");			
					$CSVbuf .= AddCSVcolumn("Variazione %");			
					$CSVbuf .= AddCSVcolumn("Scarto ammesso");	
					$CSVbuf .= AddCSVRow();
					$CSVbuf .= AddCSVcolumn("");			
					$CSVbuf .= AddCSVcolumn("");		
					$CSVbuf .= AddCSVcolumn("");
					$CSVbuf .= AddCSVcolumn($dati["UnMis"]);
					$CSVbuf .= AddCSVcolumn($dati["UnMis"]);
					$CSVbuf .= AddCSVcolumn("Kg.");
				}
				//}}}	
			}
				# dati movimenti	
		//{{{ 
				if($dati["TIPO"]=="S") {
					$scarico = $dati["quantita"];
				} else {
					$scarico = "-";
				}
				if(!is_null($dati["PS_DESTINO"])) {
					$pesoDestino = $dati["PS_DESTINO"];
				} else {
					$pesoDestino = "-";
				}

				if(!is_null($dati["PS_DESTINO"]) && $dati["TIPO"]=="S") {
					
					switch($dati["UnMis"]){
						case 'Kg.':
							$QS	= $dati["quantita"];
							break;
						case 'Litri':
							$QS = $dati["quantita"]*$dati["peso_spec"];
							break;
						case 'Mc.':
							$QS = $dati["quantita"]*$dati["peso_spec"]*1000;
							break;
						}

					$percDes = @round($dati["PS_DESTINO"]*100/$QS,2);

					//$percDes = @round($dati["PS_DESTINO"]*100/$dati["quantita"],2);
					$percDesAbs= $percDes - 100 . "%";
					$counterVar++;
					$TotVar+=abs(100-$percDes);
					}
				else{
					$percDesAbs = "-";			
					}

				if(!is_null($dati["NFORM"])) {
					$formulario = $dati["NFORM"]; 	
				} else {
					$formulario = " - ";
				}
				if(!$asCSV) {
					CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
					$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
					$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(20,4,$dati["NMOV"],0,"C");
					$FEDIT->FGE_PdfBuffer->SetXY(35,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(10,4,$dati["TIPO"],0,"C");
					$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(20,4,date("d/m/Y",strtotime($dati["DTMOV"])),0,"C");
					$FEDIT->FGE_PdfBuffer->SetXY(60,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(23,4,number_format_unlimited_precision($scarico),0,"R");
					$FEDIT->FGE_PdfBuffer->SetXY(85,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(23,4,$pesoDestino,0,"R");
					$FEDIT->FGE_PdfBuffer->SetXY(108,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(18,4,round($percDesAbs, 2),0,"R");
					$FEDIT->FGE_PdfBuffer->SetXY(130,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(17,4,"� ".$dati["MaxVar"]."%",0,"R");
					$FEDIT->FGE_PdfBuffer->SetXY(147,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(25,4,$formulario,0,"C");
					$FEDIT->FGE_PdfBuffer->SetXY(172,$Ypos);
					if($dati["adr"]=='1') $adr="s�"; else $adr="no";
					$FEDIT->FGE_PdfBuffer->MultiCell(10,4,$adr,0,"C");
					$Ypos += 4;
				} else {
					$CSVbuf .= AddCSVRow();
					$CSVbuf .= AddCSVcolumn($dati["NMOV"]);
					$CSVbuf .= AddCSVcolumn($dati["TIPO"]);
					$CSVbuf .= AddCSVcolumn(date("d/m/Y",strtotime($dati["DTMOV"])));			
					$CSVbuf .= AddCSVcolumn(number_format_unlimited_precision($scarico));			
					$CSVbuf .= AddCSVcolumn($pesoDestino);			
					$CSVbuf .= AddCSVcolumn(round($percDesAbs, 2));
					$CSVbuf .= AddCSVcolumn("� ".$dati["MaxVar"]."%");
					$CSVbuf .= AddCSVcolumn($formulario);				
				}//}}}
			$counter++;
		}
	}
}
?>
