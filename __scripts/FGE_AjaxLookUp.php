<?php
session_start();
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeForgEdit.php");
#
#	Field Label
#
$TName = $_GET["table"];
$FName = $_GET["field"];
$TabIndex = $_GET["Tidx"];
$LkTable = $_GET["LkTable"];
$OnChange = $_GET["OnCng"];
$Detailed = $_GET["Detail"];
$ForceFirst = $_GET["ForceFirst"]; 
$Disabled = $_GET["Disabl"];

$FGE_FName = $TName . ":" . $FName; 
$FGE_Label = $FEDIT->FGE_TableInfo[$TName][$FName]["FGE_Label"];
$FGE_Mandatory = $FEDIT->FGE_TableInfo[$TName][$FName]["FGE_Mandatory"];
#
$OutputBuffer =  utf8_encode($FEDIT->FGE_MFLabel($FGE_FName,$FGE_Label,$FGE_Mandatory,$Disabled));
#
$OutputBuffer .= $FEDIT->FGE_LookUpMakeCombo($TName,$FName,$TabIndex,$LkTable,$OnChange,$Detailed,$ForceFirst,$Disabled);

echo $OutputBuffer;
require_once("../__includes/COMMON_sleepForgEdit.php");
?>