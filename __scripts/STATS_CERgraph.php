<?php

	$Ypos +=10;
	require("STATS_palette.php");
	$ToScreen = array();
	$ColPointer = 0;
	$RIFbuf = "";
	$QNTmin = 0;
	$QNTmax = 0;
	$DTmin = "";
	$DTmax = "";
	foreach($FEDIT->DbRecordSet as $k=>$dati) {
		if($dati["ID_RIF"]!=$RIFbuf) {
			$RIFbuf = $dati["ID_RIF"];
			$ToScreen[$dati["ID_RIF"]] = array(
				"CER" => $dati["COD_CER"],
				"DES" => $dati["descrizione"],
				"COL" => $palette[$ColPointer],
				"SCAR"=>array()
			);
		}
		#
		#	AGGIUNGE MOVIMENTI
		#
		$ToScreen[$dati["ID_RIF"]]["SCAR"][] = array(
			"DT"=>$dati["DTMOV"],
			"NUM"=>$dati["NMOV"],
			"Q"=>$dati["pesoN"]
			);
		#
		# 	PUNTO MINIMO E MASSIMO ASSE X (DATA)
		#
		if($DTmin=="") {
			$DTmin = date("m/d/Y",strtotime($dati["DTMOV"]));
		} else {
			#echo $dati["DTMOV"] . " min date span: " . dateDiff("",$DTmin,date("m/d/Y",strtotime($dati["DTMOV"]))) . "<br>"; 
			if(dateDiff("",$DTmin,date("m/d/Y",strtotime($dati["DTMOV"])))<0) {
				$DTmin = date("m/d/Y",strtotime($dati["DTMOV"]));
				#echo "decreased min " . $DTmin . " " . $dati["NMOV"] . "<br>";
			}
		}
		if($DTmax=="") {
			$DTmax = date("m/d/Y",strtotime($dati["DTMOV"]));
		} else {
			#echo $dati["DTMOV"] . " max date span :" . dateDiff("",$DTmax,date("m/d/Y",strtotime($dati["DTMOV"]))) . "<br>";
			if(dateDiff("",$DTmax,date("m/d/Y",strtotime($dati["DTMOV"])))>=1) {
				$DTmax = date("m/d/Y",strtotime($dati["DTMOV"]));	
				#echo "increased max " . $DTmax . " " . $dati["NMOV"] . "<br>";
			}			
		}
		$DaySpan = dateDiff("d",$DTmin,$DTmax);
		$DayYAxisInterval = $DaySpan/20;
		#
		#	PUNTO MASSIMO ASSE Y (QUANTIT�)
		#
		if($dati["pesoN"]>$QNTmax) {
			$QNTmax = $dati["pesoN"]; 	
		}
		$ColPointer++;		
		if($ColPointer==count($palette)) {
			$ColPointer = 0;
		}
	}
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	#
	#	ASSE Y (quantita)
	#
	$Ystart = $Ypos;
	$Yend = $Ypos+130;
	$FEDIT->FGE_PdfBuffer->Line(25,$Ystart,25,$Yend);
	$FEDIT->FGE_PdfBuffer->SetXY(3,$Ystart-10);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,3,"Scarico\nKg.",0,"C");
	#
	#	ASSE X (tempo)
	#
	$Xstart = 25;
	$Xend = $Xstart+250;
	$FEDIT->FGE_PdfBuffer->Line($Xstart,$Yend,$Xend,$Yend);
	$FEDIT->FGE_PdfBuffer->SetXY($Xend,$Yend+3);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,3,"Data",0,"C");
	#
	#	LINEE Y (quantita)
	#
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	for($x=$Yend;$x>=$Ystart;$x-=13) {
		$FEDIT->FGE_PdfBuffer->Line(23,$x,27,$x);	
	}
	#
	#	LINEE X (tempo)
	#
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	for($x=$Xstart;$x<=$Xend;$x+=12.5) {
		$FEDIT->FGE_PdfBuffer->Line($x,$Yend-2,$x,$Yend+2);
	}
	#
	#	LEGENDA X (tempo)
	#
	$counter = 0;
	for($x=$Xstart;$x<=$Xend;$x+=12.5) {
		$FEDIT->FGE_PdfBuffer->SetXY($x-8,$Yend+5);
		if($counter==0) {
			$dt = date("d/m/y",strtotime($DTmin));
		} else {
			$dt = date("d/m/y",strtotime($DTmin)+(86400*$DayYAxisInterval*($counter+1)));	
		}
		$FEDIT->FGE_PdfBuffer->MultiCell(15,3,$dt,0,"C");
		$counter ++;
	}
	#
	#	LEGENDA Y (quantita) 
	#
	$counter = 0;
	for($x=$Yend;$x>=$Ystart;$x-=13) {
		$FEDIT->FGE_PdfBuffer->SetXY(7,$x-1.5);
		if($counter>0) {
			$qnt = round((($QNTmax/10)*$counter),2);
		} else {
			$qnt = 0;	
		}
		$FEDIT->FGE_PdfBuffer->MultiCell(15,3,$qnt,0,"R");
		$counter++;
	}
	#
	#	DRAWS CER
	#
	
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	
	foreach($ToScreen as $idRIF=>$dati) {
		$counter = 0;
		$LastX = 0;
		$LastY = 0;
		foreach($dati["SCAR"] as $key=>$scarico) {
			if($scarico["Q"]==0) {
				$QntYPos = $Yend;	
			} else {
				$yShift = round(($scarico["Q"] * 130 / $QNTmax),2); # 130 � la dimensione in mm dell'asse y
				$QntYPos = $Yend-$yShift;
			}
			
			$xShift = datediff("",$DTmin,date("m/d/Y",strtotime($scarico["DT"])));
			if($xShift==0) {
				$QntXPos = $Xstart;	
			} else {
				$QntXPos = $Xstart + round(($xShift*250 / $DaySpan),2);
			}
			DrawMV($counter,$QntXPos,$QntYPos,$FEDIT->FGE_PdfBuffer,$scarico["DT"],$scarico["Q"],$scarico["NUM"],$LastX,$LastY,$dati["COL"]);
			$counter++;
		}
	}
	#
	#	DETTAGLIO MOVIMENTI
	#
	$FEDIT->FGE_PdfBuffer->addpage();
	$FEDIT->FGE_PdfBuffer->SetXY(0,10);
	$Ypos = 30;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',14);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,15,"GRAFICO ANDAMENTO CER - DETTAGLIO",0,"C"); 
	foreach($ToScreen as $idRIF=>$dati) {
		$Ypos +=10;
		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFillColor($dati["COL"][0],$dati["COL"][1],$dati["COL"][2]);
		$FEDIT->FGE_PdfBuffer->MultiCell(4,4,"",0,"C",1);
		$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,5,$dati["CER"] . " - " . $dati["DES"],0,"L"); 
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
		foreach($dati["SCAR"] as $key=>$scarico) {
			$Ypos += 5;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetXY(30,$Ypos);		
			$FEDIT->FGE_PdfBuffer->SetDrawColor(0,0,0);
			$CellLen = strlen($scarico["NUM"])*4;
			$FEDIT->FGE_PdfBuffer->MultiCell($CellLen,4,$scarico["NUM"],0,"C",1);
			$FEDIT->FGE_PdfBuffer->SetXY(30+$CellLen,$Ypos);
			$stringa = "Movimento n� " . $scarico["NUM"] . " del " . date("d/m/Y",strtotime($scarico["DT"])) . " - "  . $scarico["Q"] . " Kg.";
			$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$stringa,0,"L");
		}
	}
	
	function DrawMV($c,$X,$Y,$PDFobj,$data,$Q,$NMOV,&$LastX,&$LastY,$colori) {
		$PDFobj->SetXY($X-1,$Y-1);
		$PDFobj->SetFillColor($colori[0],$colori[1],$colori[2]);
		$PDFobj->SetDrawColor($colori[0],$colori[1],$colori[2]);
		$CellLen = strlen($NMOV)*4;
		$PDFobj->MultiCell($CellLen,4,$NMOV,0,"C",1);
		if($c==0) {
			$LastX = $X;
			$LastY = $Y;
		} else {
			$PDFobj->Line($LastX,$LastY,$X,$Y);
			$LastX = $X;
			$LastY = $Y;
		}	
	}
?>
