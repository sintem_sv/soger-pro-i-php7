<?php

session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/SQLFunct.php");
global $SOGER;

if(isset($_GET["table"]) && isset($_GET["filter"]) && isset($_GET["table"])) {
	if(!isset($_GET["hash"])) {
		print_r($_GET);
		die();
		$DevWarn = "missing hash";
		$SOGER->SetFeedback($DevWarn,"3");
		require_once("../__includes/COMMON_sleepSoger.php");
		header("location:../");
	} else {
		if(MakeUrlHash($_GET["table"],$_GET["pri"],$_GET["filter"])!=$_GET["hash"]) {
			$DevWarn = "hash mismatch";
			$SOGER->SetFeedback($DevWarn,"3");
			require_once("../__includes/COMMON_sleepSoger.php");
			header("location:../");
		}
	}
}

switch($_GET["FGE_action"]) {
	#
	#	MODIFICA
	#
	case "edit":
		switch ($_GET["table"]) {
//{{{
			case "core_intestatari":
				$SOGER->AppLocation="AdminNuovoIntestatario";
			break;
			case "core_gruppi":
				$SOGER->AppLocation="AdminNuovoGruppo";
			break;
			case "core_impianti":
				$SOGER->AppLocation="AdminNuovoImpianto";
			break;

			####
			case "core_users":
				if($SOGER->UserData['core_userssresponsabile']==1){
					$SOGER->AppLocation="RespNuovoUtente";
					}
				else{
					if($_GET['UserIsResponsabile']==1)
						$SOGER->AppLocation="AdminNuovoResponsabile";
					else
						$SOGER->AppLocation="AdminNuovoUtente";
					}
			break;
			####

			case "lov_num_onu":
			if($SOGER->UserData["core_usersO1"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica delle anagrafiche [cod. O1]","1");
				} else {
				$SOGER->AppLocation="UserEditOnu";
			}
			break;
			case "lov_comuni_istat":
			if($SOGER->UserData["core_usersO1"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica delle anagrafiche [cod. O1]","1");
				} else {
				$SOGER->AppLocation="UserEditComune";
			}
			break;
			case "user_aziende_produttori":
			if($SOGER->UserData["core_usersO1"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica delle anagrafiche [cod. O1]","1");
				} else {
				$SOGER->AppLocation="UserNuovoProduttore";
			}
			break;
			case "user_aziende_destinatari":
			if($SOGER->UserData["core_usersO1"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica delle anagrafiche [cod. O1]","1");
				} else {
				$SOGER->AppLocation="UserNuovoDestinatario";
			}
			break;
			case "user_aziende_intermediari":
			if($SOGER->UserData["core_usersO1"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica delle anagrafiche [cod. O1]","1");
				} else {
				$SOGER->AppLocation="UserNuovoIntermediario";
			}
			break;
			case "user_aziende_trasportatori":
			if($SOGER->UserData["core_usersO1"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica delle anagrafiche [cod. O1]","1");
				} else {
				$SOGER->AppLocation="UserNuovoTrasportatore";
			}
			break;
			case "user_schede_rifiuti":
				if($SOGER->UserData["core_usersO1"]=="0") {
					$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica delle anagrafiche [cod. O1]","1");
					}
				else {
					$SOGER->AppLocation="UserNuovoRifiuto2";
					}
			break;
			case "user_schede_rifiuti_etsym":
					$SOGER->AppLocation="UserRifiutoSegnaletica";
			break;
			case "user_schede_rifiuti_pittogrammi":
					$SOGER->AppLocation="UserRifiutoPittogrammi";
			break;
			case "user_schede_rifiuti_deposito":
					$SOGER->AppLocation="UserRifiutoContenitore";
			break;
			case "user_schede_sicurezza":
			if($SOGER->UserData["core_usersO1"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica delle anagrafiche [cod. O1]","1");
				} else {
				$SOGER->AppLocation="UserNuovaSchedaSic";
			}
			break;
			case "user_contratti_budget":
			if($SOGER->UserData["core_usersG3"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere alla gestione economica [cod. G3]","1");
				} else {
				$SOGER->AppLocation="UserNuovoBudgetRif";
			}
			break;

			case "user_contratti_destinatari":
			if($SOGER->UserData["core_usersG3"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere alla gestione economica [cod. G3]","1");
				} else {
				$SOGER->AppLocation="UserNuovoContrattoDestinatario";
			}
			break;
			case "user_contratti_dest_cond":
			if($SOGER->UserData["core_usersG3"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere alla gestione economica [cod. G3]","1");
				} else {
				$SOGER->AppLocation="UserCondizioneContrattoDestinatario";
			}
			break;

			case "user_contratti_produttori":
			if($SOGER->UserData["core_usersG3"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere alla gestione economica [cod. G3]","1");
				} else {
				$SOGER->AppLocation="UserNuovoContrattoProduttore";
			}
			break;
			case "user_contratti_pro_cond":
			if($SOGER->UserData["core_usersG3"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere alla gestione economica [cod. G3]","1");
				} else {
				$SOGER->AppLocation="UserCondizioneContrattoProduttore";
			}
			break;

			case "user_contratti_trasportari":
			if($SOGER->UserData["core_usersG3"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere alla gestione economica [cod. G3]","1");
				} else {
				$SOGER->AppLocation="UserNuovoContrattoTrasportatore";
			}
			break;
			case "user_contratti_trasp_cond":
			if($SOGER->UserData["core_usersG3"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere alla gestione economica [cod. G3]","1");
				} else {
				$SOGER->AppLocation="UserCondizioneContrattoTrasportatore";
			}
			break;

			case "user_contratti_intermediari":
			if($SOGER->UserData["core_usersG3"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere alla gestione economica [cod. G3]","1");
				} else {
				$SOGER->AppLocation="UserNuovoContrattoIntermediario";
			}
			break;
			case "user_contratti_int_cond":
			if($SOGER->UserData["core_usersG3"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere alla gestione economica [cod. G3]","1");
				} else {
				$SOGER->AppLocation="UserCondizioneContrattoIntermediario";
			}
			break;


			case "user_movimenti":

				$ScaricoBusy = "Il carico � correlato ad uno scarico: il rifiuto non pu� essere modificato";

				if(ScaricoInvolved()) {
					$AdditionalPar = "&Involved";
					$SOGER->SetFeedback($ScaricoBusy,"3");
					}

				if($SOGER->UserData["core_usersO5"]=="0") {
					$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica dei movimenti [cod. O5]","1");
					}
				else {
					$FEDIT->FGE_FLushTableInfo();
					$FEDIT->FGE_UseTables(array("user_movimenti"));
					$FEDIT->FGE_SetSelectFields(array("TIPO","TIPO_S_SCHEDA","ID_MOV"),"user_movimenti");
					$FEDIT->FGE_SetFilter("ID_MOV",$_GET["filter"],"user_movimenti");
					$FEDIT->SDbRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
					switch($FEDIT->DbRecordSet[0]["user_movimentiTIPO"]){
						case "C":
							$SOGER->AppLocation="UserNuovoMovCarico";
							break;
						case "S":
							if($FEDIT->DbRecordSet[0]["user_movimentiTIPO_S_SCHEDA"]==1 AND $FEDIT->DbRecordSet[0]["user_movimentiNMOV"]==9999999)
								$SOGER->AppLocation		= "UserNuovaSchedaSistri";
							else{
								$SOGER->AppBackStatus	= $SOGER->AppLocation;
								$SOGER->AppLocation="UserNuovoMovScarico";
								}
							break;
						}
				}
			break;


			case "user_movimenti_fiscalizzati":
			if($SOGER->UserData["core_usersO5"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica dei movimenti [cod. O5]","1");
				} else {
				$FEDIT->FGE_FLushTableInfo();
				$FEDIT->FGE_UseTables(array("user_movimenti_fiscalizzati"));
				$FEDIT->FGE_SetSelectFields(array("TIPO","TIPO_S_SCHEDA","TIPO_S_INTERNO","ID_MOV_F"),"user_movimenti_fiscalizzati");
				$FEDIT->FGE_SetFilter("ID_MOV_F",$_GET["filter"],"user_movimenti_fiscalizzati");
				$FEDIT->SDbRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
				switch($FEDIT->DbRecordSet[0]["user_movimenti_fiscalizzatiTIPO"]){
					case "C":
						$SOGER->AppLocation="UserNuovoMovCaricoF";
						break;
					case "S":
						// per lo scarico devo sapere se tornare sul reg. crono o sull'elenco schede/fir
						// print_r($SOGER->AppLocation);die();
						if($FEDIT->DbRecordSet[0]["user_movimenti_fiscalizzatiTIPO_S_INTERNO"]==1){
							$SOGER->AppBackStatus	= 'UserVediMovimentiF';
							$SOGER->AppLocation		= 'UserNuovoMovScaricoInternoF';
							}
						else{
							if($FEDIT->DbRecordSet[0]["user_movimenti_fiscalizzatiTIPO_S_SCHEDA"]==1 AND $FEDIT->DbRecordSet[0]["user_movimenti_fiscalizzatiNMOV"]==9999999){
                                                            $SOGER->AppBackStatus	= $SOGER->AppLocation;
                                                            $SOGER->AppLocation     = 'UserNuovaSchedaSistriF';
                                                            }
							else{
                                                            if($SOGER->AppLocation=='UserVediMovimentiIVcopie'){
                                                                $SOGER->AppLocation='UserNuovoMovScaricoFiscale';
                                                                $SOGER->AppBackStatus	= $SOGER->AppLocation;
                                                            }
                                                            if($SOGER->AppLocation=='UserVediMovimentiF')
                                                                $SOGER->AppLocation='UserNuovoMovScaricoF';
                                                            if($SOGER->AppLocation=='UserVediMovimentiFiscali')
                                                                $SOGER->AppLocation='UserNuovoMovScaricoFiscale';
                                                            }
							}
						break;
					}
			}
			break;
		}
		$MovimentoBusy = "Il record � impiegato in un movimento: alcuni campi non possono essere modificati";
		if(MovimentoInvolved()) {
			$AdditionalPar = "&Involved";
			$SOGER->SetFeedback($MovimentoBusy,"3");
			}

		$url = $_SERVER["QUERY_STRING"];
		if(isset($AdditionalPar)) {
			$url .= $AdditionalPar;
		}
	break;
	case "delete":
	#
	#	CANCELLAZIONE
	#
//{{{
		$url = "";
		$LogSql  = "INSERT INTO core_logs ";
                $LogSql .= "(`table` , `pri_key` , `ID_USR` , `ID_IMP` , `sqlType` , `sqlCommand` ) ";
                $LogSql .= "VALUES ";
		$LogSql .= "('" . $_GET["table"] . "','" . $_GET["filter"]  . "','";
		$LogSql .= $SOGER->UserData["core_usersID_USR"] . "','";
		$LogSql .= $SOGER->UserData["core_usersID_IMP"] . "','cancellazione','--')";
		$MovimentoBusy = "Il record � impiegato in un movimento: impossibile cancellarlo";

		switch ($_GET["table"]) {

			case "user_movimenti_fiscalizzati":
				if($SOGER->UserData["core_usersO5"]=="0") {
					$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica dei movimenti [cod. O5]","1");
					}
				else {

					$ID_MOV_F=$_GET['filter'];

					// delete da tabella costi
					if($_SESSION['econ_ly']){
						$sql = "DELETE FROM user_movimenti_costi WHERE ID_MOV_F='" . $ID_MOV_F . "'";
						$FEDIT->SDBWrite($sql,true,false);
						}

					$sql="SELECT ID_RIF, quantita, TIPO, NMOV FROM user_movimenti_fiscalizzati WHERE ID_MOV_F='".$ID_MOV_F."'";
					$FEDIT->SDBRead($sql,"DbRecordSet", true, false);
					$sql="SELECT FKErifCarico FROM user_movimenti_fiscalizzati WHERE ID_RIF='".$FEDIT->DbRecordSet[0]['ID_RIF']."' AND TIPO='S' AND ( FKErifCarico='".$FEDIT->DbRecordSet[0]['NMOV']."' OR FKErifCarico LIKE '%,".$FEDIT->DbRecordSet[0]['NMOV'].",%' OR FKErifCarico LIKE '".$FEDIT->DbRecordSet[0]['NMOV'].",%' OR FKErifCarico LIKE ',".$FEDIT->DbRecordSet[0]['NMOV']."')";
					$FEDIT->SDBRead($sql,"DbRecordSetScaricato", true, false);
					if(isset($FEDIT->DbRecordSetScaricato)) $Scaricato=true; else $Scaricato=false;

					if(!$Scaricato){
						// aggiorno disponibilit�
						$ID_RIF=$FEDIT->DbRecordSet[0]['ID_RIF'];
						$NMOV=$FEDIT->DbRecordSet[0]['NMOV'];

						if($FEDIT->DbRecordSet[0]['TIPO']=="C"){
							$SQL = "UPDATE user_schede_rifiuti SET FKEdisponibilita= IF (FKEdisponibilita - ".$FEDIT->DbRecordSet[0]['quantita'].">0, FKEdisponibilita - ".$FEDIT->DbRecordSet[0]['quantita'].", 0) WHERE ID_RIF='".$ID_RIF."'";

							$FEDIT->SDBWrite($SQL,true,false);
							}
						if($FEDIT->DbRecordSet[0]['TIPO']=="S"){
							if(is_null($FEDIT->DbRecordSet[0]['quantita']))
								$Quantita=0;
							else
								$Quantita=$FEDIT->DbRecordSet[0]['quantita'];
							$SQL="UPDATE user_schede_rifiuti SET FKEdisponibilita=FKEdisponibilita + ".$Quantita." WHERE ID_RIF='".$ID_RIF."'";
							$FEDIT->SDBWrite($SQL,true,false);
							}

						// delete da tabella movimenti
						$FEDIT->FGE_SQL_MakeDelete($_GET["table"],$_GET["pri"],$_GET["filter"],$LogSql);

						// aggiorno numerazione movimenti su registro fiscale
						$sql ="UPDATE user_movimenti_fiscalizzati SET NMOV=NMOV-1 WHERE ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."' AND NMOV>".$NMOV." AND NMOV<>9999999 ";
						include("../__includes/SOGER_DirectProfilo.php");
						$FEDIT->SDBWrite($sql,true,false);

						// aggiorno riferimenti movimenti di carico
						$TableName		= "user_movimenti_fiscalizzati";
						$LegamiSISTRI	= false;
						//require("MovimentiRifMovCarico.php");

						// aggiorno le medie
						require("../__libs/SogerAUTOC_funct.php");
						$GiorniLavorati		=	giornilavorativi(date('Y')."-01-01", date('Y-m-d'));
						$GiorniLavorativi	=	5;

						## DERIVO QUANTITA' SCARICO MEDIO
						$sql = "SELECT AVG(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS media FROM user_movimenti_fiscalizzati WHERE ID_RIF='".$ID_RIF."' AND TIPO='S'";
						$FEDIT->SDBRead($sql,"DbRecordset",true,false);
						$MEDIA_S=number_format($FEDIT->DbRecordset[0]['media'], 2, ".", "");

						## DERIVO PRODUZIONE MEDIA GIORNALIERA E SETTIMANALE - scarichi
						$sql = "SELECT SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS scaricato FROM user_movimenti_fiscalizzati WHERE ID_RIF='".$ID_RIF."' AND TIPO='S'";
						$FEDIT->SDBRead($sql,"DbRecordset",true,false);
						$PRODOTTO=$FEDIT->DbRecordset[0]['scaricato'];
						$MediaCggRound  = number_format($PRODOTTO / $GiorniLavorati, 2, ".", "");
						$MediaSettRound = number_format($PRODOTTO / $GiorniLavorati * $GiorniLavorativi, 2, ".", "");

						## AGGIORNO MEDIE EFFETTIVE
						$SQL ="UPDATE user_schede_rifiuti_deposito SET media_gg_eff='".$MediaCggRound."', media_settimanale_eff='".$MediaSettRound."', scarico_medio='".$MEDIA_S."' WHERE ID_RIF='".$ID_RIF."'";
						//die($SQL);
						$FEDIT->SDBWrite($SQL,true,false);
					}
				else{
					if($Scaricato)
						$SOGER->SetFeedback($MovimentoBusy,"1");
					}


				}
			break;


			case "user_movimenti":
			if($SOGER->UserData["core_usersO5"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica dei movimenti [cod. O5]","1");
				}
			else {

				$sql="SELECT ID_RIF, quantita, TIPO, NMOV FROM user_movimenti WHERE ID_MOV='".$_GET['filter']."'";
				$FEDIT->SDBRead($sql,"DbRecordSet", true, false);
				$sql="SELECT FKErifCarico FROM user_movimenti WHERE ID_RIF='".$FEDIT->DbRecordSet[0]['ID_RIF']."' AND TIPO='S' AND ( FKErifCarico='".$FEDIT->DbRecordSet[0]['NMOV']."' OR FKErifCarico LIKE '%,".$FEDIT->DbRecordSet[0]['NMOV'].",%' OR FKErifCarico LIKE '".$FEDIT->DbRecordSet[0]['NMOV'].",%' OR FKErifCarico LIKE ',".$FEDIT->DbRecordSet[0]['NMOV']."')";
				$FEDIT->SDBRead($sql,"DbRecordSetScaricato", true, false);
				if(isset($FEDIT->DbRecordSetScaricato)) $Scaricato=true; else $Scaricato=false;

				if(!$Scaricato){
					// aggiorno disponibilit�
					$ID_RIF=$FEDIT->DbRecordSet[0]['ID_RIF'];
					$NMOV=$FEDIT->DbRecordSet[0]['NMOV'];

					if($FEDIT->DbRecordSet[0]['TIPO']=="C"){
						$SQL = "UPDATE user_schede_rifiuti SET FKEdisponibilita= IF (FKEdisponibilita - ".$FEDIT->DbRecordSet[0]['quantita'].">0, FKEdisponibilita - ".$FEDIT->DbRecordSet[0]['quantita'].", 0) WHERE ID_RIF='".$ID_RIF."'";
						$FEDIT->SDBWrite($SQL,true,false);
						}
					if($FEDIT->DbRecordSet[0]['TIPO']=="S"){
						if(is_null($FEDIT->DbRecordSet[0]['quantita']))
							$Quantita=0;
						else
							$Quantita=$FEDIT->DbRecordSet[0]['quantita'];
						$SQL="UPDATE user_schede_rifiuti SET FKEdisponibilita=FKEdisponibilita + ".$Quantita." WHERE ID_RIF='".$ID_RIF."'";
						$FEDIT->SDBWrite($SQL,true,false);
						}

					// delete da tabella movimenti
					$FEDIT->FGE_SQL_MakeDelete($_GET["table"],$_GET["pri"],$_GET["filter"],$LogSql);

					// aggiorno numerazione movimenti su registro industriale
					$sql ="UPDATE user_movimenti SET NMOV=NMOV-1 WHERE ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."' AND NMOV>".$NMOV." AND NMOV<>9999999 ";
					include("../__includes/SOGER_DirectProfilo.php");
					$FEDIT->SDBWrite($sql,true,false);

					// aggiorno le medie
					require("../__libs/SogerAUTOC_funct.php");
					$GiorniLavorati		=	giornilavorativi(date('Y')."-01-01", date('Y-m-d'));
					$GiorniLavorativi	=	5;
				}
			else{
				if($Scaricato)
					$SOGER->SetFeedback($MovimentoBusy,"1");
				}


			}
			break;
			case "user_schede_rifiuti":
				if($SOGER->UserData["core_usersO1"]=="0") {
					$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica delle anagrafiche [cod. O1]","1");
					} else {
					if(!MovimentoInvolved()) {
						$FEDIT->FGE_SQL_MakeDelete($_GET["table"],$_GET["pri"],$_GET["filter"],$LogSql);
					} else {
						$SOGER->SetFeedback($MovimentoBusy,"1");
					}
				}
			break;
			case "user_aziende_produttori":
				if($SOGER->UserData["core_usersO1"]=="0") {
					$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica delle anagrafiche [cod. O1]","1");
					} else {
					if(!MovimentoInvolved()) {
						$FEDIT->FGE_SQL_MakeDelete($_GET["table"],$_GET["pri"],$_GET["filter"],$LogSql);
						require("../__includes/USER_SchedeProduttoriFlushSessionVars.php");
					} else {
						$SOGER->SetFeedback($MovimentoBusy,"1");
					}
				}
			break;
			case "user_aziende_destinatari":
				if($SOGER->UserData["core_usersO1"]=="0") {
					$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica delle anagrafiche [cod. O1]","1");
					} else {
						if(!MovimentoInvolved()) {
						$FEDIT->FGE_SQL_MakeDelete($_GET["table"],$_GET["pri"],$_GET["filter"],$LogSql);
						require("../__includes/USER_SchedeDestinatariFlushSessionVars.php");
						} else {
							$SOGER->SetFeedback($MovimentoBusy,"1");
						}
				}
			break;
			case "user_aziende_intermediari":
				if($SOGER->UserData["core_usersO1"]=="0") {
					$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica delle anagrafiche [cod. O1]","1");
					} else {
					if(!MovimentoInvolved()) {
						$FEDIT->FGE_SQL_MakeDelete($_GET["table"],$_GET["pri"],$_GET["filter"],$LogSql);
						require("../__includes/USER_SchedeIntermediariFlushSessionVars.php");
					} else {
						$SOGER->SetFeedback($MovimentoBusy,"1");
					}
				}
			break;
			case "user_aziende_trasportatori":
			if($SOGER->UserData["core_usersO1"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica delle anagrafiche [cod. O1]","1");
				} else {
					if(!MovimentoInvolved()) {
						$FEDIT->FGE_SQL_MakeDelete($_GET["table"],$_GET["pri"],$_GET["filter"],$LogSql);
						require("../__includes/USER_SchedeTrasportatoriFlushSessionVars.php");
					} else {
						$SOGER->SetFeedback($MovimentoBusy,"1");
					}
			}
			break;

			case "user_autorizzazioni_dest":
                        case "user_autorizzazioni_pro":
			case "user_autorizzazioni_trasp":
			case "user_autorizzazioni_interm":
				if(isset($_GET['inherit'])){
					$sql="SELECT num_aut FROM ".$_GET['table']." WHERE ".$_GET['pri']."=".$_GET['filter'].";";
					$FEDIT->SDbRead($sql,"DbRecordSet",true,false);
					$num_aut=$FEDIT->DbRecordSet[0]['num_aut'];
					## qui devo dire che cancello solo i record dello stesso impianto!!!!!!!!! ##
					switch($_GET["table"]){
						case "user_autorizzazioni_dest":
							$PrimaryKeyImp="ID_UIMD";
							$TableImp="user_impianti_destinatari";
							$PrimaryKeyAz="ID_AZD";
							$TableAz="user_aziende_destinatari";
							break;
                                                case "user_autorizzazioni_pro":
							$PrimaryKeyImp="ID_UIMP";
							$TableImp="user_impianti_produttori";
							$PrimaryKeyAz="ID_AZP";
							$TableAz="user_aziende_produttori";
							break;
						case "user_autorizzazioni_trasp":
							$PrimaryKeyImp="ID_UIMT";
							$TableImp="user_impianti_trasportatori";
							$PrimaryKeyAz="ID_AZT";
							$TableAz="user_aziende_trasportatori";
							break;
						case "user_autorizzazioni_interm":
							$PrimaryKeyImp="ID_UIMI";
							$TableImp="user_impianti_intermediari";
							$PrimaryKeyAz="ID_AZI";
							$TableAz="user_aziende_intermediari";
							break;
						}
					$sql ="SELECT ".$_GET['pri']." FROM ".$_GET['table']." WHERE num_aut='".$num_aut."' ";
					$sql.="AND ".$PrimaryKeyImp." IN ( ";
					$sql.="SELECT ".$PrimaryKeyImp." FROM ".$TableImp." WHERE ".$PrimaryKeyAz." IN ( ";
					$sql.="SELECT ".$PrimaryKeyAz." FROM ".$TableAz." WHERE ID_IMP = '".$SOGER->UserData["core_impiantiID_IMP"]."' ";
					$sql.="AND ".$SOGER->UserData['workmode']."='1' ))";

					$FEDIT->SDbRead($sql,"DbRecordSetAUT",true,false);
					$Involved=false;
					for($a=0;$a<count($FEDIT->DbRecordSetAUT);$a++){
						if(!MovimentoInvolved($_GET['pri'], $FEDIT->DbRecordSetAUT[$a][$_GET['pri']], $_GET['table'])){
							$FEDIT->FGE_SQL_MakeDelete($_GET["table"],$_GET["pri"],$FEDIT->DbRecordSetAUT[$a][$_GET['pri']],$LogSql);
							}
						else{
							$Involved=true;
							}
						}
					if($Involved)
						$SOGER->SetFeedback("Tutti i record ".$num_aut." non impiegati nella movimentazione sono stati eliminati","3");
					else
						$SOGER->SetFeedback("Il record ".$num_aut." � stato eliminato" ,"1");
					}
				else{
					if(!MovimentoInvolved()) {
						$FEDIT->FGE_SQL_MakeDelete($_GET["table"],$_GET["pri"],$_GET["filter"],$LogSql);
						switch ($_GET["table"]) {
                                                        case "user_autorizzazioni_pro":
								require("../__includes/USER_SchedeProduttoriIDcheck.php");
								$FEDIT->SDbRead($sql,"DbRecordSet",true,false);
								$url  = "table=".$_GET["table"]."&pri=".$_GET["pri"]."&filter=".$FEDIT->DbRecordSet[0][$_GET["pri"]];
								$url .= "&hash=" . MakeUrlHash($_GET["table"],$_GET["pri"],$FEDIT->DbRecordSet[0][$_GET["pri"]]);
								break;
							case "user_autorizzazioni_interm":
								require("../__includes/USER_SchedeIntermediariIDcheck.php");
								$FEDIT->SDbRead($sql,"DbRecordSet",true,false);
								$url  = "table=".$_GET["table"]."&pri=".$_GET["pri"]."&filter=".$FEDIT->DbRecordSet[0][$_GET["pri"]];
								$url .= "&hash=" . MakeUrlHash($_GET["table"],$_GET["pri"],$FEDIT->DbRecordSet[0][$_GET["pri"]]);
								break;
							case "user_autorizzazioni_dest":
								require("../__includes/USER_SchedeDestinatariIDcheck.php");
								$FEDIT->SDbRead($sql,"DbRecordSet",true,false);
								$url  = "table=".$_GET["table"]."&pri=".$_GET["pri"]."&filter=".$FEDIT->DbRecordSet[0][$_GET["pri"]];
								$url .= "&hash=" . MakeUrlHash($_GET["table"],$_GET["pri"],$FEDIT->DbRecordSet[0][$_GET["pri"]]);
								break;
							case "user_autorizzazioni_trasp":
								$url = "__scripts/status.php?area=UserNuovoTrasportatoreAutorizzazioni";
								break;
							case "user_automezzi":
								$url = "__scripts/status.php?area=UserNuovoTrasportatoreAutomezzi";
								break;
							case "user_rimorchi":
								//require("../__includes/USER_SchedeTrasportatoriIDcheck.php");
								$url = "__scripts/status.php?area=UserNuovoTrasportatoreRimorchi";
								break;
							}
						}
					else {
						$SOGER->SetFeedback($MovimentoBusy,"1");
						}
					}
				break;

			case "user_impianti_produttori":
				if(!MovimentoInvolved()) {
					$FEDIT->FGE_SQL_MakeDelete($_GET["table"],$_GET["pri"],$_GET["filter"],$LogSql);
					require("../__includes/USER_SchedeProduttoriIDcheck.php");
					$FEDIT->SDbRead($sql,"DbRecordSet",true,false);
					if($_GET["table"]=="user_impianti_produttori" && !is_null($FEDIT->DbRecordSet[0]["ID_UIMP"])) {
						$url="table=" . $_GET["table"] . "&pri=" . $_GET["pri"] . "&filter=" . $FEDIT->DbRecordSet[0]["ID_UIMP"];
						$url .= "&hash=" . MakeUrlHash($_GET["table"],$_GET["pri"],$FEDIT->DbRecordSet[0]["ID_UIMP"]);
						}
					}
				else {
					$SOGER->SetFeedback($MovimentoBusy,"1");
					}
			break;

			case "user_impianti_destinatari":
				if(!MovimentoInvolved()) {
					$FEDIT->FGE_SQL_MakeDelete($_GET["table"],$_GET["pri"],$_GET["filter"],$LogSql);
					require("../__includes/USER_SchedeDestinatariIDcheck.php");
					$FEDIT->SDbRead($sql,"DbRecordSet",true,false);
					if($_GET["table"]=="user_impianti_destinatari" && !is_null($FEDIT->DbRecordSet[0]["ID_UIMD"])) {
						$url="table=" . $_GET["table"] . "&pri=" . $_GET["pri"] . "&filter=" . $FEDIT->DbRecordSet[0]["ID_UIMD"];
						$url .= "&hash=" . MakeUrlHash($_GET["table"],$_GET["pri"],$FEDIT->DbRecordSet[0]["ID_UIMD"]);
					}
					if($_GET["table"]=="user_autorizzazioni_dest" && !is_null($FEDIT->DbRecordSet[0]["ID_AUTHD"])) {
						$url="table=" . $_GET["table"] . "&pri=" . $_GET["pri"] . "&filter=" . $FEDIT->DbRecordSet[0]["ID_AUTHD"];
						$url .= "&hash=" . MakeUrlHash($_GET["table"],$_GET["pri"],$FEDIT->DbRecordSet[0]["ID_AUTHD"]);
					}
				} else {
					$SOGER->SetFeedback($MovimentoBusy,"1");
				}
			break;


			case "user_automezzi":
			case "user_rimorchi":
				if(isset($_GET['inherit'])){

					$sql="SELECT description FROM ".$_GET['table']." WHERE ".$_GET['pri']."=".$_GET['filter'].";";
					$FEDIT->SDbRead($sql,"DbRecordSet",true,false);
					$targa=$FEDIT->DbRecordSet[0]['description'];

					$sql ="SELECT ".$_GET['pri']." FROM ".$_GET['table']." WHERE description='".$targa."' ";
					$sql.="AND ID_AUTHT IN ( ";
					$sql.="SELECT ID_AUTHT FROM user_autorizzazioni_trasp WHERE ID_UIMT IN ( ";
					$sql.="SELECT ID_UIMT FROM user_impianti_trasportatori WHERE ID_AZT IN ( ";
					$sql.="SELECT ID_AZT FROM user_aziende_trasportatori WHERE ID_IMP = '".$SOGER->UserData["core_impiantiID_IMP"]."' ";
					$sql.="AND ".$SOGER->UserData['workmode']."='1' )))";
					$FEDIT->SDbRead($sql,"DbRecordSetAUT",true,false);

					$Involved=false;
					for($a=0;$a<count($FEDIT->DbRecordSetAUT);$a++){
						if(!MovimentoInvolved($_GET['pri'], $FEDIT->DbRecordSetAUT[$a][$_GET['pri']], $_GET['table'])){
							$FEDIT->FGE_SQL_MakeDelete($_GET["table"],$_GET["pri"],$FEDIT->DbRecordSetAUT[$a][$_GET['pri']],$LogSql);
							}
						else{
							$Involved=true;
							}
						}
					if($Involved)
						$SOGER->SetFeedback("Tutti i record ".$targa." non impiegati nella movimentazione sono stati eliminati","3");
					else
						$SOGER->SetFeedback("Il record ".$targa." � stato eliminato" ,"1");
					}
				else{
					if(!MovimentoInvolved()) {
						$FEDIT->FGE_SQL_MakeDelete($_GET["table"],$_GET["pri"],$_GET["filter"],$LogSql);
						//require("../__includes/USER_SchedeTrasportatoriIDcheck.php");
						//$FEDIT->SDbRead($sql,"DbRecordSet",true,false);
						if($_GET['table']=='user_automezzi')
							$url = "__scripts/status.php?area=UserNuovoTrasportatoreAutomezzii";
						if($_GET['table']=='user_rimorchi')
							$url = "__scripts/status.php?area=UserNuovoTrasportatoreRimorchi";
						//$url  = "table=".$_GET["table"]."&pri=".$_GET["pri"]."&filter=".$FEDIT->DbRecordSet[0][$_GET["pri"]];
						//$url .= "&hash=" . MakeUrlHash($_GET["table"],$_GET["pri"],$FEDIT->DbRecordSet[0][$_GET["pri"]]);
						}
					else {
						$SOGER->SetFeedback($MovimentoBusy,"1");
						}
					}
				break;





			case "user_impianti_trasportatori":
			case "user_automezzi":
			case "user_rimorchi":
				if(!MovimentoInvolved()) {
					$FEDIT->FGE_SQL_MakeDelete($_GET["table"],$_GET["pri"],$_GET["filter"],$LogSql);
					//require("../__includes/USER_SchedeTrasportatoriIDcheck.php");
					$FEDIT->SDbRead($sql,"DbRecordSet",true,false);
					if($_GET["table"]=="user_impianti_trasportatori" && !is_null($FEDIT->DbRecordSet[0]["ID_UIMT"])) {
						$url = "__scripts/status.php?area=UserNuovoTrasportatoreImpianto";
						//$url="table=" . $_GET["table"] . "&pri=" . $_GET["pri"] . "&filter=" . $FEDIT->DbRecordSet[0]["ID_UIMT"];
						//$url .= "&hash=" . MakeUrlHash($_GET["table"],$_GET["pri"],$FEDIT->DbRecordSet[0]["ID_UIMT"]);
					}
					if($_GET["table"]=="user_autorizzazioni_trasp" && !is_null($FEDIT->DbRecordSet[0]["ID_AUTHT"])) {
						$url = "__scripts/status.php?area=UserNuovoTrasportatoreAutorizzazioni";
						//$url="table=" . $_GET["table"] . "&pri=" . $_GET["pri"] . "&filter=" . $FEDIT->DbRecordSet[0]["ID_AUTHT"];
						//$url .= "&hash=" . MakeUrlHash($_GET["table"],$_GET["pri"],$FEDIT->DbRecordSet[0]["ID_AUTHT"]);
					}

				} else {
					$SOGER->SetFeedback($MovimentoBusy,"1");
				}
			break;
			case "user_autisti":
			if(!MovimentoInvolved()) {
				$FEDIT->FGE_SQL_MakeDelete($_GET["table"],$_GET["pri"],$_GET["filter"],$LogSql);
				//require("../__includes/USER_SchedeTrasportatoriIDcheck.php");
				//$FEDIT->SDbRead($sql,"DbRecordSet",true,false);
				//if(!is_null($FEDIT->DbRecordSet[0]["ID_AUTST"])) {
					//$url="table=" . $_GET["table"] . "&pri=" . $_GET["pri"] . "&filter=" . $FEDIT->DbRecordSet[0]["ID_AUTST"];
					//$url .= "&hash=" . MakeUrlHash($_GET["table"],$_GET["pri"],$FEDIT->DbRecordSet[0]["ID_AUTST"]);
				//}
			} else {
				$SOGER->SetFeedback($MovimentoBusy,"1");
			}
			break;

			case "user_movimenti_costi":
				$sqlRedirection="SELECT ID_MOV_F FROM user_movimenti_costi WHERE ".$_GET['pri']."='".$_GET['filter']."'";
				$FEDIT->SDbRead($sqlRedirection,"DbRecordSet",true,false);
				$ID_MOV_F=$FEDIT->DbRecordSet[0]['ID_MOV_F'];
				$FEDIT->FGE_SQL_MakeDelete($_GET["table"],$_GET["pri"],$_GET["filter"],$LogSql);
				$url="table=user_movimenti_fiscalizzati&pri=ID_MOV_F&filter=" . $ID_MOV_F;
				$url .= "&hash=" . MakeUrlHash("user_movimenti_fiscalizzati","ID_MOV_F",$ID_MOV_F);
				break;

			case "core_users":

				// Elimino i suoi 'cron'
				$SQL="DELETE FROM user_promemoria WHERE ID_USR=".$_GET["filter"].";";
				$FEDIT->SDBWrite($SQL,true,false);

				// Elimino i suoi files e cartelle
				$sqlID_IMP="SELECT ID_IMP FROM core_users WHERE ID_USR=".$_GET["filter"].";";
				$FEDIT->SDbRead($sqlID_IMP,"DbRecordSetID_IMP",true,false);
				$ID_IMP=$FEDIT->DbRecordSetID_IMP[0]['ID_IMP'];
				$ID_USR=str_pad((string)$_GET["filter"], 7, "0", STR_PAD_LEFT);
				delTree('../__documents/__adr/'.$ID_IMP.'/'.$ID_USR);
				delTree('../__documents/__alarm/'.$ID_IMP.'/'.$ID_USR);
				delTree('../__documents/__reg/'.$ID_IMP.'/'.$ID_USR);

				// Elimino l'utente
				$FEDIT->FGE_SQL_MakeDelete($_GET["table"],$_GET["pri"],$_GET["filter"],$LogSql);

				break;

			default:
			$FEDIT->FGE_SQL_MakeDelete($_GET["table"],$_GET["pri"],$_GET["filter"],$LogSql);
			break;
		} //}}}
	break;

	case "cloneSchedaRifiuto":
		#
		#	DUPLICAZIONE CUSTOM PER SCHEDA RIFIUTO
		#
		$action ='duplicazione';
		if($_GET["riclassificazione"]==1) $action.="R";
		if($_GET["fornitori"]==1) $action.="F";
		$LogSql  = "INSERT INTO core_logs ";
                $LogSql .= "(`table` , `pri_key` , `ID_USR` , `ID_IMP` , `sqlType` , `sqlCommand` ) ";
                $LogSql .= "VALUES ";
		$LogSql .= "('" . $_GET["table"] . "','" . $_GET["filter"]  . "','";
		$LogSql .= $SOGER->UserData["core_usersID_USR"] . "','";
		$LogSql .= $SOGER->UserData["core_usersID_IMP"] . "','".$action."','--')";
		if($SOGER->UserData["core_usersO1"]=="0") {
			$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica delle anagrafiche [cod. O1]","1");
			}
		else {
			$FEDIT->FGE_CloneRecordRifiuto($_GET["table"],$_GET["filter"],$_GET["fornitori"],$_GET["riclassificazione"], $LogSql);
			$url = "table=" . $_GET["table"] . "&pri=" . $_GET["pri"] . "&filter=" . $_GET["filter"];
			$url .= "&hash=" . MakeUrlHash($_GET["table"],$_GET["pri"],$_GET["filter"]);
			$SOGER->SetFeedback("La scheda rifiuto � stata duplicata correttamente","2");

			}

		break;
/*
	case "cloneMatch":
		#
		#	DUPLICAZIONE e ASSOCIAZIONE FORNITORI
		#
		$LogSql  = "INSERT INTO core_logs ";
                $LogSql .= "(`table` , `pri_key` , `ID_USR` , `ID_IMP` , `sqlType` , `sqlCommand` ) ";
                $LogSql .= "VALUES ";
		$LogSql .= "('" . $_GET["table"] . "','" . $_GET["filter"]  . "','";
		$LogSql .= $SOGER->UserData["core_usersID_USR"] . "','";
		$LogSql .= $SOGER->UserData["core_usersID_IMP"] . "','duplicazione+','--')";
		if($SOGER->UserData["core_usersO1"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica delle anagrafiche [cod. O1]","1");
			} else {
				$FEDIT->FGE_CloneRecordRifiutoAndMatch($_GET["table"],$_GET["filter"],$LogSql);
				//$url = $_SERVER["QUERY_STRING"];
				$url = "table=" . $_GET["table"] . "&pri=" . $_GET["pri"] . "&filter=" . $_GET["filter"];
				$url .= "&hash=" . MakeUrlHash($_GET["table"],$_GET["pri"],$_GET["filter"]);
			}
		break;
*/
	case "clone":
	#
	#	DUPLICAZIONE
	#
	$LogSql  = "INSERT INTO core_logs ";
        $LogSql .= "(`table` , `pri_key` , `ID_USR` , `ID_IMP` , `sqlType` , `sqlCommand` ) ";
        $LogSql .= "VALUES ";
	$LogSql .= "('" . $_GET["table"] . "','" . $_GET["filter"]  . "','";
	$LogSql .= $SOGER->UserData["core_usersID_USR"] . "','";
	$LogSql .= $SOGER->UserData["core_usersID_IMP"] . "','duplicazione','--')";
	switch ($_GET["table"]) {

		case "user_aziende_intermediari":
		case "user_aziende_produttori":
		case "user_aziende_trasportatori":
		case "user_aziende_destinatari":
			if($SOGER->UserData["core_usersO1"]=="0") {
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica delle anagrafiche [cod. O1]","1");
			} else {
				$FEDIT->FGE_CloneRecord($_GET["table"],$_GET["filter"],$LogSql);
				//$url = $_SERVER["QUERY_STRING"];
				$url = "table=" . $_GET["table"] . "&pri=" . $_GET["pri"] . "&filter=" . $_GET["filter"];
				$url .= "&hash=" . MakeUrlHash($_GET["table"],$_GET["pri"],$_GET["filter"]);
			}
		break;
//		case "user_schede_rifiuti":
//			if($SOGER->UserData["core_usersO1"]=="0") {
//				$SOGER->SetFeedback("L'utente non ha i permessi necessari per la modifica delle anagrafiche [cod. O1]","1");
//			} else {
//				$FEDIT->FGE_CloneRecordRifiuto($_GET["table"],$_GET["filter"],$LogSql);
//				$url = "table=" . $_GET["table"] . "&pri=" . $_GET["pri"] . "&filter=" . $_GET["filter"];
//				$url .= "&hash=" . MakeUrlHash($_GET["table"],$_GET["pri"],$_GET["filter"]);
//			}
//		break;
		default:
			$FEDIT->FGE_CloneRecord($_GET["table"],$_GET["filter"],$LogSql);
			$url = "table=" . $_GET["table"] . "&pri=" . $_GET["pri"] . "&filter=" . $FEDIT->DbLastID;
			$url .= "&hash=" . MakeUrlHash($_GET["table"],$_GET["pri"],$FEDIT->DbLastID);
			//$url = "table=" . $_GET["table"] . "&pri=" . $_GET["pri"] . "&filter=" . $_GET["filter"];
			//$url .= "&hash=" . MakeUrlHash($_GET["table"],$_GET["pri"],$_GET["filter"]);
			#$url = $_SERVER["QUERY_STRING"];
		break;
	}
	break;

	#
	case "approve":
		if($SOGER->UserData["core_usersG1"]=="0") {
			$SOGER->SetFeedback("L'utente non ha i permessi necessari per l'approvazione di fornitori e schede rifiuto [cod. G1]","1");
			}
		else {
			$LogSql  = "INSERT INTO core_logs ";
                        $LogSql .= "(`table` , `pri_key` , `ID_USR` , `ID_IMP` , `sqlType` , `sqlCommand` ) ";
                        $LogSql .= "VALUES ";
			$LogSql .= "('" . $_GET['table'] . "','" . $_GET['filter'] . "','";
			$LogSql .= $SOGER->UserData["core_usersID_USR"] . "','";
			$LogSql .= $SOGER->UserData["core_usersID_IMP"] . "','approvazione','--')";
			$FEDIT->FGE_ApproveRecord($_GET["table"],$_GET['pri'],$_GET["filter"],$LogSql);
			$SOGER->SetFeedback("Il record � stato approvato","2");
			}
		break;

	case "disapprove":
		if($SOGER->UserData["core_usersG1"]=="0") {
			$SOGER->SetFeedback("L'utente non ha i permessi necessari per l'approvazione di fornitori e schede rifiuto [cod. G1]","1");
			}
		else {
			$LogSql  = "INSERT INTO core_logs ";
                        $LogSql .= "(`table` , `pri_key` , `ID_USR` , `ID_IMP` , `sqlType` , `sqlCommand` ) ";
                        $LogSql .= "VALUES ";
			$LogSql .= "('" . $_GET['table'] . "','" . $_GET['filter'] . "','";
			$LogSql .= $SOGER->UserData["core_usersID_USR"] . "','";
			$LogSql .= $SOGER->UserData["core_usersID_IMP"] . "','disapprovazione','--')";
			$FEDIT->FGE_DisapproveRecord($_GET["table"],$_GET['pri'],$_GET["filter"],$LogSql);
			$SOGER->SetFeedback("Il record non � stato approvato","1");
			}
		break;

	case "approveISO":
		$LogSql  = "INSERT INTO core_logs ";
                $LogSql .= "(`table` , `pri_key` , `ID_USR` , `ID_IMP` , `sqlType` , `sqlCommand` ) ";
                $LogSql .= "VALUES ";
		$LogSql .= "('" . $_GET['table'] . "','" . $_GET['filter'] . "','";
		$LogSql .= $SOGER->UserData["core_usersID_USR"] . "','";
		$LogSql .= $SOGER->UserData["core_usersID_IMP"] . "','approvazione iso','--')";
		$FEDIT->FGE_ApproveRecordISO($_GET["table"],$_GET['pri'],$_GET["filter"],$LogSql,$_GET["Field"]);
		$SOGER->SetFeedback("Il record � stato approvato","2");
		break;

	case "disapproveISO":
		$LogSql  = "INSERT INTO core_logs ";
                $LogSql .= "(`table` , `pri_key` , `ID_USR` , `ID_IMP` , `sqlType` , `sqlCommand` ) ";
                $LogSql .= "VALUES ";
		$LogSql .= "('" . $_GET['table'] . "','" . $_GET['filter'] . "','";
		$LogSql .= $SOGER->UserData["core_usersID_USR"] . "','";
		$LogSql .= $SOGER->UserData["core_usersID_IMP"] . "','disapprovazione iso','--')";
		$FEDIT->FGE_DisapproveRecordISO($_GET["table"],$_GET['pri'],$_GET["filter"],$LogSql,$_GET["Field"]);
		$SOGER->SetFeedback("Il record non � stato approvato","1");
		break;

	case "setRosette":
		$LogSql="";
		//$LogSql  = "INSERT INTO core_logs ";
                //$LogSql .= "(`table` , `pri_key` , `ID_USR` , `ID_IMP` , `sqlType` , `sqlCommand` ) ";
                //$LogSql .= "VALUES ";
		//$LogSql .= "('" . $_GET['table'] . "', '" . $_GET['filter'] . "','";
		//$LogSql .= $SOGER->UserData["core_usersID_USR"] . "','";
		//$LogSql .= $SOGER->UserData["core_usersID_IMP"] . "','SetRosette".$_GET['RosetteID']."','update')";
		$FEDIT->FGE_SetRosette($_GET["table"],$_GET['description'],$_GET['RosetteID'],$_GET['ID_UIMT'],$LogSql);
		$SOGER->SetFeedback("La coccarda � stata associata alle targhe","2");
		break;

	case "setID_LIM":
		$LogSql="";
		//$LogSql  = "INSERT INTO core_logs ";
                //$LogSql .= "(`table` , `pri_key` , `ID_USR` , `ID_IMP` , `sqlType` , `sqlCommand` ) ";
                //$LogSql .= "VALUES ";
		//$LogSql .= "('" . $_GET['table'] . "', '" . $_GET['filter'] . "','";
		//$LogSql .= $SOGER->UserData["core_usersID_USR"] . "','";
		//$LogSql .= $SOGER->UserData["core_usersID_IMP"] . "','SetRosette".$_GET['RosetteID']."','update')";
		$FEDIT->FGE_SetID_LIM($_GET["table"],$_GET['description'],$_GET['ID_LIM'],$_GET['ID_UIMT'],$LogSql);
		$SOGER->SetFeedback("Le limitazioni al trasporto sono state estese alle targhe","2");
		break;
	#

	case "print":
	#
	#	STAMPA
	#
	require_once("../__libs/fpdf.php");
	$FEDIT->FGE_FlushTableInfo();
	#	dimensioni pagina, orientamento ecc
//{{{
		switch ($_GET["table"]) {
			case "numerazione_vidimazione":
					# numerazione pagine registro a scopo vidimazione
					$orientation="P";
					$um="mm";
					$Format = array(210,297);
					$ZeroMargin = true;
					$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin);
					$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
				break;
			case "user_movimenti":
				if(!isset($_GET["Registro"])) {
					# formulario
					$orientation="P";
					$um="mm";
					$Format = array(210,304);
					$ZeroMargin = true;
					$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin);

					// FONT TYPE
					$SQL_FONT="SELECT FPDF_fontName from lov_font_fir WHERE FRM_FONT_ID=".$SOGER->UserData['core_usersFRM_FONT_ID'];
					$FEDIT->SdbRead($SQL_FONT, "DbRecordSetFont");
					$FontType=$FEDIT->DbRecordSetFont[0]['FPDF_fontName'];

					// FONT SIZE
					$SQL_FONT_SIZE="SELECT punti FROM lov_font_fir_size WHERE FRM_FONT_SIZE_ID=".$SOGER->UserData['core_usersFRM_FONT_SIZE_ID'];
					$FEDIT->SdbRead($SQL_FONT_SIZE, "DbRecordSetFontSize");
					$FontSize=$FEDIT->DbRecordSetFontSize[0]['punti'];

					// SET FONT
					$FEDIT->FGE_PdfBuffer->SetFont($FontType,'',$FontSize);
					}
				else {
					if(isset($_GET["cartaBianca"])) {
						# registro (carta bianca)
						$orientation="P";
						$um="mm";
						$Format = array(210,297);
						$ZeroMargin = true;
						$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin);
						$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
						}
					else {
						if(!isset($_GET['Industriale'])){
							# registro (fiscale)
							$orientation="P";
							$um="mm";
							//$Format = array(210,297);
							$Format = array(210,304);
							$ZeroMargin = true;
							$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin);
							$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
							}
						else{
							# registro (industriale)
							$orientation="L";
							$um="mm";
							$Format = array(210,297);
							$ZeroMargin = true;
							if(isset($_GET['Cronologico']))
								$DcName = "Stampa_Registro_Industriale_Cronologico";
							if(isset($_GET['Settimanale']))
								$DcName = "Stampa_Registro_Industriale_Settimanale";
							$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin,$DcName);
							$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
							}
						}
					}

				break;

			case "user_movimenti_fiscalizzati":
				if(!isset($_GET["Registro"])) {
					# formulario
					$orientation="P";
					$um="mm";
					$Format = array(210,304);
					$ZeroMargin = true;
					$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin);

					// FONT TYPE
					$SQL_FONT="SELECT FPDF_fontName from lov_font_fir WHERE FRM_FONT_ID=".$SOGER->UserData['core_usersFRM_FONT_ID'];
					$FEDIT->SdbRead($SQL_FONT, "DbRecordSetFont");
					$FontType=$FEDIT->DbRecordSetFont[0]['FPDF_fontName'];

					// FONT SIZE
					$SQL_FONT_SIZE="SELECT punti FROM lov_font_fir_size WHERE FRM_FONT_SIZE_ID=".$SOGER->UserData['core_usersFRM_FONT_SIZE_ID'];
					$FEDIT->SdbRead($SQL_FONT_SIZE, "DbRecordSetFontSize");
					$FontSize=$FEDIT->DbRecordSetFontSize[0]['punti'];

					// SET FONT
					$FEDIT->FGE_PdfBuffer->SetFont($FontType,'',$FontSize);
					}
				else {
					if(isset($_GET["cartaBianca"])) {
						# registro (carta bianca)
						$orientation="P";
						$um="mm";
						$Format = array(210,297);
						$ZeroMargin = true;
						$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin);
						$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
						}
					else {
						if(!isset($_GET['Industriale'])){
							# registro (fiscale) e registro unico p+d
							$orientation="P";
							$um="mm";
							//$Format = array(210,297);
							$Format = array(210,304);
							$ZeroMargin = true;
							$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin);
							$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
							}
						else{
							# registro (industriale)
							$orientation="L";
							$um="mm";
							$Format = array(210,297);
							$ZeroMargin = true;
							$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin);
							$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
							}
						}
					}
			break;
			default:
				$orientation="L";
				$um="mm";
				$Format = array(210,297);
				$ZeroMargin = true;


				# SET DcName FOR FOOTER TO PRINT ISO PROC.
				switch($_GET["table"]){
					case "numerazione_vidimazione":
                                                $orientation="P";
						$DcName = "Numerazione_Pagine_Registro";
						break;
					case "user_schede_rifiuti":
                                                $orientation="P";
						$DcName = "Stampa_Rifiuti";
						break;
					case "user_aziende_produttori":
						$DcName = "Stampa_Anagrafica_Produttore";
						break;
					case "user_aziende_destinatari":
						$DcName = "Stampa_Anagrafica_Destinatario";
						break;
					case "user_aziende_trasportatori":
						$DcName = "Stampa_Anagrafica_Trasportatore";
						break;
					case "user_aziende_intermediari":
						$DcName = "Stampa_Anagrafica_Intermediario";
						break;
					}

				$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin,$DcName);
				$FEDIT->FGE_PdfBuffer->SetFont('Arial','',7);
			break;
		} //}}}

	$deniedByConfig = false;
	switch ($_GET["table"]) {
//{{{
			case "numerazione_vidimazione": #crea la numerazione per far vidimare il registro in camera di commercio#
				$DcName = "Numerazione_Pagine_Registro";
				$Xcorr =  0;
				$Ycorr =  0;

				$PageNr = $_GET["PaginaIniz"];

				if ($_GET["PaginaIniz"]=="1")
					$TotalPages = $_GET["PaginaTotale"];
				else
					$TotalPages = $PageNr + $_GET["PaginaTotale"] -1;

				//print_r($SOGER->UserData);

				$BufAnno = date("Y");
				for($c=$PageNr;$c<=$TotalPages;$c++) {
					if($PageNr<=$TotalPages) {

						$FEDIT->FGE_PdfBuffer->SetXY(10+$Xcorr,5+$Ycorr);
						$Label = "Registro di carico e scarico rifiuti";
						if(!is_null($_GET['CodiceRegistro']) AND trim($_GET['CodiceRegistro'])!=''){
							$Label.=" - ".$_GET['CodiceRegistro'];
							}
						$FEDIT->FGE_PdfBuffer->MultiCell(80,5,$Label,0,"L");

						if(isset($_GET['RagSoc'])){
							$FEDIT->FGE_PdfBuffer->SetXY(10+$Xcorr,271+$Ycorr);
							$FEDIT->FGE_PdfBuffer->MultiCell(130,5,"Societ�: ".$SOGER->UserData['core_impiantidescription']." - C.F. : ".$SOGER->UserData["core_impianticodfisc"],0,"L");
							}
						$FEDIT->FGE_PdfBuffer->SetXY(165+$Xcorr,271+$Ycorr);
						if(trim($_GET['AnnoRegistro'])=='')
							$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Pagina ".$PageNr,0,"L");
						else
							$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Pagina ".$PageNr." / ".$_GET['AnnoRegistro'],0,"L");
						if($PageNr<$TotalPages)
							$FEDIT->FGE_PdfBuffer->AddPage();
						$PageNr++;
					}
				}
				break;
	//}}}

			case "user_schede_rifiuti":
//{{{
			if($SOGER->UserData["core_usersO2"]=="0") {
				$deniedByConfig = true;
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per la stampa delle anagrafiche [cod. O2]","1");
			} else {
				$DcName = "Stampa_Rifiuti";
				$FEDIT->FGE_UseTables("user_schede_rifiuti");
				$FEDIT->FGE_SetFormFields(array("ID_IMP","ID_CER","pericoloso","descrizione","cod_pro","mud"),"user_schede_rifiuti");
				$FEDIT->FGE_SetFormFields(array("ID_SF","peso_spec","ID_UMIS","caratteristiche"),"user_schede_rifiuti");
				//$FEDIT->FGE_SetFormFields(array("pericoloso","H1","H2","H3A","H3B","H4","H5","H6","H7","H8","H9","H10","H11","H12","H13","H14","H15"),"user_schede_rifiuti");
				$FEDIT->FGE_SetFormFields(array("pericoloso","HP1","HP2","HP3","HP4","HP5","HP6","HP7","HP8","HP9","HP10","HP11","HP12","HP13","HP14","HP15"),"user_schede_rifiuti");
				$FEDIT->FGE_SetFormFields(array("adr", "ID_ONU", "ONU_per"),"user_schede_rifiuti");
				$FEDIT->FGE_SetFormFields(array("et_comp_per"),"user_schede_rifiuti");
				$FEDIT->FGE_DescribeFields();
				#
				$FEDIT->FGE_HideFields(array("ID_IMP"),"user_schede_rifiuti");
				#
				$FEDIT->FGE_SetTitle("ID_CER","Caratteristiche Generali","user_schede_rifiuti");
				$FEDIT->FGE_SetTitle("ID_SF","Natura - Propriet�","user_schede_rifiuti");
				//$FEDIT->FGE_SetTitle("H1","Caratteristiche di Pericolosit�","user_schede_rifiuti");
				$FEDIT->FGE_SetTitle("HP1","Caratteristiche di Pericolosit� - Reg. UE 1357/2014","user_schede_rifiuti");
				$FEDIT->FGE_SetTitle("adr","ADR","user_schede_rifiuti");
				$FEDIT->FGE_SetTitle("et_comp_per","Etichetta di pericolo","user_schede_rifiuti");
				#
				$FEDIT->FGE_LookUpDefault("ID_UMIS","user_schede_rifiuti");
				$FEDIT->FGE_LookUpCFG("ID_ONU","user_schede_rifiuti");
				$FEDIT->FGE_UseTables("lov_num_onu");
				$FEDIT->FGE_SetSelectFields(array("ID_ONU","description", "des", "classe", "imb", "eti", "codice_galleria"),"lov_num_onu");
				$FEDIT->FGE_LookUpDescribe();
				$FEDIT->FGE_LookUpDone();

				$FEDIT->FGE_LookUpDefault("ID_SF","user_schede_rifiuti");
				$FEDIT->FGE_LookUpDefault("C_S_ID","user_schede_rifiuti");
				#
				$FEDIT->FGE_LookUpCFG("ID_CER","user_schede_rifiuti");
				$FEDIT->FGE_UseTables("lov_cer");
				$FEDIT->FGE_SetSelectFields(array("ID_CER","COD_CER"),"lov_cer");
				$FEDIT->FGE_LookUpDescribe();
				$FEDIT->FGE_LookUpDone();
				$FEDIT->FGE_MakeForm("STAMPA SCHEDE RIFIUTI PRODOTTI","");
			}//}}}
			break;
			case "user_aziende_produttori":
//{{{
			if($SOGER->UserData["core_usersO2"]=="0") {
				$deniedByConfig = true;
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per la stampa delle anagrafiche [cod. O2]","1");
			} else {
				$DcName = "Stampa_Anagrafica_Produttore";
				$FEDIT->FGE_UseTables("user_aziende_produttori");
				$FEDIT->FGE_SetFormFields(array("ID_IMP","description","indirizzo","ID_COM","piva","codfisc"),"user_aziende_produttori");
				$FEDIT->FGE_SetTitle("description","Intestatario","user_aziende_produttori");
				$FEDIT->FGE_DescribeFields();
				$FEDIT->FGE_LookUpDefault("ID_COM","user_aziende_produttori");
				$FEDIT->FGE_HideFields(array("ID_IMP"),"user_aziende_produttori");
				$FEDIT->FGE_MakeForm("STAMPA ANAGRAFICA AZIENDE - PRODUTTORE","");
				#
				$FEDIT->FGE_FlushTableInfo();
				$FEDIT->FGE_UseTables(array("user_impianti_produttori"));
				$FEDIT->FGE_SetSelectFields(array("ID_AZP","description","ID_COM","website","referente","telefono","cellulare","fax","email"),"user_impianti_produttori");
				$FEDIT->FGE_DescribeFields();
				$FEDIT->FGE_LookUpDefault("ID_COM","user_impianti_produttori");
				$FEDIT->FGE_SetFilter("ID_AZP",$_GET["filter"],"user_impianti_produttori");
				$FEDIT->FGE_HideFields(array("ID_AZP"),"user_impianti_produttori");
				$FEDIT->FGE_DataGrid("Impianti");
				#
				$FEDIT->FGE_FlushTableInfo();
				$FEDIT->FGE_UseTables(array("user_autorizzazioni_pro","user_impianti_produttori","user_aziende_produttori"));
				$FEDIT->FGE_SetSelectFields(array("ID_AZP","description"),"user_impianti_produttori");
                                $FEDIT->FGE_SetSelectFields(array("ID_RIF","ID_AUT","num_aut","rilascio","scadenza"),"user_autorizzazioni_pro");
				$FEDIT->FGE_DescribeFields();
                                $FEDIT->FGE_LookUpCFG("ID_RIF","user_autorizzazioni_pro");
				$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
				$FEDIT->FGE_SetSelectFields(array("descrizione","ID_CER","ID_IMP"),"user_schede_rifiuti");
				$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
				$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP"),"user_schede_rifiuti");
				$FEDIT->FGE_LookUpDescribe();
				$FEDIT->FGE_LookUpDone();
				$FEDIT->FGE_LookUpDefault("ID_AUT","user_autorizzazioni_pro");
				$FEDIT->FGE_SetFilter("ID_AZP",$_GET["filter"],"user_impianti_produttori");
				$FEDIT->FGE_HideFields(array("ID_AZP"),"user_impianti_produttori");
                                $FEDIT->FGE_DataGrid("Autorizzazioni");
			} //}}}
			break;
			case "user_aziende_destinatari":
//{{{
			if($SOGER->UserData["core_usersO2"]=="0") {
				$deniedByConfig = true;
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per la stampa delle anagrafiche [cod. O2]","1");
			} else {
				$DcName = "Stampa_Anagrafica_Destinatario";
				$FEDIT->FGE_UseTables("user_aziende_destinatari");
				$FEDIT->FGE_SetFormFields(array("ID_AZD","description","indirizzo"),"user_aziende_destinatari");
				$FEDIT->FGE_SetFormFields(array("codfisc","piva","esenzione_contributo","contributo"),"user_aziende_destinatari");
				$FEDIT->FGE_SetTitle("description","Intestatario","user_aziende_destinatari");
				$FEDIT->FGE_DescribeFields();
				$FEDIT->FGE_LookUpDefault("ID_COM","user_aziende_destinatari");
				$FEDIT->FGE_HideFields(array("ID_IMP"),"user_aziende_destinatari");
				$FEDIT->FGE_MakeForm("STAMPA ANAGRAFICA AZIENDE - DESTINATARIO","");
				#
				$FEDIT->FGE_FlushTableInfo();
				$FEDIT->FGE_UseTables(array("user_impianti_destinatari"));
                                $FEDIT->FGE_SetSelectFields(array("ID_AZD","description","ID_COM","website","referente","telefono","cellulare","fax","email"),"user_impianti_destinatari");
				$FEDIT->FGE_DescribeFields();
				$FEDIT->FGE_LookUpDefault("ID_COM","user_impianti_destinatari");
				$FEDIT->FGE_SetFilter("ID_AZD",$_GET["filter"],"user_impianti_destinatari");
				$FEDIT->FGE_HideFields(array("ID_AZD"),"user_impianti_destinatari");
				$FEDIT->FGE_DataGrid("Impianti");
				#
				$FEDIT->FGE_FlushTableInfo();
				$FEDIT->FGE_UseTables(array("user_autorizzazioni_dest","user_impianti_destinatari","user_aziende_destinatari"));
				$FEDIT->FGE_SetSelectFields(array("ID_AZD","description"),"user_impianti_destinatari");
				$FEDIT->FGE_SetSelectFields(array("ID_RIF","num_aut","rilascio","scadenza","ID_OP_RS","ID_ORIGINE_DATI"),"user_autorizzazioni_dest");
				$FEDIT->FGE_DescribeFields();
				$FEDIT->FGE_LookUpDefault("ID_ORIGINE_DATI","user_autorizzazioni_dest");
				$FEDIT->FGE_LookUpDefault("ID_OP_RS","user_autorizzazioni_dest");
				$FEDIT->FGE_LookUpCFG("ID_RIF","user_autorizzazioni_dest");
				$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
				$FEDIT->FGE_SetSelectFields(array("descrizione","ID_CER"),"user_schede_rifiuti");
				$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
				$FEDIT->FGE_HideFields(array("ID_CER"),"user_schede_rifiuti");
				$FEDIT->FGE_LookUpDescribe();
				$FEDIT->FGE_LookUpDone();
				$FEDIT->FGE_SetFilter("ID_AZD",$_GET["filter"],"user_impianti_destinatari");
				$FEDIT->FGE_HideFields(array("ID_AZD"),"user_impianti_destinatari");
				$FEDIT->FGE_DataGrid("Autorizzazioni");
			}
			//}}}
			break;
			case "user_aziende_trasportatori":
//{{{
			if($SOGER->UserData["core_usersO2"]=="0") {
				$deniedByConfig = true;
				$SOGER->SetFeedback("L'utente non ha i permessi necessari per la stampa delle anagrafiche [cod. O2]","1");
			} else {
				$DcName = "Stampa_Anagrafica_Trasportatore";
				$FEDIT->FGE_UseTables(array("user_aziende_trasportatori"));
				$FEDIT->FGE_SetFormFields(array("ID_AZT","description","indirizzo","codfisc","esenzione_contributo","contributo"),"user_aziende_trasportatori");
				$FEDIT->FGE_SetTitle("description","Intestatario","user_aziende_trasportatori");
				$FEDIT->FGE_DescribeFields();
				$FEDIT->FGE_LookUpDefault("ID_COM","user_aziende_trasportatori");
				$FEDIT->FGE_HideFields(array("ID_IMP"),"user_aziende_trasportatori");
				$FEDIT->FGE_MakeForm("STAMPA ANAGRAFICA AZIENDE - TRASPORTATORE","");
				#
				$FEDIT->FGE_FlushTableInfo();
				$FEDIT->FGE_UseTables(array("user_impianti_trasportatori"));
                                $FEDIT->FGE_SetSelectFields(array("ID_AZT","description","ID_COM","website","referente","telefono","cellulare","fax","email"),"user_impianti_trasportatori");
				$FEDIT->FGE_DescribeFields();
				$FEDIT->FGE_LookUpDefault("ID_COM","user_impianti_trasportatori");
				$FEDIT->FGE_SetFilter("ID_AZT",$_GET["filter"],"user_impianti_trasportatori");
				$FEDIT->FGE_HideFields(array("ID_AZT"),"user_impianti_trasportatori");
				$FEDIT->FGE_DataGrid("Impianti");
				#
				$FEDIT->FGE_FlushTableInfo();
				$FEDIT->FGE_UseTables(array("user_autorizzazioni_trasp","user_impianti_trasportatori","user_aziende_trasportatori"));
				$FEDIT->FGE_SetSelectFields(array("ID_AZT","description"),"user_impianti_trasportatori");
				$FEDIT->FGE_SetSelectFields(array("ID_RIF","ID_AUT","num_aut","rilascio","scadenza","ID_ORIGINE_DATI"),"user_autorizzazioni_trasp");
				$FEDIT->FGE_DescribeFields();
				$FEDIT->FGE_LookUpDefault("ID_ORIGINE_DATI","user_autorizzazioni_trasp");
				$FEDIT->FGE_LookUpDefault("ID_AUT","user_autorizzazioni_trasp");
				$FEDIT->FGE_LookUpCFG("ID_RIF","user_autorizzazioni_trasp");
				$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
				$FEDIT->FGE_SetSelectFields(array("descrizione","ID_CER"),"user_schede_rifiuti");
				$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
				$FEDIT->FGE_HideFields(array("ID_CER"),"user_schede_rifiuti");
				$FEDIT->FGE_LookUpDescribe();
				$FEDIT->FGE_LookUpDone();
				$FEDIT->FGE_HideFields(array("ID_AZT"),"user_impianti_trasportatori");
				$FEDIT->FGE_SetFilter("ID_AZT",$_GET["filter"],"user_impianti_trasportatori");
				$FEDIT->FGE_DataGrid("Autorizzazioni");
				#
				$FEDIT->FGE_FlushTableInfo();
				$FEDIT->FGE_UseTables(array("user_automezzi","user_autorizzazioni_trasp","user_aziende_trasportatori","lov_cer","user_schede_rifiuti","user_impianti_trasportatori"));
				$FEDIT->FGE_SetSelectFields(array("ID_AZT"),"user_aziende_trasportatori");
				$FEDIT->FGE_SetSelectFields(array("num_aut"),"user_autorizzazioni_trasp");
				$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
				$FEDIT->FGE_SetSelectFields(array("descrizione","ID_RIF"),"user_schede_rifiuti");
				$FEDIT->FGE_SetSelectFields(array("description"),"user_impianti_trasportatori");
				$FEDIT->FGE_SetSelectFields(array("description","adr","ID_MZ_TRA","ID_ORIGINE_DATI"),"user_automezzi");
				$FEDIT->FGE_DescribeFields();
				$FEDIT->FGE_LookUpDefault("ID_ORIGINE_DATI","user_automezzi");
				$FEDIT->FGE_SetOrder("user_automezzi:description");
				$FEDIT->FGE_SetFilter("ID_AZT",$_GET["filter"],"user_aziende_trasportatori");
				$FEDIT->FGE_LookUpDefault("ID_MZ_TRA","user_automezzi");
				$FEDIT->FGE_DataGrid("Automezzi");
				#
				$FEDIT->FGE_FlushTableInfo();
				$FEDIT->FGE_UseTables(array("user_rimorchi","user_autorizzazioni_trasp","user_aziende_trasportatori","lov_cer","user_schede_rifiuti","user_impianti_trasportatori"));
				$FEDIT->FGE_SetSelectFields(array("num_aut"),"user_autorizzazioni_trasp");
				$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
				$FEDIT->FGE_SetSelectFields(array("descrizione","ID_RIF"),"user_schede_rifiuti");
				$FEDIT->FGE_SetSelectFields(array("ID_AZT"),"user_aziende_trasportatori");
				$FEDIT->FGE_SetSelectFields(array("description"),"user_impianti_trasportatori");
				$FEDIT->FGE_SetSelectFields(array("description","adr","ID_MZ_RMK","ID_ORIGINE_DATI"),"user_rimorchi");
				#
				$FEDIT->FGE_DescribeFields();
				$FEDIT->FGE_LookUpDefault("ID_ORIGINE_DATI","user_rimorchi");
				$FEDIT->FGE_SetOrder("user_rimorchi:description");
				$FEDIT->FGE_SetFilter("ID_AZT",$_GET["filter"],"user_aziende_trasportatori");
				$FEDIT->FGE_LookUpDefault("ID_MZ_RMK","user_rimorchi");
				$FEDIT->FGE_DataGrid("Rimorchi");
				#
				$FEDIT->FGE_FlushTableInfo();
				$FEDIT->FGE_UseTables("user_autisti","user_impianti_trasportatori","user_aziende_trasportatori");
				$FEDIT->FGE_SetSelectFields(array("nome","description","adr","patente","rilascio","scadenza"),"user_autisti");
				$FEDIT->FGE_SetSelectFields(array("ID_AZT","description"),"user_impianti_trasportatori");
				$FEDIT->FGE_SetSelectFields(array("ID_AZT","description"),"user_aziende_trasportatori");
				$FEDIT->FGE_SetFilter("ID_AZT",$_GET["filter"],"user_aziende_trasportatori");
				#
				$FEDIT->FGE_DescribeFields();
				$FEDIT->FGE_SetOrder("user_autisti:description");
				$FEDIT->FGE_SetFilter("ID_AZT",$_GET["filter"],"user_aziende_trasportatori");
				$FEDIT->FGE_HideFields("ID_AZT","user_impianti_trasportatori");
				$FEDIT->FGE_DataGrid("Autisti");
			}//}}}
			break;
			case "user_aziende_intermediari":
				if($SOGER->UserData["core_usersO2"]=="0") {
					$deniedByConfig = true;
					$SOGER->SetFeedback("L'utente non ha i permessi necessari per la stampa delle anagrafiche [cod. O2]","1");
					}
				else {
					$DcName = "Stampa_Anagrafica_Intermediario";
					$FEDIT->FGE_UseTables("user_aziende_intermediari");
					$FEDIT->FGE_SetFormFields(array("ID_AZI","description","indirizzo"),"user_aziende_intermediari");
					$FEDIT->FGE_SetFormFields(array("codfisc","piva","esenzione_contributo","contributo"),"user_aziende_intermediari");
					$FEDIT->FGE_SetTitle("description","Intestatario","user_aziende_intermediari");
					$FEDIT->FGE_DescribeFields();
					$FEDIT->FGE_LookUpDefault("ID_COM","user_aziende_intermediari");
					$FEDIT->FGE_HideFields(array("ID_IMP"),"user_aziende_intermediari");
					$FEDIT->FGE_MakeForm("STAMPA ANAGRAFICA AZIENDE - INTERMEDIARI","");
					#
					$FEDIT->FGE_FlushTableInfo();
					$FEDIT->FGE_UseTables(array("user_impianti_intermediari"));
                                        $FEDIT->FGE_SetSelectFields(array("ID_AZI","description","ID_COM","website","referente","telefono","cellulare","fax","email"),"user_impianti_intermediari");
					$FEDIT->FGE_DescribeFields();
					$FEDIT->FGE_LookUpDefault("ID_COM","user_impianti_intermediari");
					$FEDIT->FGE_SetFilter("ID_AZI",$_GET["filter"],"user_impianti_intermediari");
					$FEDIT->FGE_HideFields(array("ID_AZI"),"user_impianti_intermediari");
					$FEDIT->FGE_DataGrid("Impianti");
					#
					$FEDIT->FGE_FlushTableInfo();
					$FEDIT->FGE_UseTables(array("user_autorizzazioni_interm","user_impianti_intermediari","user_aziende_intermediari"));
					$FEDIT->FGE_SetSelectFields(array("ID_AZI","description"),"user_impianti_intermediari");
					$FEDIT->FGE_SetSelectFields(array("ID_RIF","num_aut","rilascio","scadenza","ID_ORIGINE_DATI"),"user_autorizzazioni_interm");
					$FEDIT->FGE_DescribeFields();
					$FEDIT->FGE_LookUpDefault("ID_ORIGINE_DATI","user_autorizzazioni_interm");
					$FEDIT->FGE_LookUpCFG("ID_RIF","user_autorizzazioni_interm");
					$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
					$FEDIT->FGE_SetSelectFields(array("descrizione","ID_CER"),"user_schede_rifiuti");
					$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
					$FEDIT->FGE_HideFields(array("ID_CER"),"user_schede_rifiuti");
					$FEDIT->FGE_LookUpDescribe();
					$FEDIT->FGE_LookUpDone();
					$FEDIT->FGE_SetFilter("ID_AZI",$_GET["filter"],"user_impianti_intermediari");
					$FEDIT->FGE_HideFields(array("ID_AZI"),"user_impianti_intermediari");
					$FEDIT->FGE_DataGrid("Autorizzazioni");
				}

				break;

			case "user_movimenti":
				if(!isset($_GET["Registro"])) {
					# stampa fomulario

					if($SOGER->UserData["core_usersD3"]=="0") {
						$deniedByConfig = true;
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per la stampa del formulario [cod. D3]","1");
						}
					else {
						$Xcorr =  $SOGER->UserData["core_usersFRM_PRNT_X"];
						if($SOGER->UserData["core_usersFRM_PRNT_Y"]>-4.8) {
							$Ycorr = $SOGER->UserData["core_usersFRM_PRNT_Y"];
						} else {
							$Ycorr = -4.8;
						}
						#
						$sql = "UPDATE user_movimenti SET FISCALE='" . $_GET["Fiscale"] . "' WHERE ID_MOV='" . $_GET["filter"] . "' AND FISCALE='0'";
						$FEDIT->SDBWrite($sql,true,false);

						$DcName = "Stampa Formulario";
						$sql = "SELECT ";
						$sql .= "user_movimenti.ID_RIF,user_movimenti.ID_COMM,user_movimenti.ID_CAR, user_movimenti.ID_AZP,user_movimenti.ID_UIMP,user_movimenti.NOTEF,user_movimenti.DTFORM";
						$sql .= ",user_movimenti.ID_AZD,user_movimenti.ID_UIMD,user_movimenti.ID_AUTHD,user_movimenti.ID_OP_RS";
						$sql .= ",user_movimenti.ID_AZT,user_movimenti.ID_UIMT,user_movimenti.ID_AUTHT,user_movimenti.ID_AZI,user_movimenti.ID_UIMI,user_movimenti.ID_AUTHI,user_movimenti.ID_AUTO";
						$sql .= ",user_movimenti.ID_RMK,user_movimenti.ID_AUTST,QL";
						$sql .= ",user_movimenti.percorso,user_movimenti.adr,user_movimenti.NPER,user_movimenti.VER_DESTINO,user_movimenti.dt_in_trasp,user_movimenti.hr_in_trasp";
						$sql .= ",user_movimenti.FKEcfiscD, user_movimenti.FKEcfiscT";
						$sql .= ",user_movimenti.quantita,user_movimenti.tara,user_movimenti.pesoL,user_movimenti.pesoN,user_movimenti.COLLI";
						$sql .= ",user_schede_rifiuti.descrizione,user_schede_rifiuti.caratteristiche,user_schede_rifiuti.ID_ONU,user_schede_rifiuti.ONU_per, user_schede_rifiuti.ADR_NOTE";
						$sql .= ",H1,H2,H3A,H3B,H4,H5,H6,H7,H8,H9,H10,H11,H12,H13,H14,H15";
						$sql .= ",user_schede_rifiuti.NOTEF AS NOTE_RIFIUTO";
						$sql .= ",lov_cer.COD_CER,lov_cer.PERICOLOSO as pericoloso, lov_cer.description AS LawCerDes, lov_stato_fisico.description AS STFdes,lov_stato_fisico.ID_SF as SFID";
						$sql .= ",lov_misure.description AS Umisura";
						$sql .= " FROM user_movimenti";
						$sql .= " JOIN user_schede_rifiuti on user_movimenti.ID_RIF=user_schede_rifiuti.ID_RIF";
						$sql .= " JOIN lov_cer on user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
						$sql .= " JOIN lov_stato_fisico ON user_schede_rifiuti.ID_SF=lov_stato_fisico.ID_SF";
						$sql .= " JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS";
						$sql .= " WHERE ID_MOV='" . $_GET["filter"] . "'";
						$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
						#
						#	ID SOGGETTI
						#
			//{{{
						$ID_RIF		= $FEDIT->DbRecordSet[0]["ID_RIF"];
						$ID_CAR		= $FEDIT->DbRecordSet[0]["ID_CAR"];
						$ID_COMM	= $FEDIT->DbRecordSet[0]["ID_COMM"];
						$ID_AZI		= $FEDIT->DbRecordSet[0]["ID_AZI"];
						$ID_UIMI	= $FEDIT->DbRecordSet[0]["ID_UIMI"];
						$ID_AUTHI	= $FEDIT->DbRecordSet[0]["ID_AUTHI"];
						$ID_OP_RS	= $FEDIT->DbRecordSet[0]["ID_OP_RS"];
						$ID_AZP		= $FEDIT->DbRecordSet[0]["ID_AZP"];
						$ID_UIMP	= $FEDIT->DbRecordSet[0]["ID_UIMP"];
						$ID_AZD		= $FEDIT->DbRecordSet[0]["ID_AZD"];
						$ID_UIMD	= $FEDIT->DbRecordSet[0]["ID_UIMD"];
						$ID_AUTHD	= $FEDIT->DbRecordSet[0]["ID_AUTHD"];
						$ID_AZT		= $FEDIT->DbRecordSet[0]["ID_AZT"];
						$ID_UIMT	= $FEDIT->DbRecordSet[0]["ID_UIMT"];
						$ID_AUTHT	= $FEDIT->DbRecordSet[0]["ID_AUTHT"];
						$ID_AUTOMZ	= $FEDIT->DbRecordSet[0]["ID_AUTO"];
						$ID_RMK		= $FEDIT->DbRecordSet[0]["ID_RMK"];
						$ID_AUTST	= $FEDIT->DbRecordSet[0]["ID_AUTST"];
						//}}}
						#
						#	ALTRI DATI
						#
			//{{{
						$ID_ONU = $FEDIT->DbRecordSet[0]["ID_ONU"];
						$ONU_pericoloso = $FEDIT->DbRecordSet[0]["ONU_per"];
						$ADR_NOTE = $FEDIT->DbRecordSet[0]["ADR_NOTE"];
						$COLLI = $FEDIT->DbRecordSet[0]["COLLI"];
						$inTR_data = $FEDIT->DbRecordSet[0]["dt_in_trasp"];
						$inTR_ora = $FEDIT->DbRecordSet[0]["hr_in_trasp"];
						$adr = $FEDIT->DbRecordSet[0]["adr"];
						$percorso = $FEDIT->DbRecordSet[0]["percorso"];
						$VerificaDestino =  $FEDIT->DbRecordSet[0]["VER_DESTINO"];
						$PesoNettoKg = $FEDIT->DbRecordSet[0]["pesoN"];
						$PesoLordoKg = $FEDIT->DbRecordSet[0]["pesoL"];
						$TaraKg = $FEDIT->DbRecordSet[0]["tara"];
						$PesoUnMisura =$FEDIT->DbRecordSet[0]["quantita"];
						$UnMisura =$FEDIT->DbRecordSet[0]["Umisura"];
						$Caratteristiche = $FEDIT->DbRecordSet[0]["caratteristiche"];
						$IDStatoFisico = $FEDIT->DbRecordSet[0]["SFID"];
						$StatoFisico = $FEDIT->DbRecordSet[0]["STFdes"];
						$Cer = $FEDIT->DbRecordSet[0]["COD_CER"];
						$Pericoloso = $FEDIT->DbRecordSet[0]["pericoloso"];
						$CerDescrizione = $FEDIT->DbRecordSet[0]["LawCerDes"];
						$DesRifiuto = $FEDIT->DbRecordSet[0]["descrizione"];
						$NOTEformulario = $FEDIT->DbRecordSet[0]["NOTEF"];
						$NOTEformulario_RIF = $FEDIT->DbRecordSet[0]["NOTE_RIFIUTO"];
						$QL = $FEDIT->DbRecordSet[0]["QL"];
						$NPER = $FEDIT->DbRecordSet[0]["NPER"];
						$CF_D = $FEDIT->DbRecordSet[0]["FKEcfiscD"];
						$CF_T = $FEDIT->DbRecordSet[0]["FKEcfiscT"];


						#
						#	ESENZIONE SCHEDA TRASPORTO
						#
						//$EsenzioneST = false;
						//$Collettame  = $FEDIT->DbRecordSet[0]["collettame"];
						//if( $Collettame=='1' | (strtoupper($CF_D)==strtoupper($CF_T)))
						//	$EsenzioneST = true;



						#
						#	CLASSI H
						#
						$CLASSIH = "";
						foreach($FEDIT->DbRecordSet[0] as $k=>$v) {
							if($k[0]=="H" && $v=="1") {
								$CLASSIH .= $k . " ";
								}
							}

						#
						#	DATA FORMULARIO
						#
						if($FEDIT->DbRecordSet[0]["DTFORM"]!="0000-00-00" && !is_null($FEDIT->DbRecordSet[0]["DTFORM"])) {
							$dtF = date(FGE_SCREEN_DATE,strtotime($FEDIT->DbRecordSet[0]["DTFORM"]));
							//$FEDIT->FGE_PdfBuffer->SetXY(165.2+$Xcorr,4.8+$Ycorr);
							$FEDIT->FGE_PdfBuffer->SetXY(165.2+$Xcorr,6.5+$Ycorr);
							$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$dtF,0,"L");
							}

						#
						#	NUMERO COLLI
						#
						if($SOGER->UserData["core_usersFRM_DIS_QNT"]==0 && $COLLI!="0" && !is_null($COLLI)) {
							#$FEDIT->FGE_PdfBuffer->SetXY(170+$Xcorr,182+$Ycorr);
							$FEDIT->FGE_PdfBuffer->SetXY(170+$Xcorr,178.5+$Ycorr);
							$FEDIT->FGE_PdfBuffer->MultiCell(15,5,$COLLI,0,"L");
							}

						#
						#	PRODUTTORE
						#
		//{{{
						if($SOGER->UserData["core_usersFRM_DIS_PRO"]=="0") {
							$sql = "SELECT user_aziende_produttori.description AS proDes,user_aziende_produttori.codfisc,";
							$sql .= "user_aziende_produttori.piva AS piva,user_impianti_produttori.telefono AS telefono,user_impianti_produttori.email AS email,user_impianti_produttori.description AS impInd";
							$sql .= ",lov_comuni_istat.description AS comDes,lov_comuni_istat.shdes_prov";
							$sql .= " FROM user_aziende_produttori";
							$sql .= " JOIN user_impianti_produttori ON user_aziende_produttori.ID_AZP=user_impianti_produttori.ID_AZP";
							$sql .= " JOIN lov_comuni_istat ON user_impianti_produttori.ID_COM=lov_comuni_istat.ID_COM";
							$sql .=" WHERE user_aziende_produttori.ID_AZP='$ID_AZP' AND user_impianti_produttori.ID_UIMP='$ID_UIMP'";
							$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
							#
                                                        $Azienda = $FEDIT->DbRecordSet[0]["proDes"];
							$CodFisc = $FEDIT->DbRecordSet[0]["codfisc"];
							if($CodFisc!=$FEDIT->DbRecordSet[0]["piva"] AND $FEDIT->DbRecordSet[0]["piva"]!="")
								$PivaP = "P.Iva: ".$FEDIT->DbRecordSet[0]["piva"];
							else $PivaP="";
							$Telefono = $FEDIT->DbRecordSet[0]["telefono"];
							$Email = $FEDIT->DbRecordSet[0]["email"];
							$NumAlboAutotrasp="";
							$IMPPRdes = $FEDIT->DbRecordSet[0]["impInd"]!="-"? $FEDIT->DbRecordSet[0]["impInd"] . " - " . $FEDIT->DbRecordSet[0]["comDes"] . "(" . $FEDIT->DbRecordSet[0]["shdes_prov"] . ")" : "";
							$Autorizzazione = false;
							FormSoggetto("produttore",26,$Azienda,$IMPPRdes,$CodFisc,$PivaP,$Autorizzazione,$Telefono,$Email,$NumAlboAutotrasp,$FEDIT->FGE_PdfBuffer);
						} //}}}
						#
						#	DESTINATARIO
						#
			//{{{
						if($SOGER->UserData["core_usersFRM_DIS_DES"]==0 && $ID_AZD!=0) {
								$sql = "SELECT user_aziende_destinatari.description AS desDes,user_aziende_destinatari.codfisc,user_aziende_destinatari.piva as piva";
								if($ID_UIMD!="0") {
									$sql .= ",user_impianti_destinatari.telefono AS telefono,user_impianti_destinatari.email AS email,user_impianti_destinatari.description AS impInd";
								}
								if($ID_AUTHD!="0") {
									$Autorizzazione = true;
									$sql .= ",user_autorizzazioni_dest.rilascio,user_autorizzazioni_dest.num_aut,user_autorizzazioni_dest.ID_OP_RS2, user_autorizzazioni_dest.note AS NoteAuth";
									$sql .= ",lov_operazioni_rs_sec.description AS OPRS2des";
								} else {
									$Autorizzazione = false;
								}
								$sql .= ",lov_comuni_istat.description AS comDes,lov_comuni_istat.shdes_prov";
								$sql .= " FROM user_aziende_destinatari";
								if($ID_UIMD!="0") {
									$sql .= " JOIN user_impianti_destinatari ON user_aziende_destinatari.ID_AZD=user_impianti_destinatari.ID_AZD";
								}
								$sql .= " JOIN lov_comuni_istat ON user_impianti_destinatari.ID_COM=lov_comuni_istat.ID_COM";
								if($ID_AUTHD!="0") {
									$sql .= " LEFT JOIN user_autorizzazioni_dest ON user_impianti_destinatari.ID_UIMD=user_autorizzazioni_dest.ID_UIMD";
									$sql .= " LEFT JOIN lov_operazioni_rs_sec ON user_autorizzazioni_dest.ID_OP_RS2 = lov_operazioni_rs_sec.ID_OP_RS2 ";
								}
								$sql .=" WHERE user_aziende_destinatari.ID_AZD='$ID_AZD'";
								if($ID_UIMD!="0") {
									$sql .=" AND user_impianti_destinatari.ID_UIMD='$ID_UIMD'";
								}
								if($ID_AUTHD!="0") {
									$sql .= " AND user_autorizzazioni_dest.ID_AUTHD='$ID_AUTHD'";
								}
								$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
								#


								$Azienda = $FEDIT->DbRecordSet[0]["desDes"];
								$CodFisc = $FEDIT->DbRecordSet[0]["codfisc"];
								if($CodFisc!=$FEDIT->DbRecordSet[0]["piva"] AND $FEDIT->DbRecordSet[0]["piva"]!="")
									$PivaD = "P.Iva: ".$FEDIT->DbRecordSet[0]["piva"];
								else $PivaD="";
								$Telefono = $ID_UIMD!="0"? $FEDIT->DbRecordSet[0]["telefono"]:"";
								$Email = $ID_UIMD!="0"? $FEDIT->DbRecordSet[0]["email"]:"";
								$NumAlboAutotrasp="";

								if(isset($FEDIT->DbRecordSet[0]["ID_OP_RS2"])) {
									$ID_OP_RS2 = $FEDIT->DbRecordSet[0]["ID_OP_RS2"];
									$ID_OP_RS2des = $FEDIT->DbRecordSet[0]["OPRS2des"];
								} else {
									$ID_OP_RS2 = null;
									$ID_OP_RS2des = null;
								}
								$IMPdes = $FEDIT->DbRecordSet[0]["impInd"] . " - " . $FEDIT->DbRecordSet[0]["comDes"] . "(" . $FEDIT->DbRecordSet[0]["shdes_prov"] . ")";
								if(!$Autorizzazione) {
									FormSoggetto("destinatario",56.1,$Azienda,@$IMPdes,$CodFisc,$PivaD,$Autorizzazione,$Telefono,$Email,$NumAlboAutotrasp,$FEDIT->FGE_PdfBuffer);
								} else {
									$NoteAuthD = $FEDIT->DbRecordSet[0]["NoteAuth"];
									FormSoggetto("destinatario",56.1,$Azienda,@$IMPdes,$CodFisc,$PivaD,$Autorizzazione,$Telefono,$Email,$NumAlboAutotrasp,$FEDIT->FGE_PdfBuffer,$FEDIT->DbRecordSet[0]["num_aut"],$FEDIT->DbRecordSet[0]["rilascio"]);
								}
							} else {
									$ID_OP_RS2 = null;
									$ID_OP_RS2des = null;
							}//}}}
							#
							#	TRASPORTATORE
							#
			//{{{
							if($SOGER->UserData["core_usersFRM_DIS_TRA"]==0 && $ID_AZT!=0) {
								$sql = "SELECT user_aziende_trasportatori.description AS traDes,user_aziende_trasportatori.NumAlboAutotrasp AS NumAlboAutotrasp,user_aziende_trasportatori.codfisc,user_aziende_trasportatori.piva as piva";
								if($ID_UIMT!="0") {
									$sql .= ",user_impianti_trasportatori.telefono AS telefono,user_impianti_trasportatori.email AS email,user_impianti_trasportatori.description AS impInd";
								}
								if($ID_AUTHT!="0") {
									$Autorizzazione = true;
									$sql .= ",user_autorizzazioni_trasp.rilascio,user_autorizzazioni_trasp.num_aut, user_autorizzazioni_trasp.note AS NoteAuth";
								} else {
									$Autorizzazione = false;
								}
								$sql .= ",lov_comuni_istat.description AS comDes,lov_comuni_istat.shdes_prov";
								$sql .= " FROM user_aziende_trasportatori";
								if($ID_UIMT!="0") {
									$sql .= " JOIN user_impianti_trasportatori ON user_aziende_trasportatori.ID_AZT=user_impianti_trasportatori.ID_AZT";
								}
								$sql .= " JOIN lov_comuni_istat ON user_impianti_trasportatori.ID_COM=lov_comuni_istat.ID_COM";
								if($ID_AUTHT!="0") {
									$sql .= " LEFT JOIN user_autorizzazioni_trasp ON user_impianti_trasportatori.ID_UIMT=user_autorizzazioni_trasp.ID_UIMT";
								}
								$sql .=" WHERE user_aziende_trasportatori.ID_AZT='$ID_AZT'";
								if($ID_UIMT!="0") {
									$sql .=" AND user_impianti_trasportatori.ID_UIMT='$ID_UIMT'";
								}
								if($ID_AUTHT!="0") {
									$sql .= " AND user_autorizzazioni_trasp.ID_AUTHT='$ID_AUTHT'";
								}
								$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
								#



								$Azienda = $FEDIT->DbRecordSet[0]["traDes"];
								$CodFisc = $FEDIT->DbRecordSet[0]["codfisc"];
								if($CodFisc!=$FEDIT->DbRecordSet[0]["piva"] AND $FEDIT->DbRecordSet[0]["piva"]!="")
									$PivaT = "P.Iva: ".$FEDIT->DbRecordSet[0]["piva"];
								else $PivaT="";
								$Telefono = $ID_UIMT!="0"? $FEDIT->DbRecordSet[0]["telefono"]:"";
								$Email = $ID_UIMT!="0"? $FEDIT->DbRecordSet[0]["email"]:"";
								$NumAlboAutotrasp = $FEDIT->DbRecordSet[0]["NumAlboAutotrasp"];
								$IMPdes = $FEDIT->DbRecordSet[0]["impInd"] . " - " . $FEDIT->DbRecordSet[0]["comDes"] . "(" . $FEDIT->DbRecordSet[0]["shdes_prov"] . ")";
								if(!$Autorizzazione) {
									FormSoggetto("trasportatore",85.5,$Azienda,@$IMPdes,$CodFisc,$PivaT,$Autorizzazione,$Telefono,$Email,$NumAlboAutotrasp,$FEDIT->FGE_PdfBuffer);
								} else {
									$NoteAuthT = $FEDIT->DbRecordSet[0]["NoteAuth"];
									FormSoggetto("trasportatore",85.5,$Azienda,@$IMPdes,$CodFisc,$PivaT,$Autorizzazione,$Telefono,$Email,$NumAlboAutotrasp,$FEDIT->FGE_PdfBuffer,$FEDIT->DbRecordSet[0]["num_aut"],$FEDIT->DbRecordSet[0]["rilascio"]);
								}
							} //}}}
							#
							#	NON PERICOLOSI NEL PROPRIO STABILIMENTO
							#
			//{{{
							if($NPER=="1") {
								$FEDIT->FGE_PdfBuffer->SetXY(71+$Xcorr,106.5+$Ycorr);
								$FEDIT->FGE_PdfBuffer->MultiCell(5,5,"X",0,"L");
								$FEDIT->FGE_PdfBuffer->SetXY(80+$Xcorr,106.5+$Ycorr);
								if($SOGER->UserData["core_usersFRM_DIS_PRO"]=="0") {
									$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$IMPPRdes,0,"L");
								}
							} //}}}
							#
							#	ANNOTAZIONI FORMULARIO
							#
			//{{{

							if(is_null($NOTEformulario) AND trim($NOTEformulario)=='') {
								$NOTEformulario = "";
								}
							else {
								$NOTEformulario = $NOTEformulario."\n";
								}

							# NOTE RELATIVE AL RIFIUTO
							if(!is_null($NOTEformulario_RIF) AND trim($NOTEformulario_RIF)!='') {
								$NOTEformulario.= $NOTEformulario_RIF."\n";
								}

							# NOTE RELATIVE ALLE AUTORIZZAZIONI DEI SOGGETTI
							if(isset($NoteAuthD) && $NoteAuthD!=""){
								$NOTEformulario.="Note aut. dest.: ";
								$NOTEformulario.=$NoteAuthD;
								$NOTEformulario.="\n";
								}

							if(isset($NoteAuthT) && $NoteAuthT!=""){
								$NOTEformulario.="Note num. albo trasp.: ";
								$NOTEformulario.=$NoteAuthT;
								$NOTEformulario.="\n";
								}

							if($SOGER->UserData["core_usersFRM_DIS_INT"]=="0" && $ID_AZI!="0" && !is_null($ID_AZI)) {
                                                                $sql = "SELECT user_aziende_intermediari.description AS intDes,user_aziende_intermediari.codfisc,user_aziende_intermediari.piva,user_impianti_intermediari.telefono,user_impianti_intermediari.email,user_impianti_intermediari.cellulare,user_impianti_intermediari.printRecapitoInFir,user_aziende_intermediari.indirizzo,user_aziende_intermediari.esclusoFIR";
                                                                $sql .= ",lov_comuni_istat.description AS comDes,lov_comuni_istat.shdes_prov";
                                                                $sql .= " FROM user_impianti_intermediari";
                                                                $sql .= " JOIN user_aziende_intermediari ON user_aziende_intermediari.ID_AZI=user_impianti_intermediari.ID_AZI";
																$sql .= " JOIN lov_comuni_istat ON user_aziende_intermediari.ID_COM=lov_comuni_istat.ID_COM";
                                                                $sql .= " WHERE user_impianti_intermediari.ID_UIMI=".$ID_UIMI.";";
								$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

								if($FEDIT->DbRecordSet[0]["esclusoFIR"]==0){
									$Intermediario = $FEDIT->DbRecordSet[0]["intDes"] . " - " . $FEDIT->DbRecordSet[0]["indirizzo"] . " - " . $FEDIT->DbRecordSet[0]["comDes"] . " (" . $FEDIT->DbRecordSet[0]["shdes_prov"] . ") - Codice Fiscale: " . $FEDIT->DbRecordSet[0]["codfisc"];

									if($FEDIT->DbRecordSet[0]["codfisc"]!=$FEDIT->DbRecordSet[0]["piva"] && $FEDIT->DbRecordSet[0]["piva"]!="")
										$Intermediario.=" P.Iva: ".$FEDIT->DbRecordSet[0]["piva"];

                                                                        if($FEDIT->DbRecordSet[0]['printRecapitoInFir']==1){
                                                                            if($FEDIT->DbRecordSet[0]['telefono']!=""){
                                                                                $Intermediario.=" Tel: ".$FEDIT->DbRecordSet[0]['telefono'];
                                                                                }
                                                                            elseif($FEDIT->DbRecordSet[0]['cellulare']!=""){
                                                                                $Intermediario.=" Cell: ".$FEDIT->DbRecordSet[0]['cellulare'];
                                                                                }
                                                                            elseif($FEDIT->DbRecordSet[0]['email']!=""){
                                                                                $Intermediario.=" Mail: ".$FEDIT->DbRecordSet[0]['email'];
                                                                                }
                                                                            }
									if(!is_null($ID_AUTHI)){
										$sql ="SELECT num_aut, rilascio, note FROM user_autorizzazioni_interm WHERE ID_AUTHI=".$ID_AUTHI;
										$FEDIT->SDBRead($sql,"DbRecordSetAuthI",true,false);
										$rilascio=explode("-",$FEDIT->DbRecordSetAuthI[0]['rilascio']);
										$rilascio=$rilascio[2]."/".$rilascio[1]."/".$rilascio[0];
										$Intermediario.=" Autorizzazione: ".$FEDIT->DbRecordSetAuthI[0]['num_aut']." del ".$rilascio;
										if($FEDIT->DbRecordSetAuthI[0]['note']!=''){
											$Intermediario.=" Note: ".addslashes($FEDIT->DbRecordSetAuthI[0]['note']);
											}
										}

									$NOTEformulario.="INTERMEDIARIO: " . $Intermediario."\n";
									}
								}

							# note relative al trasporto ADR
							if($SOGER->UserData["core_usersFRM_ADR"]=="1") {
								if(!is_null($ID_ONU) && $ID_ONU!=0) {
									$sql = "SELECT ONU_DES as des, lov_num_onu.description as num, eti, imb, classe, codice_galleria, ADR_2_1_3_5_5, ADR_PERICOLOSO_AMBIENTE, categoria_trasporto FROM user_schede_rifiuti JOIN lov_num_onu ON lov_num_onu.ID_ONU=user_schede_rifiuti.ID_ONU WHERE ID_RIF='$ID_RIF'";
									$FEDIT->SDBRead($sql,"DbRecordSet",true,false);


									if($FEDIT->DbRecordSet[0]["des"]==""){
										$sql = "SELECT des, description as num, categoria_trasporto FROM lov_num_onu WHERE ID_ONU='$ID_ONU'";
										$FEDIT->SDBRead($sql,"DbRecordSetONU",true,false);
										$ONUdes=$FEDIT->DbRecordSetONU[0]["des"];
										$ONUnum=$FEDIT->DbRecordSetONU[0]["num"];
										$CategoriaADR=$FEDIT->DbRecordSetONU[0]["categoria_trasporto"];
										}
									else{
										$ONUdes=$FEDIT->DbRecordSet[0]["des"];
										$ONUnum=$FEDIT->DbRecordSet[0]["num"];
										$CategoriaADR=$FEDIT->DbRecordSet[0]["categoria_trasporto"];
										}


									if($FEDIT->DbRecsNum>0) {

										$ImballaggioVNR = ($Cer=="150110" && $ONUnum!='3509')? true:false;

										if($ImballaggioVNR){
											$NOTEformularioADR.="RIFIUTO, IMBALLAGGI VUOTI";
											}
										else{
											if($FEDIT->DbRecordSet[0]["ADR_2_1_3_5_5"]==0)
												$NOTEformularioADR.="UN " . $ONUnum . " RIFIUTO " . $ONUdes;
											else
												$NOTEformularioADR.="UN " . $ONUnum . " " . $ONUdes;
											}

										if(!is_null($ONU_pericoloso) && $ONU_pericoloso!="") {
											$NOTEformularioADR.= ", ($ONU_pericoloso)";
											}

										if(!is_null($FEDIT->DbRecordSet[0]["eti"]) && $FEDIT->DbRecordSet[0]["eti"]!="") {
                                                                                        $etis = explode("+", $FEDIT->DbRecordSet[0]["eti"]);
                                                                                        $other_etis = "";
                                                                                        $oe = array();
                                                                                        if(count($etis)>1){
                                                                                            $other_etis.= " (";
                                                                                            for($e=1;$e<count($etis);$e++){
                                                                                                $oe[] = $etis[$e];
                                                                                            }
                                                                                            $other_etis.= implode(", ", $oe);
                                                                                            $other_etis.= ")";
                                                                                        }
                                                                                        $eti = $etis[0].$other_etis;
											$NOTEformularioADR.= ", " . $eti;
											}

										if(!$ImballaggioVNR){
											if(!is_null($FEDIT->DbRecordSet[0]["imb"]) && $FEDIT->DbRecordSet[0]["imb"]!="") {
												$NOTEformularioADR.= ", " . $FEDIT->DbRecordSet[0]["imb"];
												}
											}

										// codice restrizione galleria
										if(!$ImballaggioVNR && $QL=="0")
											$NOTEformularioADR.= " " . $FEDIT->DbRecordSet[0]["codice_galleria"];

										// NOTE ADR (es. codice imballaggio)
										if(!is_null($ADR_NOTE) AND trim($ADR_NOTE)!=''){
											$NOTEformularioADR.=" - ".$ADR_NOTE." - ";
											}

										// CAP. 5.4.1.1.18 - PERICOLOSO PER L'AMBIENTE
										$ONU_ESCLUSI = array("3082", "3077");
										if(!in_array($ONUnum, $ONU_ESCLUSI) AND $FEDIT->DbRecordSet[0]["ADR_PERICOLOSO_AMBIENTE"]==1){
											$NOTEformularioADR.=" PERICOLOSO PER L'AMBIENTE. ";
											}

										// CAP. 2.1.3.5.5
										if($FEDIT->DbRecordSet[0]["ADR_2_1_3_5_5"]==1){
											$NOTEformularioADR.=" RIFIUTI CONFORMI AL CAP. 2.1.3.5.5 . ";
											}

										// Oli in ADR
										$CER_OLI = array("120106", "120107", "120110", "120119", "130101", "130109", "130110", "130111", "130112", "130113", "130204", "130205", "130206", "130207", "130208", "130301", "130306", "130307", "130308", "130309", "130310", "130401", "130402", "130403", "130506", "130701", "130702", "130703");
										$ONU_OLI = array("3082", "3077");
										if($SOGER->UserData['core_usersFRM_ADR_OLI']==1 AND in_array($Cer, $CER_OLI) AND in_array($ONUnum, $ONU_OLI)){
											$NOTEformularioADR.= "Classificazione ADR conforme al CAP. 2.1.3.1 e 2.1.3.9 . ";
											}

										// classe 6.2
										if($FEDIT->DbRecordSet[0]['classe']=='6.2'){
											## riferimento del produttore
											if($ID_UIMP<>'0000000'){
												$SQL_UIMP = "SELECT ONU_62 FROM user_impianti_produttori WHERE ID_UIMP=".$ID_UIMP.";";
												$FEDIT->SDBRead($SQL_UIMP,"DbRecordSet62UIMP",true,false);
												if(!is_null($FEDIT->DbRecordSet62UIMP[0]["ONU_62"]) && $FEDIT->DbRecordSet62UIMP[0]["ONU_62"]!='')
													$NOTEformularioADR.="Rifiuto classe 6.2 - Persona di riferimento per il produttore: ".$FEDIT->DbRecordSet62UIMP[0]["ONU_62"].". ";
											}
											## riferimento del trasportatore
											if($ID_UIMT<>'0000000'){
												$SQL_UIMT = "SELECT ONU_62 FROM user_impianti_trasportatori WHERE ID_UIMT=".$ID_UIMT.";";
												$FEDIT->SDBRead($SQL_UIMT,"DbRecordSet62UIMT",true,false);
												if(!is_null($FEDIT->DbRecordSet62UIMT[0]["ONU_62"]) && $FEDIT->DbRecordSet62UIMT[0]["ONU_62"]!='')
													$NOTEformularioADR.="Rifiuto classe 6.2 - Persona di riferimento per il trasportatore: ".$FEDIT->DbRecordSet62UIMT[0]["ONU_62"].". ";
											}
											## riferimento del destinatario
											if($ID_UIMD<>'0000000'){
												$SQL_UIMD = "SELECT ONU_62 FROM user_impianti_destinatari WHERE ID_UIMD=".$ID_UIMD.";";
												$FEDIT->SDBRead($SQL_UIMD,"DbRecordSet62UIMD",true,false);
												if(!is_null($FEDIT->DbRecordSet62UIMD[0]["ONU_62"]) && $FEDIT->DbRecordSet62UIMD[0]["ONU_62"]!='')
													$NOTEformularioADR.="Rifiuto classe 6.2 - Persona di riferimento per il destinatario: ".$FEDIT->DbRecordSet62UIMD[0]["ONU_62"].". ";
											}
											## riferimento del intermediario
											if($ID_UIMI<>'0000000'){
												$SQL_UIMI = "SELECT ONU_62 FROM user_impianti_intermediari WHERE ID_UIMI=".$ID_UIMI.";";
												$FEDIT->SDBRead($SQL_UIMI,"DbRecordSet62UIMI",true,false);
												if(!is_null($FEDIT->DbRecordSet62UIMI[0]["ONU_62"]) && $FEDIT->DbRecordSet62UIMI[0]["ONU_62"]!='')
													$NOTEformularioADR.="Rifiuto classe 6.2 - Persona di riferimento per l'intermediario: ".$FEDIT->DbRecordSet62UIMI[0]["ONU_62"].". ";
											}
										}


										if($QL=="1") {
											if(!$ImballaggioVNR){
												$NOTEformularioADR .= " Esente dal trasporto ADR per unit� di trasporto, Cap. 1.1.3.6, categoria di trasporto ".$CategoriaADR.". ";
												}
											else{
												$NOTEformularioADR .= " Esente dal trasporto ADR per unit� di trasporto, Cap. 1.1.3.6, categoria di trasporto 4. ";
												}
											}

										$NOTEformulario .= $NOTEformularioADR."\n";

									}
								}
							}

							if($UnMisura=="Mc.") {
								$NOTEformulario .= "Quantit�: $PesoUnMisura Mc.\n";
								}
							if($SOGER->UserData["core_usersFRM_PRINT_CER"]=="1") {
									$NOTEformulario .= "NOME CER: $CerDescrizione\n";
							}
							if(!is_null($ID_OP_RS2) && $ID_OP_RS2!="0" && $SOGER->UserData["core_usersOPRDsec"]=="1") {
								$NOTEformulario .= "Altra causale di recupero/smaltimento autorizzata: " . $ID_OP_RS2des . "\n";
							}

							$FEDIT->FGE_PdfBuffer->SetXY(20+$Xcorr,122+$Ycorr);
							$FEDIT->FGE_PdfBuffer->MultiCell(170,5,$NOTEformulario,0,"L");
							//}}}
							#
							#	DESCRIZIONE RIFIUTO
							#
							#$FEDIT->FGE_PdfBuffer->SetXY(47+$Xcorr,169+$Ycorr);
							$FEDIT->FGE_PdfBuffer->SetXY(47+$Xcorr,165+$Ycorr);
							//$FEDIT->FGE_PdfBuffer->MultiCell(130,5,strtoupper($DesRifiuto),0,"L");
							$FEDIT->FGE_PdfBuffer->MultiCell(130,5,$DesRifiuto,0,"L");
							#
							#	CER
							#
							if( (3+$Xcorr)<0 )
								$XPosition = 1.5;
							else
								$XPosition = 3+$Xcorr;
							$FEDIT->FGE_PdfBuffer->SetXY($XPosition,178.5+$Ycorr);

							if($SOGER->UserData["core_usersCER_AXT"]=="1" && $Pericoloso=="1")
								$FEDIT->FGE_PdfBuffer->MultiCell(18,5,$Cer."*",0,"L");
							else
								$FEDIT->FGE_PdfBuffer->MultiCell(18,5,$Cer,0,"L");
							#
							#	STATO FISICO
							#
			//{{{
							switch($IDStatoFisico) {
								case "1":
								#$FEDIT->FGE_PdfBuffer->SetXY(71+$Xcorr,178+$Ycorr);
								$FEDIT->FGE_PdfBuffer->SetXY(73+$Xcorr,175+$Ycorr);
								break;
								case "2":
								$FEDIT->FGE_PdfBuffer->SetXY(77+$Xcorr,175+$Ycorr);
								break;
								case "3":
								$FEDIT->FGE_PdfBuffer->SetXY(83+$Xcorr,175+$Ycorr);
								break;
								case "4":
								$FEDIT->FGE_PdfBuffer->SetXY(87+$Xcorr,175+$Ycorr);###87 era 85###
								break;
							}
							$FEDIT->FGE_PdfBuffer->MultiCell(5,5,"X",0,"L");
							$FEDIT->FGE_PdfBuffer->SetXY(57+$Xcorr,178.5+$Ycorr);
							$FEDIT->FGE_PdfBuffer->MultiCell(35,5,$StatoFisico,0,"L"); //}}}
							#
							#	CLASSI H
							#
							$FEDIT->FGE_PdfBuffer->SetXY(92+$Xcorr,178.5+$Ycorr);
							$FEDIT->FGE_PdfBuffer->MultiCell(70,5,$CLASSIH,0,"L");
							#
							#	RECUPERO / SMALTIMENTO
							#
			//{{{
							if(!is_null($ID_OP_RS) && $ID_OP_RS!="0") {
								$sql = "SELECT lov_operazioni_rs.description AS opRS FROM lov_operazioni_rs WHERE ID_OP_RS='$ID_OP_RS'";
								$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
								$OPRS =  $FEDIT->DbRecordSet[0]["opRS"];
								if($OPRS[0]=="R") {
									if( (1.5+$Xcorr)<0 ) $XPosition = 1.5; else	$XPosition = 1.5+$Xcorr;
									$FEDIT->FGE_PdfBuffer->SetXY($XPosition,191+$Ycorr);
								} else {
									$FEDIT->FGE_PdfBuffer->SetXY(19.2+$Xcorr,191+$Ycorr);
								}
								$FEDIT->FGE_PdfBuffer->MultiCell(5,5,"X",0,"L");
								$FEDIT->FGE_PdfBuffer->SetXY(40+$Xcorr,191+$Ycorr);
								$FEDIT->FGE_PdfBuffer->MultiCell(40,5,$OPRS,0,"L");
							} //}}}
							#
							#	CARATTERISTICHE CHIMICO FISICHE
							#
							#$FEDIT->FGE_PdfBuffer->SetXY(110+$Xcorr,195+$Ycorr);
							$FEDIT->FGE_PdfBuffer->SetXY(110+$Xcorr,191+$Ycorr);
							$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$Caratteristiche,0,"L");
							#
							#	QUANTITA
							#
			//{{{
							if($SOGER->UserData["core_usersFRM_DIS_QNT"]==0) {
								if($UnMisura=="Mc.") {
									# peso lordo
									$FEDIT->FGE_PdfBuffer->SetXY(9+$Xcorr,205+$Ycorr);
									$FEDIT->FGE_PdfBuffer->MultiCell(20,5,"0",0,"L");
									# tara
									$FEDIT->FGE_PdfBuffer->SetXY(9+$Xcorr,208+$Ycorr);
									$FEDIT->FGE_PdfBuffer->MultiCell(20,5,"0",0,"L");
									#
									$FEDIT->FGE_PdfBuffer->SetXY(30+$Xcorr,200+$Ycorr);				// segna X su kili
									$FEDIT->FGE_PdfBuffer->MultiCell(5,5,"X",0,"L");
									#
									$FEDIT->FGE_PdfBuffer->SetXY(38.5+$Xcorr,205+$Ycorr);			// peso convertito in kili
									$FEDIT->FGE_PdfBuffer->MultiCell(20,5,number_format_unlimited_precision($PesoNettoKg),0,"L");
								} else {
									if($UnMisura=="Kg.") {
										$FEDIT->FGE_PdfBuffer->SetXY(30+$Xcorr,200+$Ycorr);
										$FEDIT->FGE_PdfBuffer->MultiCell(5,5,"X",0,"L");
									}
									if($UnMisura=="Litri") {
										$FEDIT->FGE_PdfBuffer->SetXY(30+$Xcorr,204+$Ycorr); 		// 204 era 205 //
										$FEDIT->FGE_PdfBuffer->MultiCell(5,5,"X",0,"L");
									}
									# peso lordo
									$FEDIT->FGE_PdfBuffer->SetXY(9+$Xcorr,205+$Ycorr);
									$FEDIT->FGE_PdfBuffer->MultiCell(20,4,number_format_unlimited_precision($PesoLordoKg),0,"L");
									# tara
									$FEDIT->FGE_PdfBuffer->SetXY(9+$Xcorr,208+$Ycorr);
									$FEDIT->FGE_PdfBuffer->MultiCell(20,5,number_format_unlimited_precision($TaraKg),0,"L");
									# peso
									$FEDIT->FGE_PdfBuffer->SetXY(38+$Xcorr,205+$Ycorr);
									$FEDIT->FGE_PdfBuffer->MultiCell(20,5,number_format_unlimited_precision($PesoUnMisura),0,"L");
								}
								#
								if($VerificaDestino=="1") {
									$FEDIT->FGE_PdfBuffer->SetXY(30+$Xcorr,208+$Ycorr);				// 208 era 210 //
									$FEDIT->FGE_PdfBuffer->MultiCell(5,5,"X",0,"L");
								}
							} //}}}
							#
							#	PERCORSO
							#
							$FEDIT->FGE_PdfBuffer->SetXY(62+$Xcorr,204+$Ycorr);
							$FEDIT->FGE_PdfBuffer->MultiCell(80,5,$percorso,0,"L");
							#
							#	TRASPORTO ADR
							#
			//{{{
							if($adr=="1") {
								#$FEDIT->FGE_PdfBuffer->SetXY(166+$Xcorr,211+$Ycorr);
								$FEDIT->FGE_PdfBuffer->SetXY(166+$Xcorr,207+$Ycorr);
							} else {
								#$FEDIT->FGE_PdfBuffer->SetXY(179+$Xcorr,211+$Ycorr);
								$FEDIT->FGE_PdfBuffer->SetXY(179+$Xcorr,207+$Ycorr);
							}
							$FEDIT->FGE_PdfBuffer->MultiCell(5,5,"X",0,"L"); //}}}
							#
							#	AUTOMEZZO
							#
			//{{{
							if($ID_AUTOMZ!="0" && !is_null($ID_AUTOMZ)) {
								$sql = "SELECT user_automezzi.description FROM user_automezzi WHERE ID_AUTO='$ID_AUTOMZ'";
								$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
								$FEDIT->FGE_PdfBuffer->SetXY(83+$Xcorr,229+$Ycorr);
								$FEDIT->FGE_PdfBuffer->MultiCell(35,5,$FEDIT->DbRecordSet[0]["description"],0,"L");
							} //}}}
							#
							#	RIMORCHIO
							#
			//{{{
							if($ID_RMK!="0" && !is_null($ID_RMK)) {
								$sql = "SELECT user_rimorchi.description FROM user_rimorchi WHERE ID_RMK='$ID_RMK'";
								$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
								$FEDIT->FGE_PdfBuffer->SetXY(144+$Xcorr,229+$Ycorr);
								$FEDIT->FGE_PdfBuffer->MultiCell(35,5,$FEDIT->DbRecordSet[0]["description"],0,"L");
							} //}}}
							#
							#	CONDUCENTE
							#
			//{{{
							if($ID_AUTST!="0" && !is_null($ID_AUTST)) {
								$sql = "SELECT user_autisti.description,user_autisti.nome FROM user_autisti WHERE ID_AUTST='$ID_AUTST'";
								$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
								$FEDIT->FGE_PdfBuffer->SetXY(35+$Xcorr,234+$Ycorr);
								$FEDIT->FGE_PdfBuffer->MultiCell(80,5,$FEDIT->DbRecordSet[0]["description"] . " " . $FEDIT->DbRecordSet[0]["nome"],0,"L");
							} //}}}
							#
							#	DATA/ORA TRASPORTO
							#


						if($inTR_data!="0000-00-00" && !is_null($inTR_data)) {
							$FEDIT->FGE_PdfBuffer->SetXY(150+$Xcorr,234+$Ycorr);
							$dt = date(FGE_SCREEN_DATE, strtotime($inTR_data));
							$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$dt,0,"L");
						}
						if($inTR_ora!="00:00:00" && $inTR_ora!="00:00" && !is_null($inTR_ora)) {
							$FEDIT->FGE_PdfBuffer->SetXY(177.5+$Xcorr,234+$Ycorr);
							$ora = date("H:i",strtotime($inTR_ora));
							$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$ora,0,"L");
						}
					}

				}
			else{
				// isset($_GET["Registro"])
				# registro industriale
				if(isset($_GET['Industriale'])){

					# reg. cronologico
					if(isset($_GET['Cronologico'])){
						include("../__includes/REG_cronologicoCODE.php");
						}

					# reg. settimanale
					if(isset($_GET['Settimanale'])){
						include("../__includes/REG_settimanaleCODE.php");
						}

					}

				}

				break;

			case "user_movimenti_fiscalizzati":
				if(!isset($_GET["Registro"])) {
					# stampa fomulario
					if($SOGER->UserData["core_usersD3"]=="0") {
						$deniedByConfig = true;
						$SOGER->SetFeedback("L'utente non ha i permessi necessari per la stampa del formulario [cod. D3]","1");
						}
					else {
						$Xcorr =  $SOGER->UserData["core_usersFRM_PRNT_X"];
						if($SOGER->UserData["core_usersFRM_PRNT_Y"]>-4.8) {
							$Ycorr = $SOGER->UserData["core_usersFRM_PRNT_Y"];
						} else {
							$Ycorr = -4.8;
						}
						#
						$sql = "UPDATE user_movimenti_fiscalizzati SET FISCALE='" . $_GET["Fiscale"] . "' WHERE ID_MOV_F='" . $_GET["filter"] . "' AND FISCALE='0'";
						$FEDIT->SDBWrite($sql,true,false);

						$DcName = "Stampa Formulario";
						$sql = "SELECT ";
						$sql .= "user_movimenti_fiscalizzati.ID_RIF,user_movimenti_fiscalizzati.ID_COMM,user_movimenti_fiscalizzati.ID_CAR, user_movimenti_fiscalizzati.ID_AZP,user_movimenti_fiscalizzati.ID_UIMP,user_movimenti_fiscalizzati.NOTEF,user_movimenti_fiscalizzati.DTFORM";
						$sql .= ",user_movimenti_fiscalizzati.ID_AZD,user_movimenti_fiscalizzati.ID_UIMD,user_movimenti_fiscalizzati.ID_AUTHD,user_movimenti_fiscalizzati.ID_OP_RS";
						$sql .= ",user_movimenti_fiscalizzati.ID_AZT,user_movimenti_fiscalizzati.ID_UIMT,user_movimenti_fiscalizzati.ID_AUTHT,user_movimenti_fiscalizzati.ID_AZI,user_movimenti_fiscalizzati.ID_UIMI,user_movimenti_fiscalizzati.ID_AUTHI,user_movimenti_fiscalizzati.ID_AUTO";
						$sql .= ",user_movimenti_fiscalizzati.ID_RMK,user_movimenti_fiscalizzati.ID_AUTST,QL";
						$sql .= ",user_movimenti_fiscalizzati.percorso,user_movimenti_fiscalizzati.adr,user_movimenti_fiscalizzati.NPER,user_movimenti_fiscalizzati.VER_DESTINO,user_movimenti_fiscalizzati.dt_in_trasp,user_movimenti_fiscalizzati.hr_in_trasp";
						$sql .= ",user_movimenti_fiscalizzati.FKEcfiscD, user_movimenti_fiscalizzati.FKEcfiscT";
						$sql .= ",user_movimenti_fiscalizzati.quantita,user_movimenti_fiscalizzati.tara,user_movimenti_fiscalizzati.pesoL,user_movimenti_fiscalizzati.pesoN,user_movimenti_fiscalizzati.COLLI";
						$sql .= ",user_schede_rifiuti.descrizione,user_schede_rifiuti.caratteristiche,user_schede_rifiuti.ID_ONU,user_schede_rifiuti.ONU_per, user_schede_rifiuti.ADR_NOTE, user_schede_rifiuti.ID_ETICHETTA_ADR, user_schede_rifiuti.ID_IMBALLAGGIO_ADR";
						$sql .= ",H1,H2,H3A,H3B,H4,H5,H6,H7,H8,H9,H10,H11,H12,H13,H14,H15";
						$sql .= ",HP1,HP2,HP3,HP4,HP5,HP6,HP7,HP8,HP9,HP10,HP11,HP12,HP13,HP14,HP15";
						$sql .= ",user_schede_rifiuti.NOTEF AS NOTE_RIFIUTO";
						$sql .= ",lov_cer.COD_CER,lov_cer.PERICOLOSO as pericoloso, lov_cer.description AS LawCerDes, lov_stato_fisico.description AS STFdes,lov_stato_fisico.ID_SF as SFID";
						$sql .= ",lov_misure.description AS Umisura";
						$sql .= " FROM user_movimenti_fiscalizzati";
						$sql .= " JOIN user_schede_rifiuti on user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF";
						$sql .= " JOIN lov_cer on user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
						$sql .= " JOIN lov_stato_fisico ON user_schede_rifiuti.ID_SF=lov_stato_fisico.ID_SF";
						$sql .= " JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS";
						$sql .= " WHERE ID_MOV_F='" . $_GET["filter"] . "'";
						$sql .= " AND user_movimenti_fiscalizzati.ID_IMP='" . $SOGER->UserData["core_usersID_IMP"] . "'";
						$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
						#
						#	ID SOGGETTI
						#
			//{{{
						$ID_RIF = $FEDIT->DbRecordSet[0]["ID_RIF"];
						$ID_CAR = $FEDIT->DbRecordSet[0]["ID_CAR"];     ##
						$ID_COMM = $FEDIT->DbRecordSet[0]["ID_COMM"];   ##
						$ID_AZI = $FEDIT->DbRecordSet[0]["ID_AZI"];
						$ID_UIMI = $FEDIT->DbRecordSet[0]["ID_UIMI"];
						$ID_AUTHI = $FEDIT->DbRecordSet[0]["ID_AUTHI"];
						$ID_OP_RS = $FEDIT->DbRecordSet[0]["ID_OP_RS"];
						$ID_AZP = $FEDIT->DbRecordSet[0]["ID_AZP"];
						$ID_UIMP = $FEDIT->DbRecordSet[0]["ID_UIMP"];
						$ID_AZD = $FEDIT->DbRecordSet[0]["ID_AZD"];
						$ID_UIMD = $FEDIT->DbRecordSet[0]["ID_UIMD"];
						$ID_AUTHD = $FEDIT->DbRecordSet[0]["ID_AUTHD"];
						$ID_AZT = $FEDIT->DbRecordSet[0]["ID_AZT"];
						$ID_UIMT = $FEDIT->DbRecordSet[0]["ID_UIMT"];
						$ID_AUTHT = $FEDIT->DbRecordSet[0]["ID_AUTHT"];
						$ID_AUTOMZ = $FEDIT->DbRecordSet[0]["ID_AUTO"];
						$ID_RMK = $FEDIT->DbRecordSet[0]["ID_RMK"];
						$ID_AUTST = $FEDIT->DbRecordSet[0]["ID_AUTST"];
						//}}}
						#
						#	ALTRI DATI
						#
			//{{{
						$ID_ONU = $FEDIT->DbRecordSet[0]["ID_ONU"];
						$ID_ETICHETTA_ADR = $FEDIT->DbRecordSet[0]["ID_ETICHETTA_ADR"];
						$ID_IMBALLAGGIO_ADR = $FEDIT->DbRecordSet[0]["ID_IMBALLAGGIO_ADR"];
						$ONU_pericoloso = $FEDIT->DbRecordSet[0]["ONU_per"];
						$ADR_NOTE = $FEDIT->DbRecordSet[0]["ADR_NOTE"];
						$COLLI = $FEDIT->DbRecordSet[0]["COLLI"];
						$inTR_data = $FEDIT->DbRecordSet[0]["dt_in_trasp"];
						$inTR_ora = $FEDIT->DbRecordSet[0]["hr_in_trasp"];
						$adr = $FEDIT->DbRecordSet[0]["adr"];
						$percorso = $FEDIT->DbRecordSet[0]["percorso"];
						$VerificaDestino =  $FEDIT->DbRecordSet[0]["VER_DESTINO"];
						$PesoNettoKg = $FEDIT->DbRecordSet[0]["pesoN"];
						$PesoLordoKg = $FEDIT->DbRecordSet[0]["pesoL"];
						$TaraKg = $FEDIT->DbRecordSet[0]["tara"];
						$PesoUnMisura =$FEDIT->DbRecordSet[0]["quantita"];
						$UnMisura =$FEDIT->DbRecordSet[0]["Umisura"];
						$Caratteristiche = $FEDIT->DbRecordSet[0]["caratteristiche"];
						$IDStatoFisico = $FEDIT->DbRecordSet[0]["SFID"];
						$StatoFisico = $FEDIT->DbRecordSet[0]["STFdes"];
						$Cer = $FEDIT->DbRecordSet[0]["COD_CER"];
						$Pericoloso = $FEDIT->DbRecordSet[0]["pericoloso"];
						$CerDescrizione = $FEDIT->DbRecordSet[0]["LawCerDes"];
						$DesRifiuto = $FEDIT->DbRecordSet[0]["descrizione"];
						$NOTEformulario = $FEDIT->DbRecordSet[0]["NOTEF"];
						$NOTEformulario_RIF = $FEDIT->DbRecordSet[0]['NOTE_RIFIUTO'];
						$QL = $FEDIT->DbRecordSet[0]["QL"];
						$NPER = $FEDIT->DbRecordSet[0]["NPER"];
						$CF_D = $FEDIT->DbRecordSet[0]["FKEcfiscD"];
						$CF_T = $FEDIT->DbRecordSet[0]["FKEcfiscT"];


						#
						#	ESENZIONE SCHEDA TRASPORTO
						#
						//$EsenzioneST = false;
						//$Collettame  = $FEDIT->DbRecordSet[0]["collettame"];
						//if( $Collettame=='1' | (strtoupper($CF_D)==strtoupper($CF_T)))
						//	$EsenzioneST = true;



							#
							#	CLASSI H
							#
							$CLASSIH = "";
							foreach($FEDIT->DbRecordSet[0] as $k=>$v) {
								if($k[0]=="H" && $k[1]!="P" && $v=="1") {
									$CLASSIH .= $k . " ";
								}
							}

							#
							#	CLASSI HP
							#
							$CLASSIHP = "";
							foreach($FEDIT->DbRecordSet[0] as $k=>$v) {
								if($k[0]=="H" && $k[1]=="P" && $v=="1") {
									$CLASSIHP .= $k . " ";
								}
							}
							
							#
							#	COMPRESENZA HP4 / HP8
							#
							if($SOGER->UserData['core_usersPRNT_HP4_HP8']==0){
								if (strpos($CLASSIHP, 'HP4') !== false && strpos($CLASSIHP, 'HP8') !== false) {
									$CLASSIHP = str_replace('HP4 ', '', $CLASSIHP);
								}
							}
							

							#
							#	DATA FORMULARIO
							#
			//{{{
							if($FEDIT->DbRecordSet[0]["DTFORM"]!="0000-00-00" && !is_null($FEDIT->DbRecordSet[0]["DTFORM"])) {
								$dtF = date(FGE_SCREEN_DATE,strtotime($FEDIT->DbRecordSet[0]["DTFORM"]));
								$DTFORM = $FEDIT->DbRecordSet[0]["DTFORM"];
								$FEDIT->FGE_PdfBuffer->SetXY(165.2+$Xcorr,6.5+$Ycorr);
								$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$dtF,0,"L");
								}
							else $DTFORM = date('Y-m-d');


							#
							#	NUMERO COLLI
							#
		//{{{
							if($SOGER->UserData["core_usersFRM_DIS_QNT"]==0 && $COLLI!="0" && !is_null($COLLI)) {
								$FEDIT->FGE_PdfBuffer->SetXY(170+$Xcorr,178.5+$Ycorr);
								$FEDIT->FGE_PdfBuffer->MultiCell(15,5,$COLLI,0,"L");
							}

							#
							#	PRODUTTORE
							#
			//{{{
							if($SOGER->UserData["core_usersFRM_DIS_PRO"]=="0") {
								$sql = "SELECT user_aziende_produttori.description AS proDes,user_aziende_produttori.codfisc,";
								$sql .= "user_aziende_produttori.piva AS piva,user_impianti_produttori.telefono AS telefono,user_impianti_produttori.email AS email,user_impianti_produttori.description AS impInd";
								$sql .= ",lov_comuni_istat.description AS comDes,lov_comuni_istat.shdes_prov";
								$sql .= " FROM user_aziende_produttori";
								$sql .= " JOIN user_impianti_produttori ON user_aziende_produttori.ID_AZP=user_impianti_produttori.ID_AZP";
								$sql .= " JOIN lov_comuni_istat ON user_impianti_produttori.ID_COM=lov_comuni_istat.ID_COM";
								$sql .=" WHERE user_aziende_produttori.ID_AZP='$ID_AZP' AND user_impianti_produttori.ID_UIMP='$ID_UIMP'";
								$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
								#

								$Azienda = $FEDIT->DbRecordSet[0]["proDes"];
								$CodFisc = $FEDIT->DbRecordSet[0]["codfisc"];
								if($CodFisc!=$FEDIT->DbRecordSet[0]["piva"] AND $FEDIT->DbRecordSet[0]["piva"]!="")
									$PivaP = "P.Iva: ".$FEDIT->DbRecordSet[0]["piva"];
								else $PivaP="";
								$Telefono = $FEDIT->DbRecordSet[0]["telefono"];
								$Email = $FEDIT->DbRecordSet[0]["email"];
								$NumAlboAutotrasp="";
								$IMPPRdes = $FEDIT->DbRecordSet[0]["impInd"]!="-"? $FEDIT->DbRecordSet[0]["impInd"] . " - " . $FEDIT->DbRecordSet[0]["comDes"] . "(" . $FEDIT->DbRecordSet[0]["shdes_prov"] . ")" : "";
                                $Autorizzazione = false;
								FormSoggetto("produttore",26,$Azienda,$IMPPRdes,$CodFisc,$PivaP,$Autorizzazione,$Telefono,$Email,$NumAlboAutotrasp,$FEDIT->FGE_PdfBuffer);

							} //}}}
							#
							#	DESTINATARIO
							#
			//{{{
						if($SOGER->UserData["core_usersFRM_DIS_DES"]==0 && $ID_AZD!=0) {
								$sql = "SELECT user_aziende_destinatari.description AS desDes,user_aziende_destinatari.codfisc,user_aziende_destinatari.piva as piva";
								if($ID_UIMD!="0") {
									$sql .= ",user_impianti_destinatari.telefono AS telefono,user_impianti_destinatari.email AS email,user_impianti_destinatari.description AS impInd";
								}
								if($ID_AUTHD!="0") {
									$Autorizzazione = true;
									$sql .= ",user_autorizzazioni_dest.rilascio,user_autorizzazioni_dest.num_aut,user_autorizzazioni_dest.ID_OP_RS2, user_autorizzazioni_dest.note AS NoteAuth";
									$sql .= ",lov_operazioni_rs_sec.description AS OPRS2des";
								} else {
									$Autorizzazione = false;
								}
								$sql .= ",lov_comuni_istat.description AS comDes,lov_comuni_istat.shdes_prov";
								$sql .= " FROM user_aziende_destinatari";
								if($ID_UIMD!="0") {
									$sql .= " JOIN user_impianti_destinatari ON user_aziende_destinatari.ID_AZD=user_impianti_destinatari.ID_AZD";
								}
								$sql .= " JOIN lov_comuni_istat ON user_impianti_destinatari.ID_COM=lov_comuni_istat.ID_COM";
								if($ID_AUTHD!="0") {
									$sql .= " LEFT JOIN user_autorizzazioni_dest ON user_impianti_destinatari.ID_UIMD=user_autorizzazioni_dest.ID_UIMD";
									$sql .= " LEFT JOIN lov_operazioni_rs_sec ON user_autorizzazioni_dest.ID_OP_RS2 = lov_operazioni_rs_sec.ID_OP_RS2 ";
								}
								$sql .=" WHERE user_aziende_destinatari.ID_AZD='$ID_AZD'";
								if($ID_UIMD!="0") {
									$sql .=" AND user_impianti_destinatari.ID_UIMD='$ID_UIMD'";
								}
								if($ID_AUTHD!="0") {
									$sql .= " AND user_autorizzazioni_dest.ID_AUTHD='$ID_AUTHD'";
								}
								$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
								#


								$Azienda = $FEDIT->DbRecordSet[0]["desDes"];
								$CodFisc = $FEDIT->DbRecordSet[0]["codfisc"];
								if($CodFisc!=$FEDIT->DbRecordSet[0]["piva"] AND $FEDIT->DbRecordSet[0]["piva"]!="")
									$PivaD = "P.Iva: ".$FEDIT->DbRecordSet[0]["piva"];
								else $PivaD="";
								$Telefono = $FEDIT->DbRecordSet[0]["telefono"];
								$Email = $FEDIT->DbRecordSet[0]["email"];
								$NumAlboAutotrasp="";

								if(isset($FEDIT->DbRecordSet[0]["ID_OP_RS2"])) {
									$ID_OP_RS2 = $FEDIT->DbRecordSet[0]["ID_OP_RS2"];
									$ID_OP_RS2des = $FEDIT->DbRecordSet[0]["OPRS2des"];
								} else {
									$ID_OP_RS2 = null;
									$ID_OP_RS2des = null;
								}
								$IMPdes = $FEDIT->DbRecordSet[0]["impInd"] . " - " . $FEDIT->DbRecordSet[0]["comDes"] . "(" . $FEDIT->DbRecordSet[0]["shdes_prov"] . ")";
								if(!$Autorizzazione) {
									FormSoggetto("destinatario",56.1,$Azienda,@$IMPdes,$CodFisc,$PivaD,$Autorizzazione,$Telefono,$Email,$NumAlboAutotrasp,$FEDIT->FGE_PdfBuffer);
								} else {
									$NoteAuthD = $FEDIT->DbRecordSet[0]["NoteAuth"];
									FormSoggetto("destinatario",56.1,$Azienda,@$IMPdes,$CodFisc,$PivaD,$Autorizzazione,$Telefono,$Email,$NumAlboAutotrasp,$FEDIT->FGE_PdfBuffer,$FEDIT->DbRecordSet[0]["num_aut"],$FEDIT->DbRecordSet[0]["rilascio"]);
								}
							} else {
									$ID_OP_RS2 = null;
									$ID_OP_RS2des = null;
							}//}}}
							#
							#	TRASPORTATORE
							#
			//{{{
							if($SOGER->UserData["core_usersFRM_DIS_TRA"]==0 && $ID_AZT!=0) {
								$sql = "SELECT user_aziende_trasportatori.description AS traDes,user_aziende_trasportatori.NumAlboAutotrasp AS NumAlboAutotrasp,user_aziende_trasportatori.codfisc,user_aziende_trasportatori.piva as piva";
								if($ID_UIMT!="0") {
									$sql .= ",user_impianti_trasportatori.telefono AS telefono,user_impianti_trasportatori.email AS email,user_impianti_trasportatori.description AS impInd";
								}
								if($ID_AUTHT!="0") {
									$Autorizzazione = true;
									$sql .= ",user_autorizzazioni_trasp.rilascio,user_autorizzazioni_trasp.num_aut, user_autorizzazioni_trasp.note AS NoteAuth";
								} else {
									$Autorizzazione = false;
								}
								$sql .= ",lov_comuni_istat.description AS comDes,lov_comuni_istat.shdes_prov";
								$sql .= " FROM user_aziende_trasportatori";
								if($ID_UIMT!="0") {
									$sql .= " JOIN user_impianti_trasportatori ON user_aziende_trasportatori.ID_AZT=user_impianti_trasportatori.ID_AZT";
								}
								$sql .= " JOIN lov_comuni_istat ON user_impianti_trasportatori.ID_COM=lov_comuni_istat.ID_COM";
								if($ID_AUTHT!="0") {
									$sql .= " LEFT JOIN user_autorizzazioni_trasp ON user_impianti_trasportatori.ID_UIMT=user_autorizzazioni_trasp.ID_UIMT";
								}
								$sql .=" WHERE user_aziende_trasportatori.ID_AZT='$ID_AZT'";
								if($ID_UIMT!="0") {
									$sql .=" AND user_impianti_trasportatori.ID_UIMT='$ID_UIMT'";
								}
								if($ID_AUTHT!="0") {
									$sql .= " AND user_autorizzazioni_trasp.ID_AUTHT='$ID_AUTHT'";
								}
								$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
								#



								$Azienda = $FEDIT->DbRecordSet[0]["traDes"];
								$CodFisc = $FEDIT->DbRecordSet[0]["codfisc"];
								if($CodFisc!=$FEDIT->DbRecordSet[0]["piva"] AND $FEDIT->DbRecordSet[0]["piva"]!="")
									$PivaT = "P.Iva: ".$FEDIT->DbRecordSet[0]["piva"];
								else $PivaT="";
								$Telefono = $FEDIT->DbRecordSet[0]["telefono"];
								$Email = $FEDIT->DbRecordSet[0]["email"];
								$NumAlboAutotrasp = $FEDIT->DbRecordSet[0]["NumAlboAutotrasp"];
								$IMPdes = $FEDIT->DbRecordSet[0]["impInd"] . " - " . $FEDIT->DbRecordSet[0]["comDes"] . "(" . $FEDIT->DbRecordSet[0]["shdes_prov"] . ")";
								if(!$Autorizzazione) {
									FormSoggetto("trasportatore",85.5,$Azienda,@$IMPdes,$CodFisc,$PivaT,$Autorizzazione,$Telefono,$Email,$NumAlboAutotrasp,$FEDIT->FGE_PdfBuffer);
								} else {
									$NoteAuthT = $FEDIT->DbRecordSet[0]["NoteAuth"];
									FormSoggetto("trasportatore",85.5,$Azienda,@$IMPdes,$CodFisc,$PivaT,$Autorizzazione,$Telefono,$Email,$NumAlboAutotrasp,$FEDIT->FGE_PdfBuffer,$FEDIT->DbRecordSet[0]["num_aut"],$FEDIT->DbRecordSet[0]["rilascio"]);
								}
							} //}}}
							#
							#	NON PERICOLOSI NEL PROPRIO STABILIMENTO
							#
			//{{{
							if($NPER=="1") {
								$FEDIT->FGE_PdfBuffer->SetXY(71+$Xcorr,106.5+$Ycorr);
								$FEDIT->FGE_PdfBuffer->MultiCell(5,5,"X",0,"L");
								$FEDIT->FGE_PdfBuffer->SetXY(80+$Xcorr,106.5+$Ycorr);
								if($SOGER->UserData["core_usersFRM_DIS_PRO"]=="0") {
									$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$IMPPRdes,0,"L");
								}
							} //}}}
							#
							#	ANNOTAZIONI FORMULARIO
							#
			//{{{

							if(is_null($NOTEformulario) AND trim($NOTEformulario)=='') {
								$NOTEformulario = "";
								}
							else {
								$NOTEformulario = $NOTEformulario."\n";
								}

							# NOTE RELATIVE ALLA VECCHIA CLASSIFICAZIONE (H)
//							if($SOGER->UserData["core_usersPRNT_CLASSIH"]=="1") {
//								if(trim($CLASSIH)!="" && strtotime(date($DTFORM))>=strtotime('2015-06-01')){
//									$NOTEformulario.="Classificazione secondo Direttiva 2008/98/CE: ".$CLASSIH."\n";
//									}
//								}

							# NOTE RELATIVE AL RIFIUTO
							if(!is_null($NOTEformulario_RIF) AND trim($NOTEformulario_RIF)!='') {
								$NOTEformulario.= $NOTEformulario_RIF."\n";
								}

							# NOTE RELATIVE ALLE AUTORIZZAZIONI DEI SOGGETTI
							if(isset($NoteAuthD) && $NoteAuthD!=""){
								$NOTEformulario.="Note aut. dest.: ";
								$NOTEformulario.=$NoteAuthD;
								$NOTEformulario.="\n";
								}

							if(isset($NoteAuthT) && $NoteAuthT!=""){
								$NOTEformulario.="Note num. albo trasp.: ";
								$NOTEformulario.=$NoteAuthT;
								$NOTEformulario.="\n";
								}

							if($SOGER->UserData["core_usersFRM_DIS_INT"]=="0" && $ID_AZI!="0" && !is_null($ID_AZI)) {
                                $sql = "SELECT user_aziende_intermediari.description AS intDes,user_aziende_intermediari.codfisc,user_aziende_intermediari.piva,user_impianti_intermediari.telefono,user_impianti_intermediari.email,user_impianti_intermediari.cellulare,user_impianti_intermediari.printRecapitoInFir,user_aziende_intermediari.indirizzo,user_aziende_intermediari.esclusoFIR";
                                $sql .= ",lov_comuni_istat.description AS comDes,lov_comuni_istat.shdes_prov";
                                $sql .= " FROM user_impianti_intermediari";
                                $sql .= " JOIN user_aziende_intermediari ON user_aziende_intermediari.ID_AZI=user_impianti_intermediari.ID_AZI";
								$sql .= " JOIN lov_comuni_istat ON user_aziende_intermediari.ID_COM=lov_comuni_istat.ID_COM";
                                $sql .= " WHERE user_impianti_intermediari.ID_UIMI=".$ID_UIMI.";";
                                $FEDIT->SDBRead($sql,"DbRecordSet",true,false);

								if($FEDIT->DbRecordSet[0]["esclusoFIR"]==0){
									$Intermediario = $FEDIT->DbRecordSet[0]["intDes"] . " - " . $FEDIT->DbRecordSet[0]["indirizzo"] . " - " . $FEDIT->DbRecordSet[0]["comDes"] . " (" . $FEDIT->DbRecordSet[0]["shdes_prov"] . ") - Codice Fiscale: " . $FEDIT->DbRecordSet[0]["codfisc"];

									if($FEDIT->DbRecordSet[0]["codfisc"]!=$FEDIT->DbRecordSet[0]["piva"] && $FEDIT->DbRecordSet[0]["piva"]!="")
										$Intermediario.=" P.Iva: ".$FEDIT->DbRecordSet[0]["piva"];

										if($FEDIT->DbRecordSet[0]['printRecapitoInFir']==1){
											if($FEDIT->DbRecordSet[0]['telefono']!=""){
												$Intermediario.=" Tel: ".$FEDIT->DbRecordSet[0]['telefono'];
												}
											elseif($FEDIT->DbRecordSet[0]['cellulare']!=""){
												$Intermediario.=" Cell: ".$FEDIT->DbRecordSet[0]['cellulare'];
												}
											elseif($FEDIT->DbRecordSet[0]['email']!=""){
												$Intermediario.=" Mail: ".$FEDIT->DbRecordSet[0]['email'];
												}
											}

									if(!is_null($ID_AUTHI)){
										$sql ="SELECT num_aut, rilascio, note FROM user_autorizzazioni_interm WHERE ID_AUTHI=".$ID_AUTHI;
										$FEDIT->SDBRead($sql,"DbRecordSetAuthI",true,false);
										$rilascio=explode("-",$FEDIT->DbRecordSetAuthI[0]['rilascio']);
										$rilascio=$rilascio[2]."/".$rilascio[1]."/".$rilascio[0];
										$Intermediario.=" Autorizzazione: ".$FEDIT->DbRecordSetAuthI[0]['num_aut']." del ".$rilascio;
										if($FEDIT->DbRecordSetAuthI[0]['note']!=''){
											$Intermediario.=" Note: ".addslashes($FEDIT->DbRecordSetAuthI[0]['note']);
											}
										}

									$NOTEformulario.="INTERMEDIARIO: " . $Intermediario."\n";
									}
								}

							# note relative al trasporto ADR
							if($SOGER->UserData["core_usersFRM_ADR"]=="1") {
								if(!is_null($ID_ONU) && $ID_ONU!=0) {
									$sql = "SELECT ONU_DES as des, lov_num_onu.description as num, eti, imb, codice_galleria, classe, ADR_2_1_3_5_5, ADR_PERICOLOSO_AMBIENTE, categoria_trasporto FROM user_schede_rifiuti JOIN lov_num_onu ON lov_num_onu.ID_ONU=user_schede_rifiuti.ID_ONU WHERE ID_RIF='$ID_RIF'";
									$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

									if($FEDIT->DbRecordSet[0]["des"]==""){
										$sql = "SELECT des, description as num, categoria_trasporto FROM lov_num_onu WHERE ID_ONU='$ID_ONU'";
										$FEDIT->SDBRead($sql,"DbRecordSetONU",true,false);
										$ONUdes=$FEDIT->DbRecordSetONU[0]["des"];
										$ONUnum=$FEDIT->DbRecordSetONU[0]["num"];
										$CategoriaADR=$FEDIT->DbRecordSetONU[0]["categoria_trasporto"];
										}
									else{
										$ONUdes=$FEDIT->DbRecordSet[0]["des"];
										$ONUnum=$FEDIT->DbRecordSet[0]["num"];
										$CategoriaADR=$FEDIT->DbRecordSet[0]["categoria_trasporto"];
										}

									if($FEDIT->DbRecsNum>0) {

										$ImballaggioVNR = ($Cer=="150110" && $ONUnum!='3509')? true:false;

										if($ImballaggioVNR){
											$NOTEformularioADR.="RIFIUTO, IMBALLAGGI VUOTI";
											}
										else{
											if($FEDIT->DbRecordSet[0]["ADR_2_1_3_5_5"]==0)
												$NOTEformularioADR.="UN " . $ONUnum . " RIFIUTO " . $ONUdes;
											else
												$NOTEformularioADR.="UN " . $ONUnum . " " . $ONUdes;
											}

										if(!is_null($ONU_pericoloso) && $ONU_pericoloso!="") {
											$NOTEformularioADR.= ", ($ONU_pericoloso)";
											}

										if(strpos($FEDIT->DbRecordSet[0]["eti"], '5.2.2.1.12') !== false){
											$sql = "SELECT description FROM lov_num_onu_etichette WHERE ID_ETICHETTA_ADR=".$ID_ETICHETTA_ADR.";";
											$FEDIT->SDBRead($sql,"DbRecordSetEtichetta",true,false);
											$eti=$FEDIT->DbRecordSetEtichetta[0]["description"];
											$NOTEformularioADR.= ", " . $eti;
										}
										elseif(!is_null($FEDIT->DbRecordSet[0]["eti"]) && $FEDIT->DbRecordSet[0]["eti"]!="") {
											$etis = explode("+", $FEDIT->DbRecordSet[0]["eti"]);
											$other_etis = "";
											$oe = array();
											if(count($etis)>1){
												$other_etis.= " (";
												for($e=1;$e<count($etis);$e++){
													$oe[] = $etis[$e];
												}
												$other_etis.= implode(", ", $oe);
												$other_etis.= ")";
											}
											$eti = $etis[0].$other_etis;
											$NOTEformularioADR.= ", " . $eti;
										}

										if(!$ImballaggioVNR){
											$imb = $FEDIT->DbRecordSet[0]["imb"];
											if(!is_null($ID_IMBALLAGGIO_ADR) && $ID_IMBALLAGGIO_ADR>0){
												$sql = "SELECT description FROM lov_num_onu_imballaggio WHERE ID_IMBALLAGGIO_ADR=".$ID_IMBALLAGGIO_ADR.";";
												$FEDIT->SDBRead($sql,"DbRecordSetImballaggio",true,false);
												$imb = $FEDIT->DbRecordSetImballaggio[0]["description"];
											}
											if(!is_null($imb) && $imb!="") {
												$NOTEformularioADR.= ", " . $imb;
											}
										}

										// codice restrizione galleria
										if(!$ImballaggioVNR && $QL=="0")
											$NOTEformularioADR.= " " . $FEDIT->DbRecordSet[0]["codice_galleria"];

										// NOTE ADR (es. codice imballaggio)
										if(!is_null($ADR_NOTE) AND trim($ADR_NOTE)!=''){
											$NOTEformularioADR.=" - ".$ADR_NOTE." - ";
											}

										// CAP. 5.4.1.1.18 - PERICOLOSO PER L'AMBIENTE
										$ONU_ESCLUSI = array("3082", "3077");
										if(!in_array($ONUnum, $ONU_ESCLUSI) AND $FEDIT->DbRecordSet[0]["ADR_PERICOLOSO_AMBIENTE"]==1){
											$NOTEformularioADR.=" PERICOLOSO PER L'AMBIENTE. ";
											}

										// CAP. 2.1.3.5.5
										if($FEDIT->DbRecordSet[0]["ADR_2_1_3_5_5"]==1){
											$NOTEformularioADR.=" RIFIUTI CONFORMI AL CAP. 2.1.3.5.5 . ";
											}

										// Oli in ADR
										$CER_OLI = array("120106", "120107", "120110", "120119", "130101", "130109", "130110", "130111", "130112", "130113", "130204", "130205", "130206", "130207", "130208", "130301", "130306", "130307", "130308", "130309", "130310", "130401", "130402", "130403", "130506", "130701", "130702", "130703");
										$ONU_OLI = array("3082", "3077");
										if($SOGER->UserData['core_usersFRM_ADR_OLI']==1 AND in_array($Cer, $CER_OLI) AND in_array($ONUnum, $ONU_OLI)){
											$NOTEformularioADR.= " Classificazione ADR conforme al CAP. 2.1.3.1 e 2.1.3.9 . ";
											}

										// classe 6.2
										if($FEDIT->DbRecordSet[0]['classe']=='6.2'){
											## riferimento del produttore
											if($ID_UIMP<>'0000000'){
												$SQL_UIMP = "SELECT ONU_62 FROM user_impianti_produttori WHERE ID_UIMP=".$ID_UIMP.";";
												$FEDIT->SDBRead($SQL_UIMP,"DbRecordSet62UIMP",true,false);
												if(!is_null($FEDIT->DbRecordSet62UIMP[0]["ONU_62"]) && $FEDIT->DbRecordSet62UIMP[0]["ONU_62"]!='')
													$NOTEformularioADR.="Rifiuto classe 6.2 - Persona di riferimento per il produttore: ".$FEDIT->DbRecordSet62UIMP[0]["ONU_62"].". ";
											}
											## riferimento del trasportatore
											if($ID_UIMT<>'0000000'){
												$SQL_UIMT = "SELECT ONU_62 FROM user_impianti_trasportatori WHERE ID_UIMT=".$ID_UIMT.";";
												$FEDIT->SDBRead($SQL_UIMT,"DbRecordSet62UIMT",true,false);
												if(!is_null($FEDIT->DbRecordSet62UIMT[0]["ONU_62"]) && $FEDIT->DbRecordSet62UIMT[0]["ONU_62"]!='')
													$NOTEformularioADR.="Rifiuto classe 6.2 - Persona di riferimento per il trasportatore: ".$FEDIT->DbRecordSet62UIMT[0]["ONU_62"].". ";
											}
											## riferimento del destinatario
											if($ID_UIMD<>'0000000'){
												$SQL_UIMD = "SELECT ONU_62 FROM user_impianti_destinatari WHERE ID_UIMD=".$ID_UIMD.";";
												$FEDIT->SDBRead($SQL_UIMD,"DbRecordSet62UIMD",true,false);
												if(!is_null($FEDIT->DbRecordSet62UIMD[0]["ONU_62"]) && $FEDIT->DbRecordSet62UIMD[0]["ONU_62"]!='')
													$NOTEformularioADR.="Rifiuto classe 6.2 - Persona di riferimento per il destinatario: ".$FEDIT->DbRecordSet62UIMD[0]["ONU_62"].". ";
											}
											## riferimento del intermediario
											if($ID_UIMI<>'0000000'){
												$SQL_UIMI = "SELECT ONU_62 FROM user_impianti_intermediari WHERE ID_UIMI=".$ID_UIMI.";";
												$FEDIT->SDBRead($SQL_UIMI,"DbRecordSet62UIMI",true,false);
												if(!is_null($FEDIT->DbRecordSet62UIMI[0]["ONU_62"]) && $FEDIT->DbRecordSet62UIMI[0]["ONU_62"]!='')
													$NOTEformularioADR.="Rifiuto classe 6.2 - Persona di riferimento per l'intermediario: ".$FEDIT->DbRecordSet62UIMI[0]["ONU_62"].". ";
											}
										}


										if($QL=="1") {
											if(!$ImballaggioVNR){
												$NOTEformularioADR .= " Esente dal trasporto ADR per unit� di trasporto, Cap. 1.1.3.6, categoria di trasporto ".$CategoriaADR.". ";
												}
											else{
												$NOTEformularioADR .= " Esente dal trasporto ADR per unit� di trasporto, Cap. 1.1.3.6, categoria di trasporto 4. ";
												}
											}


										$NOTEformulario .= $NOTEformularioADR."\n";
									}
								}
							}

							if($UnMisura=="Mc.") {
								$NOTEformulario .= "Quantit�: $PesoUnMisura Mc.\n";
								}
							if($SOGER->UserData["core_usersFRM_PRINT_CER"]=="1") {
								$NOTEformulario .= "NOME CER: $CerDescrizione\n";
								}
							if(!is_null($ID_OP_RS2) && $ID_OP_RS2!="0" && $SOGER->UserData["core_usersOPRDsec"]=="1") {
								$NOTEformulario .= "Altra causale di recupero/smaltimento autorizzata: " . $ID_OP_RS2des."\n";
								}

							$FEDIT->FGE_PdfBuffer->SetXY(20+$Xcorr,122+$Ycorr);
							$FEDIT->FGE_PdfBuffer->MultiCell(170,5,$NOTEformulario,0,"L");
							//}}}
							#
							#	DESCRIZIONE RIFIUTO
							#
							#$FEDIT->FGE_PdfBuffer->SetXY(47+$Xcorr,169+$Ycorr);
							$FEDIT->FGE_PdfBuffer->SetXY(47+$Xcorr,165+$Ycorr);
							//$FEDIT->FGE_PdfBuffer->MultiCell(130,5,strtoupper($DesRifiuto),0,"L");
							$FEDIT->FGE_PdfBuffer->MultiCell(130,5,$DesRifiuto,0,"L");
							#
							#	CER
							#
							if( (3+$Xcorr)<0 )
								$XPosition = 1.5;
							else
								$XPosition = 3+$Xcorr;
							$FEDIT->FGE_PdfBuffer->SetXY($XPosition,178.5+$Ycorr);

							if($SOGER->UserData["core_usersCER_AXT"]=="1" && $Pericoloso=="1")
								$FEDIT->FGE_PdfBuffer->MultiCell(18,5,$Cer."*",0,"L");
							else
								$FEDIT->FGE_PdfBuffer->MultiCell(18,5,$Cer,0,"L");
							#
							#	STATO FISICO
							#
			//{{{
							switch($IDStatoFisico) {
								case "1":
								#$FEDIT->FGE_PdfBuffer->SetXY(71+$Xcorr,178+$Ycorr);
								$FEDIT->FGE_PdfBuffer->SetXY(73+$Xcorr,175+$Ycorr);
								break;
								case "2":
								$FEDIT->FGE_PdfBuffer->SetXY(77+$Xcorr,175+$Ycorr);
								break;
								case "3":
								$FEDIT->FGE_PdfBuffer->SetXY(83+$Xcorr,175+$Ycorr);
								break;
								case "4":
								$FEDIT->FGE_PdfBuffer->SetXY(87+$Xcorr,175+$Ycorr);###87 era 85###
								break;
							}
							$FEDIT->FGE_PdfBuffer->MultiCell(5,5,"X",0,"L");
							$FEDIT->FGE_PdfBuffer->SetXY(57+$Xcorr,178.5+$Ycorr);
							$FEDIT->FGE_PdfBuffer->MultiCell(35,5,$StatoFisico,0,"L"); //}}}

							#
							#	CLASSIFICAZIONE DI PERICOLO
							#
//							if(strtotime(date($DTFORM))>=strtotime('2015-06-01')){
								$FEDIT->FGE_PdfBuffer->SetXY(92+$Xcorr,178.5+$Ycorr);
								$FEDIT->FGE_PdfBuffer->MultiCell(70,5,$CLASSIHP,0,"L");
//								}
//							else{
//								$FEDIT->FGE_PdfBuffer->SetXY(92+$Xcorr,178.5+$Ycorr);
//								$FEDIT->FGE_PdfBuffer->MultiCell(70,5,$CLASSIH,0,"L");
//								}

							#
							#	RECUPERO / SMALTIMENTO
							#
							if(!is_null($ID_OP_RS) && $ID_OP_RS!="0") {
								$sql = "SELECT lov_operazioni_rs.description AS opRS FROM lov_operazioni_rs WHERE ID_OP_RS='$ID_OP_RS'";
								$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
								$OPRS =  $FEDIT->DbRecordSet[0]["opRS"];
								if($OPRS[0]=="R") {
									if( (1.5+$Xcorr)<0 ) $XPosition = 1.5; else	$XPosition = 1.5+$Xcorr;
									$FEDIT->FGE_PdfBuffer->SetXY($XPosition,191+$Ycorr);
								} else {
									$FEDIT->FGE_PdfBuffer->SetXY(19.2+$Xcorr,191+$Ycorr);
								}
								$FEDIT->FGE_PdfBuffer->MultiCell(5,5,"X",0,"L");
								$FEDIT->FGE_PdfBuffer->SetXY(40+$Xcorr,191+$Ycorr);
								$FEDIT->FGE_PdfBuffer->MultiCell(40,5,$OPRS,0,"L");
							} //}}}
							#
							#	CARATTERISTICHE CHIMICO FISICHE
							#
							#$FEDIT->FGE_PdfBuffer->SetXY(110+$Xcorr,195+$Ycorr);
							$FEDIT->FGE_PdfBuffer->SetXY(110+$Xcorr,191+$Ycorr);
							$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$Caratteristiche,0,"L");
							#
							#	QUANTITA
							#
			//{{{
							if($SOGER->UserData["core_usersFRM_DIS_QNT"]==0) {
								if($UnMisura=="Mc.") {
									# peso lordo
									$FEDIT->FGE_PdfBuffer->SetXY(9+$Xcorr,205+$Ycorr);
									$FEDIT->FGE_PdfBuffer->MultiCell(20,5,"0",0,"L");
									# tara
									$FEDIT->FGE_PdfBuffer->SetXY(9+$Xcorr,208+$Ycorr);
									$FEDIT->FGE_PdfBuffer->MultiCell(20,5,"0",0,"L");
									#
									$FEDIT->FGE_PdfBuffer->SetXY(30+$Xcorr,200+$Ycorr);				// segna X su kili
									$FEDIT->FGE_PdfBuffer->MultiCell(5,5,"X",0,"L");
									#
									$FEDIT->FGE_PdfBuffer->SetXY(38.5+$Xcorr,205+$Ycorr);			// peso convertito in kili
									$FEDIT->FGE_PdfBuffer->MultiCell(20,5,number_format_unlimited_precision($PesoNettoKg),0,"L");
								} else {
									if($UnMisura=="Kg.") {
										$FEDIT->FGE_PdfBuffer->SetXY(30+$Xcorr,200+$Ycorr);
										$FEDIT->FGE_PdfBuffer->MultiCell(5,5,"X",0,"L");
									}
									if($UnMisura=="Litri") {
										$FEDIT->FGE_PdfBuffer->SetXY(30+$Xcorr,204+$Ycorr); 		// 204 era 205 //
										$FEDIT->FGE_PdfBuffer->MultiCell(5,5,"X",0,"L");
									}
									# peso lordo
									$FEDIT->FGE_PdfBuffer->SetXY(9+$Xcorr,205+$Ycorr);
									$FEDIT->FGE_PdfBuffer->MultiCell(20,4,number_format_unlimited_precision($PesoLordoKg),0,"L");
									# tara
									$FEDIT->FGE_PdfBuffer->SetXY(9+$Xcorr,208+$Ycorr);
									$FEDIT->FGE_PdfBuffer->MultiCell(20,5,number_format_unlimited_precision($TaraKg),0,"L");
									# peso
									$FEDIT->FGE_PdfBuffer->SetXY(38+$Xcorr,205+$Ycorr);
									$FEDIT->FGE_PdfBuffer->MultiCell(20,5,number_format_unlimited_precision($PesoUnMisura),0,"L");
								}
								#
								if($VerificaDestino=="1") {
									$FEDIT->FGE_PdfBuffer->SetXY(30+$Xcorr,208+$Ycorr);				// 208 era 210 //
									$FEDIT->FGE_PdfBuffer->MultiCell(5,5,"X",0,"L");
								}
							} //}}}
							#
							#	PERCORSO
							#
							$FEDIT->FGE_PdfBuffer->SetXY(62+$Xcorr,204+$Ycorr);
							$FEDIT->FGE_PdfBuffer->MultiCell(80,5,$percorso,0,"L");
							#
							#	TRASPORTO ADR
							#
			//{{{
							if($adr=="1") {
								#$FEDIT->FGE_PdfBuffer->SetXY(166+$Xcorr,211+$Ycorr);
								$FEDIT->FGE_PdfBuffer->SetXY(166+$Xcorr,207+$Ycorr);
							} else {
								#$FEDIT->FGE_PdfBuffer->SetXY(179+$Xcorr,211+$Ycorr);
								$FEDIT->FGE_PdfBuffer->SetXY(179+$Xcorr,207+$Ycorr);
							}
							$FEDIT->FGE_PdfBuffer->MultiCell(5,5,"X",0,"L"); //}}}
							#
							#	AUTOMEZZO
							#
			//{{{
							if($ID_AUTOMZ!="0" && !is_null($ID_AUTOMZ)) {
								$sql = "SELECT user_automezzi.description FROM user_automezzi WHERE ID_AUTO='$ID_AUTOMZ'";
								$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
								$FEDIT->FGE_PdfBuffer->SetXY(83+$Xcorr,229+$Ycorr);
								$FEDIT->FGE_PdfBuffer->MultiCell(35,5,$FEDIT->DbRecordSet[0]["description"],0,"L");
							} //}}}
							#
							#	RIMORCHIO
							#
			//{{{
							if($ID_RMK!="0" && !is_null($ID_RMK)) {
								$sql = "SELECT user_rimorchi.description FROM user_rimorchi WHERE ID_RMK='$ID_RMK'";
								$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
								$FEDIT->FGE_PdfBuffer->SetXY(144+$Xcorr,229+$Ycorr);
								$FEDIT->FGE_PdfBuffer->MultiCell(35,5,$FEDIT->DbRecordSet[0]["description"],0,"L");
							} //}}}
							#
							#	CONDUCENTE
							#
			//{{{
							if($ID_AUTST!="0" && !is_null($ID_AUTST)) {
								$sql = "SELECT user_autisti.description,user_autisti.nome FROM user_autisti WHERE ID_AUTST='$ID_AUTST'";
								$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
								$FEDIT->FGE_PdfBuffer->SetXY(35+$Xcorr,234+$Ycorr);
								$FEDIT->FGE_PdfBuffer->MultiCell(80,5,$FEDIT->DbRecordSet[0]["description"] . " " . $FEDIT->DbRecordSet[0]["nome"],0,"L");
							} //}}}
							#
							#	DATA/ORA TRASPORTO
							#


						if($inTR_data!="0000-00-00" && !is_null($inTR_data)) {
							$FEDIT->FGE_PdfBuffer->SetXY(150+$Xcorr,234+$Ycorr);
							$dt = date(FGE_SCREEN_DATE, strtotime($inTR_data));
							$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$dt,0,"L");
						}
						if($inTR_ora!="00:00:00" && $inTR_ora!="00:00" && !is_null($inTR_ora)) {
							$FEDIT->FGE_PdfBuffer->SetXY(177.5+$Xcorr,234+$Ycorr);
							$ora = date("H:i",strtotime($inTR_ora));
							$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$ora,0,"L");
						}
					}

					}
				else { ## ovvero abbiamo $_GET['registro']

					# registro unico (p+d)
					if(isset($_GET['UnicoPD'])){
						include("../__includes/REG_unicoPDCODE.php");
						}

					# registro fanghi
					if(isset($_GET['Fanghi'])){
						include("../__includes/REG_fanghiCODE.php");
						}

					# registro fiscale
					if(isset($_GET['Fiscalizzato'])){

						# stampa registro produttore, trasportatore e destinatario
						if($SOGER->UserData["workmode"]!="intermediario"){
							include("../__includes/REG_fiscaleCODE.php");
							}

						# registro intermediario
						else{
							include("../__includes/REG_intermCODE.php");
							}

						}
/*
					# registro industriale
					if(isset($_GET['Industriale'])){

						# reg. cronologico
						if(isset($_GET['Cronologico'])){
							include("../__includes/REG_cronologicoCODE.php");
							}

						# reg. settimanale
						if(isset($_GET['Settimanale'])){
							include("../__includes/REG_settimanaleCODE.php");
							}

						}
*/
					}

			break;
		}
	if(!$deniedByConfig) {
		$FEDIT->FGE_PdfBuffer->Output($DcName.".pdf","D");
		unset($FEDIT->FGE_PdfBuffer);
		die();
	}
	break;
}

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");

header("location:../?" . $url);





function SfondoRegistro($XStart,$YStart,&$PdfBuffer) {
		$PdfBuffer->Image("Registro.png",$XStart,$YStart,203,79,"PNG");
}
function SfondoRegistroInt($XStart,$YStart,&$PdfBuffer) {
		$PdfBuffer->Image("RegistroInt.png",$XStart,$YStart,203,79,"PNG");
}


function DatiRegistro(&$Recordset,$x,$y,&$PdfBuffer, $LDP) {
//{{{
	global $SOGER,$FEDIT;
	# carico/scarico

	//print_r("Function called @ ".date('d-m-Y H:i:s')." for record NMOV ".$Recordset["NMOV"]."<br />");

	if($Recordset["TIPO"]=="S") {
		$PdfBuffer->SetXY($x+20,$y+1);
		$PdfBuffer->MultiCell(5,5,"X",0);
		}
	else {
		if($SOGER->UserData['workmode']=='trasportatore' && $SOGER->UserData["core_usersreg_trasp_doublecheck"]=="1"){
			$PdfBuffer->SetXY($x+20,$y+1);
			$PdfBuffer->MultiCell(5,5,"X",0);
			}
		$PdfBuffer->SetXY($x+20,$y+10);
		$PdfBuffer->MultiCell(5,5,"X",0);
		}
	# data movimento
	if(!is_null($Recordset["DTMOV"]) && $Recordset["DTMOV"]!="0000-00-00") {
		$DTMOV = $Recordset["DTMOV"];
		$PdfBuffer->SetXY($x+6,$y+18);
		$PdfBuffer->MultiCell(20,5,date("d/m/Y",strtotime($Recordset["DTMOV"])),0);
		}
	else
		$DTMOV = date('Y-m-d');

	# numero movimento
	$PdfBuffer->SetXY($x+6,$y+27);
	$PdfBuffer->MultiCell(20,5,$Recordset["NMOV"],0);
	# numero formulario
	if(!is_null($Recordset["NFORM"])) {
		$PdfBuffer->SetXY($x+5,$y+44);
		$PdfBuffer->MultiCell(25,5,$Recordset["NFORM"],0);
	}
	# data formulario
	if($Recordset["DTFORM"]!="0000-00-00" && !is_null($Recordset["DTFORM"]) && !is_null($Recordset["NFORM"])) {
		$PdfBuffer->SetXY($x+6,$y+52);
		$PdfBuffer->MultiCell(20,5,date("d/m/Y",strtotime($Recordset["DTFORM"])),0);
		}
	# rif movimenti di carico
	if(!is_null($Recordset["FKErifCarico"])) {
		$maxChar=28;
		if(strlen($Recordset["FKErifCarico"])<=$maxChar){
			$PdfBuffer->SetXY($x+4,$y+69);
			$otherC = '';
			$PdfBuffer->MultiCell(22,5,$Recordset["FKErifCarico"],0,"L");
			}
		else{
			$PdfBuffer->SetXY($x+4,$y+69);
			$otherC = $Recordset["FKErifCarico"];
			$PdfBuffer->MultiCell(22,5,"Vedi annotazioni",0,"L");
			}
		}
	else
		$otherC = '';

	# codice cer
	$PdfBuffer->SetXY($x+37,$y+6);
	if($SOGER->UserData["core_usersCER_AXT"]=="1" && $Recordset["pericoloso"]==1)
		$PdfBuffer->MultiCell(30,5,$Recordset["COD_CER"]."*",0);
	else
		$PdfBuffer->MultiCell(30,5,$Recordset["COD_CER"],0);
	# descrizione rifiuto
	$PdfBuffer->SetXY($x+30,$y+23);
	$PdfBuffer->MultiCell(45,4,$Recordset["descrizione"],0,"L");
	# stato fisico
	$PdfBuffer->SetXY($x+30,$y+48);
	$PdfBuffer->MultiCell(45,4,$Recordset["STFdes"],0,"L");
	# classi H
	$YEAR = substr($FEDIT->DbServerData["db"], -4);
	if($YEAR<2016){
		$CLASSIH = "";
		foreach($Recordset as $k=>$v) {
				if($k[0]=="H" && $k[1]!="P" && $v=="1") {
						$CLASSIH .= $k . " ";
				}
		}
	}
	# classi HP
	$CLASSIHP = "";
	foreach($Recordset as $k=>$v) {
		if($k[0]=="H" && $k[1]=="P" && $v=="1") {
			$CLASSIHP .= $k . " ";
		}
	}
	# COMPRESENZA HP4 / HP8
	if($SOGER->UserData['core_usersPRNT_HP4_HP8']==0){
		if (strpos($CLASSIHP, 'HP4') !== false && strpos($CLASSIHP, 'HP8') !== false) {
			$CLASSIHP = str_replace('HP4 ', '', $CLASSIHP);
		}
	}

	# CLASSIFICAZIONE DI PERICOLO
	if( strtotime(date($DTMOV)) < strtotime('2015-06-01') || ($YEAR<2016 && $Recordset["PerRiclassificazione"]=="1" && $Recordset["TIPO"]=="S" && trim($CLASSIH)!="") ){
		$CLASSIH_PRINTED = true;
		$PdfBuffer->SetXY($x+30,$y+61);
		$PdfBuffer->MultiCell(70,5,$CLASSIH,0,"L");
		}
	else{
		$CLASSIH_PRINTED = false;
		$PdfBuffer->SetXY($x+30,$y+61);
		$PdfBuffer->MultiCell(70,5,$CLASSIHP,0,"L");
		}

	# recupero / smaltimento
	# toppa ecostudio
	if(($Recordset["TIPO"]=="S" | $SOGER->UserData["core_usersREG_RS_CARICHI"]=="1") && !is_null($Recordset["OPRSdes"])) {
		if(strstr($Recordset["OPRSdes"],"R")) {
			$PdfBuffer->SetXY($x+65.4,$y+73.2);
			$PdfBuffer->MultiCell(5,5,"X",0);
			$PdfBuffer->SetXY($x+84.5,$y+73.2);
			$PdfBuffer->MultiCell(10,5,$Recordset["OPRSdes"],0,"L");
		} else {
			$PdfBuffer->SetXY($x+30,$y+73.2);
			$PdfBuffer->MultiCell(5,5,"X",0);
			$PdfBuffer->SetXY($x+53,$y+73.2);
			$PdfBuffer->MultiCell(10,5,$Recordset["OPRSdes"],0,"L");
		}

	}
	# annotazioni
	//if(!is_null($Recordset["NOTER"]) | !is_null($Recordset["VER_DESTINO"])) {

		$PdfBuffer->SetXY($x+168.5,$y+7);
		$note = "";

		# note registro scritte manualmente
		if(!is_null($Recordset["NOTER"]) AND trim($Recordset["NOTER"])!='') {
			$note .= $Recordset["NOTER"] . "\n";
			}

		# NOTE RELATIVE ALLA VECCHIA CLASSIFICAZIONE (H)
		if($SOGER->UserData["core_usersPRNT_CLASSIH"]=="1" && $YEAR<2016){
			if(trim($CLASSIH)!="" && strtotime(date($DTMOV))>=strtotime('2015-06-01') && !$CLASSIH_PRINTED){
				$note.="Classificazione secondo Direttiva 2008/98/CE: ".$CLASSIH."\n";
				}
			}

		if(!is_null($Recordset["NOTE_RIFIUTO"]) AND trim($Recordset["NOTE_RIFIUTO"])!='')
			$note .= $Recordset["NOTE_RIFIUTO"]."\n";

		# dati del produttore (solo se sono destinatario, su un movimento di carico e REG_PROD_NOTEC=1
		if($SOGER->UserData['workmode']=='destinatario' AND $Recordset["TIPO"]=="C" AND $SOGER->UserData["core_usersREG_PROD_NOTEC"]=="1"){
			$produttore = "PRODUTTORE: ".$Recordset["PRdes"] . "\n";
			$produttore .= $Recordset["IMdes"] . " - " . $Recordset["COMdes"];
			// se produttore estero, stampo nazione
			if($Recordset["COMproItaliano"]==1)
				$produttore .= " (" . $Recordset["COMproPR"] . ")\n";
			else
				$produttore .= " (" . $Recordset["COMproDesProv"] . ")\n";
			$note.=$produttore;
			}

		# commerciante
		if(!is_null($Recordset["INTdes"]) AND $Recordset["INTcommerciante"]==1 AND $Recordset["esclusoFIR"]==0) {
			$note.="COMMERCIANTE: \n";
			$note.=$Recordset["INTdes"]." - C.F.: ".$Recordset["INTcfisc"]."\n";
			$note.=$Recordset["INTind"]. " - " . $Recordset["COMintdes"] . " (" . $Recordset["COMintPR"] . ")\n";
			}

		# Allegato VII
		if(!is_null($Recordset["N_ANNEX_VII"]) AND trim($Recordset["N_ANNEX_VII"])!=''){
			$note .="Allegato VII n�: " . trim($Recordset["N_ANNEX_VII"]) . "\n";
			}

		# Numero di lotto
		if(!is_null($Recordset["lotto"]) AND trim($Recordset["lotto"])!=''){
			$note .="Lotto numero: " . trim($Recordset["lotto"]) . "\n";
			}

		# carichi di riferimento
		if($otherC!=""){
			if(strlen($otherC)>480) $otherC=str_replace(" ", "", $otherC);
			$note .="Riferimenti di carico: " . $otherC . "\n";
			}

		# peso a destino
		if(($Recordset["VER_DESTINO"]=="1" OR $SOGER->UserData['core_usersREG_PS_DEST']=="1") && !is_null($Recordset["PS_DESTINO"]) && trim($Recordset["PS_DESTINO"])!="" && (($Recordset["TIPO"]=="S" && $SOGER->UserData["workmode"]=="produttore") ) ){
			$note .="Peso a destino: " . number_format_unlimited_precision($Recordset["PS_DESTINO"])." Kg\n";
			}

		# nome cer
		if($SOGER->UserData["core_usersREG_NOMECER"]=="1") {
			$note .="Nome CER: ".$Recordset["NomeCER"]."\n";
			}

		# rifiuti da manutenzione
		if($Recordset["RifDaManutenzione"]=='1' AND $Recordset["TIPO"]=="S" AND $SOGER->UserData["workmode"]=="destinatario"){
			$note .="Rifiuto da attivit� di manutenzione delle infrastrutture (Art. 230 D.Lgs. 152/2006)";
			}

                $print_id_scheda = false;
                $print_numero_scheda = false;

		# ID della Scheda SISTRI Produttore
		if($Recordset["DTMOV"]>'2014-02-28' && $SOGER->UserData["workmode"]=="produttore" && $SOGER->UserData["core_impiantiMODULO_SIS"]=='1' && $SOGER->UserData["core_usersREG_IDSIS_SCHEDA"]=='1' && $Recordset["TIPO"]=="S" && !is_null($Recordset['idSIS_scheda']) && trim($Recordset['idSIS_scheda'])!='' ){
                    $print_id_scheda = true;
			if($note!="") $note .= "\n";
			$note .="Generata Scheda SISTRI Produttore id ".$Recordset['idSIS_scheda'];
			if(!is_null($Recordset["dataSchedaSistri_invio"]) && trim($Recordset["dataSchedaSistri_invio"])!=''){
				$DataInvioScheda = date("d/m/Y",strtotime($Recordset["dataSchedaSistri_invio"]));
				$note .=" del ".$DataInvioScheda;
				}
			}

                # Numero della Scheda SISTRI
		if($Recordset["DTMOV"]>'2014-02-28' && $SOGER->UserData["workmode"]=="produttore" && $SOGER->UserData["core_impiantiMODULO_SIS"]=='1' && $SOGER->UserData["core_usersREG_NUMERO_SCHEDA"]=='1' && $Recordset["TIPO"]=="S" && !is_null($Recordset['numero_scheda']) && trim($Recordset['numero_scheda'])!='' ){
                    $print_numero_scheda = true;
			if($note!="") $note .= "\n";
                    $note .="Generata Scheda SISTRI numero ".$Recordset['numero_scheda'];
                    if(!is_null($Recordset["dataSchedaSistri_invio"]) && trim($Recordset["dataSchedaSistri_invio"])!=''){
                        $DataInvioScheda = date("d/m/Y",strtotime($Recordset["dataSchedaSistri_invio"]));
                        $note .=" del ".$DataInvioScheda;
                    }
                }

		# Causale annullamento in Sistri
		if($Recordset["ANN_registrazione"]=='1' && ($SOGER->UserData["core_usersREG_IDSIS_SCHEDA"]=='1' || $SOGER->UserData["core_usersREG_NUMERO_SCHEDA"]=='1')){
                    if($note!="")
                        $note .= "\n";
			$note .="Registrazione cronologica SISTRI ".$Recordset['idSIS']." annullata. Motivazione: ".addslashes($Recordset["ANN_registrazione_causale"]);
			}

		if($Recordset["ANN_scheda"]=='1' && ($SOGER->UserData["core_usersREG_IDSIS_SCHEDA"]=='1' || $SOGER->UserData["core_usersREG_NUMERO_SCHEDA"]=='1')){
                    if($note!="")
                        $note .= "\n";
                    $note .="Scheda SISTRI ";
                    if($print_numero_scheda)
                        $note.= $Recordset['numero_scheda'] ." ";
                    if($print_id_scheda)
                        $note.= "(".$Recordset['idSIS_scheda'].") ";
                    $note.="annullata. Motivazione: ".addslashes($Recordset["ANN_scheda_causale"]);
			}

		# Modulo LDP: FIR di riferimento del registro destinatario
		if($LDP!='')
			$note.=$LDP;


		if(strlen($otherC)>480) 
			$h = 3;
		else
			$h = 4;
		$PdfBuffer->MultiCell(35,$h,$note,0,"L");

	# produttore
	if($SOGER->UserData["core_usersREG_DIS_PROD"]=="0" AND !is_null($Recordset["PRdes"])) {
		if( ($SOGER->UserData['workmode']=='destinatario' AND $SOGER->UserData["core_usersREG_PROD_NOTEC"]=="0") OR $SOGER->UserData['workmode']!='destinatario'){
		$produttore = $Recordset["PRdes"] . "\n";
		if($SOGER->UserData["core_intestatariID_INT"]!="001AMGIN")
			$produttore .= "Impianto di " . $Recordset["IMdes"] . " - " . $Recordset["COMdes"];
		else
			$produttore .= "Unit� locale: " . $Recordset["IMdes"] . " - " . $Recordset["COMdes"];
		// se produttore estero, stampo nazione
		if($Recordset["COMproItaliano"]==1)
			$produttore .= " (" . $Recordset["COMproPR"] . ")";
		else
			$produttore .= " (" . $Recordset["COMproDesProv"] . ")";
		$PdfBuffer->SetXY($x+100,$y+11);
		$PdfBuffer->MultiCell(65,4,$produttore,0,"L");
		}
	}


	# intermediario
	if(!is_null($Recordset["INTdes"]) AND $Recordset["INTcommerciante"]==0 AND $Recordset["esclusoFIR"]==0) {
		$PdfBuffer->SetXY($x+114,$y+40.5);
		$PdfBuffer->MultiCell(45,4,$Recordset["INTdes"],0,"L");
		$PdfBuffer->SetXY($x+105,$y+69.5);
		$PdfBuffer->MultiCell(45,4,$Recordset["INTcfisc"],0,"L");
		$sede = $Recordset["INTind"] . " - " . $Recordset["INTCAP"] . " - " . $Recordset["COMintdes"];
		// se intermediario estero, stampo nazione
		if($Recordset["COMintItaliano"]==1)
			$sede .= " (" . $Recordset["COMintPR"] . ")";
		else
			$sede .= " (" . $Recordset["COMintDesProv"] . ")";

		$PdfBuffer->SetXY($x+105,$y+57);
		$PdfBuffer->MultiCell(60,4,$sede,0,"L");

		if(!is_null($Recordset["INTaut"]) && $Recordset["DTMOV"]>'2011-04-16') {
			$PdfBuffer->SetXY($x+115,$y+74);
			$aut = $Recordset["INTaut"] . " del " . $Recordset["INTrilascio"];
			$PdfBuffer->MultiCell(50,4,$aut,0,"L");
		}
	}

	# quantita

	if($Recordset["peso_spec"]=="0")
		$Recordset["peso_spec"]="1";

	switch($Recordset["Umisura"]) { #
		case "1":	# scheda rifiuto in KG

			switch($SOGER->UserData["core_usersID_UMIS"]){ #unit� misura stampa registro
				case "garbage": # garbage = id 0, ogni riga del registro avr� l'unit� di misura della scheda rifiuto a cui fa riferimento.
				default:
				case "1": #kg
					$PdfBuffer->SetXY($x+80,$y+19);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L");
					break;

				case "2": #litri
					$PdfBuffer->SetXY($x+80,$y+36);
					$Q = KgQConvert($Recordset["quantita"],"Litri",$Recordset["peso_spec"]);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Q),0,"L");
					break;

				case "3": #mc
					$PdfBuffer->SetXY($x+80,$y+53);
					$Q = KgQConvert($Recordset["quantita"],"M3",$Recordset["peso_spec"]);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Q),0,"L");
					break;
				}

			if($SOGER->UserData["core_usersUMIS_RIF"]==1 && $Recordset["Umisura"] != $SOGER->UserData["core_usersID_UMIS"]){
				$PdfBuffer->SetXY($x+80,$y+19);
				$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L");
				}

		break;

		case "2":	# scheda rifiuto in LITRI
			switch($SOGER->UserData["core_usersID_UMIS"]){ #unit� misura stampa registro

				case "1": #kg
					$PdfBuffer->SetXY($x+80,$y+19);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]*$Recordset["peso_spec"]),0,"L");
					break;

				case "garbage":
				default:
				case "2": #litri
					$PdfBuffer->SetXY($x+80,$y+36);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L");
					break;

				case "3": #mc
					$PdfBuffer->SetXY($x+80,$y+53);
					if($Recordset["quantita"]!="0") {
						$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]/1000),0,"L");
					} else {
						$PdfBuffer->MultiCell(15,4,"0",0,"L");
					}
					break;

				}

			if($SOGER->UserData["core_usersUMIS_RIF"]==1 && $Recordset["Umisura"] != $SOGER->UserData["core_usersID_UMIS"]){
					$PdfBuffer->SetXY($x+80,$y+36);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L");
				}

		break;

		case "3":	# scheda rifiuto in METRI CUBI

			switch($SOGER->UserData["core_usersID_UMIS"]){ #unit� misura stampa registro
				case "1": #kg
					$PdfBuffer->SetXY($x+80,$y+19);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]*$Recordset["peso_spec"]*1000) ,0,"L");
					break;

				case "2": #litri
					$PdfBuffer->SetXY($x+80,$y+36);
					if($Recordset["quantita"]!="0") {
						$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]*1000),0,"L");
					} else {
						$PdfBuffer->MultiCell(15,4,"0",0,"L");
					}
					break;

				case "garbage":
				default:
				case "3": #mc
					$PdfBuffer->SetXY($x+80,$y+53);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L");
					break;

				}

			if($SOGER->UserData["core_usersUMIS_RIF"]==1 && $Recordset["Umisura"] != $SOGER->UserData["core_usersID_UMIS"]){
					$PdfBuffer->SetXY($x+80,$y+53);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L");
				}

		break;

	}
	//}}}
}


function DatiRegistroInt(&$Recordset,$x,$y,&$PdfBuffer,$ContaNMOV) {
//{{{
	global $SOGER,$FEDIT;

	# data movimento
	if(!is_null($Recordset["DTMOV"]) && $Recordset["DTMOV"]!="0000-00-00") {
		$PdfBuffer->SetXY($x+7,$y+13);
		$PdfBuffer->MultiCell(20,5,date("d/m/Y",strtotime($Recordset["DTMOV"])),0);
	}

	# numero movimento
	$PdfBuffer->SetXY($x+8,$y+21);
	$PdfBuffer->MultiCell(20,5,"Mov. ". $Recordset["NMOV"],0);

	# numero formulario
	if($Recordset["NFORM"]) {
		$PdfBuffer->SetXY($x+5,$y+33);
		$PdfBuffer->MultiCell(20,5,$Recordset["NFORM"],0);
	}

	# data formulario
	if(!is_null($Recordset["DTFORM"]) && $Recordset["DTFORM"]!="0000-00-00") {
		$PdfBuffer->SetXY($x+7,$y+41);
		$PdfBuffer->MultiCell(20,5,date("d/m/Y",strtotime($Recordset["DTFORM"])),0);
	}


	# codice cer
	$PdfBuffer->SetXY($x+47,$y+13);
	if($SOGER->UserData["core_usersCER_AXT"]=="1" && $Recordset["pericoloso"]==1)
		$PdfBuffer->MultiCell(30,5,$Recordset["COD_CER"]."*",0);
	else
		$PdfBuffer->MultiCell(30,5,$Recordset["COD_CER"],0);


	# descrizione rifiuto
	$PdfBuffer->SetXY($x+30,$y+21);
	$PdfBuffer->MultiCell(45,4,$Recordset["descrizione"],0,"L");

	# stato fisico
	$PdfBuffer->SetXY($x+30,$y+41);
	$PdfBuffer->MultiCell(45,4,$Recordset["STFdes"],0,"L");

	# classi HP
	$CLASSIHP = "";
	foreach($Recordset as $k=>$v) {
		if($k[0]=="H" && $k[1]=="P" && $v=="1") {
			$CLASSIHP .= $k . " ";
		}
	}
	# COMPRESENZA HP4 / HP8
	if($SOGER->UserData['core_usersPRNT_HP4_HP8']==0){
		if (strpos($CLASSIHP, 'HP4') !== false && strpos($CLASSIHP, 'HP8') !== false) {
			$CLASSIHP = str_replace('HP4 ', '', $CLASSIHP);
		}
	}

	# CLASSIFICAZIONE DI PERICOLO
	$PdfBuffer->SetXY($x+30,$y+53.5);
	$PdfBuffer->MultiCell(45,4,$CLASSIHP,0,"L");

	# recupero / smaltimento
	if(!is_null($Recordset["OPRSdes"])) {
		if(strstr($Recordset["OPRSdes"],"R")) {
			$PdfBuffer->SetXY($x+27.5,$y+73.5);
			$PdfBuffer->MultiCell(5,5,"X",0);
			$PdfBuffer->SetXY($x+60,$y+73.5);
			$PdfBuffer->MultiCell(10,5,$Recordset["OPRSdes"],0,"L");
		} else {
			$PdfBuffer->SetXY($x+27.5,$y+69.5);
			$PdfBuffer->MultiCell(5,5,"X",0);
			$PdfBuffer->SetXY($x+60,$y+69.5);
			$PdfBuffer->MultiCell(10,5,$Recordset["OPRSdes"],0,"L");
		}

	}

	# produttore
	if($SOGER->UserData["core_usersREG_DIS_PROD"]=="0") {
		$PdfBuffer->SetXY($x+90,$y+9);
		$PdfBuffer->MultiCell(50,4,$Recordset["PRdes"],0,"L");
		$PdfBuffer->SetXY($x+93,$y+17);
		$PdfBuffer->MultiCell(40,4,$Recordset["PRCF"],0,"L");
		$PdfBuffer->SetXY($x+90,$y+25);
		$PdfBuffer->MultiCell(50,4,$Recordset["IMPRdes"] . " - " . $Recordset["COMPRdes"] . " (" . $Recordset["COMPRPR"] . ")",0,"L");
		}

	# destinatario
	$PdfBuffer->SetXY($x+149,$y+9);
	$PdfBuffer->MultiCell(50,4,$Recordset["DEdes"],0,"L");
	$PdfBuffer->SetXY($x+152,$y+17);
	$PdfBuffer->MultiCell(40,4,$Recordset["DECF"],0,"L");
	$PdfBuffer->SetXY($x+149,$y+25);
	$PdfBuffer->MultiCell(50,4,$Recordset["IMDEdes"] . " - " . $Recordset["COMDEdes"] . " (" . $Recordset["COMDEPR"] . ")",0,"L");

	# trasportatore
	$PdfBuffer->SetXY($x+90,$y+45);
	$PdfBuffer->MultiCell(50,4,$Recordset["TRdes"],0,"L");
	$PdfBuffer->SetXY($x+93,$y+58);
	$PdfBuffer->MultiCell(40,4,$Recordset["TRCF"],0,"L");
	$PdfBuffer->SetXY($x+90,$y+66);
	$PdfBuffer->MultiCell(50,4,$Recordset["IMTRdes"] . " - " . $Recordset["COMTRdes"] . " (" . $Recordset["COMTRPR"] . ")",0,"L");


	# annotazioni
	$PdfBuffer->SetXY($x+145,$y+45);
	$note = "";

	if(!is_null($Recordset["PS_DESTINO"]) && trim($Recordset["PS_DESTINO"])!="")
		$note .="Peso a destino: " . number_format_unlimited_precision($Recordset["PS_DESTINO"])." Kg \n";

	if(!is_null($Recordset["NOTER"]))
		$note .= $Recordset["NOTER"]."\n";

        # NOTE RELATIVE ALLA VECCHIA CLASSIFICAZIONE (H)
        if($SOGER->UserData["core_usersPRNT_CLASSIH"]=="1" && $YEAR<2016){
                if(trim($CLASSIH)!="" && strtotime(date($DTMOV))>=strtotime('2015-06-01') && !$CLASSIH_PRINTED){
                        $note.="Classificazione secondo Direttiva 2008/98/CE: ".$CLASSIH."\n";
                        }
                }

	if(!is_null($Recordset["NOTE_RIFIUTO"]))
		$note .= $Recordset["NOTE_RIFIUTO"]."\n";

	if($SOGER->UserData["core_usersREG_NOMECER"]=="1")
		$note .="Nome CER: ".$Recordset["NomeCER"]."\n";

	$PdfBuffer->MultiCell(55,4,$note,0,"L");

	# quantita

	if($Recordset["peso_spec"]=="0")
		$Recordset["peso_spec"]="1";

	switch($Recordset["Umisura"]) { #
		case "1":	# scheda rifiuto in KG

			switch($SOGER->UserData["core_usersID_UMIS_INT"]){ #unit� misura stampa registro
				case "garbage": # garbage = id 0, ogni riga del registro avr� l'unit� di misura della scheda rifiuto a cui fa riferimento.
				default:
				case "1": #kg
					$PdfBuffer->SetXY($x+75,$y+17);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L");
					break;

				case "2": #litri
					$PdfBuffer->SetXY($x+75,$y+32);
					$Q = KgQConvert($Recordset["quantita"],"Litri",$Recordset["peso_spec"]);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Q),0,"L");
					break;
				}

			if($SOGER->UserData["core_usersUMIS_RIF"]==1 && ($Recordset["Umisura"] != $SOGER->UserData["core_usersID_UMIS_INT"])){
				$PdfBuffer->SetXY($x+75,$y+17);
				$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L");
				}

		break;

		case "2":	# scheda rifiuto in LITRI
			switch($SOGER->UserData["core_usersID_UMIS_INT"]){ #unit� misura stampa registro

				case "1": #kg
					$PdfBuffer->SetXY($x+75,$y+17);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]*$Recordset["peso_spec"]),0,"L");
					break;

				case "garbage":
				default:
				case "2": #litri
					$PdfBuffer->SetXY($x+75,$y+32);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L");
					break;

				}

			if($SOGER->UserData["core_usersUMIS_RIF"]==1 && $Recordset["Umisura"] != $SOGER->UserData["core_usersID_UMIS_INT"]){
					$PdfBuffer->SetXY($x+75,$y+32);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]),0,"L");
				}

		break;

		case "3":	# scheda rifiuto in METRI CUBI

			switch($SOGER->UserData["core_usersID_UMIS_INT"]){ #unit� misura stampa registro
				case "1": #kg
					$PdfBuffer->SetXY($x+75,$y+17);
					$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]*$Recordset["peso_spec"]*1000),0,"L");
					break;

				case "garbage":
				default:
				case "2": #litri
					$PdfBuffer->SetXY($x+75,$y+32);
					if($Recordset["quantita"]!="0") {
						$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]*1000),0,"L");
					} else {
						$PdfBuffer->MultiCell(15,4,"0",0,"L");
					}
					break;

				}

			if($SOGER->UserData["core_usersUMIS_RIF"]==1 && $Recordset["Umisura"] != $SOGER->UserData["core_usersID_UMIS_INT"]){
					$PdfBuffer->SetXY($x+75,$y+32);
					if($Recordset["quantita"]!="0") {
						$PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($Recordset["quantita"]*1000),0,"L");
					} else {
						$PdfBuffer->MultiCell(15,4,"0",0,"L");
					}
				}

		break;

	}

}


function delTree($dir) {
   $files = array_diff(scandir($dir), array('.','..'));
    foreach ($files as $file) {
      (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
    }
    return rmdir($dir);
  }



function FormSoggetto($soggetto,$Ystart,$Azienda,$Imp=null,$CodFisc,$Piva,$AutorBool,$Telefono,$Email,$NumAlboAutotrasp,&$PdfBuffer,$AutNum=null,$AuthRil=null) {
//{{{
	global $Xcorr,$Ycorr;
	# azienda
	$Y = $Ystart+$Ycorr;
	$PdfBuffer->SetXY(37+$Xcorr,$Y);
	$PdfBuffer->MultiCell(130,5,strtoupper($Azienda),0);

	if($NumAlboAutotrasp!="" AND $soggetto=="trasportatore" AND $SOGER->UserData['core_usersFRM_PRINT_CONTO_TERZI']==1){
		$PdfBuffer->SetXY(70+$Xcorr,$Y+12.75);
		$PdfBuffer->MultiCell(166,5,"Numero iscrizione Albo Autotrasportatori: ".$NumAlboAutotrasp,0);
		}

	# impianto
	$Y+= 8.5;
	if(!is_null($Imp)) {
		if($soggetto!="destinatario") {
			$PdfBuffer->SetXY(15.5+$Xcorr,$Y);
		} else {
			$PdfBuffer->SetXY(25.5+$Xcorr,$Y);
		}
		$PdfBuffer->MultiCell(155,5,$Imp,0);
	}
	# codice fiscale
	$Y+= 8.5;
	$PdfBuffer->SetXY(22+$Xcorr,$Y);
	$PdfBuffer->MultiCell(130,4,strtoupper($CodFisc),0);
	# autorizzazione
		if(!is_null($AutNum)) {
			$PdfBuffer->SetXY(122.5+$Xcorr,$Y);
			$PdfBuffer->MultiCell(35,5,$AutNum,0);
			$PdfBuffer->SetXY(165.5+$Xcorr,$Y);
			$PdfBuffer->MultiCell(25+$Xcorr,5,date(FGE_SCREEN_DATE, strtotime($AuthRil)),0);
		} //}}}
}
?>