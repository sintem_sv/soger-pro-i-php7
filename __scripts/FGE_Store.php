<?php

session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__libs/SQLFunct.php");
global $SOGER;

if($FEDIT->MakeFormHash()!=$_POST["hash"]) {
	$SOGER->AppLocation="UserMainMenu";
	$SOGER->SetFeedback("Access Denied","1");
	require_once("../__includes/COMMON_sleepSoger.php");
	header("location: ../");
	}

$InsInfo = explode(":",$_POST["FGE_PrimaryKeyRef"]);
$TableI  = explode(":",$_POST["FGE_PrimaryKeyRef"]);


switch($TableI[0]) {

	case "core_impianti":
		$s = "SELECT * FROM core_impianti WHERE ID_IMP='" . $_POST["core_impianti:ID_IMP"] . "'";
		$FEDIT->SDBRead($s,"DBRecordSet");

		if($FEDIT->DbRecsNum==0) {
                    $FEDIT->SDBWrite($FEDIT->FGE_SQL_MakeInsert(),true,false);
                    $_SESSION["FGE_IDs"][$InsInfo[0]] = $_POST["core_impianti:ID_IMP"];
                    $label = "salvato";

                    // creo le cartelle che conterranno i documenti generati automaticamente
                    mkdir("../__documents/__reg/".$_POST["core_impianti:ID_IMP"], 0777);
                    mkdir("../__documents/__alarm/".$_POST["core_impianti:ID_IMP"], 0777);
                    mkdir("../__documents/__adr/".$_POST["core_impianti:ID_IMP"], 0777);

                    // creo le utenze Sintem per la manutenzione
                    $sql_user = "INSERT INTO `core_users` (`usr`, `superpwd`, `pwd`, `nuovapwd`, `nuovapwd_confirm`, `SIS_identity`, `SIS_Nome_Delegato`, `SIS_Cognome_Delegato`, `idSIS_sede`, `sede_description`, `idSIS_regCrono`, `regCrono_type`, `manutenzione`, `isSintem`, `produttore`, `trasportatore`, `destinatario`, `intermediario`, `description`, `nome`, `cognome`, `email`, `telefono`, `sadmin`, `sresponsabile`, `ID_IMP`, `O1`, `O2`, `O3`, `O4`, `O5`, `O6`, `O7`, `O8`, `O9`, `O10`, `O11`, `O12`, `G1`, `G2`, `G3`, `G4`, `G5`, `G6`, `G7`, `G8`, `G9`, `G10`, `G11`, `G12`, `G13`, `D1`, `D2`, `D3`, `D4`, `D5`, `D6`, `D7`, `R1`, `R2`, `R3`, `R4`, `R5`, `R6`, `R7`, `MAP_showInd`, `mov_fiscalize_merge`, `mov_fiscalize_order`, `mov_fiscalize_rif`, `reg_trasp_doublecheck`, `ck_ADR`, `ck_validazione_conferimento`, `ck_AUT`, `ck_CNTR`, `ck_AUTgg`, `ck_ANALISI`, `ck_ANALISIgg`, `ck_MOV_SISTRI`, `ck_MOV_SISTRIgg`, `ck_DEP_TEMP`, `ck_DEP_TEMPgg`, `q_limite`, `q_limite_p`, `q_limite_t`, `ck_RIT_FORM`, `ck_RIT_FORMgg`,  `NMOVDEF`, `NMOVSHOW`, `via_diretta`, `FRM_DefaultVD`, `FRM_DIS_PRO`, `FRM_DIS_DES`, `FRM_DIS_TRA`, `FRM_DIS_INT`, `FRM_DIS_QNT`, `FRM_PRINT_CER`, `FRM_PRNT_X`, `FRM_PRNT_Y`, `FRM_FONT_ID`, `FRM_FONT_SIZE_ID`, `FRM_ADR`, `FRM_ADR_OLI`, `FRM_SCHEDA_TRASP`, `FRM_SET_HR`, `FRM_SET_DATE`, `ID_ST_DET`, `ID_COMM`, `ID_CAR`, `REG_PRNT_X`, `REG_PRNT_Y`, `REG_DIS_PROD`, `REG_QNT_M`, `REG_QNT_L`, `REG_NOMECER`, `REG_RS_CARICHI`, `REG_PS_DEST`, `REG_IDSIS_SCHEDA`, `REG_NUMERO_SCHEDA`, `CER_AXT`, `OPRDsec`, `AUT_SCAD_LOCK`, `FDA_LOCK`, `CONTO_TERZI_LOCK`, `CONTO_TERZI_CHECK`, `CHECK_DESTINATARIO`, `CHECK_TRASPORTATORE`, `TargheOTF`, `AutistaOTF`, `ID_UMIS`, `ID_UMIS_INT`, `UMIS_RIF`, `FANGHI`, `ID_FISC_PROD`, `ID_FISC_DEST`, `ID_FISC_TRASP`, `ID_FISC_INTERM`, `ID_DAY`, `ID_GDEP`, `MEMO_registro`, `MEMO_registro_mail`, `NOTER_SISTRI`, `NOTEF_SISTRI`, `approved`) VALUES('sintem', NULL, '".$_POST["core_impianti:ID_IMP"]."', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 0, 1, 1, 1, 1, 1, 'Ufficio Tecnico Sintem', 'Ufficio', 'Tecnico', '', '', 0, 0, '".$_POST["core_impianti:ID_IMP"]."', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 80, 1, 20, 1, 5, 1, 120, 20, 10, 30, 1, 30, 'AAA-000000/07', 200, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 2, 1, 1, 1, 1, 1, 1, 2, 1, 2, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 5, 1, 1, '', 0, 0, 1);";
                    $FEDIT->SDBWrite($sql_user,true,false);
                }
		else {
                    $label = "modificato";
                    $_SESSION["FGE_IDs"][$InsInfo[0]] = $_POST["core_impianti:ID_IMP"];
                    $FEDIT->SDBWrite($FEDIT->FGE_SQL_MakeUpdate(),true,false);
                }
	break;

	case "core_intestatari":
		$s = "SELECT * FROM core_intestatari WHERE ID_INT='" . $_POST["core_intestatari:ID_INT"] . "'";
		$FEDIT->SDBRead($s,"DBRecordSet");
		if($FEDIT->DbRecsNum==0) {
			$FEDIT->SDBWrite($FEDIT->FGE_SQL_MakeInsert(),true,false);
			$_SESSION["FGE_IDs"][$InsInfo[0]] = $_POST["core_intestatari:ID_INT"];
			$label = "salvato";
			}
		else {
			$label = "modificato";
			$_SESSION["FGE_IDs"][$InsInfo[0]] = $_POST["core_intestatari:ID_INT"];
			$FEDIT->SDBWrite($FEDIT->FGE_SQL_MakeUpdate(),true,false);
			}
	break;

	case "core_gruppi":
		$s = "SELECT * FROM core_gruppi WHERE ID_GRP='" . $_POST["core_gruppi:ID_GRP"] . "'";
		$FEDIT->SDBRead($s,"DBRecordSet");
		if($FEDIT->DbRecsNum==0) {
			$FEDIT->SDBWrite($FEDIT->FGE_SQL_MakeInsert(),true,false);
			$_SESSION["FGE_IDs"][$InsInfo[0]] = $_POST["core_gruppi:ID_GRP"];
			$label = "salvato";
			}
		else {
			$label = "modificato";
			$_SESSION["FGE_IDs"][$InsInfo[0]] = $_POST["core_gruppi:ID_GRP"];
			$FEDIT->SDBWrite($FEDIT->FGE_SQL_MakeUpdate(),true,false);
			}
	break;

	##

        case "user_autorizzazioni_pro":
	case "user_autorizzazioni_trasp":
	case "user_autorizzazioni_dest":
	case "user_autorizzazioni_interm":
		require('INHERIT_EDITS_Autorizzazioni.php');
		break;

	case "user_automezzi":
	case "user_rimorchi":
		require('INHERIT_EDITS_Targhe.php');
		break;

	case "user_schede_sicurezza":
		$SQL="UPDATE user_schede_rifiuti SET prescrizioni_mov='".addslashes($_POST["user_schede_sicurezza:prescrizioni_mov"])."' WHERE ID_RIF=".$_POST["user_schede_sicurezza:ID_RIF"];

		$s = "SELECT * FROM user_schede_sicurezza WHERE ID_SK='" . $_POST["user_schede_sicurezza:ID_SK"] . "'";

		$FEDIT->SDBRead($s,"DBRecordSet");
		if($FEDIT->DbRecsNum==0){
			$FEDIT->SDBWrite($FEDIT->FGE_SQL_MakeInsert(),true,false);
			}
		else{
			$FEDIT->SDBWrite($FEDIT->FGE_SQL_MakeUpdate(),true,false);
			}
		$_SESSION["FGE_IDs"][$InsInfo[0]] = $_POST["user_schede_sicurezza:ID_SK"];
		$label = "salvato";
		$FEDIT->SDBWrite($SQL,true,false);
		break;

	## END


	default:

	if($_POST[$_POST["FGE_PrimaryKeyRef"]]!="") {

			#
			# bugfix per CAR_DataAnalisi che quando hidden viene convertita in formato non scrivibile su db
			# deve funzionare solo quando � hidden.. quando � hidden ho "-" come separatore, altrimenti ho "/"
			#
			if(strpos($TableI[0], "user_movimenti")!==false && strpos($_POST[$TableI[0].":CAR_DataAnalisi"], "/")===false){
				$CAR_DataAnalisi			= $_POST[$TableI[0].":CAR_DataAnalisi"];
				$CAR_DataAnalisi_array		= explode("-", $CAR_DataAnalisi);
				$CAR_DataAnalisiToConvert	= $CAR_DataAnalisi_array[2]."/".$CAR_DataAnalisi_array[1]."/".$CAR_DataAnalisi_array[0];
				$_POST[$TableI[0].":CAR_DataAnalisi"] = $CAR_DataAnalisiToConvert;
				}


			if($TableI[0]=='user_schede_rifiuti'){
                                // Se non ho compilato "Famiglie di sostanze classificate pericolose presenti nel rifiuto", lo setto uguale a "ADR: componenti pericolosi"
                                if(trim($_POST['user_schede_rifiuti:et_comp_per'])=='')
                                    $_POST['user_schede_rifiuti:et_comp_per'] = $_POST['user_schede_rifiuti:ONU_per'];
				// Ci sono campi della scheda rifiuto che sono ripresi in maniera identica in user_schede_rifiuti_deposito (solo in lettura)
				// Quando li variamo dobbiamo aggiornare anche user_schede_rifiuti_deposito..
				$SQL="SELECT ID_DEP FROM user_schede_rifiuti_deposito WHERE ID_RIF=".$_POST[$_POST["FGE_PrimaryKeyRef"]].";";
				$FEDIT->SDbRead($SQL,"DbRecordSet",true,false);
				if(count($FEDIT->DbRecordSet)==1){
					$SQL="UPDATE user_schede_rifiuti_deposito SET ID_UMIS=".$_POST['user_schede_rifiuti:ID_UMIS'].", ID_SF=".$_POST['user_schede_rifiuti:ID_SF'].", peso_spec='".$_POST['user_schede_rifiuti:peso_spec']."' WHERE ID_DEP=".$FEDIT->DbRecordSet[0]['ID_DEP'].";";
					$FEDIT->SDBWrite($SQL,true,false);
					}
                                // Ci sono campi della scheda rifiuto che sono ripresi in maniera identica in user_schede_rifiuti_pittogrammi (solo in lettura)
				// Quando li variamo dobbiamo aggiornare anche user_schede_rifiuti_pittogrammi
                                for($h=1;$h<=15;$h++){
                                    if($_POST['user_schede_rifiuti:HP'.$h]=='1')
                                        $HP_CLASSES.="HP".$h." ";
                                    }
                                $SQL="UPDATE user_schede_rifiuti_pittogrammi SET HP_CLASSES='".$HP_CLASSES."' WHERE ID_RIF=".$_POST[$_POST["FGE_PrimaryKeyRef"]].";";
				$FEDIT->SDBWrite($SQL,true,false);
				}

			#
			#	pezza soger (preferenze modificate al volo)
			#
			if($TableI[0]=="core_users") {
				foreach($_POST as $k=>$v) {
					$k=str_replace(":","",$k);
					if(isset($SOGER->UserData[$k])) {
						$SOGER->UserData[$k] = $v;
						}
					}
				}
			#
			#	fine pezza
			#


			#
			#	GESTIONE DEPOSITO, ALLINEO I DUE CAMPI MAX STOCK
			#

			if($SOGER->AppLocation=='UserRifiutoContenitore'){
				$_POST['user_schede_rifiuti_deposito:MaxStockClone'] = $_POST['user_schede_rifiuti_deposito:MaxStock'];
				}


			#
			#	PROPAGAZIONE DEI CONTROLLI A TUTTI GLI UTENTI DELLO STESSO IMPIANTO
			#

			if($SOGER->AppLocation=="UserConfigControlli"){
				$SQL ="UPDATE core_users SET ";

				$SQL.="AUT_SCAD_LOCK='".$_POST['core_users:AUT_SCAD_LOCK']."', ";
                                if($SOGER->UserData['core_impiantiMODULO_FDA']==1)
                                    $SQL.="FDA_LOCK='".$_POST['core_users:FDA_LOCK']."', ";
				$SQL.="CONTO_TERZI_LOCK='".$_POST['core_users:CONTO_TERZI_LOCK']."', ";

				$SQL.="CONTO_TERZI_CHECK='".$_POST['core_users:CONTO_TERZI_CHECK']."', ";
				$SQL.="CHECK_DESTINATARIO='".$_POST['core_users:CHECK_DESTINATARIO']."', ";
				$SQL.="CHECK_TRASPORTATORE='".$_POST['core_users:CHECK_TRASPORTATORE']."', ";
				$SQL.="CHECK_MOV_ORDER='".$_POST['core_users:CHECK_MOV_ORDER']."', ";
				$SQL.="CHECK_DOUBLE_FIR='".$_POST['core_users:CHECK_DOUBLE_FIR']."', ";

				$SQL.="ID_GDEP='".$_POST['core_users:ID_GDEP']."', ";
				$SQL.="GGedit_{$SOGER->UserData['workmode']}='".$_POST['core_users:GGedit_'.$SOGER->UserData["workmode"]]."', ";
				$SQL.="RESTORE_NMOV='".$_POST['core_users:RESTORE_NMOV']."', ";
				$SQL.="FRM_pericolosi='".$_POST['core_users:FRM_pericolosi']."', ";

				if(isset($_POST['core_users:FRM_LAYOUT_DEST']))
					$SQL.="FRM_LAYOUT_DEST='".$_POST['core_users:FRM_LAYOUT_DEST']."', ";
				$SQL.="FRM_LAYOUT_SCARICO_INTERNO='".$_POST['core_users:FRM_LAYOUT_SCARICO_INTERNO']."'";

				if($SOGER->UserData['core_impiantiREG_IND']==1){

					$SQL.=", ";

					if($_POST['core_users:ID_DAY']!='garbage')
						$SQL.="ID_DAY='".$_POST['core_users:ID_DAY']."', ";
					else
						$SQL.="ID_DAY='0', ";

					if($SOGER->UserData['workmode']=="produttore"){
						if($_POST['core_users:ID_FISC_PROD']!='garbage')
							$SQL.="ID_FISC_PROD='".$_POST['core_users:ID_FISC_PROD']."', ";
						else
							$SQL.="ID_FISC_PROD='0', ";
						}

					if($SOGER->UserData['workmode']=="destinatario"){
						if($_POST['core_users:ID_FISC_DEST']!='garbage')
							$SQL.="ID_FISC_DEST='".$_POST['core_users:ID_FISC_DEST']."', ";
						else
							$SQL.="ID_FISC_DEST='0', ";
						}

					if($SOGER->UserData['workmode']=="trasportatore"){
						if($_POST['core_users:ID_FISC_TRASP']!='garbage')
							$SQL.="ID_FISC_TRASP='".$_POST['core_users:ID_FISC_TRASP']."', ";
						else
							$SQL.="ID_FISC_TRASP='0', ";
						}

					if($SOGER->UserData['workmode']=="intermediario"){
						if($_POST['core_users:ID_FISC_INTERM']!='garbage')
							$SQL.="ID_FISC_INTERM='".$_POST['core_users:ID_FISC_INTERM']."', ";
						else
							$SQL.="ID_FISC_INTERM='0', ";
						}

					$SQL.="D2='".$_POST['core_users:D2']."', ";
					$SQL.="mov_fiscalize_merge='".$_POST['core_users:mov_fiscalize_merge']."', ";
					$SQL.="mov_fiscalize_order='".$_POST['core_users:mov_fiscalize_order']."', ";
					$SQL.="PRNT_HP4_HP8='".$_POST['core_users:PRNT_HP4_HP8']."' ";
				}


				$SQL.=" WHERE ID_IMP='".$_POST['core_users:ID_IMP']."'";
				//die($SQL);
				$FEDIT->SDBWrite($SQL,true,false);


				## QUANDO CAMBIO GESTIONE DEPOSITO, METTO NULL I LIMITI DELLA MODALITA' NON UTILIZZATA E AL VALORE DI DEFAULT I LIMITI DELLA MODALITA' SCELTA
				}

			if($SOGER->AppLocation=="UserConfigStampe"){
				$_SESSION["MovFilter"]["LIMIT"] = $_POST['core_users:NMOVSHOW'];
				}

			#
			#	Leggo precedente ID_RIF per controlli su FKEdisponibilita e reg. ind.
			#
//			if($TableI[0] == "user_movimenti_fiscalizzati"){
//				$sql ="SELECT ID_RIF from user_movimenti_fiscalizzati WHERE ID_MOV='".$_POST['user_movimenti_fiscalizzati:ID_MOV']."';";
//				$FEDIT->SdbRead($sql,"DbRecordSet",true,false);
//				$PREV_ID_RIF=$FEDIT->DbRecordSet[0]["ID_RIF"];
//				}


			#
			#	LOGS
			#

			$UpdSql = $FEDIT->FGE_SQL_MakeUpdate();
			$LogSql  = "INSERT INTO core_logs ";
                        $LogSql .= "(`table` , `pri_key` , `ID_USR` , `ID_IMP` , `sqlType` , `sqlCommand` ) ";
                        $LogSql .= "VALUES ";
			$LogSql .= "('" . $TableI[0] . "','" .  $_POST[$_POST["FGE_PrimaryKeyRef"]] . "','";
			$LogSql .= $SOGER->UserData["core_usersID_USR"] . "','";
			$LogSql .= $SOGER->UserData["core_usersID_IMP"] . "','modifica','" . addslashes($UpdSql) . "')";
			$FEDIT->SDBWrite($LogSql,true,false);
			$FEDIT->SDBWrite($UpdSql,true,false);

			#
			#	SOGER ECO intercetta gli AGGIORNAMENTI sulla tabella movimenti per i costi
			#
			if($TableI[0] == "user_movimenti_fiscalizzati" && $SOGER->UserData["core_impiantiMODULO_ECO"]=="1" && $FEDIT->DbInsEdtNum!==0 ) {
				require("../__libs/SogerECO_funct.php");
				$MovID = $_POST[$_POST["FGE_PrimaryKeyRef"]];
				$impianto = $SOGER->UserData["core_impiantiID_IMP"];
				$resultArray = array();
				$sql = "DELETE FROM user_movimenti_costi WHERE ID_MOV_F='$MovID' AND FKE_UM <> 'VRB';";
				$FEDIT->SDBWrite($sql,true,false);
				$sql = "SELECT quantita, ID_AZI, ID_UIMD, DTMOV FROM user_movimenti_fiscalizzati WHERE ID_MOV_F='$MovID'";
				$FEDIT->SDbRead($sql,"DbRecordSet",true,false);
				if($FEDIT->DbRecordSet[0]["quantita"]!="0") {
					$ID_AZI		= $FEDIT->DbRecordSet[0]["ID_AZI"];
					$ID_UIMD	= $FEDIT->DbRecordSet[0]["ID_UIMD"];
					$DTMOV		= $FEDIT->DbRecordSet[0]["DTMOV"];
					$updCosti	= true;
					require("../__libs/SogerECO_GeneraCosti.php");
					}
				}

		#
		# se sto lavorando sul registro industriale e correggo una qta, aggiorno FKEdisponibilita dei movimenti successivi
		#

		if($TableI[0]=="user_movimenti_fiscalizzati"){
			## AGGIORNO FKEdisponibilita dei movimenti successivi
			// select NMOV
			$sql="SELECT NMOV FROM user_movimenti_fiscalizzati WHERE ID_MOV_F='".$_POST['user_movimenti_fiscalizzati:ID_MOV_F']."'";
			$FEDIT->SdbRead($sql,"DbRecordSet",true,false);
			$NMOV=$FEDIT->DbRecordSet[0]["NMOV"];

			## Devo aggiornare FKEdisponibilita se:
			## - cambio quantit�		$Diff!=0
			## - cambio scheda rifiuto	$PREV_ID_RIF!=$_POST['user_movimenti:ID_RIF']
			## - cambio entrambi

			$Diff=$_POST['user_movimenti_fiscalizzati:quantita'] - $_POST['user_movimenti_fiscalizzati:qta_hidden'];
			switch($_POST['user_movimenti_fiscalizzati:FKEumis']){
				case "Kg.":
					$QTA=$Diff;
					break;
				case "Litri":
					$QTA=$Diff * $_POST['user_movimenti_fiscalizzati:FKEpesospecifico'];
					break;
				case "Mc.":
					$QTA=$Diff * $_POST['user_movimenti_fiscalizzati:FKEpesospecifico'] * 1000;
					break;
				}

			if($Diff!=0 && $PREV_ID_RIF!=$_POST['user_movimenti_fiscalizzati:ID_RIF']){
				## ho cambiato qta e rifiuto: aumento dispo nuovo rifiuto, diminuisco dispo vecchio rifiuto
				if($Diff>0)
					$sql="UPDATE user_movimenti_fiscalizzati SET FKEdisponibilita=FKEdisponibilita+".abs($QTA)." WHERE ID_RIF='".$_POST['user_movimenti_fiscalizzati:ID_RIF']."' AND NMOV>".$NMOV.";";
				else
					$sql="UPDATE user_movimenti_fiscalizzati SET FKEdisponibilita=FKEdisponibilita-".abs($QTA)." WHERE ID_RIF='".$_POST['user_movimenti_fiscalizzati:ID_RIF']."' AND NMOV>".$NMOV.";";
				$FEDIT->SDBWrite($sql,true,false);


				//$sql="UPDATE user_movimenti SET FKEdisponibilita= IF (FKEdisponibilita - ".abs($QTA).">0, FKEdisponibilita - ".abs($QTA).", 0) WHERE ID_RIF='".$PREV_ID_RIF."' AND NMOV>".$NMOV.";";
				//$FEDIT->SDBWrite($sql,true,false);
				}
			else{

				if($Diff!=0){
					## ho cambiato qta ma il rifiuto � rimasto lo stesso
					if($QTA>0)
						$sql="UPDATE user_movimenti_fiscalizzati SET FKEdisponibilita=FKEdisponibilita+".abs($QTA)." WHERE ID_RIF='".$_POST['user_movimenti_fiscalizzati:ID_RIF']."' AND NMOV>".$NMOV.";";
					else
						$sql="UPDATE user_movimenti_fiscalizzati SET FKEdisponibilita= IF (FKEdisponibilita - ".abs($QTA).">0, FKEdisponibilita - ".abs($QTA).", 0) WHERE ID_RIF='".$_POST['user_movimenti_fiscalizzati:ID_RIF']."' AND NMOV>".$NMOV.";";
					$FEDIT->SDBWrite($sql,true,false);
					}

				if($PREV_ID_RIF!=$_POST['user_movimenti_fiscalizzati:ID_RIF']){
					## ho cambiato rifiuto ma la qta � invariata

					## aumento di disponibilita nei movimenti successivi del nuovo rifiuto
					$sql="UPDATE user_movimenti_fiscalizzati SET FKEdisponibilita=FKEdisponibilita+".abs($QTA)." WHERE ID_RIF='".$_POST['user_movimenti_fiscalizzati:ID_RIF']."' AND NMOV>".$NMOV.";";
					$FEDIT->SDBWrite($sql,true,false);

					## diminuisco la disponibilita nei movimenti successivi del vecchio rifiuto
					$sql="UPDATE user_movimenti_fiscalizzati SET FKEdisponibilita=FKEdisponibilita-".abs($QTA)." WHERE ID_RIF='".$PREV_ID_RIF."' AND NMOV>".$NMOV.";";
					$FEDIT->SDBWrite($sql,true,false);

					}
				}



			## ripristino qta_hidden
			//$SQL = "UPDATE user_movimenti SET qta_hidden='".$_POST['user_movimenti:quantita']."' WHERE ID_MOV='".$_POST['user_movimenti:ID_MOV']."'";
			//$FEDIT->SDBWrite($SQL,true,false);
			}




		#
		#	aggiorno la disponibilit�del rifiuto e i dati di produzione media ( solo se gi� record in tabella, no INSERT )
		#

		if($TableI[0]=="user_movimenti_fiscalizzati"){
			## AGGIORNO LA DISPONIBILITA' DI QUESTI RIFIUTI
			$IMP_FILTER=true;
			$DISPO_PRINT_OUT = false;
			$IDRIF = $_POST['user_movimenti_fiscalizzati:ID_RIF'];
			$TableName	= "user_movimenti_fiscalizzati";
			$IDMov		= "ID_MOV_F";
                        $NMOV           = null;
			require("MovimentiDispoRIF.php");
			if($Disponibilita<0) $Disponibilita=0;
                        $FEDIT->SDBWrite("UPDATE user_schede_rifiuti SET FKEdisponibilita='$Disponibilita' WHERE ID_RIF='".$IDRIF."'",true,false);
			}


		if($TableI[0]=="user_movimenti_fiscalizzati"){

			//if($FEDIT->DbRecsNum=="1"){

				require("../__libs/SogerAUTOC_funct.php");
				$GiorniLavorati		=	giornilavorativi(date('Y')."-01-01", date('Y-m-d'));
				$GiorniLavorativi	=	5;

				## DERIVO QUANTITA' SCARICO MEDIO
				$sql = "SELECT AVG(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS media FROM user_movimenti_fiscalizzati WHERE ID_RIF='".$_POST['user_movimenti_fiscalizzati:ID_RIF']."' AND TIPO='S'";
				$FEDIT->SDBRead($sql,"DbRecordset",true,false);
				$MEDIA_S=number_format($FEDIT->DbRecordset[0]['media'], 2, ".", "");

				## DERIVO PRODUZIONE MEDIA GIORNALIERA E SETTIMANALE - scarichi
				$sql = "SELECT SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS scaricato FROM user_movimenti_fiscalizzati WHERE ID_RIF='".$_POST['user_movimenti_fiscalizzati:ID_RIF']."' AND TIPO='S'";
				$FEDIT->SDBRead($sql,"DbRecordset",true,false);
				$PRODOTTO=$FEDIT->DbRecordset[0]['scaricato'];
				$MediaCggRound  = number_format($PRODOTTO / $GiorniLavorati, 2, ".", "");
				$MediaSettRound = number_format($PRODOTTO / $GiorniLavorati * $GiorniLavorativi, 2, ".", "");

				## AGGIORNO MEDIE EFFETTIVE
				$SQL ="UPDATE user_schede_rifiuti_deposito SET media_gg_eff='".$MediaCggRound."', media_settimanale_eff='".$MediaSettRound."', scarico_medio='".$MEDIA_S."' WHERE ID_RIF='".$_POST['user_movimenti_fiscalizzati:ID_RIF']."'";
				//die($SQL);
				$FEDIT->SDBWrite($SQL,true,false);
				}
			//}



		#
		#	seconda pezza: avviso numero formulario gi� presente - update -
		#	se ho reg ind + reg fisc, controllo tab user_movimenti, se ho solo reg fisc, controllo tab user_movimenti_fiscalizzati

		if($SOGER->UserData['core_impiantiREG_IND']=='1'){
			$check	= "user_movimenti";
			$pri	= "ID_MOV";
			}
		else{
			$check	= "user_movimenti_fiscalizzati";
			$pri	= "ID_MOV_F";
			}

		// aggiungo condizione: solo se c'� trasportatore! altrimenti � scarico interno (come FIS)
		if($TableI[0]==$check && $_POST[$check.":ID_AZT"]<>'garbage' && ( ($_POST[$check.":TIPO"]=="S" AND $_POST[$check.":produttore"]=="1") | ($_POST[$check.":TIPO"]=="C" AND $_POST[$check.":destinatario"]=="1") ) ) {
			$sql  = "SELECT COUNT(*) AS CONTO FROM ".$check." WHERE NFORM='" . $_POST[$check.":NFORM"] . "' AND";
			$sql .= " ".$check.".ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
			$sql .= " AND ".$pri."<>'" . $_POST[$check.":ID_MOV"] . "'";
			// aggiungo condizione: solo se c'� trasportatore! altrimenti � scarico interno (come FIS)
			$sql .= " AND ID_AZT IS NOT NULL AND ID_AZT<>0 ";
			$sql .= " AND NMOV<>9999999 ";
			$FEDIT->SDbRead($sql,"DbRecordSet",true,false);
			if($FEDIT->DbRecordSet[0]["CONTO"]>1) {
				$FormularioDuplicato = true;
			}
		}




		#
		#
		#
		$_SESSION["FGE_IDs"][$InsInfo[0]] = $_POST[$_POST["FGE_PrimaryKeyRef"]];
		$label = "modificato";
		}


	else { # STO FACENDO UN INSERIMENTO

		#
		#	GESTIONE DEPOSITO, ALLINEO I DUE CAMPI MAX STOCK
		#
		if($SOGER->AppLocation=='UserRifiutoContenitore'){
			$_POST['user_schede_rifiuti_deposito:MaxStockClone'] = $_POST['user_schede_rifiuti_deposito:MaxStock'];
			}


		#
		#	SE STO FACENDO SCHEDA SISTRI, VALORE CUSTOM PER NMOV
		#
		if($TableI[0]=='user_movimenti' OR $TableI[0]=='user_movimenti_fiscalizzati'){
			if($_POST[$TableI[0].':TIPO_S_SCHEDA']==1){
				$_POST[$TableI[0].':NMOV'] = 9999999;
				}
			}

		$InsSql = $FEDIT->FGE_SQL_MakeInsert();

		# Va gestito il valore di ritorno (duplicate key for entry 'x' for key 'UniqueString' in tabella user_movimenti_fiscalizzati)
		if($FEDIT->SDBWrite($InsSql,true,false)){
			$MovID			= $FEDIT->DbLastID;
			$RifID			= $FEDIT->DbLastID;
			$_SESSION["FGE_IDs"][$InsInfo[0]] = $FEDIT->DbLastID;
			$LogSql  = "INSERT INTO core_logs ";
			$LogSql .= "(`table` , `pri_key` , `ID_USR` , `ID_IMP` , `sqlType` , `sqlCommand` ) ";
			$LogSql .= "VALUES ";
			$LogSql .= "('" . $TableI[0] . "','--','";
			$LogSql .= $SOGER->UserData["core_usersID_USR"] . "','";
			$LogSql .= $SOGER->UserData["core_usersID_IMP"] . "','creazione','" . addslashes($InsSql) . "')";
			$FEDIT->SDBWrite($LogSql,true,false);
		}

		# SET originalID_RIF
		if($TableI[0]=='user_schede_rifiuti'){
			// Se non ho compilato "Famiglie di sostanze classificate pericolose presenti nel rifiuto", lo setto uguale a "ADR: componenti pericolosi"
			if(trim($_POST['user_schede_rifiuti:et_comp_per'])=='')
				$_POST['user_schede_rifiuti:et_comp_per'] = $_POST['user_schede_rifiuti:ONU_per'];
			$SQL="UPDATE user_schede_rifiuti SET originalID_RIF=".$RifID." WHERE ID_RIF=".$RifID.";";
			$FEDIT->SDBWrite($SQL,true,false);
			}

		#
		#	seconda pezza: avviso numero formulario gi� presente - insert -
		#

		if($SOGER->UserData['core_impiantiREG_IND']=='1')
			$check	= "user_movimenti";
		else
			$check	= "user_movimenti_fiscalizzati";

		if($TableI[0]==$check && trim($_POST[$check.":NFORM"])!='' && ( ($_POST[$check.":TIPO"]=="S" AND $_POST[$check.":produttore"]=="1") | ($_POST[$check.":TIPO"]=="C" AND $_POST[$check.":destinatario"]=="1") ) ) {
			$sql  = "SELECT COUNT(*) AS CONTO FROM ".$check." WHERE NFORM='" . $_POST[$check.":NFORM"] . "' AND";
			$sql .= " ".$check.".ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
			$sql .= " AND NMOV<>9999999 ";
			require("../__includes/SOGER_DirectProfilo.php");
			$FEDIT->SDbRead($sql,"DbRecordSet",true,false);
			if($FEDIT->DbRecordSet[0]["CONTO"]>1) {
				$FormularioDuplicato = true;
			}
		}

		#
		#	aggiorno la disponibilit� del rifiuto e i dati di produzione media ( solo se gi� record in tabella, no INSERT )
		#

		if($TableI[0]=="user_movimenti_fiscalizzati"){
			## AGGIORNO LA DISPONIBILITA' DI QUESTI RIFIUTI
			$IMP_FILTER	= true;
			$DISPO_PRINT_OUT = false;
			$IDRIF		= $_POST['user_movimenti_fiscalizzati:ID_RIF'];
			$TableName	= "user_movimenti_fiscalizzati";
			$IDMov		= "ID_MOV_F";
			$EXCLUDE_9999999	= true;
                        $NMOV           = null;
			require("MovimentiDispoRIF.php");
			if($Disponibilita<0) $Disponibilita=0;
			$FEDIT->SDBWrite("UPDATE user_schede_rifiuti SET FKEdisponibilita='$Disponibilita' WHERE ID_RIF='".$IDRIF."'",true,false);
			}

		#
		#	aggiorno la disponibilit� del rifiuto e i dati di produzione media ( solo se gi� record in tabella, no INSERT )
		#

		if($TableI[0]=="user_movimenti_fiscalizzati"){
			## RICAVARE ID_DEP
			$sql="SELECT ID_DEP FROM user_schede_rifiuti_deposito WHERE ID_IMP='".$SOGER->UserData["core_impiantiID_IMP"]."' AND ID_RIF='".$_POST['user_movimenti_fiscalizzati:ID_RIF']."'";
			$FEDIT->SDBRead($sql,"DbRecordset",true,false);
			$ID_DEP=$FEDIT->DbRecordset[0]['ID_DEP'];

			if($FEDIT->DbRecsNum=="1"){

				require("../__libs/SogerAUTOC_funct.php");
				$GiorniLavorati		=	giornilavorativi(date('Y')."-01-01", date('Y-m-d'));
				$GiorniLavorativi	=	5;

				## DERIVO QUANTITA' SCARICO MEDIO
				$sql = "SELECT AVG(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS media FROM user_movimenti_fiscalizzati WHERE ID_RIF='".$_POST['user_movimenti_fiscalizzati:ID_RIF']."' AND TIPO='S'";
				$FEDIT->SDBRead($sql,"DbRecordset",true,false);
				$MEDIA_S=number_format($FEDIT->DbRecordset[0]['media'], 2, ".", "");

				## DERIVO PRODUZIONE MEDIA GIORNALIERA E SETTIMANALE - scarichi
				$sql = "SELECT SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS scaricato FROM user_movimenti_fiscalizzati WHERE ID_RIF='".$_POST['user_movimenti_fiscalizzati:ID_RIF']."' AND TIPO='S'";
				$FEDIT->SDBRead($sql,"DbRecordset",true,false);
				$PRODOTTO=$FEDIT->DbRecordset[0]['scaricato'];
				$MediaCggRound  = number_format($PRODOTTO / $GiorniLavorati, 2, ".", "");
				$MediaSettRound = number_format($PRODOTTO / $GiorniLavorati * $GiorniLavorativi, 2, ".", "");

				## AGGIORNO MEDIE EFFETTIVE
				$SQL ="UPDATE user_schede_rifiuti_deposito SET media_gg_eff='".$MediaCggRound."', media_settimanale_eff='".$MediaSettRound."', scarico_medio='".$MEDIA_S."' WHERE ID_DEP='".$ID_DEP."'";
				//die($SQL);
				$FEDIT->SDBWrite($SQL,true,false);
				}
			}

		#
		#	SOGER ECO intercetta i SALVATAGGI sulla tabella movimenti per generare i costi
		#	rimosso: i costi vengono generati solo dal dettaglio
		#	---> 08/03/2010 e io adesso lo rimetto, vedi AMGA
		 if($TableI[0] == "user_movimenti_fiscalizzati" && $SOGER->UserData["core_impiantiMODULO_ECO"]=="1" && $_POST["user_movimenti_fiscalizzati:quantita"]!="0") {
			 require("../__libs/SogerECO_funct.php");
			 $ID_AZI		= $_POST["user_movimenti_fiscalizzati:ID_AZI"];
			 $ID_UIMD		= $_POST["user_movimenti_fiscalizzati:ID_UIMD"];
                         $date                  = explode('/', $_POST["user_movimenti_fiscalizzati:DTMOV"]);
                         $DTMOV                 = $date[2].'-'.$date[1].'-'.$date[0];
			 $impianto		= $SOGER->UserData["core_impiantiID_IMP"];
			 $resultArray	= array();
                         require("../__libs/SogerECO_GeneraCosti.php");
		 }

		$label = "salvato";
	}
	break;
}

if(!@$FormularioDuplicato) {
	if($FEDIT->DbInsEdtNum>0) {
		$SOGER->SetFeedback("Record $label","2");
	}
} else {
	$SOGER->SetFeedback("Attenzione: il numero di formulario � duplicato (il record � stato comunque salvato)","3");
}

if(isset($updCosti)) {
	$SOGER->SetFeedback("Attenzione: il computo economico � stato aggiornato","3");
}

if(isset($_POST["redirection"]) && $_POST["redirection"]!="") {
	$url = "location: " . $_POST["redirection"] . "?table=". $InsInfo[0] . "&pri=" . $InsInfo[1] . "&filter=" . $_SESSION["FGE_IDs"][$InsInfo[0]];
} else {
	$url = "location: ../?table=". $InsInfo[0] . "&pri=" . $InsInfo[1] . "&filter=" . $_SESSION["FGE_IDs"][$InsInfo[0]];
}

$url .= "&hash=" . MakeUrlHash($InsInfo[0],$InsInfo[1],$_SESSION["FGE_IDs"][$InsInfo[0]]);

# impedisce compilazione automatica data/ora inizio trasporto
//if(isset($_POST['notCompileDate']))	$url.="&notCompileDate=1";

if(MovimentoInvolved($InsInfo[1],$_SESSION["FGE_IDs"][$InsInfo[0]],$InsInfo[0])) {
	$url .= "&Involved";
	$MovimentoBusy = "Il record � impiegato in un movimento: alcuni campi non possono essere modificati";
	$SOGER->SetFeedback($MovimentoBusy,"3");
}

require_once("../__includes/COMMON_sleepForgEdit.php");
require_once("../__includes/COMMON_sleepSoger.php");

header($url);
?>