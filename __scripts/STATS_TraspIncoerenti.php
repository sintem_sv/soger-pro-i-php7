<?php

$CounterInc=0;

## TIPI DI INCOERENZA: ########################################################
##																			 ##
## -1 trasporto in conto proprio	ma trasportatore	non ha abilitazione  ##
## -2 trasporto in conto terzi		ma trasportatore	non ha abilitazione  ##
##																			 ##
## -3 trasporto in conto proprio	ma automezzo		non ha abilitazione  ##
## -4 trasporto in conto terzi		ma automezzo		non ha abilitazione  ##
##																			 ##
## -5 trasporto in conto proprio	ma rimorchio		non ha abilitazione  ##
## -6 trasporto in conto terzi		ma rimorchio		non ha abilitazione  ##
##																			 ##
###############################################################################

for($m=0;$m<count($FEDIT->DbRecordSet);$m++){
	
	
	if(strtoupper($FEDIT->DbRecordSet[$m]['CF_D'])!=strtoupper($FEDIT->DbRecordSet[$m]['CF_T'])){ //conto terzi
		if($FEDIT->DbRecordSet[$m]['terzi']==""){
			$Incoerenti[$CounterInc]['NMOV']=$FEDIT->DbRecordSet[$m]['NMOV'];
			$Incoerenti[$CounterInc]['TIPO']=$FEDIT->DbRecordSet[$m]['TIPO'];
			$Incoerenti[$CounterInc]['NFORM']=$FEDIT->DbRecordSet[$m]['NFORM'];
			$Incoerenti[$CounterInc]['DTFORM']=$FEDIT->DbRecordSet[$m]['DTFORM'];
			$Incoerenti[$CounterInc]['TRASP']=$FEDIT->DbRecordSet[$m]['trasportatore'];
			$Incoerenti[$CounterInc]['proprio']=$FEDIT->DbRecordSet[$m]['proprio'];
			$Incoerenti[$CounterInc]['terzi']=$FEDIT->DbRecordSet[$m]['terzi'];
			$Incoerenti[$CounterInc]['AUTO']=$FEDIT->DbRecordSet[$m]['AUTO'];
			$Incoerenti[$CounterInc]['RMK']=$FEDIT->DbRecordSet[$m]['RMK'];
			$Incoerenti[$CounterInc]['INC']=2;
			$CounterInc++;
			}
		else{
			if($FEDIT->DbRecordSet[$m]['LIM_AUTO']=='3' | $FEDIT->DbRecordSet[$m]['LIM_AUTO']=='4'){
				$Incoerenti[$CounterInc]['NMOV']=$FEDIT->DbRecordSet[$m]['NMOV'];
				$Incoerenti[$CounterInc]['TIPO']=$FEDIT->DbRecordSet[$m]['TIPO'];
				$Incoerenti[$CounterInc]['NFORM']=$FEDIT->DbRecordSet[$m]['NFORM'];
				$Incoerenti[$CounterInc]['DTFORM']=$FEDIT->DbRecordSet[$m]['DTFORM'];
				$Incoerenti[$CounterInc]['TRASP']=$FEDIT->DbRecordSet[$m]['trasportatore'];
				$Incoerenti[$CounterInc]['proprio']=$FEDIT->DbRecordSet[$m]['proprio'];
				$Incoerenti[$CounterInc]['terzi']=$FEDIT->DbRecordSet[$m]['terzi'];
				$Incoerenti[$CounterInc]['AUTO']=$FEDIT->DbRecordSet[$m]['AUTO'];
				$Incoerenti[$CounterInc]['RMK']=$FEDIT->DbRecordSet[$m]['RMK'];
				$Incoerenti[$CounterInc]['INC']=4;
				$CounterInc++;
				}
			else{
				if($FEDIT->DbRecordSet[$m]['LIM_RMK']=='3' | $FEDIT->DbRecordSet[$m]['LIM_RMK']=='4'){
					$Incoerenti[$CounterInc]['NMOV']=$FEDIT->DbRecordSet[$m]['NMOV'];
					$Incoerenti[$CounterInc]['TIPO']=$FEDIT->DbRecordSet[$m]['TIPO'];
					$Incoerenti[$CounterInc]['NFORM']=$FEDIT->DbRecordSet[$m]['NFORM'];
					$Incoerenti[$CounterInc]['DTFORM']=$FEDIT->DbRecordSet[$m]['DTFORM'];
					$Incoerenti[$CounterInc]['TRASP']=$FEDIT->DbRecordSet[$m]['trasportatore'];
					$Incoerenti[$CounterInc]['proprio']=$FEDIT->DbRecordSet[$m]['proprio'];
					$Incoerenti[$CounterInc]['terzi']=$FEDIT->DbRecordSet[$m]['terzi'];
					$Incoerenti[$CounterInc]['AUTO']=$FEDIT->DbRecordSet[$m]['AUTO'];
					$Incoerenti[$CounterInc]['RMK']=$FEDIT->DbRecordSet[$m]['RMK'];
					$Incoerenti[$CounterInc]['INC']=6;
					$CounterInc++;
					}
				}
			}
		}
	else{ //conto proprio
		if($FEDIT->DbRecordSet[$m]['proprio']==""){
			$Incoerenti[$CounterInc]['NMOV']=$FEDIT->DbRecordSet[$m]['NMOV'];
			$Incoerenti[$CounterInc]['TIPO']=$FEDIT->DbRecordSet[$m]['TIPO'];
			$Incoerenti[$CounterInc]['NFORM']=$FEDIT->DbRecordSet[$m]['NFORM'];
			$Incoerenti[$CounterInc]['DTFORM']=$FEDIT->DbRecordSet[$m]['DTFORM'];
			$Incoerenti[$CounterInc]['TRASP']=$FEDIT->DbRecordSet[$m]['trasportatore'];
			$Incoerenti[$CounterInc]['proprio']=$FEDIT->DbRecordSet[$m]['proprio'];
			$Incoerenti[$CounterInc]['terzi']=$FEDIT->DbRecordSet[$m]['terzi'];
			$Incoerenti[$CounterInc]['AUTO']=$FEDIT->DbRecordSet[$m]['AUTO'];
			$Incoerenti[$CounterInc]['RMK']=$FEDIT->DbRecordSet[$m]['RMK'];
			$Incoerenti[$CounterInc]['INC']=1;
			$CounterInc++;
			}
		else{
			if($FEDIT->DbRecordSet[$m]['LIM_AUTO']=='2' | $FEDIT->DbRecordSet[$m]['LIM_AUTO']=='4'){
				$Incoerenti[$CounterInc]['NMOV']=$FEDIT->DbRecordSet[$m]['NMOV'];
				$Incoerenti[$CounterInc]['TIPO']=$FEDIT->DbRecordSet[$m]['TIPO'];
				$Incoerenti[$CounterInc]['NFORM']=$FEDIT->DbRecordSet[$m]['NFORM'];
				$Incoerenti[$CounterInc]['DTFORM']=$FEDIT->DbRecordSet[$m]['DTFORM'];
				$Incoerenti[$CounterInc]['TRASP']=$FEDIT->DbRecordSet[$m]['trasportatore'];
				$Incoerenti[$CounterInc]['proprio']=$FEDIT->DbRecordSet[$m]['proprio'];
				$Incoerenti[$CounterInc]['terzi']=$FEDIT->DbRecordSet[$m]['terzi'];
				$Incoerenti[$CounterInc]['AUTO']=$FEDIT->DbRecordSet[$m]['AUTO'];
				$Incoerenti[$CounterInc]['RMK']=$FEDIT->DbRecordSet[$m]['RMK'];
				$Incoerenti[$CounterInc]['INC']=3;
				$CounterInc++;
				}
			else{
				if($FEDIT->DbRecordSet[$m]['LIM_RMK']=='2' | $FEDIT->DbRecordSet[$m]['LIM_RMK']=='4'){
					$Incoerenti[$CounterInc]['NMOV']=$FEDIT->DbRecordSet[$m]['NMOV'];
					$Incoerenti[$CounterInc]['TIPO']=$FEDIT->DbRecordSet[$m]['TIPO'];
					$Incoerenti[$CounterInc]['NFORM']=$FEDIT->DbRecordSet[$m]['NFORM'];
					$Incoerenti[$CounterInc]['DTFORM']=$FEDIT->DbRecordSet[$m]['DTFORM'];
					$Incoerenti[$CounterInc]['TRASP']=$FEDIT->DbRecordSet[$m]['trasportatore'];
					$Incoerenti[$CounterInc]['proprio']=$FEDIT->DbRecordSet[$m]['proprio'];
					$Incoerenti[$CounterInc]['terzi']=$FEDIT->DbRecordSet[$m]['terzi'];
					$Incoerenti[$CounterInc]['AUTO']=$FEDIT->DbRecordSet[$m]['AUTO'];
					$Incoerenti[$CounterInc]['RMK']=$FEDIT->DbRecordSet[$m]['RMK'];
					$Incoerenti[$CounterInc]['INC']=5;
					$CounterInc++;
					}
				}
			}
		}


	}


//print_r($Incoerenti);




$Ypos+=20;
$counter=0;

if($Incoerenti){
	foreach($Incoerenti as $k=>$dati) {	
		
		if($counter==0) {
	//{{{ 
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
			
			$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,4,"Mov. #","TLB","C");

			$FEDIT->FGE_PdfBuffer->SetXY(30,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(10,4,"Tipo","TB","C");

			$FEDIT->FGE_PdfBuffer->SetXY(40,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Data Mov.","TB","C");
			
			$FEDIT->FGE_PdfBuffer->SetXY(60,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(25,4,"Formulario","TB","C");
			
			$FEDIT->FGE_PdfBuffer->SetXY(85,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(65,4,"Trasportatore","TB","C");
			
			$FEDIT->FGE_PdfBuffer->SetXY(150,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Automezzo","TB","C");

			$FEDIT->FGE_PdfBuffer->SetXY(170,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Rimorchio","TB","C");

			$FEDIT->FGE_PdfBuffer->SetXY(190,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(90,4,"Incoerenza","TBR","C");

			$Ypos += 10; //}}}
		}
		
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	
		$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,4,$dati["NMOV"],0,"C");
		
		$FEDIT->FGE_PdfBuffer->SetXY(30,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(10,4,$dati["TIPO"],0,"C");
		
		$FEDIT->FGE_PdfBuffer->SetXY(40,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,4,date("d/m/Y",strtotime($dati["DTFORM"])),0,"C");

		$FEDIT->FGE_PdfBuffer->SetXY(60,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,4,$dati["NFORM"],0,"L");
		
		$FEDIT->FGE_PdfBuffer->SetXY(85,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(60,4,$dati["TRASP"],0,"L");
		
		$FEDIT->FGE_PdfBuffer->SetXY(150,$Ypos);
		if($dati["AUTO"]=="") $dati["AUTO"]="--";
		$FEDIT->FGE_PdfBuffer->MultiCell(20,4,$dati["AUTO"],0,"C");
		
		$FEDIT->FGE_PdfBuffer->SetXY(170,$Ypos);
		if($dati["RMK"]=="") $dati["RMK"]="--";
		$FEDIT->FGE_PdfBuffer->MultiCell(20,4,$dati["RMK"],0,"C");
		
		$FEDIT->FGE_PdfBuffer->SetXY(190,$Ypos);
		switch($dati["INC"]){
			case '1':
				$msg = "Il trasporto � in conto proprio, ma il trasportatore non risulta abilitato";
				break;
			case '2':
				$msg = "Il trasporto � in conto terzi, ma il trasportatore non risulta abilitato";
				break;
			case '3':
				$msg = "Il trasporto � in conto proprio, ma l'automezzo non risulta abilitato";
				break;
			case '4':
				$msg = "Il trasporto � in conto terzi, ma l'automezzo non risulta abilitato";
				break;
			case '5':
				$msg = "Il trasporto � in conto proprio, ma il rimorchio non risulta abilitato";
				break;
			case '6':
				$msg = "Il trasporto � in conto terzi, ma il rimorchio non risulta abilitato";
				break;
			}
		$FEDIT->FGE_PdfBuffer->MultiCell(90,4,$msg,0,"L");
		

		$Ypos += 14;	 
		$counter++;
	}
}


## TIPI DI INCOERENZA: ########################################################
##																			 ##
## -1 trasporto in conto proprio	ma trasportatore	non ha abilitazione  ##
## -2 trasporto in conto terzi		ma trasportatore	non ha abilitazione  ##
##																			 ##
## -3 trasporto in conto proprio	ma automezzo		non ha abilitazione  ##
## -4 trasporto in conto terzi		ma automezzo		non ha abilitazione  ##
##																			 ##
## -5 trasporto in conto proprio	ma rimorchio		non ha abilitazione  ##
## -6 trasporto in conto terzi		ma rimorchio		non ha abilitazione  ##
##																			 ##
###############################################################################


?>