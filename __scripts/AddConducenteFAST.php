<?php
session_start();
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");	
require_once("../__includes/COMMON_wakeForgEditDG.php");

global $SOGER;
$FEDG->FGE_FlushTableInfo();
$FEDG->FGE_UseTables("user_autisti");
$FEDG->FGE_SetFormFields(array("ID_UIMT","nome","description","adr","patente","rilascio","scadenza"),"user_autisti");
$FEDG->FGE_SetBreak("adr","user_autisti");

$FEDG->FGE_DescribeFields();
#	
$FEDG->FGE_LookUpCFG("ID_UIMT","user_autisti");
$FEDG->FGE_UseTables("user_impianti_trasportatori");
$FEDG->FGE_SetSelectFields(array("description","ID_AZT"),"user_impianti_trasportatori");
$FEDG->FGE_SetFilter("ID_AZT",$_GET["ImpID"],"user_impianti_trasportatori");
$FEDG->FGE_HideFields("ID_AZT","user_impianti_trasportatori");
$FEDG->FGE_LookUpDescribe();
$FEDG->FGE_LookUpDone();
echo $FEDG->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea autista");
require_once("../__includes/COMMON_sleepForgEditDG.php");

?>
