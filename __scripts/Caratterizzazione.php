<?php
session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__libs/SQLFunct.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");

global $SOGER;

$sql ="SELECT core_impianti.description AS azienda, CONCAT(core_impianti.indirizzo, ' - ', lov_comuni_istat.description, ' (', lov_comuni_istat.shdes_prov, ')') AS stabilimento, lov_cer.description as nomecer, lov_produzione_rifiuto_freq.description as prod_f_rifiuto, lov_tipo_rifiuto.description as tipo_rifiuto, lov_produzione_rifiuto.description as prod_rifiuto, descrizione, user_schede_rifiuti.pericoloso, H1, H2, H3A, H3B, H4, H5, H6, H7, H8, H9, H10, H11, H12, H13, H14, H15, HP1, HP2, HP3, HP4, HP5, HP6, HP7, HP8, HP9, HP10, HP11, HP12, HP13, HP14, HP15, user_schede_rifiuti.adr, ID_ONU, CAR_LastUpd, CAR_Responsabile, CAR_Ciclo, CAR_MatPrime, CAR_Note, CAR_prep_pericolosi, et_comp_per, CAR_AnalisiNecessaria, CAR_DataAnalisi, CAR_Laboratorio, CAR_NumDocumento, CAR_prep_pericolosi, user_schede_rifiuti.peso_spec, lov_cer.COD_CER as cer, lov_origine_rifiuto.description as origine, lov_fonte_rifiuto.description as fonte, lov_destinazione_rifiuto.description as destinazione, lov_stato_fisico.description as statofisico, lov_misure.description as misura, lov_contenitori.description AS CAR_Contenitori, lov_contenitori.m_cubi AS CAR_ContenitoriCap ";
$sql.="FROM user_schede_rifiuti ";
$sql.="JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
$sql.="JOIN core_impianti ON core_impianti.ID_IMP=user_schede_rifiuti.ID_IMP ";
$sql.="JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM=core_impianti.ID_COM ";
$sql.="LEFT JOIN lov_produzione_rifiuto ON user_schede_rifiuti.ID_RIFPROD=lov_produzione_rifiuto.ID_RIFPROD ";
$sql.="LEFT JOIN lov_produzione_rifiuto_freq ON user_schede_rifiuti.ID_RIFPROD_F=lov_produzione_rifiuto_freq.ID_RIFPROD_F ";
$sql.="LEFT JOIN lov_tipo_rifiuto ON user_schede_rifiuti.ID_RIFTYPE=lov_tipo_rifiuto.ID_RIFTYPE ";
$sql.="LEFT JOIN lov_origine_rifiuto ON user_schede_rifiuti.ID_OR_RIF=lov_origine_rifiuto.ID_OR_RIF ";
$sql.="LEFT JOIN lov_fonte_rifiuto ON user_schede_rifiuti.ID_FONTE_RIF=lov_fonte_rifiuto.ID_FONTE_RIF ";
$sql.="LEFT JOIN lov_destinazione_rifiuto ON user_schede_rifiuti.ID_DEST_RIF=lov_destinazione_rifiuto.ID_DEST_RIF ";
$sql.="JOIN lov_stato_fisico ON user_schede_rifiuti.ID_SF=lov_stato_fisico.ID_SF ";
$sql.="JOIN lov_misure ON user_schede_rifiuti.ID_UMIS=lov_misure.ID_UMIS ";
$sql.="LEFT JOIN user_schede_rifiuti_deposito ON user_schede_rifiuti.ID_RIF=user_schede_rifiuti.ID_RIF ";
$sql.="LEFT JOIN lov_contenitori ON user_schede_rifiuti_deposito.ID_CONT=lov_contenitori.ID_CONT ";
$sql.="WHERE user_schede_rifiuti.ID_RIF='".$_GET['filter']."' AND user_schede_rifiuti.ID_IMP='".$SOGER->UserData["core_impiantiID_IMP"] . "' ";

$FEDIT->SDbRead($sql,"DbRecordSet");

$CAR_NOTE = $FEDIT->DbRecordSet[0]['CAR_Note'];

# dati azienda #
$azienda = $FEDIT->DbRecordSet[0]['azienda'];
$stabilimento = $FEDIT->DbRecordSet[0]['stabilimento'];


# data ultimo aggiornamento #
$LastUpd=explode(" ",$FEDIT->DbRecordSet[0]['CAR_LastUpd']);
$LastUpdTrunc=explode("-",$LastUpd[0]);
$anno=$LastUpdTrunc[0];
$mese=$LastUpdTrunc[1];
$giorno=$LastUpdTrunc[2];
$LastUpdDate=$giorno."/".$mese."/".$anno;

# pericolosit� rifiuto #
if($FEDIT->DbRecordSet[0]['pericoloso']==0) $pericoloso="No"; else $pericoloso="S�";

# l'analisi � necessaria? #
if($FEDIT->DbRecordSet[0]["CAR_AnalisiNecessaria"]==0) $analisi=false; else $analisi=true;

# adr, num onu, classe pericolo e imballaggio #
if($FEDIT->DbRecordSet[0]['adr']==0){
	$adr="No";
	$onu="-";
	$pericoloadr="-";
	$imballaggio="-";
	}
else{ 
	$adr="S�";
	$sql="SELECT description, eti, imb FROM lov_num_onu WHERE ID_ONU=".$FEDIT->DbRecordSet[0]['ID_ONU'].";";
	$FEDIT->SDbRead($sql,"DbRecordSetOnu");
	$onu=$FEDIT->DbRecordSetOnu[0]['description'];
	$pericoloadr=$FEDIT->DbRecordSetOnu[0]['eti'];
	$imballaggio=$FEDIT->DbRecordSetOnu[0]['imb'];
	}

# classi H #
//$classiH=array();
//$elencoH="";
//$definizioniH="";
//if($FEDIT->DbRecordSet[0]['H1']==1) $classiH[]="H1";
//if($FEDIT->DbRecordSet[0]['H2']==1) $classiH[]="H2";
//if($FEDIT->DbRecordSet[0]['H3A']==1) $classiH[]="H3A";
//if($FEDIT->DbRecordSet[0]['H3B']==1) $classiH[]="H3B";
//if($FEDIT->DbRecordSet[0]['H4']==1) $classiH[]="H4";
//if($FEDIT->DbRecordSet[0]['H5']==1) $classiH[]="H5";
//if($FEDIT->DbRecordSet[0]['H6']==1) $classiH[]="H6";
//if($FEDIT->DbRecordSet[0]['H7']==1) $classiH[]="H7";
//if($FEDIT->DbRecordSet[0]['H8']==1) $classiH[]="H8";
//if($FEDIT->DbRecordSet[0]['H9']==1) $classiH[]="H9";
//if($FEDIT->DbRecordSet[0]['H10']==1) $classiH[]="H10";
//if($FEDIT->DbRecordSet[0]['H11']==1) $classiH[]="H11";
//if($FEDIT->DbRecordSet[0]['H12']==1) $classiH[]="H12";
//if($FEDIT->DbRecordSet[0]['H13']==1) $classiH[]="H13";
//if($FEDIT->DbRecordSet[0]['H14']==1) $classiH[]="H14";
//if($FEDIT->DbRecordSet[0]['H15']==1) $classiH[]="H15";
//
//if(count($classiH)>0){
//	$classiHdef=array();
//	for($h=0;$h<count($classiH);$h++){
//		$sql="SELECT DES FROM lov_classi_h WHERE CL_H='".$classiH[$h]."';";
//		$FEDIT->SDbRead($sql,"DbRecordSetH");
//		$classiHdef[$h]=$FEDIT->DbRecordSetH[0]['DES'];
//		$elencoH.=$classiH[$h]." - ".$classiHdef[$h]."\\par \\par ";
//		}
//	}
//else
//	$elencoH="Nessuna classe H attribuita\\par ";


# classi HP #
$classiHP=array();
$elencoHP="";
$definizioniHP="";
if($FEDIT->DbRecordSet[0]['HP1']==1) $classiHP[]="HP1";
if($FEDIT->DbRecordSet[0]['HP2']==1) $classiHP[]="HP2";
if($FEDIT->DbRecordSet[0]['HP3']==1) $classiHP[]="HP3";
if($FEDIT->DbRecordSet[0]['HP4']==1) $classiHP[]="HP4";
if($FEDIT->DbRecordSet[0]['HP5']==1) $classiHP[]="HP5";
if($FEDIT->DbRecordSet[0]['HP6']==1) $classiHP[]="HP6";
if($FEDIT->DbRecordSet[0]['HP7']==1) $classiHP[]="HP7";
if($FEDIT->DbRecordSet[0]['HP8']==1) $classiHP[]="HP8";
if($FEDIT->DbRecordSet[0]['HP9']==1) $classiHP[]="HP9";
if($FEDIT->DbRecordSet[0]['HP10']==1) $classiHP[]="HP10";
if($FEDIT->DbRecordSet[0]['HP11']==1) $classiHP[]="HP11";
if($FEDIT->DbRecordSet[0]['HP12']==1) $classiHP[]="HP12";
if($FEDIT->DbRecordSet[0]['HP13']==1) $classiHP[]="HP13";
if($FEDIT->DbRecordSet[0]['HP14']==1) $classiHP[]="HP14";
if($FEDIT->DbRecordSet[0]['HP15']==1) $classiHP[]="HP15";

if(count($classiHP)>0){
	$classiHPdef=array();
	for($hp=0;$hp<count($classiHP);$hp++){
		$sql="SELECT DESCR_EXTENDED FROM lov_classi_hp WHERE CL_HP='".$classiHP[$hp]."';";
		$FEDIT->SDbRead($sql,"DbRecordSetHP");
		$classiHPdef[$hp]=$FEDIT->DbRecordSetHP[0]['DESCR_EXTENDED'];
		$elencoHP.=$classiHP[$hp]." - ".$classiHPdef[$hp]."\\par \\par ";
		}
	}
else
	$elencoHP="Nessuna classe HP attribuita\\par ";


$ReplacedWith = array();
$ToBeReplaced = array(
	0=>"[LAST_UPD]",
	1=>"[RESPONSABILE]",
	2=>"[CER]",
	3=>"[SCHEDA_RIF]",
	4=>"[PERICOLOSO]",
	5=>"[ORIGINE]",
	6=>"[FONTE]",
	7=>"[CICLO]",
	8=>"[DATA_ANALISI]",
	9=>"[NUM_DOC]",
	10=>"[MAT_PRIME]",
	11=>"[DESTINAZIONE]",
	12=>"[STATO_FIS]",
	13=>"[UM]",
	14=>"[PESO_SPEC]",
	15=>"[CONTENITORI]",
	16=>"[CAPACITA]",
	17=>"[N_CONT]",
	18=>"[ADR]",
	19=>"[ONU]",
	20=>"[ETI]",
	21=>"[IMB]",
	//22=>"[CLH]",
	23=>"[CLHP]",
	24=>"[IMAGE]",
	25=>"[NOME_CER]",
	26=>"[RIFIUTO_TYPE]",
	27=>"[RIFIUTO_PROD]",
	28=>"[RIFIUTO_PROD_F]",
	29=>"[LAB_ANALISI]",
	30=>"[PROC_COMBO]",
	31=>"[MAC_COMBO]",
	32=>"[SOSTANZE_PREPARATI_PERICOLOSI]",
	33=>"[SOST_PERIC]",
	34=>"[PROCEDURA_ISO]",
	35=>"[NOTE]",
	36=>"[AZIENDA]",
	37=>"[STABILIMENTO]"
	);

$ReplacedWith[0] = $LastUpdDate;

# responsabile analisi #
if(!is_null($FEDIT->DbRecordSet[0]['CAR_Responsabile']) AND $FEDIT->DbRecordSet[0]['CAR_Responsabile']!='')
	$ReplacedWith[1] = $FEDIT->DbRecordSet[0]['CAR_Responsabile']; 
else{
	if($analisi)
		$ReplacedWith[1] = "non specificato";
	else
		$ReplacedWith[1] = "--";
	}

$ReplacedWith[2] = $FEDIT->DbRecordSet[0]['cer'];
$ReplacedWith[3] = $FEDIT->DbRecordSet[0]['descrizione'];
$ReplacedWith[4] = $pericoloso;

if($FEDIT->DbRecordSet[0]['fonte']=="")
	$FEDIT->DbRecordSet[0]['fonte']="Fonte non specificata";
$ReplacedWith[5] = $FEDIT->DbRecordSet[0]['fonte'];

if($FEDIT->DbRecordSet[0]['origine']=="")
	$FEDIT->DbRecordSet[0]['origine']="Origine non specificata";
$ReplacedWith[6] = $FEDIT->DbRecordSet[0]['origine'];

$ReplacedWith[7] = $FEDIT->DbRecordSet[0]['CAR_Ciclo'];


# data analisi #
$dataAnalisiRec=explode("-",$FEDIT->DbRecordSet[0]['CAR_DataAnalisi']);
$anno=$dataAnalisiRec[0];
$mese=$dataAnalisiRec[1];
$giorno=$dataAnalisiRec[2];
$DataAnalisi=$giorno."/".$mese."/".$anno;
if($DataAnalisi=="00/00/0000"){
	if($analisi)
		$DataAnalisi="non specificato";	
	else
		$DataAnalisi="per il completamento della caratterizzazione del rifiuto, in base all'origine e composizione, non � stato necessario effettuare un�analisi di composizione e far redigere relativo referto analitico";
	}
$ReplacedWith[8] = $DataAnalisi;

# num documento #
if(!is_null($FEDIT->DbRecordSet[0]['CAR_NumDocumento']) AND $FEDIT->DbRecordSet[0]['CAR_NumDocumento']!=''){
	$ReplacedWith[9] = $FEDIT->DbRecordSet[0]['CAR_NumDocumento'];
	}
else{
	if($analisi)
		$ReplacedWith[9] = "non specificato";
	else
		$ReplacedWith[9] = "--";
	}

$ReplacedWith[10] = $FEDIT->DbRecordSet[0]['CAR_MatPrime'];
if($FEDIT->DbRecordSet[0]['destinazione']=="")
	$FEDIT->DbRecordSet[0]['destinazione']="Destinazione non specificata";
$ReplacedWith[11] = $FEDIT->DbRecordSet[0]['destinazione'];
$ReplacedWith[12] = $FEDIT->DbRecordSet[0]['statofisico'];
$ReplacedWith[13] = $FEDIT->DbRecordSet[0]['misura'];
$ReplacedWith[14] = $FEDIT->DbRecordSet[0]['peso_spec'];

$sql="SELECT lov_contenitori.description as contenitore, lov_contenitori.m_cubi as portata, user_schede_rifiuti_deposito.NumCont, user_schede_rifiuti_deposito.MaxStock FROM user_schede_rifiuti_deposito JOIN lov_contenitori ON lov_contenitori.ID_CONT=user_schede_rifiuti_deposito.ID_CONT WHERE ID_RIF=".$_GET['filter'];
$FEDIT->SDbRead($sql,"DbRecordSetCont");

if($FEDIT->DbRecordSetCont[0]['contenitore']!=''){
	$ReplacedWith[15] = $FEDIT->DbRecordSetCont[0]['contenitore'];
	if($FEDIT->DbRecordSetCont[0]['MAxStock']=='0')
		$ReplacedWith[16] = $FEDIT->DbRecordSetCont[0]['portata']." Metri cubi";
	else
		$ReplacedWith[16] = $FEDIT->DbRecordSetCont[0]['MaxStock']." Kg";
	$ReplacedWith[17] = $FEDIT->DbRecordSetCont[0]['NumCont'];
	}
else{
	$ReplacedWith[15] = "in attesa di definizione";
	$ReplacedWith[16] = "in attesa di definizione";
	$ReplacedWith[17] = "in attesa di definizione";
	}

$ReplacedWith[18] = $adr;
$ReplacedWith[19] = $onu;
$ReplacedWith[20] = $pericoloadr;
$ReplacedWith[21] = $imballaggio;
//$ReplacedWith[22] = $elencoH;
$ReplacedWith[23] = $elencoHP;

## inserire immagine nell' rtf
$image="../__upload/caratterizzazioni/".$_GET['filter'].".jpg";
if (file_exists($image)) { 
	$newImage="";
	$b=fopen($image,"rb");
	$imgData=getimagesize($image);
	$newImagePre="{\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
	while (!feof($b)) {
		$newImage.= fgets($b);
	}
	$hex=bin2hex($newImage);
	$imgDat=$newImagePre.$hex."}}";
	}
else
	$imgDat="Immagine non disponibile.";
$ReplacedWith[24] = $imgDat;
$ReplacedWith[25] = $FEDIT->DbRecordSet[0]['nomecer'];
$ReplacedWith[26] = $FEDIT->DbRecordSet[0]['tipo_rifiuto'];
$ReplacedWith[27] = $FEDIT->DbRecordSet[0]['prod_rifiuto'];
$ReplacedWith[28] = $FEDIT->DbRecordSet[0]['prod_f_rifiuto'];

if(!is_null($FEDIT->DbRecordSet[0]['CAR_Laboratorio']) AND $FEDIT->DbRecordSet[0]['CAR_Laboratorio']!='')
	$ReplacedWith[29] = $FEDIT->DbRecordSet[0]['CAR_Laboratorio'];
else{
	if($analisi)
		$ReplacedWith[29] = "non specificato";
	else
		$ReplacedWith[29] = "--";
	}


##processi
$processiList="";
$SQL="SELECT description FROM lov_processi WHERE ID_PROC IN (SELECT ID_PROC FROM user_schede_rifiuti_processi WHERE ID_RIF=".$_GET['filter'].")";
$FEDIT->SDbRead($SQL,"DbRecordSetProc");
if(isset($FEDIT->DbRecordSetProc)){
	for($p=0;$p<count($FEDIT->DbRecordSetProc);$p++){
		$processiList.="* ".$FEDIT->DbRecordSetProc[$p]['description']."\line";
		}
	}
else
	$processiList.="Nessun processo specificato\line";

$ReplacedWith[30] = $processiList;

##macchinari
$macchinariList="";
$SQL="SELECT description FROM lov_macchinari WHERE ID_MAC IN (SELECT ID_MAC FROM user_schede_rifiuti_macchinari WHERE ID_RIF=".$_GET['filter'].")";
$FEDIT->SDbRead($SQL,"DbRecordSetMac");
if(isset($FEDIT->DbRecordSetMac)){
	for($m=0;$m<count($FEDIT->DbRecordSetMac);$m++){
		$macchinariList.="* ".$FEDIT->DbRecordSetMac[$m]['description']."\line";
		}
	}
else
	$macchinariList.="Nessun macchinario specificato\line";

$ReplacedWith[31] = $macchinariList;





if($FEDIT->DbRecordSet[0]['CAR_prep_pericolosi']=='0')
	$ReplacedWith[32] = "Nel ciclo produttivo non si utilizzano sostanze o preparati classificati pericolosi.";
else
	$ReplacedWith[32] = "Nel ciclo produttivo si utilizzano sostanze o preparati classificati pericolosi.";

if(trim($FEDIT->DbRecordSet[0]['et_comp_per'])!="")
	$ReplacedWith[33] = $FEDIT->DbRecordSet[0]['et_comp_per'];
else
	$ReplacedWith[33] = "nessuna";


## PROCEDURA ISO - ID_ISOF=51
$SQL="SELECT procedura FROM core_impianti_iso WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1 AND ID_ISOF=51;";
$FEDIT->SdbRead($SQL, "DbRecordSet");
$PROCEDURA=$FEDIT->DbRecordSet[0]["procedura"];
if(!is_null($PROCEDURA))
	$ReplacedWith[34]="Documento elaborato in ottemperanza alla procedura cliente: ".$PROCEDURA;
else
	$ReplacedWith[34]="";

$ReplacedWith[35] = $CAR_NOTE;

$ReplacedWith[36] = $azienda;
$ReplacedWith[37] = $stabilimento;

$FileName = "Scheda_Caratterizzazione_Rifiuto.rtf";
$template = "CARATTERIZZAZIONE.rtf";

StreamOut($template,$FileName,$ToBeReplaced,$ReplacedWith);

require_once("../__includes/COMMON_sleepForgEdit.php");
?>