<?php
session_start();
require_once("Soger_includes.inc");
require_once("ForgEdit_includes.inc");
require_once("../__libs/SQLFunct.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");
global $SOGER;		

#
#	DATI BASE SCHEDA
#
$ToBeReplaced = array(
	"[ORCR]",
	"[CER]",
	"[CERDES]",
	"[DATA]",
	"[NOTE]",
	"[STFIS]",
	"[ODORE]",
	"[COLORE]",
	"[PH]",
	"[LASTLAB]",
	"[SZ3PRSOC]",
	"[SZ3CNTACC]",
	"[SZ4IN]",
	"[SZ5]",
	"[SZ6]",
	"[SZ2]",
	"[SZ3RSP]",
	"[SZ3PL]",
	"[SZ3OC]",
	"[SEZ7.1]",
	"[SEZ7.11]",
	"[SEZ7.12]",
	"[SEZ7.13]",
	"[SEZ7.2]",
	"[SEZ7.21]",
	"[SEZ7.22]",
	"[SEZ7.23]",
	"[SEZ7.24]",
	"[SEZ7.25]",
	"[SEZ7.26]",
	"[SEZ7.27]",
	"[SEZ7.28]",
	"[SEZ7.3]",
	"[SEZ7.31]",
	"[SEZ7.32]",
	"[SEZ7.33]",
	"[SEZ7.34]",
	"[SEZ7.35]",
	"[SEZ7.36]",
	"[SEZ7.37]",
	"[SEZ7.38]",
	"[SEZ7.39]",
	"[SEZ7.391]",
	"[SEZ7.4]",
	"[SEZ7.41]",
	"[SEZ7.42]",
	"[SEZ7.43]",
	"[SEZ7.44]",
	"[SEZ7.45]",
	"[PROCEDURA_ISO]"
	);

$FEDIT->FGE_FlushTableInfo();	
$FEDIT->FGE_UseTables(array("user_schede_sicurezza","user_schede_rifiuti","lov_cer","lov_stato_fisico_sk"));
$FEDIT->FGE_SetSelectFields(array("ID_SFSK","data_compilazione","odore","colore","pH","lastLab","note","ID_RSK"),"user_schede_sicurezza");
$FEDIT->FGE_SetSelectFields(array("RIUTILZ","carrello","pedana","cisterna","manuale", "prescrizioni_mov"),"user_schede_sicurezza");
$FEDIT->FGE_SetSelectFields(array("cr_carrello","cr_pianale","cr_cassone","cr_ragno","cr_cisterna","cr_manuale"),"user_schede_sicurezza");
$FEDIT->FGE_SetSelectFields(array("description"),"lov_stato_fisico_sk");
$FEDIT->FGE_SetSelectFields(array("cnt_fustim","cnt_fustip","cnt_container","cnt_contpl","cnt_sacchi","cnt_cisterna"),"user_schede_sicurezza");
$FEDIT->FGE_SetSelectFields(array("noacq","origine","stock","responsabile","cnt_bigbag"),"user_schede_sicurezza");
$FEDIT->FGE_SetSelectFields(array("descrizione","pericoloso","adr"),"user_schede_rifiuti");
//$FEDIT->FGE_SetSelectFields(array("ID_RIF","H1","H2","H3A","H3B","H4","H5","H6","H7","H8","H9","H10","H11","H12","H13","H14","H15"),"user_schede_rifiuti");
$FEDIT->FGE_SetSelectFields(array("ID_RIF","HP1","HP2","HP3","HP4","HP5","HP6","HP7","HP8","HP9","HP10","HP11","HP12","HP13","HP14","HP15"),"user_schede_rifiuti");
$FEDIT->FGE_SetSelectFields(array("COD_CER","description"),"lov_cer");
$FEDIT->FGE_DescribeFields();
$FEDIT->FGE_SetFilter($_GET["pri"],$_GET["filter"],"user_schede_sicurezza");
$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);

$ReplacedWith = array(
	"\"" .  $FEDIT->DbRecordSet[0]["lov_cerCOD_CER"] . " - " . $FEDIT->DbRecordSet[0]["lov_cerdescription"] . "\"",
	$FEDIT->DbRecordSet[0]["lov_cerCOD_CER"],
	$FEDIT->DbRecordSet[0]["user_schede_rifiutidescrizione"],
	date("d/m/Y",strtotime($FEDIT->DbRecordSet[0]["user_schede_sicurezzadata_compilazione"])),
	$FEDIT->DbRecordSet[0]["user_schede_sicurezzanote"],
	$FEDIT->DbRecordSet[0]["lov_stato_fisico_skdescription"],
	$FEDIT->DbRecordSet[0]["user_schede_sicurezzaodore"],
	$FEDIT->DbRecordSet[0]["user_schede_sicurezzacolore"],
	$FEDIT->DbRecordSet[0]["user_schede_sicurezzapH"],
	($FEDIT->DbRecordSet[0]["user_schede_sicurezzalastLab"]? date("d/m/Y",strtotime($FEDIT->DbRecordSet[0]["user_schede_sicurezzalastLab"])):'')
	);
	
$template = "SCH_SIC.rtf";
$FileName =  date("d-m-Y") . "--Cer" . $FEDIT->DbRecordSet[0]["lov_cerCOD_CER"] . "--Procedura.di.Sicurezza.rtf";
$StatoFisico = $FEDIT->DbRecordSet[0]["user_schede_sicurezzaID_SFSK"];
$Rischio = $FEDIT->DbRecordSet[0]["user_schede_sicurezzaID_RSK"];
$adr = $FEDIT->DbRecordSet[0]["user_schede_rifiutiadr"];

$ID_RIF=$FEDIT->DbRecordSet[0]["user_schede_rifiutiID_RIF"];



#
#	SEZIONE 3 - PRIMO SOCCORSO (HARD CODED)
#
//{{{ 
$tmp ="";
if($FEDIT->DbRecordSet[0]["user_schede_rifiutiHP6"]=='0' && $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP8"]=='0') {
	$tmp .= "Contattare un medico se sono presenti sintomi.\par ";
} else {
	$tmp .= "Contattare urgentemente un medico se sono presenti sintomi. Contattare un CAV (Centro Anti Veleni) per maggiori informazioni.\par ";
	$tmp .= "Le persone venute a contatto con la materia possono aver subito effetti anche se non sono presenti sintomi. Consigliare comunque una visita medica.\par ";
}
$ReplacedWith[] = $tmp; //}}}
#
#	SEZIONE 3 - CONTATTO ACCIDENTALE (HARD CODED)
#
//{{{ 
$tmp ="";
if($FEDIT->DbRecordSet[0]["user_schede_rifiutiHP6"]!='0' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP8"]!='0' && $StatoFisico=='4') {
	$tmp .= "CONTATTO CON LA PELLE: togliere immediatamente gli abiti impregnati, lavare la pelle con molta acqua e sapone. Consultare il medico.\par ";
} elseif ($StatoFisico=='4') {
	$tmp .= "CONTATTO CON LA PELLE: togliere gli abiti impregnati, lavare la pelle con molta acqua.\par ";
}
if($StatoFisico=='1' | $StatoFisico=='2') {
	$tmp .= "CONTATTO CON LA PELLE: togliere gli abiti che sono stati a contatto del rifiuto, lavare la pelle con molta acqua.\par ";	
}
if($FEDIT->DbRecordSet[0]["user_schede_rifiutiHP6"]!='0' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP8"]!='0') {
	$tmp .= "CONTATTO CON GLI OCCHI: lavare immediatamente con molta acqua tenendo la palpebra aperta, per almeno 10 minuti.\par ";
} else {
	$tmp .= "CONTATTO CON GLI OCCHI: lavare immediatamente con molta acqua tenendo la palpebra aperta, per almeno 3 minuti.\par ";
}
$tmp .= "INGESTIONE: consultare il medico per le cure del caso. Somministrare carbone attivato.\par ";
if($FEDIT->DbRecordSet[0]["user_schede_rifiutiHP6"]!='0' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP8"]!='0') {
	$tmp .= "INALAZIONE: in caso di esposizione alle nebbie o ai vapori, trasportare l'infortunato in ambiente pulito, e contattare un medico. Somministrare ossigeno e ventilare, se necessario.\par ";
} else {
	$tmp .= "INALAZIONE: in caso di esposizione prolungata o ad elevate concentrazioni di nebbie o  vapori, trasportare l'infortunato in ambiente pulito, e contattare un medico. Somministrare ossigeno e ventilare, se necessario.\par ";
}
$ReplacedWith[] = $tmp; //}}}
#
#	SEZIONE 4 - (HARD CODED)
#
//{{{ 
$tmp ="";
if($StatoFisico=='1' | $StatoFisico=='2') {
	$tmp .= "NELLE OPERAZIONI DI CONFEZIONAMENTO Collocare i rifiuti solo nei contenitori previsti. Verificare che siano chiusi al termine del riempimento. Prima di eseguire il riempimento, verificare che non vi siano residui di precedenti materiali.\par ";	
}
if($StatoFisico=='4') {
	$tmp .= "NELLE OPERAZIONI DI CONFEZIONAMENTO Collocare i rifiuti solo nei contenitori previsti. Verificare che i tappi siano serrati al termine del riempimento. Prima di eseguire il riempimento, verificare che non vi siano residui di precedenti materiali.\par ";	
}
if($adr =='1' && $FEDIT->DbRecordSet[0]["user_schede_sicurezzaRIUTILZ"]=='1' ) {
	$tmp .= "NON riutilizzare mai contenitori di recupero per il confezionamento,  anche se puliti e in buone condizioni.\par ";	
}
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzaRIUTILZ"]=='0' ) {
	$tmp .= "Utilizzare contenitori di recupero solo se puliti e in buone condizioni.\par ";
}
$tmp .= "NELLE OPERAZIONI DI IMMAGAZZINAMENTO. Collocare i contenitori dei rifiuti solo nelle aree previste, verificando la corrispondenza delle etichette sui contenitori. Maneggiare con cura.\par ";
$tmp .= "Impilare i colli solo se espressamente previsto. Lasciare libero lo spazio per la manovra dei mezzi di sollevamento e di carico.\par ";
if($StatoFisico=='4') {
	$tmp .= "Disporre i contenitori sopra le vasche di contenimento o nelle aree dotate di sistemi di raccolta dei liquidi.\par ";	
}
$tmp .= "Verificare che tutti i contenitori abbiano etichetta di riconoscimento con il CER o specifica descrizione.\par ";
if($FEDIT->DbRecordSet[0]["user_schede_rifiutipericoloso"]!='0') {
	$tmp .= "Verificare che tutti i contenitori abbiano etichetta con \"R\" su campo Giallo.\par ";	
}
if($FEDIT->DbRecordSet[0]["user_schede_rifiutiadr"]!='0') {
	$tmp .= "Verificare che tutti i contenitori abbiano etichetta \"a losanga\" per il trasporto.\par ";	
}
$tmp .= "\par MEZZI DI MOVIMENTAZIONE PREVISTI E AMMESSI PER LA MOVIMENTAZIONE E L'IMMAGAZZINAMENTO:\par ";
#"carrello","pedana","cisterna","manuale"
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzacarrello"]=='1') {
	$tmp .= "carrello elevatore.\par ";
}
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzapedana"]=='1') {
	$tmp .= "pedana mobile.\par ";
}
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzacisterna"]=='1') {
	$tmp .= "cisterna mobile.\par ";
}
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzamanuale"]=='1') {
	$tmp .= "movimentazione manuale.\par ";
}
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzamanuale"]=='0' && $FEDIT->DbRecordSet[0]["user_schede_sicurezzapedana"]=='0'  && $FEDIT->DbRecordSet[0]["user_schede_sicurezzacisterna"]=='0' && $FEDIT->DbRecordSet[0]["user_schede_sicurezzamanuale"]=='0') {
	$tmp .= "nessuno.\par ";
}
$tmp .= "NELLE OPERAZIONI DI CARICO: prestare attenzione ai veicoli in movimento e alle attrezzature nell'area. Allontanare le persone non addette alle operazioni, se necessario. Procedere sempre a velocit� ridotta. Verificare la corrispondenza tra il carico e la documentazione fornita (Formulario).\par ";
if($FEDIT->DbRecordSet[0]["user_schede_rifiutiadr"]!='0') {
	$tmp .= "NELLE OPERAZIONI DI CARICO: prestare attenzione ai veicoli in movimento e alle attrezzature nell' area. Allontanare le persone non addette alle operazioni, se necessario. Procedere sempre a velocit� ridotta. Verificare la corrispondenza tra il carico e la documentazione fornita (Formulario e Scheda di Trasporto). Controllare le etichette dei contenitori e sul veicolo (per l'ADR).\par ";	
}
$tmp .= "\par MEZZI DI MOVIMENTAZIONE PREVISTI E AMMESSI PER IL CARICO DEI VEICOLI:\par ";
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzacr_carrello"]=='1') {
	$tmp .= "carrello elevatore.\par ";
}
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzacr_pianale"]=='1') {
	$tmp .= "pianale mobile.\par ";
}
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzacr_cassone"]=='1') {
	$tmp .= "cassone scarrabile.\par ";
}
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzacr_ragno"]=='1') {
	$tmp .= "ragno.\par ";
}
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzacr_cisterna"]=='1') {
	$tmp .= "cisterna mobile.\par ";
}
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzacr_manuale"]=='1') {
	$tmp .= "carico manuale.\par ";
}
$tmp .= "\par CONTENITORI PREVISTI E AMMESSI:\par ";
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzacnt_fustim"]=='1') {
	$tmp .= "fusti metallici.\par ";
}
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzacnt_fustip"]=='1') {
	$tmp .= "fusti in plastica.\par ";
}
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzacnt_container"]=='1') {
	$tmp .= "container.\par ";
}
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzacnt_contpl"]=='1') {
	$tmp .= "contenitori in plastica 1 mc.\par ";
}
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzacnt_sacchi"]=='1') {
	$tmp .= "sacchi.\par ";
}
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzacnt_cisterna"]=='1') {
	$tmp .= "cisterna.\par ";
}
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzacnt_bigbag"]=='1') {
	$tmp .= "big bags.\par ";
}
$tmp .= "Non utilizzare contenitori non previsti, o in cattive condizioni d'uso.\par ";
if($FEDIT->DbRecordSet[0]["user_schede_rifiutiHP3"]=='1') {
	$tmp .= "NELLE OPERAZIONI DI CARICO. Spegnere il motore. Non usare apparecchiature elettriche se non indispensabili, non usare fiamme libere e non fumare. Disporre i segnali di avvertimento e/o recintare a zona.\par ";	
}
if($FEDIT->DbRecordSet[0]["user_schede_rifiutiHP5"]=='1' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP6"]=='1' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP7"]=='1' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP10"]=='1' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP11"]=='1') {
	$tmp .= "Allontanare le persone ed i mezzi estranei dalla zona pericolosa.\par ";
	$tmp .= "Eseguire le operazioni di carico cercando di mantenersi sopra vento. \par ";
	}

$tmp .= "\par PRESCRIZIONI PARTICOLARI PER LA MOVIMENTAZIONE:\par ";
if(trim($FEDIT->DbRecordSet[0]["user_schede_sicurezzaprescrizioni_mov"])!='')
	$tmp .= $FEDIT->DbRecordSet[0]["user_schede_sicurezzaprescrizioni_mov"]."\par ";
else
	$tmp .= "Nessuna prescrizione particolare per la movimentazione.\par ";


$ReplacedWith[] = $tmp; //}}}


#
#	SEZIONE 5 - (HARD CODED)
#
//{{{ 
$tmp ="";
if($FEDIT->DbRecordSet[0]["user_schede_rifiutiHP6"]=='1') {
	$tmp .= "Informare tutte le persone presenti del rischio e consigliare di chiudere porte e finestre. Non rimanere nei pressi della zone di sversamento. \par ";
}
$tmp .= "In caso di sversamento in ambienti chiusi: aerare l�ambiente.\par ";
$tmp .= "CONTENERE O ARRESTARE LE PERDITE DEL CARICO DI PICCOLA QUANTITA',  in particolare se il materiale pu� defluire nei corsi d'acqua e nei tombini o altre condutture sotterranee.\par ";
if($StatoFisico=='4') {
	$tmp .= "EQUIPAGGIAMENTI NECESSARI - un telo gommato copri tombino; un recipiente di raccolta; tubi di drenaggio; - una scopa;- una confezione di sabbia asciutta (o altro materiale assorbente) da almeno 10 kg; stracci assorbenti.\par ";	
}
if($StatoFisico!='4' && $StatoFisico!='5' ) {
	$tmp .= "EQUIPAGGIAMENTI NECESSARI - un recipiente di raccolta; - una scopa o una pala;- una confezione di sabbia asciutta (o altro materiale assorbente) da almeno 10 kg; stracci assorbenti.\par ";	
}
$tmp .= "Avvisare il diretto responsabile. Il materiale impiegato per la raccolta del rifiuto fuoriuscito deve essere collocato in contenitori a parte, e non nel contenitore originario.\par ";
$tmp .= "Verificare l'integrit� degli imballi e dei contenitori prima di concludere il carico, e se presentano danni visibili sostituirli.\par ";
if($FEDIT->DbRecordSet[0]["user_schede_rifiutiHP5"]=='1' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP6"]=='1' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP7"]=='1' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP10"]=='1' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP11"]=='1') {
	$tmp .= "NON CERCARE DI CONTENERE LE PERDITE DEL CARICO, NEPPURE DI PICCOLA QUANTITA', neanche se il materiale pu� defluire nei corsi d'acqua e nei tombini o altre condutture sotterranee.\par ";
}
if($FEDIT->DbRecordSet[0]["user_schede_rifiutiHP7"]=='1' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP10"]=='1' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP11"]=='1') {
	$tmp .= "Mantenere la calma. AVVISARE il diretto responsabile IMMEDIATAMENTE. Allontanare i presenti (compreso l'autista). Delimitata la zona, rimanere a distanza di sicurezza e impedire ad altri di avvicinarsi.\par ";
}
if($FEDIT->DbRecordSet[0]["user_schede_rifiutiHP5"]=='1' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP6"]=='1') {
	$tmp .= "Mantenere la calma. AVVISARE il diretto responsabile IMMEDIATAMENTE. Allontanare i presenti (compreso l'autista) e mettersi al sicuro, mantenendosi sopravento.\par ";
}
if( ($FEDIT->DbRecordSet[0]["user_schede_rifiutiHP5"]!='1' && $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP6"]!='1' && $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP7"]!='1' && $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP10"]!='1' && $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP11"]!='1') && $StatoFisico=='4') {
	$tmp .= "Cospargere e contenere le perdite con sabbia o con idoneo materiale assorbente ed impedire che il liquido fuoriuscito (ed i vapori) penetrino nelle fogne, cantine, corsi d'acqua.\par ";
}
if( ($FEDIT->DbRecordSet[0]["user_schede_rifiutiHP5"]!='1' && $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP6"]!='1' && $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP7"]!='1' && $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP10"]!='1' && $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP11"]!='1') && $StatoFisico=='5' && $StatoFisico=='4') {
	$tmp .= "Cospargere e contenere le perdite con sabbia o con idoneo materiale assorbente ed impedire che il materiale fuoriuscito penetrino nelle fogne, cantine, corsi d'acqua.\par ";
}
if( ($FEDIT->DbRecordSet[0]["user_schede_rifiutiHP5"]!='1' && $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP6"]!='1' && $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP7"]!='1' && $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP10"]!='1' && $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP11"]!='1')) {
	$tmp .= "Bloccare le perdite tenendosi sopravvento.\par ";
	$tmp .= "In caso di fuoriuscite nell'area di stoccaggio o di carico, pulire completamente la zona prima di allontanarsi. Tutti gli altri rifiuti e le merci coinvolte devono essere controllati per prevenire qualsiasi contaminazione.\par ";
	$tmp .= "Effettuare le operazioni descritte a condizione che possa essere fatto senza rischi per l�autista e per le persone presenti.\par ";
}
$tmp .= "\par RISCHIO DI INCENDIO\par ";
if( $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP3"]!='1') {
	$tmp .= "Il rifiuto non � infiammabile.\par ";
} else {
	$tmp .= "Il rifiuto � facilmente infiammabile, anche alla normale temperatura ambiente. Tenere lontano da qualsiasi scintilla, innesco di fiamma o ignizione e proteggere dalle fonti di calore.\par ";	
}
if($FEDIT->DbRecordSet[0]["user_schede_rifiutiHP2"]=='1') {
	$tmp .= "Il rifiuto pu� facilmente innescare incendi, anche alla normale temperatura ambiente. Tenere lontano da qualsiasi contatto con altre materie, scintille, innesco di fiamma o ignizione e proteggere dalle fonti di calore.\par ";
	$tmp .= "Aumenta il rischio di incendi. Pu� provocare l'accensione di altre materie.\par ";
}
if( $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP5"]=='1' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP6"]=='1' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP7"]=='1' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP10"]=='1' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP11"]=='1' ) {
	$tmp .= "Tenersi sopravento. Non avvicinarsi per non respirare i fumi e i vapori. Pericolo di avvelenamento, anche per esposizione di breve durata. Informare le persone circostanti del pericolo. Se l'incendio si sviluppa dentro locali chiusi, far evacuare se possibile l'aria inquinata verso l'esterno.\par ";
}
if( $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP4"]=='1' | $FEDIT->DbRecordSet[0]["user_schede_rifiutiHP8"]=='1') {
	$tmp .= "Tenersi sopravento. Non avvicinarsi per non respirare i fumi e i vapori. Pericolo di gravi danni alla pelle agli occhi ed alle vie respiratorie, anche per esposizione di breve durata. Informare le persone circostanti del pericolo. Se l'incendio si sviluppa dentro locali chiusi, far evacuare se possibile l'aria inquinata verso l'esterno.\par ";
}
$tmp .= "L'operatore deve intervenire solo in caso di incendio ad un contenitore, al veicolo di carico, e solo se di piccola entit�: non intervenire se l�incendio coinvolge l'intera area o l'intero carico.\par ";
if($FEDIT->DbRecordSet[0]["user_schede_sicurezzanoacq"]!='1') {
	$tmp .= "Estinguere con estintori a polvere chimica, a polvere o ad anidride carbonica indirizzando il getto alla base delle fiamme. Evitare, se possibile, l�uso di acqua.\par ";
} else {
	$tmp .= "Estinguere solo con estintori a polvere chimica, a polvere o ad anidride carbonica indirizzando il getto alla base delle fiamme. Evitare assolutamente l�uso di acqua. In tale caso la materia reagirebbe violentemente.\par ";
}
$tmp .= "Raffreddare le superfici dei contenitori esposti alle fiamme con acqua.\par ";
$tmp .= "Allontanare persone e mezzi dalla zona circostante l'incendio.\par ";
$tmp .= "EQUIPAGGIAMENTI VARI - estintore da 2 kg per incendio del veicolo; estintore da 6 kg per incendio dei contenitori; - una veste fluorescente o una bandoliera, a norma EN 471 per ogni membro in fase di spegnimento.\par ";
$tmp .= "Avvertire al pi� presto il Responsabile Aziendale ed i Vigili del Fuoco (115).\par ";
$ReplacedWith[] = $tmp; //}}}
#
#	SEZIONE 6
#
$tmp = "";
$tmp .= "Origine del rifiuto - lavorazione: " . $FEDIT->DbRecordSet[0]["user_schede_sicurezzaorigine"] . "\par ";
$tmp .= "Area di stoccaggio: " . $FEDIT->DbRecordSet[0]["user_schede_sicurezzastock"] . "\par ";
$tmp .= "Responsabile ambientale: " . $FEDIT->DbRecordSet[0]["user_schede_sicurezzaresponsabile"] . "\par ";
$ReplacedWith[] = $tmp; 
#
#	SEZIONE 2
#
//{{{ 
if($FEDIT->DbRecordSet[0]["user_schede_rifiutipericoloso"]!='0') {
	$FiltroSF = " (ID_SFSK='' OR ID_SFSK LIKE '%" . $FEDIT->DbRecordSet[0]["user_schede_sicurezzaID_SFSK"] . "%' ";
		if($FEDIT->DbRecordSet[0]["user_schede_rifiutiadr"]!='0') {
			$FiltroSF .= " OR adr='1' ";
		}
	$FiltroSF .= ") ";
	$FiltroHP = "";
        $tmp = "";
	foreach($FEDIT->DbRecordSet[0] as $FName=>$FValue) {
            if(strpos($FName,"user_schede_rifiutiHP")!==false && $FValue=="1") {
                $FiltroHP .= str_replace("user_schede_rifiuti","",$FName)  . "='$FValue' OR ";
                $HP = str_replace("user_schede_rifiuti","",$FName);
                #***agg hp
                $sqlSez2 = "SELECT CONCAT(DESCR, ' - ', DESCR_EXTENDED) AS HP FROM lov_classi_hp WHERE CL_HP='".$HP."';";
                $FEDIT->SDBRead($sqlSez2,"DbRecordSet",true,false);
                $tmp .= $FEDIT->DbRecordSet[0]['HP'] . "\par ";
                #***fine agg hp
                }
            }
//	if($FiltroHP != "") {
//		$FiltroHP =  substr($FiltroHP, 0, -3);	
//	}
//	$sqlSez2 = "SELECT frase FROM data_sk_sez2 WHERE (" . $FiltroSF . "AND (" . $FiltroH . "))";
//	$FEDIT->FGE_FlushTableInfo();	
//	$FEDIT->SDBRead($sqlSez2,"DbRecordSet",true,false);
//	$tmp = "";
//	foreach($FEDIT->DbRecordSet as $FName=>$FValue) {
//		foreach($FValue as $k=>$frase) {
//			$tmp .= $frase . "\par ";
//		}	
//	}
} else {
	$tmp = "Rifiuto classificato non pericoloso. Privo di rischi specifici.\par ";
	$tmp .= "Rifiuto classificato non pericoloso. Seguire le norme e procedure di sicurezza previste per la movimentazione e stoccaggio del rifiuto. Segnalare al Responsabile qualsiasi anomalia dei contenitori e della sua conservazione. Aprire i contenitori solo per le attivit� di riempimento, controllo e manutenzione.";
}
$ReplacedWith[] = $tmp; //}}}
#
#	SEZIONE 3 - VIE RESPIRATORIE
#
//{{{ 
$sqlSez3 = "SELECT frase FROM data_sk_sez3_resp WHERE RSK='" . $Rischio . "'";
$sqlSez3 .= " AND (SFY='" . $StatoFisico .  "' OR SFY='') AND (SFN<>'" .$StatoFisico . "' OR SFN='')";
$FEDIT->FGE_FlushTableInfo();	
$FEDIT->SDBRead($sqlSez3,"DbRecordSet",true,false);
$tmp = "";
	foreach($FEDIT->DbRecordSet as $FName=>$FValue) {
		foreach($FValue as $k=>$frase) {
			$tmp .= $frase . "\par ";
		}	
	}
$ReplacedWith[] = $tmp; //}}}
#
#	SEZIONE 3 - PELLE
#
//{{{ 
$sqlSez3 = "SELECT frase FROM data_sk_sez3_pl WHERE RSK='" . $Rischio . "'";
$FEDIT->FGE_FlushTableInfo();	
$FEDIT->SDBRead($sqlSez3,"DbRecordSet",true,false);
$tmp = "";
	foreach($FEDIT->DbRecordSet as $FName=>$FValue) {
		foreach($FValue as $k=>$frase) {
			$tmp .= $frase . "\par ";
		}	
	}
$ReplacedWith[] = $tmp; //}}}
#
#	SEZIONE 3 - OCCHI
#
//{{{ 
$sqlSez3 = "SELECT frase FROM data_sk_sez3_oc WHERE RSK='" . $Rischio . "'";
$sqlSez3 .= " AND (SFY='" . $StatoFisico .  "' OR SFY='') AND (SFN<>'" .$StatoFisico . "' OR SFN='')";
$FEDIT->FGE_FlushTableInfo();	
$FEDIT->SDBRead($sqlSez3,"DbRecordSet",true,false);
$tmp = "";
	foreach($FEDIT->DbRecordSet as $FName=>$FValue) {
		foreach($FValue as $k=>$frase) {
			$tmp .= $frase . "\par ";
		}	
	}
$ReplacedWith[] = $tmp; //}}}







#
#	SEZIONE 7 - SEGNALETICA
#


## SEGNALETICA SICUREZZA



# controllare se c'� record x segnaletica di sicurezza, altrimenti faccio insert
$sql = "SELECT * FROM user_schede_rifiuti_etsym WHERE ID_RIF='".$ID_RIF."';";
$FEDIT->SdbRead($sql,"DbRecordSetSym",true,false);


# NESSUN RECORD, CALCOLO SEGNALETICA PARTENDO DA CLASSI H
if(!isset($FEDIT->DbRecordSetSym[0])){
	//$sql = "SELECT ID_SF, pericoloso, H1, H2, H3A, H3B, H4, H5, H6, H7, H8, H9, H10, H11, H12, H13, H14, H15 FROM user_schede_rifiuti WHERE ID_RIF='".$FEDIT->DbRecordSet[0]["user_schede_rifiutiID_RIF"]."';";
    $sql = "SELECT ID_SF, pericoloso, HP1, HP2, HP3, HP4, HP5, HP6, HP7, HP8, HP9, HP10, HP11, HP12, HP13, HP14, HP15 FROM user_schede_rifiuti WHERE ID_RIF='".$FEDIT->DbRecordSet[0]["user_schede_rifiutiID_RIF"]."';";
	$FEDIT->SdbRead($sql,"DbRecordSetInfoRif",true,false);

	## SEMPRE
	$segnaletica["P002"]="1";	
	$segnaletica["M009"]="1";
	$segnaletica["M008"]="1";
	
	## INDEFINITO
	$segnaletica["W003"]="0";
	$segnaletica["M014"]="0";
	$segnaletica["M001"]="0";
	$segnaletica["E013"]="0";
	$segnaletica["E004"]="0";
	
	if($FEDIT->DbRecordSetInfoRif[0]["HP3"]=="1"){
		$segnaletica["P003"]="1";
		$segnaletica["W021"]="1";
		}
	else{
		$segnaletica["P003"]="0";
		$segnaletica["W021"]="0";
		}

	if($FEDIT->DbRecordSetInfoRif[0]["HP2"]=="1"){
		$segnaletica["P011"]="1";
		$segnaletica["W028"]="1";
		}
	else{
		$segnaletica["P011"]="0";
		$segnaletica["W028"]="0";
		}

	if($FEDIT->DbRecordSetInfoRif[0]["HP1"]=="1")
		$segnaletica["W002"]="1";
	else
		$segnaletica["W002"]="0";

	if($FEDIT->DbRecordSetInfoRif[0]["HP6"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP7"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP10"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP11"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP12"]=="1")
		$segnaletica["W016"]="1";
	else
		$segnaletica["W016"]="0";

	if($FEDIT->DbRecordSetInfoRif[0]["HP8"]=="1")
		$segnaletica["W023"]="1";
	else
		$segnaletica["W023"]="0";

	if($FEDIT->DbRecordSetInfoRif[0]["HP4"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP5"]=="1")
		$segnaletica["W001"]="1";
	else
		$segnaletica["W001"]="0";

	if($FEDIT->DbRecordSetInfoRif[0]["ID_SF"]=="4" | $FEDIT->DbRecordSetInfoRif[0]["pericoloso"]=="1")
		$segnaletica["M004"]="1";
	else
		$segnaletica["M004"]="0";

	if($FEDIT->DbRecordSetInfoRif[0]["ID_SF"]=="2" | $FEDIT->DbRecordSetInfoRif[0]["pericoloso"]=="1")
		$segnaletica["M016"]="1";
	else
		$segnaletica["M016"]="0";

	if($FEDIT->DbRecordSetInfoRif[0]["ID_SF"]=="4" | $FEDIT->DbRecordSetInfoRif[0]["HP7"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP10"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP11"]=="1")
		$segnaletica["M010"]="1";
	else
		$segnaletica["M010"]="0";

	if($FEDIT->DbRecordSetInfoRif[0]["ID_SF"]=="4" | $FEDIT->DbRecordSetInfoRif[0]["HP8"]=="1"){
		$segnaletica["M013"]="1";
		$segnaletica["E012"]="1";
		}
	else{
		$segnaletica["M013"]="0";
		$segnaletica["E012"]="0";
		}

	if($FEDIT->DbRecordSetInfoRif[0]["pericoloso"]=="1")
		$segnaletica["E003"]="1";
	else
		$segnaletica["E003"]="0";

	if($FEDIT->DbRecordSetInfoRif[0]["HP4"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP8"]=="1")
		$segnaletica["E011"]="1";
	else
		$segnaletica["E011"]="0";


	}
else{
	$segnaletica["P003"]=$FEDIT->DbRecordSetSym[0]["P003"];
	$segnaletica["P011"]=$FEDIT->DbRecordSetSym[0]["P011"];
	$segnaletica["P002"]=$FEDIT->DbRecordSetSym[0]["P002"];
	$segnaletica["M004"]=$FEDIT->DbRecordSetSym[0]["M004"];
	$segnaletica["M016"]=$FEDIT->DbRecordSetSym[0]["M016"];
	$segnaletica["M013"]=$FEDIT->DbRecordSetSym[0]["M013"];
	$segnaletica["M009"]=$FEDIT->DbRecordSetSym[0]["M009"];
	$segnaletica["M008"]=$FEDIT->DbRecordSetSym[0]["M008"];
	$segnaletica["M001"]=$FEDIT->DbRecordSetSym[0]["M001"];
	$segnaletica["M014"]=$FEDIT->DbRecordSetSym[0]["M014"];
	$segnaletica["M010"]=$FEDIT->DbRecordSetSym[0]["M010"];
	$segnaletica["M015"]=$FEDIT->DbRecordSetSym[0]["M015"];
	$segnaletica["M003"]=$FEDIT->DbRecordSetSym[0]["M003"];
	$segnaletica["W003"]=$FEDIT->DbRecordSetSym[0]["W003"];
	$segnaletica["W016"]=$FEDIT->DbRecordSetSym[0]["W016"];
	$segnaletica["W021"]=$FEDIT->DbRecordSetSym[0]["W021"];
	$segnaletica["W023"]=$FEDIT->DbRecordSetSym[0]["W023"];
	$segnaletica["W028"]=$FEDIT->DbRecordSetSym[0]["W028"];
	$segnaletica["W002"]=$FEDIT->DbRecordSetSym[0]["W002"];
	$segnaletica["W001"]=$FEDIT->DbRecordSetSym[0]["W001"];
	$segnaletica["W022"]=$FEDIT->DbRecordSetSym[0]["W022"];
	$segnaletica["E003"]=$FEDIT->DbRecordSetSym[0]["E003"];
	$segnaletica["E012"]=$FEDIT->DbRecordSetSym[0]["E012"];
	$segnaletica["E011"]=$FEDIT->DbRecordSetSym[0]["E011"];
	$segnaletica["E013"]=$FEDIT->DbRecordSetSym[0]["E013"];
	$segnaletica["E004"]=$FEDIT->DbRecordSetSym[0]["E004"];
	}

if($segnaletica["P003"]=="1" | $segnaletica["P011"]=="1" | $segnaletica["P002"]=="1") 
	$divieto=true; else $divieto=false;

if($segnaletica["M004"]=="1" | $segnaletica["M016"]=="1" | $segnaletica["M013"]=="1" | $segnaletica["M009"]=="1" | $segnaletica["M008"]=="1" | $segnaletica["M001"]=="1" | $segnaletica["M014"]=="1" | $segnaletica["M010"]=="1" | $segnaletica["M015"]=="1" | $segnaletica["M003"]=="1") 
	$prescrizione=true; else $prescrizione=false;

if($segnaletica["W003"]=="1" | $segnaletica["W016"]=="1" | $segnaletica["W021"]=="1" | $segnaletica["W023"]=="1" | $segnaletica["W028"]=="1" | $segnaletica["W002"]=="1" | $segnaletica["W001"]=="1" | $segnaletica["W022"]=="1") 
	$avvertimento=true; else $avvertimento=false;

if($segnaletica["E003"]=="1" | $segnaletica["E012"]=="1" | $segnaletica["E011"]=="1" | $segnaletica["E013"]=="1" | $segnaletica["E004"]=="1") 
	$salvataggio=true; else $salvataggio=false;


$directory="etichette/sicurezza/divieto/ruotate/";

if($divieto) $sez71="SEGNALI DI DIVIETO"; else $sez71=""; $ReplacedWith[] = $sez71;
if($divieto){
	if($segnaletica["P003"]=="1"){
		$image=$directory."P003.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;

		
	if($segnaletica["P011"]=="1"){
		$image=$directory."P011.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	

	if($segnaletica["P002"]=="1"){
		$image=$directory."P002.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;

}
else{
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	}

$directory="etichette/sicurezza/pericolo/ruotate/";


if($avvertimento) $sez72="SEGNALI DI PERICOLO"; else $sez72=""; $ReplacedWith[] = $sez72;
if($avvertimento){

	if($segnaletica["W003"]=="1"){
		$image=$directory."W003.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
				}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
	
	if($segnaletica["W016"]=="1"){
		$image=$directory."W016.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
	if($segnaletica["W021"]=="1"){
		$image=$directory."W021.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
	if($segnaletica["W023"]=="1"){
		$image=$directory."W023.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
	if($segnaletica["W028"]=="1"){
		$image=$directory."W028.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
	if($segnaletica["W002"]=="1"){
		$image=$directory."W002.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;

	if($segnaletica["W001"]=="1"){
		$image=$directory."W001.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
	
	if($segnaletica["W022"]=="1"){
		$image=$directory."W022.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
}
else{
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	}


$directory="etichette/sicurezza/obbligo/ruotate/";


if($prescrizione) $sez73="SEGNALI DI OBBLIGO"; else $sez73=""; $ReplacedWith[] = $sez73;
if($prescrizione){
	
	if($segnaletica["M004"]=="1"){
		$image=$directory."M004.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
	if($segnaletica["M016"]=="1"){
		$image=$directory."M016.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
	if($segnaletica["M013"]=="1"){
		$image=$directory."M013.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
	if($segnaletica["M009"]=="1"){
		$image=$directory."M009.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
	if($segnaletica["M008"]=="1"){
		$image=$directory."M008.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
	if($segnaletica["M001"]=="1"){
		$image=$directory."M001.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
	if($segnaletica["M014"]=="1"){
		$image=$directory."M014.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
	if($segnaletica["M010"]=="1"){
		$image=$directory."M010.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
	
	
	if($segnaletica["M015"]=="1"){
		$image=$directory."M015.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;



	if($segnaletica["M003"]=="1"){
		$image=$directory."M003.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
}
else{
	$ReplacedWith[] = "";	
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	}


$directory="etichette/sicurezza/emergenza/ruotate/";

if($salvataggio) $sez74="SEGNALI DI EMERGENZA"; else $sez74=""; $ReplacedWith[] = $sez74;
if($salvataggio){
	if($segnaletica["E003"]=="1"){
		$image=$directory."E003.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
	if($segnaletica["E012"]=="1"){
		$image=$directory."E012.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;

	if($segnaletica["E011"]=="1"){
		$image=$directory."E011.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
	if($segnaletica["E013"]=="1"){
		$image=$directory."E013.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;


	if($segnaletica["E004"]=="1"){
		$image=$directory."E004.jpg";
		if(file_exists($image)) { 
			$newImage="";
			$b=fopen($image,"rb");
			$imgData=getimagesize($image);
			$newImagePre=" {\\*\\shppict{\\pict \\jpegblip \\picw".$imgData[0]." \\pich".$imgData[1]." \\wbmbitspixel24 ";
			while (!feof($b)) {
				$newImage.= fgets($b);
			}
			$hex=bin2hex($newImage);
			$imgDat=$newImagePre.$hex."}}";
			}
		else
			$imgDat="Immagine non disponibile.";
		}
	else{
		$imgDat="";
		}

	$ReplacedWith[] = $imgDat;
	
	
}
else{
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	$ReplacedWith[] = "";
	}




## PROCEDURA ISO - ID_ISOF=11
$SQL="SELECT procedura FROM core_impianti_iso WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1 AND ID_ISOF=11;";
$FEDIT->SdbRead($SQL, "DbRecordSet");
$PROCEDURA=$FEDIT->DbRecordSet[0]["procedura"];
if(!is_null($PROCEDURA))
	$ReplacedWith[]="Documento elaborato in ottemperanza alla procedura cliente: ".$PROCEDURA;
else
	$ReplacedWith[]="";


StreamOut($template,$FileName,$ToBeReplaced,$ReplacedWith);

require_once("../__includes/COMMON_sleepForgEdit.php");
?>
