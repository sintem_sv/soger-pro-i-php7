<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

$_SESSION['MudFileSize']=0;
//$_SESSION['EOL']=chr(13);
$_SESSION['EOL']=chr(13).chr(10);

function createMudFile(){

	global $SOGER;
	global $FEDIT;
	$FEDIT->DbServerData["db"]="soger2012";
	$MUD2012 = "";

	$MovTable="user_movimenti_fiscalizzati";

#AA#


	## leggo dati unit� locale - core_impianti
	$sql ="SELECT codfisc as codice, ATECO_07, TRIM(REA) AS REA, TRIM(TotAddettiUL) AS TotAddettiUL, TRIM(core_impianti.description) as RagioneSociale, lov_comuni_istat.cod_prov as istatP,  lov_comuni_istat.cod_comISTAT as istatC, TRIM(indirizzo) as via, lov_comuni_istat.CAP as cap, TRIM(LR_nome) AS LR_nome, TRIM(LR_cognome) AS LR_cognome ";
	$sql.="FROM core_impianti ";
	$sql.="JOIN lov_comuni_istat ON core_impianti.ID_COM=lov_comuni_istat.ID_COM ";
	$sql.="LEFT JOIN lov_ateco2007 ON core_impianti.ID_ATECO_07=lov_ateco2007.ID_ATECO_07 ";
	$sql.="WHERE core_impianti.ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."'";
	$FEDIT->SdbRead($sql,"DbRecordset",true,false);

	## costante tipo record
	$AARecord ="AA;";
	
	## Anno di riferimento della dichiarazione (AAAA)
	$AARecord.="2012;";

	## codice fiscale identificativo
	$AARecord.=spaceFiller(strtoupper($FEDIT->DbRecordset[0]["codice"]),16).";";
	
	## info che user� di seguito
	$SL_codice		= spaceFiller(strtoupper($FEDIT->DbRecordset[0]["codice"]),16);
	$SL_LRC			= spaceFiller(strtoupper($FEDIT->DbRecordset[0]["LR_cognome"]),25);
	$SL_LRN			= spaceFiller(strtoupper($FEDIT->DbRecordset[0]["LR_nome"]),25);
	
	## codice di identificazione univoca dell'unit� locale - PM mette 0
	$AARecord.=spaceFiller(strtoupper($SOGER->UserData["core_usersID_IMP"]),15).";";

	## codice istat attivit� svolta ( senza punti e lettere )
	if(is_null($FEDIT->DbRecordset[0]["ATECO_07"]))
		$CodiceAttivitaATECO="      ";
	else
		$CodiceAttivitaATECO=zeroFiller_right(str_replace(".", "", $FEDIT->DbRecordset[0]["ATECO_07"]), 6);
	$AARecord.=$CodiceAttivitaATECO.";";

	## N� iscrizione Rep.Notizie Econ.Amm. (REA)
	$AARecord.=zeroFiller($FEDIT->DbRecordset[0]["REA"],9).";";

	## Totale addetti nell' unit� locale
	$AARecord.=zeroFiller($FEDIT->DbRecordset[0]["TotAddettiUL"],5).";";
	
	## Descrizione della ragione sociale
	$AARecord.=spaceFiller(strtoupper($FEDIT->DbRecordset[0]["RagioneSociale"]),60).";";
	
	## ISTAT Provincia dell' unit� locale
	$AARecord.=zeroFiller($FEDIT->DbRecordset[0]["istatP"],3).";";

	## ISTAT Comune dell' unit� locale
	$AARecord.=zeroFiller($FEDIT->DbRecordset[0]["istatC"],3).";";

	## via dell'unit� locale
	$AARecord.=spaceFiller($FEDIT->DbRecordset[0]["via"],30).";";

	## civico unit� locale
	$AARecord.="      ;";

	## cap unit� locale
	$AARecord.=zeroFiller($FEDIT->DbRecordset[0]["cap"],5).";";

	## prefisso telefonico unit� locale
	$AARecord.="     ;";

	## numero telefonico unit� locale
	$AARecord.="          ;";

	## leggo codice fiscale e ragione sociale per la SEDE LEGALE, per questo leggo da core_gruppi
	$sql ="SELECT lov_comuni_istat.cod_prov AS istatP, lov_comuni_istat.cod_comISTAT AS istatC, TRIM(core_gruppi.indirizzo) AS via, lov_comuni_istat.CAP as cap ";
	$sql.="FROM core_gruppi ";
	$sql.="JOIN lov_comuni_istat ON core_gruppi.ID_COM=lov_comuni_istat.ID_COM ";
	$sql.="WHERE core_gruppi.ID_GRP='".$SOGER->UserData["core_gruppiID_GRP"]."'";
	$FEDIT->SdbRead($sql,"DbRecordset",true,false);

	## info che user� di seguito
	$SL_istatP	 = zeroFiller($FEDIT->DbRecordset[0]["istatP"],3);
	$SL_istatC   = zeroFiller($FEDIT->DbRecordset[0]["istatC"],3);
	$SL_via		 = spaceFiller(strtoupper($FEDIT->DbRecordset[0]["via"]),30);
	$SL_civico	 = "      ";
	$SL_cap		 = zeroFiller($FEDIT->DbRecordset[0]["cap"],5);
	$SL_prefisso = "     ";
	$SL_telefono = "          ";
	
	## info che user� di seguito
	$UL_id = spaceFiller(strtoupper($SOGER->UserData["core_usersID_IMP"]),15);

	## ISTAT provincia sede legale
	$AARecord.=$SL_istatP.";";

	## ISTAT comune sede legale
	$AARecord.=$SL_istatC.";";

	## via sede legale
	$AARecord.=$SL_via.";";

	## civico sede legale
	$AARecord.=$SL_civico.";";

	## cap sede legale
	$AARecord.=$SL_cap.";";

	## prefisso sede legale
	$AARecord.=$SL_prefisso.";";

	## numero sede legale
	$AARecord.=$SL_telefono.";";

	## cognome legale rappresentante
	$AARecord.=$SL_LRC.";";

	## nome legale rappresentante
	$AARecord.=$SL_LRN.";";

	## data compilazione
	$AARecord.=date('Ymd').";";

	## mesi di attivit� nell'anno
	$AARecord.="12;";

	## Annulla e sostituisce
	$AARecord.="0;";

	## Data della dichiarazione sostituita
	$AARecord.="00000000;";

#BA#

	$sql ="SELECT lov_cer.COD_CER as cer, user_schede_rifiuti.ID_RIF, user_schede_rifiuti.produttore, user_schede_rifiuti.trasportatore, user_schede_rifiuti.intermediario, user_schede_rifiuti.destinatario, user_schede_rifiuti.ID_UMIS ";
	$sql.="FROM user_schede_rifiuti ";
	$sql.="JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
	$sql.="WHERE user_schede_rifiuti.ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."' AND user_schede_rifiuti.mud='0' ";
	$sql.="AND intermediario=0 ";
	$sql.="ORDER BY cer ";
	$FEDIT->SdbRead($sql,"DbRecordset",true,false);

	$BufCER		= 0;
	$SchedeRif	= array();
	$c			= 0;
	$counterRT  = 0;
	$counterDR  = 0;
	$counterTE  = 0;
	

	for($r=0;$r<count($FEDIT->DbRecordset);$r++){
		if($BufCER!=$FEDIT->DbRecordset[$r]['cer']){
			$BufCER=$FEDIT->DbRecordset[$r]['cer'];
			$c++;
			$SchedeRif[$c]['cer']=$FEDIT->DbRecordset[$r]['cer'];
			$SchedeRif[$c]['ID_RIF']=$FEDIT->DbRecordset[$r]['ID_RIF'];
 			}
		else{ // cer uguale
			$SchedeRif[$c]['ID_RIF'].="|".$FEDIT->DbRecordset[$r]['ID_RIF'];
			}
		}
		
		$BARecord="";
		$counterBA=0;

		for($sr=1;$sr<=count($SchedeRif);$sr++){

			# costante tipo record
			$BARecord.="BA;";
			
			# Anno di riferimento della dichiarazione (AAAA)
			$BARecord.="2012;";

			# codice fiscale identificativo
			$BARecord.=$SL_codice.";";

			# codice identificativo unit� locale
			$BARecord.=$UL_id.";";

			# numero ordine progressivo scheda rif
			$BARecord.="####;";

			# codice CER
			$BARecord.=$SchedeRif[$sr]['cer'].";";

			# schede rifiuto soger che confluiscono in scheda rif
			$ID_RIF=explode("|", $SchedeRif[$sr]['ID_RIF']);			
			
			# formula: giacenza al 31/12 ( FKEdisponibilita ) + SUM(destino) oppure SUM(pesoN)    //scarichi
			$SchedeRif[$sr]['Giac_31-12'] = 0;

			for($s=0;$s<count($ID_RIF);$s++){
				## AGGIORNO LA DISPONIBILITA' DEL RIFIUTO
				$IMP_FILTER	= true;
				$DISPO_PRINT_OUT = false;
				$IDRIF		= $ID_RIF[$s];
				$TableName  = $MovTable;
				$IDMov		= "ID_MOV_F";
				$DTMOV		= date("Y-m-d");
				require("MovimentiDispoRIF.php");
				if($Disponibilita<0) $Disponibilita=0;
				$FEDIT->SDBWrite("UPDATE user_schede_rifiuti SET FKEdisponibilita='$Disponibilita' WHERE ID_RIF='".$IDRIF."'",true,false);
				
				# ricavo disponibilit� rifiuto
				$sql ="SELECT FKEdisponibilita, giac_ini, produttore, trasportatore, intermediario, destinatario, ID_UMIS, peso_spec FROM user_schede_rifiuti WHERE ID_RIF='".$ID_RIF[$s]."' ";
				$FEDIT->SdbRead($sql,"DispoRif",true,false);

				if($FEDIT->DispoRif[0]['produttore']=='1'){
					$SchedeRif[$sr]['Giac_31-12']	= $SchedeRif[$sr]['Giac_31-12'] + $FEDIT->DispoRif[0]['FKEdisponibilita'];
					}
				
				$SchedeRif[$sr]['ID_UMIS']=$FEDIT->DispoRif[0]['ID_UMIS'];
				if($FEDIT->DispoRif[0]['peso_spec']==0) $peso_spec=1; else $peso_spec=$FEDIT->DispoRif[0]['peso_spec'];
				$SchedeRif[$sr]['peso_spec']=$peso_spec;

				// $FEDIT->DispoRif[0]['giac_ini'] va convertito in kg ($GiacIni)
				switch($FEDIT->DispoRif[0]['ID_UMIS']){
					case 1:
						$GiacIni	= $FEDIT->DispoRif[0]['giac_ini'];
						break;
					case 2:
						$GiacIni	= $FEDIT->DispoRif[0]['giac_ini'] * $SchedeRif[$sr]['peso_spec'];
						break;
					case 3:
						$GiacIni	= $FEDIT->DispoRif[0]['giac_ini'] * $SchedeRif[$sr]['peso_spec'] * 1000;
						break;
					}

				if($FEDIT->DispoRif[0]['produttore']==1){
					if(!isset($SchedeRif[$sr]['prodotto'])) $SchedeRif[$sr]['prodotto']=0;
					$SchedeRif[$sr]['prodotto']+=($FEDIT->DispoRif[0]['FKEdisponibilita']-$GiacIni);
					//if($SchedeRif[$sr]['prodotto']<0) $SchedeRif[$sr]['prodotto']=0;
					$SchedeRif[$sr]['consegnato']=0;
					$SchedeRif[$sr]['ricevuto']=0;
					$SchedeRif[$sr]['trasportato']=0;
					}

				//if($ID_RIF[$s]=='0008547') die($SchedeRif[$sr]['prodotto']);
				//if($ID_RIF[$s]=='0008531') die(var_dump($SchedeRif));
				//0008547|0008531
			
				if($FEDIT->DispoRif[0]['trasportatore']==1){
					$SchedeRif[$sr]['prodotto']=0;
					$SchedeRif[$sr]['consegnato']=0;
					$SchedeRif[$sr]['ricevuto']=0;
					$SchedeRif[$sr]['trasportato']=0;
					}
							
				if($FEDIT->DispoRif[0]['destinatario']==1){
					if(!isset($SchedeRif[$sr]['ricevuto'])) $SchedeRif[$sr]['ricevuto']=0;
					$SchedeRif[$sr]['prodotto']=0;
					$SchedeRif[$sr]['consegnato']=0;
					$SchedeRif[$sr]['ricevuto']+=$FEDIT->DispoRif[0]['FKEdisponibilita']-$GiacIni;
					$SchedeRif[$sr]['trasportato']=0;
					}
				
				//$SchedeRif[$sr]['ricevuto']=0;
				
				//if($FEDIT->DispoRif[0]['destinatario']==1)
				//	$SchedeRif[$sr]['ricevuto']=$FEDIT->DispoRif[0]['FKEdisponibilita']-$GiacIni;
				//else
				//	$SchedeRif[$sr]['ricevuto']=0;				
				}

			# ricavo qta prodotte
			$sql ="SELECT ".$MovTable.".pesoN as stimato, ".$MovTable.".PS_DESTINO as destino ";
			$sql.="FROM ".$MovTable." ";
			$sql.="WHERE ".$MovTable.".ID_RIF IN (".str_replace("|",",",$SchedeRif[$sr]['ID_RIF']).") AND ";
			$sql.="".$MovTable.".TIPO='S' AND ";
			$sql.="".$MovTable.".produttore='1' AND ";
			$sql.="(".$MovTable.".DTMOV>='".str_replace("soger","",$FEDIT->DbServerData["db"])."-01-01' AND ".$MovTable.".DTMOV<='".str_replace("soger","",$FEDIT->DbServerData["db"])."-12-31') ";
			$FEDIT->SdbRead($sql,"DbRecordset",true,false);
			
			if(isset($FEDIT->DbRecordset)){
				for($c=0;$c<count($FEDIT->DbRecordset);$c++){
					if(!is_null($FEDIT->DbRecordset[$c]['destino']) && $FEDIT->DbRecordset[$c]['destino']>0){
						$SchedeRif[$sr]['prodotto']+=$FEDIT->DbRecordset[$c]['destino'];
						$SchedeRif[$sr]['consegnato']+=$FEDIT->DbRecordset[$c]['destino'];
						}
					else{
						$SchedeRif[$sr]['prodotto']+=$FEDIT->DbRecordset[$c]['stimato'];
						$SchedeRif[$sr]['consegnato']+=$FEDIT->DbRecordset[$c]['stimato'];
						}
					}
				}
			else{
				$SchedeRif[$sr]['prodotto']+=0;
				$SchedeRif[$sr]['consegnato']+=0;
				}

			//die(var_dump($SchedeRif));

			# ricavo qta ricevute
			$sql ="SELECT ".$MovTable.".pesoN as stimato, ".$MovTable.".PS_DESTINO as destino ";
			$sql.="FROM ".$MovTable." ";
			$sql.="WHERE ".$MovTable.".ID_RIF IN (".str_replace("|",",",$SchedeRif[$sr]['ID_RIF']).") AND ";
			$sql.="".$MovTable.".TIPO='C' AND ";
			//$sql.="".$MovTable.".destinatario='1' AND ";
			$sql.="(".$MovTable.".DTMOV>='".str_replace("soger","",$FEDIT->DbServerData["db"])."-01-01' AND ".$MovTable.".DTMOV<='".str_replace("soger","",$FEDIT->DbServerData["db"])."-12-31') ";

			//echo $sql."<hr>";
			$FEDIT->SdbRead($sql,"DbRecordset",true,false);
			
			if(isset($FEDIT->DbRecordset)){
				for($c=0;$c<count($FEDIT->DbRecordset);$c++){
					if(!is_null($FEDIT->DbRecordset[$c]['destino']) && $FEDIT->DbRecordset[$c]['destino']>0)
						$SchedeRif[$sr]['ricevuto']+=$FEDIT->DbRecordset[$c]['destino'];
					else
						$SchedeRif[$sr]['ricevuto']+=$FEDIT->DbRecordset[$c]['stimato'];
					}
				}
			else
				$SchedeRif[$sr]['ricevuto']+=0;


			# ricavo qta trasportate
			$sql ="SELECT ".$MovTable.".pesoN as stimato, ".$MovTable.".PS_DESTINO as destino ";
			$sql.="FROM ".$MovTable." ";
			$sql.="WHERE ".$MovTable.".ID_RIF IN (".str_replace("|",",",$SchedeRif[$sr]['ID_RIF']).") AND ";
			$sql.="".$MovTable.".TIPO='C' AND ";
			$sql.="".$MovTable.".trasportatore='1' AND ";
			$sql.="(".$MovTable.".DTMOV>='".str_replace("soger","",$FEDIT->DbServerData["db"])."-01-01' AND ".$MovTable.".DTMOV<='".str_replace("soger","",$FEDIT->DbServerData["db"])."-12-31') ";
			$FEDIT->SdbRead($sql,"DbRecordset",true,false);
			
			if(isset($FEDIT->DbRecordset)){
				for($c=0;$c<count($FEDIT->DbRecordset);$c++){
					if(!is_null($FEDIT->DbRecordset[$c]['destino']) && $FEDIT->DbRecordset[$c]['destino']>0){
						$SchedeRif[$sr]['ricevuto']+=$FEDIT->DbRecordset[$c]['destino'];
						$SchedeRif[$sr]['trasportato']+=$FEDIT->DbRecordset[$c]['destino'];
						$SchedeRif[$sr]['consegnato']+=$FEDIT->DbRecordset[$c]['destino'];
						}
					else{
						$SchedeRif[$sr]['ricevuto']+=$FEDIT->DbRecordset[$c]['stimato'];
						$SchedeRif[$sr]['trasportato']+=$FEDIT->DbRecordset[$c]['stimato'];
						$SchedeRif[$sr]['consegnato']+=$FEDIT->DbRecordset[$c]['stimato'];
						}
					}
				}
			else{
				$SchedeRif[$sr]['ricevuto']+=0;
				$SchedeRif[$sr]['trasportato']+=0;
				$SchedeRif[$sr]['consegnato']+=0;
				}

		# rifiuto prodotto in unit� locale
		if($SchedeRif[$sr]['prodotto']<0) $SchedeRif[$sr]['prodotto']=0;
		$Prodotto = zeroFiller(number_format($SchedeRif[$sr]['prodotto'],3,",",""),11);
		if($Prodotto>9999999.999){
			$Prodotto=$Prodotto/1000;
			$UM=2;
			}
		else $UM=1;
		//$BARecord.=zeroFiller(number_format($SchedeRif[$sr]['prodotto'],3,",",""),11).";";
		$BARecord.=$Prodotto.";";
		# unit� di misura - Kg
		$BARecord.=$UM.";";

		# rifiuto ricevuto da terzi
		$BARecord.=zeroFiller(number_format($SchedeRif[$sr]['ricevuto'],3,",",""),11).";";
		# unit� di misura - Kg
		$BARecord.="1;";

		# numero moduli RT allegati		
		if($SchedeRif[$sr]['ricevuto']=="0") 
			$BARecord.="00000;"; 
		else{
			$sql ="SELECT DISTINCT ".$MovTable.".ID_UIMP, TRIM(user_aziende_produttori.description) as azienda, user_aziende_produttori.codfisc, ";
			$sql.="TRIM(user_impianti_produttori.description) as indirizzo, lov_comuni_istat.cod_prov, lov_comuni_istat.cod_comISTAT as cod_com, ";
			$sql.="lov_comuni_istat.CAP ";
			$sql.="FROM ".$MovTable." ";
			$sql.="JOIN user_impianti_produttori ON ".$MovTable.".ID_UIMP=user_impianti_produttori.ID_UIMP ";
			$sql.="JOIN user_aziende_produttori ON user_impianti_produttori.ID_AZP=user_aziende_produttori.ID_AZP ";
			$sql.="JOIN lov_comuni_istat ON user_impianti_produttori.ID_COM=lov_comuni_istat.ID_COM ";
			$sql.="WHERE ID_RIF IN (".str_replace("|",",",$SchedeRif[$sr]['ID_RIF']).") AND ";
			$sql.="".$MovTable.".TIPO='C' AND ";
			//$sql.="(".$MovTable.".destinatario=1 OR ".$MovTable.".trasportatore=1 ) AND ";
			$sql.="(".$MovTable.".DTMOV>='".str_replace("soger","",$FEDIT->DbServerData["db"])."-01-01' AND ".$MovTable.".DTMOV<='".str_replace("soger","",$FEDIT->DbServerData["db"])."-12-31') ";

			$FEDIT->SdbRead($sql,"DbRecordset",true,false);
			if(isset($FEDIT->DbRecordset)){
				$RTrset=$FEDIT->DbRecordset;
				$BARecord.=zeroFiller(count($RTrset),5).";";
				}
			else
				$BARecord.="00000;";
			}

		# rifiuto prodotto fuori unit� locale
		$BARecord.="0000000,000;";
		# unit� di misura - Kg
		$BARecord.="1;";
		# numero moduli RE allegati
		$BARecord.="00000;";

		# rifiuto trasportato dal dichiarante
		$BARecord.=zeroFiller(number_format($SchedeRif[$sr]['trasportato'],3,",",""),11).";";
		# unit� di misura - Kg
		$BARecord.="1;";
		
		# numero moduli TE allegati
		$sql ="SELECT DISTINCT ".$MovTable.".ID_UIMT, TRIM(user_aziende_trasportatori.description) AS azienda,
		user_aziende_trasportatori.codfisc, ";
		$sql.="TRIM(user_impianti_trasportatori.description) AS indirizzo, lov_comuni_istat.cod_prov, lov_comuni_istat.cod_comISTAT AS cod_com, ";
		$sql.="lov_comuni_istat.CAP ";
		$sql.="FROM ".$MovTable." ";
		$sql.="JOIN user_impianti_trasportatori ON ".$MovTable.".ID_UIMT=user_impianti_trasportatori.ID_UIMT ";
		$sql.="JOIN user_aziende_trasportatori ON user_impianti_trasportatori.ID_AZT=user_aziende_trasportatori.ID_AZT ";
		$sql.="JOIN lov_comuni_istat ON user_impianti_trasportatori.ID_COM=lov_comuni_istat.ID_COM ";
		$sql.="WHERE ID_RIF IN (".str_replace("|",",",$SchedeRif[$sr]['ID_RIF']).") AND ";
		//if($SchedeRif[$sr]['prodotto']>0)
		$sql.="".$MovTable.".TIPO='S' AND ";
		$sql.="(".$MovTable.".DTMOV>='".str_replace("soger","",$FEDIT->DbServerData["db"])."-01-01' AND ".$MovTable.".DTMOV<='".str_replace("soger","",$FEDIT->DbServerData["db"])."-12-31') ";
		$FEDIT->SdbRead($sql,"DbRecordset",true,false);

		if(isset($FEDIT->DbRecordset)){
			$TErset=$FEDIT->DbRecordset;
			$BARecord.=zeroFiller(count($TErset),5).";";
			}
		else{
			$BARecord.="00000;";
			unset($TErset);
			}
		
		# rifiuto consegnato a terzi
		$BARecord.=zeroFiller(number_format($SchedeRif[$sr]['consegnato'],3,",",""),11).";";
		# unit� di misura - Kg
		$BARecord.="1;";

		# numero moduli DR allegati
		$sql ="SELECT DISTINCT ".$MovTable.".ID_UIMD, TRIM(user_aziende_destinatari.description) as azienda, user_aziende_destinatari.codfisc, ";
		$sql.="TRIM(user_impianti_destinatari.description) as indirizzo, lov_comuni_istat.cod_prov, lov_comuni_istat.cod_comISTAT as cod_com, ";
		$sql.="lov_comuni_istat.CAP ";
		$sql.="FROM ".$MovTable." ";
		$sql.="JOIN user_impianti_destinatari ON ".$MovTable.".ID_UIMD=user_impianti_destinatari.ID_UIMD ";
		$sql.="JOIN user_aziende_destinatari ON user_impianti_destinatari.ID_AZD=user_aziende_destinatari.ID_AZD ";
		$sql.="JOIN lov_comuni_istat ON user_impianti_destinatari.ID_COM=lov_comuni_istat.ID_COM ";
		$sql.="WHERE ID_RIF IN (".str_replace("|",",",$SchedeRif[$sr]['ID_RIF']).") AND ";
		// if($SchedeRif[$sr]['prodotto']>0 | $SchedeRif[$sr]['consegnato']>0)    ??? why ???
			$sql.="".$MovTable.".TIPO='S' AND ";
			//$sql.="".$MovTable.".produttore=1 AND ";
			$sql.="(".$MovTable.".produttore=1 OR ".$MovTable.".trasportatore=1 ) AND";
		$sql.="(".$MovTable.".DTMOV>='".str_replace("soger","",$FEDIT->DbServerData["db"])."-01-01' AND ".$MovTable.".DTMOV<='".str_replace("soger","",$FEDIT->DbServerData["db"])."-12-31') ";
		
		$FEDIT->SdbRead($sql,"DbRecordset",true,false);
		if(isset($FEDIT->DbRecordset)){
			$DRrset=$FEDIT->DbRecordset;
			$BARecord.=zeroFiller(count($DRrset),5).";";
			}
		else{
			$BARecord.="00000;";
			unset($DRrset);
			}

		# Rifiuto in giacenza presso il produttore al 31/12
		$BARecord.=zeroFiller(number_format($SchedeRif[$sr]['Giac_31-12'],3,",",""),11).";";
		# unit� di misura - Kg
		$BARecord.="1;";

		#Quantit� complessiva avviata a recupero - lo compilano solo i destinatari
		$BARecord.="0000000,000;";
		# unit� di misura - Kg
		$BARecord.="1;";

		#Quantit� complessiva avviata a smaltimento - lo compilano solo i destinatari
		$BARecord.="0000000,000;";
		# unit� di misura - Kg
		$BARecord.="1;";

		# per eliminare schede qta 0
		if($SchedeRif[$sr]['prodotto']<0)	$SchedeRif[$sr]['prodotto']=0;
		if($SchedeRif[$sr]['consegnato']<0) $SchedeRif[$sr]['consegnato']=0;
		if($SchedeRif[$sr]['ricevuto']<0)	$SchedeRif[$sr]['ricevuto']=0;

		if($SchedeRif[$sr]['prodotto']+$SchedeRif[$sr]['consegnato']+$SchedeRif[$sr]['ricevuto']+$SchedeRif[$sr]['trasportato'] <= 0){
			unset($RTrset);
			unset($DRrset);
			unset($TErset);			
			$BARecord = substr($BARecord,0,strlen($BARecord)-189);
			$BBRecord="";
			}
		else{
			$BBRecord=$_SESSION['EOL'];
			$counterBA++;
			$BARecord = str_replace("####",zeroFiller($counterBA,4),$BARecord);
			}
		
	
		#BB# tanti quanti sono gli allegati RT DR TE del record BA
		$allegati=0;
		
		#RT#

		$RTRecord="";
		if(isset($RTrset)){
			
			$counterRT+=count($RTrset);		

			for($rt=0;$rt<count($RTrset);$rt++){

				# costante tipo record
				$RTRecord.="BB;";
				
				# anno di riferimento della dichiarazione 
				$RTRecord.="2012;";

				# codice fiscale del dichiarante
				$RTRecord.=spaceFiller($SL_codice,16).";";

				# codice id unit� locale
				$RTRecord.=spaceFiller($UL_id,15).";";

				# numero progressivo scheda rif
				$RTRecord.=zerofiller($counterBA,4).";";

				# codice del rifiuto CER
				$RTRecord.=$SchedeRif[$sr]['cer'].";";

				# tipo allegato
				$RTRecord.="RT;";

				# numero progressivo allegato
				$RTRecord.=zeroFiller($rt+1 , 5 ) .";";
				$allegati+=$rt+1;

				# codice fiscale soggetto 
				$RTRecord.=spaceFiller($RTrset[$rt]['codfisc'],16).";";

				# nome o rag. sociale
				$RTRecord.=spaceFiller($RTrset[$rt]['azienda'],60).";";

				# codice istat provincia
				$RTRecord.=zeroFiller($RTrset[$rt]['cod_prov'],3).";";

				# codice istat comune
				$RTRecord.=zeroFiller($RTrset[$rt]['cod_com'],3).";";

				# via
				$RTRecord.=spaceFiller($RTrset[$rt]['indirizzo'],30).";";
				/*		
				$tmp='';
				$string = spaceFiller($RTrset[$rt]['indirizzo'],30);
				$length = strlen($string);
				if(strpos($RTrset[$rt]['azienda'], "OSTERIA")!==false){
					//die(var_dump($RTrset[$rt]['indirizzo']));
					for ($i=0; $i<$length; $i++) {
						$tmp.=ord($string[$i])."<br />";
						}
					die($tmp);
					}
				*/

				# civico
				$RTRecord.="      ;";

				# cap
				$RTRecord.=spaceFiller($RTrset[$rt]['CAP'],5).";";

				# quantit� dichiarata
				# somma di tutti i carichi con produttore Y
				$sql ="SELECT SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS dichiarato, user_schede_rifiuti.peso_spec, user_schede_rifiuti.ID_UMIS, ";
				$sql.="lov_comuni_istat.nazione ";
				$sql.="FROM ".$MovTable." ";
				$sql.="JOIN user_schede_rifiuti ON ".$MovTable.".ID_RIF=user_schede_rifiuti.ID_RIF ";
				$sql.="JOIN user_impianti_produttori ON user_impianti_produttori.ID_UIMP=".$MovTable.".ID_UIMP ";
				$sql.="JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM = user_impianti_produttori.ID_COM ";
				$sql.="WHERE ".$MovTable.".ID_UIMP='".$RTrset[$rt]['ID_UIMP']."' ";
				$sql.="AND ".$MovTable.".ID_RIF IN (".str_replace("|",",",$SchedeRif[$sr]['ID_RIF']).")";
				//$sql.="AND (".$MovTable.".destinatario='1' OR ".$MovTable.".trasportatore=1) ";
				$sql.="AND ".$MovTable.".TIPO='C' ";
				$sql.="AND (".$MovTable.".DTMOV>='".str_replace("soger","",$FEDIT->DbServerData["db"])."-01-01' AND ".$MovTable.".DTMOV<='".str_replace("soger","",$FEDIT->DbServerData["db"])."-12-31') ";
				//$sql.="GROUP BY ".$MovTable.".destinatario ";
				$FEDIT->SdbRead($sql,"DbRecordset",true,false);

				if(isset($FEDIT->DbRecordset)){
					$dichiarato=$FEDIT->DbRecordset[0]['dichiarato'];
					$RTRecord.=zeroFiller(number_format($dichiarato,3,",",""),11).";";
					}
				else
					$RTRecord.="0000000,000;";

				# unit� di misura
				$RTRecord.="1;";

				# nome della nazione - se estero
				//$RTRecord.="                    ;";
				$RTRecord.=spaceFiller(@$FEDIT->DbRecordset[0]['nazione'],20).";";

				# Codice Regolamento CEE 1013/2006
				$RTRecord.="      ;";

				# Ricevuto da Privati ( NON LO SAPPIAMO )
				$RTRecord.="0;";

				# Quantit� a smaltimento
				$RTRecord.="0000000,000;";
				# Unit� di misura
				$RTRecord.="1;";
				# Quantit� a recupero di materia
				$RTRecord.="0000000,000;";
				# Unit� di misura
				$RTRecord.="1;";
				# Quantit� a recupero di energia
				$RTRecord.="0000000,000;";
				# Unit� di misura
				$RTRecord.="1;";

				$RTRecord.=$_SESSION['EOL'];
				}
			}

		$BBRecord.=$RTRecord;


		#DR#
		$DRRecord="";

		if(isset($DRrset)){

			$counterDR+=count($DRrset);

			for($dr=0;$dr<count($DRrset);$dr++){

				# costanti
				$DRRecord.="BB;";
				
				# anno di riferimento della dichiarazione 
				$DRRecord.="2012;";

				# codice fiscale del dichiarante
				$DRRecord.=spaceFiller($SL_codice,16).";";

				# codice id unit� locale
				$DRRecord.=spaceFiller($UL_id,15).";";

				# numero progressivo scheda rif
				$DRRecord.=zerofiller($counterBA,4).";";

				# codice del rifiuto CER
				$DRRecord.=$SchedeRif[$sr]['cer'].";";

				# tipo allegato
				$DRRecord.="DR;";

				# numero progressivo allegato
				$DRRecord.=zeroFiller($dr+1 , 5) .";";
				$allegati+=$dr+1;

				# codice fiscale soggetto 
				$DRRecord.=spaceFiller($DRrset[$dr]['codfisc'],16).";";

				# nome o rag. sociale
				$DRRecord.=spaceFiller($DRrset[$dr]['azienda'],60).";";

				# codice istat provincia
				$DRRecord.=zeroFiller($DRrset[$dr]['cod_prov'],3).";";

				# codice istat comune
				$DRRecord.=zeroFiller($DRrset[$dr]['cod_com'],3).";";

				# via
				$DRRecord.=spaceFiller($DRrset[$dr]['indirizzo'],30).";";

				# civico
				$DRRecord.="      ;";

				# cap
				$DRRecord.=spaceFiller($DRrset[$dr]['CAP'],5).";";

				# quantit� dichiarata
				# somma di tutti gli scarichi con destinatario Y
				$sql ="SELECT SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS dichiarato, user_schede_rifiuti.peso_spec, user_schede_rifiuti.ID_UMIS, ";
				$sql.="lov_comuni_istat.nazione ";
				$sql.="FROM ".$MovTable." ";
				$sql.="JOIN user_schede_rifiuti ON ".$MovTable.".ID_RIF=user_schede_rifiuti.ID_RIF ";
				$sql.="JOIN user_impianti_destinatari ON user_impianti_destinatari.ID_UIMD=".$MovTable.".ID_UIMD ";
				$sql.="JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM = user_impianti_destinatari.ID_COM ";
				$sql.="WHERE ".$MovTable.".ID_UIMD='".$DRrset[$dr]['ID_UIMD']."' ";
				$sql.="AND ".$MovTable.".ID_RIF IN (".str_replace("|",",",$SchedeRif[$sr]['ID_RIF']).")";
				//$sql.="AND ".$MovTable.".produttore='1' ";
				$sql.="AND ".$MovTable.".TIPO='S' ";
				$sql.="AND (".$MovTable.".DTMOV>='".str_replace("soger","",$FEDIT->DbServerData["db"])."-01-01' AND ".$MovTable.".DTMOV<='".str_replace("soger","",$FEDIT->DbServerData["db"])."-12-31') ";
				//$sql.="GROUP BY ".$MovTable.".produttore ";
				//die($sql);

				$FEDIT->SdbRead($sql,"DbRecordset",true,false);
				
				if(isset($FEDIT->DbRecordset)){
					$dichiarato=$FEDIT->DbRecordset[0]['dichiarato'];
					$DRRecord.=zeroFiller(number_format($dichiarato,3,",",""),11).";";
					}
				else
					$DRRecord.="0000000,000;";
	
				# unit� di misura
				$DRRecord.="1;";

				# nome della nazione - se estero
				//$DRRecord.="                    ;";
				$DRRecord.=spaceFiller(@$FEDIT->DbRecordset[0]['nazione'],20).";";

				# Codice Regolamento CEE 1013/2006
				$DRRecord.="      ;";

				# Ricevuto da Privati
				$DRRecord.="0;";

				# Quantit� a smaltimento
				$DRRecord.="0000000,000;";
				# Unit� di misura
				$DRRecord.="1;";
				# Quantit� a recupero di materia
				$DRRecord.="0000000,000;";
				# Unit� di misura
				$DRRecord.="1;";
				# Quantit� a recupero di energia
				$DRRecord.="0000000,000;";
				# Unit� di misura
				$DRRecord.="1;";

				$DRRecord.=$_SESSION['EOL'];
				}
			}

		$BBRecord.=$DRRecord;

#TE#

		$TERecord="";
		if(isset($TErset)){

			$counterTE+=count($TErset);

			for($te=0;$te<count($TErset);$te++){

				# costanti
				$TERecord.="BB;";
				
				# anno di riferimento della dichiarazione
				$TERecord.="2012;";

				# codice fiscale del dichiarante
				$TERecord.=spaceFiller($SL_codice,16).";";

				# codice id unit� locale
				$TERecord.=spaceFiller($UL_id,15).";";

				# numero progressivo scheda rif
				//$TERecord.=zerofiller($sr,4).";";
				$TERecord.=zerofiller($counterBA,4).";";

				# codice del rifiuto CER
				$TERecord.=$SchedeRif[$sr]['cer'].";";

				# tipo allegato
				$TERecord.="TE;";

				# numero progressivo allegato
				$TERecord.=zeroFiller($te+1 , 5) .";";
				$allegati+=$te+1;

				# codice fiscale soggetto 
				$TERecord.=spaceFiller($TErset[$te]['codfisc'],16).";";

				# nome o rag. sociale
				$TERecord.=spaceFiller($TErset[$te]['azienda'],60).";";

				# codice istat provincia
				$TERecord.=zeroFiller($TErset[$te]['cod_prov'],3).";";

				# codice istat comune
				$TERecord.=zeroFiller($TErset[$te]['cod_com'],3).";";

				# via
				$TERecord.=spaceFiller($TErset[$te]['indirizzo'],30).";";

				# civico
				$TERecord.="      ;";

				# cap
				$TERecord.=spaceFiller($TErset[$te]['CAP'],5).";";

				# quantit� dichiarata
				# somma di tutti i movimenti con trasportatore Y
				$sql ="SELECT SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS dichiarato, user_schede_rifiuti.peso_spec, user_schede_rifiuti.ID_UMIS, ";
				$sql.="lov_comuni_istat.nazione ";
				$sql.="FROM ".$MovTable." ";
				$sql.="JOIN user_schede_rifiuti ON ".$MovTable.".ID_RIF=user_schede_rifiuti.ID_RIF ";
				$sql.="JOIN user_impianti_trasportatori ON user_impianti_trasportatori.ID_UIMT=".$MovTable.".ID_UIMT ";
				$sql.="JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM = user_impianti_trasportatori.ID_COM ";
				$sql.="WHERE ".$MovTable.".ID_UIMT='".$TErset[$te]['ID_UIMT']."' ";
				$sql.="AND ".$MovTable.".ID_RIF IN (".str_replace("|",",",$SchedeRif[$sr]['ID_RIF']).")";
				$sql.="AND ".$MovTable.".produttore='1' ";
				$sql.="AND ".$MovTable.".TIPO='S' ";
				$sql.="AND (".$MovTable.".DTMOV>='".str_replace("soger","",$FEDIT->DbServerData["db"])."-01-01' AND ".$MovTable.".DTMOV<='".str_replace("soger","",$FEDIT->DbServerData["db"])."-12-31') ";
				$sql.="GROUP BY ".$MovTable.".ID_RIF ";
				$FEDIT->SdbRead($sql,"DbRecordset",true,false);
				/*
				if(isset($FEDIT->DbRecordset)){
					$dichiarato=$FEDIT->DbRecordset[0]['dichiarato'];
					$TERecord.=zeroFiller(number_format($dichiarato,3,",",""),11).";";
					}
				else
					$TERecord.="0000000,000;";
				*/

				# quantit� dichiarata
				$TERecord.="0000000,000;";

				# unit� di misura
				$TERecord.="1;";

				# nome della nazione - se estero
				//$TERecord.="                    ;";
				$TERecord.=spaceFiller(@$FEDIT->DbRecordset[0]['nazione'],20).";";

				# Codice Regolamento CEE 1013/2006
				$TERecord.="      ;";
			
				# Ricevuto da Privati
				$TERecord.="0;";

				# Quantit� a smaltimento
				$TERecord.="0000000,000;";
				# Unit� di misura
				$TERecord.="1;";
				# Quantit� a recupero di materia
				$TERecord.="0000000,000;";
				# Unit� di misura
				$TERecord.="1;";
				# Quantit� a recupero di energia
				$TERecord.="0000000,000;";
				# Unit� di misura
				$TERecord.="1;";

				$TERecord.=$_SESSION['EOL'];
				}
			}

		$BBRecord.=$TERecord;

		$BARecord.=$BBRecord;

		
		} //chiude for schede BA // $SchedeRif

#AB#

	# costante tipo record
	$ABRecord ="AB;";
	
	# Anno di riferimento della dichiarazione
	$ABRecord.="2012;";

	# codice fiscale sede legale
	$ABRecord.=$SL_codice.";";

	# codice identificativo unit� locale
	$ABRecord.=$UL_id.";";

	# numero di schede rif
	$ABRecord.=zeroFiller($counterBA,6).";";

	# Sezione intermediazione, numero di schede INT
	$ABRecord.="000000;";

	# Sezione veicoli - Compilata Scheda AUT?
	$ABRecord.="0;";

	# Sezione veicoli - Compilata Scheda ROT?
	$ABRecord.="0;";

	# Sezione veicoli - Compilata Scheda FRA?
	$ABRecord.="0;";

	# Sezione RAEE - Numero di schede TRA-RAEE
	$ABRecord.="00;";

	# Sezione RAEE - Numero di schede CR-RAEE
	$ABRecord.="00;";

	# VFU - Data autorizzazione art. 208, 209, 213
	$ABRecord.="        ;";

	# VFU - Data autorizzazione art. 216
	$ABRecord.="        ;";

	# RAEE - Data autorizzazione art. 208, 209, 213
	$ABRecord.="        ;";

	# RAEE - Data autorizzazione art. 216
	$ABRecord.="        ;";

	# Data certificazione EMAS
	$ABRecord.="00000000;";

	# Numero registrazione certificazione EMAS
	$ABRecord.="         ;";

	# Data certificazione ISO 14000
	$ABRecord.="00000000;";
	
	
	
	
	## unisco record in MUD2012
	$MUD2012 =$AARecord.$_SESSION['EOL'];
	$MUD2012.=$ABRecord.$_SESSION['EOL'];
	$MUD2012.=$BARecord.$_SESSION['EOL'];

	## devo salvare $MUD2012 in file di testo temporaneo
	$tmpMUDfile="../__updates/TMPMUD.txt";
	$tmpMUD=fopen($tmpMUDfile, "w+");
	fwrite($tmpMUD, $MUD2012);

	## conto le righe tranne XX.. attenzione a non mettere \n\r in ultima riga, oppure -2
	//$recordTotali=count(file($tmpMUDfile));
	$recordTotali=(count(explode($_SESSION['EOL'], $MUD2012))-2);
	
	
	## trasformo testo MUD2012 in array ogni \n\r
	$records=explode($_SESSION['EOL'], $MUD2012);

	## array tipologia righe
	$recordType = array("AA", "AB", "BA", "BB", "BC", "BD", "BE", "DA", "DB", "RA", "RB", "RC", "RD", "RE", "RF", "VC", "VD", "VE", "VF", "VG", "VH");
	$recordTypeCounter = array();
	foreach($recordType as $type){
		$recordTypeCounter[$type]=0;
		}
	

	## dimensioni del file
	$_SESSION['MudFileSize'] = filesize($tmpMUDfile);

	fclose($tmpMUD); 
	unlink($tmpMUDfile);

#XX#
		
	## costanti
	$XXRecord = "XX;4.00/12;01;";

	## data e ora
	$data=date("Ymd");
	$ora=date("His");
	$XXRecord.= $data.";".$ora.";";
	
	## numero record totali
	$XXRecord.= zeroFiller($recordTotali,8).";";

	## numero record per type
	foreach($recordType as $type){
		for($r=0;$r<count($records);$r++){
			if($type==substr($records[$r],0,2))
				$recordTypeCounter[$type]++;
			}
		}

	## integro record XX
	foreach($recordTypeCounter as $counter){
		$XXRecord.= zeroFiller($counter,5).";";
		}

	## FILLER-01 // I campi di tipo filler sono non utilizzati per cui ci metta in questo caso "00000".
	$XXRecord .="00000;";

	## intestatario MUD - info da core_impianti ( prima prendeva info da core_gruppi... PERCHE'? )
	$sql = "SELECT codfisc, TRIM(core_impianti.description) AS rag_soc, TRIM(indirizzo) AS indirizzo, lov_comuni_istat.CAP, lov_comuni_istat.description AS comune, lov_comuni_istat.shdes_prov AS provincia ";
	$sql.= "FROM core_impianti ";
	$sql.= "JOIN lov_comuni_istat ON core_impianti.ID_COM=lov_comuni_istat.ID_COM ";
	$sql.= "WHERE core_impianti.ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."'";

	$FEDIT->SdbRead($sql,"DbRecordset",true,false);

	## integro record XX
	$XXRecord.= spaceFiller(strtoupper($FEDIT->DbRecordset[0]["codfisc"]),16).";";		# codice fiscale
	$XXRecord.= spaceFiller(strtoupper($FEDIT->DbRecordset[0]["rag_soc"]),60).";";		# ragione sociale
	$XXRecord.= spaceFiller(strtoupper($FEDIT->DbRecordset[0]["indirizzo"]),30).";";	# via
	$XXRecord.= "          ;";															# civico
	$XXRecord.= spaceFiller(strtoupper($FEDIT->DbRecordset[0]["CAP"]),5).";";			# cap
	$XXRecord.= spaceFiller(strtoupper($FEDIT->DbRecordset[0]["comune"]),30).";";		# citt�
	$XXRecord.= spaceFiller(strtoupper($FEDIT->DbRecordset[0]["provincia"]),2).";";		# sigla provincia
	$XXRecord.= "     ;";																# prefisso
	$XXRecord.= "          ;";															# telefono
	$XXRecord.= "                                                            ;";		# email
	$XXRecord.= "                              ;";										# riservato (?)

	$ultimateMUD2012 =$XXRecord.$_SESSION['EOL'];
	$ultimateMUD2012.=$AARecord.$_SESSION['EOL'];
	$ultimateMUD2012.=$ABRecord.$_SESSION['EOL'];
	$ultimateMUD2012.=$BARecord;

	## Restituisce contenuto MUD2012.000

	$FEDIT->DbServerData["db"]="soger2013";
	return  strtoupper($ultimateMUD2012);
	}


function getMudFileSize(){
	return zeroFiller($_SESSION['MudFileSize'],9);
	}

function zeroFiller($value,$char,$int=true){
	# $int � true o false a seconda che sia int o float
	//$filled = sprintf("%01.2f", $value);
	if($int)
		$filled = str_pad(substr($value,0,$char), $char, "0", STR_PAD_LEFT);
	return $filled;
	}

function zeroFiller_right($value,$char,$int=true){
	# $int � true o false a seconda che sia int o float
	//$filled = sprintf("%01.2f", $value);
	if($int)
		$filled = str_pad(substr($value,0,$char), $char, "0", STR_PAD_RIGHT);
	return $filled;
	}

function spaceFiller($value,$char){
	$toReplace=array('�','�','�','�','�','�');
	$replaceWith=array("E'", "E'", "A'", "O'", "I'", "U'");
	$value = trim($value);
	// reg_exp from http://stackoverflow.com/questions/1176904/php-how-to-remove-all-non-printable-characters-in-a-string
	$value=preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $value);
	$filled = str_pad(substr(str_replace($toReplace,$replaceWith,$value),0,$char), $char, " ", STR_PAD_RIGHT);
	return $filled;
	}

?>