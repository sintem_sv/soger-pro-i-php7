<?php

$RecordSet[0]['COD_CER']=$FEDIT->DbRecordSet[0]['COD_CER'];
$RecordSet[0]['operazione']=$FEDIT->DbRecordSet[0]['operazione'];
$RecordSet[0]['operazione_des']=$FEDIT->DbRecordSet[0]['operazione_des'];
$RecordSet[0]['rifiuto']=$FEDIT->DbRecordSet[0]['rifiuto'];
$RecordSet[0]['quantita']=$FEDIT->DbRecordSet[0]['quantita'];
$RecordSet[0]['destinatario']=$FEDIT->DbRecordSet[0]['destinatario'];
$counter=1;

for($t=1;$t<count($FEDIT->DbRecordSet);$t++){

	if($FEDIT->DbRecordSet[$t]['operazione']==$RecordSet[$counter-1]['operazione']){
		# stesso operazione, aggiungo solo se rifiuto e destinatario sono diversi dal precedente

		if(($FEDIT->DbRecordSet[$t]['rifiuto']!=$RecordSet[$counter-1]['rifiuto']) || ($FEDIT->DbRecordSet[$t]['destinatario']!=$RecordSet[$counter-1]['destinatario'])){
			$RecordSet[$counter]['COD_CER']=$FEDIT->DbRecordSet[$t]['COD_CER'];
			$RecordSet[$counter]['operazione']=$FEDIT->DbRecordSet[$t]['operazione'];
			$RecordSet[$counter]['operazione_des']=$FEDIT->DbRecordSet[$t]['operazione_des'];
			$RecordSet[$counter]['rifiuto']=$FEDIT->DbRecordSet[$t]['rifiuto'];
			$RecordSet[$counter]['quantita']=$FEDIT->DbRecordSet[$t]['quantita'];
			$RecordSet[$counter]['destinatario']=$FEDIT->DbRecordSet[$t]['destinatario'];
			$counter++;
			}
		else{ #stesso operazione, stesso rifiuto, stesso destinatario: sommo
			$RecordSet[$counter-1]['quantita']+=$FEDIT->DbRecordSet[$t]['quantita'];
			}

		}
	else{ #diverso operazione, aggiungo..
		$RecordSet[$counter]['COD_CER']=$FEDIT->DbRecordSet[$t]['COD_CER'];
		$RecordSet[$counter]['operazione']=$FEDIT->DbRecordSet[$t]['operazione'];
		$RecordSet[$counter]['operazione_des']=$FEDIT->DbRecordSet[$t]['operazione_des'];
		$RecordSet[$counter]['rifiuto']=$FEDIT->DbRecordSet[$t]['rifiuto'];
		$RecordSet[$counter]['quantita']=$FEDIT->DbRecordSet[$t]['quantita'];
		$RecordSet[$counter]['destinatario']=$FEDIT->DbRecordSet[$t]['destinatario'];
		$counter++;
		}
	
	}


//print_r($RecordSet);



//echo $sql;

$lastoperazione="-";
$lastRifiuto="-";
$lastDestinatario="-";
$tmpQta=0;

$totale=0;
$superTotale=0;

$istoTot=array();
$istoTrat=array();

foreach($RecordSet as $k=>$dati) {

	if($dati['operazione']!=$lastoperazione){
		
		$istoTrat[]=$dati['operazione'];

		if($totale!=0){
			$istoTot[]=$totale;
			$totale=0;
			}
		$lastoperazione=$dati['operazione'];
		$totale+=$dati['quantita'];
		$superTotale+=$dati['quantita'];
		}
	else{
		# stessa operazione, altro cer o destinatario
		$totale+=$dati['quantita'];
		$superTotale+=$dati['quantita'];
		}
	}




# totale dell'ultimo record #
if($totale!=0){
	$istoTot[]=$totale;
	$totale=0;
	}
#




#
#	ISTOGRAMMA
#

//$FEDIT->FGE_PdfBuffer->addpage();
$Ypos = 0;

require("STATS_palette.php");
$ColPointer = 0;




for($nc=0;$nc<count($istoTrat);$nc++){
	if($istoTrat[$nc]==""){
		$ncPerc=round(((100/$superTotale)*$istoTot[$nc]),2);
		$ncTot=$istoTot[$nc];
		}
	}


if(!isset($ncPerc)) $ncPerc=0;
if(!isset($ncTot)) $ncTot=0;
$cPerc=100-$ncPerc;

$superTotale-=$ncTot;

$Ypos+=66;

$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','BU',12);
$FEDIT->FGE_PdfBuffer->MultiCell(0,15,"Movimenti censiti",0,"L"); 

$Ypos+=15;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Movimenti non censiti","R","L"); 
$FEDIT->FGE_PdfBuffer->SetFillColor($palette[0][0],$palette[0][1],$palette[0][2]);
$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
if($ncPerc==0){
	$ncPercGraph=0.01;
	$FEDIT->FGE_PdfBuffer->MultiCell($ncPercGraph,5,"",0,"C",1);
	}
else
	$FEDIT->FGE_PdfBuffer->MultiCell($ncPerc,5,"",0,"C",1);
$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$ncPerc." %","L","R"); 

$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Movimenti censiti","R","L"); 
$FEDIT->FGE_PdfBuffer->SetFillColor($palette[3][0],$palette[3][1],$palette[3][2]);
$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
if($cPerc==0){
	$cPercGraph=0.01;
	$FEDIT->FGE_PdfBuffer->MultiCell($cPercGraph,5,"",0,"C",1);
	}
else
	$FEDIT->FGE_PdfBuffer->MultiCell($cPerc,5,"",0,"C",1);
$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$cPerc." %","L","R"); 

$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
$FEDIT->FGE_PdfBuffer->MultiCell(175,5,"","T","C"); 

$Ypos+=5;
$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Totale movimenti","R","L"); 
$FEDIT->FGE_PdfBuffer->SetFillColor($palette[2][0],$palette[2][1],$palette[2][2]);
$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(100,5,"",0,"C",1);
$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(20,5,"100 %","L","R"); 


	$TotalR=0;
	$TotalD=0;

if($cPerc>0){

	$Ypos+=10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','BU',12);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,15,"Dettaglio movimenti censiti",0,"L"); 

	$Ypos+=15;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	for($i=0;$i<count($istoTrat);$i++){
		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		if($istoTrat[$i]!=""){
			$OPLetter=str_split($istoTrat[$i]);
			$FEDIT->FGE_PdfBuffer->MultiCell(50,5,$istoTrat[$i],"R","L"); 
			$FEDIT->FGE_PdfBuffer->SetFillColor($palette[$ColPointer][0],$palette[$ColPointer][1],$palette[$ColPointer][2]);
			$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
			$perc=round(((100/$superTotale)*$istoTot[$i]),2);
			if($OPLetter[0]=="R"){
				$TotalR+=$perc;
				}
			else{ //D
				$TotalD+=$perc;
				}
			$FEDIT->FGE_PdfBuffer->MultiCell($perc,5,"",0,"C",1);
			$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$perc." %","L","R"); 

			$Ypos+=10;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

			if($ColPointer<count($palette))
				$ColPointer++;
			else
				$ColPointer=0;
			}
	}

	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(175,5,"","T","C"); 
	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Tutte le operazioni R/D","R","L"); 
	$FEDIT->FGE_PdfBuffer->SetFillColor($palette[$ColPointer][0],$palette[$ColPointer][1],$palette[$ColPointer][2]);
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,"",0,"C",1);
	$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,5,"100 %","L","R"); 
	}

if($cPerc>0){

	$Ypos+=10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','BU',12);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,15,"Recupero / Smaltimento",0,"L"); 

	$Ypos+=15;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Smaltimento","R","L"); 
	$FEDIT->FGE_PdfBuffer->SetFillColor($palette[0][0],$palette[0][1],$palette[0][2]);
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	if($TotalD==0) $TotalD=0.01;
	$FEDIT->FGE_PdfBuffer->MultiCell($TotalD,5,"",0,"C",1);
	$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$TotalD." %","L","R"); 

	$Ypos+=10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Recupero","R","L"); 
	$FEDIT->FGE_PdfBuffer->SetFillColor($palette[3][0],$palette[3][1],$palette[3][2]);
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	if($TotalR==0) $TotalR=0.01;
	$FEDIT->FGE_PdfBuffer->MultiCell($TotalR,5,"",0,"C",1);
	$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$TotalR." %","L","R"); 

	$Ypos+=10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(175,5,"","T","C"); 

	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Totale operazioni R + D","R","L"); 
	$FEDIT->FGE_PdfBuffer->SetFillColor($palette[2][0],$palette[2][1],$palette[2][2]);
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,"",0,"C",1);
	$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,5,"100 %","L","R"); 

}

?>