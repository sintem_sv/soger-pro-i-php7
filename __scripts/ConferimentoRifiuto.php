<?php
session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__libs/SQLFunct.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");

global $SOGER;

## rifiuto ##
$sql ="SELECT descrizione, lov_cer.COD_CER as cer, lov_cer.description as desc_cer ";
$sql.="FROM user_schede_rifiuti ";
$sql.="JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
$sql.="WHERE user_schede_rifiuti.ID_RIF='".$_POST['ID_RIF']."' AND user_schede_rifiuti.ID_IMP='".$SOGER->UserData["core_impiantiID_IMP"] . "' ";
$FEDIT->SDbRead($sql,"DbRecordSetRif");

## destinatario rifiuto ##
$sql ="SELECT user_aziende_destinatari.description, user_aziende_destinatari.indirizzo, lov_comuni_istat.description as comune, lov_comuni_istat.shdes_prov ";
$sql.="FROM user_aziende_destinatari ";
$sql.="JOIN lov_comuni_istat on user_aziende_destinatari.ID_COM=lov_comuni_istat.ID_COM ";
$sql.="WHERE user_aziende_destinatari.ID_AZD='".$_POST['dest_rif']."' AND user_aziende_destinatari.ID_IMP='".$SOGER->UserData["core_impiantiID_IMP"] . "' ";
$FEDIT->SDbRead($sql,"DbRecordSetDestRif");

## destinatario richiesta ##
if($_POST['destinatario']!=0){
	$tabella="user_aziende_destinatari";
	$id=$_POST['destinatario'];
	}
if($_POST['trasportatore']!=0){
	$tabella="user_aziende_trasportatori";
	$id=$_POST['trasportatore'];
	}
if($_POST['intermediario']!=0){
	$tabella="user_aziende_intermediari";
	$id=$_POST['intermediario'];
	}


$sql ="SELECT ".$tabella.".description, ".$tabella.".indirizzo, lov_comuni_istat.description as comune, lov_comuni_istat.shdes_prov ";
$sql.="FROM ".$tabella." ";
$sql.="JOIN lov_comuni_istat on ".$tabella.".ID_COM=lov_comuni_istat.ID_COM ";
$sql.="WHERE ".$tabella.".ID_AZ".strtoupper($tabella[13])."='".$id."' AND ".$tabella.".ID_IMP='".$SOGER->UserData["core_impiantiID_IMP"] . "' ";
$FEDIT->SDbRead($sql,"DbRecordSetDestRich");



$ReplacedWith = array();
$ToBeReplaced = array(
	0=>"[IMP_RICH]",
	1=>"[DEST]",
	2=>"[DEST_IMP]",
	3=>"[USER]",
	4=>"[DATA]",
	5=>"[CER]",
	6=>"[NOME_CER]",
	7=>"[SCHEDA_RIF]",
	8=>"[QTA]",
	9=>"[DEST_RIF]",
	10=>"[TODAY]",
	11=>"[PROCEDURA_ISO]"
	);

$ReplacedWith[0] = $SOGER->UserData["core_impiantidescription"];
$ReplacedWith[1] = $FEDIT->DbRecordSetDestRich[0]['description'];
$ReplacedWith[2] = $FEDIT->DbRecordSetDestRich[0]['indirizzo']." - ".$FEDIT->DbRecordSetDestRich[0]['comune']." ( ".$FEDIT->DbRecordSetDestRich[0]['shdes_prov']." )";
$ReplacedWith[3] = $SOGER->UserData["core_usersdescription"];
$ReplacedWith[4] = $_POST['data'];
$ReplacedWith[5] = $FEDIT->DbRecordSetRif[0]['cer'];
$ReplacedWith[6] = $FEDIT->DbRecordSetRif[0]['desc_cer'];
$ReplacedWith[7] = $FEDIT->DbRecordSetRif[0]['descrizione'];
$ReplacedWith[8] = $_POST['quantita'];
$ReplacedWith[9] = $FEDIT->DbRecordSetDestRif[0]['description'].", ".$FEDIT->DbRecordSetDestRif[0]['indirizzo']." - ".$FEDIT->DbRecordSetDestRif[0]['comune']." ( ".$FEDIT->DbRecordSetDestRif[0]['shdes_prov']." )";
$ReplacedWith[10] = date("d/m/Y");



## PROCEDURA ISO - ID_ISOF=12
$SQL="SELECT procedura FROM core_impianti_iso WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1 AND ID_ISOF=11;";
$FEDIT->SdbRead($SQL, "DbRecordSet");
$PROCEDURA=$FEDIT->DbRecordSet[0]["procedura"];
if(!is_null($PROCEDURA))
	$ReplacedWith[11]="Documento elaborato in ottemperanza alla procedura cliente: ".$PROCEDURA;
else
	$ReplacedWith[11]="";




$FileName = "Richiesta_conferimento_rifiuto.rtf";
$template = "RIC_CONFERIMENTO.rtf";


StreamOut($template,$FileName,$ToBeReplaced,$ReplacedWith);

//print_r($ReplacedWith);

require_once("../__includes/COMMON_sleepForgEdit.php");
?>