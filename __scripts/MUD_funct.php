<?php
/*
error_reporting(E_ALL);
ini_set("display_errors", 1);
*/

/*
La produzione del file in formato ASCII (ISO 646) in formato MS-Dos (tipicamente con PC o sistemi Unix), richiede che ogni record termini con la coppia di caratteri "CRLF" (hex. 0d0a).
*/

$ANNO_DICHIARAZIONE	= "2021";
$DB_PREFIX			= "soger";
$CODICE_FISCALE		= $SOGER->UserData['core_impianticodfisc'];
$CODICE_UL			= $SOGER->UserData["core_usersID_IMP"];
$MUD_PS_DEST		= $SOGER->UserData['core_impiantiMUD_PS_DEST'];

# REGISTRI ATTIVI IN SOGER
$REGISTRO = array();
$REGISTRO['P']		= $SOGER->UserData['core_impiantiproduttore'];
$REGISTRO['T']		= $SOGER->UserData['core_impiantitrasportatore'];
$REGISTRO['D']		= $SOGER->UserData['core_impiantidestinatario'];
$REGISTRO['I']		= $SOGER->UserData['core_impiantiintermediario'];

function getDatabaseName(){
	global $DB_PREFIX;
	global $ANNO_DICHIARAZIONE;
	return $DB_PREFIX.$ANNO_DICHIARAZIONE;
	}

function getAnnoDichiarazione(){
	global $ANNO_DICHIARAZIONE;
	return $ANNO_DICHIARAZIONE;
	}

function getCodiceFiscale(){
	global $CODICE_FISCALE;
	return $CODICE_FISCALE;
	}

function getCodiceUL(){
	global $CODICE_UL;
	return $CODICE_UL;
	}

function getMudPsDest(){
	global $MUD_PS_DEST;
	return $MUD_PS_DEST;
	}

function getRegistriSoger(){
	global $REGISTRO;
	return $REGISTRO;
	}

function zeroFiller($value,$char,$int=true){
	# $int � true o false a seconda che sia int o float
	if($int)
		$filled = str_pad(substr($value,0,$char), $char, "0", STR_PAD_LEFT);
	return $filled;
	}

function spaceFiller($value,$char){
	$toReplace		= array('�','�','�','�','�','�');
	$replaceWith	= array("E'", "E'", "A'", "O'", "I'", "U'");
	$filled			= str_pad(substr(str_replace($toReplace,$replaceWith,$value),0,$char), $char, " ", STR_PAD_RIGHT);
	return $filled;
	}

function formatQuantita($Q){
	if(is_null($Q)){ $QTA = 0; $UM = 0;	}
	else{
		if($Q>9999999){
			$QTA = $Q/1000;
			$UM	 = 2;
			}
		else{
			$QTA = $Q;
			$UM	 = 1;
			}
		}
	return $QTA."|".$UM;
	}

function WriteMUD($content, $file, $position='bottom'){

	$content.=chr(13).chr(10); // <-------- VERIFICA!

	switch($position){

		case 'top':
			if(file_exists($file))
				$content    = $content.file_get_contents($file);
			break;

		default:
		case 'bottom':
			if(file_exists($file))
				$content	= file_get_contents($file).$content;
			break;

		}

	file_put_contents($file, $content);

	}

function CloseMUD($file){

	// get contents of a file into a string
	$handle		= fopen($file, "r");
	$content	= fread($handle, filesize($file));
	fclose($handle);
	unlink($file);
	return strtoupper($content);
	}

function createMudFile(){

	global $SOGER;
	global $FEDIT;
	$FEDIT->DbServerData["db"] = getDatabaseName();

	$IS_GESTORE_IMBALLAGGI = false;

	// Create temporary file to store MUD records
	$file		= "../__updates/MUD-".$SOGER->UserData['core_usersID_IMP'].".txt";
	$fp			= fopen($file, 'a+');
	fclose($fp);
	chmod($file, 0777);

	# aggiorno la disponibilit� di tutti i rifiuti
	$sqlDisponibilita="SELECT ID_RIF FROM user_schede_rifiuti WHERE ID_IMP='".getCodiceUL()."';";
	$FEDIT->SDBRead($sqlDisponibilita,"DispoRif",true,false);
        if($FEDIT->DbRecsNum>0) {
            for($r=0;$r<count($FEDIT->DispoRif);$r++){
                    $IMP_FILTER			= true;
                    $DISPO_PRINT_OUT	= false;
                    $IDRIF				= $FEDIT->DispoRif[$r]['ID_RIF'];
                    $TableName			= "user_movimenti_fiscalizzati";
                    $IDMov				= "ID_MOV_F";
                    $EXCLUDE_9999999	= true;
                    require("MovimentiDispoRIF.php");
                    if($Disponibilita<0) $Disponibilita=0;
                    $FEDIT->SDBWrite("UPDATE user_schede_rifiuti SET FKEdisponibilita='$Disponibilita' WHERE ID_RIF='" . $FEDIT->DispoRif[$r]['ID_RIF'] . "'",true,false);
                    }
        }

	// Record IA
	// IA - Scheda IMB - Gestione rifiuti da imballaggio
	$sql ="SELECT lov_cer.COD_CER, user_schede_rifiuti.ID_RIF, user_schede_rifiuti.ID_SF, user_schede_rifiuti.FKEdisponibilita AS GIAC_31_12, destinatario ";
	$sql.="FROM user_schede_rifiuti ";
	$sql.="JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
	$sql.="JOIN lov_stato_fisico ON lov_stato_fisico.ID_SF=user_schede_rifiuti.ID_SF ";
	$sql.="WHERE user_schede_rifiuti.ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."' ";
	$sql.="AND ID_RIF IN (SELECT ID_RIF FROM user_movimenti_fiscalizzati JOIN user_impianti_produttori ON user_movimenti_fiscalizzati.ID_UIMP=user_impianti_produttori.ID_UIMP JOIN user_impianti_destinatari ON user_movimenti_fiscalizzati.ID_UIMD=user_impianti_destinatari.ID_UIMD WHERE ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."' AND (produttore=1 OR (destinatario=1 AND ( FKEcfiscP<>FKEcfiscD OR TRIM(CONCAT(user_impianti_produttori.description, user_impianti_produttori.ID_COM))<>TRIM(CONCAT(user_impianti_destinatari.description, user_impianti_destinatari.ID_COM))) ) )) AND ";
	$sql.="lov_cer.COD_CER IN('150101', '150102', '150103', '150104', '150105', '150106', '150107', '150109', '191201', '191202', '191203', '191204', '191205', '191207', '191212') ";
	$sql.="ORDER BY COD_CER, user_schede_rifiuti.ID_SF ";
	$FEDIT->SdbRead($sql,"DbRecordset",true,false);

	$DICH_IMB = false;
        if($FEDIT->DbRecsNum>0) {
            for($i=0;$i<count($FEDIT->DbRecordset);$i++){
                    if($FEDIT->DbRecordset[$i]['destinatario']=='1'){
                            $DICH_IMB = true;
                            }
                    }
        }
	if($DICH_IMB){
		// Se hanno stesso CER "sono lo stesso rifiuto"
		$RBUFFER	= "000000";
		$RIFIUTI	= array();
		$GIAC_31_12 = 0;
		for($r=0;$r<count($FEDIT->DbRecordset);$r++){
			if($FEDIT->DbRecordset[$r]['COD_CER']!=$RBUFFER){
				$RIFIUTI[]	= $FEDIT->DbRecordset[$r];
				}
			else{
				$RIFIUTI[count($RIFIUTI)-1]['ID_RIF']	.= "|".$FEDIT->DbRecordset[$r]['ID_RIF'];
				}
			$RBUFFER	= $FEDIT->DbRecordset[$r]['COD_CER'];
			$GIAC_31_12+= $FEDIT->DbRecordset[$r]['GIAC_31_12'];
			}

		if(count($RIFIUTI)>0){
			$IS_GESTORE_IMBALLAGGI = true;
			IArecord($file, $RIFIUTI, $GIAC_31_12);
			}
		}

	// Record BA
	// BA - Scheda RIF - Comunicazione Rifiuti
	$sql ="SELECT lov_cer.COD_CER, user_schede_rifiuti.ID_RIF, user_schede_rifiuti.ID_SF ";
	$sql.="FROM user_schede_rifiuti ";
	$sql.="JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
	$sql.="JOIN lov_stato_fisico ON lov_stato_fisico.ID_SF=user_schede_rifiuti.ID_SF ";
	$sql.="WHERE user_schede_rifiuti.ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."' ";
	$sql.="AND ID_RIF IN (SELECT ID_RIF FROM user_movimenti_fiscalizzati WHERE ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."' AND intermediario=0) ";
	if($IS_GESTORE_IMBALLAGGI)
		$sql.="AND lov_cer.COD_CER NOT IN('150101', '150102', '150103', '150104', '150105', '150106', '150107', '150109', '191201', '191202', '191203', '191204', '191205', '191207', '191212') ";

	$sql.="ORDER BY COD_CER, user_schede_rifiuti.ID_SF ";
	$FEDIT->SdbRead($sql,"DbRecordset",true,false);

	// Se hanno stesso CER e ID_SF, "sono lo stesso rifiuto"
	$RBUFFER	= "000000|0";
	$RIFIUTI	= array();
        if($FEDIT->DbRecsNum>0) {
            for($r=0;$r<count($FEDIT->DbRecordset);$r++){
                    if($FEDIT->DbRecordset[$r]['COD_CER']."|".$FEDIT->DbRecordset[$r]['ID_SF']!=$RBUFFER){
                            $RIFIUTI[]	= $FEDIT->DbRecordset[$r];
                            }
                    else{
                            $RIFIUTI[count($RIFIUTI)-1]['ID_RIF']	.= "|".$FEDIT->DbRecordset[$r]['ID_RIF'];
                            $RIFIUTI[count($RIFIUTI)-1]['ID_SF']	.= "|".$FEDIT->DbRecordset[$r]['ID_SF'];
                            }
                    $RBUFFER	= $FEDIT->DbRecordset[$r]['COD_CER']."|".$FEDIT->DbRecordset[$r]['ID_SF'];
                    }
        }

	for($r=0;$r<count($RIFIUTI);$r++){
		BArecord($file, $RIFIUTI[$r], $r+1);
		}


	// Record DA
	// DA - Scheda INT - Rifiuti commercializzati ed intermediati senza detenzione
	$sql ="SELECT lov_cer.COD_CER, user_schede_rifiuti.ID_RIF, user_schede_rifiuti.ID_SF ";
	$sql.="FROM user_schede_rifiuti ";
	$sql.="JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
	$sql.="JOIN lov_stato_fisico ON lov_stato_fisico.ID_SF=user_schede_rifiuti.ID_SF ";
	$sql.="WHERE user_schede_rifiuti.ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."' ";
	$sql.="AND ID_RIF IN (SELECT ID_RIF FROM user_movimenti_fiscalizzati WHERE ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."' AND intermediario=1) ";
	$sql.="ORDER BY COD_CER, user_schede_rifiuti.ID_SF ";
	$FEDIT->SdbRead($sql,"DbRecordset",true,false);

	// Se hanno stesso CER e ID_SF, "sono lo stesso rifiuto"
	$RBUFFER	= "000000|0";
	$RIFIUTI	= array();
        if($FEDIT->DbRecsNum>0) {
            for($r=0;$r<count($FEDIT->DbRecordset);$r++){
                    if($FEDIT->DbRecordset[$r]['COD_CER']."|".$FEDIT->DbRecordset[$r]['ID_SF']!=$RBUFFER){
                            $RIFIUTI[]	= $FEDIT->DbRecordset[$r];
                            }
                    else{
                            $RIFIUTI[count($RIFIUTI)-1]['ID_RIF']	.= "|".$FEDIT->DbRecordset[$r]['ID_RIF'];
                            $RIFIUTI[count($RIFIUTI)-1]['ID_SF']	.= "|".$FEDIT->DbRecordset[$r]['ID_SF'];
                            }
                    $RBUFFER	= $FEDIT->DbRecordset[$r]['COD_CER']."|".$FEDIT->DbRecordset[$r]['ID_SF'];
                    }
        }

	for($r=0;$r<count($RIFIUTI);$r++){
		DArecord($file, $RIFIUTI[$r], $r+1);
		}


	## CALLED IN REVERSED ORDER, PUT CONTENT AT THE BEGINNING OF THE FILE

        //if($REGISTRO['D']==1){
            // Record AU
            // AU- Scheda SA-AUT - Sezione anagrafica - Scheda autorizzazioni
            // AUrecord($file);
        //}

        // Record AB
	// AB- Scheda SA-2 - Sezione anagrafica - Scheda riassuntiva
	ABrecord($file);

	// Record AA
	// AA- Scheda SA-1 - ANAGRAFICA AZIENDA E UNITA' LOCALE
	AArecord($file);

	// Record XX
	// Pur essendo il primo nel file prodotto, deve essere l'ultimo elaborato
	XXrecord($file);

	return CloseMUD($file);

	}



function IArecord($file, $RIFIUTI, $GIAC_31_12){

	global $SOGER;
	global $FEDIT;
	$CODICE_UL					= getCodiceUL();
	$CODICE_FISCALE				= getCodiceFiscale();
	$MUD_PS_DEST				= getMudPsDest();
	$ANNO_DICHIARAZIONE			= getAnnoDichiarazione();
	$FEDIT->DbServerData["db"]	= getDatabaseName();

	## Costante tipo record
	$IARecord = "IA;";
	## Anno di riferimento della dichiarazione (AAAA)
	$IARecord.= $ANNO_DICHIARAZIONE.";";
	## codice fiscale identificativo
	$IARecord.= spaceFiller(strtoupper($CODICE_FISCALE),16).";";
	## codice di identificazione univoca dell'unit� locale
	$IARecord.= spaceFiller(strtoupper($CODICE_UL),15).";";
	## quantit� di rifiuto consegnato a terzi
	$rifs="";
	for($r=0;$r<count($RIFIUTI);$r++){
		$rifs.=$RIFIUTI[$r]['ID_RIF']."|";
		}
	$rifs = substr($rifs, 0, strlen($rifs)-1);
	if($MUD_PS_DEST=='1')
		$sql = "SELECT SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS QTA3 ";
	else
		$sql = "SELECT SUM(IF( PS_DESTINO >0 AND VER_DESTINO=1, PS_DESTINO, pesoN )) AS QTA3 ";
	$sql.= "FROM user_movimenti_fiscalizzati ";
	$sql.= "JOIN user_impianti_produttori ON user_impianti_produttori.ID_UIMP=user_movimenti_fiscalizzati.ID_UIMP ";
	$sql.= "JOIN user_impianti_destinatari ON user_impianti_destinatari.ID_UIMD=user_movimenti_fiscalizzati.ID_UIMD ";
	$sql.= "WHERE ID_RIF IN (".str_replace("|",",",$rifs).") AND TIPO='S' AND PerRiclassificazione=0 AND DEST_ACC_RIFIUTO<>3 AND user_movimenti_fiscalizzati.produttore='1' AND ";
	$sql.="user_movimenti_fiscalizzati.ID_AZD<>0000000 AND ";
	$sql.= "( FKEcfiscP<>FKEcfiscD OR TRIM(CONCAT(user_impianti_produttori.description, user_impianti_produttori.ID_COM))<>TRIM(CONCAT(user_impianti_destinatari.description, user_impianti_destinatari.ID_COM))) AND ";
	$sql.= "(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') ";
	$FEDIT->SdbRead($sql,"DbRecordsetQTA3",true,false);
	$QTA = explode("|",formatQuantita($FEDIT->DbRecordsetQTA3[0]['QTA3']));
	$IARecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
	## Unit� di misura
	$IARecord.= $QTA[1].";";
	## Numero di MODULI RT-IMB allegati
	$ModuliRT_IMB = ICrecord_RT($RIFIUTI);
	$IARecord.= zeroFiller(count($ModuliRT_IMB),5).";";
	## Numero di MODULI DR-IMB allegati
	$ModuliDR_IMB = IDrecord($RIFIUTI);
	$IARecord.= zeroFiller(count($ModuliDR_IMB),5).";";
	## Numero di MODULI TE-IMB allegati
	$ModuliTE_IMB = ICrecord_TE($RIFIUTI);
	$IARecord.= zeroFiller(count($ModuliTE_IMB),5).";";
	## Numero di MODULI MG-IMB allegati
	$IARecord.= "00000;";
	## Rifiuti in giacenza al 31/12
	$QTA = explode("|",formatQuantita($GIAC_31_12));
	$IARecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
	## Unit� di misura
	$IARecord.= $QTA[1].";";
	
	## IA
	WriteMUD($IARecord, $file, 'bottom');

	## IB
	for($r=0;$r<count($RIFIUTI);$r++){
		IBrecord($file, $RIFIUTI[$r], $r+1);
		}

	## IC - RT_IMB
	for($rtImb=0;$rtImb<count($ModuliRT_IMB);$rtImb++){
		WriteMUD($ModuliRT_IMB[$rtImb], $file, 'bottom');
		}

	## IC - TE_IMB
	for($teImb=0;$teImb<count($ModuliTE_IMB);$teImb++){
		WriteMUD($ModuliTE_IMB[$teImb], $file, 'bottom');
		}

	## ID
	for($drImb=0;$drImb<count($ModuliDR_IMB);$drImb++){
		WriteMUD($ModuliDR_IMB[$drImb], $file, 'bottom');
		}
	}

function IBrecord($file, $rifiuto, $r){

	global $SOGER;
	global $FEDIT;
	$CODICE_UL					= getCodiceUL();
	$CODICE_FISCALE				= getCodiceFiscale();
	$MUD_PS_DEST				= getMudPsDest();
	$ANNO_DICHIARAZIONE			= getAnnoDichiarazione();
	$FEDIT->DbServerData["db"]	= getDatabaseName();
	$CER_RICEVUTI_DA_TERZI		= array('150101', '150102', '150103', '150104', '150105', '150106', '150107', '150109', '191201', '191202', '191203', '191204', '191205', '191207', '191212');

	$sql = "SELECT ROUND(SUM(pesoN),3) AS RICEVUTO, user_aziende_produttori.IscrittoCONAI ";
	$sql.= "FROM user_movimenti_fiscalizzati ";
	$sql.= "JOIN user_aziende_produttori ON user_aziende_produttori.ID_AZP=user_movimenti_fiscalizzati.ID_AZP ";
	$sql.= "WHERE ID_RIF IN (".str_replace("|",",",$rifiuto['ID_RIF']).") AND TIPO='C' AND user_movimenti_fiscalizzati.destinatario='1' AND ";
	$sql.= "(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') ";
	$sql.= "GROUP BY IscrittoCONAI ";
	$FEDIT->SdbRead($sql,"DbRecordsetIB",true,false);

        if($FEDIT->DbRecsNum>0) {
            for($ib=0;$ib<count($FEDIT->DbRecordsetIB);$ib++){
                    if(in_array($rifiuto['COD_CER'], $CER_RICEVUTI_DA_TERZI)){
                            ## Costante tipo record
                            $IBRecord = "IB;";
                            ## Anno di riferimento della dichiarazione (AAAA)
                            $IBRecord.= $ANNO_DICHIARAZIONE.";";
                            ## codice fiscale identificativo
                            $IBRecord.= spaceFiller(strtoupper($CODICE_FISCALE),16).";";
                            ## codice di identificazione univoca dell'unit� locale
                            $IBRecord.= spaceFiller(strtoupper($CODICE_UL),15).";";
                            ## codice del rifiuto (CER)
                            $IBRecord.= $rifiuto['COD_CER'].";";
							## Alluminio
							$IBRecord.= "0;"; // va scorporato manualmente
                            ## Tipo di rifiuto 
							## RC = Ricevuto da Superficie Pubblica (ex "da circuito CONAI")
							## RX = Ricevuto da Superficie Privata (ex "circuito extra CONAI")
                            $IBRecord.= ($FEDIT->DbRecordsetIB[$ib]['IscrittoCONAI']==0 ? "RX;" : "RC;");
                            ## Quantit�
                            $QTA = explode("|",formatQuantita($FEDIT->DbRecordsetIB[$ib]['RICEVUTO']));
                            $IBRecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
                            ## Unit� di misura
                            $IBRecord.= $QTA[1].";";
                            ## IB
                            WriteMUD($IBRecord, $file, 'bottom');
                            }
                    }
        }

	if($MUD_PS_DEST=='1')
		$sql = "SELECT SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS PRODOTTO ";
	else
		$sql = "SELECT SUM(IF( PS_DESTINO >0 AND VER_DESTINO=1, PS_DESTINO, pesoN )) AS PRODOTTO ";
	$sql.= "FROM user_movimenti_fiscalizzati ";
	$sql.= "WHERE ID_RIF IN (".str_replace("|",",",$rifiuto['ID_RIF']).") AND TIPO='S' AND PerRiclassificazione=0 AND DEST_ACC_RIFIUTO<>3 AND user_movimenti_fiscalizzati.produttore='1' AND ";
	$sql.= "(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') ";
	$FEDIT->SdbRead($sql,"DbRecordsetIB",true,false);
	## Costante tipo record
	$IBRecord = "IB;";
	## Anno di riferimento della dichiarazione (AAAA)
	$IBRecord.= $ANNO_DICHIARAZIONE.";";
	## codice fiscale identificativo
	$IBRecord.= spaceFiller(strtoupper($CODICE_FISCALE),16).";";
	## codice di identificazione univoca dell'unit� locale
	$IBRecord.= spaceFiller(strtoupper($CODICE_UL),15).";";
	## codice del rifiuto (CER)
	$IBRecord.= $rifiuto['COD_CER'].";";
	## Alluminio
	$IBRecord.= "0;"; // va scorporato manualmente
	## Tipo di rifiuto 
	## P1 = Prodotto nell'unit� locale da trattamento imballaggi monomateriale
	## PP = Prodotto nell'unit� locale da trattamento imballaggi multimateriale
	$IBRecord.= "P1;";
	## Quantit�
	$QTA = explode("|",formatQuantita($FEDIT->DbRecordsetIB[0]['PRODOTTO']));
	$IBRecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
	## Unit� di misura
	$IBRecord.= $QTA[1].";";
	## IB
	WriteMUD($IBRecord, $file, 'bottom');

	}



function ICrecord_RT($rifiuti){

	global $SOGER;
	global $FEDIT;
	$CODICE_UL					= getCodiceUL();
	$CODICE_FISCALE				= getCodiceFiscale();
	$MUD_PS_DEST				= getMudPsDest();
	$ANNO_DICHIARAZIONE			= getAnnoDichiarazione();
	$FEDIT->DbServerData["db"]	= getDatabaseName();
	//$CER_RICEVUTI_DA_TERZI		= array('150101', '150102', '150103', '150104', '150105', '150106', '150107', '150109');
	$CER_RICEVUTI_DA_TERZI		= array('150101', '150102', '150103', '150104', '150105', '150106', '150107', '150109', '191201', '191202', '191203', '191204', '191205', '191207', '191212');


	for($r=0;$r<count($rifiuti);$r++){

		if(in_array($rifiuti[$r]['COD_CER'], $CER_RICEVUTI_DA_TERZI)){

			$sql ="SELECT DISTINCT user_movimenti_fiscalizzati.ID_UIMP, TRIM(user_aziende_produttori.description) as azienda, user_aziende_produttori.codfisc, ";
			$sql.="TRIM(user_impianti_produttori.description) as indirizzo, lov_comuni_istat.cod_prov, lov_comuni_istat.cod_comISTAT as cod_com, ";
			$sql.="lov_comuni_istat.CAP, lov_comuni_istat.nazione, ROUND(SUM(pesoN),3) AS RICEVUTO, user_aziende_produttori.IscrittoCONAI ";
			$sql.="FROM user_movimenti_fiscalizzati ";
			$sql.="JOIN user_impianti_produttori ON user_movimenti_fiscalizzati.ID_UIMP=user_impianti_produttori.ID_UIMP ";
			$sql.="JOIN user_impianti_destinatari ON user_impianti_destinatari.ID_UIMD=user_movimenti_fiscalizzati.ID_UIMD ";
			$sql.="JOIN user_aziende_produttori ON user_impianti_produttori.ID_AZP=user_aziende_produttori.ID_AZP ";
			$sql.="JOIN lov_comuni_istat ON user_impianti_produttori.ID_COM=lov_comuni_istat.ID_COM ";
			$sql.="WHERE ID_RIF IN (".str_replace("|",",",$rifiuti[$r]['ID_RIF']).") AND ";
			$sql.="TIPO='C' AND user_movimenti_fiscalizzati.destinatario=1 AND ";
			$sql.="(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') AND ";
			$sql.="( FKEcfiscP<>FKEcfiscD OR TRIM(CONCAT(user_impianti_produttori.description, user_impianti_produttori.ID_COM))<>TRIM(CONCAT(user_impianti_destinatari.description, user_impianti_destinatari.ID_COM))) ";
			$sql.="GROUP BY ID_UIMP ";
			$FEDIT->SdbRead($sql,"DbRecordsetRT",true,false);

                        if($FEDIT->DbRecsNum>0) {
                            for($RT=0;$RT<count($FEDIT->DbRecordsetRT);$RT++){
                                    ## Costante tipo record
                                    $ICRecord = "IC;";
                                    ## Anno di riferimento della dichiarazione (AAAA)
                                    $ICRecord.= $ANNO_DICHIARAZIONE.";";
                                    ## codice fiscale identificativo
                                    $ICRecord.= spaceFiller(strtoupper($CODICE_FISCALE),16).";";
                                    ## codice di identificazione univoca dell'unit� locale
                                    $ICRecord.= spaceFiller(strtoupper($CODICE_UL),15).";";
                                    ## codice del rifiuto (CER)
                                    $ICRecord.= $rifiuti[$r]['COD_CER'].";";
                                    ## Tipo allegato ("RT" / "TE")
                                    $ICRecord.= "RT;";
                                    ## Numero progressivo dell'allegato
                                    $ICRecord.= zeroFiller(($RT+1),5).";";
                                    ## Codice fiscale del soggetto che ha conferito
                                    $ICRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetRT[$RT]['codfisc']),16).";";
                                    ## Nome o ragione sociale
                                    $ICRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetRT[$RT]['azienda']),60).";";
                                    ## codice istat provincia
                                    $ICRecord.= zeroFiller($FEDIT->DbRecordsetRT[$RT]['cod_prov'],3).";";
                                    ## codice istat comune
                                    $ICRecord.= zeroFiller($FEDIT->DbRecordsetRT[$RT]['cod_com'],3).";";
                                    ## via
                                    $ICRecord.= spaceFiller($FEDIT->DbRecordsetRT[$RT]['indirizzo'],30).";";
                                    ## civico
                                    $ICRecord.= "      ;";
                                    ## cap
                                    $ICRecord.= spaceFiller($FEDIT->DbRecordsetRT[$RT]['CAP'],5).";";
                                    ## Quantit� dichiarata
                                    $QTA = explode("|",formatQuantita($FEDIT->DbRecordsetRT[$RT]['RICEVUTO']));
                                    $ICRecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
                                    ## Unit� di misura
                                    $ICRecord.= $QTA[1].";";
                                    ## nome della nazione - se estero
                                    if($FEDIT->DbRecordsetRT[$RT]['nazione']!='ITALIA') $NAZIONE = $FEDIT->DbRecordsetRT[$RT]['nazione']; else $NAZIONE='';
                                    $ICRecord.=spaceFiller($NAZIONE,20).";";
                                    ## Codice Regolamento CEE 1013/2006
                                    $ICRecord.="      ;";
                                    ## Provenienza del rifiuto
									## 1 = Superficie pubblica (ex "Circuito CONAI")
									## 2 = Superficie privata (ex "Circuito Extra CONAI")
                                    $ICRecord.=($FEDIT->DbRecordsetRT[$RT]['IscrittoCONAI']==0 ? "2;" : "1;");
									## Tipo di trattamento previsto
									## 0 = ND
									## 1 = Recupero di materia
									## 2 = Recupero di energia
									## 3 = Discarica
									## 4 = Incenerimento
									## 5 = Altre operazioni di smaltimento
									$ICRecord.="0;";

                                    $ModuliRT_IMB[]	= $ICRecord;
                                    //unset($ICRecord);
                                    }
                        }
			}
		}
	return $ModuliRT_IMB;
	}



function ICrecord_TE($rifiuti){

	global $SOGER;
	global $FEDIT;
	$CODICE_UL					= getCodiceUL();
	$CODICE_FISCALE				= getCodiceFiscale();
	$MUD_PS_DEST				= getMudPsDest();
	$ANNO_DICHIARAZIONE			= getAnnoDichiarazione();
	$FEDIT->DbServerData["db"]	= getDatabaseName();

	for($r=0;$r<count($rifiuti);$r++){

		//$ModuliTE_IMB = array();

		$sql ="SELECT DISTINCT user_movimenti_fiscalizzati.ID_AZT, TRIM(user_aziende_trasportatori.description) as azienda, ";
		$sql.="user_aziende_trasportatori.codfisc, lov_comuni_istat.nazione, ";
		if($MUD_PS_DEST=='1')
			$sql.="SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS CONFERITO_T3 ";
		else
			$sql.="SUM(IF( PS_DESTINO >0 AND VER_DESTINO=1, PS_DESTINO, pesoN )) AS CONFERITO_T3 ";
		$sql.="FROM user_movimenti_fiscalizzati ";
		$sql.="JOIN user_aziende_trasportatori ON user_aziende_trasportatori.ID_AZT=user_movimenti_fiscalizzati.ID_AZT ";
		$sql.="JOIN lov_comuni_istat ON user_aziende_trasportatori.ID_COM=lov_comuni_istat.ID_COM ";
		$sql.="WHERE ID_RIF IN (".str_replace("|",",",$rifiuti[$r]['ID_RIF']).") AND TIPO='S' AND PerRiclassificazione=0 AND DEST_ACC_RIFIUTO<>3 AND user_movimenti_fiscalizzati.produttore='1' AND ";
		$sql.="FKEcfiscD<>FKEcfiscT AND user_movimenti_fiscalizzati.ID_AZT<>0000000 AND ";
		$sql.="(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') ";
		$sql.="GROUP BY user_movimenti_fiscalizzati.ID_AZT ";
		$sql.="HAVING CONFERITO_T3>0 ";
		$FEDIT->SdbRead($sql,"DbRecordsetTE",true,false);

                if($FEDIT->DbRecsNum>0) {
                    for($TE=0;$TE<count($FEDIT->DbRecordsetTE);$TE++){
                            ## Costante tipo record
                            $ICRecord = "IC;";
                            ## Anno di riferimento della dichiarazione (AAAA)
                            $ICRecord.= $ANNO_DICHIARAZIONE.";";
                            ## codice fiscale identificativo
                            $ICRecord.= spaceFiller(strtoupper($CODICE_FISCALE),16).";";
                            ## codice di identificazione univoca dell'unit� locale
                            $ICRecord.= spaceFiller(strtoupper($CODICE_UL),15).";";
                            ## codice del rifiuto (CER)
                            $ICRecord.= $rifiuti[$r]['COD_CER'].";";
                            ## Tipo allegato ("RT" / "TE")
                            $ICRecord.= "TE;";
                            ## Numero progressivo dell'allegato
                            $ICRecord.= zeroFiller(($TE+1),5).";";
                            ## Codice fiscale del soggetto che ha trasportato
                            $ICRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetTE[$TE]['codfisc']),16).";";
                            ## Nome o ragione sociale
                            $ICRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetTE[$TE]['azienda']),60).";";
                            ## codice istat provincia
                            $ICRecord.= "   ;";
                            ## codice istat comune
                            $ICRecord.= "   ;";
                            ## via
                            $ICRecord.= "                              ;";
                            ## civico
                            $ICRecord.= "      ;";
                            ## cap
                            $ICRecord.= "     ;";
                            ## Quantit� dichiarata
                            $QTA = explode("|",formatQuantita($FEDIT->DbRecordsetTE[$TE]['CONFERITO_T3']));
                            $ICRecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
                            ## Unit� di misura
                            $ICRecord.= $QTA[1].";";
                            ## nome della nazione - se estero
                            $ICRecord.="                    ;";
                            ## Codice Regolamento CEE 1013/2006
                            $ICRecord.="      ;";
                            ## Provenienza del rifiuto
							## 1 = Superficie pubblica (ex "Circuito CONAI")
							## 2 = Superficie privata (ex "Circuito Extra CONAI")
							$ICRecord.=($FEDIT->DbRecordsetRT[$RT]['IscrittoCONAI']==0 ? "2;" : "1;");
							## Tipo di trattamento previsto
							## 0 = ND
							## 1 = Recupero di materia
							## 2 = Recupero di energia
							## 3 = Discarica
							## 4 = Incenerimento
							## 5 = Altre operazioni di smaltimento
							$ICRecord.="0;";

                            $ModuliTE_IMB[]	= $ICRecord;
                            //unset($ICRecord);
                            }
                }
		}
	return $ModuliTE_IMB;
	}




function IDrecord($rifiuti){

	global $SOGER;
	global $FEDIT;
	$CODICE_UL					= getCodiceUL();
	$CODICE_FISCALE				= getCodiceFiscale();
	$MUD_PS_DEST				= getMudPsDest();
	$ANNO_DICHIARAZIONE			= getAnnoDichiarazione();
	$FEDIT->DbServerData["db"]	= getDatabaseName();

	for($r=0;$r<count($rifiuti);$r++){

		//$ModuliDR_IMB	= array();
		$OP_RD			= array('R1'=>0, 'R2'=>0, 'R3'=>0, 'R4'=>0, 'R5'=>0, 'R6'=>0, 'R7'=>0, 'R8'=>0, 'R9'=>0, 'R10'=>0, 'R11'=>0, 'R12'=>0, 'R13'=>0, 'D1'=>0, 'D2'=>0, 'D3'=>0, 'D4'=>0, 'D5'=>0, 'D6'=>0, 'D7'=>0, 'D8'=>0, 'D9'=>0, 'D10'=>0, 'D11'=>0, 'D12'=>0, 'D13'=>0, 'D14'=>0, 'D15'=>0);
		$DOUBLE_RD		= array();
		$starting_pos	= 229;

		$sql ="SELECT DISTINCT user_movimenti_fiscalizzati.ID_UIMD AS ID_UIMD, TRIM(user_aziende_destinatari.description) as azienda, ";
		$sql.="user_aziende_destinatari.codfisc, TRIM(user_impianti_destinatari.description) as indirizzo, lov_comuni_istat.cod_prov, ";
		$sql.="lov_comuni_istat.cod_comISTAT as cod_com, lov_comuni_istat.CAP, lov_comuni_istat.nazione,";
		if($MUD_PS_DEST=='1')
			$sql.="SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS CONFERITO ";
		else
			$sql.="SUM(IF( PS_DESTINO >0 AND VER_DESTINO=1, PS_DESTINO, pesoN )) AS CONFERITO ";
		$sql.="FROM user_movimenti_fiscalizzati ";
		$sql.="JOIN user_aziende_destinatari ON user_aziende_destinatari.ID_AZD=user_movimenti_fiscalizzati.ID_AZD ";
		$sql.="JOIN user_impianti_destinatari ON user_impianti_destinatari.ID_UIMD=user_movimenti_fiscalizzati.ID_UIMD ";
		$sql.="JOIN user_impianti_produttori ON user_movimenti_fiscalizzati.ID_UIMP=user_impianti_produttori.ID_UIMP ";
		$sql.="JOIN lov_comuni_istat ON user_impianti_destinatari.ID_COM=lov_comuni_istat.ID_COM ";
		$sql.="WHERE ID_RIF IN (".str_replace("|",",",$rifiuti[$r]['ID_RIF']).") AND TIPO='S' AND PerRiclassificazione=0 AND DEST_ACC_RIFIUTO<>3 AND user_movimenti_fiscalizzati.produttore='1' AND ";
		$sql.="user_movimenti_fiscalizzati.ID_AZD<>0000000 AND ";
		$sql.= "( FKEcfiscP<>FKEcfiscD OR TRIM(CONCAT(user_impianti_produttori.description, user_impianti_produttori.ID_COM))<>TRIM(CONCAT(user_impianti_destinatari.description, user_impianti_destinatari.ID_COM))) AND ";
		$sql.="(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') ";
		$sql.="GROUP BY user_movimenti_fiscalizzati.ID_UIMD ";
		$sql.="HAVING CONFERITO>0 ";
		$FEDIT->SdbRead($sql,"DbRecordsetDR",true,false);

                if($FEDIT->DbRecsNum>0) {
                    for($DR=0;$DR<count($FEDIT->DbRecordsetDR);$DR++){
                            ## Costante tipo record
                            $IDRecord = "ID;";
                            ## Anno di riferimento della dichiarazione (AAAA)
                            $IDRecord.= $ANNO_DICHIARAZIONE.";";
                            ## codice fiscale identificativo
                            $IDRecord.= spaceFiller(strtoupper($CODICE_FISCALE),16).";";
                            ## codice di identificazione univoca dell'unit� locale
                            $IDRecord.= spaceFiller(strtoupper($CODICE_UL),15).";";
                            ## codice del rifiuto (CER)
                            $IDRecord.= $rifiuti[$r]['COD_CER'].";";
                            ## Numero progressivo dell'allegato
                            $IDRecord.= zeroFiller(($DR+1),5).";";
                            ## Codice fiscale del soggetto destinatario
                            $IDRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetDR[$DR]['codfisc']),16).";";
                            ## Allumionio (valorizzare in caso di rifiuto 150104)
                            $IDRecord.=($rifiuti[$r]['COD_CER']=='150104' ? "1;" : " ;");
                            ## Nome o ragione sociale
                            $IDRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetDR[$DR]['azienda']),60).";";
                            ## codice istat provincia
                            $IDRecord.= zeroFiller($FEDIT->DbRecordsetDR[$DR]['cod_prov'],3).";";
                            ## codice istat comune
                            $IDRecord.= zeroFiller($FEDIT->DbRecordsetDR[$DR]['cod_com'],3).";";
                            ## via
                            $IDRecord.= spaceFiller($FEDIT->DbRecordsetDR[$DR]['indirizzo'],30).";";
                            ## civico
                            $IDRecord.= "      ;";
                            ## cap
                            $IDRecord.= spaceFiller($FEDIT->DbRecordsetDR[$DR]['CAP'],5).";";
                            ## Quantit� dichiarata
                            $QTA = explode("|",formatQuantita($FEDIT->DbRecordsetDR[$DR]['CONFERITO']));
                            $IDRecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
                            ## Unit� di misura
                            $IDRecord.= $QTA[1].";";
                            ## nome della nazione - se estero
                            if($FEDIT->DbRecordsetDR[$DR]['nazione']!='ITALIA') $NAZIONE = $FEDIT->DbRecordsetDR[$DR]['nazione']; else $NAZIONE='';
                            $IDRecord.=spaceFiller($NAZIONE,20).";";
                            ## Codice Regolamento CEE 1013/2006
                            $IDRecord.="      ;";

                            ##
                            if($MUD_PS_DEST=='1')
                                    $sql ="SELECT SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS CONFERITO, lov_operazioni_rs.description AS RD ";
                            else
                                    $sql ="SELECT SUM(IF( PS_DESTINO >0 AND VER_DESTINO=1, PS_DESTINO, pesoN )) AS CONFERITO, lov_operazioni_rs.description AS RD ";
                            $sql.="FROM user_movimenti_fiscalizzati ";
                            $sql.="JOIN lov_operazioni_rs ON lov_operazioni_rs.ID_OP_RS=user_movimenti_fiscalizzati.ID_OP_RS ";
                            $sql.="WHERE ID_RIF IN (".str_replace("|",",",$rifiuti[$r]['ID_RIF']).") AND TIPO='S' AND PerRiclassificazione=0 AND DEST_ACC_RIFIUTO<>3 AND ";
                            $sql.="user_movimenti_fiscalizzati.produttore='1' AND ";
                            $sql.="ID_UIMD=".$FEDIT->DbRecordsetDR[$DR]['ID_UIMD']." AND ";
                            $sql.="(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') ";
                            $sql.="GROUP BY RD ";
                            $FEDIT->SdbRead($sql,"DbRecordsetOP_RD",true,false);
                            for($rd=0;$rd<count($FEDIT->DbRecordsetOP_RD);$rd++){
                                    $OP_RD[$FEDIT->DbRecordsetOP_RD[$rd]['RD']] += $FEDIT->DbRecordsetOP_RD[$rd]['CONFERITO'];
                                    }
                            for($i=1;$i<=13;$i++){
                                    if($OP_RD['R'.$i]>0 AND $OP_RD['D'.$i]>0) $DOUBLE_RD[] = $i;
                                    }
                            for($i=1;$i<=13;$i++){
                                    if(!in_array($i, $DOUBLE_RD)){
                                            $DestRif = "R;";
                                            if($OP_RD['R'.$i]>0){
                                                    $QTA = explode("|",formatQuantita($OP_RD['R'.$i]));
                                                    }
                                            else{
                                                    $QTA = explode("|",formatQuantita($OP_RD['D'.$i]));
                                                    $DestRif = "D;";
                                                    }
                                            ## Quantit� dichiarata - R1-->R13 / D1-->D13
                                            $IDRecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
                                            ## Unit� di misura
                                            $IDRecord.= $QTA[1].";";
                                            ##Destinazione del rifiuto
                                            $IDRecord.= $DestRif;
                                            }
                                    else{
                                            ## Quantit� dichiarata - R1-->R13 (ma ho anche stesso D!)
                                            $QTA = explode("|",formatQuantita($OP_RD['R'.$i]));
                                            $IDRecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
                                            ## Unit� di misura
                                            $IDRecord.= $QTA[1].";";
                                            ##Destinazione del rifiuto
                                            $IDRecord.= "R;";
                                            }
                                    }
                            ## Quantit� dichiarata - D14
                            $QTA = explode("|",formatQuantita($OP_RD['D14']));
                            $IDRecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
                            ## Unit� di misura
                            $IDRecord.= $QTA[1].";";
                            ## Quantit� dichiarata - D15
                            $QTA = explode("|",formatQuantita($OP_RD['D15']));
                            $IDRecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
                            ## Unit� di misura
                            $IDRecord.= $QTA[1].";";

                            $ModuliDR_IMB[]	= $IDRecord;

                            if(count($DOUBLE_RD)>0){
                                    for($d=0;$d<count($DOUBLE_RD);$d++){
                                            ## Quantit� dichiarata - D1-->D13 (ho anche stesso R)
                                            $QTA = explode("|",formatQuantita($OP_RD['D'.$DOUBLE_RD[$d]]));
                                            $Dqta = zeroFiller(number_format($QTA[0],3,",",""),11).";";
                                            ## Unit� di misura
                                            $Dqta.= $QTA[1].";";
                                            ## Destinazione del rifiuto
                                            $Dqta.= "D;";
                                            $pos = $starting_pos + ($DOUBLE_RD[$d]*(${$DOUBLE_RD[$d]}-1));
                                            $IDRecord = substr_replace($IDRecord, $Dqta, $pos, 14);
                                            }
                                    $ModuliDR_IMB[]	= $IDRecord;
                                    }

                            //unset($IDRecord);
                            }
                }
		}
	return $ModuliDR_IMB;
	}





function DArecord($file, $rifiuto, $r){

	global $SOGER;
	global $FEDIT;
	$CODICE_UL					= getCodiceUL();
	$CODICE_FISCALE				= getCodiceFiscale();
	$MUD_PS_DEST				= getMudPsDest();
	$ANNO_DICHIARAZIONE			= getAnnoDichiarazione();
	$FEDIT->DbServerData["db"]	= getDatabaseName();

	## Costante tipo record
	$DARecord = "DA;";
	## Anno di riferimento della dichiarazione (AAAA)
	$DARecord.= $ANNO_DICHIARAZIONE.";";
	## codice fiscale identificativo
	$DARecord.= spaceFiller(strtoupper($CODICE_FISCALE),16).";";
	## codice di identificazione univoca dell'unit� locale
	$DARecord.= spaceFiller(strtoupper($CODICE_UL),15).";";
	## numero d'ordine progressivo di scheda rif
	$DARecord.= zeroFiller($r,4).";";
	## codice del rifiuto (CER)
	$DARecord.= $rifiuto['COD_CER'].";";
	## stato fisico
	$ID_SF = array_unique(explode("|", $rifiuto['ID_SF']));
	## Stato fisico: Solido pulverulento
	if(in_array('1', $ID_SF)) $DARecord.= "1;"; else $DARecord.= "0;";
	## Stato fisico: Solido non pulverulento
	if(in_array('2', $ID_SF)) $DARecord.= "1;"; else $DARecord.= "0;";
	## Stato fisico: Fangoso palabile
	if(in_array('3', $ID_SF)) $DARecord.= "1;"; else $DARecord.= "0;";
	## Stato fisico: Liquido
	if(in_array('4', $ID_SF)) $DARecord.= "1;"; else $DARecord.= "0;";
	## Stato fisico: Aeriforme
	$DARecord.= "0;";
	## Stato fisico: Vischioso e sciropposo
	$DARecord.= "0;";
	## Stato fisico: Altro
	$DARecord.= "0;";
	## Quantit� complessivamente intermediata
	$sql = "SELECT ROUND(SUM(pesoN),3) AS INTERMEDIATA ";
	$sql.= "FROM user_movimenti_fiscalizzati ";
	$sql.= "WHERE ID_RIF IN (".str_replace("|",",",$rifiuto['ID_RIF']).") AND TIPO='C' AND intermediario='1' AND ";
	$sql.= "(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') ";
	$FEDIT->SdbRead($sql,"DbRecordsetDA_1",true,false);
	$QTA = explode("|",formatQuantita($FEDIT->DbRecordsetDA_1[0]['INTERMEDIATA']));
	$DARecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
	## Unit� di misura
	$DARecord.= $QTA[1].";";

	## Numero di moduli UO allegati
	if($FEDIT->DbRecordsetDA_1[0]['INTERMEDIATA']>0){
		$ModuliUO = ModuliUO($rifiuto, $r);
		$DARecord.= zeroFiller(count($ModuliUO),5).";";
		}
	else
		$DARecord.= "00000;";

	## Numero di moduli UD allegati
	if($FEDIT->DbRecordsetDA_1[0]['INTERMEDIATA']>0){
		$ModuliUD = ModuliUD($rifiuto, $r);
		$DARecord.= zeroFiller(count($ModuliUD),5).";";
		}
	else
		$DARecord.= "00000;";

	## DA
	WriteMUD($DARecord, $file, 'bottom');

	## UO
	if($FEDIT->DbRecordsetDA_1[0]['INTERMEDIATA']>0){
		for($uo=0;$uo<count($ModuliUO);$uo++){
			WriteMUD($ModuliUO[$uo], $file, 'bottom');
			}
		}

	## UD
	if($FEDIT->DbRecordsetDA_1[0]['INTERMEDIATA']>0){
		for($ud=0;$ud<count($ModuliUD);$ud++){
			WriteMUD($ModuliUD[$ud], $file, 'bottom');
			}
		}

	}



function BArecord($file, $rifiuto, $r){

	global $SOGER;
	global $FEDIT;
	$CODICE_UL					= getCodiceUL();
	$CODICE_FISCALE				= getCodiceFiscale();
	$MUD_PS_DEST				= getMudPsDest();
	$ANNO_DICHIARAZIONE			= getAnnoDichiarazione();
	$FEDIT->DbServerData["db"]	= getDatabaseName();

	## Costante tipo record
	$BARecord = "BA;";
	## Anno di riferimento della dichiarazione (AAAA)
	$BARecord.= $ANNO_DICHIARAZIONE.";";
	## codice fiscale identificativo
	$BARecord.= spaceFiller(strtoupper($CODICE_FISCALE),16).";";
	## codice di identificazione univoca dell'unit� locale
	$BARecord.= spaceFiller(strtoupper($CODICE_UL),15).";";
	## numero d'ordine progressivo di scheda rif
	$BARecord.= zeroFiller($r,4).";";
	## codice del rifiuto (CER)
	$BARecord.= $rifiuto['COD_CER'].";";
	## stato fisico
	$ID_SF = array_unique(explode("|", $rifiuto['ID_SF']));
	## Stato fisico: Solido pulverulento
	if(in_array('1', $ID_SF)) $BARecord.= "1;"; else $BARecord.= "0;";
	## Stato fisico: Solido non pulverulento
	if(in_array('2', $ID_SF)) $BARecord.= "1;"; else $BARecord.= "0;";
	## Stato fisico: Fangoso palabile
	if(in_array('3', $ID_SF)) $BARecord.= "1;"; else $BARecord.= "0;";
	## Stato fisico: Liquido
	if(in_array('4', $ID_SF)) $BARecord.= "1;"; else $BARecord.= "0;";
	## Stato fisico: Aeriforme
	$BARecord.= "0;";
	## Stato fisico: Vischioso e sciropposo
	$BARecord.= "0;";
	## Stato fisico: Altro
	$BARecord.= "0;";
	## Rifiuto prodotto nell'unit� locale
	if($MUD_PS_DEST=='1')
            $sql = "SELECT ROUND(SUM(IF(PS_DESTINO>0, PS_DESTINO, pesoN)),3) AS PRODOTTO_IN ";
	else
            $sql = "SELECT ROUND(SUM(IF(PS_DESTINO>0 AND VER_DESTINO=1, PS_DESTINO, pesoN)),3) AS PRODOTTO_IN ";
	$sql.= ", ROUND(SUM(pesoN),3) AS STIMATO ";
	$sql.= "FROM user_movimenti_fiscalizzati ";
	$sql.= "JOIN user_impianti_produttori ON user_impianti_produttori.ID_UIMP=user_movimenti_fiscalizzati.ID_UIMP ";
	$sql.= "WHERE ID_RIF IN (".str_replace("|",",",$rifiuto['ID_RIF']).") AND TIPO='S' AND produttore='1' AND PerRiclassificazione=0 AND DEST_ACC_RIFIUTO<>3 AND FuoriUnitaLocale=0 AND ";
	$sql.= "(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') ";
	$FEDIT->SdbRead($sql,"DbRecordsetBA_1",true,false);
        $sql = "SELECT ROUND(SUM(FKEdisponibilita),3) AS G3112, ROUND(SUM(CASE WHEN ID_UMIS=1 THEN giac_ini WHEN ID_UMIS=2 THEN (giac_ini*peso_spec) WHEN ID_UMIS=3 THEN (giac_ini*peso_spec*1000) END),3) AS GINI ";
	$sql.= "FROM user_schede_rifiuti ";
	$sql.= "WHERE ID_RIF IN (".str_replace("|",",",$rifiuto['ID_RIF']).") AND produttore=1 ";
        $FEDIT->SdbRead($sql,"DbRecordsetBA_G",true,false);
        // giacenza iniziale originalID_RIF
        $sql = "SELECT ROUND(SUM(CASE WHEN ID_UMIS=1 THEN giac_ini WHEN ID_UMIS=2 THEN (giac_ini*peso_spec) WHEN ID_UMIS=3 THEN (giac_ini*peso_spec*1000) END),3) AS GINI ";
        $sql.= "FROM user_schede_rifiuti WHERE ";
        $sql.= "ID_RIF IN (SELECT DISTINCT originalID_RIF FROM user_schede_rifiuti WHERE ID_RIF IN (".str_replace("|",",",$rifiuto['ID_RIF']).") AND produttore=1)";
        $sql.= "AND ";
        $sql.= "ID_RIF NOT IN (".str_replace("|",",",$rifiuto['ID_RIF']).") AND produttore=1 ";
        $FEDIT->SdbRead($sql,"DbRecordsetBA_Gorig",true,false);

        $STIMATO		= $FEDIT->DbRecordsetBA_1[0]['STIMATO'];
	$PRODOTTO_IN            = $FEDIT->DbRecordsetBA_1[0]['PRODOTTO_IN'];
	$G3112			= $FEDIT->DbRecordsetBA_G[0]['G3112'];
	$GINI			= $FEDIT->DbRecordsetBA_G[0]['GINI'];
        $GINI_orig		= $FEDIT->DbRecordsetBA_Gorig[0]['GINI'];
//	if($STIMATO>$GINI)
//		$QTA = $PRODOTTO_IN + $G3112 - $GINI;
//	else
//		$QTA = '0|1';
        if(is_null($STIMATO)) $STIMATO = $PRODOTTO_IN = 0;
        if(is_null($GINI_orig)) $GINI_orig = 0;
        $QTA = $PRODOTTO_IN + $G3112 - $GINI - $GINI_orig;
        // Sottraggo ancora la giac.ini della scheda da cui ha avuto origine il rifiuto
	$QTA = explode("|",formatQuantita($QTA));
        $BARecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
	## Unit� di misura
	$BARecord.= $QTA[1].";";
	## Rifiuto Ricevuto da terzi
	$sql = "SELECT ROUND(SUM(pesoN),3) AS RICEVUTO ";
	$sql.= "FROM user_movimenti_fiscalizzati ";
	$sql.= "JOIN user_impianti_produttori ON user_impianti_produttori.ID_UIMP=user_movimenti_fiscalizzati.ID_UIMP ";
	$sql.= "JOIN user_impianti_destinatari ON user_impianti_destinatari.ID_UIMD=user_movimenti_fiscalizzati.ID_UIMD ";
	$sql.= "WHERE ID_RIF IN (".str_replace("|",",",$rifiuto['ID_RIF']).") AND TIPO='C' ";
	$sql.= "AND (trasportatore='1' OR ";
	$REGISTRO = getRegistriSoger();
	$sql.= ($REGISTRO['T']=='1' ? "(destinatario=1 AND FKEcfiscT<>'".getCodiceFiscale()."')" : "destinatario=1");
	$sql.= ") ";
	$sql.= "AND ( FKEcfiscP<>FKEcfiscD OR TRIM(CONCAT(user_impianti_produttori.description, user_impianti_produttori.ID_COM))<>TRIM(CONCAT(user_impianti_destinatari.description, user_impianti_destinatari.ID_COM))) AND ";
	$sql.= "(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31')";

	$FEDIT->SdbRead($sql,"DbRecordsetBA_2",true,false);
	$QTA = explode("|",formatQuantita($FEDIT->DbRecordsetBA_2[0]['RICEVUTO']));
	$BARecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
	## Unit� di misura
	$BARecord.= $QTA[1].";";
	## Numero di moduli RT allegati
	if($FEDIT->DbRecordsetBA_2[0]['RICEVUTO']>0){
		$ModuliRT = ModuliRT($rifiuto, $r);
		$BARecord.= zeroFiller(count($ModuliRT),5).";";
		}
	else
		$BARecord.= "00000;";
	## Rifiuto prodotto fuori dall'unit� locale
	if($MUD_PS_DEST=='1')
		$sql = "SELECT ROUND(SUM(IF(PS_DESTINO>0, PS_DESTINO, pesoN)),3) AS PRODOTTO_OUT ";
	else
		$sql = "SELECT ROUND(SUM(IF(PS_DESTINO>0 AND VER_DESTINO=1, PS_DESTINO, pesoN)),3) AS PRODOTTO_OUT ";
	$sql.= "FROM user_movimenti_fiscalizzati ";
	$sql.= "JOIN user_impianti_produttori ON user_impianti_produttori.ID_UIMP=user_movimenti_fiscalizzati.ID_UIMP ";
	$sql.= "WHERE ID_RIF IN (".str_replace("|",",",$rifiuto['ID_RIF']).") AND TIPO='S' AND PerRiclassificazione=0 AND DEST_ACC_RIFIUTO<>3 AND produttore='1' AND FuoriUnitaLocale=1 AND ";
	$sql.= "(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') ";
	$FEDIT->SdbRead($sql,"DbRecordsetBA_3",true,false);
	$QTA = explode("|",formatQuantita($FEDIT->DbRecordsetBA_3[0]['PRODOTTO_OUT']));
	$BARecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
	## Unit� di misura
	$BARecord.= $QTA[1].";";
	## Numero di moduli RE allegati
	if($FEDIT->DbRecordsetBA_3[0]['PRODOTTO_OUT']>0){
		$ModuliRE = ModuliRE($rifiuto, $r);
		$BARecord.= zeroFiller(count($ModuliRE),5).";";
		}
	else
		$BARecord.= "00000;";
	## Rifiuto trasportato dal dichiarante
	/*
	Si ricorda che nella casella rifiuto trasportato dal dichiarante dev'essere comunicata la
        quantit� trasportata in uscita dall'unit� locale del produttore/detentore verso unit� locali
        di soggetti terzi o altre unit� locali del soggetto dichiarante e NON devono essere
        comunicate le quantit� trasportate in ingresso all'unit� locale dichiarante.
        */
	if($MUD_PS_DEST=='1')
		$sql = "SELECT ROUND(SUM(IF(PS_DESTINO>0, PS_DESTINO, pesoN)),3) AS TRASPORTATO ";
	else
		$sql = "SELECT ROUND(SUM(IF(PS_DESTINO>0 AND VER_DESTINO=1, PS_DESTINO, pesoN)),3) AS TRASPORTATO ";
	$sql.= "FROM user_movimenti_fiscalizzati ";
	$sql.= "JOIN user_impianti_trasportatori ON user_impianti_trasportatori.ID_UIMT=user_movimenti_fiscalizzati.ID_UIMT ";
	$sql.= "JOIN user_impianti_destinatari ON user_impianti_destinatari.ID_UIMD=user_movimenti_fiscalizzati.ID_UIMD ";
	$sql.= "WHERE ID_RIF IN (".str_replace("|",",",$rifiuto['ID_RIF']).") AND TIPO='C' AND trasportatore='1' AND ";
	$sql.= "(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') AND ";
	$sql.= "( FKEcfiscT<>FKEcfiscD OR TRIM(CONCAT(user_impianti_destinatari.description, user_impianti_destinatari.ID_COM))<>TRIM(CONCAT(user_impianti_trasportatori.description, user_impianti_trasportatori.ID_COM))) ";
	$FEDIT->SdbRead($sql,"DbRecordsetBA_4",true,false);
	$QTA = explode("|",formatQuantita($FEDIT->DbRecordsetBA_4[0]['TRASPORTATO']));
	$BARecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
	## Unit� di misura
	$BARecord.= $QTA[1].";";
	## Rifiuto conferito a trasportatori conto terzi
	if($MUD_PS_DEST=='1')
		$sql = "SELECT ROUND(SUM(IF(PS_DESTINO>0, PS_DESTINO, pesoN)),3) AS CONFERITO_T3 ";
	else
		$sql = "SELECT ROUND(SUM(IF(PS_DESTINO>0 AND VER_DESTINO=1, PS_DESTINO, pesoN)),3) AS CONFERITO_T3 ";
	$sql.= "FROM user_movimenti_fiscalizzati ";
	$sql.= "WHERE ID_RIF IN (".str_replace("|",",",$rifiuto['ID_RIF']).") AND TIPO='S' AND produttore='1' AND PerRiclassificazione=0 AND DEST_ACC_RIFIUTO<>3 AND ";
	$sql.= "FKEcfiscT<>FKEcfiscD AND FKEcfiscT<>FKEcfiscP AND user_movimenti_fiscalizzati.ID_AZT<>0000000 AND";
	$sql.= "(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31')";
	$FEDIT->SdbRead($sql,"DbRecordsetBA_5",true,false);
	## Numero di moduli TE allegati
	if($FEDIT->DbRecordsetBA_5[0]['CONFERITO_T3']>0){
		$ModuliTE = ModuliTE($rifiuto, $r);
		$BARecord.= zeroFiller(count($ModuliTE),5).";";
		}
	else
		$BARecord.= "00000;";
	## Rifiuto consegnato a terzi per recupero/smaltimento - totale
	if($MUD_PS_DEST=='1')
		$sql = "SELECT ROUND(SUM(IF(PS_DESTINO>0, PS_DESTINO, pesoN)),3) AS CONFERITO ";
	else
		$sql = "SELECT ROUND(SUM(IF(PS_DESTINO>0 AND VER_DESTINO=1, PS_DESTINO, pesoN)),3) AS CONFERITO ";
	$sql.= "FROM user_movimenti_fiscalizzati ";
	$sql.= "JOIN user_impianti_produttori ON user_movimenti_fiscalizzati.ID_UIMP=user_impianti_produttori.ID_UIMP ";
	$sql.= "JOIN user_impianti_destinatari ON user_movimenti_fiscalizzati.ID_UIMD=user_impianti_destinatari.ID_UIMD ";
	$sql.= "WHERE ID_RIF IN (".str_replace("|",",",$rifiuto['ID_RIF']).") AND PerRiclassificazione=0 AND DEST_ACC_RIFIUTO<>3 AND ((produttore='1' AND TIPO='S') OR (trasportatore=1 AND TIPO='C' AND FKEcfiscP<>'".getCodiceFiscale()."')) AND ";
	$sql.= "user_movimenti_fiscalizzati.ID_AZD<>0000000 AND";
	$sql.= "(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') AND ";
	$sql.= "( FKEcfiscP<>FKEcfiscD OR TRIM(CONCAT(user_impianti_produttori.description, user_impianti_produttori.ID_COM))<>TRIM(CONCAT(user_impianti_destinatari.description, user_impianti_destinatari.ID_COM))) ";
	$FEDIT->SdbRead($sql,"DbRecordsetBA_6",true,false);
	$QTA = explode("|",formatQuantita($FEDIT->DbRecordsetBA_6[0]['CONFERITO']));
	$BARecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
	## Unit� di misura
	$BARecord.= $QTA[1].";";
	## Numero di moduli DR allegati
	if($FEDIT->DbRecordsetBA_6[0]['CONFERITO']>0){
		$ModuliDR = ModuliDR($rifiuto, $r);
		$BARecord.= zeroFiller(count($ModuliDR),5).";";
		}
	else
		$BARecord.= "00000;";

	## Rifiuto in giacenza presso il produttore al 31/12 ("da avviare a recupero")
	$sql = "SELECT ROUND(SUM(FKEdisponibilita),3) AS G3112 ";
	$sql.= "FROM user_schede_rifiuti ";
	$sql.= "WHERE ID_RIF IN (".str_replace("|",",",$rifiuto['ID_RIF']).") AND produttore=1 ";
	$FEDIT->SdbRead($sql,"DbRecordsetBA_G",true,false);
	$G3112 = $FEDIT->DbRecordsetBA_G[0]['G3112'];
	$QTA = explode("|",formatQuantita($G3112));
	$BARecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
	## Unit� di misura
	$BARecord.= $QTA[1].";";

	## Rifiuto in giacenza presso il produttore al 31/12 ("da avviare a smaltimento")
	$BARecord.= zeroFiller(number_format(0,3,",",""),11).";";
	$BARecord.= "0;";



	## Rifiuto avviato a recupero
	if($MUD_PS_DEST=='1')
		$sql = "SELECT ROUND(SUM(IF(PS_DESTINO>0, PS_DESTINO, pesoN)),3) AS CONFERITO_R ";
	else
		$sql = "SELECT ROUND(SUM(IF(PS_DESTINO>0 AND VER_DESTINO=1, PS_DESTINO, pesoN)),3) AS CONFERITO_R ";
	$sql.= "FROM user_movimenti_fiscalizzati ";
	$sql.= "JOIN lov_operazioni_rs ON lov_operazioni_rs.ID_OP_RS=user_movimenti_fiscalizzati.ID_OP_RS ";
	$sql.= "WHERE ID_RIF IN (".str_replace("|",",",$rifiuto['ID_RIF']).") AND TIPO='C' AND destinatario='1' AND ";
	$sql.= "(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') AND ";
	$sql.= "SUBSTRING(lov_operazioni_rs.description, 1, 1)='R' ";
	$FEDIT->SdbRead($sql,"DbRecordsetBA_7",true,false);
	## Quantit� complessiva avviata a recupero
	$QTA = explode("|",formatQuantita($FEDIT->DbRecordsetBA_7[0]['CONFERITO_R']));
	$BARecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
	## Unit� di misura
	$BARecord.= $QTA[1].";";
	## Rifiuto avviato a smaltimento
	if($MUD_PS_DEST=='1')
		$sql = "SELECT ROUND(SUM(IF(PS_DESTINO>0, PS_DESTINO, pesoN)),3) AS CONFERITO_S ";
	else
		$sql = "SELECT ROUND(SUM(IF(PS_DESTINO>0 AND VER_DESTINO=1, PS_DESTINO, pesoN)),3) AS CONFERITO_S ";
	$sql.= "FROM user_movimenti_fiscalizzati ";
	$sql.= "JOIN lov_operazioni_rs ON lov_operazioni_rs.ID_OP_RS=user_movimenti_fiscalizzati.ID_OP_RS ";
	$sql.= "WHERE ID_RIF IN (".str_replace("|",",",$rifiuto['ID_RIF']).") AND TIPO='C' AND destinatario='1' AND ";
	$sql.= "(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') AND ";
	$sql.= "SUBSTRING(lov_operazioni_rs.description, 1, 1)='D' ";
	$FEDIT->SdbRead($sql,"DbRecordsetBA_8",true,false);
	## Quantit� complessiva avviata a recupero
	$QTA = explode("|",formatQuantita($FEDIT->DbRecordsetBA_8[0]['CONFERITO_S']));
	$BARecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
	## Unit� di misura
	$BARecord.= $QTA[1].";";

	## BA
        if( $STIMATO>0
                || $G3112>$GINI
                || $FEDIT->DbRecordsetBA_2[0]['RICEVUTO']>0
                || $FEDIT->DbRecordsetBA_3[0]['PRODOTTO_OUT']>0
                || $FEDIT->DbRecordsetBA_5[0]['CONFERITO_T3']>0
                || $FEDIT->DbRecordsetBA_6[0]['CONFERITO']>0
                ){
            WriteMUD($BARecord, $file, 'bottom');

            ## RT
            if($FEDIT->DbRecordsetBA_2[0]['RICEVUTO']>0){
                    for($rt=0;$rt<count($ModuliRT);$rt++){
                            WriteMUD($ModuliRT[$rt], $file, 'bottom');
                            }
                    }

            ## RE
            if($FEDIT->DbRecordsetBA_3[0]['PRODOTTO_OUT']>0){
                    for($re=0;$re<count($ModuliRE);$re++){
                            WriteMUD($ModuliRE[$re], $file, 'bottom');
                            }
                    }

            ## TE
            if($FEDIT->DbRecordsetBA_5[0]['CONFERITO_T3']>0){
                    for($te=0;$te<count($ModuliTE);$te++){
                            WriteMUD($ModuliTE[$te], $file, 'bottom');
                            }
                    }

            ## DR
            if($FEDIT->DbRecordsetBA_6[0]['CONFERITO']>0){
                    for($dr=0;$dr<count($ModuliDR);$dr++){
                            WriteMUD($ModuliDR[$dr], $file, 'bottom');
                            }
                    }
            }

	}



function ABrecord($file){

	global $SOGER;
	global $FEDIT;
	$CODICE_UL					= getCodiceUL();
	$CODICE_FISCALE				= getCodiceFiscale();
	$MUD_PS_DEST				= getMudPsDest();
	$ANNO_DICHIARAZIONE			= getAnnoDichiarazione();
	$FEDIT->DbServerData["db"]	= getDatabaseName();

	## Costante tipo record
	$ABRecord = "AB;";
	## Anno di riferimento della dichiarazione (AAAA)
	$ABRecord.= $ANNO_DICHIARAZIONE.";";
	## codice fiscale identificativo
	$ABRecord.= spaceFiller(strtoupper($CODICE_FISCALE),16).";";
	## codice di identificazione univoca dell'unit� locale
	$ABRecord.= spaceFiller(strtoupper($CODICE_UL),15).";";

	$RecordType = array('AA'=>0, 'AB'=>0, 'BA'=>0, 'BB'=>0, 'BC'=>0, 'BD'=>0, 'BE'=>0, 'DA'=>0, 'DB'=>0, 'RA'=>0, 'RB'=>0, 'RC'=>0, 'RD'=>0, 'RE'=>0, 'RF'=>0, 'VC'=>0, 'VD'=>0, 'VE'=>0, 'VF'=>0, 'VG'=>0, 'VH'=>0, 'IA'=>0, 'IB'=>0, 'IC'=>0, 'ID'=>0, 'IE'=>0, 'IF'=>0, 'MA'=>0, 'AU'=>0, 'AR'=>0);
	$fp			= fopen($file, 'r');
	while (!feof($fp)){
		$line	= fgets($fp);
		$line	= trim($line);
		if($line!=''){
			$exp	= explode(";", $line);
			$RecordType[$exp[0]]++;
			$lines[]=$line;
			}
		}
	fclose($fp);
	## numero di schede rif
	$ABRecord.= zeroFiller($RecordType['BA'],6).";";

	# Sezione intermediazione, numero di schede INT
	$ABRecord.= zeroFiller($RecordType['DA'],6).";";

	# Sezione veicoli - Compilata Scheda AUT?
	$ABRecord.="0;";

	# Sezione veicoli - Compilata Scheda ROT?
	$ABRecord.="0;";

	# Sezione veicoli - Compilata Scheda FRA?
	$ABRecord.="0;";

	# Sezione RAEE - Numero di schede TRA-RAEE
	$ABRecord.="00;";

	# Sezione RAEE - Numero di schede CR-RAEE
	$ABRecord.="00;";

	# Sezione imballaggi - Compilata Scheda IMB?
	$ABRecord.= ($RecordType['DA']>0 ? "1;" : "0;");

    # Sezione autorizzazioni - Numero di schede AUT
    $ABRecord.="00;";

	# Sezione riciclaggio - Numero di schede RIC
    $ABRecord.="00;";

	# FILLER-01 - Lasciato per retrocompatibiit�
        for($f=0; $f<181; $f++)
            $ABRecord.=" ";
        $ABRecord.=";";

	WriteMUD($ABRecord, $file, 'top');

	}



function AArecord($file){

	global $SOGER;
	global $FEDIT;
	$CODICE_UL					= getCodiceUL();
	$CODICE_FISCALE				= getCodiceFiscale();
	$MUD_PS_DEST				= getMudPsDest();
	$ANNO_DICHIARAZIONE			= getAnnoDichiarazione();
	$FEDIT->DbServerData["db"]	= getDatabaseName();

	## Costante tipo record
	$AARecord = "AA;";
	## Anno di riferimento della dichiarazione (AAAA)
	$AARecord.= $ANNO_DICHIARAZIONE.";";

	## leggo dati unit� locale - core_impianti
	$sql ="SELECT ATECO_07, TRIM(REA) AS REA, TRIM(TotAddettiUL) AS TotAddettiUL, TRIM(core_impianti.description) as RagioneSociale, lov_comuni_istat.cod_prov as istatP, lov_comuni_istat.cod_comISTAT as istatC, TRIM(indirizzo) as via, lov_comuni_istat.CAP as cap, TRIM(LR_nome) AS LR_nome, TRIM(LR_cognome) AS LR_cognome ";
	$sql.="FROM core_impianti ";
	$sql.="JOIN lov_comuni_istat ON core_impianti.ID_COM=lov_comuni_istat.ID_COM ";
	$sql.="LEFT JOIN lov_ateco2007 ON core_impianti.ID_ATECO_07=lov_ateco2007.ID_ATECO_07 ";
	$sql.="WHERE core_impianti.ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."'";
	$FEDIT->SdbRead($sql,"DbRecordsetAA1",true,false);

	## info che user� di seguito
	$LR_cognome		= spaceFiller(strtoupper($FEDIT->DbRecordsetAA1[0]["LR_cognome"]),25);
	$LR_nome		= spaceFiller(strtoupper($FEDIT->DbRecordsetAA1[0]["LR_nome"]),25);

	## codice fiscale identificativo
	$AARecord.= spaceFiller(strtoupper($CODICE_FISCALE),16).";";
	## codice di identificazione univoca dell'unit� locale
	$AARecord.= spaceFiller(strtoupper($CODICE_UL),15).";";
	## codice istat attivit� svolta ( senza punti e lettere )
	if(is_null($FEDIT->DbRecordsetAA1[0]["ATECO_07"]))
		$CodiceAttivitaATECO = "      ";
	else{
		$CodiceAttivitaATECO = str_replace(".", "", $FEDIT->DbRecordsetAA1[0]["ATECO_07"]);
		}
	$AARecord.= spaceFiller($CodiceAttivitaATECO,6).";";
	## N� iscrizione Rep.Notizie Econ.Amm. (REA)
	$AARecord.= zeroFiller($FEDIT->DbRecordsetAA1[0]["REA"],9).";";
	## Totale addetti nell' unit� locale
	$AARecord.= zeroFiller($FEDIT->DbRecordsetAA1[0]["TotAddettiUL"],5).";";
	## Descrizione della ragione sociale
	$AARecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetAA1[0]["RagioneSociale"]),60).";";
	## ISTAT Provincia dell' unit� locale
	$AARecord.= zeroFiller($FEDIT->DbRecordsetAA1[0]["istatP"],3).";";
	## ISTAT Comune dell' unit� locale
	$AARecord.= zeroFiller($FEDIT->DbRecordsetAA1[0]["istatC"],3).";";
	## via dell'unit� locale
	$AARecord.= spaceFiller($FEDIT->DbRecordsetAA1[0]["via"],30).";";
	## civico unit� locale
	$AARecord.= "      ;";
	## cap unit� locale
	$AARecord.= zeroFiller($FEDIT->DbRecordsetAA1[0]["cap"],5).";";
	## prefisso telefonico unit� locale
	$AARecord.= "     ;";
	## numero telefonico unit� locale
	$AARecord.= "          ;";

	## leggo dati sede legale - core_gruppi
	$sql ="SELECT lov_comuni_istat.cod_prov AS istatP, lov_comuni_istat.cod_comISTAT AS istatC, TRIM(core_gruppi.indirizzo) AS via, lov_comuni_istat.CAP as cap ";
	$sql.="FROM core_gruppi ";
	$sql.="JOIN lov_comuni_istat ON core_gruppi.ID_COM=lov_comuni_istat.ID_COM ";
	$sql.="WHERE core_gruppi.ID_GRP='".$SOGER->UserData["core_gruppiID_GRP"]."'";
	$FEDIT->SdbRead($sql,"DbRecordsetAA2",true,false);

	## ISTAT provincia sede legale
	$AARecord.= zeroFiller($FEDIT->DbRecordsetAA2[0]["istatP"],3).";";
	## ISTAT comune sede legale
	$AARecord.= zeroFiller($FEDIT->DbRecordsetAA2[0]["istatC"],3).";";
	## via sede legale
	$AARecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetAA2[0]["via"]),30).";";
	## civico sede legale
	$AARecord.= "      ;";
	## cap sede legale
	$AARecord.= zeroFiller($FEDIT->DbRecordsetAA2[0]["cap"],5).";";
	## prefisso sede legale
	$AARecord.= "     ;";
	## numero sede legale
	$AARecord.= "          ;";

	## cognome legale rappresentante
	$AARecord.= $LR_cognome.";";
	## nome legale rappresentante
	$AARecord.= $LR_nome.";";
	## data compilazione
	$AARecord.= date('Ymd').";";
	## mesi di attivit� nell'anno
	$AARecord.= "12;";
	## Annulla e sostituisce
	$AARecord.= "0;";
	## Data della dichiarazione sostituita
	$AARecord.= "00000000;";

	WriteMUD($AARecord, $file, 'top');
	}


function XXrecord($file){

	global $SOGER;
	global $FEDIT;
	$CODICE_UL					= getCodiceUL();
	$CODICE_FISCALE				= getCodiceFiscale();
	$MUD_PS_DEST				= getMudPsDest();
	$ANNO_DICHIARAZIONE			= getAnnoDichiarazione();
	$FEDIT->DbServerData["db"]	= getDatabaseName();

	## Costante tipo record
	$XXRecord = "XX;";
	## Costante release file
	$XXRecord.= "6.04/21;";
	## Tipo di file (costante=10)
	$XXRecord.= "01;";
	## Data di creazione in formato "AAAAMMGG"
	$XXRecord.= date("Ymd").";";
	## Ora di creazione in formato "HHMMSS"
	$XXRecord.= date("His").";";

	$RecordType = array('AA'=>0, 'AB'=>0, 'BA'=>0, 'BB'=>0, 'BC'=>0, 'BD'=>0, 'BE'=>0, 'DA'=>0, 'DB'=>0, 'RA'=>0, 'RB'=>0, 'RC'=>0, 'RD'=>0, 'RE'=>0, 'RF'=>0, 'VC'=>0, 'VD'=>0, 'VE'=>0, 'VF'=>0, 'VG'=>0, 'VH'=>0, 'IA'=>0, 'IB'=>0, 'IC'=>0, 'ID'=>0, 'IE'=>0, 'IF'=>0, 'MA'=>0, 'AU'=>0, 'AR'=>0);
	$fp			= fopen($file, 'r');
	$lines		= array();
	while (!feof($fp)){
		$line	= fgets($fp);
		$line	= trim($line);
		if($line!=''){
			$exp	= explode(";", $line);
			$RecordType[$exp[0]]++;
			$lines[]=$line;
			}
		}
	fclose($fp);
	## Numero totale dei record estratti
	$XXRecord.= zeroFiller(count($lines),8).";";
	## Numero di record per tipo
	foreach($RecordType as $key => $value) {
		$XXRecord.= zeroFiller($value,5).";";
		}
	
	## intestatario MUD - info da core_impianti
	$sql = "SELECT codfisc, telefono, TRIM(core_impianti.description) AS rag_soc, TRIM(indirizzo) AS indirizzo, lov_comuni_istat.CAP, lov_comuni_istat.description AS comune, lov_comuni_istat.shdes_prov AS provincia ";
	$sql.= "FROM core_impianti ";
	$sql.= "JOIN lov_comuni_istat ON core_impianti.ID_COM=lov_comuni_istat.ID_COM ";
	$sql.= "WHERE core_impianti.ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."'";

	$FEDIT->SdbRead($sql,"DbRecordsetMUD",true,false);

	## integro record XX
	$XXRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetMUD[0]["codfisc"]),16).";";		# codice fiscale
	$XXRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetMUD[0]["rag_soc"]),60).";";		# ragione sociale
	$XXRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetMUD[0]["indirizzo"]),30).";";		# via
	$XXRecord.= "          ;";																# civico
	$XXRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetMUD[0]["CAP"]),5).";";			# cap
	$XXRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetMUD[0]["comune"]),30).";";		# citt�
	$XXRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetMUD[0]["provincia"]),2).";";		# sigla provincia
	$XXRecord.= "     ;";																	# prefisso
	$XXRecord.= "          ;";																# telefono
	$XXRecord.= "                                                            ;";			# email
	$XXRecord.= "                              ;";											# riservato (?)

	WriteMUD($XXRecord, $file, 'top');

	}


function ModuliRT($RIF, $r){

	global $SOGER;
	global $FEDIT;
	$CODICE_UL					= getCodiceUL();
	$CODICE_FISCALE				= getCodiceFiscale();
	$MUD_PS_DEST				= getMudPsDest();
	$ANNO_DICHIARAZIONE			= getAnnoDichiarazione();
	$FEDIT->DbServerData["db"]	= getDatabaseName();

	$ModuliRT = array();

	$sql ="SELECT DISTINCT user_movimenti_fiscalizzati.ID_UIMP, TRIM(user_aziende_produttori.description) as azienda, user_aziende_produttori.codfisc, ";
	$sql.="TRIM(user_impianti_produttori.description) as indirizzo, lov_comuni_istat.cod_prov, lov_comuni_istat.cod_comISTAT as cod_com, ";
	$sql.="lov_comuni_istat.CAP, lov_comuni_istat.nazione, CONCAT( TRIM(user_aziende_produttori.description), TRIM(user_impianti_produttori.description) ) AS azienda_indirizzo, ";
	if($MUD_PS_DEST=='1')
		$sql.="SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS RICEVUTO ";
	else
		$sql.="SUM(IF( PS_DESTINO >0 AND VER_DESTINO=1, PS_DESTINO, pesoN )) AS RICEVUTO ";
	$sql.="FROM user_movimenti_fiscalizzati ";
	$sql.="JOIN user_impianti_produttori ON user_movimenti_fiscalizzati.ID_UIMP=user_impianti_produttori.ID_UIMP ";
	$sql.="JOIN user_impianti_destinatari ON user_movimenti_fiscalizzati.ID_UIMD=user_impianti_destinatari.ID_UIMD ";
	$sql.="JOIN user_aziende_produttori ON user_impianti_produttori.ID_AZP=user_aziende_produttori.ID_AZP ";
	$sql.="JOIN lov_comuni_istat ON user_impianti_produttori.ID_COM=lov_comuni_istat.ID_COM ";
	$sql.="WHERE ID_RIF IN (".str_replace("|",",",$RIF['ID_RIF']).") ";
	$sql.="AND TIPO='C' ";
	$sql.="AND (user_movimenti_fiscalizzati.trasportatore='1' OR ";
	$REGISTRO = getRegistriSoger();
	$sql.= ($REGISTRO['T']=='1' ? "(user_movimenti_fiscalizzati.destinatario=1 AND FKEcfiscT<>'".getCodiceFiscale()."')" : "user_movimenti_fiscalizzati.destinatario=1");
	$sql.=") ";
	$sql.="AND ( FKEcfiscP<>FKEcfiscD OR TRIM(CONCAT(user_impianti_produttori.description, user_impianti_produttori.ID_COM))<>TRIM(CONCAT(user_impianti_destinatari.description, user_impianti_destinatari.ID_COM))) ";
	$sql.="AND (DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') ";

	$sql.="GROUP BY azienda_indirizzo ";
	$FEDIT->SdbRead($sql,"DbRecordsetBB",true,false);
        if($FEDIT->DbRecsNum>0) {
            for($RT=0;$RT<count($FEDIT->DbRecordsetBB);$RT++){
                    ## Costante tipo record
                    $BBRecord = "BB;";
                    ## Anno di riferimento della dichiarazione (AAAA)
                    $BBRecord.= $ANNO_DICHIARAZIONE.";";
                    ## Codice fiscale identificativo del dichiarante
                    $BBRecord.= spaceFiller(strtoupper($CODICE_FISCALE),16).";";
                    ## codice di identificazione univoca dell'unit� locale
                    $BBRecord.= spaceFiller(strtoupper($CODICE_UL),15).";";
                    ## numero d'ordine progressivo di scheda rif
                    $BBRecord.= zeroFiller($r,4).";";
                    ## codice del rifiuto (CER)
                    $BBRecord.= $RIF['COD_CER'].";";
                    ## Tipo di allegato
                    $BBRecord.= "RT;";
                    ## Numero progressivo dell'allegato
                    $BBRecord.= zeroFiller(($RT+1),5).";";
                    ## Codice fiscale del soggetto che ha conferito
                    $BBRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetBB[$RT]['codfisc']),16).";";
                    ## Nome o ragione sociale
                    $BBRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetBB[$RT]['azienda']),60).";";
                    # codice istat provincia
                    $BBRecord.= zeroFiller($FEDIT->DbRecordsetBB[$RT]['cod_prov'],3).";";
                    # codice istat comune
                    $BBRecord.= zeroFiller($FEDIT->DbRecordsetBB[$RT]['cod_com'],3).";";
                    # via
                    $BBRecord.= spaceFiller($FEDIT->DbRecordsetBB[$RT]['indirizzo'],30).";";
                    # civico
                    $BBRecord.= "      ;";
                    # cap
                    $BBRecord.= spaceFiller($FEDIT->DbRecordsetBB[$RT]['CAP'],5).";";
                    # Quantit� dichiarata
                    $QTA = explode("|",formatQuantita($FEDIT->DbRecordsetBB[$RT]['RICEVUTO']));
                    $BBRecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
                    ## Unit� di misura
                    $BBRecord.= $QTA[1].";";
                    # nome della nazione - se estero
                    if($FEDIT->DbRecordsetBB[$RT]['nazione']!='ITALIA') $NAZIONE = $FEDIT->DbRecordsetBB[$RT]['nazione']; else $NAZIONE='';
                    $BBRecord.=spaceFiller($NAZIONE,20).";";
                    # Codice Regolamento CEE 1013/2006
                    $BBRecord.="      ;";
                    # Ricevuto da Privati ( NON LO SAPPIAMO )
                    $BBRecord.="0;";
                    # Quantit� ad altre operazioni di smaltimento ( NON LO SAPPIAMO )
                    $BBRecord.="           ;";
                    # Unit� di misura
                    $BBRecord.=" ;";
                    # Quantit� a recupero di materia
                    $BBRecord.="           ;";
                    # Unit� di misura
                    $BBRecord.=" ;";
                    # Quantit� a recupero di energia ( NON LO SAPPIAMO )
                    $BBRecord.="           ;";
                    # Unit� di misura
                    $BBRecord.=" ;";
					# Quantit� a incenerimento
                    $BBRecord.="           ;";
                    # Unit� di misura
                    $BBRecord.=" ;";
					# Quantit� avviata in discarica ( NON LO SAPPIAMO )
                    $BBRecord.="           ;";
                    # Unit� di misura
                    $BBRecord.=" ;";
					# Origine urbana ( NON LO SAPPIAMO )
                    $BBRecord.="0;";
					# Pile e accumulatori portatili
					$codiciPile = array('160601', '160602', '160603', '160604', '160605', '200133', '200134');
					if(in_array($RIF['COD_CER'], $codiciPile)){
						$BBRecord.="1;";
					}
					else{
						$BBRecord.="0;";
					}

                    $ModuliRT[]	= $BBRecord;
                    unset($BBRecord);
                    }
        }

	return $ModuliRT;

	}






function ModuliRE($RIF, $r){

	global $SOGER;
	global $FEDIT;
	$CODICE_UL					= getCodiceUL();
	$CODICE_FISCALE				= getCodiceFiscale();
	$MUD_PS_DEST				= getMudPsDest();
	$ANNO_DICHIARAZIONE			= getAnnoDichiarazione();
	$FEDIT->DbServerData["db"]	= getDatabaseName();

	$ModuliRE = array();

	$sql ="SELECT DISTINCT lov_comuni_istat.ID_COM, lov_comuni_istat.cod_prov, lov_comuni_istat.cod_comISTAT as cod_com, ";
	if($MUD_PS_DEST=='1')
		$sql.="SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS PRODOTTO_OUT ";
	else
		$sql.="SUM(IF( PS_DESTINO >0 AND VER_DESTINO=1, PS_DESTINO, pesoN )) AS PRODOTTO_OUT ";
	$sql.="FROM user_movimenti_fiscalizzati ";
	$sql.="JOIN user_impianti_produttori ON user_movimenti_fiscalizzati.ID_UIMP=user_impianti_produttori.ID_UIMP ";
	$sql.="JOIN lov_comuni_istat ON user_impianti_produttori.ID_COM=lov_comuni_istat.ID_COM ";
	$sql.="WHERE ID_RIF IN (".str_replace("|",",",$RIF['ID_RIF']).") AND TIPO='S' AND PerRiclassificazione=0 AND DEST_ACC_RIFIUTO<>3 AND produttore='1' AND FuoriUnitaLocale=1 AND ";
	$sql.="(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') ";
	$sql.="GROUP BY lov_comuni_istat.ID_COM ";
	$sql.="ORDER BY lov_comuni_istat.description ";
	$FEDIT->SdbRead($sql,"DbRecordsetBC",true,false);

        if($FEDIT->DbRecsNum>0) {
            for($RE=0;$RE<count($FEDIT->DbRecordsetBC);$RE++){
                    ## Costante tipo record
                    $BCRecord = "BC;";
                    ## Anno di riferimento della dichiarazione (AAAA)
                    $BCRecord.= $ANNO_DICHIARAZIONE.";";
                    ## Codice fiscale identificativo del dichiarante
                    $BCRecord.= spaceFiller(strtoupper($CODICE_FISCALE),16).";";
                    ## codice di identificazione univoca dell'unit� locale
                    $BCRecord.= spaceFiller(strtoupper($CODICE_UL),15).";";
                    ## numero d'ordine progressivo di scheda rif
                    $BCRecord.= zeroFiller($r,4).";";
                    ## codice del rifiuto (CER)
                    $BCRecord.= $RIF['COD_CER'].";";
                    ## Numero progressivo dell'allegato
                    $BCRecord.= zeroFiller(($RE+1),5).";";
                    ## codice istat provincia
                    $BCRecord.= zeroFiller($FEDIT->DbRecordsetBC[$RE]['cod_prov'],3).";";
                    ## codice istat comune
                    $BCRecord.= zeroFiller($FEDIT->DbRecordsetBC[$RE]['cod_com'],3).";";
                    ## attivit� che ha originato il rifiuto
                    $BCRecord.= "0;";
                    # Quantit� dichiarata
                    $QTA = explode("|",formatQuantita($FEDIT->DbRecordsetBC[$RE]['PRODOTTO_OUT']));
                    $BCRecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
                    ## Unit� di misura
                    $BCRecord.= $QTA[1].";";

                    $ModuliRE[]	= $BCRecord;
                    unset($BCRecord);
                    }
        }

	return $ModuliRE;

	}







function ModuliTE($RIF, $r){

	global $SOGER;
	global $FEDIT;
	$CODICE_UL					= getCodiceUL();
	$CODICE_FISCALE				= getCodiceFiscale();
	$MUD_PS_DEST				= getMudPsDest();
	$ANNO_DICHIARAZIONE			= getAnnoDichiarazione();
	$FEDIT->DbServerData["db"]	= getDatabaseName();

	$ModuliTE = array();

	$sql ="SELECT DISTINCT user_movimenti_fiscalizzati.ID_AZT, TRIM(user_aziende_trasportatori.description) as azienda, ";
	$sql.="user_aziende_trasportatori.codfisc, lov_comuni_istat.nazione, ";
	if($MUD_PS_DEST=='1')
		$sql.="SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS CONFERITO_T3 ";
	else
		$sql.="SUM(IF( PS_DESTINO >0 AND VER_DESTINO=1, PS_DESTINO, pesoN )) AS CONFERITO_T3 ";
	$sql.="FROM user_movimenti_fiscalizzati ";
	$sql.="JOIN user_aziende_trasportatori ON user_aziende_trasportatori.ID_AZT=user_movimenti_fiscalizzati.ID_AZT ";
	$sql.="JOIN lov_comuni_istat ON user_aziende_trasportatori.ID_COM=lov_comuni_istat.ID_COM ";
	$sql.="WHERE ID_RIF IN (".str_replace("|",",",$RIF['ID_RIF']).") AND TIPO='S' AND PerRiclassificazione=0 AND DEST_ACC_RIFIUTO<>3 AND user_movimenti_fiscalizzati.produttore='1' AND ";
	$sql.="FKEcfiscT<>FKEcfiscD AND user_movimenti_fiscalizzati.ID_AZT<>0000000 AND ";
	$sql.="(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') ";
	$sql.="GROUP BY user_movimenti_fiscalizzati.ID_AZT ";
	$sql.="HAVING CONFERITO_T3>0 ";
	$FEDIT->SdbRead($sql,"DbRecordsetBB",true,false);

        if($FEDIT->DbRecsNum>0) {
            for($TE=0;$TE<count($FEDIT->DbRecordsetBB);$TE++){
                    ## Costante tipo record
                    $BBRecord = "BB;";
                    ## Anno di riferimento della dichiarazione (AAAA)
                    $BBRecord.= $ANNO_DICHIARAZIONE.";";
                    ## Codice fiscale identificativo del dichiarante
                    $BBRecord.= spaceFiller(strtoupper($CODICE_FISCALE),16).";";
                    ## codice di identificazione univoca dell'unit� locale
                    $BBRecord.= spaceFiller(strtoupper($CODICE_UL),15).";";
                    ## numero d'ordine progressivo di scheda rif
                    $BBRecord.= zeroFiller($r,4).";";
                    ## codice del rifiuto (CER)
                    $BBRecord.= $RIF['COD_CER'].";";
                    ## Tipo di allegato
                    $BBRecord.= "TE;";
                    ## Numero progressivo dell'allegato
                    $BBRecord.= zeroFiller(($TE+1),5).";";
                    ## Codice fiscale del soggetto che ha conferito
                    $BBRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetBB[$TE]['codfisc']),16).";";
                    ## Nome o ragione sociale
                    $BBRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetBB[$TE]['azienda']),60).";";
                    # codice istat provincia
                    $BBRecord.= "   ;";
                    # codice istat comune
                    $BBRecord.= "   ;";
                    # via
                    $BBRecord.= "                              ;";
                    # civico
                    $BBRecord.= "      ;";
                    # cap
                    $BBRecord.= "     ;";
                    # Quantit� dichiarata
                    $BBRecord.= "           ;";
                    ## Unit� di misura
                    $BBRecord.= " ;";
                    # nome della nazione - se estero
                    if($FEDIT->DbRecordsetBB[$TE]['nazione']!='ITALIA') $NAZIONE = $FEDIT->DbRecordsetBB[$TE]['nazione']; else $NAZIONE='';
                    $BBRecord.=spaceFiller($NAZIONE,20).";";
                    # Codice Regolamento CEE 1013/2006
                    $BBRecord.="      ;";
                    # Ricevuto da Privati ( NON LO SAPPIAMO )
                    $BBRecord.=" ;";
                    # Quantit� ad altre operazioni di smaltimento ( NON LO SAPPIAMO )
                    $BBRecord.="           ;";
                    # Unit� di misura
                    $BBRecord.=" ;";
                    # Quantit� a recupero di materia ( NON LO SAPPIAMO )
                    $BBRecord.="           ;";
                    # Unit� di misura
                    $BBRecord.=" ;";
                    # Quantit� a recupero di energia ( NON LO SAPPIAMO )
                    $BBRecord.="           ;";
                    # Unit� di misura
                    $BBRecord.=" ;";
					# Quantit� a incenerimento ( NON LO SAPPIAMO )
                    $BBRecord.="           ;";
                    # Unit� di misura
                    $BBRecord.=" ;";
					# Quantit� avviata in discarica ( NON LO SAPPIAMO )
                    $BBRecord.="           ;";
                    # Unit� di misura
                    $BBRecord.=" ;";
					# Origine urbana ( NON LO SAPPIAMO )
                    $BBRecord.="0;";
					# Pile e accumulatori portatili
					$BBRecord.="0;";

                    $ModuliTE[]	= $BBRecord;
                    unset($BBRecord);
                    }
        }

	return $ModuliTE;

	}




function ModuliDR($RIF, $r){

	global $SOGER;
	global $FEDIT;
	$CODICE_UL					= getCodiceUL();
	$CODICE_FISCALE				= getCodiceFiscale();
	$MUD_PS_DEST				= getMudPsDest();
	$ANNO_DICHIARAZIONE			= getAnnoDichiarazione();
	$FEDIT->DbServerData["db"]	= getDatabaseName();

	$ModuliDR = array();

	$sql ="SELECT DISTINCT user_movimenti_fiscalizzati.ID_UIMD, TRIM(user_aziende_destinatari.description) as azienda, ";
	$sql.="user_aziende_destinatari.codfisc, TRIM(user_impianti_destinatari.description) as indirizzo, lov_comuni_istat.cod_prov, ";
	$sql.="lov_comuni_istat.cod_comISTAT as cod_com, lov_comuni_istat.CAP, lov_comuni_istat.nazione,";
	if($MUD_PS_DEST=='1')
		$sql.="SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS CONFERITO ";
	else
		$sql.="SUM(IF( PS_DESTINO >0 AND VER_DESTINO=1, PS_DESTINO, pesoN )) AS CONFERITO ";
	$sql.="FROM user_movimenti_fiscalizzati ";
	$sql.="JOIN user_aziende_destinatari ON user_aziende_destinatari.ID_AZD=user_movimenti_fiscalizzati.ID_AZD ";
	$sql.="JOIN user_impianti_destinatari ON user_impianti_destinatari.ID_UIMD=user_movimenti_fiscalizzati.ID_UIMD ";
	$sql.="JOIN user_impianti_produttori ON user_movimenti_fiscalizzati.ID_UIMP=user_impianti_produttori.ID_UIMP ";
	$sql.="JOIN lov_comuni_istat ON user_impianti_destinatari.ID_COM=lov_comuni_istat.ID_COM ";
	$sql.="WHERE ID_RIF IN (".str_replace("|",",",$RIF['ID_RIF']).") AND PerRiclassificazione=0 AND DEST_ACC_RIFIUTO<>3 AND ((user_movimenti_fiscalizzati.produttore='1' AND TIPO='S') OR (user_movimenti_fiscalizzati.trasportatore=1 AND TIPO='C' AND FKEcfiscP<>'".getCodiceFiscale()."')) AND ";
	$sql.="user_movimenti_fiscalizzati.ID_AZD<>0000000 AND ";
	$sql.="(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') AND ";
	$sql.="( FKEcfiscP<>FKEcfiscD OR TRIM(CONCAT(user_impianti_produttori.description, user_impianti_produttori.ID_COM))<>TRIM(CONCAT(user_impianti_destinatari.description, user_impianti_destinatari.ID_COM))) ";
	$sql.="GROUP BY user_movimenti_fiscalizzati.ID_UIMD ";
	$sql.="HAVING CONFERITO>0 ";
	$FEDIT->SdbRead($sql,"DbRecordsetBB",true,false);

        if($FEDIT->DbRecsNum>0) {
            for($DR=0;$DR<count($FEDIT->DbRecordsetBB);$DR++){
                    ## Costante tipo record
                    $BBRecord = "BB;";
                    ## Anno di riferimento della dichiarazione (AAAA)
                    $BBRecord.= $ANNO_DICHIARAZIONE.";";
                    ## Codice fiscale identificativo del dichiarante
                    $BBRecord.= spaceFiller(strtoupper($CODICE_FISCALE),16).";";
                    ## codice di identificazione univoca dell'unit� locale
                    $BBRecord.= spaceFiller(strtoupper($CODICE_UL),15).";";
                    ## numero d'ordine progressivo di scheda rif
                    $BBRecord.= zeroFiller($r,4).";";
                    ## codice del rifiuto (CER)
                    $BBRecord.= $RIF['COD_CER'].";";
                    ## Tipo di allegato
                    $BBRecord.= "DR;";
                    ## Numero progressivo dell'allegato
                    $BBRecord.= zeroFiller(($DR+1),5).";";
                    ## Codice fiscale del soggetto che ha conferito
                    $BBRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetBB[$DR]['codfisc']),16).";";
                    ## Nome o ragione sociale
                    $BBRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetBB[$DR]['azienda']),60).";";
                    # codice istat provincia
                    $BBRecord.= zeroFiller($FEDIT->DbRecordsetBB[$DR]['cod_prov'],3).";";
                    # codice istat comune
                    $BBRecord.= zeroFiller($FEDIT->DbRecordsetBB[$DR]['cod_com'],3).";";
                    # via
                    $BBRecord.= spaceFiller($FEDIT->DbRecordsetBB[$DR]['indirizzo'],30).";";
                    # civico
                    $BBRecord.= "      ;";
                    # cap
                    $BBRecord.= spaceFiller($FEDIT->DbRecordsetBB[$DR]['CAP'],5).";";
                    # Quantit� dichiarata
                    $QTA = explode("|",formatQuantita($FEDIT->DbRecordsetBB[$DR]['CONFERITO']));
                    $BBRecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
                    ## Unit� di misura
                    $BBRecord.= $QTA[1].";";
                    # nome della nazione - se estero
                    if($FEDIT->DbRecordsetBB[$DR]['nazione']!='ITALIA') $NAZIONE = $FEDIT->DbRecordsetBB[$DR]['nazione']; else $NAZIONE='';
                    $BBRecord.=spaceFiller($NAZIONE,20).";";
                    # Codice Regolamento CEE 1013/2006
                    $BBRecord.="      ;";
                    # Ricevuto da Privati ( NON LO SAPPIAMO )
                    $BBRecord.=" ;";
                    
                    if($NAZIONE!=''){
							# Quantit� ad altre operazioni di smaltimento ( NON LO SAPPIAMO )
                            $QTA = explode("|",formatQuantita($FEDIT->DbRecordsetBB[$DR]['CONFERITO']));
                            $BBRecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
                            ## Unit� di misura
                            $BBRecord.= $QTA[1].";";
                            # Quantit� a recupero di materia ( NON LO SAPPIAMO )
                            $BBRecord.="0000000,000;";
                            # Unit� di misura
                            $BBRecord.="1;";
                            # Quantit� a recupero di energia ( NON LO SAPPIAMO )
                            $BBRecord.="0000000,000;";
                            # Unit� di misura
                            $BBRecord.="1;";
							# Quantit� a incenerimento ( NON LO SAPPIAMO )
							$BBRecord.="0000000,000;";
							# Unit� di misura
							$BBRecord.="1;";
							# Quantit� avviata in discarica ( NON LO SAPPIAMO )
							$BBRecord.="0000000,000;";
							# Unit� di misura
							$BBRecord.="1;";
                        }
                    else{
							# Quantit� ad altre operazioni di smaltimento ( NON LO SAPPIAMO )
                            $BBRecord.="           ;";
                            ## Unit� di misura
                            $BBRecord.=" ;";
                            # Quantit� a recupero di materia ( NON LO SAPPIAMO )
                            $BBRecord.="           ;";
                            # Unit� di misura
                            $BBRecord.=" ;";
                            # Quantit� a recupero di energia ( NON LO SAPPIAMO )
                            $BBRecord.="           ;";
                            # Unit� di misura
                            $BBRecord.=" ;";
							# Quantit� a incenerimento ( NON LO SAPPIAMO )
							$BBRecord.="           ;";
							# Unit� di misura
							$BBRecord.=" ;";
							# Quantit� avviata in discarica ( NON LO SAPPIAMO )
							$BBRecord.="           ;";
							# Unit� di misura
							$BBRecord.=" ;";							
                        }
					# Origine urbana ( NON LO SAPPIAMO )
					$BBRecord.="0;";
					# Pile e accumulatori portatili
					$BBRecord.="0;";

                    $ModuliDR[]	= $BBRecord;
                    unset($BBRecord);
                    }
        }

	return $ModuliDR;

	}


function ModuliUO($RIF, $r){

	global $SOGER;
	global $FEDIT;
	$CODICE_UL					= getCodiceUL();
	$CODICE_FISCALE				= getCodiceFiscale();
	$MUD_PS_DEST				= getMudPsDest();
	$ANNO_DICHIARAZIONE			= getAnnoDichiarazione();
	$FEDIT->DbServerData["db"]	= getDatabaseName();

	$ModuliUO = array();

	$sql ="SELECT DISTINCT user_movimenti_fiscalizzati.ID_UIMP, TRIM(user_aziende_produttori.description) as azienda, ";
	$sql.="user_aziende_produttori.codfisc, TRIM(user_impianti_produttori.description) as indirizzo, lov_comuni_istat.cod_prov, ";
	$sql.="lov_comuni_istat.cod_comISTAT as cod_com, lov_comuni_istat.CAP, lov_comuni_istat.nazione,";
	$sql.="ROUND(SUM(pesoN),3) AS INTERMEDIATA ";
	$sql.="FROM user_movimenti_fiscalizzati ";
	$sql.="JOIN user_aziende_produttori ON user_aziende_produttori.ID_AZP=user_movimenti_fiscalizzati.ID_AZP ";
	$sql.="JOIN user_impianti_produttori ON user_impianti_produttori.ID_UIMP=user_movimenti_fiscalizzati.ID_UIMP ";
	$sql.="JOIN lov_comuni_istat ON user_impianti_produttori.ID_COM=lov_comuni_istat.ID_COM ";
	$sql.="WHERE ID_RIF IN (".str_replace("|",",",$RIF['ID_RIF']).") AND TIPO='C' AND user_movimenti_fiscalizzati.intermediario='1' AND ";
	$sql.="(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') ";
	$sql.="GROUP BY user_movimenti_fiscalizzati.ID_UIMP ";
	$sql.="HAVING INTERMEDIATA>0 ";
	$FEDIT->SdbRead($sql,"DbRecordsetDB",true,false);

        if($FEDIT->DbRecsNum>0) {
            for($UO=0;$UO<count($FEDIT->DbRecordsetDB);$UO++){
                    ## Costante tipo record
                    $DBRecord = "DB;";
                    ## Anno di riferimento della dichiarazione (AAAA)
                    $DBRecord.= $ANNO_DICHIARAZIONE.";";
                    ## Codice fiscale identificativo del dichiarante
                    $DBRecord.= spaceFiller(strtoupper($CODICE_FISCALE),16).";";
                    ## codice di identificazione univoca dell'unit� locale
                    $DBRecord.= spaceFiller(strtoupper($CODICE_UL),15).";";
                    ## numero d'ordine progressivo di scheda int
                    $DBRecord.= zeroFiller($r,4).";";
                    ## codice del rifiuto (CER)
                    $DBRecord.= $RIF['COD_CER'].";";
                    ## Tipo di allegato
                    $DBRecord.= "UO;";
                    ## Numero progressivo dell'allegato
                    $DBRecord.= zeroFiller(($UO+1),5).";";
                    ## Codice fiscale del'unit� locale di origine
                    $DBRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetDB[$UO]['codfisc']),16).";";
                    ## Descrizione della ragione sociale
                    $DBRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetDB[$UO]['azienda']),60).";";
                    # codice istat provincia
                    $DBRecord.= zeroFiller($FEDIT->DbRecordsetDB[$UO]['cod_prov'],3).";";
                    # codice istat comune
                    $DBRecord.= zeroFiller($FEDIT->DbRecordsetDB[$UO]['cod_com'],3).";";
                    # via
                    $DBRecord.= spaceFiller($FEDIT->DbRecordsetDB[$UO]['indirizzo'],30).";";
                    # civico
                    $DBRecord.= "      ;";
                    # nome della nazione - se estero
                    if($FEDIT->DbRecordsetDB[$UO]['nazione']!='ITALIA') $NAZIONE = $FEDIT->DbRecordsetDB[$UO]['nazione']; else $NAZIONE='';
                    $DBRecord.=spaceFiller($NAZIONE,20).";";
                    # Quantit� acquisita dell'anno
                    $QTA = explode("|",formatQuantita($FEDIT->DbRecordsetDB[$UO]['INTERMEDIATA']));
                    $DBRecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
                    ## Unit� di misura
                    $DBRecord.= $QTA[1].";";

                    $ModuliUO[]	= $DBRecord;
                    unset($DBRecord);
                    }
        }

	return $ModuliUO;

	}




function ModuliUD($RIF, $r){

	global $SOGER;
	global $FEDIT;
	$CODICE_UL					= getCodiceUL();
	$CODICE_FISCALE				= getCodiceFiscale();
	$MUD_PS_DEST				= getMudPsDest();
	$ANNO_DICHIARAZIONE			= getAnnoDichiarazione();
	$FEDIT->DbServerData["db"]	= getDatabaseName();

	$ModuliUD = array();

	$sql ="SELECT DISTINCT user_movimenti_fiscalizzati.ID_UIMD, TRIM(user_aziende_destinatari.description) as azienda, ";
	$sql.="user_aziende_destinatari.codfisc, TRIM(user_impianti_destinatari.description) as indirizzo, lov_comuni_istat.cod_prov, ";
	$sql.="lov_comuni_istat.cod_comISTAT as cod_com, lov_comuni_istat.CAP, lov_comuni_istat.nazione,";
	$sql.="ROUND(SUM(pesoN),3) AS INTERMEDIATA ";
	$sql.="FROM user_movimenti_fiscalizzati ";
	$sql.="JOIN user_aziende_destinatari ON user_aziende_destinatari.ID_AZD=user_movimenti_fiscalizzati.ID_AZD ";
	$sql.="JOIN user_impianti_destinatari ON user_impianti_destinatari.ID_UIMD=user_movimenti_fiscalizzati.ID_UIMD ";
	$sql.="JOIN lov_comuni_istat ON user_impianti_destinatari.ID_COM=lov_comuni_istat.ID_COM ";
	$sql.="WHERE ID_RIF IN (".str_replace("|",",",$RIF['ID_RIF']).") AND TIPO='C' AND user_movimenti_fiscalizzati.intermediario='1' AND ";
	$sql.="(DTMOV>='".$ANNO_DICHIARAZIONE."-01-01' AND DTMOV<='".$ANNO_DICHIARAZIONE."-12-31') ";
	$sql.="GROUP BY user_movimenti_fiscalizzati.ID_UIMD ";
	$sql.="HAVING INTERMEDIATA>0 ";
	$FEDIT->SdbRead($sql,"DbRecordsetDB",true,false);

        if($FEDIT->DbRecsNum>0) {
            for($UD=0;$UD<count($FEDIT->DbRecordsetDB);$UD++){
                    ## Costante tipo record
                    $DBRecord = "DB;";
                    ## Anno di riferimento della dichiarazione (AAAA)
                    $DBRecord.= $ANNO_DICHIARAZIONE.";";
                    ## Codice fiscale identificativo del dichiarante
                    $DBRecord.= spaceFiller(strtoupper($CODICE_FISCALE),16).";";
                    ## codice di identificazione univoca dell'unit� locale
                    $DBRecord.= spaceFiller(strtoupper($CODICE_UL),15).";";
                    ## numero d'ordine progressivo di scheda int
                    $DBRecord.= zeroFiller($r,4).";";
                    ## codice del rifiuto (CER)
                    $DBRecord.= $RIF['COD_CER'].";";
                    ## Tipo di allegato
                    $DBRecord.= "UD;";
                    ## Numero progressivo dell'allegato
                    $DBRecord.= zeroFiller(($UD+1),5).";";
                    ## Codice fiscale del'unit� locale di origine
                    $DBRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetDB[$UD]['codfisc']),16).";";
                    ## Descrizione della ragione sociale
                    $DBRecord.= spaceFiller(strtoupper($FEDIT->DbRecordsetDB[$UD]['azienda']),60).";";
                    # codice istat provincia
                    $DBRecord.= zeroFiller($FEDIT->DbRecordsetDB[$UD]['cod_prov'],3).";";
                    # codice istat comune
                    $DBRecord.= zeroFiller($FEDIT->DbRecordsetDB[$UD]['cod_com'],3).";";
                    # via
                    $DBRecord.= spaceFiller($FEDIT->DbRecordsetDB[$UD]['indirizzo'],30).";";
                    # civico
                    $DBRecord.= "      ;";
                    # nome della nazione - se estero
                    if($FEDIT->DbRecordsetDB[$UD]['nazione']!='ITALIA') $NAZIONE = $FEDIT->DbRecordsetDB[$UD]['nazione']; else $NAZIONE='';
                    $DBRecord.=spaceFiller($NAZIONE,20).";";
                    # Quantit� acquisita dell'anno
                    $QTA = explode("|",formatQuantita($FEDIT->DbRecordsetDB[$UD]['INTERMEDIATA']));
                    $DBRecord.= zeroFiller(number_format($QTA[0],3,",",""),11).";";
                    ## Unit� di misura
                    $DBRecord.= $QTA[1].";";

                    $ModuliUD[]	= $DBRecord;
                    unset($DBRecord);
                    }
        }

	return $ModuliUD;

	}



?>