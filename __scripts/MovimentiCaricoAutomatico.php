<?php
session_start();
global $SOGER;
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
	$RawdataMovScarico = explode("/",$_GET["DTMV"]);
	$dataMovScarico = $RawdataMovScarico[2] . "-" . $RawdataMovScarico[1] . "-" . $RawdataMovScarico[0];  
	$RawdataFormulario = explode("/",$_GET["DTFORM"]);
	$dataFormulario = $RawdataFormulario[2] . "-" . $RawdataFormulario[1] . "-" . $RawdataFormulario[0];  

	$TableName=$_GET['TableName'];

	$sql ="INSERT INTO " .$TableName ." ";
	$sql.="(TIPO,tara,pesoL,NMOV,DTMOV,ID_IMP,ID_RIF,originalID_RIF,lotto,FANGHI,quantita,qta_hidden,pesoN,FKEdisponibilita,FKEumis,FKESF,FKEpesospecifico,ID_AZP,FKEcfiscP,ID_UIMP";

	# se non sono produttore, nel carico automatico riporto anche i dati di trasportatore, destinatario e intermediario..
	# il carico sarebbe l'ingresso nell'impianto del rifiuto con formulario, dovrebbe essere fatto manualmente, ma alcuni (Marchesini) lo registrano come movimento di scarico
	if($SOGER->UserData["workmode"]!="produttore") {
		$sql.=",adr,ID_ONU,QL,DTFORM,NFORM";
		$sql.=",ID_AZD,FKEcfiscD,ID_UIMD,ID_AUTHD,FKErilascioD,FKEscadenzaD,ID_OP_RS";
		$sql.=",ID_AZT,FKEcfiscT,ID_UIMT,ID_AUTHT,FKErilascioT,FKEscadenzaT";
		$sql.=",ID_AZI,FKEcfiscI,ID_UIMI,ID_AUTHI,FKErilascioI,FKEscadenzaI";
		}

	# se il carico automatico lo sto creando per una scheda SISTRI (NMOV=9999999), allora l'NMOV del carico devo ricavarmelo..
	if($_GET['NMOV']=='9999999'){

		## NMOV
		$sqlNMOV = "SELECT MAX(NMOV) AS NMOV FROM ".$TableName." WHERE NMOV<>9999999 AND ID_IMP='".$SOGER->UserData["core_impiantiID_IMP"]."' AND ".$SOGER->UserData['workmode']."=1;";
		$FEDIT->SDBRead($sqlNMOV,"DbRecordSetNMOV",true,false);
		if(isset($FEDIT->DbRecordSetNMOV))
			$NMOV = $FEDIT->DbRecordSetNMOV[0]['NMOV']+1;
		else $NMOV = 1;

		## DTMOV
		$dataMovScarico = date('Y-m-d');
		}
	else $NMOV = $_GET['NMOV'];
	
	//$sql .= ",produttore,trasportatore,destinatario,intermediario,idSIS_regCrono)";
	$sql .= ",produttore,trasportatore,destinatario,intermediario, ID_USR, UniqueString)";
	
	$sql .= " VALUES ('C','0','0','" . $NMOV . "','" . $dataMovScarico . "','" . $SOGER->UserData["core_impiantiID_IMP"] . "','${_GET["ID_RIF"]}','${_GET["originalID_RIF"]}','${_GET["lotto"]}','${_GET["FANGHI"]}','";
		
	$sql .= $_GET["DIFF"] . "','${_GET["DIFF"]}', '${_GET["KG"]}', '${_GET["Dispo"]}', '${_GET["UMIS"]}','${_GET["SF"]}', '${_GET["PS"]}', '${_GET["ID_AZP"]}','${_GET["FKEcfiscP"]}','${_GET["ID_UIMP"]}',";

	if($SOGER->UserData["workmode"]!="produttore") {
		$sql .= "'${_GET["adr"]}','${_GET["ID_ONU"]}','${_GET["QL"]}','" . $dataFormulario . "', '${_GET["NFORM"]}', ";
		$sql .= "'${_GET["ID_AZD"]}','${_GET["FKEcfiscD"]}','${_GET["ID_UIMD"]}','${_GET["ID_AUTHD"]}','${_GET["FKErilascioD"]}','${_GET["FKEscadenzaD"]}','${_GET["ID_OP_RS"]}',";
		$sql .= "'${_GET["ID_AZT"]}','${_GET["FKEcfiscT"]}','${_GET["ID_UIMT"]}','${_GET["ID_AUTHT"]}','${_GET["FKErilascioT"]}','${_GET["FKEscadenzaT"]}',";
		$sql .= "'${_GET["ID_AZI"]}','${_GET["FKEcfiscI"]}','${_GET["ID_UIMI"]}','${_GET["ID_AUTHI"]}','${_GET["FKErilascioI"]}','${_GET["FKEscadenzaI"]}',";
		}

	if($SOGER->UserData["workmode"]=="produttore") {
		$sql .="1,";
	 } else {
		$sql .="0,";
	 }
	 if($SOGER->UserData["workmode"]=="trasportatore") {
		$sql .="1,";
	 } else {
		$sql .="0,";
	 }
	 if($SOGER->UserData["workmode"]=="destinatario") {
		$sql .="1,";
	 } else {
		$sql .="0,";
	 }
	 if($SOGER->UserData["workmode"]=="intermediario") {
		$sql .="1";
	 } else {
		$sql .="0";
	 }

	//$sql .=", '".$SOGER->UserData['idSIS_regCrono']."')";
	
	# UNIQUE STRING
	$Identifiers	= $SOGER->UserData['core_usersID_USR'].$SOGER->UserData['core_usersID_IMP'].$SOGER->UserData['workmode'];
	$UniqueString	= md5(uniqid($Identifiers, true));

	$sql .=", '".$SOGER->UserData['core_usersID_USR']."', '".$UniqueString."'";
	$sql .=")";
	$FEDIT->SDBWrite($sql,true,false);
	#echo $FEDIT->DbLastID;

        $LogSql  = "INSERT INTO core_logs ";
        $LogSql .= "(`table` , `pri_key` , `ID_USR` , `ID_IMP` , `sqlType` , `sqlCommand` ) ";
        $LogSql .= "VALUES ";
	$LogSql .= "('" . $TableName . "','" .  $FEDIT->DbLastID . "','";
	$LogSql .= $SOGER->UserData["core_usersID_USR"] . "','";
	$LogSql .= $SOGER->UserData["core_usersID_IMP"] . "','creazione auto','" . addslashes($sql) . "')";
	$FEDIT->SDBWrite($LogSql,true,false);

	//echo ($sql);
	

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>

