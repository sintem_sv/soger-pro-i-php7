<?php

$ID_AZX	= "";
$LastRecord = count($FEDIT->DbRecordSet)-1;

for($m=0; $m<count($FEDIT->DbRecordSet); $m++){

	if($FEDIT->DbRecordSet[$m]["AZref"]!=$ID_AZX) {

		// STAMPO TOTALI CER PRECEDENTE
		if($ID_CER!=""){
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
			$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,number_format((float)$TOTALONE_SCARICO, 2, ',', '.'),0,"R");
			$FEDIT->FGE_PdfBuffer->SetXY(260,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(25,4,number_format((float)$TOTALONE_COSTI, 2, ',', '.') . " �",0,"R");
			}


		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
		$Ypos  += 10;
		$ID_AZX		= $FEDIT->DbRecordSet[$m]["AZref"];
		$ID_UIMX	= "";
		$ID_CER		= "";

		$Ypos=20;
		$FEDIT->FGE_PdfBuffer->AddPage();
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
		$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$FEDIT->DbRecordSet[$m]["AZdes"]." ( C.F. ".$FEDIT->DbRecordSet[$m]['AZCF']." )",0,"L");
		$Ypos += 4;
		$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
		$AzAddr = $FEDIT->DbRecordSet[$m]["AZind"] . " - " . $FEDIT->DbRecordSet[$m]['AZCAP'] . " - " . $FEDIT->DbRecordSet[$m]["AZcom"] . " (" . $FEDIT->DbRecordSet[$m]["AZshprov"] . ") - " . $FEDIT->DbRecordSet[$m]["AZnaz"];    
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$AzAddr,0,"L");
		$Ypos += 4; 
		}

	## IMPIANTO DI:
	if($PrintImpianto) {

		if($FEDIT->DbRecordSet[$m]["IMPref"]!=$ID_UIMX) {

			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
			$ID_UIMX	= $FEDIT->DbRecordSet[$m]["IMPref"];
			$ID_CER		= "";

			$Ypos += 3;
			$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
			$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Impianto di:",0,"L");
			$ImpAddr = $FEDIT->DbRecordSet[$m]["IMPdes"] . " - " . $FEDIT->DbRecordSet[$m]['IMPCAP'] . " - " . $FEDIT->DbRecordSet[$m]["IMPcom"] . " (" . $FEDIT->DbRecordSet[$m]["IMPshprov"] . ") - " . $FEDIT->DbRecordSet[$m]["IMPnaz"];
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
			$FEDIT->FGE_PdfBuffer->SetXY(32,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$ImpAddr,0,"L");
			$Ypos += 4;
			}
		}




	if($ID_CER!=$FEDIT->DbRecordSet[$m]["CERref"]) {

		// STAMPO TOTALI CER PRECEDENTE
		if($ID_CER!=""){
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
			$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,number_format((float)$TOTALONE_SCARICO, 2, ',', '.'),0,"R");
			$FEDIT->FGE_PdfBuffer->SetXY(260,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(25,4,number_format((float)$TOTALONE_COSTI, 2, ',', '.') . " �",0,"R");
			}

		$TOTALONE_SCARICO	= 0;
		$TOTALONE_COSTI		= 0;

		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
		$ID_CER = $FEDIT->DbRecordSet[$m]["CERref"];
		$CERdes = $FEDIT->DbRecordSet[$m]["COD_CER"] . " - " . $FEDIT->DbRecordSet[$m]["descrizione"];
		
		# dati cer
		$BottomPage = 280;
		if($FEDIT->FGE_PdfBuffer->GetY() + 30 > $BottomPage){ 
			$FEDIT->FGE_PdfBuffer->AddPage(); 
			$Ypos = 10;
			}
		$Ypos += 10;
		$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$CERdes,0,"L");
		$Ypos += 6; 
		
		# intestazione movimenti
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetXY(5,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Movimento","B","C");
		$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Data FIR","B","C");
		$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Scarico kg","B","C");
		$FEDIT->FGE_PdfBuffer->SetXY(65,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(50,4,"Comune","B","C");
		$FEDIT->FGE_PdfBuffer->SetXY(115,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(120,4,"Costi","B","C");
		$FEDIT->FGE_PdfBuffer->SetXY(235,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,4,"N� FIR","B","C");
		$FEDIT->FGE_PdfBuffer->SetXY(260,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,4,"Totale","B","C");
		$Ypos += 4;
		}
	
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);

		$COSTI_MOV			= array();
		$QUANTICOSTI		= 0;
		$COSTO_TOT_MOV		= 0;

		# CONTRIBUTO AIA
		/*
		$ID_COMUNE_UDINE	= "0015622";
		$INDENNITA			= 1.91;
		if($FEDIT->DbRecordSet[$m]["id_comune_conferitore"]!=$ID_COMUNE_UDINE){
			$T		= $FEDIT->DbRecordSet[$m]["pesoN"] / 1000;
			$I_AIA	= $T * $INDENNITA;
			$COSTI_MOV[$QUANTICOSTI]['causale'] = "Indennit� AIA (� 1.91 x ton): ";
			$COSTI_MOV[$QUANTICOSTI]['importo'] = (float)$I_AIA;
			$COSTO_TOT_MOV+=(float)$I_AIA;
			$QUANTICOSTI++;
			}
		*/

		# CONTRATTI
		switch($_POST['Stat']){
			case "ECO_PRO_CAFC":
				$PrimaryCond	= "ID_CNT_COND_P";
				$CondTable		= "user_contratti_pro_cond";
				break;
			case "ECO_TRA_CAFC":
				$PrimaryCond	= "ID_CNT_COND_T";
				$CondTable		= "user_contratti_trasp_cond";
				break;
			case "ECO_DES_CAFC":
				$PrimaryCond	= "ID_CNT_COND_D";
				$CondTable		= "user_contratti_dest_cond";
				break;
			case "ECO_INT_CAFC":
				$PrimaryCond	= "ID_CNT_COND_I";
				$CondTable		= "user_contratti_int_cond";
				break;
			}

		$sql = "SELECT user_movimenti_costi.euro as importo, ".$CondTable.".description AS causale FROM user_movimenti_costi JOIN ".$CondTable." ON ".$CondTable.".".$PrimaryCond."=user_movimenti_costi.".$PrimaryCond." WHERE ID_MOV_F=".$FEDIT->DbRecordSet[$m]["ID_MOV_F"].";";
		$FEDIT->SDBRead($sql,"DbRecordSetCosti",true,false);
		for($c=0;$c<count($FEDIT->DbRecordSetCosti);$c++){
			$COSTI_MOV[$QUANTICOSTI]['causale'] = $FEDIT->DbRecordSetCosti[$c]['causale'];
			$COSTI_MOV[$QUANTICOSTI]['importo'] = (float)$FEDIT->DbRecordSetCosti[$c]['importo'];
			$COSTO_TOT_MOV += (float)$FEDIT->DbRecordSetCosti[$c]['importo'];
			$QUANTICOSTI++;			
			}

		$RIGHECOSTI = $QUANTICOSTI;
		if($RIGHECOSTI>0)
			$JUMP		= $RIGHECOSTI*4;
		else
			$JUMP		= 4;
		
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
		$FEDIT->FGE_PdfBuffer->SetXY(5,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,$JUMP,$FEDIT->DbRecordSet[$m]["NMOV"],"B","C");
		$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
		$DTFORM_A	= explode('-',$FEDIT->DbRecordSet[$m]["DTFORM"]);
		$DTFORM		= $DTFORM_A[2]."/".$DTFORM_A[1]."/".$DTFORM_A[0];
		$FEDIT->FGE_PdfBuffer->MultiCell(20,$JUMP,$DTFORM,"B","C");
		$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,$JUMP,number_format((float)$FEDIT->DbRecordSet[$m]["pesoN"], 2, ',', '.'),"B","R");
		$TOTALONE_SCARICO += (float)$FEDIT->DbRecordSet[$m]["pesoN"];
		$FEDIT->FGE_PdfBuffer->SetXY(65,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(50,$JUMP,$FEDIT->DbRecordSet[$m]["comune_conferitore"],"B","L");

		if(count($COSTI_MOV)>0){
			for($c=0;$c<count($COSTI_MOV);$c++){
				$FEDIT->FGE_PdfBuffer->SetXY(115,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(90,4,$COSTI_MOV[$c]['causale'],"B","L");
				$FEDIT->FGE_PdfBuffer->SetXY(205,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format((float)$COSTI_MOV[$c]['importo'], 2, ',', '.')." �","B","R");
				$Ypos += 4;
				}
			}
		else{
			$FEDIT->FGE_PdfBuffer->SetXY(115,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(120,4,"","B","L");
			}
		
		if(count($COSTI_MOV)>0) $Ypos -= $JUMP;
		$FEDIT->FGE_PdfBuffer->SetXY(235,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,$JUMP,$FEDIT->DbRecordSet[$m]["NFORM"],"B","C");
		$FEDIT->FGE_PdfBuffer->SetXY(260,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,$JUMP,number_format((float)$COSTO_TOT_MOV, 2, ',', '.')." �","B","R");
		$TOTALONE_COSTI += (float)$COSTO_TOT_MOV;
		$Ypos += $JUMP;

	}

		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,4,number_format((float)$TOTALONE_SCARICO, 2, ',', '.'),0,"R");
		$FEDIT->FGE_PdfBuffer->SetXY(260,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,4,number_format((float)$TOTALONE_COSTI, 2, ',', '.') . " �",0,"R");



?>