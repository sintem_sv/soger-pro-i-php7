<?php
session_start();
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");

if($SOGER->UserData["core_usersG3"]=="0") {
	$SOGER->SetFeedback("L'utente non ha i permessi necessari per accedere alla gestione economica [cod. G3]","1");	
} else {
	$SOGER->AppLocation ="UserCondizioneContrattoIntermediario";
}

require_once("../__includes/COMMON_sleepSoger.php");
header("location: ../");
?>
