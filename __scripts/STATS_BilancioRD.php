<?php

$RecordSet[0]['COD_CER']=$FEDIT->DbRecordSet[0]['COD_CER'];
$RecordSet[0]['operazione']=$FEDIT->DbRecordSet[0]['operazione'];
$RecordSet[0]['operazione_des']=$FEDIT->DbRecordSet[0]['operazione_des'];
$RecordSet[0]['rifiuto']=$FEDIT->DbRecordSet[0]['rifiuto'];
$RecordSet[0]['quantita']=$FEDIT->DbRecordSet[0]['quantita'];
$RecordSet[0]['quantitaDestino']=$FEDIT->DbRecordSet[0]['quantitaDestino'];
$RecordSet[0]['destinatario']=$FEDIT->DbRecordSet[0]['destinatario'];

$counter=1;

for($t=1;$t<count($FEDIT->DbRecordSet);$t++){

	if($FEDIT->DbRecordSet[$t]['operazione']==$RecordSet[$counter-1]['operazione']){
		# stesso operazione, aggiungo solo se rifiuto e destinatario sono diversi dal precedente

		if(($FEDIT->DbRecordSet[$t]['rifiuto']!=$RecordSet[$counter-1]['rifiuto']) || ($FEDIT->DbRecordSet[$t]['destinatario']!=$RecordSet[$counter-1]['destinatario'])){
			$RecordSet[$counter]['COD_CER']=$FEDIT->DbRecordSet[$t]['COD_CER'];
			$RecordSet[$counter]['operazione']=$FEDIT->DbRecordSet[$t]['operazione'];
			$RecordSet[$counter]['operazione_des']=$FEDIT->DbRecordSet[$t]['operazione_des'];
			$RecordSet[$counter]['rifiuto']=$FEDIT->DbRecordSet[$t]['rifiuto'];
			$RecordSet[$counter]['quantita']=$FEDIT->DbRecordSet[$t]['quantita'];
			$RecordSet[$counter]['quantitaDestino']=$FEDIT->DbRecordSet[$t]['quantitaDestino'];
			$RecordSet[$counter]['destinatario']=$FEDIT->DbRecordSet[$t]['destinatario'];
			$counter++;
			}
		else{ #stesso operazione, stesso rifiuto, stesso destinatario: sommo
			$RecordSet[$counter-1]['quantita']+=$FEDIT->DbRecordSet[$t]['quantita'];
			$RecordSet[$counter-1]['quantitaDestino']+=$FEDIT->DbRecordSet[$t]['quantitaDestino'];
			}

		}
	else{ #diverso operazione, aggiungo..
		$RecordSet[$counter]['COD_CER']=$FEDIT->DbRecordSet[$t]['COD_CER'];
		$RecordSet[$counter]['operazione']=$FEDIT->DbRecordSet[$t]['operazione'];
		$RecordSet[$counter]['operazione_des']=$FEDIT->DbRecordSet[$t]['operazione_des'];
		$RecordSet[$counter]['rifiuto']=$FEDIT->DbRecordSet[$t]['rifiuto'];
		$RecordSet[$counter]['quantita']=$FEDIT->DbRecordSet[$t]['quantita'];
		$RecordSet[$counter]['quantitaDestino']=$FEDIT->DbRecordSet[$t]['quantitaDestino'];
		$RecordSet[$counter]['destinatario']=$FEDIT->DbRecordSet[$t]['destinatario'];
		$counter++;
		}

	}


//print_r($RecordSet);



//echo $sql;

$lastoperazione="-";
$lastRifiuto="-";
$lastDestinatario="-";

$totale=0;
$superTotale=0;
$totaleDestino=0;
$superTotaleDestino=0;

$istoTot=array();
$istoTotDestino=array();
$istoTrat=array();

foreach($RecordSet as $k=>$dati) {

	if($dati['operazione']!=$lastoperazione){

		$istoTrat[]=$dati['operazione'];

		if($totale!=0){
			$istoTot[]=$totale;
			$istoTotDestino[]=$totaleDestino;
			$Ypos+=7;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
			$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(120,4,"Totale:","T","L");
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
			$FEDIT->FGE_PdfBuffer->SetXY(140,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(55,4,$totale." Kg","T","R");
			$FEDIT->FGE_PdfBuffer->SetXY(195,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(30,4,$totaleDestino." Kg","T","R");
			$totale=0;
			$totaleDestino=0;
			}

		$Ypos+=10;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
		if($dati['operazione']=="")
			$FEDIT->FGE_PdfBuffer->MultiCell(267,4,"Operazione non specificata","B","L");
		else
			$FEDIT->FGE_PdfBuffer->MultiCell(267,4,$dati['operazione']." - ".$dati['operazione_des'],"B","L");


		$Ypos+=10;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,4,"CER",0,"L");

		$FEDIT->FGE_PdfBuffer->SetXY(35,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(130,4,"Descrizione",0,"L");

		$FEDIT->FGE_PdfBuffer->SetXY(165,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Scarico (stimato)",0,"L");

		$FEDIT->FGE_PdfBuffer->SetXY(195,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Scarico (a destino)",0,"L");

		$FEDIT->FGE_PdfBuffer->SetXY(225,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(65,4,"Destinatario",0,"L");

		$Ypos+=6;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,3,$dati['COD_CER'],0,"L");

		$FEDIT->FGE_PdfBuffer->SetXY(35,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(130,3,$dati['rifiuto'],0,"L");

		$FEDIT->FGE_PdfBuffer->SetXY(165,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,3,number_format_unlimited_precision($dati['quantita'])." Kg",0,"R");

		$FEDIT->FGE_PdfBuffer->SetXY(195,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,3,number_format_unlimited_precision($dati['quantitaDestino'])." Kg",0,"R");

		$FEDIT->FGE_PdfBuffer->SetXY(225,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(65,3,$dati['destinatario'],0,"L");

		$lastoperazione=$dati['operazione'];
		$totale+=$dati['quantita'];
		$totaleDestino+=$dati['quantitaDestino'];
		$superTotale+=$dati['quantita'];
		$superTotaleDestino+=$dati['quantitaDestino'];
		}
	else{
		# stesso operazione, altro cer o destinatario
		$Ypos+=6;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,3,$dati['COD_CER'],0,"L");

		$FEDIT->FGE_PdfBuffer->SetXY(35,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(130,3,$dati['rifiuto'],0,"L");

		$FEDIT->FGE_PdfBuffer->SetXY(165,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,3,number_format_unlimited_precision($dati['quantita'])." Kg",0,"R");

		$FEDIT->FGE_PdfBuffer->SetXY(195,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,3,number_format_unlimited_precision($dati['quantitaDestino'])." Kg",0,"R");

		$FEDIT->FGE_PdfBuffer->SetXY(225,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(65,3,$dati['destinatario'],0,"L");

		$totale+=$dati['quantita'];
		$superTotale+=$dati['quantita'];
		$totaleDestino+=$dati['quantitaDestino'];
		$superTotaleDestino+=$dati['quantitaDestino'];
		}
	}


# totale dell'ultimo record #
if($totale!=0){

	$istoTot[]=$totale;
	$istoTotDestino[]=$totaleDestino;

	$Ypos+=7;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(120,4,"Totale:","T","L");
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->SetXY(140,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(55,4,$totale." Kg","T","R");
	$FEDIT->FGE_PdfBuffer->SetXY(195,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,$totaleDestino." Kg","T","R");
	$totale=0;
	$totaleDestino=0;
	}
#


# somma dei totali #
$Ypos+=10;
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(55,4,"Tutte le operazione R/D",0,"L");
$Ypos+=4;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(125,4,"Totale:","T","L");
$FEDIT->FGE_PdfBuffer->SetXY(140,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(55,4,$superTotale." Kg","T","R");
$FEDIT->FGE_PdfBuffer->SetXY(195,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,4,$superTotaleDestino." Kg","T","R");
#








//print_r($istoTrat);





#
#	ISTOGRAMMA (PARTITO)
#

$FEDIT->FGE_PdfBuffer->addpage();
$Ypos = 30;

require("STATS_palette.php");
$ColPointer = 0;




for($nc=0;$nc<count($istoTrat);$nc++){
	if($istoTrat[$nc]==""){
		$ncPerc=round(((100/$superTotale)*$istoTot[$nc]),2);
		$ncTot=$istoTot[$nc];
		}
	}


if(!isset($ncPerc)) $ncPerc=0;
if(!isset($ncTot)) $ncTot=0;
$cPerc=100-$ncPerc;

$superTotale-=$ncTot;

$FEDIT->FGE_PdfBuffer->SetXY(0,10);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',14);
$FEDIT->FGE_PdfBuffer->MultiCell(0,15,"Distribuzione Recuperi / Smaltimenti (stimati)",0,"C");


$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Movimenti non censiti","R","L");
$FEDIT->FGE_PdfBuffer->SetFillColor($palette[0][0],$palette[0][1],$palette[0][2]);
$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
if($ncPerc==0){
	$ncPercGraph=0.01;
	$FEDIT->FGE_PdfBuffer->MultiCell($ncPercGraph,5,"",0,"C",1);
	}
else
	$FEDIT->FGE_PdfBuffer->MultiCell($ncPerc,5,"",0,"C",1);

## agg.
$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$ncTot." Kg.",0,"L");

$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$ncPerc." %","L","R");

$Ypos+=10;

$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Movimenti censiti","R","L");
$FEDIT->FGE_PdfBuffer->SetFillColor($palette[3][0],$palette[3][1],$palette[3][2]);
$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
if($cPerc==0){
	$cPercGraph=0.01;
	$FEDIT->FGE_PdfBuffer->MultiCell($cPercGraph,5,"",0,"C",1);
	}
else
	$FEDIT->FGE_PdfBuffer->MultiCell($cPerc,5,"",0,"C",1);

## agg.
$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$superTotale." Kg.",0,"L");

$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$cPerc." %","L","R");

	$TotalR=0;
	$TotalD=0;
	$TotalRKG=0;
	$TotalDKG=0;



if($cPerc>0){

	$Ypos+=30;

	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','BU',12);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,15,"Dettaglio movimenti censiti",0,"L");

	$Ypos+=15;

	for($i=0;$i<count($istoTrat);$i++){
		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		if($istoTrat[$i]!=""){
			$OPLetter=str_split($istoTrat[$i]);
			$FEDIT->FGE_PdfBuffer->MultiCell(50,5,$istoTrat[$i],"R","L");
			$FEDIT->FGE_PdfBuffer->SetFillColor($palette[$ColPointer][0],$palette[$ColPointer][1],$palette[$ColPointer][2]);
			$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
			$perc=round(((100/$superTotale)*$istoTot[$i]),2);

			if($OPLetter[0]=="R"){
				$TotalR+=$perc;
				$TotalRKG+=$istoTot[$i];
				}
			else{ //D
				$TotalD+=$perc;
				$TotalDKG+=$istoTot[$i];
				}

			if($perc==0){
				$percGraph=0.01;
				$FEDIT->FGE_PdfBuffer->MultiCell($percGraph,5,"",0,"C",1);
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell($perc,5,"",0,"C",1);

			## agg.
			$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$istoTot[$i]." Kg.",0,"L");

			$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$perc." %","L","R");

			$Ypos+=10;

			if($ColPointer<count($palette))
				$ColPointer++;
			else
				$ColPointer=0;
			}
	}

	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,"","T","C");
	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Tutte le operazioni R/D","R","L");
	$FEDIT->FGE_PdfBuffer->SetFillColor($palette[$ColPointer][0],$palette[$ColPointer][1],$palette[$ColPointer][2]);
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,"",0,"C",1);

	## agg.
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$superTotale+$ncTot." Kg.",0,"L");

	$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,5,"100 %","L","R");
	}


if($cPerc>0){

	$Ypos+=10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','BU',12);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,15,"Recupero / Smaltimento",0,"L");

	$Ypos+=15;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Smaltimento","R","L");
	$FEDIT->FGE_PdfBuffer->SetFillColor($palette[0][0],$palette[0][1],$palette[0][2]);
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	if($TotalD==0)
		$FEDIT->FGE_PdfBuffer->MultiCell(0.01,5,"",0,"C",1);
	else
		$FEDIT->FGE_PdfBuffer->MultiCell($TotalD,5,"",0,"C",1);
	## agg.
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$TotalDKG." Kg.",0,"L");

	$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$TotalD." %","L","R");

	$Ypos+=10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Recupero","R","L");
	$FEDIT->FGE_PdfBuffer->SetFillColor($palette[3][0],$palette[3][1],$palette[3][2]);
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	if($TotalR==0)
		$FEDIT->FGE_PdfBuffer->MultiCell(0.01,5,"",0,"C",1);
	else
		$FEDIT->FGE_PdfBuffer->MultiCell($TotalR,5,"",0,"C",1);
	## agg.
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$TotalRKG." Kg.",0,"L");

	$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$TotalR." %","L","R");

	$Ypos+=10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(175,5,"","T","C");

	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Totale operazioni R/D","R","L");
	$FEDIT->FGE_PdfBuffer->SetFillColor($palette[2][0],$palette[2][1],$palette[2][2]);
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,"",0,"C",1);
	## agg.
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$superTotale+$ncTot." Kg.",0,"L");
	$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,5,"100 %","L","R");

}











#
#	ISTOGRAMMA (A DESTINO)
#

$FEDIT->FGE_PdfBuffer->addpage();
$Ypos = 30;

require("STATS_palette.php");
$ColPointer = 0;




for($nc=0;$nc<count($istoTrat);$nc++){
	if($istoTrat[$nc]==""){
		$ncPercDestino=round(((100/$superTotaleDestino)*$istoTotDestino[$nc]),2);
		$ncTotDestino=$istoTotDestino[$nc];
		}
	}


if(!isset($ncPercDestino)) $ncPercDestino=0;
if(!isset($ncTotDestino)) $ncTotDestino=0;
$cPercDestino=100-$ncPercDestino;

$superTotaleDestino-=$ncTotDestino;

$FEDIT->FGE_PdfBuffer->SetXY(0,10);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',14);
$FEDIT->FGE_PdfBuffer->MultiCell(0,15,"Distribuzione Recuperi / Smaltimenti (a destino)",0,"C");


$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Movimenti non censiti","R","L");
$FEDIT->FGE_PdfBuffer->SetFillColor($palette[0][0],$palette[0][1],$palette[0][2]);
$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
if($ncPercDestino==0){
	$ncPercGraph=0.01;
	$FEDIT->FGE_PdfBuffer->MultiCell($ncPercGraph,5,"",0,"C",1);
	}
else
	$FEDIT->FGE_PdfBuffer->MultiCell($ncPercDestino,5,"",0,"C",1);

## agg.
$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$ncTotDestino." Kg.",0,"L");

$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$ncPercDestino." %","L","R");

$Ypos+=10;

$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Movimenti censiti","R","L");
$FEDIT->FGE_PdfBuffer->SetFillColor($palette[3][0],$palette[3][1],$palette[3][2]);
$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
if($cPercDestino==0){
	$cPercGraph=0.01;
	$FEDIT->FGE_PdfBuffer->MultiCell($cPercGraph,5,"",0,"C",1);
	}
else
	$FEDIT->FGE_PdfBuffer->MultiCell($cPercDestino,5,"",0,"C",1);

## agg.
$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$superTotaleDestino." Kg.",0,"L");

$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$cPercDestino." %","L","R");


$TotalRdestino=0;
$TotalDdestino=0;
$TotalRKGdestino=0;
$TotalDKGdestino=0;


if($cPercDestino>0){

	$Ypos+=30;

	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','BU',12);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,15,"Dettaglio movimenti censiti",0,"L");

	$Ypos+=15;

	for($i=0;$i<count($istoTrat);$i++){
		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		if($istoTrat[$i]!=""){
			$OPLetter=str_split($istoTrat[$i]);
			$FEDIT->FGE_PdfBuffer->MultiCell(50,5,$istoTrat[$i],"R","L");
			$FEDIT->FGE_PdfBuffer->SetFillColor($palette[$ColPointer][0],$palette[$ColPointer][1],$palette[$ColPointer][2]);
			$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
			$percDestino=round(((100/$superTotaleDestino)*$istoTotDestino[$i]),2);

			if($OPLetter[0]=="R"){
				$TotalRdestino+=$percDestino;
				$TotalRKGdestino+=$istoTotDestino[$i];
				}
			else{ //D
				$TotalDdestino+=$percDestino;
				$TotalDKGdestino+=$istoTotDestino[$i];
				}

			if($percDestino==0){
				$percDestinoGraph=0.01;
				$FEDIT->FGE_PdfBuffer->MultiCell($percDestinoGraph,5,"",0,"C",1);
				}
			else
				$FEDIT->FGE_PdfBuffer->MultiCell($percDestino,5,"",0,"C",1);



			## agg.
			$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$istoTotDestino[$i]." Kg.",0,"L");

			$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$percDestino." %","L","R");

			$Ypos+=10;

			if($ColPointer<count($palette))
				$ColPointer++;
			else
				$ColPointer=0;
			}
	}

	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,"","T","C");
	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Tutte le operazioni R/D","R","L");
	$FEDIT->FGE_PdfBuffer->SetFillColor($palette[$ColPointer][0],$palette[$ColPointer][1],$palette[$ColPointer][2]);
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,"",0,"C",1);

	## agg.
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$superTotaleDestino+$ncTotDestino." Kg.",0,"L");

	$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,5,"100 %","L","R");
	}


if($cPercDestino>0){

	$Ypos+=10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','BU',12);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,15,"Recupero / Smaltimento",0,"L");

	$Ypos+=15;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Smaltimento","R","L");
	$FEDIT->FGE_PdfBuffer->SetFillColor($palette[0][0],$palette[0][1],$palette[0][2]);
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	if($TotalDdestino==0)
		$FEDIT->FGE_PdfBuffer->MultiCell(0.01,5,"",0,"C",1);
	else
		$FEDIT->FGE_PdfBuffer->MultiCell($TotalDdestino,5,"",0,"C",1);
	## agg.
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$TotalDKGdestino." Kg.",0,"L");

	$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$TotalDdestino." %","L","R");

	$Ypos+=10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Recupero","R","L");
	$FEDIT->FGE_PdfBuffer->SetFillColor($palette[3][0],$palette[3][1],$palette[3][2]);
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	if($TotalRdestino==0)
		$FEDIT->FGE_PdfBuffer->MultiCell(0.01,5,"",0,"C",1);
	else
		$FEDIT->FGE_PdfBuffer->MultiCell($TotalRdestino,5,"",0,"C",1);
	## agg.
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$TotalRKGdestino." Kg.",0,"L");

	$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$TotalRdestino." %","L","R");

	$Ypos+=10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(175,5,"","T","C");

	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Totale operazioni R/D","R","L");
	$FEDIT->FGE_PdfBuffer->SetFillColor($palette[2][0],$palette[2][1],$palette[2][2]);
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,"",0,"C",1);
	## agg.
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,$superTotaleDestino+$ncTotDestino." Kg.",0,"L");
	$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,5,"100 %","L","R");

}


?>