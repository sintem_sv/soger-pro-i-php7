<?php
session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/fpdf.php");
//include("../__libs/SQLFunct.php");
require_once("STATS_funct.php");


	function ConvertiData($dt) {
		
		if($dt!="0000-00-00") {
			$DataConvertita = strtotime($dt);
			$tmp = date(FGE_SCREEN_DATE, $DataConvertita);
			//$tmp .= "|" . dateDiff("d",date("m/d/Y",$DataConvertita),date("m/d/Y"));
		} else {
			$tmp = "Giac. Iniz.";
		}
		return $tmp;
	}

	//print_r($FEDIT->DbRecordSet);

	for($i=0;$i<count($FEDIT->DbRecordSet);$i++){
		$indice=count($rifiuti);
		if($FEDIT->DbRecordSet[$i]['RifID']!=$RIFbuf) {	
			$rifiuti[$indice]['totC']=$FEDIT->DbRecordSet[$i]['giac_ini'];
			$rifiuti[$indice]['COD_CER']=$FEDIT->DbRecordSet[$i]['COD_CER'];
			$rifiuti[$indice]['CERref']=$FEDIT->DbRecordSet[$i]['CERref'];
			$rifiuti[$indice]['RifID']=$FEDIT->DbRecordSet[$i]['RifID'];
			$rifiuti[$indice]['giac_ini']=$FEDIT->DbRecordSet[$i]['giac_ini'];
                        $rifiuti[$indice]['isGiacenzaIniziale']=$FEDIT->DbRecordSet[$i]['isGiacenzaIniziale'];
			$rifiuti[$indice]['descrizione']=$FEDIT->DbRecordSet[$i]['descrizione'];
			$rifiuti[$indice]['ID_RIFPROD_F']=$FEDIT->DbRecordSet[$i]['ID_RIFPROD_F'];
			if($FEDIT->DbRecordSet[$i]['TIPO']=="C"){
				$rifiuti[$indice]['carico'][0]['qta']=$FEDIT->DbRecordSet[$i]['quantita'];
				$rifiuti[$indice]['carico'][0]['num']=$FEDIT->DbRecordSet[$i]['NMOV'];
				$rifiuti[$indice]['carico'][0]['data']=$FEDIT->DbRecordSet[$i]['DTMOV'];
				$rifiuti[$indice]['totC']+=$FEDIT->DbRecordSet[$i]['quantita'];
				}
			if($FEDIT->DbRecordSet[$i]['TIPO']=="S")
				@$rifiuti[$indice]['totS']=$FEDIT->DbRecordSet[$i]['quantita'];
			}
		else{
			# somme
			$countCarichi=count($rifiuti[$indice-1]['carico']);
			if($FEDIT->DbRecordSet[$i]['TIPO']=="C"){
				$rifiuti[$indice-1]['carico'][$countCarichi]['qta']=$FEDIT->DbRecordSet[$i]['quantita'];
				$rifiuti[$indice-1]['carico'][$countCarichi]['num']=$FEDIT->DbRecordSet[$i]['NMOV'];
				$rifiuti[$indice-1]['carico'][$countCarichi]['data']=$FEDIT->DbRecordSet[$i]['DTMOV'];
				$rifiuti[$indice-1]['totC']+=$FEDIT->DbRecordSet[$i]['quantita'];
				}
			if($FEDIT->DbRecordSet[$i]['TIPO']=="S")
				@$rifiuti[$indice-1]['totS']+=$FEDIT->DbRecordSet[$i]['quantita'];
			}
		//print_r("RIF: ".$FEDIT->DbRecordSet[$i]['RifID']." TIPO:".$FEDIT->DbRecordSet[$i]['TIPO']." ".$FEDIT->DbRecordSet[$i]['quantita']."<br /> ");
		$RIFbuf=$FEDIT->DbRecordSet[$i]['RifID'];
		}

	//print_r($rifiuti);
	

	for($r=0;$r<count($rifiuti);$r++){
		
		$carichi=RemoveCarichiScaricati($rifiuti[$r], $CarichiRifiuto);
	
		if(!is_null($carichi)){
			//print_r(var_dump($carichi)."<br /><hr>");
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
			$BottomPage = 200;
			if($FEDIT->FGE_PdfBuffer->GetY() + 100 > $BottomPage){ 
				$FEDIT->FGE_PdfBuffer->AddPage(); 
				$Ypos = 10;
				}

			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(275,10,$rifiuti[$r]['COD_CER']." - ".$rifiuti[$r]['descrizione'],"LBR","L");
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',6);
			$Ypos += 20;
			$XRect=12.5;
			$RectWidth=270;
			$RectHeight=10;
			//$FEDIT->FGE_PdfBuffer->Rect($XRect, $Ypos, $RectWidth, $RectHeight, "D");
			$FEDIT->FGE_PdfBuffer->Image('../__css/ControlloDeposito.png', $XRect, $Ypos, $RectWidth, $RectHeight, "png");
			
			for($l=$XRect; $l<=$RectWidth+30; $l=$l+30){
				$Lines[]=$l;
				$day=10*(count($Lines)-1);				
				if($day==0){
					$FEDIT->FGE_PdfBuffer->Line($l,	$Ypos, $l, $Ypos+12);
					$FEDIT->FGE_PdfBuffer->SetXY($l-4,$Ypos+12);
					$day=substr(ConvertiData($carichi[$rifiuti[$r]['RifID']][0]['data']), 0, 5);	
					$FEDIT->FGE_PdfBuffer->MultiCell(20,8,$day,"","L");
					}
				else{
					# se il rifiuto a frequenza produzione regolare, disegno le tacche ogni 10 gg ( ogni 30 unit� ) 
					if($rifiuti[$r]['ID_RIFPROD_F']=='1'){
						$FEDIT->FGE_PdfBuffer->Line($l,	$Ypos, $l, $Ypos+12);
						$FEDIT->FGE_PdfBuffer->SetXY($l-3,$Ypos+12);
						$day.=" gg";	
						$FEDIT->FGE_PdfBuffer->MultiCell(20,8,$day,"","L");
						}
					}
				}

			# buffer
			if($carichi[$rifiuti[$r]['RifID']][0]['data']=='0000-00-00'){
				$caricoBuff=date('Y').'-01-01';
				}
			else{
				$caricoBuff=$carichi[$rifiuti[$r]['RifID']][0]['data'];
				}
			$dateStart=$caricoBuff;
			$Xbuff=12.5;

			# disegno linea in corrispondenza di ogni carico
			//print_r("<h1>".$carichi[$rifiuti[$r]['RifID']][0]['desc']."</h1><hr>");
			for($c=1;$c<count($carichi[$rifiuti[$r]['RifID']]); $c++){
				
				$Carico1=explode("-", $caricoBuff);
				$Carico1=$Carico1[1]."/".$Carico1[2]."/".$Carico1[0];
				//print_r("Carico buff: ".$caricoBuff."<hr>");
				//print_r("Carico 1: ".$Carico1."<hr>");

				$Carico2=explode("-", $carichi[$rifiuti[$r]['RifID']][$c]['data']);
				$Carico2=$Carico2[1]."/".$Carico2[2]."/".$Carico2[0];
				//print_r("Carico 2: ".$Carico2."<hr>");

				$giorniDifferenza=dateDiff("d", $Carico1, $Carico2);
				//print_r("Differenza: ".$giorniDifferenza."<hr>");

				$X=$Xbuff + ( 3 * $giorniDifferenza );
				$FEDIT->FGE_PdfBuffer->Line($X,	$Ypos-2, $X, $Ypos+10);

				$FEDIT->FGE_PdfBuffer->SetXY($X-4,$Ypos-8);
				$FEDIT->FGE_PdfBuffer->MultiCell(20,8,substr(ConvertiData($carichi[$rifiuti[$r]['RifID']][$c]['data']), 0, 5),"","L");

				$Xbuff=$X;
				$caricoBuff=$carichi[$rifiuti[$r]['RifID']][$c]['data'];
				}

			# disegno il riempimento come differenza tra oggi e dateStart
			$XRect=12.5;
			$Carico1=explode("-", $dateStart);
			$Carico1=$Carico1[1]."/".$Carico1[2]."/".$Carico1[0];
			$Carico2=date('m')."/".date('d')."/".date('Y');
			$GiorniTrascorsi=dateDiff("d", $Carico1, $Carico2);
			$RectWidth = 3 * $GiorniTrascorsi;
			$RectHeight=10;
			if($RectWidth>270)
				$RectWidth=270;
			$FEDIT->FGE_PdfBuffer->SetFillColor(255,255,255);
			$FEDIT->FGE_PdfBuffer->Rect($XRect+$RectWidth, $Ypos, 270-$RectWidth, $RectHeight, "DF");


			unset($Lines);
			$Ypos += 25;
			}
		}
	
	# print pdf
	PDFOut($FEDIT->FGE_PdfBuffer,$DcName);

?>