<?php

$Table=$TableI[0];
switch($Table){
        case "user_autorizzazioni_pro":
		$PrimaryKey="ID_AUTH";
		$PrimaryKeyImp="ID_UIMP";
		$TableImp="user_impianti_produttori";
		$PrimaryKeyAz="ID_AZP";
		$TableAz="user_aziende_produttori";
		break;
	case "user_autorizzazioni_trasp":
		$PrimaryKey="ID_AUTHT";
		$PrimaryKeyImp="ID_UIMT";
		$TableImp="user_impianti_trasportatori";
		$PrimaryKeyAz="ID_AZT";
		$TableAz="user_aziende_trasportatori";
		break;
	case "user_autorizzazioni_dest":
		$PrimaryKey="ID_AUTHD";
		$PrimaryKeyImp="ID_UIMD";
		$TableImp="user_impianti_destinatari";
		$PrimaryKeyAz="ID_AZD";
		$TableAz="user_aziende_destinatari";
		break;
	case "user_autorizzazioni_interm":
		$PrimaryKey="ID_AUTHI";
		$PrimaryKeyImp="ID_UIMI";
		$TableImp="user_impianti_intermediari";
		$PrimaryKeyAz="ID_AZI";
		$TableAz="user_aziende_intermediari";
		break;
	}

if($_POST[$_POST["FGE_PrimaryKeyRef"]]!=""){
	$FEDIT->SDBWrite($FEDIT->FGE_SQL_MakeUpdate(),true,false);
	$_SESSION["FGE_IDs"][$InsInfo[0]]=$_POST[$Table.":".$PrimaryKey];
	$label = "modificato";

	if($_POST[$Table.":inherit_edits"]=='1'){
		
		$FEDIT->SDBWrite("UPDATE ".$Table." SET inherit_edits=0 WHERE ".$PrimaryKey."=".$_POST[$_POST["FGE_PrimaryKeyRef"]] ,true,false);
		
		## aggiorno stesse aut su impianto
		$items = explode("/",$_POST[$Table.':rilascio']);
		$rilascio = $items[2] . "-" . $items[1] . "-" . $items[0];
		if($rilascio=="--") $rilascio="0000-00-00";

		$items = explode("/",$_POST[$Table.':scadenza']);
		$scadenza = $items[2] . "-" . $items[1] . "-" . $items[0];
		if($scadenza=="--") $scadenza="0000-00-00";
		$s ="UPDATE ".$Table." SET ";
		$s.="ID_AUT='".$_POST[$Table.':ID_AUT']."', PREV_num_aut='".$_POST[$Table.':num_aut']."', num_aut='".$_POST[$Table.':num_aut']."', rilascio='".$rilascio."', scadenza='".$scadenza."', ID_ORIGINE_DATI='".$_POST[$Table.':ID_ORIGINE_DATI']."' ";
		$s.="WHERE PREV_num_aut='".$_POST[$Table.':PREV_num_aut']."' AND ".$PrimaryKeyImp."=".$_POST[$Table.':'.$PrimaryKeyImp];
		$FEDIT->SDBWrite($s,true,false);
		}
	else{
		$FEDIT->SDBWrite("UPDATE ".$Table." SET PREV_num_aut = num_aut, inherit_edits=0 WHERE ".$PrimaryKey."=".$_POST[$_POST["FGE_PrimaryKeyRef"]] ,true,false);	
		}
	}
else{
	$FEDIT->SDBWrite($FEDIT->FGE_SQL_MakeInsert(),true,false);
	$_SESSION["FGE_IDs"][$InsInfo[0]]=$FEDIT->DbLastID;
	$label = "salvato";
	}
?>