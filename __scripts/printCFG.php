<?php
session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/fpdf.php");
include("../__libs/SQLFunct.php");
require_once("STATS_funct.php");


###########################################

$orientation= "P";
$statTitle	= "LIVELLO DI SICUREZZA";
$DcName		= date("d/m/Y") . "--Livello_di_sicurezza";

if($SOGER->UserData['core_impiantiREG_IND']==1) $TableName="user_movimenti"; else $TableName="user_movimenti_fiscalizzati";

$ID_USR		= $SOGER->UserData['core_usersID_USR'];
$ID_IMP		= $SOGER->UserData['core_usersID_IMP'];
$WORKMODE	= $SOGER->UserData['workmode'];

# LIVELLO SICUREZZA
if($SOGER->UserData["core_usersAUT_SCAD_LOCK"]==1)              $LkAutScadute='attivo';		else $LkAutScadute='non attivo';
if($SOGER->UserData["core_usersFDA_LOCK"]==1 && $SOGER->UserData['core_impiantiMODULO_FDA']==1)                   $LkFDA='attivo';		else $LkFDA='non attivo';
if($SOGER->UserData["core_usersCONTO_TERZI_CHECK"]==1)		$CkContoTerzi='attivo';		else $CkContoTerzi='non attivo';
if($SOGER->UserData["core_usersCONTO_TERZI_LOCK"]==1)		$LkContoTerzi='attivo';		else $LkContoTerzi='non attivo';
if($SOGER->UserData["core_usersCHECK_DESTINATARIO"]==1)		$CkDestinatario='attivo';	else $CkDestinatario='non attivo';
if($SOGER->UserData["core_usersCHECK_TRASPORTATORE"]==1)	$CkTrasportatore='attivo';	else $CkTrasportatore='non attivo';
if($SOGER->UserData["core_usersCHECK_MOV_ORDER"]==1)		$CkMovOrder='attivo';		else $CkMovOrder='non attivo';
if($SOGER->UserData["core_usersCHECK_DOUBLE_FIR"]==1)		$CkDoubleFir='attivo';		else $CkDoubleFir='non attivo';
if($SOGER->UserData["core_usersPRNT_HP4_HP8"]==1)			$PrintHp4Hp8='attiva';		else $PrintHp4Hp8='non attiva';
$SQL="SELECT description FROM lov_gestione_deposito WHERE ID_GDEP=".$SOGER->UserData["core_usersID_GDEP"].";";
$FEDIT->SDBRead($SQL,"DbRecordSet",true,false);
$GestioneDep=$FEDIT->DbRecordSet[0]['description'];


# CONTROLLI
$CkRitornoFormulario				= $SOGER->UserData["core_usersck_RIT_FORM"];
$intervalloForm						= $SOGER->UserData["core_usersck_RIT_FORMgg"];
$CkAutorizzazioni					= $SOGER->UserData["core_usersck_AUT"];
$intervallo							= $SOGER->UserData["core_usersck_AUTgg"];
$CkAnalisi							= $SOGER->UserData["core_usersck_ANALISI"];
$intervalloAnalisi					= $SOGER->UserData["core_usersck_ANALISIgg"];
$CkMOV_SISTRI						= $SOGER->UserData["core_usersck_MOV_SISTRI"];
$intervalloMOV_SISTRI				= $SOGER->UserData["core_usersck_MOV_SISTRIgg"];
$CkContributi						= $SOGER->UserData["core_usersck_CNTR"];
$CkDepositoTemporaneo				= $SOGER->UserData["core_usersck_DEP_TEMP"];
$CkDepositoTemporaneoM3				= $SOGER->UserData["core_usersq_limite"];
$CkDepositoTemporaneoM3pericolosi	= $SOGER->UserData["core_usersq_limite_p"];
$CkDepositoTemporaneoM3totali		= $SOGER->UserData["core_usersq_limite_t"];

# GIORNI MODIFICA MOVIMENTI
$GGedit_produttore                  = $SOGER->UserData["core_usersGGedit_produttore"];
$GGedit_trasportatore               = $SOGER->UserData["core_usersGGedit_trasportatore"];
$GGedit_destinatario                = $SOGER->UserData["core_usersGGedit_destinatario"];
$GGedit_intermediario               = $SOGER->UserData["core_usersGGedit_intermediario"];

$um="mm";
$Format = array(210,297);
$ZeroMargin = true;
$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin,$DcName);

$Ypos		= 30;
$Xpos		= 10;

$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',$LargeFont);

$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"Livello di sicurezza","TLR","C", 1);
$Ypos+=12;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"Configurazione allarmi, controlli e notifiche","BLR","C", 1);


## INTESTAZIONE AZIENDA
$SQL ="SELECT core_impianti.description as azienda, indirizzo, produttore, trasportatore, destinatario, intermediario, lov_comuni_istat.description AS comune, lov_comuni_istat.shdes_prov as provincia ";
$SQL.="FROM core_impianti JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM=core_impianti.ID_COM ";
$SQL.="WHERE core_impianti.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."';";
$FEDIT->SDBRead($SQL,"DbRecordSet",true,false);

$WM_global = array('produttore', 'trasportatore', 'destinatario', 'intermediario');
foreach($WM_global AS $k=>$w){
    if($FEDIT->DbRecordSet[0][$w]=='0')
        unset($WM_global[$k]);
}
$WM_global = array_values($WM_global);

$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);
$Ypos+=12;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(190,12,$FEDIT->DbRecordSet[0]['azienda'],"TLR","C");
$Ypos+=12;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"Stabilimento di ".$FEDIT->DbRecordSet[0]['indirizzo']." - ".$FEDIT->DbRecordSet[0]['comune']." (".$FEDIT->DbRecordSet[0]['provincia'].")","LR","C");
$Ypos+=12;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"Utente ".$SOGER->UserData['core_usersnome']. " " .$SOGER->UserData['core_userscognome'],"BLR","C");




## LIVELLO DI SICUREZZA
$Ypos+=30;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"Livello di sicurezza di gestione dei rifiuti","TLR","C", 1);
$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);

$Ypos+=12;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Blocco salvataggio movimentazione con soggetti con autorizzazione scadute/assente","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$LkAutScadute,"BLR","C");


$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Blocco salvataggio movimentazione la cui verifica targhe con Albo ha dato esito negativo","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$LkFDA,"BLR","C");


$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Blocco salvataggio movimentazione con trasportatori con abilitazione conto terzi assente","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$LkContoTerzi,"BLR","C");

$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Allarme movimentazione con trasportatori con abilitazione conto terzi assente","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$CkContoTerzi,"BLR","C");

$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Blocco salvataggio formulari senza destinatario","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$CkDestinatario,"BLR","C");

$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Blocco salvataggio formulari senza trasportatore","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$CkTrasportatore,"BLR","C");

$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Blocco salvataggio movimenti fuori ordine cronologico","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$CkMovOrder,"BLR","C");

$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Blocco salvataggio formulari gi� registrati","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$CkDoubleFir,"BLR","C");

$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Stampa della classe di pericolo HP 4 (irritante) in compresenza di HP 8 (corrosivo)","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$PrintHp4Hp8,"BLR","C");

$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Modalit� di gestione del deposito","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$GestioneDep,"BLR","C");

foreach ($WM_global as $key => $value) {
    $Ypos+=10;
    CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
    if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
    $Xpos=10;
    $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
    $FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Registro ".$value." - Movimenti modificabili per giorni","BLR","L");
    $Xpos+=160;
    $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
    $GG = ${"GGedit_".$value};
    if($value=="produttore") $limit=14; else $limit=2;
	if($GG>$limit){
		$FEDIT->FGE_PdfBuffer->SetFillColor(255,0,0);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$GG,"BLR","C",1);
		$avviso = "Attenzione, i giorni di modificabilit� della movimentazione indicati in configurazione superano le disposizioni dell'articolo 190 del D.Lgs. 152/2006 sulla corretta tenuta del registro di carico e scarico rifiuti.";
		$Ypos+=10;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
		$Xpos=10;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,10,$avviso,"BLR","L");
		$LISTA1['GGEDIT'][] = $avviso;
	}
	else{
		$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$GG,"BLR","C");
		$LISTA3['GGEDIT'][] = "Il sistema non ha individuato, al periodo considerato, criticit� da segnalare.";
	}
}

## FISCALIZZAZIONE
/*
switch($SOGER->UserData['workmode']){
	case "produttore":
		$ID_FISC_W	= $SOGER->UserData['core_usersID_FISC_PROD'];
		$Table		= "lov_fiscalizzazione_prod";
		$Field		= "ID_FISC_PROD";
		break;
	case "trasportatore":
		$ID_FISC_W	= $SOGER->UserData['core_usersID_FISC_TRASP'];
		$Table		= "lov_fiscalizzazione_trasp";
		$Field		= "ID_FISC_TRASP";
		break;
	case "destinatario":
		$ID_FISC_W	= $SOGER->UserData['core_usersID_FISC_DEST'];
		$Table		= "lov_fiscalizzazione_dest";
		$Field		= "ID_FISC_DEST";
		break;
	case "intermediario":
		$ID_FISC_W	= $SOGER->UserData['core_usersID_FISC_INTERM'];
		$Table		= "lov_fiscalizzazione_int";
		$Field		= "ID_FISC_INTERM";
		break;
	}

if($ID_FISC_W!='garbage' AND $ID_FISC_W!=0){
	$SQL="SELECT description FROM ".$Table." WHERE ".$Field."=".$ID_FISC_W.";";
	$FEDIT->SDBRead($SQL,"FrequenzaFiscalizzazione",true,false);
	}

if($SOGER->UserData['core_usersID_DAY']!='garbage' AND $SOGER->UserData['core_usersID_DAY']!=0){
	$SQL="SELECT description FROM lov_days WHERE ID_DAY=".$SOGER->UserData['core_usersID_DAY'];
	$FEDIT->SDBRead($SQL,"GiornoFiscalizzazione",true,false);
	}

$Ypos+=30;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;
$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"Fiscalizzazione","TLR","C", 1);
$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

$Ypos+=12;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Giorno di scrittura del registro fiscale","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersID_DAY']!='garbage' AND $SOGER->UserData['core_usersID_DAY']!=0){
	$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$FEDIT->GiornoFiscalizzazione[0]['description'],"BLR","C");
	}
else{
	$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"non impostato","BLR","C");
	}
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Frequenza di scrittura del registro fiscale","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($ID_FISC_W!='garbage' AND $ID_FISC_W!=0){
	$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"ogni ".$FEDIT->FrequenzaFiscalizzazione[0]['description'],"BLR","C");
	}
else{
	$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"non impostata","BLR","C");
	}
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Funzione di annullo ultima fiscalizzazione","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersD2']==1) $D2="abilitata"; else $D2="non abilitata";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$D2,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Accorpa carichi dello stesso rifiuto","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersmov_fiscalize_merge']==1) $merge="s�"; else $merge="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$merge,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Fiscalizza prima i carichi poi gli scarichi","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersmov_fiscalize_order']==1) $order="s�"; else $order="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$order,"BLR","C");
$Ypos+=10;
$Xpos=10;


$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Fiscalizza solo i movimenti del rifiuto selezionato","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersmov_fiscalize_rif']==1) $rif="s�"; else $rif="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$rif,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;
*/


## COSTI DI TRASPORTO
$SQL="SELECT * FROM user_viaggi_impianti WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' ORDER BY ID_MZ_TRA DESC;";
# [0]->furgone (ID_MZ_TRA=7)
# [1]->mezzo pesante (ID_MZ_TRA=2)
$FEDIT->SDBRead($SQL,"Costi",true,false);
if($FEDIT->DbRecsNum==0) {
	$SQL="SELECT * FROM lov_costi_trasposto WHERE ID_MZ_TRA IN (7, 2) ORDER BY ID_MZ_TRA DESC;";
	$FEDIT->SDBRead($SQL,"Costi",true,false);
	}

$Ypos+=30;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;
$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"Costi di trasporto (�/km)","TLR","C", 1);
$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

$CellWidth=20;
$Ypos+=12;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"","TBLR","L");
$Xpos+=30;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,10,"Gasolio","TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,10,"Autostrada","TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,"Assicurazione RCA","TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,"Bollo, revisione, etc.","TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,"Salario conducente","TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,10,"Manutenzione","TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,"Spese generali","TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,10,"Totale","TBLR","C");

$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,5,"Furgone","TBLR","L");
$Xpos+=30;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,$FEDIT->Costi[0]['costo_gasolio'],"TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,$FEDIT->Costi[0]['costo_autostrada'],"TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,$FEDIT->Costi[0]['costo_rc'],"TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,$FEDIT->Costi[0]['costo_bra'],"TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,$FEDIT->Costi[0]['costo_conducente'],"TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,$FEDIT->Costi[0]['costo_usura'],"TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,$FEDIT->Costi[0]['costo_generale'],"TBLR","C");
$Xpos+=$CellWidth;
$Totale=$FEDIT->Costi[0]['costo_gasolio']+$FEDIT->Costi[0]['costo_autostrada']+$FEDIT->Costi[0]['costo_rc']+$FEDIT->Costi[0]['costo_bra']+$FEDIT->Costi[0]['costo_conducente']+$FEDIT->Costi[0]['costo_usura']+$FEDIT->Costi[0]['costo_generale'];
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,$Totale,"TBLR","C");

$Ypos+=5;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,5,"Mezzo pesante","TBLR","L");
$Xpos+=30;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,$FEDIT->Costi[1]['costo_gasolio'],"TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,$FEDIT->Costi[1]['costo_autostrada'],"TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,$FEDIT->Costi[1]['costo_rc'],"TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,$FEDIT->Costi[1]['costo_bra'],"TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,$FEDIT->Costi[1]['costo_conducente'],"TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,$FEDIT->Costi[1]['costo_usura'],"TBLR","C");
$Xpos+=$CellWidth;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,$FEDIT->Costi[1]['costo_generale'],"TBLR","C");
$Xpos+=$CellWidth;
$Totale=$FEDIT->Costi[1]['costo_gasolio']+$FEDIT->Costi[1]['costo_autostrada']+$FEDIT->Costi[1]['costo_rc']+$FEDIT->Costi[1]['costo_bra']+$FEDIT->Costi[1]['costo_conducente']+$FEDIT->Costi[1]['costo_usura']+$FEDIT->Costi[1]['costo_generale'];
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,$Totale,"TBLR","C");




## COSTO DI TRASPORTO (correttivo)
# [0]->furgone (ID_MZ_TRA=7)
# [1]->mezzo pesante (ID_MZ_TRA=2)

$Ypos+=40;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;
$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"Costi di trasporto (�/viaggio)","TLR","C", 1);
$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

$CellWidth=50;
$Ypos+=12;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(140,10,"Mezzo di trasporto","TBLR","C");
$Xpos+=140;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,10,"Correttivo per viaggio","TBLR","C");

$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(140,5,"Furgone","TBLR","L");
$Xpos+=140;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,$FEDIT->Costi[0]['costo_correttivo'],"TBLR","C");

$Ypos+=5;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(140,5,"Mezzo pesante","TBLR","L");
$Xpos+=140;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell($CellWidth,5,$FEDIT->Costi[1]['costo_correttivo'],"TBLR","C");



# STAMPA FIR
$Ypos+=30;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;
$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"Configurazione stampa formulari","TLR","C", 1);
$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

$Ypos+=12;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Disabilita la stampa dei dati del produttore","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersFRM_DIS_PRO']==1) $FRM_DIS_PRO="s�"; else $FRM_DIS_PRO="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$FRM_DIS_PRO,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Disabilita la stampa dei dati del destinatario","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersFRM_DIS_DES']==1) $FRM_DIS_DES="s�"; else $FRM_DIS_DES="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$FRM_DIS_DES,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Disabilita la stampa dei dati del trasportatore","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersFRM_DIS_TRA']==1) $FRM_DIS_TRA="s�"; else $FRM_DIS_TRA="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$FRM_DIS_TRA,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Disabilita la stampa dei dati dell'intermediario","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersFRM_DIS_INT']==1) $FRM_DIS_INT="s�"; else $FRM_DIS_INT="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$FRM_DIS_INT,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Disabilita la stampa dei quantitativi","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersFRM_DIS_QNT']==1) $FRM_DIS_QNT="s�"; else $FRM_DIS_QNT="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$FRM_DIS_QNT,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Stampa il nome CER nelle annotazioni","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersFRM_PRINT_CER']==1) $FRM_PRINT_CER="s�"; else $FRM_PRINT_CER="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$FRM_PRINT_CER,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Stampa operazione R/D secondaria nelle annotazioni","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersOPRDsec']==1) $OPRDsec="s�"; else $OPRDsec="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$OPRDsec,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Stampa dicitura ADR nelle annotazioni","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersFRM_ADR']==1) $FRM_ADR="s�"; else $FRM_ADR="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$FRM_ADR,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Stampa dicitura ADR degli oli nelle annotazioni","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersFRM_ADR_OLI']==1) $FRM_ADR_OLI="s�"; else $FRM_ADR_OLI="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$FRM_ADR_OLI,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Inserisci automaticamente l'ora di inizio trasporto","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersFRM_SET_HR']==1) $FRM_SET_HR="s�"; else $FRM_SET_HR="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$FRM_SET_HR,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Inserisci automaticamente la data di inizio trasporto","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersFRM_SET_DATE']==1) $FRM_SET_DATE="s�"; else $FRM_SET_DATE="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$FRM_SET_DATE,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Inserisci automaticamente \"via diretta\" nel campo \"percorso\"","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersvia_diretta']==1) $via_diretta="s�"; else $via_diretta="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$via_diretta,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Barra automaticamente la casella \"verifica a destino\"","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersFRM_DefaultVD']==1) $FRM_DefaultVD="s�"; else $FRM_DefaultVD="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$FRM_DefaultVD,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Stampa il numero di iscrizione all'abo autotrasportatori in conto terzi","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersFRM_PRINT_CONTO_TERZI']==1) $FRM_PrintContoTerzi="s�"; else $FRM_PrintContoTerzi="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$FRM_PrintContoTerzi,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

/*
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Equipollenza del formulario alla scheda di trasporto","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$SQL="SELECT description FROM lov_scheda_trasp_details WHERE ID_ST_DET=".$SOGER->UserData['core_usersID_ST_DET'].";";
$FEDIT->SDBRead($SQL,"DbRecordSet",true,false);
$Equipollenza=$FEDIT->DbRecordSet[0]['description'];
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',6);
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$Equipollenza,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Soggetto committente","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$SQL="SELECT description FROM lov_scheda_trasp_b WHERE ID_COMM=".$SOGER->UserData['core_usersID_COMM'].";";
$FEDIT->SDBRead($SQL,"DbRecordSet",true,false);
$Committente=$FEDIT->DbRecordSet[0]['description'];
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',6);
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$Committente,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Soggetto caricatore","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$SQL="SELECT description FROM lov_scheda_trasp_c WHERE ID_CAR=".$SOGER->UserData['core_usersID_CAR'].";";
$FEDIT->SDBRead($SQL,"DbRecordSet",true,false);
$Caricatore=$FEDIT->DbRecordSet[0]['description'];
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',6);
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$Caricatore,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;
*/



# STAMPA REGISTRO
$FEDIT->FGE_PdfBuffer->addpage();
$Ypos=30;
$Xpos=10;
$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"Configurazione stampa registro","TLR","C", 1);
$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

$Ypos+=12;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Disabilita la stampa dei dati del produttore","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersREG_DIS_PRO']==1) $REG_DIS_PRO="s�"; else $REG_DIS_PRO="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$REG_DIS_PRO,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Stampa il nome CER nelle annotazioni","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersREG_NOMECER']==1) $REG_NOMECER="s�"; else $REG_NOMECER="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$REG_NOMECER,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Stampa l'ID Scheda SISTRI nelle annotazioni","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersREG_IDSIS_SCHEDA']==1) $REG_IDSIS_SCHEDA="s�"; else $REG_IDSIS_SCHEDA="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$REG_IDSIS_SCHEDA,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Stampa il numero della Scheda SISTRI nelle annotazioni","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersREG_NUMERO_SCHEDA']==1) $REG_NUMERO_SCHEDA="s�"; else $REG_NUMERO_SCHEDA="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$REG_NUMERO_SCHEDA,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Unit� di misura in cui sono riportati i quantitativi a registro","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
switch($ID_UMIS){
	default:
	case 1:
		$UMIS="kg";
		break;
	case 2:
		$UMIS="litri";
		break;
	case 3:
		$UMIS="metri cubi";
		break;
	}
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$UMIS,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Stampa anche l'unit� di misura della scheda rifiuto","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersUMIS_RIF']==1) $UMIS_RIF="s�"; else $UMIS_RIF="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$UMIS_RIF,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;


$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Stampa peso a destino anche se verifica non richiesta","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersREG_PS_DEST']==1) $REG_PS_DEST="s�"; else $REG_PS_DEST="no";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$REG_PS_DEST,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;




# ALLARMI E CONTROLLI
$Ypos+=30;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;
$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"Allarmi e controlli","TLR","C", 1);
$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

$Ypos+=12;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Controllo della scadenza delle autorizzazioni dei fornitori","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersck_AUT']==1) $ck_AUT="attivo"; else $ck_AUT="non attivo";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$ck_AUT,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Mostra avviso scadenza autorizzazioni con un anticipo di giorni","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$SOGER->UserData['core_usersck_AUTgg'],"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Controllo della scadenza del contributo annuale dei fornitori","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersck_CNTR']==1) $ck_CNTR="attivo"; else $ck_CNTR="non attivo";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$ck_CNTR,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Controllo della scadenza delle analisi di laboratorio","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersck_ANALISI']==1) $ck_AUT="attivo"; else $ck_AUT="non attivo";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$ck_AUT,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Mostra avviso fine validit� analisi di laboratorio con un anticipo di giorni","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$SOGER->UserData['core_usersck_ANALISIgg'],"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

if($SOGER->UserData['workmode']=='produttore'){
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Controllo della firma della movimentazione inviata a SISTRI","BLR","L");
	$Xpos+=160;
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	if($SOGER->UserData['core_usersck_MOV_SISTRI']==1) $ck_MOV_SISTRI="attivo"; else $ck_MOV_SISTRI="non attivo";
	$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$ck_MOV_SISTRI,"BLR","C");
	$Ypos+=10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
	$Xpos=10;

	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Mostra avviso firma della movimentazione inviata a SISTRI con un anticipo di giorni","BLR","L");
	$Xpos+=160;
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$SOGER->UserData['core_usersck_MOV_SISTRIgg'],"BLR","C");
	$Ypos+=10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
	$Xpos=10;
	}

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Controllo del rientro della IV copia del formulario","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersck_RIT_FORM']==1) $ck_RIT_FORM="attivo"; else $ck_RIT_FORM="non attivo";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$ck_RIT_FORM,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Mostra avviso mancato rientro della IV copia del formulario con un anticipo di giorni","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$SOGER->UserData['core_usersck_RIT_FORMgg'],"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Controllo dell'idoneit� ADR dei mezzi e degli autisti","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersck_ADR']==1) $ck_ADR="attivo"; else $ck_ADR="non attivo";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$ck_ADR,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Verifica delle targhe tramite servizio di validazione conferimenti","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersck_validazione_conferimento']==1) $ck_ADR="attivo"; else $ck_ADR="non attivo";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$ck_ADR,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Controllo sforamento del deposito temporaneo","BLR","L");
$Xpos+=160;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
if($SOGER->UserData['core_usersck_DEP_TEMP']==1) $ck_DEP_TEMP="attivo"; else $ck_DEP_TEMP="non attivo";
$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$ck_DEP_TEMP,"BLR","C");
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
$Xpos=10;

if($SOGER->UserData['core_usersID_GDEP']=='1'){ // tempo
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Metri cubi di deposito temporaneo per i rifiuti pericolosi","BLR","L");
	$Xpos+=160;
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$SOGER->UserData['core_usersq_limite_p'],"BLR","C");
	$Ypos+=10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
	$Xpos=10;

	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Metri cubi di deposito temporaneo per tutti i rifiuti","BLR","L");
	$Xpos+=160;
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$SOGER->UserData['core_usersq_limite_t'],"BLR","C");
	$Ypos+=10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
	$Xpos=10;
	}
else{ // volume
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(160,10,"Giorni di anticipo per l'allarme del superamento del deposito temporaneo","BLR","L");
	$Xpos+=160;
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,10,$SOGER->UserData['core_usersck_DEP_TEMPgg'],"BLR","C");
	$Ypos+=10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
	$Xpos=10;
	}



# UTENTI ABILITATI AL SETTAGGIO DEI CONTROLLI
$FEDIT->FGE_PdfBuffer->addpage();
$Ypos=30;
$Xpos=10;
$Xpos=10;
$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"Utenti abilitati al settaggio dei controlli","TLR","C", 1);
$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

$Ypos+=12;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;

$SQL="SELECT nome, cognome FROM core_users WHERE R5=1 AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1 AND usr<>'sintem';";
$FEDIT->SDBRead($SQL,"Controller",true,false);

for($c=0;$c<count($FEDIT->Controller);$c++){
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(190,10,$FEDIT->Controller[$c]['nome']." ".$FEDIT->Controller[$c]['cognome'],"BLR","L");
	$Ypos+=10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	if($FEDIT->FGE_PdfBuffer->GetY()<30) $Ypos=30;
	}

PDFOut($FEDIT->FGE_PdfBuffer,$DcName);

?>