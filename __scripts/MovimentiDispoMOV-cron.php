<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);


//session_start();
require_once("ForgEdit_includes.inc");
require_once("../__libs/zip3.php");
require_once("../__libs/fpdf.php");
require_once("../__classes/SogerMailer.php");


## CONNECTION TO DATABASE
$FEDIT=new DbLink();
$database = 'soger'.date('Y', strtotime('-1 year'));
$FEDIT->DbConn($database);

//$Tables		= array("user_movimenti", "user_movimenti_fiscalizzati");
$Tables		= array("user_movimenti_fiscalizzati");
$SendMail	= false;
$Message	= '';
$AlarmArray = array();

# Leggo tab rifiuti
$SQL="SELECT ID_RIF FROM user_schede_rifiuti;";
$FEDIT->SdbRead($SQL,"DbRecordSetRif",true,false);
$Rifiuti=$FEDIT->DbRecordSetRif;

for($r=0;$r<count($Rifiuti);$r++){

	for($t=0;$t<count($Tables);$t++){

		$TableName = $Tables[$t];

		if($TableName=="user_movimenti") $IDMov="ID_MOV"; else $IDMov="ID_MOV_F";

		$SQL ="SELECT ".$IDMov.", ".$TableName.".ID_IMP, ".$TableName.".produttore, ".$TableName.".trasportatore, ".$TableName.".destinatario, ".$TableName.".intermediario, user_schede_rifiuti.giac_ini, user_schede_rifiuti.descrizione, user_schede_rifiuti.ID_UMIS,  user_schede_rifiuti.peso_spec, ".$TableName.".ID_RIF, DTMOV, NMOV, TIPO, quantita, pesoN, ".$TableName.".FKEdisponibilita, lov_cer.COD_CER, user_schede_rifiuti.descrizione ";
		$SQL.="FROM ".$TableName." ";
		$SQL.="JOIN user_schede_rifiuti ON ".$TableName.".ID_RIF=user_schede_rifiuti.ID_RIF ";
		$SQL.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
		$SQL.="WHERE ".$TableName.".ID_RIF=".$Rifiuti[$r]['ID_RIF']." ";
		$SQL.="ORDER BY NMOV ASC";
		//echo $SQL."<hr>";
		$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);

		$rifBuffer=0;
		
		if($FEDIT->DbRecsNum>0){
			for($m=0; $m<count($FEDIT->DbRecordSet); $m++){

				if($FEDIT->DbRecordSet[$m]['ID_RIF']!=$rifBuffer){
					//echo "<hr>";
					# converto giac_ini in kg
					switch($FEDIT->DbRecordSet[$m]['ID_UMIS']){
						case 1:
							$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'];
							break;
						case 2:
							$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'] * $FEDIT->DbRecordSet[$m]['peso_spec'];
							break;
						case 3:
							$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'] * $FEDIT->DbRecordSet[$m]['peso_spec'] * 1000;
							break;
						}

					$Disponibilita=$KG_giac_ini;
					$rifBuffer=$FEDIT->DbRecordSet[$m]['ID_RIF'];
					//echo $Disponibilita."<br />";
					}
				else{
					for($i=0;$i<$m;$i++){
						if($FEDIT->DbRecordSet[$i]['ID_RIF']==$FEDIT->DbRecordSet[$m]['ID_RIF']){
							if($FEDIT->DbRecordSet[$i]['TIPO']=='C')
								$Disponibilita+=$FEDIT->DbRecordSet[$i]['pesoN'];
							else
								$Disponibilita-=$FEDIT->DbRecordSet[$i]['pesoN'];
							}
						}
					//echo $Disponibilita."<br />";
					}

				if($FEDIT->DbRecordSet[$m]['FKEdisponibilita']<>number_format($Disponibilita, 2, '.', '')){
					$sql="UPDATE ".$TableName." SET FKEdisponibilita=".number_format($Disponibilita, 2, '.', '')." WHERE ".$IDMov."=".$FEDIT->DbRecordSet[$m][$IDMov];
					$FEDIT->SDBWrite($sql,true,false);
					}

				# NOTIFICA MAIL
				if(number_format($Disponibilita, 2, '.', '')<0){
					$SendMail=true;
					$IdData = $TableName."|".$FEDIT->DbRecordSet[$m]['ID_IMP']."|".$FEDIT->DbRecordSet[$m]['produttore'].$FEDIT->DbRecordSet[$m]['trasportatore'].$FEDIT->DbRecordSet[$m]['destinatario'].$FEDIT->DbRecordSet[$m]['intermediario'];
					if(!in_array($IdData,$AlarmArray)){ 
						$AlarmArray[] = $IdData;
						if($FEDIT->DbRecordSet[$m]['produttore']==1)	$workmode='produttore';
						if($FEDIT->DbRecordSet[$m]['trasportatore']==1) $workmode='trasportatore';
						if($FEDIT->DbRecordSet[$m]['destinatario']==1)	$workmode='destinatario';
						if($FEDIT->DbRecordSet[$m]['intermediario']==1) $workmode='intermediario';

						$Message.="<b>Database: </b>" . $database."</b><br />";
						$Message.="<b>Impianto ".$FEDIT->DbRecordSet[$m]['ID_IMP']."</b><br />";
						$Message.="Profilo d'uso: ".$workmode."<br />";
						$Message.="Scheda rifiuto: ".$FEDIT->DbRecordSet[$i]['COD_CER']." - ".$FEDIT->DbRecordSet[$i]['descrizione']." (id: ".$FEDIT->DbRecordSet[$i]['ID_RIF'].")<br />";
						$Message.="Tabella: ".$TableName."<br />";
						$Message.="Movimento: Numero ".$FEDIT->DbRecordSet[$i]['NMOV']." del ".$FEDIT->DbRecordSet[$i]['DTMOV']." (id: ".$FEDIT->DbRecordSet[$i][$IDMov].")<br />";

						$Message.="<br />";
						}
					//die("ECCO.. DISPO: ".$Disponibilita." - RIFIUTO: ".$FEDIT->DbRecordSet[$i]['ID_RIF']);
					}

				# converto giac_ini in kg
				switch($FEDIT->DbRecordSet[$m]['ID_UMIS']){
					case 1:
						$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'];
						break;
					case 2:
						$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'] *  $FEDIT->DbRecordSet[$m]['peso_spec'];
						break;
					case 3:
						$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'] *  $FEDIT->DbRecordSet[$m]['peso_spec'] * 1000;
						break;
					}

				$Disponibilita=$KG_giac_ini;
				} // for movimenti
			} // if movimenti>0
		} // for tabelle movimenti
	}// for rifiuti




# Leggo tab rifiuti
$SQL="SELECT ID_RIF FROM user_schede_rifiuti;";
$FEDIT->SdbRead($SQL,"DbRecordSetRif",true,false);
$Rifiuti=$FEDIT->DbRecordSetRif;

for($r=0;$r<count($Rifiuti);$r++){

	for($t=0;$t<count($Tables);$t++){

		$TableName = $Tables[$t];

		if($TableName=="user_movimenti") $IDMov="ID_MOV"; else $IDMov="ID_MOV_F";

		$SQL ="SELECT ".$IDMov.", ".$TableName.".ID_IMP, ".$TableName.".produttore, ".$TableName.".trasportatore, ".$TableName.".destinatario, ".$TableName.".intermediario, user_schede_rifiuti.giac_ini, user_schede_rifiuti.descrizione, user_schede_rifiuti.ID_UMIS,  user_schede_rifiuti.peso_spec, ".$TableName.".ID_RIF, DTMOV, NMOV, TIPO, quantita, pesoN, ".$TableName.".FKEdisponibilita, lov_cer.COD_CER, user_schede_rifiuti.descrizione ";
		$SQL.="FROM ".$TableName." ";
		$SQL.="JOIN user_schede_rifiuti ON ".$TableName.".ID_RIF=user_schede_rifiuti.ID_RIF ";
		$SQL.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
		$SQL.="WHERE ".$TableName.".ID_RIF=".$Rifiuti[$r]['ID_RIF']." ";
		$SQL.="ORDER BY NMOV ASC";
		//echo $SQL."<hr>";
		$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);

		$rifBuffer=0;
		
		if($FEDIT->DbRecsNum>0){
			for($m=0; $m<count($FEDIT->DbRecordSet); $m++){

				if($FEDIT->DbRecordSet[$m]['ID_RIF']!=$rifBuffer){
					//echo "<hr>";
					# converto giac_ini in kg
					switch($FEDIT->DbRecordSet[$m]['ID_UMIS']){
						case 1:
							$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'];
							break;
						case 2:
							$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'] * $FEDIT->DbRecordSet[$m]['peso_spec'];
							break;
						case 3:
							$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'] * $FEDIT->DbRecordSet[$m]['peso_spec'] * 1000;
							break;
						}

					$Disponibilita=$KG_giac_ini;
					$rifBuffer=$FEDIT->DbRecordSet[$m]['ID_RIF'];
					//echo $Disponibilita."<br />";
					}
				else{
					for($i=0;$i<$m;$i++){
						if($FEDIT->DbRecordSet[$i]['ID_RIF']==$FEDIT->DbRecordSet[$m]['ID_RIF']){
							if($FEDIT->DbRecordSet[$i]['TIPO']=='C')
								$Disponibilita+=$FEDIT->DbRecordSet[$i]['pesoN'];
							else
								$Disponibilita-=$FEDIT->DbRecordSet[$i]['pesoN'];
							}
						}
					//echo $Disponibilita."<br />";
					}

				if($FEDIT->DbRecordSet[$m]['FKEdisponibilita']<>number_format($Disponibilita, 2, '.', '')){
					$sql="UPDATE ".$TableName." SET FKEdisponibilita=".number_format($Disponibilita, 2, '.', '')." WHERE ".$IDMov."=".$FEDIT->DbRecordSet[$m][$IDMov];
					$FEDIT->SDBWrite($sql,true,false);
					}

				# NOTIFICA MAIL
				if(number_format($Disponibilita, 2, '.', '')<0){
					$SendMail=true;
					$IdData = $TableName."|".$FEDIT->DbRecordSet[$m]['ID_IMP']."|".$FEDIT->DbRecordSet[$m]['produttore'].$FEDIT->DbRecordSet[$m]['trasportatore'].$FEDIT->DbRecordSet[$m]['destinatario'].$FEDIT->DbRecordSet[$m]['intermediario'];
					if(!in_array($IdData,$AlarmArray)){ 
						$AlarmArray[] = $IdData;
						if($FEDIT->DbRecordSet[$m]['produttore']==1)	$workmode='produttore';
						if($FEDIT->DbRecordSet[$m]['trasportatore']==1) $workmode='trasportatore';
						if($FEDIT->DbRecordSet[$m]['destinatario']==1)	$workmode='destinatario';
						if($FEDIT->DbRecordSet[$m]['intermediario']==1) $workmode='intermediario';

						$Message.="<b>Database: </b>" . $database."</b><br />";
						$Message.="<b>Impianto ".$FEDIT->DbRecordSet[$m]['ID_IMP']."</b><br />";
						$Message.="Profilo d'uso: ".$workmode."<br />";
						$Message.="Scheda rifiuto: ".$FEDIT->DbRecordSet[$i]['COD_CER']." - ".$FEDIT->DbRecordSet[$i]['descrizione']." (id: ".$FEDIT->DbRecordSet[$i]['ID_RIF'].")<br />";
						$Message.="Tabella: ".$TableName."<br />";
						$Message.="Movimento: Numero ".$FEDIT->DbRecordSet[$i]['NMOV']." del ".$FEDIT->DbRecordSet[$i]['DTMOV']." (id: ".$FEDIT->DbRecordSet[$i][$IDMov].")<br />";

						$Message.="<br />";
						}
					//die("ECCO.. DISPO: ".$Disponibilita." - RIFIUTO: ".$FEDIT->DbRecordSet[$i]['ID_RIF']);
					}

				# converto giac_ini in kg
				switch($FEDIT->DbRecordSet[$m]['ID_UMIS']){
					case 1:
						$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'];
						break;
					case 2:
						$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'] *  $FEDIT->DbRecordSet[$m]['peso_spec'];
						break;
					case 3:
						$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'] *  $FEDIT->DbRecordSet[$m]['peso_spec'] * 1000;
						break;
					}

				$Disponibilita=$KG_giac_ini;
				} // for movimenti
			} // if movimenti>0
		} // for tabelle movimenti
	}// for rifiuti
	
	
## SEND NOTIFICATION MAIL TO USER
$mail = new SogerMailer();
$mail->isHTML(true);
$mail->setFrom('cron@sogerpro.it', 'So.Ge.R. PRO');
$mail->addAddress('programmazione@sintem.it');
$mail->addAddress('assistenza@sintem.it');
$mail->addAddress('programmazione2@sintem.it');
$mail->Subject  = 'Script ricalcolo disponibilitÓ '.date('d-m-Y');

if(!$SendMail)
    $Message = "Nulla da segnalare";

//$message  = utf8_encode($message);
$mail->Body = $Message;
$mail->send();

echo $Message;

?>