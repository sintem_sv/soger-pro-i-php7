<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
session_start();
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");

$IDRIF = $_POST['ID_RIF'];

#
#	AGGIORNO DISPONIBILITA RIFIUTO
#
$IMP_FILTER			= true;
$DISPO_PRINT_OUT	= false;
$TableName			= "user_movimenti_fiscalizzati";
$IDMov				= "ID_MOV_F";
$DTMOV				= date("Y-m-d");
$EXCLUDE_9999999	= true;
require("MovimentiDispoRIF.php");
if($Disponibilita<0) $Disponibilita=0;
$SQL="UPDATE user_schede_rifiuti SET FKEdisponibilita='$Disponibilita' WHERE ID_RIF='".$IDRIF."'";
$FEDIT->SDBWrite($SQL,true,false);


#
#	RESTITUISCO INFO RIFIUTO
#
$SQL = "SELECT descrizione, lov_cer.COD_CER, FKEdisponibilita AS KG FROM user_schede_rifiuti JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER WHERE ID_RIF=".$IDRIF.";";
$FEDIT->SdbRead($SQL,"InfoData");

echo json_encode(array_map("utf8_encode", $FEDIT->InfoData[0]));

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>