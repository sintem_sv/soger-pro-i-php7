<?php


//print_r($FEDIT->DbRecordSet);

$RecordSet[0]['COD_CER']=$FEDIT->DbRecordSet[0]['COD_CER'];
$RecordSet[0]['trattamento']=$FEDIT->DbRecordSet[0]['trattamento'];
$RecordSet[0]['RS']=$FEDIT->DbRecordSet[0]['RS'];
$RecordSet[0]['rifiuto']=$FEDIT->DbRecordSet[0]['rifiuto'];
$RecordSet[0]['quantita']=$FEDIT->DbRecordSet[0]['quantita'];
$counter=1;

for($t=1;$t<count($FEDIT->DbRecordSet);$t++){

	if($FEDIT->DbRecordSet[$t]['trattamento']==$RecordSet[$counter-1]['trattamento']){
		# stesso trattamento, aggiungo solo se rifiuto o operazione R/S diversi dal precedente

		if($FEDIT->DbRecordSet[$t]['rifiuto']!=$RecordSet[$counter-1]['rifiuto'] || $FEDIT->DbRecordSet[$t]['RS']!=$RecordSet[$counter-1]['RS']){
			$RecordSet[$counter]['COD_CER']=$FEDIT->DbRecordSet[$t]['COD_CER'];
			$RecordSet[$counter]['trattamento']=$FEDIT->DbRecordSet[$t]['trattamento'];
			$RecordSet[$counter]['RS']=$FEDIT->DbRecordSet[$t]['RS'];
			$RecordSet[$counter]['rifiuto']=$FEDIT->DbRecordSet[$t]['rifiuto'];
			$RecordSet[$counter]['quantita']=$FEDIT->DbRecordSet[$t]['quantita'];
			$counter++;
			}
		else{ #stesso trattamento, stesso rifiuto
			$RecordSet[$counter-1]['quantita']+=$FEDIT->DbRecordSet[$t]['quantita'];
			}

		}
	else{ #diverso trattamento, aggiungo..
		$RecordSet[$counter]['COD_CER']=$FEDIT->DbRecordSet[$t]['COD_CER'];
		$RecordSet[$counter]['trattamento']=$FEDIT->DbRecordSet[$t]['trattamento'];
		$RecordSet[$counter]['RS']=$FEDIT->DbRecordSet[$t]['RS'];
		$RecordSet[$counter]['rifiuto']=$FEDIT->DbRecordSet[$t]['rifiuto'];
		$RecordSet[$counter]['quantita']=$FEDIT->DbRecordSet[$t]['quantita'];
		$counter++;
		}
	
	}


//print_r($RecordSet);



//echo $sql;

$lastTrattamento="-";
$lastRifiuto="-";
$lastDestinatario="-";
$lastRS="-";
$tmpQta=0;

$totale=0;
$superTotale=0;

$istoTot=array();
$istoTrat=array();

foreach($RecordSet as $k=>$dati) {

	if($dati['trattamento']!=$lastTrattamento || $dati['RS']!=$lastRS){
		if($dati['RS']=="") $dati['RS']="Op. R/D non specificata";
		
		$istoTrat[]=$dati['trattamento']." [".$dati['RS']."]";

		if($totale!=0){
			$istoTot[]=$totale;
			$Ypos+=5;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
			$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(120,4,"Totale:","T","L");
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
			$FEDIT->FGE_PdfBuffer->SetXY(140,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(55,4,$totale." Kg","T","R");
			$totale=0;
			}

		$Ypos+=10;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
		

		if($dati['trattamento']=="")
			$FEDIT->FGE_PdfBuffer->MultiCell(180,4,"Trattamento non specificato [".$dati['RS']."]","B","L");
		else
			$FEDIT->FGE_PdfBuffer->MultiCell(180,4,"Tipo di trattamento: ".$dati['trattamento']." [".$dati['RS']."]","B","L");


		$Ypos+=6;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,4,"CER",0,"L");

		$FEDIT->FGE_PdfBuffer->SetXY(35,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(135,4,"Descrizione",0,"L");

		$FEDIT->FGE_PdfBuffer->SetXY(170,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,4,"Scarico",0,"L");

		//$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
		//$FEDIT->FGE_PdfBuffer->MultiCell(65,4,"Destinatario",0,"L");

		$Ypos+=6;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,3,$dati['COD_CER'],0,"L");

		$FEDIT->FGE_PdfBuffer->SetXY(35,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(135,3,$dati['rifiuto'],0,"L");

		$FEDIT->FGE_PdfBuffer->SetXY(170,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,3,$dati['quantita']." Kg",0,"R");

		//$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
		//$FEDIT->FGE_PdfBuffer->MultiCell(65,4,$dati['destinatario'],0,"L");

		$lastTrattamento=$dati['trattamento'];
		$lastRS=$dati['RS'];
		$totale+=$dati['quantita'];
		$superTotale+=$dati['quantita'];
		}
	else{
		# stesso trattamento e op. r/s, altro cer o destinatario
		$Ypos+=6;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,3,$dati['COD_CER'],0,"L");

		$FEDIT->FGE_PdfBuffer->SetXY(35,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(135,3,$dati['rifiuto'],0,"L");

		$FEDIT->FGE_PdfBuffer->SetXY(170,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,3,$dati['quantita']." Kg",0,"R");

		//$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
		//$FEDIT->FGE_PdfBuffer->MultiCell(65,4,$dati['destinatario'],0,"L");

		$totale+=$dati['quantita'];
		$superTotale+=$dati['quantita'];
		}
	}


# totale dell'ultimo record #
if($totale!=0){

	$istoTot[]=$totale;

	$Ypos+=6;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(120,4,"Totale:","T","L");
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->SetXY(140,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(55,4,$totale." Kg","T","R");
	$totale=0;
	}
#


# somma dei totali #
$Ypos+=10;
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(55,4,"Tutti i trattamenti",0,"L");
$Ypos+=4;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(120,4,"Totale:","T","L");
$FEDIT->FGE_PdfBuffer->SetXY(140,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(55,4,$superTotale." Kg","T","R");
#








//print_r($istoTrat);





#
#	ISTOGRAMMA
#

$FEDIT->FGE_PdfBuffer->addpage();
$Ypos = 30;

require("STATS_palette.php");
$ColPointer = 0;




for($nc=0;$nc<count($istoTrat);$nc++){
	if($istoTrat[$nc]==""){
		$ncPerc=round(((100/$superTotale)*$istoTot[$nc]),2);
		$ncTot=$istoTot[$nc];
		}
	}


if(!isset($ncPerc)) $ncPerc=0;
if(!isset($ncTot)) $ncTot=0;
$cPerc=100-$ncPerc;

$superTotale-=$ncTot;

$FEDIT->FGE_PdfBuffer->SetXY(0,10);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',14);
$FEDIT->FGE_PdfBuffer->MultiCell(0,15,"BILANCIO ECOLOGICO",0,"C"); 


$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Movimenti non censiti","R","L"); 
$FEDIT->FGE_PdfBuffer->SetFillColor($palette[0][0],$palette[0][1],$palette[0][2]);
$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
if($ncPerc==0){
	$ncPercGraph=0.01;
	$FEDIT->FGE_PdfBuffer->MultiCell($ncPercGraph,5,"",0,"C",1);
	}
else
	$FEDIT->FGE_PdfBuffer->MultiCell($ncPerc,5,"",0,"C",1);
$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$ncPerc." %","L","R"); 

$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Movimenti censiti","R","L"); 
$FEDIT->FGE_PdfBuffer->SetFillColor($palette[3][0],$palette[3][1],$palette[3][2]);
$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
if($cPerc==0){
	$cPercGraph=0.01;
	$FEDIT->FGE_PdfBuffer->MultiCell($cPercGraph,5,"",0,"C",1);
	}
else
	$FEDIT->FGE_PdfBuffer->MultiCell($cPerc,5,"",0,"C",1);
$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$cPerc." %","L","R"); 


if($cPerc>0){

	$Ypos+=30;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','BU',12);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,15,"Dettaglio movimenti censiti",0,"L"); 

	$Ypos+=15;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	for($i=0;$i<count($istoTrat);$i++){
		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		if($istoTrat[$i]!=""){
			$FirstChar=str_split($istoTrat[$i]);
			if($FirstChar[1]=="[") // se il secondo carattere � [
				$FEDIT->FGE_PdfBuffer->MultiCell(50,3,"Trat. non specificato".$istoTrat[$i],"R","L"); 
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(50,5,$istoTrat[$i],"R","L"); 
			$FEDIT->FGE_PdfBuffer->SetFillColor($palette[$ColPointer][0],$palette[$ColPointer][1],$palette[$ColPointer][2]);
			$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
			$perc=round(((100/$superTotale)*$istoTot[$i]),2);
			$FEDIT->FGE_PdfBuffer->MultiCell($perc,5,"",0,"C",1);
			$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$perc." %","L","R"); 

			$Ypos+=10;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

			if($ColPointer<count($palette))
				$ColPointer++;
			else
				$ColPointer=0;
			}
	}

	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,"","T","C"); 
	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,5,"Tutti i trattamenti","R","L"); 
	$FEDIT->FGE_PdfBuffer->SetFillColor($palette[$ColPointer][0],$palette[$ColPointer][1],$palette[$ColPointer][2]);
	$FEDIT->FGE_PdfBuffer->SetXY(72,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(100,5,"",0,"C",1);
	$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,5,"100 %","L","R"); 
	}



?>