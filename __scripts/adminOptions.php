<?php
session_start();
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");

if(!isset($_POST["AdminChoice"])) {  
	$SOGER->SetFeedback("selezionare un'opzione",1);
	header("Location: ../");	
} 

switch($_POST["AdminChoice"]) {
	case "NuovoIntestatario":
	$SOGER->AppLocation ="AdminNuovoIntestatario";
	break;          
	case "NuovoGruppo": 
	$SOGER->AppLocation ="AdminNuovoGruppo";
	break;
	case "NuovoImpianto":
	$SOGER->AppLocation ="AdminNuovoImpianto";
	break;	
	case "NuovoUtente":
	$SOGER->AppLocation ="AdminNuovoUtente";
	break;
	case "NuovoResponsabile":
	$SOGER->AppLocation ="AdminNuovoResponsabile";
	break;
	case "VerificaUtenti":
	$SOGER->AppLocation ="AdminReviewUsers";
	break;
	case "VerificaGruppi":
	$SOGER->AppLocation ="AdminReviewGruppi";
	break;	
	case "VerificaImpianti":
	$SOGER->AppLocation ="AdminReviewImpianti";
	break;		
	case "VerificaIntestatari":
	$SOGER->AppLocation ="AdminReviewIntestatari";
	break;	
	case "VerificaResponsabili":
	$SOGER->AppLocation ="AdminReviewResponsabili";
	break;	
}

	require_once("../__includes/COMMON_sleepSoger.php");
	header("Location: ../");
?>
