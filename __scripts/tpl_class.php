<?php
	
	class template {
	
//	File-Handler f�r Template-File
		var $tpl_file = "";
		
//	Delimiters f�r Platzhalter
		var $delimiter="%";
		var $block_limit="{{";
		
//	Mailtext zum versenden von Templates per E-Mail
		var $mailtext="";
		
//	Template einlesen
		function read_file($filename){
			$this->tpl_file=file($filename);
			return $this->tpl_file;
		}
		
//	Platzhalter ersetzen
		function replace($get,$set){
			$this->source=$this->delimiter.$get.$this->delimiter;
			for($i=0;$i<count($this->tpl_file);$i++){
				$this->tpl_file[$i]=str_replace($this->source,$set,$this->tpl_file[$i]);
			};
			return $this->tpl_file;
		}
		
//	Ausgabe des fertig Templates
		function push(){
			for($i=0;$i<count($this->tpl_file);$i++){
				echo $this->tpl_file[$i];
			};
		}
		
//	Ausgabe des Templates als Email-Text in eine Variable
		function sendToVar(){
			for($i=0;$i<count($this->tpl_file);$i++){
				$this->ausgabe.=$this->tpl_file[$i];
			};
			return $this->ausgabe;
		}
		
//	L�schen des Template-Inhaltes
		function clear(){
			$this->tpl_file=NULL;
		}
		
	}
?>