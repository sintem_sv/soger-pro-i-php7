<?php

if($TableName=="user_movimenti") $IDMov="ID_MOV"; else $IDMov="ID_MOV_F";

$SQL ="SELECT ".$IDMov.", user_schede_rifiuti.giac_ini, user_schede_rifiuti.ID_UMIS,  user_schede_rifiuti.peso_spec, ".$TableName.".ID_RIF, DTMOV, NMOV, TIPO, quantita, pesoN, ".$TableName.".FKEdisponibilita, lov_cer.COD_CER, user_schede_rifiuti.descrizione ";
$SQL.="FROM ".$TableName." ";
$SQL.="JOIN user_schede_rifiuti ON ".$TableName.".ID_RIF=user_schede_rifiuti.ID_RIF ";
$SQL.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
$SQL.="WHERE ".$TableName.".ID_IMP='".$ID_IMP."' AND ".$TableName.".".$workmode."=1 ";
if(isset($IDRIF))
	$SQL.="AND ".$TableName.".ID_RIF=".$IDRIF." ";
$SQL.="ORDER BY ".$TableName.".ID_RIF, NMOV ASC, ".$IDMov." ASC ";

//echo $SQL;

$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);

$rifBuffer=0;

for($m=0; $m<count($FEDIT->DbRecordSet); $m++){

	if($FEDIT->DbRecordSet[$m]['ID_RIF']!=$rifBuffer){
		//echo "<hr>";
		# converto giac_ini in kg
		switch($FEDIT->DbRecordSet[$m]['ID_UMIS']){
			case 1:
				$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'];
				break;
			case 2:
				$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'] *  $FEDIT->DbRecordSet[$m]['peso_spec'];
				break;
			case 3:
				$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'] *  $FEDIT->DbRecordSet[$m]['peso_spec'] * 1000;
				break;
			}

		$Disponibilita=$KG_giac_ini;
		$rifBuffer=$FEDIT->DbRecordSet[$m]['ID_RIF'];
		//echo $Disponibilita."<br />";
		}
	else{
		for($i=0;$i<$m;$i++){
			if($FEDIT->DbRecordSet[$i]['ID_RIF']==$FEDIT->DbRecordSet[$m]['ID_RIF']){
				if($FEDIT->DbRecordSet[$i]['TIPO']=='C')
					$Disponibilita+=$FEDIT->DbRecordSet[$i]['pesoN'];
				else
					$Disponibilita-=$FEDIT->DbRecordSet[$i]['pesoN'];
				}
			}
		//echo $Disponibilita."<br />";
		}

	if($FEDIT->DbRecordSet[$m]['FKEdisponibilita']<>number_format($Disponibilita, 2, '.', '')){
		$sql="UPDATE ".$TableName." SET FKEdisponibilita=".number_format($Disponibilita, 2, '.', '')." WHERE ".$IDMov."=".$FEDIT->DbRecordSet[$m][$IDMov];
		$FEDIT->SDBWrite($sql,true,false);
		}
	
	# converto giac_ini in kg
	switch($FEDIT->DbRecordSet[$m]['ID_UMIS']){
		case 1:
			$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'];
			break;
		case 2:
			$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'] *  $FEDIT->DbRecordSet[$m]['peso_spec'];
			break;
		case 3:
			$KG_giac_ini = $FEDIT->DbRecordSet[$m]['giac_ini'] *  $FEDIT->DbRecordSet[$m]['peso_spec'] * 1000;
			break;
		}

	$Disponibilita=$KG_giac_ini;


	}

?>