<?php
session_start();
global $SOGER;
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/SQLFunct.php");


// aggiorno i riferimenti sul carico produttore
$SQL = "UPDATE user_movimenti_fiscalizzati SET LINK_DestProd='".$_POST['LINK_DestProd']."' WHERE ID_MOV_F=".$_POST['ID_MOV_F'].";";
$FEDIT->SDBWrite($SQL,true,false);

// rimuovo legami al carico da fir destinatario
$SQL = "SELECT ID_MOV_F, LINK_DestProd FROM user_movimenti_fiscalizzati WHERE destinatario=1 AND LINK_DestProd LIKE '%".$_POST['ID_MOV_F']."%';";
$FEDIT->SDBRead($SQL,"DbRecordSetFormulari",true,false);
if(isset($FEDIT->DbRecordSetFormulari)){
	for($f=0;$f<count($FEDIT->DbRecordSetFormulari);$f++){
		$Carichi = explode(',', $FEDIT->DbRecordSetFormulari[$f]['LINK_DestProd']);
		$Carichi2= array();
		for($c=0;$c<count($Carichi);$c++){
			if($Carichi[$c]!=$_POST['ID_MOV_F'])
				$Carichi2[] = $Carichi[$c];
			}
		$LINK_DestProd = (count($Carichi2)>0 ? implode(',', $Carichi2) : '');
		$SQL = "UPDATE user_movimenti_fiscalizzati SET LINK_DestProd='".$LINK_DestProd."' WHERE ID_MOV_F=".$FEDIT->DbRecordSetFormulari[$f]['ID_MOV_F'].";";
		$FEDIT->SDBWrite($SQL,true,false);
		}
	}

// aggiorno legami al carico su fir destinatario
if($_POST['LINK_DestProd']!=''){
	$fir = explode(',', $_POST['LINK_DestProd']);
	for($f=0;$f<count($fir);$f++){
		$SQL = "SELECT LINK_DestProd FROM user_movimenti_fiscalizzati WHERE destinatario=1 AND ID_MOV_F=".$fir[$f].";";
		$FEDIT->SDBRead($SQL,"DbRecordSetFormulari",true,false);
		if($FEDIT->DbRecordSetFormulari[0]['LINK_DestProd']!=''){
			$Carichi = explode(',', $FEDIT->DbRecordSetFormulari[0]['LINK_DestProd']);
			$Carichi[] = $_POST['ID_MOV_F'];
			$Carichi = implode(',', $Carichi);
			}
		else
			$Carichi = $_POST['ID_MOV_F'];
		$SQL = "UPDATE user_movimenti_fiscalizzati SET LINK_DestProd='".$Carichi."' WHERE ID_MOV_F=".$fir[$f].";";
		$FEDIT->SDBWrite($SQL,true,false);
		}
	}

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>