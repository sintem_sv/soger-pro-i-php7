<?php
global $SOGER;

if($_GET["IDrif"]=="garbage") $_GET["IDrif"]=0;

//if($_GET["IDrif"]!=="garbage") {
	$tableInfoBuffer = $FEDIT->FGE_TableInfo;

	#
	#	REPLY ARRAY 0-3 (stato fisico, codice progressivo, unitÓ di misura, peso specifico)
	#
	$FEDIT->FGE_FLushTableInfo();
	$FEDIT->FGE_UseTables(array("user_schede_rifiuti","lov_stato_fisico","lov_misure"));
	$FEDIT->FGE_SetSelectFields(array("ID_RIF","originalID_RIF","ClassificazioneHP","ID_SF","peso_spec","lotto","adr","ID_ONU","pericoloso","FANGHI_reg","CAR_NumDocumento","CAR_Laboratorio","CAR_DataAnalisi","prescrizioni_mov"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("description"),"lov_stato_fisico");
	$FEDIT->FGE_SetSelectFields(array("description"),"lov_misure");
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_SetFilter("ID_RIF", $_GET["IDrif"],"user_schede_rifiuti");
	$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);

	if(is_null($FEDIT->DbRecordSet[0]["user_schede_rifiutiID_ONU"]) | trim($FEDIT->DbRecordSet[0]["user_schede_rifiutiID_ONU"])=='')
		$ID_ONU='NULLO';
	else $ID_ONU=$FEDIT->DbRecordSet[0]["user_schede_rifiutiID_ONU"];

	if(is_null($FEDIT->DbRecordSet[0]["user_schede_rifiutiCAR_NumDocumento"]) | trim($FEDIT->DbRecordSet[0]["user_schede_rifiutiCAR_NumDocumento"])=='')
		$CAR_NumDocumento='NULLO';
	else $CAR_NumDocumento=$FEDIT->DbRecordSet[0]["user_schede_rifiutiCAR_NumDocumento"];

	if(is_null($FEDIT->DbRecordSet[0]["user_schede_rifiutiCAR_Laboratorio"]) | trim($FEDIT->DbRecordSet[0]["user_schede_rifiutiCAR_Laboratorio"])=='')
		$CAR_Laboratorio='NULLO';
	else $CAR_Laboratorio=$FEDIT->DbRecordSet[0]["user_schede_rifiutiCAR_Laboratorio"];

	if(is_null($FEDIT->DbRecordSet[0]["user_schede_rifiutiCAR_DataAnalisi"]) | trim($FEDIT->DbRecordSet[0]["user_schede_rifiutiCAR_DataAnalisi"])=='')
		$CAR_DataAnalisi='NULLO';
	else $CAR_DataAnalisi=$FEDIT->DbRecordSet[0]["user_schede_rifiutiCAR_DataAnalisi"];

	if(is_null($FEDIT->DbRecordSet[0]["user_schede_rifiutiprescrizioni_mov"]) | trim($FEDIT->DbRecordSet[0]["user_schede_rifiutiprescrizioni_mov"])=='')
		$prescrizioni_mov='NULLO';
	else $prescrizioni_mov=$FEDIT->DbRecordSet[0]["user_schede_rifiutiprescrizioni_mov"];

	if(is_null($FEDIT->DbRecordSet[0]["user_schede_rifiutilotto"]) | trim($FEDIT->DbRecordSet[0]["user_schede_rifiutilotto"])=='')
		$lotto='NULLO';
	else $lotto=$FEDIT->DbRecordSet[0]["user_schede_rifiutilotto"];

	$reply  = $FEDIT->DbRecordSet[0]["lov_stato_fisicodescription"];
	$reply .= "&&".$FEDIT->DbRecordSet[0]["user_schede_rifiutioriginalID_RIF"];
	$reply .= "&&".$FEDIT->DbRecordSet[0]["user_schede_rifiutiClassificazioneHP"];
	$reply .= "|" .$FEDIT->DbRecordSet[0]["user_schede_rifiutiadr"];
	$reply .= "&&".$ID_ONU;
	$reply .= "&&".$FEDIT->DbRecordSet[0]["user_schede_rifiutipericoloso"];
	$reply .= "&&".$CAR_NumDocumento;
	$reply .= "&&".$CAR_Laboratorio;
	$reply .= "&&".$CAR_DataAnalisi;
	$reply .= "&&".$prescrizioni_mov;
	$reply .= "&&".$lotto;
	$reply .= "|" .$FEDIT->DbRecordSet[0]["lov_misuredescription"]."&&".$FEDIT->DbRecordSet[0]["user_schede_rifiutiFANGHI_reg"];;
	$reply .= "|" .$FEDIT->DbRecordSet[0]["user_schede_rifiutipeso_spec"];

	#
	#	REPLY ARRAY 4:  PRODUTTORI codice fiscale
	#
	$FEDIT->FGE_FLushTableInfo();
	$FEDIT->FGE_UseTables(array("user_aziende_produttori","user_impianti_produttori"));
	//$FEDIT->FGE_SetSelectFields(array("ID_AZP","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono","codfisc","approved"),"user_aziende_produttori");
	$FEDIT->FGE_SetSelectFields(array("ID_AZP","ID_IMP","produttore","trasportatore","destinatario","intermediario","codfisc","approved"),"user_aziende_produttori");
	$FEDIT->FGE_SetSelectFields(array("ID_UIMP","approved"),"user_impianti_produttori");
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_aziende_produttori");
	#
	$FEDIT->FGE_SetFilter("approved","1","user_aziende_produttori");
	$FEDIT->FGE_SetFilter("approved","1","user_impianti_produttori");
	#
	$ProfiloFields = "user_aziende_produttori";
	include("../__includes/SOGER_FiltriProfilo.php");
        $FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
	if(isset($FEDIT->DbRecordSet)) {
		$multipleAZP = false;
		$multipleUIMP = false;
		if($FEDIT->DbRecsNum==1) {
			$ID_AZP = $FEDIT->DbRecordSet[0]["user_aziende_produttoriID_AZP"];
			$ID_UIMP = $FEDIT->DbRecordSet[0]["user_impianti_produttoriID_UIMP"];
			#
			#	REPLY ARRAY 4 (Produttori: codice fiscale)
			#
			$reply .= "|" . $FEDIT->DbRecordSet[0]["user_aziende_produttoricodfisc"];
		} else {
			foreach($FEDIT->DbRecordSet as $k=>$dati) {
				#
				if(!isset($tmpID_AZP)) {
					$tmpID_AZP = $dati["user_aziende_produttoriID_AZP"];
				} else {
					if($tmpID_AZP!=$dati["user_aziende_produttoriID_AZP"]) {
						$multipleAZP = true;
					}
				}
				#
				if(!isset($tmpID_UIMP)) {
					$tmpID_UIMP = $dati["user_impianti_produttoriID_UIMP"];
				} else {
					if($tmpID_UIMP!=$dati["user_impianti_produttoriID_UIMP"]) {
						$multipleUIMP = true;
					}
				}
			}
			#
			#	REPLY ARRAY 4 (Produttori: codice fiscale)
			#
			if(!$multipleAZP) {
				$ID_AZP = $tmpID_AZP;
				$reply .= "|" . $FEDIT->DbRecordSet[0]["user_aziende_produttoricodfisc"];
			} else {
				$reply .= "|";
			}
			if(!$multipleUIMP) {
				$ID_UIMP = $tmpID_UIMP;
			}
		}
	} else {
		$multipleAZP = true;
		$multipleUIMP = true;
		$reply .= "|";
	}




	#
	#	REPLY ARRAY 5-9: (solo scarico): DESTINATARI
	#

	if($_GET["Tipo"]=="S" | ($_GET["Tipo"]=="C" && $SOGER->UserData['workmode']!='produttore') ) {
		$FEDIT->FGE_FLushTableInfo();
		$FEDIT->FGE_UseTables(array("user_aziende_destinatari","user_impianti_destinatari","user_autorizzazioni_dest"));
		//$FEDIT->FGE_SetSelectFields(array("ID_AZD","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono","codfisc","approved"),"user_aziende_destinatari");
		$FEDIT->FGE_SetSelectFields(array("ID_AZD","ID_IMP","produttore","trasportatore","destinatario","intermediario","codfisc","approved"),"user_aziende_destinatari");
		$FEDIT->FGE_SetSelectFields(array("ID_UIMD", "IN_ITALIA", "approved"),"user_impianti_destinatari");
		$FEDIT->FGE_SetSelectFields(array("ID_AUTHD","ID_RIF","ID_OP_RS","ID_TRAT","rilascio","scadenza","ID_ORIGINE_DATI","approved"),"user_autorizzazioni_dest");
		$FEDIT->FGE_DescribeFields();
		$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_aziende_destinatari");
		#
		$FEDIT->FGE_SetFilter("approved","1","user_aziende_destinatari");
		$FEDIT->FGE_SetFilter("approved","1","user_impianti_destinatari");
		$FEDIT->FGE_SetFilter("approved","1","user_autorizzazioni_dest");
		#
		$FEDIT->FGE_SetFilter("ID_RIF", $_GET["IDrif"],"user_autorizzazioni_dest");
		$ProfiloFields = "user_aziende_destinatari";
		include("../__includes/SOGER_FiltriProfilo.php");
                $FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);


		if(isset($FEDIT->DbRecordSet)) {

				$multipleAZD = false;
				$multipleUIMD = false;
				$multipleAUTHD = false;

				if($FEDIT->DbRecsNum==1) {
					$ID_AZD = $FEDIT->DbRecordSet[0]["user_aziende_destinatariID_AZD"];
					$ID_UIMD = $FEDIT->DbRecordSet[0]["user_impianti_destinatariID_UIMD"];
					$ID_OP_RS = $FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_OP_RS"];
					$ID_TRAT = $FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_TRAT"];
					$ID_AUTHD = $FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_AUTHD"];
					$Transfrontaliero = $FEDIT->DbRecordSet[0]["user_impianti_destinatariIN_ITALIA"];
					$ID_ORIGINE_DATI=$FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_ORIGINE_DATI"];
					#
					#	REPLY ARRAY 5-9 (Destinatari: codice fiscale, rilascio e scadenza autorizzazione)
					#
					$reply .= "|" . $FEDIT->DbRecordSet[0]["user_aziende_destinataricodfisc"];
					$reply .= "|" . date(FGE_SCREEN_DATE, strtotime($FEDIT->DbRecordSet[0]["user_autorizzazioni_destrilascio"]));
					$reply .= "&&" . $ID_TRAT;
					$reply .= "|" . DateScreenConvert($FEDIT->DbRecordSet[0]["user_autorizzazioni_destscadenza"]);
					$reply .= "|" . $FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_ORIGINE_DATI"];
					$reply .= "&&" . $FEDIT->DbRecordSet[0]["user_impianti_destinatariIN_ITALIA"];
					}
				else {
					foreach($FEDIT->DbRecordSet as $k=>$dati) {

						if(!isset($tmpID_AUTHD)) {
							$tmpID_AUTHD = $dati["user_autorizzazioni_destID_AUTHD"];
							$tmpID_OP_RS = $dati["user_autorizzazioni_destID_OP_RS"];
							$tmpID_TRAT  = $dati["user_autorizzazioni_destID_TRAT"];
							}
						else {
							if($tmpID_AUTHD!=$dati["user_autorizzazioni_destID_AUTHD"]) {
								$multipleAUTHD = true;
								}
							}


						if(!isset($tmpID_AZD)) {
							$tmpID_AZD = $dati["user_aziende_destinatariID_AZD"];
							}
						else {
							if($tmpID_AZD!=$dati["user_aziende_destinatariID_AZD"]) {
								$multipleAZD = true;
								}
							}


						if(!isset($tmpID_UIMD)) {
							$tmpID_UIMD = $dati["user_impianti_destinatariID_UIMD"];
							$tmpTransfrontaliero = $dati["user_impianti_destinatariIN_ITALIA"];
							}
						else {
							if($tmpID_UIMD!=$dati["user_impianti_destinatariID_UIMD"]) {
								$multipleUIMD = true;
								}
							}


						}
					#
					#	REPLY ARRAY 5-9 (Destinatari: codice fiscale, rilascio e scadenza autorizzazione)
					#

					if(!$multipleAZD) {
						$ID_AZD = $tmpID_AZD;
						$reply .= "|" . $dati["user_aziende_destinataricodfisc"];
						}
					else {
						$reply .= "|";
						}

					if(!$multipleUIMD) {
						$ID_UIMD = $tmpID_UIMD;
						}

					if(!$multipleAUTHD) {
						$ID_OP_RS = $tmpID_OP_RS;
						$ID_TRAT  = $tmpID_TRAT;
						$ID_AUTHD = $tmpID_AUTHD;
						$reply .= "|" . date(FGE_SCREEN_DATE, strtotime($dati["user_autorizzazioni_destrilascio"]));
						$reply .= "&&" . $ID_TRAT;
						$reply .= "|" . DateScreenConvert($dati["user_autorizzazioni_destscadenza"]);
						$reply .= "|" . $FEDIT->DbRecordSet[0]["user_autorizzazioni_destID_ORIGINE_DATI"];
						$reply .= "&&" . $dati["user_impianti_destinatariIN_ITALIA"];
						}
					else {
						$reply .= "||||";
						}

					}
				}

		else {
				$reply .= "|||||";
			}
		}







	//}}}
	#
	#	REPLY ARRAY 10-14: (solo scarico) TRASPORTATORI
	#
//{{{
	if($_GET["Tipo"]=="S" | ($_GET["Tipo"]=="C" && $SOGER->UserData['workmode']!='produttore') ) {
		$FEDIT->FGE_FLushTableInfo();
		$FEDIT->FGE_UseTables(array("user_aziende_trasportatori","user_impianti_trasportatori","user_autorizzazioni_trasp"));
		//$FEDIT->FGE_SetSelectFields(array("ID_AZT","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono","codfisc","NumAlboAutotrasp","NumAlboAutotraspProprio","approved"),"user_aziende_trasportatori");
		$FEDIT->FGE_SetSelectFields(array("ID_AZT","ID_IMP","produttore","trasportatore","destinatario","intermediario","codfisc","NumAlboAutotrasp","NumAlboAutotraspProprio","approved"),"user_aziende_trasportatori");
		$FEDIT->FGE_SetSelectFields(array("ID_UIMT","approved"),"user_impianti_trasportatori");
		$FEDIT->FGE_SetSelectFields(array("ID_AUTHT","ID_RIF","rilascio","scadenza","ID_ORIGINE_DATI","approved"),"user_autorizzazioni_trasp");
		$FEDIT->FGE_DescribeFields();
		$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_aziende_trasportatori");
		$FEDIT->FGE_SetFilter("approved","1","user_aziende_trasportatori");
		$FEDIT->FGE_SetFilter("approved","1","user_impianti_trasportatori");
		$FEDIT->FGE_SetFilter("approved","1","user_autorizzazioni_trasp");
		$FEDIT->FGE_SetFilter("ID_RIF", $_GET["IDrif"],"user_autorizzazioni_trasp");
		$ProfiloFields = "user_aziende_trasportatori";
		include("../__includes/SOGER_FiltriProfilo.php");
		$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);

		if(isset($FEDIT->DbRecordSet)) {
				$multipleAZT = false;
				$multipleUIMT = false;
				$multipleAUTHT = false;
				if($FEDIT->DbRecsNum==1) {
					$ID_AZT = $FEDIT->DbRecordSet[0]["user_aziende_trasportatoriID_AZT"];
					$ID_UIMT = $FEDIT->DbRecordSet[0]["user_impianti_trasportatoriID_UIMT"];
					$ID_AUTHT = $FEDIT->DbRecordSet[0]["user_autorizzazioni_traspID_AUTHT"];
					$ID_ORIGINE_DATI = $FEDIT->DbRecordSet[0]["user_autorizzazioni_traspID_ORIGINE_DATI"];
					#
					#	REPLY ARRAY 10-14 (Trasportatori: codice fiscale, rilascio e scadenza autorizzazione)
					#
					$reply .= "|" . $FEDIT->DbRecordSet[0]["user_aziende_trasportatoricodfisc"] . "&&" . $FEDIT->DbRecordSet[0]["user_aziende_trasportatoriNumAlboAutotraspProprio"] . "&&" . $FEDIT->DbRecordSet[0]["user_aziende_trasportatoriNumAlboAutotrasp"];
					$reply .= "|" . date(FGE_SCREEN_DATE, strtotime($FEDIT->DbRecordSet[0]["user_autorizzazioni_trasprilascio"]));
					$reply .= "|" . DateScreenConvert($FEDIT->DbRecordSet[0]["user_autorizzazioni_traspscadenza"]);
					$reply .= "|" . $FEDIT->DbRecordSet[0]["user_autorizzazioni_traspID_ORIGINE_DATI"];
					}
				else {
					foreach($FEDIT->DbRecordSet as $k=>$dati) {
						#
						if(!isset($tmpID_AUTHT)) {
							$tmpID_AUTHT = $dati["user_autorizzazioni_traspID_AUTHT"];
							}
						else {
							if($tmpID_AUTHT!=$dati["user_autorizzazioni_traspID_AUTHT"]) {
								$multipleAUTHT = true;
								}
							}
						#
						if(!isset($tmpID_AZT)) {
							$tmpID_AZT = $dati["user_aziende_trasportatoriID_AZT"];
							}
						else {
							if($tmpID_AZT!=$dati["user_aziende_trasportatoriID_AZT"]) {
								$multipleAZT = true;
								}
							}
						#
						if(!isset($tmpID_UIMT)) {
							$tmpID_UIMT = $dati["user_impianti_trasportatoriID_UIMT"];
							}
						else {
							if($tmpID_UIMT!=$dati["user_impianti_trasportatoriID_UIMT"]) {
								$multipleUIMT = true;
								}
							}
						}
					#
					#	REPLY ARRAY 10-14 (Trasportatori: codice fiscale, rilascio e scadenza autorizzazione)
					#
					if(!$multipleAZT) {
						$ID_AZT = $tmpID_AZT;
						$reply .= "|" . $dati["user_aziende_trasportatoricodfisc"];
						}
					else {
						$reply .= "|";
						}
					if(!$multipleUIMT) {
						$ID_UIMT = $tmpID_UIMT;
						}
					if(!$multipleAUTHT) {
						$ID_AUTHT = $tmpID_AUTHT;
						$reply .= "|" . date(FGE_SCREEN_DATE, strtotime($dati["user_autorizzazioni_trasprilascio"]));
						$reply .= "|" . DateScreenConvert($dati["user_autorizzazioni_traspscadenza"]);
						$reply .= "|" . $FEDIT->DbRecordSet[0]["user_autorizzazioni_traspID_ORIGINE_DATI"];
						}
					else {
						$reply .= "||||";
						}
					}
			}
		else {
				$reply .= "|||||";
			}
		}


	#
	#	RIFERIMENTO AI MOVIMENTI DI CARICO
	#	e DISPONIBILITA' RIFIUTO
	#	"IF" commentato per permettere visualizzazione disponibilitÓ anche in movimenti di carico
	#
	$IMP_FILTER=true;
	$DISPO_PRINT_OUT = true;
	$IDRIF = $_GET["IDrif"];
	$DTMOV = $_GET["DTMOV"];
	$NMOV = $_GET['NMOV']? $_GET['NMOV']:null;
	$EXCLUDE_9999999	= false;
	# devo "passare" $IDMov e $TableName
	$TableName = $_GET['TableName'];
	if($TableName=="user_movimenti") $IDMov="ID_MOV";
	if($TableName=="user_movimenti_fiscalizzati") $IDMov="ID_MOV_F";
	$reply .= require("MovimentiDispoRIF.php");
	#


	// verifico presenza di giacenze automatiche da anno precedente e nel caso blocco in JS
	$SQL="SELECT id FROM user_movimenti_giacenze_iniziali WHERE ID_RIF=".$_GET["IDrif"];
	$FEDIT->SDBRead($SQL,"DbRecordSetGiacenzaIniziale",true,false);
	$gotGiacenzaIniziale = ($FEDIT->DbRecsNum>0? 1:0);
	$reply.="|".$gotGiacenzaIniziale;


	#
	#	REPLY ARRAY ??: (solo scarico) INTERMEDIARI
	#
//{{{
	if($_GET["Tipo"]=="S" | ($_GET["Tipo"]=="C" && $SOGER->UserData['workmode']!='produttore') ) {
		$FEDIT->FGE_FLushTableInfo();
		$FEDIT->FGE_UseTables(array("user_aziende_intermediari","user_impianti_intermediari","user_autorizzazioni_interm"));
		//$FEDIT->FGE_SetSelectFields(array("ID_AZI","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono","codfisc","approved"),"user_aziende_intermediari");
		$FEDIT->FGE_SetSelectFields(array("ID_AZI","ID_IMP","produttore","trasportatore","destinatario","intermediario","codfisc","approved"),"user_aziende_intermediari");
		$FEDIT->FGE_SetSelectFields(array("ID_UIMI","approved"),"user_impianti_intermediari");
		$FEDIT->FGE_SetSelectFields(array("ID_AUTHI","ID_RIF","rilascio","scadenza","ID_ORIGINE_DATI","approved"),"user_autorizzazioni_interm");
		$FEDIT->FGE_DescribeFields();
		$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_aziende_intermediari");
		$FEDIT->FGE_SetFilter("approved","1","user_aziende_intermediari");
		$FEDIT->FGE_SetFilter("approved","1","user_impianti_intermediari");
		$FEDIT->FGE_SetFilter("approved","1","user_autorizzazioni_interm");
		$FEDIT->FGE_SetFilter("ID_RIF", $_GET["IDrif"],"user_autorizzazioni_interm");
		$ProfiloFields = "user_aziende_intermediari";
		include("../__includes/SOGER_FiltriProfilo.php");
		$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);

		if(isset($FEDIT->DbRecordSet)) {
				$multipleAZI = false;
				$multipleUIMI = false;
				$multipleAUTHI = false;
				if($FEDIT->DbRecsNum==1) {
					$ID_AZI = $FEDIT->DbRecordSet[0]["user_aziende_intermediariID_AZI"];
					$ID_UIMI = $FEDIT->DbRecordSet[0]["user_impianti_intermediariID_UIMI"];
					$ID_AUTHI = $FEDIT->DbRecordSet[0]["user_autorizzazioni_intermID_AUTHI"];
					$ID_ORIGINE_DATI = $FEDIT->DbRecordSet[0]["user_autorizzazioni_traspID_ORIGINE_DATI"];
					#
					#	REPLY ARRAY 10-14 (Intermediari: codice fiscale, rilascio e scadenza autorizzazione)
					#
					$reply .= "|" . $FEDIT->DbRecordSet[0]["user_aziende_intermediaricodfisc"];
					$reply .= "|" . date(FGE_SCREEN_DATE, strtotime($FEDIT->DbRecordSet[0]["user_autorizzazioni_intermrilascio"]));
					$reply .= "|" . DateScreenConvert($FEDIT->DbRecordSet[0]["user_autorizzazioni_intermscadenza"]);
					$reply .= "|" . $FEDIT->DbRecordSet[0]["user_autorizzazioni_intermID_ORIGINE_DATI"];
					}
				else {
					foreach($FEDIT->DbRecordSet as $k=>$dati) {
						#
						if(!isset($tmpID_AUTHI)) {
							$tmpID_AUTHI = $dati["user_autorizzazioni_intermID_AUTHI"];
							}
						else {
							if($tmpID_AUTHI!=$dati["user_autorizzazioni_intermID_AUTHI"]) {
								$multipleAUTHI = true;
								}
							}
						#
						if(!isset($tmpID_AZI)) {
							$tmpID_AZI = $dati["user_aziende_intermediariID_AZI"];
							}
						else {
							if($tmpID_AZI!=$dati["user_aziende_intermediariID_AZI"]) {
								$multipleAZI = true;
								}
							}
						#
						if(!isset($tmpID_UIMI)) {
							$tmpID_UIMI = $dati["user_impianti_intermediariID_UIMI"];
							}
						else {
							if($tmpID_UIMI!=$dati["user_impianti_intermediariID_UIMI"]) {
								$multipleUIMI = true;
								}
							}
						}
					#
					#	REPLY ARRAY ?? (Intermediari: codice fiscale, rilascio e scadenza autorizzazione)
					#
					if(!$multipleAZI) {
						$ID_AZI = $tmpID_AZI;
						$reply .= "|" . $dati["user_aziende_intermediaricodfisc"];
						}
					else {
						$reply .= "|";
						}
					if(!$multipleUIMI) {
						$ID_UIMI = $tmpID_UIMI;
						}
					if(!$multipleAUTHI) {
						$ID_AUTHI = $tmpID_AUTHI;
						$reply .= "|" . date(FGE_SCREEN_DATE, strtotime($dati["user_autorizzazioni_intermrilascio"]));
						$reply .= "|" . DateScreenConvert($dati["user_autorizzazioni_intermscadenza"]);
						$reply .= "|" . $FEDIT->DbRecordSet[0]["user_autorizzazioni_intermID_ORIGINE_DATI"];
						}
					else {
						$reply .= "||||";
						}
					}
			}
		else {
				$reply .= "|||||";
			}
		}










	$FEDIT->FGE_TableInfo = $tableInfoBuffer;
	#
	#	APPLICAZIONE FILTRI / RIDEFINIZIONE COMBO: PRODUTTORI
	#
	$FEDIT->FGE_LookUpCFG("ID_AZP",$TableName,false,true);
	$FEDIT->FGE_SetFilter("ID_AZP",null,"user_aziende_produttori");
	$FEDIT->FGE_LookUpDone();

        $FEDIT->FGE_LookUpCFG("ID_UIMP",$TableName,false,true);
        if(isset($ID_AZP)) {
                $FEDIT->FGE_SetFilter("ID_AZP",$ID_AZP,"user_impianti_produttori");
        } else {
                $FEDIT->FGE_SetFilter("ID_AZP","###","user_impianti_produttori");
        }
        $FEDIT->FGE_LookUpDone();
	if(!$multipleAZP) {
		$FEDIT->FGE_SetValue("ID_AZP",$ID_AZP,$TableName);
	}
	if(!$multipleUIMP) {
		$FEDIT->FGE_SetValue("ID_UIMP",$ID_UIMP,$TableName);
	}

	#
	#	APPLICAZIONE FILTRI / RIDEFINIZIONE COMBO: DESTINATARI
	#
	//{{{
	if($_GET["Tipo"]=="S" | ($_GET["Tipo"]=="C" && $SOGER->UserData['workmode']!='produttore') ) {
		if(isset($ID_OP_RS) && !$multipleAUTHD) {
			$FEDIT->FGE_SetValue("ID_OP_RS",$ID_OP_RS,$TableName);
		} else {
			$FEDIT->FGE_SetValue("ID_OP_RS",0,$TableName);
		}

		$FEDIT->FGE_LookUpCFG("ID_AZD",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["IDrif"],"user_autorizzazioni_dest");
		$FEDIT->FGE_LookUpDone();
		$FEDIT->FGE_LookUpCFG("ID_UIMD",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["IDrif"],"user_autorizzazioni_dest");
		if(isset($ID_AZD)) {
			$FEDIT->FGE_SetFilter("ID_AZD",$ID_AZD,"user_impianti_destinatari");
		} else {
			$FEDIT->FGE_SetFilter("ID_AZD","###","user_impianti_destinatari");
		}
		$FEDIT->FGE_LookUpDone();

		$FEDIT->FGE_LookUpCFG("ID_AUTHD",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["IDrif"],"user_autorizzazioni_dest");
		if(isset($ID_UIMD)) {
			$FEDIT->FGE_SetFilter("ID_UIMD",$ID_UIMD,"user_autorizzazioni_dest");
			}
		else {
			$FEDIT->FGE_SetFilter("ID_UIMD","###","user_autorizzazioni_dest");
			}
		$FEDIT->FGE_LookUpDone();

	} else {
		$FEDIT->FGE_LookUpCFG("ID_AZD",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["IDrif"],"user_autorizzazioni_dest");
		$FEDIT->FGE_LookUpDone();
		$FEDIT->FGE_LookUpCFG("ID_UIMD",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["IDrif"],"user_autorizzazioni_dest");
		$FEDIT->FGE_LookUpDone();
		$FEDIT->FGE_LookUpCFG("ID_AUTHD",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["IDrif"],"user_autorizzazioni_dest");
		$FEDIT->FGE_LookUpDone();
	}
	//}}}
	#
	#	APPLICAZIONE FILTRI / RIDEFINIZIONE COMBO: TRASPORTATORI
	#
//{{{
	if($_GET["Tipo"]=="S" | ($_GET["Tipo"]=="C" && $SOGER->UserData['workmode']!='produttore') ) {
		$FEDIT->FGE_LookUpCFG("ID_AZT",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["IDrif"],"user_autorizzazioni_trasp");
		$FEDIT->FGE_LookUpDone();
		$FEDIT->FGE_LookUpCFG("ID_UIMT",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["IDrif"],"user_autorizzazioni_trasp");
		if(isset($ID_AZT)) {
			$FEDIT->FGE_SetFilter("ID_AZT",$ID_AZT,"user_impianti_trasportatori");
		} else {
			$FEDIT->FGE_SetFilter("ID_AZT","###","user_impianti_trasportatori");
		}
		$FEDIT->FGE_LookUpDone();
		$FEDIT->FGE_LookUpCFG("ID_AUTHT",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["IDrif"],"user_autorizzazioni_trasp");
		if(isset($ID_UIMT)) {
			$FEDIT->FGE_SetFilter("ID_UIMT",$ID_UIMT,"user_autorizzazioni_trasp");
		} else {
			$FEDIT->FGE_SetFilter("ID_UIMT","###","user_autorizzazioni_trasp");
		}
		$FEDIT->FGE_LookUpDone();
	} else {
		$FEDIT->FGE_LookUpCFG("ID_AZT",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["IDrif"],"user_autorizzazioni_trasp");
		$FEDIT->FGE_LookUpDone();
		$FEDIT->FGE_LookUpCFG("ID_UIMT",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["IDrif"],"user_autorizzazioni_trasp");
		$FEDIT->FGE_LookUpDone();
		$FEDIT->FGE_LookUpCFG("ID_AUTHT",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["IDrif"],"user_autorizzazioni_trasp");
		$FEDIT->FGE_LookUpDone();
	}
	//}}}

	#
	#
	#	APPLICAZIONE FILTRI / RIDEFINIZIONE COMBO: INTERMEDIARI
	#
//{{{
	if($_GET["Tipo"]=="S" | ($_GET["Tipo"]=="C" && $SOGER->UserData['workmode']!='produttore') ) {
		$FEDIT->FGE_LookUpCFG("ID_AZI",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["IDrif"],"user_autorizzazioni_interm");
		$FEDIT->FGE_LookUpDone();
		$FEDIT->FGE_LookUpCFG("ID_UIMI",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["IDrif"],"user_autorizzazioni_interm");
		if(isset($ID_AZI)) {
			$FEDIT->FGE_SetFilter("ID_AZI",$ID_AZI,"user_impianti_intermediari");
		} else {
			$FEDIT->FGE_SetFilter("ID_AZI","###","user_impianti_intermediari");
		}
		$FEDIT->FGE_LookUpDone();
		$FEDIT->FGE_LookUpCFG("ID_AUTHI",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["IDrif"],"user_autorizzazioni_interm");
		if(isset($ID_UIMI)) {
			$FEDIT->FGE_SetFilter("ID_UIMI",$ID_UIMI,"user_autorizzazioni_interm");
		} else {
			$FEDIT->FGE_SetFilter("ID_UIMI","###","user_autorizzazioni_interm");
		}
		$FEDIT->FGE_LookUpDone();
	} else {
		$FEDIT->FGE_LookUpCFG("ID_AZI",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["IDrif"],"user_autorizzazioni_interm");
		$FEDIT->FGE_LookUpDone();
		$FEDIT->FGE_LookUpCFG("ID_UIMI",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["IDrif"],"user_autorizzazioni_interm");
		$FEDIT->FGE_LookUpDone();
		$FEDIT->FGE_LookUpCFG("ID_AUTHI",$TableName,false,true);
		$FEDIT->FGE_SetFilter("ID_RIF",$_GET["IDrif"],"user_autorizzazioni_interm");
		$FEDIT->FGE_LookUpDone();
	}
	//}}}





echo $reply;

//	} //main if
//else {
//	#
//	#	RIPRISTINA i FILTRI di DEFAULT
//	#
//	require("../__mov_snipplets/FLTR_MovimentoDefault.php");
//	if($_GET["Tipo"]=="S" | ($_GET["Tipo"]=="C" && $SOGER->UserData['workmode']!='produttore') ) {
//		$FEDIT->FGE_SetValue("ID_OP_RS",0,$TableName);
//		//$FEDIT->FGE_SetValue("ID_TRAT",0,$TableName);
//		echo "||||||||||||||0|0|0|0||||";
//		}
//	else {
//		echo "||||||";
//		}
//	}
?>
