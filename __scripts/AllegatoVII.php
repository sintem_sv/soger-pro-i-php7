<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__libs/SQLFunct.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");

global $SOGER;


# Verifico chi � l'organizzatore della spedizione
$sql ="SELECT ID_ORG FROM ".$_GET['table']." WHERE ".$_GET['pri']."=".$_GET['filter'].";";
$FEDIT->SDBRead($sql,"DbRecordSetORG_ID",true,false);
$ID_ORG = $FEDIT->DbRecordSetORG_ID[0]['ID_ORG'];

$sql ="SELECT N_ANNEX_VII, ";
if($ID_ORG==2)
	$sql.="ID_UIMI, ";
$sql.="REFERENTE_ORG AS ORG_REFERENTE, ";
$sql.="ID_UIMD, REFERENTE_DEST AS DEST_REFERENTE, ";
$sql.="(pesoN/1000) AS QTA_MG, (pesoN/FKEpesospecifico/1000) AS QTA_M3, ";
$sql.="DTMOV AS DATA_SPEDIZIONE, ";
$sql.="ID_UIMT, REFERENTE_TRASP AS TRASP_REFERENTE, ID_AUTO, ID_RMK, ";
$sql.="ID_UIMP, REFERENTE_GEN AS GEN_REFERENTE, ";
$sql.="ID_OP_RS, ID_RIF, ";
$sql.="ID_BASILEA, ID_OCSE, ID_ALL_IIIA, ID_ALL_IIIB ";
$sql.="FROM ".$_GET['table']." WHERE ".$_GET['pri']."=".$_GET['filter'].";";
$FEDIT->SDBRead($sql,"DbRecordSetGData",true,false);


# 0. ALLEGATO VII NUMERO:
$N_ANNEX_VII	= $FEDIT->DbRecordSetGData[0]['N_ANNEX_VII'];

# 1. Persona che organizza la spedizione
// 1  --> produttore
// 2  --> intermediario

if($ID_ORG==1){
	$sql ="SELECT user_aziende_produttori.description AS ORG_NOME, ";
	$sql.="CONCAT(user_impianti_produttori.description, ' - ', lov_comuni_istat.cap, ' - ', lov_comuni_istat.description, ' (', lov_comuni_istat.shdes_prov, ')') AS ORG_INDIRIZZO, ";
	$sql.="user_impianti_produttori.telefono AS ORG_TELEFONO, ";
	$sql.="user_impianti_produttori.fax AS ORG_FAX, ";
	$sql.="user_impianti_produttori.email AS ORG_EMAIL ";
	$sql.="FROM user_impianti_produttori ";
	$sql.="JOIN user_aziende_produttori ON user_aziende_produttori.ID_AZP=user_impianti_produttori.ID_AZP ";
	$sql.="JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM=user_impianti_produttori.ID_COM ";
	$sql.="WHERE user_impianti_produttori.ID_UIMP=".$FEDIT->DbRecordSetGData[0]['ID_UIMP'].";";
	$FEDIT->SDBRead($sql,"DbRecordSetORG",true,false);
	$ORG_NOME		= $FEDIT->DbRecordSetORG[0]['ORG_NOME'];
	$ORG_INDIRIZZO	= $FEDIT->DbRecordSetORG[0]['ORG_INDIRIZZO'];
	$ORG_REFERENTE	= $FEDIT->DbRecordSetGData[0]['ORG_REFERENTE'];
	$ORG_TELEFONO	= $FEDIT->DbRecordSetORG[0]['ORG_TELEFONO'];
	$ORG_FAX		= $FEDIT->DbRecordSetORG[0]['ORG_FAX'];
	$ORG_EMAIL		= $FEDIT->DbRecordSetORG[0]['ORG_EMAIL'];
	}
else{
	if($FEDIT->DbRecordSetGData[0]['ID_UIMI']!='0000000'){
		$sql ="SELECT user_aziende_intermediari.description AS ORG_NOME, ";
		$sql.="CONCAT(user_impianti_intermediari.description, ' - ', lov_comuni_istat.cap, ' - ', lov_comuni_istat.description, ' (', lov_comuni_istat.shdes_prov, ')') AS ORG_INDIRIZZO, ";
		$sql.="user_impianti_intermediari.telefono AS ORG_TELEFONO, ";
		$sql.="user_impianti_intermediari.fax AS ORG_FAX, ";
		$sql.="user_impianti_intermediari.email AS ORG_EMAIL ";
		$sql.="FROM user_impianti_intermediari ";
		$sql.="JOIN user_aziende_intermediari ON user_aziende_intermediari.ID_AZI=user_impianti_intermediari.ID_AZI ";
		$sql.="JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM=user_impianti_intermediari.ID_COM ";
		$sql.="WHERE user_impianti_intermediari.ID_UIMI=".$FEDIT->DbRecordSetGData[0]['ID_UIMI'].";";
		$FEDIT->SDBRead($sql,"DbRecordSetORG",true,false);
		$ORG_NOME		= $FEDIT->DbRecordSetORG[0]['ORG_NOME'];
		$ORG_INDIRIZZO	= $FEDIT->DbRecordSetORG[0]['ORG_INDIRIZZO'];
		$ORG_REFERENTE	= $FEDIT->DbRecordSetGData[0]['ORG_REFERENTE'];
		$ORG_TELEFONO	= $FEDIT->DbRecordSetORG[0]['ORG_TELEFONO'];
		$ORG_FAX		= $FEDIT->DbRecordSetORG[0]['ORG_FAX'];
		$ORG_EMAIL		= $FEDIT->DbRecordSetORG[0]['ORG_EMAIL'];
		}
	else{
		$ORG_NOME		= "";
		$ORG_INDIRIZZO	= "";
		$ORG_REFERENTE	= "";
		$ORG_TELEFONO	= "";
		$ORG_FAX		= "";
		$ORG_EMAIL		= "";
		}
	}


# 2. Importatore / Destinatario
$sql ="SELECT user_aziende_destinatari.description AS DEST_NOME, ";
$sql.="CONCAT(user_impianti_destinatari.description, ' - ', lov_comuni_istat.cap, ' - ', lov_comuni_istat.description, ' (', lov_comuni_istat.nazione, ')') AS DEST_INDIRIZZO, lov_comuni_istat.nazione AS DEST_NAZIONE, ";
$sql.="user_impianti_destinatari.telefono AS DEST_TELEFONO, ";
$sql.="user_impianti_destinatari.fax AS DEST_FAX, ";
$sql.="user_impianti_destinatari.email AS DEST_EMAIL ";
$sql.="FROM user_impianti_destinatari ";
$sql.="JOIN user_aziende_destinatari ON user_aziende_destinatari.ID_AZD=user_impianti_destinatari.ID_AZD ";
$sql.="JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM=user_impianti_destinatari.ID_COM ";
$sql.="WHERE user_impianti_destinatari.ID_UIMD=".$FEDIT->DbRecordSetGData[0]['ID_UIMD'].";";

$FEDIT->SDBRead($sql,"DbRecordSetDEST",true,false);
$DEST_NOME		= utf8_decode($FEDIT->DbRecordSetDEST[0]['DEST_NOME']);
$DEST_INDIRIZZO	= utf8_decode($FEDIT->DbRecordSetDEST[0]['DEST_INDIRIZZO']);
$DEST_NAZIONE	= utf8_decode($FEDIT->DbRecordSetDEST[0]['DEST_NAZIONE']);
$DEST_REFERENTE	= utf8_decode($FEDIT->DbRecordSetGData[0]['DEST_REFERENTE']);
$DEST_TELEFONO	= $FEDIT->DbRecordSetDEST[0]['DEST_TELEFONO'];
$DEST_FAX		= $FEDIT->DbRecordSetDEST[0]['DEST_FAX'];
$DEST_EMAIL		= $FEDIT->DbRecordSetDEST[0]['DEST_EMAIL'];

# 3. Quantitativo effettivo
$QTA_MG			= $FEDIT->DbRecordSetGData[0]['QTA_MG'];
$QTA_M3			= $FEDIT->DbRecordSetGData[0]['QTA_M3'];

# 4. Data effettiva della spedizione	
$DATA_SPEDIZIONE		= $FEDIT->DbRecordSetGData[0]['DATA_SPEDIZIONE'];

# 5. Primo vettore
$sql ="SELECT user_aziende_trasportatori.description AS TRASP_NOME, ";
$sql.="CONCAT(user_impianti_trasportatori.description, ' - ', lov_comuni_istat.cap, ' - ', lov_comuni_istat.description, ' (', lov_comuni_istat.shdes_prov, ')') AS TRASP_INDIRIZZO, ";
$sql.="user_impianti_trasportatori.telefono AS TRASP_TELEFONO, ";
$sql.="user_impianti_trasportatori.fax AS TRASP_FAX, ";
$sql.="user_impianti_trasportatori.email AS TRASP_EMAIL ";
$sql.="FROM user_impianti_trasportatori ";
$sql.="JOIN user_aziende_trasportatori ON user_aziende_trasportatori.ID_AZT=user_impianti_trasportatori.ID_AZT ";
$sql.="JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM=user_impianti_trasportatori.ID_COM ";
$sql.="WHERE user_impianti_trasportatori.ID_UIMT=".$FEDIT->DbRecordSetGData[0]['ID_UIMT'].";";

$FEDIT->SDBRead($sql,"DbRecordSetTRASP",true,false);
$TRASP_NOME				= utf8_decode($FEDIT->DbRecordSetTRASP[0]['TRASP_NOME']);
$TRASP_INDIRIZZO		= utf8_decode($FEDIT->DbRecordSetTRASP[0]['TRASP_INDIRIZZO']);
$TRASP_REFERENTE		= utf8_decode($FEDIT->DbRecordSetGData[0]['TRASP_REFERENTE']);
$TRASP_TELEFONO			= $FEDIT->DbRecordSetTRASP[0]['TRASP_TELEFONO'];
$TRASP_FAX				= $FEDIT->DbRecordSetTRASP[0]['TRASP_FAX'];
$TRASP_EMAIL			= $FEDIT->DbRecordSetTRASP[0]['TRASP_EMAIL'];

if(!is_null($FEDIT->DbRecordSetGData[0]['ID_AUTO']) AND $FEDIT->DbRecordSetGData[0]['ID_AUTO']!='00'){
	$sql ="SELECT description FROM user_automezzi WHERE ID_AUTO=".$FEDIT->DbRecordSetGData[0]['ID_AUTO'].";";
	$FEDIT->SDBRead($sql,"DbRecordSetAuto",true,false);
	$Automezzo = $FEDIT->DbRecordSetAuto[0]['description'];
	}
else $Automezzo = "";

if(!is_null($FEDIT->DbRecordSetGData[0]['ID_RMK']) AND $FEDIT->DbRecordSetGData[0]['ID_RMK']!='00'){
	$sql ="SELECT description FROM user_rimorchi WHERE ID_RMK=".$FEDIT->DbRecordSetGData[0]['ID_RMK'].";";
	$FEDIT->SDBRead($sql,"DbRecordSetRimorchio",true,false);
	$Rimorchio = $FEDIT->DbRecordSetRimorchio[0]['description'];
	}
else $Rimorchio = "";

$TRASP_MEZZO			= $Automezzo . '   ' . $Rimorchio;


# 6. Generatore dei rifiuti
if($ID_ORG==1){
	$GEN_NOME		= $ORG_NOME;
	$GEN_INDIRIZZO	= $ORG_INDIRIZZO;
	$GEN_REFERENTE	= $ORG_REFERENTE;
	$GEN_TELEFONO	= $ORG_TELEFONO;
	$GEN_FAX		= $ORG_FAX;
	$GEN_EMAIL		= $ORG_EMAIL;
	}
else{
	$sql ="SELECT user_aziende_produttori.description AS GEN_NOME, ";
	$sql.="CONCAT(user_impianti_produttori.description, ' - ', lov_comuni_istat.cap, ' - ', lov_comuni_istat.description, ' (', lov_comuni_istat.shdes_prov, ')') AS GEN_INDIRIZZO, ";
	$sql.="user_impianti_produttori.telefono AS GEN_TELEFONO, ";
	$sql.="user_impianti_produttori.fax AS GEN_FAX, ";
	$sql.="user_impianti_produttori.email AS GEN_EMAIL ";
	$sql.="FROM user_impianti_produttori ";
	$sql.="JOIN user_aziende_produttori ON user_aziende_produttori.ID_AZP=user_impianti_produttori.ID_AZP ";
	$sql.="JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM=user_impianti_produttori.ID_COM ";
	$sql.="WHERE user_impianti_produttori.ID_UIMP=".$FEDIT->DbRecordSetGData[0]['ID_UIMP'].";";
	$FEDIT->SDBRead($sql,"DbRecordSetGEN",true,false);
	$GEN_NOME		= $FEDIT->DbRecordSetGEN[0]['GEN_NOME'];
	$GEN_INDIRIZZO	= $FEDIT->DbRecordSetGEN[0]['GEN_INDIRIZZO'];
	$GEN_REFERENTE	= $FEDIT->DbRecordSetGData[0]['GEN_REFERENTE'];
	$GEN_TELEFONO	= $FEDIT->DbRecordSetGEN[0]['GEN_TELEFONO'];
	$GEN_FAX		= $FEDIT->DbRecordSetGEN[0]['GEN_FAX'];
	$GEN_EMAIL		= $FEDIT->DbRecordSetGEN[0]['GEN_EMAIL'];
	}

# 8. Operazione di recupero
$sql ="SELECT description FROM lov_operazioni_rs WHERE ID_OP_RS = ".$FEDIT->DbRecordSetGData[0]['ID_OP_RS'].";";
$FEDIT->SDBRead($sql,"DbRecordSetOP_RS",true,false);
$OP_RS = $FEDIT->DbRecordSetOP_RS[0]['description'];


# 9. Denominazione abituale dei rifiuti
$sql ="SELECT lov_cer.COD_CER, lov_cer.description ";
$sql.="FROM user_schede_rifiuti JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
$sql.="WHERE ID_RIF=".$FEDIT->DbRecordSetGData[0]['ID_RIF'].";";
$FEDIT->SDBRead($sql,"DbRecordSetRIF",true,false);
$DENOMINAZIONE_RIFIUTO = $FEDIT->DbRecordSetRIF[0]['COD_CER'] . " - " . $FEDIT->DbRecordSetRIF[0]['description'];


# 10. Identificazione dei rifiuti
$sql ="SELECT code FROM lov_annex_vii_basilea WHERE ID_BASILEA = ".$FEDIT->DbRecordSetGData[0]['ID_BASILEA'].";";
$FEDIT->SDBRead($sql,"DbRecordSetBASILEA",true,false);
if(isset($FEDIT->DbRecordSetBASILEA[0]['code']))
	$BASILEA = $FEDIT->DbRecordSetBASILEA[0]['code'];
else
	$BASILEA = "";

$sql ="SELECT code FROM lov_annex_vii_ocse WHERE ID_OCSE = ".$FEDIT->DbRecordSetGData[0]['ID_OCSE'].";";
$FEDIT->SDBRead($sql,"DbRecordSetOCSE",true,false);
if(isset($FEDIT->DbRecordSetOCSE[0]['code']))
	$OCSE = $FEDIT->DbRecordSetOCSE[0]['code'];
else
	$OCSE = "";

$sql ="SELECT code FROM lov_annex_vii_iiia WHERE ID_ALL_IIIA = ".$FEDIT->DbRecordSetGData[0]['ID_ALL_IIIA'].";";
$FEDIT->SDBRead($sql,"DbRecordSetALL_IIIA",true,false);
if(isset($FEDIT->DbRecordSetALL_IIIA[0]['code']))
	$ALL_IIIA = $FEDIT->DbRecordSetALL_IIIA[0]['code'];
else
	$ALL_IIIA = "";

$sql ="SELECT code FROM lov_annex_vii_iiib WHERE ID_ALL_IIIB = ".$FEDIT->DbRecordSetGData[0]['ID_ALL_IIIB'].";";
$FEDIT->SDBRead($sql,"DbRecordSetALL_IIIB",true,false);
if(isset($FEDIT->DbRecordSetALL_IIIB[0]['code']))
	$ALL_IIIB = $FEDIT->DbRecordSetALL_IIIB[0]['code'];
else
	$ALL_IIIB = "";

$CODICE_CER = $FEDIT->DbRecordSetRIF[0]['COD_CER'];

# data spedizione #
$DataSpedizione			= explode(" ",$DATA_SPEDIZIONE);
$DataSpedizioneTrunc	= explode("-",$DataSpedizione[0]);
$anno					= $DataSpedizioneTrunc[0];
$mese					= $DataSpedizioneTrunc[1];
$giorno					= $DataSpedizioneTrunc[2];
$DATA_SPEDIZIONE		= $giorno."/".$mese."/".$anno;

$ReplacedWith = array();

$ToBeReplaced = array(
	0=>"[ORG_NOME]",
	1=>"[ORG_INDIRIZZO]",
	2=>"[ORG_REFERENTE]",
	3=>"[ORG_TELEFONO]",
	4=>"[ORG_FAX]",
	5=>"[ORG_EMAIL]",
	6=>"[DEST_NOME]",
	7=>"[DEST_INDIRIZZO]",
	8=>"[DEST_REFERENTE]",
	9=>"[DEST_TELEFONO]",
	10=>"[DEST_FAX]",
	11=>"[DEST_EMAIL]",
	12=>"[QTA_MG]",
	13=>"[QTA_M3]",
	14=>"[DATA_SPEDIZIONE]",
	15=>"[TRASP_NOME]",
	16=>"[TRASP_INDIRIZZO]",
	17=>"[TRASP_REFERENTE]",
	18=>"[TRASP_TELEFONO]",
	19=>"[TRASP_FAX]",
	20=>"[TRASP_EMAIL]",
	21=>"[TRASP_MEZZO]",
	22=>"[DATA_SPEDIZIONE]",
	23=>"[GEN_NOME]",
	24=>"[GEN_INDIRIZZO]",
	25=>"[GEN_REFERENTE]",
	26=>"[GEN_TELEFONO]",
	27=>"[GEN_FAX]",
	28=>"[GEN_EMAIL]",
	29=>"[OP_RS]",
	30=>"[DENOMINAZIONE_RIFIUTO]",
	31=>"[DEST_NOME]",
	32=>"[DEST_INDIRIZZO]",
	33=>"[DEST_REFERENTE]",
	34=>"[DEST_TELEFONO]",
	35=>"[DEST_FAX]",
	36=>"[DEST_EMAIL]",
	37=>"[BASILEA]",
	38=>"[OCSE]",
	39=>"[ALL_IIIA]",
	40=>"[ALL_IIIB]",
	41=>"[CODICE_CER]",
	42=>"[CODICE_CER]",
	43=>"[N_ANNEX_VII]",
	44=>"[ORG_REFERENTE]",
	45=>"[DATA_SPEDIZIONE]",
	46=>"[DEST_REFERENTE]",
	47=>"[DEST_NAZIONE]"
	);

$ReplacedWith[0] = $ORG_NOME;
$ReplacedWith[1] = $ORG_INDIRIZZO;
$ReplacedWith[2] = $ORG_REFERENTE;
$ReplacedWith[3] = $ORG_TELEFONO;
$ReplacedWith[4] = $ORG_FAX;
$ReplacedWith[5] = $ORG_EMAIL;

$ReplacedWith[6] = $DEST_NOME;
$ReplacedWith[7] = $DEST_INDIRIZZO;
$ReplacedWith[8] = $DEST_REFERENTE;
$ReplacedWith[9] = $DEST_TELEFONO;
$ReplacedWith[10] = $DEST_FAX;
$ReplacedWith[11] = $DEST_EMAIL;

$ReplacedWith[12] = $QTA_MG;
$ReplacedWith[13] = $QTA_M3;

$ReplacedWith[14] = $DATA_SPEDIZIONE;

$ReplacedWith[15] = $TRASP_NOME;
$ReplacedWith[16] = $TRASP_INDIRIZZO;
$ReplacedWith[17] = $TRASP_REFERENTE;
$ReplacedWith[18] = $TRASP_TELEFONO;
$ReplacedWith[19] = $TRASP_FAX;
$ReplacedWith[20] = $TRASP_EMAIL;
$ReplacedWith[21] = $TRASP_MEZZO;

$ReplacedWith[22] = $DATA_SPEDIZIONE;

$ReplacedWith[23] = $GEN_NOME;
$ReplacedWith[24] = $GEN_INDIRIZZO;
$ReplacedWith[25] = $GEN_REFERENTE;
$ReplacedWith[26] = $GEN_TELEFONO;
$ReplacedWith[27] = $GEN_FAX;
$ReplacedWith[28] = $GEN_EMAIL;

$ReplacedWith[29] = $OP_RS;

$ReplacedWith[30] = $DENOMINAZIONE_RIFIUTO;

$ReplacedWith[31] = $DEST_NOME;
$ReplacedWith[32] = $DEST_INDIRIZZO;
$ReplacedWith[33] = $DEST_REFERENTE;
$ReplacedWith[34] = $DEST_TELEFONO;
$ReplacedWith[35] = $DEST_FAX;
$ReplacedWith[36] = $DEST_EMAIL;

$ReplacedWith[37] = $BASILEA;
$ReplacedWith[38] = $OCSE;
$ReplacedWith[39] = $ALL_IIIA;
$ReplacedWith[40] = $ALL_IIIB;

$ReplacedWith[41] = $CODICE_CER;
$ReplacedWith[42] = $CODICE_CER;

$ReplacedWith[43] = $N_ANNEX_VII;

$ReplacedWith[44] = $ORG_REFERENTE;
$ReplacedWith[45] = $DATA_SPEDIZIONE;

$ReplacedWith[46] = $DEST_REFERENTE;

$ReplacedWith[47] = $DEST_NAZIONE;


$FileName = "ALLEGATO_VII.rtf";
$template = "ALLEGATO_VII.rtf";

StreamOut($template,$FileName,$ToBeReplaced,$ReplacedWith);

require_once("../__includes/COMMON_sleepForgEdit.php");
?>