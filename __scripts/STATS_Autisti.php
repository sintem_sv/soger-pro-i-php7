<?php

$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(40,4,"AUTISTA",0,"L");
$FEDIT->FGE_PdfBuffer->SetXY(60,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(60,4,"AZIENDA",0,"L");
$FEDIT->FGE_PdfBuffer->SetXY(120,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(30,3,"PATENTE",0,"C");
$FEDIT->FGE_PdfBuffer->SetXY(150,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(25,3,"RILASCIO",0,"C");
$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
$FEDIT->FGE_PdfBuffer->MultiCell(25,3,"SCADENZA",0,"C");
//$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
//$FEDIT->FGE_PdfBuffer->MultiCell(10,3,"ADR",0,"C");	


if(isset($FEDIT->DbRecordSet)){
	$autisti=count($FEDIT->DbRecordSet);
	for($a=0;$a<$autisti;$a++){
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,4,$FEDIT->DbRecordSet[$a]['COGNOME']." ".$FEDIT->DbRecordSet[$a]['NOME'],0,"L");
		$FEDIT->FGE_PdfBuffer->SetXY(60,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(60,4,$FEDIT->DbRecordSet[$a]['AZIENDA'],0,"L");
		$FEDIT->FGE_PdfBuffer->SetXY(120,$Ypos);
		if($FEDIT->DbRecordSet[$a]['PATENTE']=="") $patente=" no "; else $patente=$FEDIT->DbRecordSet[$a]['PATENTE'];
		$FEDIT->FGE_PdfBuffer->MultiCell(30,3,$patente,0,"C");
		$FEDIT->FGE_PdfBuffer->SetXY(150,$Ypos);
		if(is_null($FEDIT->DbRecordSet[$a]['RILASCIO']) || $FEDIT->DbRecordSet[$a]['RILASCIO']=="0000-00-00") $rilascio=" -- "; else $rilascio=date("d/m/Y",strtotime($FEDIT->DbRecordSet[$a]['RILASCIO']));
		$FEDIT->FGE_PdfBuffer->MultiCell(25,3,$rilascio,0,"C");
		$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
		if(is_null($FEDIT->DbRecordSet[$a]['SCADENZA']) || $FEDIT->DbRecordSet[$a]['SCADENZA']=="0000-00-00") $scadenza=" -- "; else $scadenza=date("d/m/Y",strtotime($FEDIT->DbRecordSet[$a]['SCADENZA']));
		$FEDIT->FGE_PdfBuffer->MultiCell(25,3,$scadenza,0,"C");
		$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
		//if($FEDIT->DbRecordSet[$a]['ADR']==0) $adr="no"; else $adr="s�";
		//$FEDIT->FGE_PdfBuffer->MultiCell(10,3,$adr,0,"C");
		}
	}
else{
	$FEDIT->FGE_PdfBuffer->MultiCell(270,3,"nessun autista in archivio",0,"C");
	}


?>