<?php

	$TotC = 0;
	$TotS = 0;
	$RIFbuf = "";
	$counter = 0;
	$Ypos = 33;
	$NPfound = false;
	$Pfound = false;
	$rifiuti = array();
	$CarichiRifiuto = array();

	//print_r($FEDIT->DbRecordSet);

	for($i=0;$i<count($FEDIT->DbRecordSet);$i++){
		$indice=count($rifiuti);
		if($FEDIT->DbRecordSet[$i]['RifID']!=$RIFbuf) {	
			$rifiuti[$indice]['totC']=$FEDIT->DbRecordSet[$i]['giac_ini'];
			$rifiuti[$indice]['COD_CER']=$FEDIT->DbRecordSet[$i]['COD_CER'];
			$rifiuti[$indice]['CERref']=$FEDIT->DbRecordSet[$i]['CERref'];
			$rifiuti[$indice]['RifID']=$FEDIT->DbRecordSet[$i]['RifID'];
			$rifiuti[$indice]['giac_ini']=$FEDIT->DbRecordSet[$i]['giac_ini'];
			$rifiuti[$indice]['cod_pro']=$FEDIT->DbRecordSet[$i]['cod_pro'];
			$rifiuti[$indice]['descrizione']=$FEDIT->DbRecordSet[$i]['descrizione'];
			$rifiuti[$indice]['MaxVar']=$FEDIT->DbRecordSet[$i]['MaxVar'];
			$rifiuti[$indice]['QLIM']=$FEDIT->DbRecordSet[$i]['q_limite'];
			$rifiuti[$indice]['TLIM']=$FEDIT->DbRecordSet[$i]['t_limite'];
			$rifiuti[$indice]['printDate']=$FEDIT->DbRecordSet[$i]['et_last_print'];
			$rifiuti[$indice]['componenti_per']=$FEDIT->DbRecordSet[$i]['et_comp_per'];
			$rifiuti[$indice]['ADR']=$FEDIT->DbRecordSet[$i]['adr'];
			$rifiuti[$indice]['ONU']=$FEDIT->DbRecordSet[$i]['ONU'];
			$rifiuti[$indice]['UM']=$FEDIT->DbRecordSet[$i]['UnMis'];
			$rifiuti[$indice]['IDUM']=$FEDIT->DbRecordSet[$i]['ID_UMIS'];
			$rifiuti[$indice]['PSPEC']=$FEDIT->DbRecordSet[$i]['peso_spec'];
			$rifiuti[$indice]['PER']=$FEDIT->DbRecordSet[$i]['PER'];
			$rifiuti[$indice]['H1']=$FEDIT->DbRecordSet[$i]['H1'];
			$rifiuti[$indice]['H2']=$FEDIT->DbRecordSet[$i]['H2'];
			$rifiuti[$indice]['H3A']=$FEDIT->DbRecordSet[$i]['H3A'];
			$rifiuti[$indice]['H3B']=$FEDIT->DbRecordSet[$i]['H3B'];
			$rifiuti[$indice]['H4']=$FEDIT->DbRecordSet[$i]['H4'];
			$rifiuti[$indice]['H5']=$FEDIT->DbRecordSet[$i]['H5'];
			$rifiuti[$indice]['H6']=$FEDIT->DbRecordSet[$i]['H6'];
			$rifiuti[$indice]['H7']=$FEDIT->DbRecordSet[$i]['H7'];
			$rifiuti[$indice]['H8']=$FEDIT->DbRecordSet[$i]['H8'];
			$rifiuti[$indice]['H9']=$FEDIT->DbRecordSet[$i]['H9'];
			$rifiuti[$indice]['H10']=$FEDIT->DbRecordSet[$i]['H10'];
			$rifiuti[$indice]['H11']=$FEDIT->DbRecordSet[$i]['H11'];
			$rifiuti[$indice]['H12']=$FEDIT->DbRecordSet[$i]['H12'];
			$rifiuti[$indice]['H13']=$FEDIT->DbRecordSet[$i]['H13'];
			$rifiuti[$indice]['H14']=$FEDIT->DbRecordSet[$i]['H14'];
			$rifiuti[$indice]['H15']=$FEDIT->DbRecordSet[$i]['H15'];
			if($FEDIT->DbRecordSet[$i]['TIPO']=="C"){
				$rifiuti[$indice]['carico'][0]['qta']=$FEDIT->DbRecordSet[$i]['quantita'];
				$rifiuti[$indice]['carico'][0]['num']=$FEDIT->DbRecordSet[$i]['NMOV'];
				$rifiuti[$indice]['carico'][0]['data']=$FEDIT->DbRecordSet[$i]['DTMOV'];
				$rifiuti[$indice]['totC']+=$FEDIT->DbRecordSet[$i]['quantita'];
				}
			if($FEDIT->DbRecordSet[$i]['TIPO']=="S")
				@$rifiuti[$indice]['totS']=$FEDIT->DbRecordSet[$i]['quantita'];
			}
		else{
			# somme
			$countCarichi=count($rifiuti[$indice-1]['carico']);
			if($FEDIT->DbRecordSet[$i]['TIPO']=="C"){
				$rifiuti[$indice-1]['carico'][$countCarichi]['qta']=$FEDIT->DbRecordSet[$i]['quantita'];
				$rifiuti[$indice-1]['carico'][$countCarichi]['num']=$FEDIT->DbRecordSet[$i]['NMOV'];
				$rifiuti[$indice-1]['carico'][$countCarichi]['data']=$FEDIT->DbRecordSet[$i]['DTMOV'];
				$rifiuti[$indice-1]['totC']+=$FEDIT->DbRecordSet[$i]['quantita'];
				}
			if($FEDIT->DbRecordSet[$i]['TIPO']=="S")
				@$rifiuti[$indice-1]['totS']+=$FEDIT->DbRecordSet[$i]['quantita'];
			}
		$RIFbuf=$FEDIT->DbRecordSet[$i]['RifID'];
		}

	for($r=0;$r<count($rifiuti);$r++){
		$string=CheckCarichi($rifiuti[$r], $CarichiRifiuto);
		$result=explode("|",$string);
		$rifiuti[$r]['NMOV']=$result[0];
		$rifiuti[$r]['DTMOV']=$result[1];
		$rifiuti[$r]['quantita']=$result[2];
		}

	//print_r($rifiuti);

	$nper=false;
	$s=0;
	while($s<count($rifiuti) && !$nper){ 
		if($rifiuti[$s]['PER']=="0") $nper=true;
		$s++;
		}

	$per=false;
	$s=0;
	while($s<count($rifiuti) && !$per){ 
		if($rifiuti[$s]['PER']=="1") $per=true;
		$s++;
		}


	if($per){
		# stampo intestazione P
		$Ypos += 20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(75,7,"Elenco CER Classificati Pericolosi",1,"L");
		$Ypos += 10;
		$pericolo='p';
		Intestazione($FEDIT->FGE_PdfBuffer,$Ypos,$pericolo);

		# stampo rifiuti P
		for($s=0;$s<count($rifiuti);$s++){
			if($rifiuti[$s]['PER']=="1")
				StampaRifiuto($rifiuti[$s],$FEDIT->FGE_PdfBuffer,$Ypos);
			}
		}

	if($nper){
		# stampo intestazione NP
		$Ypos += 20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(75,7,"Elenco CER Classificati Non Pericolosi",1,"L");
		$Ypos += 10;
		$pericolo='np';
		Intestazione($FEDIT->FGE_PdfBuffer,$Ypos,$pericolo);

		# stampo rifiuti NP
		for($s=0;$s<count($rifiuti);$s++){
			if($rifiuti[$s]['PER']=="0")
				StampaRifiuto($rifiuti[$s],$FEDIT->FGE_PdfBuffer,$Ypos);
			}
		}


function StampaRifiuto($dati, &$PdfObj, &$Ypos){

	CheckYPos($Ypos,$PdfObj);
	$PdfObj->SetFont('Helvetica','',8);
	$PdfObj->SetXY(10,$Ypos);
	$PdfObj->MultiCell(15,5,$dati["COD_CER"],0,"C");
	$PdfObj->SetXY(23.5,$Ypos+0.6);
	if(strlen($dati["descrizione"])>120) {
		$PdfObj->MultiCell(55,3.5,substr($dati["descrizione"],0,-25)." [..]",0,"L");
	} else {
		$PdfObj->MultiCell(55,3.5,$dati["descrizione"],0,"L");
	}
	
	
	#max var %
	$PdfObj->SetXY(80,$Ypos);
	if($dati["MaxVar"]!="")
		$PdfObj->MultiCell(17,5,$dati["MaxVar"]."%",0,"C");
	else
		$PdfObj->MultiCell(17,5,"n.d.",0,"C");

	#adr
	$PdfObj->SetXY(97,$Ypos);
	if($dati["ADR"]=="1") $dati["ADR"]="si"; else $dati["ADR"]="no";
	$PdfObj->MultiCell(13,5,$dati["ADR"],0,"C");
	
	#onu
	$PdfObj->SetXY(110,$Ypos);
	$PdfObj->MultiCell(13,5,$dati["ONU"],0,"C");

	#stoc. max
	$PdfObj->SetXY(123,$Ypos);
	if($dati["QLIM"]!="0" && !is_null($dati["QLIM"])) {
		$PdfObj->MultiCell(17,5,$dati["QLIM"] . " Kg.",0,"C");
	} else {
		$PdfObj->MultiCell(17,5,$dati["TLIM"] . " gg",0,"C");
	}


	switch($dati["NMOV"]){
		case "":  //nulla da scaricare
			//$PdfObj->SetXY(110,$Ypos);
			//$PdfObj->MultiCell(23,3.5,"nessuno",0,"C");
			$ggGiacenza="0";
			break;
		case "0": //movimentato con giac ini da scaricare
			//$PdfObj->SetXY(110,$Ypos);
			//$PdfObj->MultiCell(23,3.5,"Giacenza iniziale",0,"C");
			if($dati["DTMOV"]=="0000-00-00") $dati["DTMOV"]=date('Y')."-01-01";
			$ggGiacenza=dateDiff("d", $dati["DTMOV"], date('Y-m-d'));
			break;
		default:  //movimentato con carichi da scaricare
			//$PdfObj->SetXY(110,$Ypos);
			//$PdfObj->MultiCell(23,3.5,"Mov.#" . $dati["NMOV"] . "\ndel " . date("d/m/y",strtotime($dati["DTMOV"])),0,"C");
			$ggGiacenza=dateDiff("d", $dati["DTMOV"], date('Y-m-d'));
			break;
		}

	# gg giacenza
	$PdfObj->SetXY(140,$Ypos);
	$PdfObj->MultiCell(15,5,$ggGiacenza,0,"C");
	
	# stoccaggio
	$Qtot=$dati['quantita']; 
	$PdfObj->SetXY(155,$Ypos);
	$PdfObj->MultiCell(20,5,round($Qtot,1)." ".$dati["UM"],"","C");

	# classi h
	$PdfObj->SetXY(175,$Ypos);
	if($dati["H1"]=="1")  $ClassiH.="H1 ";
	if($dati["H2"]=="1")  $ClassiH.="H2 ";
	if($dati["H3A"]=="1") $ClassiH.="H3A ";
	if($dati["H3B"]=="1") $ClassiH.="H3B ";
	if($dati["H4"]=="1")  $ClassiH.="H4 ";
	if($dati["H5"]=="1")  $ClassiH.="H5 ";
	if($dati["H6"]=="1")  $ClassiH.="H6 ";
	if($dati["H7"]=="1")  $ClassiH.="H7 ";
	if($dati["H8"]=="1")  $ClassiH.="H8 ";
	if($dati["H9"]=="1")  $ClassiH.="H9 ";
	if($dati["H10"]=="1") $ClassiH.="H10 ";
	if($dati["H11"]=="1") $ClassiH.="H11 ";
	if($dati["H12"]=="1") $ClassiH.="H12 ";
	if($dati["H13"]=="1") $ClassiH.="H13 ";
	if($dati["H14"]=="1") $ClassiH.="H14 ";
	if($dati["H15"]=="1") $ClassiH.="H15 ";
	$PdfObj->MultiCell(30,5,$ClassiH,"","L");

	# etichetta: com. pericolosi
	$PdfObj->SetXY(205,$Ypos);
	$PdfObj->MultiCell(50,5,$dati["componenti_per"],"","C");

	# etichetta: stampata?
	$PdfObj->SetXY(255,$Ypos);
	if($dati["printDate"]=="") $dati["printDate"]="no"; else $dati["printDate"]="si";
	$PdfObj->MultiCell(25,5,$dati["printDate"],"","C");



	$Ypos +=12;		
	}

function YN($in) {
	if($in=="1") {
		return "si";
	} else {
		return "no";	
	}
}

function YNimg($in) {
	if($in=="1") {
		return "../__css/CHECKY.png";
	} else {
		return "../__css/CHECKN.png";
	}
}

function Intestazione(&$PdfObj,&$Ypos,&$pericolo) {
	CheckYPos($Ypos,$PdfObj);
	$PdfObj->SetFont('Helvetica','B',8);
	$PdfObj->SetXY(10,$Ypos);
	$PdfObj->MultiCell(15,5,"Cer",0,"C");
	$PdfObj->SetXY(25,$Ypos);
	$PdfObj->MultiCell(55,5,"Descrizione",0,"C");
	$PdfObj->SetXY(80,$Ypos);
	$PdfObj->MultiCell(17,3.5,"Scarto massimo",0,"C");
	$PdfObj->SetXY(97,$Ypos);
	$PdfObj->MultiCell(13,5,"ADR",0,"C");
	$PdfObj->SetXY(110,$Ypos);
	$PdfObj->MultiCell(13,5,"ONU",0,"C");
	$PdfObj->SetXY(123,$Ypos);
	$PdfObj->MultiCell(17,5,"Stoc. Max",0,"C");
	$PdfObj->SetXY(140,$Ypos);
	$PdfObj->MultiCell(15,3.5,"Giorni giacenza",0,"C");
	$PdfObj->SetXY(155,$Ypos);
	$PdfObj->MultiCell(20,5,"Stoccaggio","","C");
	$PdfObj->SetXY(175,$Ypos);
	$PdfObj->MultiCell(30,3.5,"Classi di pericolo","","C");
	$PdfObj->SetXY(205,$Ypos);
	$PdfObj->MultiCell(50,3.5,"Etichetta: comp. pericolosi","","C");
	$PdfObj->SetXY(255,$Ypos);
	$PdfObj->MultiCell(25,3.5,"Stampa etichetta","","C");

	$Ypos +=12;
}

?>