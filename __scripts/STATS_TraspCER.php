<?php

$Ypos = 80;
$Xpos = 20;
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);


if(!isset($FEDIT->DbRecordSet)){
	$FEDIT->FGE_PdfBuffer->SetXY(70,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(70,4,"NESSUN RECORD",0,"C");
	}
else{
	//print_r($FEDIT->DbRecordSet);
	$rifiuti=array();
	$rifTMP=array();
	$trasportatori=array();
	$trsTMP=array();
	$automezzi=array();
	$autoTMP=array();
	$rimorchi=array();
	$rimTMP=array();


	#array rifiuti
	for($r=0;$r<count($FEDIT->DbRecordSet);$r++){
		if($r==0){
			$rifTMP[0]=$FEDIT->DbRecordSet[0]['descrizione'];
			$rifiuti[0]['cer']=$FEDIT->DbRecordSet[0]['CER'];
			$rifiuti[0]['descrizione']=$FEDIT->DbRecordSet[0]['descrizione'];
			
			$rifiuti[0]['trsTMP'][]=$FEDIT->DbRecordSet[0]['trasportatore'];
			$rifiuti[0]['trasportatore'][0]['azienda']=$FEDIT->DbRecordSet[0]['trasportatore'];
			$rifiuti[0]['trasportatore'][0]['indirizzo']=$FEDIT->DbRecordSet[0]['indirizzo']." - ".$FEDIT->DbRecordSet[0]['comune']." ( ".$FEDIT->DbRecordSet[0]['provincia']." ) - CAP ".$FEDIT->DbRecordSet[0]['CAP'];
			$rifiuti[0]['trasportatore'][0]['numeroalbo']=$FEDIT->DbRecordSet[0]['num_aut'];
			$rifiuti[0]['trasportatore'][0]['rilascio']=$FEDIT->DbRecordSet[0]['rilascio'];
			$rifiuti[0]['trasportatore'][0]['scadenza']=$FEDIT->DbRecordSet[0]['scadenza'];

			$rifiuti[0]['trasportatore'][0]['autoTMP'][]=$FEDIT->DbRecordSet[0]['targa'];
			$rifiuti[0]['trasportatore'][0]['automezzi'][0]=$FEDIT->DbRecordSet[0]['targa'];

			$rifiuti[0]['trasportatore'][0]['rimTMP'][]=$FEDIT->DbRecordSet[0]['rimorchio'];
			$rifiuti[0]['trasportatore'][0]['rimorchio'][0]=$FEDIT->DbRecordSet[0]['rimorchio'];
			}
		else{
			$ti=count($rifiuti);
			$tit=count($rifiuti[$ti-1]['trsTMP']); 
			
			# rifiuto nuovo
			if(!in_array($FEDIT->DbRecordSet[$r]['descrizione'],$rifTMP)){
				$rifTMP[]=$FEDIT->DbRecordSet[$r]['descrizione'];
				$rifiuti[$ti]['cer']=$FEDIT->DbRecordSet[$r]['CER'];
				$rifiuti[$ti]['descrizione']=$FEDIT->DbRecordSet[$r]['descrizione'];
				
				$rifiuti[$ti]['trsTMP'][]=$FEDIT->DbRecordSet[$r]['trasportatore'];
				$rifiuti[$ti]['trasportatore'][0]['azienda']=$FEDIT->DbRecordSet[$r]['trasportatore'];
				$rifiuti[$ti]['trasportatore'][0]['indirizzo']=$FEDIT->DbRecordSet[$r]['indirizzo']." - ".$FEDIT->DbRecordSet[$r]['comune']." ( ".$FEDIT->DbRecordSet[$r]['provincia']." ) - CAP ".$FEDIT->DbRecordSet[$r]['CAP'];
				$rifiuti[$ti]['trasportatore'][0]['numeroalbo']=$FEDIT->DbRecordSet[$r]['num_aut'];
				$rifiuti[$ti]['trasportatore'][0]['rilascio']=$FEDIT->DbRecordSet[$r]['rilascio'];
				$rifiuti[$ti]['trasportatore'][0]['scadenza']=$FEDIT->DbRecordSet[$r]['scadenza'];
				
				$rifiuti[$ti]['trasportatore'][0]['autoTMP'][]=$FEDIT->DbRecordSet[$r]['targa'];
				$rifiuti[$ti]['trasportatore'][0]['automezzi'][0]=$FEDIT->DbRecordSet[$r]['targa'];

				$rifiuti[$ti]['trasportatore'][0]['rimTMP'][]=$FEDIT->DbRecordSet[$r]['rimorchio'];
				$rifiuti[$ti]['trasportatore'][0]['rimorchio'][0]=$FEDIT->DbRecordSet[$r]['rimorchio'];

				}
			else{ #il rifiuto � gi� in array, devo aggiungere trasportatore
				if(!in_array($FEDIT->DbRecordSet[$r]['trasportatore'],$rifiuti[$ti-1]['trsTMP'])){
					$rifiuti[$ti-1]['trsTMP'][$tit]=$FEDIT->DbRecordSet[$r]['trasportatore'];
					$rifiuti[$ti-1]['trasportatore'][$tit]['azienda']=$FEDIT->DbRecordSet[$r]['trasportatore'];
					$rifiuti[$ti-1]['trasportatore'][$tit]['indirizzo']=$FEDIT->DbRecordSet[$r]['indirizzo']." - ".$FEDIT->DbRecordSet[$r]['comune']." ( ".$FEDIT->DbRecordSet[$r]['provincia']." ) - CAP ".$FEDIT->DbRecordSet[$r]['CAP'];
					$rifiuti[$ti-1]['trasportatore'][$tit]['numeroalbo']=$FEDIT->DbRecordSet[$r]['num_aut'];
					$rifiuti[$ti-1]['trasportatore'][$tit]['rilascio']=$FEDIT->DbRecordSet[$r]['rilascio'];
					$rifiuti[$ti-1]['trasportatore'][$tit]['scadenza']=$FEDIT->DbRecordSet[$r]['scadenza'];
					
					$rifiuti[$ti-1]['trasportatore'][$tit]['autoTMP'][]=$FEDIT->DbRecordSet[$r]['targa'];
					$rifiuti[$ti-1]['trasportatore'][$tit]['automezzi'][0]=$FEDIT->DbRecordSet[$r]['targa'];

					$rifiuti[$ti-1]['trasportatore'][$tit]['rimTMP'][]=$FEDIT->DbRecordSet[$r]['rimorchio'];
					$rifiuti[$ti-1]['trasportatore'][$tit]['rimorchio'][0]=$FEDIT->DbRecordSet[$r]['rimorchio'];

					}
				else{ #trasportatore gi� in array, aggiungo automezzo / rimorchio
					$taut=count($rifiuti[$ti-1]['trasportatore'][$tit-1]['automezzi']);
					$trim=count($rifiuti[$ti-1]['trasportatore'][$tit-1]['rimorchio']);
					if(!in_array($FEDIT->DbRecordSet[$r]['targa'],$rifiuti[$ti-1]['trasportatore'][$tit-1]['autoTMP'])){
						$rifiuti[$ti-1]['trasportatore'][$tit-1]['autoTMP'][]=$FEDIT->DbRecordSet[$r]['targa'];
						$rifiuti[$ti-1]['trasportatore'][$tit-1]['automezzi'][$taut]=$FEDIT->DbRecordSet[$r]['targa'];
						}
					if(!in_array($FEDIT->DbRecordSet[$r]['rimorchio'],$rifiuti[$ti-1]['trasportatore'][$tit-1]['rimTMP'])){
						$rifiuti[$ti-1]['trasportatore'][$tit-1]['rimTMP'][]=$FEDIT->DbRecordSet[$r]['rimorchio'];
						$rifiuti[$ti-1]['trasportatore'][$tit-1]['rimorchio'][$trim]=$FEDIT->DbRecordSet[$r]['rimorchio'];
						}
					}
				}	
			}
		}
		//print_r($FEDIT->DbRecordSet);
		//print_r("<hr>");
		//print_r($rifiuti);
	}


$Ypos=(count($rifiuti)*13)+80;
for($p=0;$p<count($rifiuti);$p++){
	//CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	//$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$Ypos,0,"C");
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',12);
	$FEDIT->FGE_PdfBuffer->SetXY(0,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$rifiuti[$p]['cer']." - ".$rifiuti[$p]['descrizione'],0,"C");
	
	$Ypos+=10;
	
	for($t=0;$t<count($rifiuti[$p]['trasportatore']);$t++){

		#ragione sociale#
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','BU',10);
		$FEDIT->FGE_PdfBuffer->MultiCell(170,4,$rifiuti[$p]['trasportatore'][$t]['azienda'],0,"L");
		$Ypos+=3;

		#indirizzo#
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',5);
		$FEDIT->FGE_PdfBuffer->MultiCell(170,4,$rifiuti[$p]['trasportatore'][$t]['indirizzo'],0,"L");
		$Ypos+=2;

		#numero albo#
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(170,4,"Numero albo: ".$rifiuti[$p]['trasportatore'][$t]['numeroalbo']." del ".date("d/m/Y",strtotime($rifiuti[$p]['trasportatore'][$t]['rilascio']))." ( scad. ".date("d/m/Y",strtotime($rifiuti[$p]['trasportatore'][$t]['scadenza']))." )",0,"L");
		$Ypos+=10;
		

		#automezzi#
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->MultiCell(170,4,"Targhe automezzi autorizzati: ",0,"L");
		
		$r=0;
		
		$righe=count($rifiuti[$p]['trasportatore'][$t]['automezzi'])/4;
		$h=$righe*5;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$Xpos+=60;
		//$FEDIT->FGE_PdfBuffer->MultiCell(170,$h,"",1);
		for($a=0;$a<count($rifiuti[$p]['trasportatore'][$t]['automezzi']);$a++){
			
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
			$FEDIT->FGE_PdfBuffer->MultiCell(30,4,$rifiuti[$p]['trasportatore'][$t]['automezzi'][$a],0,"L");
			$Xpos+=30;
			$r++;
			if($r==4){
				$r=0;
				$Ypos+=5;
				$Xpos=80;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				}
			}

		$Xpos=20;
		$Ypos+=10;
		
		

		#rimorchi#
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->MultiCell(170,4,"Targhe rimorchi autorizzati: ",0,"L");
		
		$r=0;
		
		$righe=count($rifiuti[$p]['trasportatore'][$t]['rimorchio'])/4;
		$h=$righe*5;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$Xpos+=60;
		//$FEDIT->FGE_PdfBuffer->MultiCell(170,$h,"",1);

		for($a=0;$a<count($rifiuti[$p]['trasportatore'][$t]['rimorchio']);$a++){
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
			$FEDIT->FGE_PdfBuffer->MultiCell(30,4,$rifiuti[$p]['trasportatore'][$t]['rimorchio'][$a],0,"L");
			$Xpos+=30;
			$r++;
			if($r==4){
				$r=0;
				$Ypos+=5;
				$Xpos=80;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				}
			}
		$Xpos=20;
		$Ypos+=10;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		}
	}

?>