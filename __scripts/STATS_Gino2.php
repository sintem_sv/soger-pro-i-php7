<?php

session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/fpdf.php");
require_once("STATS_funct.php");
require_once("../__libs/SQLFunct.php");


$SQL ="SELECT substance.id AS ID_SUB, substance.denominazione_ita AS Sostanza, substance.cas AS CAS, substance.indice AS INDICE, substance.ce AS EINECS, ";
$SQL.="department.descr AS Reparto, product.code AS Matricola, j_sds_substance.perc AS Percentuale, product.um AS UnitaMisura, ";
$SQL.="product.unit_weight AS PesoUnitario, SUM(warehouse_register.quantity) AS Quantita ";
$SQL.="FROM substance ";
$SQL.="JOIN j_sds_substance ON j_sds_substance.id = substance.id ";
$SQL.="JOIN sds ON sds.id = j_sds_substance.sds_id ";
$SQL.="JOIN j_product_sds ON j_product_sds.id = sds.id ";
$SQL.="JOIN product ON product.id = j_product_sds.product_id ";
$SQL.="JOIN warehouse_register ON warehouse_register.product_id = product.id ";
$SQL.="JOIN department ON warehouse_register.department_id=department.id ";
$SQL.="WHERE department.plant_id=1 AND warehouse_register.date>='2013-03-01' ";
$SQL.="GROUP BY CONCAT(ID_SUB, Matricola) ";
$SQL.="ORDER BY Reparto ASC, Sostanza ASC ";

$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);

for($c=0;$c<count($FEDIT->DbRecordSet);$c++){

	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$c]['Sostanza']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$c]['CAS']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$c]['INDICE']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$c]['EINECS']);
	
	# CERCO SIMBOLI SOSTANZA
	$SQL ="SELECT data_symbol.code AS SIMBOLO FROM data_symbol JOIN j_subst_symbol ON data_symbol.id = j_subst_symbol.id WHERE j_subst_symbol.substance_id =".$FEDIT->DbRecordSet[$c]['ID_SUB'].";";
	$FEDIT->SdbRead($SQL,"DbRecordSetSimboli",true,false);
	$Simboli='';
	for($s=0;$s<count($FEDIT->DbRecordSetSimboli);$s++){
		$Simboli.=$FEDIT->DbRecordSetSimboli[$s]['SIMBOLO']." ";
		}
	$CSVbuf .= AddCSVcolumn($Simboli);

	# CERCO FRASI R SOSTANZA
	$SQL ="SELECT data_phrase_r.code AS FRASE FROM data_phrase_r JOIN j_subst_phraser ON data_phrase_r.id = j_subst_phraser.id WHERE j_subst_phraser.substance_id =".$FEDIT->DbRecordSet[$c]['ID_SUB'].";";
	$FEDIT->SdbRead($SQL,"DbRecordSetR",true,false);
	$FrasiR='';
	for($r=0;$r<count($FEDIT->DbRecordSetR);$r++){
		$FrasiR.=$FEDIT->DbRecordSetR[$r]['FRASE']." ";
		}
	$CSVbuf .= AddCSVcolumn($FrasiR);

	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$c]['Reparto']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$c]['Matricola']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$c]['Quantita']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$c]['UnitaMisura']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$c]['PesoUnitario']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$c]['Percentuale']);
	
	$CSVbuf .= AddCSVRow();

	}

CSVOut($CSVbuf,"REPORT_2");

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>