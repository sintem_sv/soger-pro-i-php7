<?php
session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/fpdf.php");
include("../__libs/SQLFunct.php");
require_once("STATS_funct.php");


$sql ="SELECT lov_cer.COD_CER, user_schede_rifiuti.descrizione, user_schede_rifiuti.pericoloso, ";
$sql.="user_schede_rifiuti_deposito.carico_automatico, user_schede_rifiuti.FKEdisponibilita as giacenza, ";
$sql.="user_schede_rifiuti_deposito.MaxStock, user_schede_rifiuti_deposito.NumCont, user_schede_rifiuti_deposito.ID_CONT, ";
$sql.="lov_misure.description as UM, lov_stato_fisico.description AS SF, lov_contenitori.description AS contenitore, ";
$sql.="lov_contenitori.portata, lov_contenitori.m_cubi, lov_contenitori.larghezza_img, lov_contenitori.altezza_img ";
$sql.="FROM user_schede_rifiuti ";
$sql.="JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
$sql.="JOIN lov_misure ON user_schede_rifiuti.ID_UMIS=lov_misure.ID_UMIS ";
$sql.="JOIN lov_stato_fisico ON user_schede_rifiuti.ID_SF=lov_stato_fisico.ID_SF ";
$sql.="JOIN user_schede_rifiuti_deposito ON user_schede_rifiuti.ID_RIF=user_schede_rifiuti_deposito.ID_RIF ";
$sql.="LEFT JOIN lov_contenitori ON user_schede_rifiuti_deposito.ID_CONT=lov_contenitori.ID_CONT ";
$sql.="WHERE user_schede_rifiuti.ID_RIF='".$_GET['ID_RIF']."'";

$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
$info=$FEDIT->DbRecordSet[0];

	$orientation="P";
	$statTitle = "STATO DEPOSITO RIFIUTO";
	$DcName = date("d/m/Y") . "--Stato_Deposito_Rifiuto";
	
	$PrintImpianto = true;
	$PrintCER = true;
	$SkipStandardNoRecs = true;

	$um="mm";
	$Format = array(210,297);
	$ZeroMargin = true;
	$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin,$DcName);

	## stats title
	$Ypos+=10;
	$FEDIT->FGE_PdfBuffer->SetXY(0,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',14);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,15,$statTitle,0,"C"); 
	
	
	$Xpos=20;


/*	## stat desc
	if(!isset($statDesc)) $statDesc=" -- ";
	$Xpos=20;
	$Ypos+=15;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Descrizione:",0,"L");
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(40,25);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$statDesc,0,"L");
*/
	## intestatario
	$intestatario = strtolower($SOGER->UserData["core_impiantidescription"]) . " (" . $SOGER->UserData["workmode"] . ")";
	$Ypos+=15;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Intestatario:",0,"L");
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(40,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$intestatario,0,"L");

	## impianto
	
	$queryImp = "SELECT indirizzo, lov_comuni_istat.description AS comune, shdes_prov FROM core_impianti JOIN lov_comuni_istat ON ";
	$queryImp.= "lov_comuni_istat.ID_COM = core_impianti.ID_COM ";
	$queryImp.= "WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."';";
	$FEDIT->SdbRead($queryImp,"DbRecordSetImpianto",true,false);

	$impianto = $FEDIT->DbRecordSetImpianto[0]['indirizzo']." - ".$FEDIT->DbRecordSetImpianto[0]['comune']." (".$FEDIT->DbRecordSetImpianto[0]['shdes_prov'].")";
	
	$Ypos+=7;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Impianto:",0,"L");
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY(40,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$impianto,0,"L");

	
	$Ypos+=20;

	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,6,"Scheda rifiuto: ","BTLR","L");

	$Xpos+=50;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(120,6,$info['COD_CER']." ".$info['descrizione'],"BTLR","L");
	
	if($info['MaxStock']>0) 
		$MAX=$info['MaxStock'];
	else{
		switch($info['UM']){
			case "Kg.":
				$MAX=$info['portata']*$info['NumCont'];
				break;
			case "Litri":
				$MAX=$info['m_cubi']*1000*$info['NumCont'];
				break;
			case "Mc.":
				$MAX=$info['m_cubi']*$info['NumCont'];
				break;
			}
		}

	$StockMax=$MAX*$info['NumCont'];

	# QTA IN ECCESSO?
	$Eccesso=0;	
	if($info['giacenza']>$StockMax)
		$Eccesso=$info['giacenza'] - $StockMax;


	if($info['giacenza']>0){
		$percentuale=$info['giacenza'] / $StockMax * 100;
		$percentuale=number_format($percentuale, 1, '.', '');
		}
	else
		$percentuale=0;

	$Xpos+=75;
	$Ypos+=6;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',30);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	if($percentuale>=90) $FEDIT->FGE_PdfBuffer->SetFillColor(253,99,71);
	if($percentuale>=60 && $percentuale<90) $FEDIT->FGE_PdfBuffer->SetFillColor(255,165,0);
	if($percentuale<60)  $FEDIT->FGE_PdfBuffer->SetFillColor(50,205,50);
	$FEDIT->FGE_PdfBuffer->MultiCell(45,33,$percentuale." %","TLR","C",1);

	$Ypos+=33;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',5);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	if($percentuale>=90) $FEDIT->FGE_PdfBuffer->SetFillColor(253,99,71);
	if($percentuale>=60 && $percentuale<90) $FEDIT->FGE_PdfBuffer->SetFillColor(255,165,0);
	if($percentuale<60)  $FEDIT->FGE_PdfBuffer->SetFillColor(50,205,50);
	$FEDIT->FGE_PdfBuffer->MultiCell(45,3,"% globale riempimento contenitori","BLR","C",1);


	$Ypos-=39;
	$Ypos+=6;
	$Xpos=20;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,6,"Rifiuto pericoloso: ","BTLR","L");

	$Xpos+=50;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	if($info['pericoloso']=='1') $pericoloso="s�"; else $pericoloso="no";
	$FEDIT->FGE_PdfBuffer->MultiCell(75,6,$pericoloso,"BTLR","L");

	$Ypos+=6;
	$Xpos=20;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	//$FEDIT->FGE_PdfBuffer->MultiCell(50,6,"Giacenza attuale (".$info['UM']."): ","BTLR","L");
	$FEDIT->FGE_PdfBuffer->MultiCell(50,6,"Giacenza attuale (kg): ","BTLR","L");

	$Xpos+=50;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(75,6,$info['giacenza'],"BTLR","L");

	$Ypos+=6;
	$Xpos=20;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	//$FEDIT->FGE_PdfBuffer->MultiCell(50,6,"Stoccaggio massimo (".$info['UM']."): ","BTLR","L");
	$FEDIT->FGE_PdfBuffer->MultiCell(50,6,"Stoccaggio massimo (kg): ","BTLR","L");

	$Xpos+=50;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(75,6,$StockMax,"BTLR","L");

	$Ypos+=6;
	$Xpos=20;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	//$FEDIT->FGE_PdfBuffer->MultiCell(50,6,"Quantit� in eccesso (".$info['UM']."): ","BTLR","L");
	$FEDIT->FGE_PdfBuffer->MultiCell(50,6,"Quantit� in eccesso (kg): ","BTLR","L");

	$Xpos+=50;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(75,6,$Eccesso,"BTLR","L");

	$Ypos+=6;
	$Xpos=20;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,6,"Numero di contenitori: ","BTLR","L");

	$Xpos+=50;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(75,6,$info['NumCont'],"BTLR","L");

	$Ypos+=6;
	$Xpos=20;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(50,6,"Tipo di contenitore: ","BTLR","L");

	$Xpos+=50;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(75,6,$info['contenitore'],"BTLR","L");

	$Pieni=0;
	$Meta =0;
	$Vuoti=0;

	# capacit� singolo contenitore
	//$StockMaxCont = round(($MAX * $info['NumCont']), 2);
	$StockMaxCont = $MAX;

	# numero di contenitori pieni
	if($info['giacenza']>0){
		$Pieni = $info['giacenza'] / $StockMaxCont;
		$exp=explode(".",$Pieni);
		$Pieni = floor($Pieni);
		if($Pieni>$info['NumCont']) $Pieni=$info['NumCont'];
		}

	
	# se $Pieni non � decimale, non ho cassoni a met� ma gli altri sono tutti vuoti
	if(isset($exp[1]) && $Pieni<$info['NumCont']){
		$Meta=1;
		$Vuoti=$info['NumCont']-$Pieni-1;
		}
	else{
		$Meta=0;
		$Vuoti=$info['NumCont']-$Pieni;
		}
		

	# percentuale cassone a met�
	if($Meta>0){
		$Tot=$info['giacenza'];
		while($Tot>$StockMaxCont){
			$Tot-=$StockMaxCont;
			}
		$PercMetaIMG = $Tot / $StockMaxCont * 100;
		$PercMeta = round($PercMetaIMG, 1);
		$QtaMeta  = $Tot;
		}
		
	# se il rifiuto � in un cumulo, c'� solo 1 contenitore "pieno"
	if($info['ID_CONT']=='15'){
		$Pieni=1;
		$Meta =0;
		$Vuoti=0;
		}

	# percentuale cumulo
	if($info['ID_CONT']=='15'){
		$Tot=$info['giacenza'];
		while($Tot>$StockMaxCont){
			$Tot-=$StockMaxCont;
			}
		$PercCumulo = $Tot / $StockMaxCont * 100;
		$PercCumulo = round($PercCumulo, 1);
		$QtaCumulo  = $Tot;
		}



	$cassoni=0;

$directory	= "contenitori/";
$larghezza	= $info['larghezza_img'];
$altezza	= $info['altezza_img'];

### CASSONI PIENI

	for($p=0;$p<$Pieni;$p++){
		$cassoni++;
		$Img=true;

		$Xpos=$Xstart+20;
		$Ypos+=30;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(75,6,$info['contenitore']." #".$cassoni,"","L");

		$Ypos-=10; 
		if($info['ID_CONT']=='1' | $info['ID_CONT']=='2' | $info['ID_CONT']=='3' | $info['ID_CONT']=='4' | $info['ID_CONT']=='5')			$Ypos+=10;
		if($info['ID_CONT']=='11' | $info['ID_CONT']=='15'){
			$Ypos+=10;
			$Xpos-=20;
			}

		$Xpos+=90;
		if($info['ID_CONT']=='15'){
			if($percentuale>66){
				$larghezza=$info['larghezza_img'];
				$altezza=$info['altezza_img'];
				}
			else{
				if($PercCumulo>33){
					$larghezza=65;
					$altezza=40;
					}
				else{
					if($PercCumulo>0){
						$larghezza=40;
						$altezza=25;
						}
					else{
						$Img=false;
						}
					}
				}
			}
		
		if($Img)
			$FEDIT->FGE_PdfBuffer->Image($directory.$info['ID_CONT']."/100.png",$Xpos,$Ypos,$larghezza,$altezza);
		$Xpos-=90;
		$Ypos+=10; 
		if($info['ID_CONT']=='1' | $info['ID_CONT']=='2' | $info['ID_CONT']=='3' | $info['ID_CONT']=='4' | $info['ID_CONT']=='5')			$Ypos-=10;
		if($info['ID_CONT']=='11' | $info['ID_CONT']=='15'){
			$Ypos-=10;
			$Xpos+=20;
			}


		$Ypos+=6;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',7);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		//$FEDIT->FGE_PdfBuffer->MultiCell(40,6,"Stoccaggio massimo (".$info['UM']."): ","TLRB","L");
		$FEDIT->FGE_PdfBuffer->MultiCell(40,6,"Stoccaggio massimo (kg): ","TLRB","L");

		$Xpos+=40;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,6,$StockMaxCont,"TLRB","R");
		$Xpos-=40;

		$Ypos+=6;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',7);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,6,"Stoccaggio attuale (kg): ","TLRB","L");

		$Xpos+=40;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		if($info['ID_CONT']=='15'){
			$StockMaxCont=$info['giacenza'];
			}
		$FEDIT->FGE_PdfBuffer->MultiCell(20,6,$StockMaxCont,"TLRB","R");
		$Xpos-=40;

		$Ypos+=6;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',7);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,6,"Livello riempimento: ","TLRB","L");

		$Xpos+=40;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		if($info['ID_CONT']=='15'){
			$perc=$PercCumulo."%";
			}
		else
			$perc="100%";

		$FEDIT->FGE_PdfBuffer->MultiCell(20,6,$perc,"TLRB","R");
		$Xpos-=40;

		}


### CASSONE A META'

	if($Meta>0){
		$cassoni++;

		$Xpos=$Xstart+20;
		$Ypos+=30;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(75,6,$info['contenitore']." #".$cassoni,"","L");

		$Ypos-=10;  
		if($info['ID_CONT']=='1' | $info['ID_CONT']=='2' | $info['ID_CONT']=='3' | $info['ID_CONT']=='4' | $info['ID_CONT']=='5')			$Ypos+=10;
		if($info['ID_CONT']=='11'){
			$Ypos+=10;
			$Xpos-=20;
			}

		$Xpos+=90;
		$Level=round($PercMetaIMG,-1);
		$FEDIT->FGE_PdfBuffer->Image($directory.$info['ID_CONT']."/".$Level.".png",$Xpos,$Ypos,$larghezza,$altezza);
		$Xpos-=90;
		$Ypos+=10;  
		if($info['ID_CONT']=='1' | $info['ID_CONT']=='2' | $info['ID_CONT']=='3' | $info['ID_CONT']=='4' | $info['ID_CONT']=='5')			$Ypos-=10;
		if($info['ID_CONT']=='11'){
			$Ypos-=10;
			$Xpos+=20;
			}


		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',7);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		//$FEDIT->FGE_PdfBuffer->MultiCell(40,6,"Stoccaggio massimo (".$info['UM']."): ","TLRB","L");
		$FEDIT->FGE_PdfBuffer->MultiCell(40,6,"Stoccaggio massimo (kg): ","TLRB","L");

		$Xpos+=40;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,6,$StockMaxCont,"TLRB","R");
		$Xpos-=40;

		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',7);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		if($info['UM']=='Kg.') $um='kg'; else $um=$info['UM'];
		$FEDIT->FGE_PdfBuffer->MultiCell(40,6,"Stoccaggio attuale (".$um."): ","TLRB","L");

		$Xpos+=40;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,6,$QtaMeta,"TLRB","R");
		$Xpos-=40;
		
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',7);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,6,"Livello riempimento: ","TLRB","L");

		$Xpos+=40;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,6,$PercMeta."%","TLRB","R");
		$Xpos-=40;
	}



### CASSONI VUOTI

	for($p=0;$p<$Vuoti;$p++){
		$cassoni++;

		$Xpos=$Xstart+20;
		$Ypos+=30;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(75,6,$info['contenitore']." #".$cassoni,"","L");

		$Ypos-=10;  
		if($info['ID_CONT']=='1' | $info['ID_CONT']=='2' | $info['ID_CONT']=='3' | $info['ID_CONT']=='4' | $info['ID_CONT']=='5')			$Ypos+=10;
		if($info['ID_CONT']=='11'){
			$Ypos+=10;
			$Xpos-=20;
			}

		$Xpos+=90;
		$FEDIT->FGE_PdfBuffer->Image($directory.$info['ID_CONT']."/0.png",$Xpos,$Ypos,$larghezza,$altezza);
		$Xpos-=90;
		$Ypos+=10;  
		if($info['ID_CONT']=='1' | $info['ID_CONT']=='2' | $info['ID_CONT']=='3' | $info['ID_CONT']=='4' | $info['ID_CONT']=='5')			$Ypos-=10;
		if($info['ID_CONT']=='11'){
			$Ypos-=10;
			$Xpos+=20;
			}


		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',7);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		//$FEDIT->FGE_PdfBuffer->MultiCell(40,6,"Stoccaggio massimo (".$info['UM']."): ","TLRB","L");
		$FEDIT->FGE_PdfBuffer->MultiCell(40,6,"Stoccaggio massimo (kg): ","TLRB","L");

		$Xpos+=40;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,6,$StockMaxCont,"TLRB","R");
		$Xpos-=40;

		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',7);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		if($info['UM']=='Kg.') $um='kg'; else $um=$info['UM'];
		$FEDIT->FGE_PdfBuffer->MultiCell(40,6,"Stoccaggio attuale (".$um."): ","TLRB","L");

		$Xpos+=40;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,6,"0","TLRB","R");
		$Xpos-=40;
		
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',7);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,6,"Livello riempimento: ","TLRB","L");

		$Xpos+=40;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,6,"0%","TLRB","R");
		$Xpos-=40;

		}

	
	
	
	
	
	
	PDFOut($FEDIT->FGE_PdfBuffer,$DcName);

?>