<?php
session_start();
require_once("ForgEdit_includes.inc");
require_once("../__libs/SQLFunct.php");
require_once("../__includes/COMMON_wakeForgEdit.php");

if($_GET["ID"]!="garbage") {
	switch($_GET["AppyTo"]) {
		case "automezzi":
			$tableInfoBuffer = $FEDIT->FGE_TableInfo;
			$FEDIT->FGE_FLushTableInfo();
			$FEDIT->FGE_UseTables(array("user_automezzi","lov_mezzi_trasporto"));
			$FEDIT->FGE_SetSelectFields(array("ID_AUTO","adr","ID_ORIGINE_DATI","ID_LIM"),"user_automezzi");
			$FEDIT->FGE_SetSelectFields(array("description"),"lov_mezzi_trasporto");
			$FEDIT->FGE_DescribeFields();
			$FEDIT->FGE_SetFilter("ID_AUTO", $_GET["ID"],"user_automezzi");
			$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
			$tmp = $FEDIT->DbRecordSet[0]["lov_mezzi_trasportodescription"] . "|";
			if($FEDIT->DbRecordSet[0]["user_automezziadr"]=="1") {
				$tmp .= FGE_SCREEN_BOOL_TRUE;	
			}
			if($FEDIT->DbRecordSet[0]["user_automezziadr"]=="0") {
				$tmp .= FGE_SCREEN_BOOL_FALSE;	
			}
			$tmp .= "|".$FEDIT->DbRecordSet[0]["user_automezziID_ORIGINE_DATI"];
			$tmp .= "|".$FEDIT->DbRecordSet[0]["user_automezziID_LIM"];
			break;
		break;
		case "rimorchi":
			$tableInfoBuffer = $FEDIT->FGE_TableInfo;
			$FEDIT->FGE_FLushTableInfo();
			$FEDIT->FGE_UseTables(array("user_rimorchi","lov_rimorchi"));
			$FEDIT->FGE_SetSelectFields(array("ID_RMK","adr","ID_ORIGINE_DATI","ID_LIM"),"user_rimorchi");
			$FEDIT->FGE_SetSelectFields(array("description"),"lov_rimorchi");
			$FEDIT->FGE_DescribeFields();
			$FEDIT->FGE_SetFilter("ID_RMK", $_GET["ID"],"user_rimorchi");
			$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
			$tmp = $FEDIT->DbRecordSet[0]["lov_rimorchidescription"] . "|";
			if($FEDIT->DbRecordSet[0]["user_rimorchiadr"]=="1") {
				$tmp .= FGE_SCREEN_BOOL_TRUE;	
			}
			if($FEDIT->DbRecordSet[0]["user_rimorchiadr"]=="0") {
				$tmp .= FGE_SCREEN_BOOL_FALSE;	
			}
			$tmp .= "|".$FEDIT->DbRecordSet[0]["user_rimorchiID_ORIGINE_DATI"];
			$tmp .= "|".$FEDIT->DbRecordSet[0]["user_rimorchiID_LIM"];
		break;
	}
	$FEDIT->FGE_TableInfo = $tableInfoBuffer;
	echo $tmp;

} else {
	echo "||";
}

require_once("../__includes/COMMON_sleepForgEdit.php");
?>
