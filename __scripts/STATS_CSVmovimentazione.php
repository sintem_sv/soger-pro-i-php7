<?php

$CSVbuf .= AddCSVcolumn("Tipo");
$CSVbuf .= AddCSVcolumn("Movimento per riclassificazione");
$CSVbuf .= AddCSVcolumn("Data Movimento");
$CSVbuf .= AddCSVcolumn("Numero Movimento");
$CSVbuf .= AddCSVcolumn("Data Formulario");
$CSVbuf .= AddCSVcolumn("Numero Formulario");
$CSVbuf .= AddCSVcolumn("Numero di lotto");
$CSVbuf .= AddCSVcolumn("Codice CER");
$CSVbuf .= AddCSVcolumn("Descrizione rifiuto");
$CSVbuf .= AddCSVcolumn("Recupero/Smaltimento");
$CSVbuf .= AddCSVcolumn("Codice operazione R/D");
$CSVbuf .= AddCSVcolumn("Produttore");
$CSVbuf .= AddCSVcolumn("Impianto");
$CSVbuf .= AddCSVcolumn("Destinatario");
$CSVbuf .= AddCSVcolumn("Impianto");
$CSVbuf .= AddCSVcolumn("Peso partito");
$CSVbuf .= AddCSVcolumn("Peso a destino");
$CSVbuf .= AddCSVcolumn("Peso lordo");
$CSVbuf .= AddCSVcolumn("Tara");
$CSVbuf .= AddCSVcolumn("Colli");
$CSVbuf .= AddCSVcolumn("Trasportatore");
$CSVbuf .= AddCSVcolumn("Automezzo");
$CSVbuf .= AddCSVcolumn("Rimorchio");
$CSVbuf .= AddCSVcolumn("Intermediario");
$CSVbuf .= AddCSVcolumn("Rifiuto pericoloso");
$YEAR = substr($FEDIT->DbServerData["db"], -4);
if($YEAR<2016)
	$CSVbuf .= AddCSVcolumn("Classi H");
$CSVbuf .= AddCSVcolumn("Classi HP");
$CSVbuf .= AddCSVcolumn("Numero ONU");
$CSVbuf .= AddCSVcolumn("Stato fisico");
$CSVbuf .= AddCSVcolumn("% sostanza secca");
$CSVbuf .= AddCSVcolumn("Note formulario");
$CSVbuf .= AddCSVcolumn("Costi trasportatore");
$CSVbuf .= AddCSVcolumn("Costi destinatario");
$CSVbuf .= AddCSVcolumn("Costi intermediario");
$CSVbuf .= AddCSVRow();

for($m=0;$m<count($FEDIT->DbRecordSet);$m++){

	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['TIPO']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['PerRiclassificazione']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['DataMovimento']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['NumeroMovimento']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['DataFormulario']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['NumeroFormulario']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['NumeroLotto']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['CER']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['Descrizione']);
	if(!is_null($FEDIT->DbRecordSet[$m]['Operazione_Recupero_Smaltimento']))
		$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['Recupero_Smaltimento']);
	else
		$CSVbuf .= AddCSVcolumn('');
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['Operazione_Recupero_Smaltimento']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['Produttore']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['ImpiantoP']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['Destinatario']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['Impianto']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['Peso_Partito']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['Peso_Destino']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['Peso_Lordo']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['Tara']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['Colli']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['Trasportatore']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['automezzo']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['rimorchio']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['Intermediario']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['Pericoloso']);
	$YEAR = substr($FEDIT->DbServerData["db"], -4);
	if($YEAR<2016)
		$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['Classi_H']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['Classi_HP']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['Classificazione_ADR_N_ONU']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['Stato_Fisico']);
	$CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['secco']);
    $CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['NoteFormulario']);

	$SQL = "SELECT SUM(euro) AS CostiT FROM user_movimenti_costi WHERE ID_MOV_F=".$FEDIT->DbRecordSet[$m]['ID_MOV_F']." AND ID_AZT IS NOT NULL;";
	$FEDIT->SdbRead($SQL,"DbRecordSetCostiT",true,false);
	$CSVbuf .= AddCSVcolumn(number_format($FEDIT->DbRecordSetCostiT[0]['CostiT'], 2, ",", ""));

	$SQL = "SELECT SUM(euro) AS CostiD FROM user_movimenti_costi WHERE ID_MOV_F=".$FEDIT->DbRecordSet[$m]['ID_MOV_F']." AND ID_AZD IS NOT NULL;";
	$FEDIT->SdbRead($SQL,"DbRecordSetCostiD",true,false);
	$CSVbuf .= AddCSVcolumn(number_format($FEDIT->DbRecordSetCostiD[0]['CostiD'], 2, ",", ""));

	$SQL = "SELECT SUM(euro) AS CostiI FROM user_movimenti_costi WHERE ID_MOV_F=".$FEDIT->DbRecordSet[$m]['ID_MOV_F']." AND ID_AZI IS NOT NULL;";
	$FEDIT->SdbRead($SQL,"DbRecordSetCostiI",true,false);
	$CSVbuf .= AddCSVcolumn(number_format($FEDIT->DbRecordSetCostiI[0]['CostiI'], 2, ",", ""));

	$CSVbuf .= AddCSVRow();

	}

?>