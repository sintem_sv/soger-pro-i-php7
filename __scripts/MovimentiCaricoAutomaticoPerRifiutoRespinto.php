<?php
session_start();
global $SOGER;
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/SQLFunct.php");

	## Original data // Movimento
	$sql ="SELECT ID_AZP, FKEcfiscP, ID_UIMP ";
	$sql.="FROM user_movimenti_fiscalizzati ";
	$sql.="WHERE ID_MOV_F=".$_POST['ID_MOV_F'].";";
	$FEDIT->SDBRead($sql,"DbRecordSetOriginalM",true,false);

	## Original data // Scheda rifiuto
	$sql ="SELECT originalID_RIF, FANGHI_reg, peso_spec, FKEdisponibilita, lov_misure.description AS FKEumis, lov_stato_fisico.description AS FKESF ";
	$sql.="FROM user_schede_rifiuti ";
	$sql.="JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS ";
	$sql.="JOIN lov_stato_fisico ON lov_stato_fisico.ID_SF=user_schede_rifiuti.ID_SF ";
	$sql.="WHERE ID_RIF=".$_POST['ID_RIF'].";";
	$FEDIT->SDBRead($sql,"DbRecordSetOriginalR",true,false);

	## Retryving NMOV
	$sql = "SELECT MAX(NMOV) AS NMOV FROM user_movimenti_fiscalizzati WHERE NMOV<>9999999 AND ID_IMP='".$SOGER->UserData["core_impiantiID_IMP"]."' AND ".$SOGER->UserData['workmode']."=1;";
	$FEDIT->SDBRead($sql,"DbRecordSetNMOV",true,false);
	if(isset($FEDIT->DbRecordSetNMOV)) $NMOV=$FEDIT->DbRecordSetNMOV[0]['NMOV']; else $NMOV=0;
	$NMOV++;
	
	## Retryving UniqueString
	$Identifiers	= $SOGER->UserData['core_usersID_USR'].$SOGER->UserData['core_usersID_IMP'].$SOGER->UserData['workmode'];
	$UniqueString	= md5(uniqid($Identifiers, true));

	## Retryving quantita
	switch($FEDIT->DbRecordSetOriginalR[0]['FKEumis']){ // --> ma si pu�? -.-
		case "Kg.":
			$quantita =  $_POST['pesoN'];
			break;
		case "Litri":
			$quantita =  KgQConvert($_POST['pesoN'], "Litri", $FEDIT->DbRecordSetOriginalR[0]['peso_spec']);
			break;
		case "Mc.":
			$quantita =  KgQConvert($_POST['pesoN'], "M3", $FEDIT->DbRecordSetOriginalR[0]['peso_spec']);
			break;
		}

	## 'Insert' query
	$sql = "INSERT INTO user_movimenti_fiscalizzati ";
	$sql.= "(TIPO, tara, pesoL, NMOV, DTMOV, ID_IMP, ID_RIF, originalID_RIF, FANGHI, quantita, pesoN, qta_hidden, FKEdisponibilita, FKEumis, FKESF, FKEpesospecifico, ID_AZP, FKEcfiscP, ID_UIMP, produttore, trasportatore, destinatario, intermediario, NOTER, ID_USR, UniqueString) ";
	$sql.= "VALUES ";
	$sql.= "('C', '0', '0', '".$NMOV."', '".date('Y-m-d')."', '".$SOGER->UserData["core_impiantiID_IMP"]."', ".$_POST['ID_RIF'].", ".$FEDIT->DbRecordSetOriginalR[0]['originalID_RIF'].", ".$FEDIT->DbRecordSetOriginalR[0]['FANGHI_reg'].", '".$quantita."', '".$_POST['pesoN']."', '".$quantita."', '".$FEDIT->DbRecordSetOriginalR[0]['FKEdisponibilita']."', '".$FEDIT->DbRecordSetOriginalR[0]['FKEumis']."', '".$FEDIT->DbRecordSetOriginalR[0]['FKESF']."', '".$FEDIT->DbRecordSetOriginalR[0]['peso_spec']."', ".$FEDIT->DbRecordSetOriginalM[0]['ID_AZP'].", '".$FEDIT->DbRecordSetOriginalM[0]['FKEcfiscP']."', ".$FEDIT->DbRecordSetOriginalM[0]['ID_UIMP'].", 1, 0, 0, 0, '".addslashes($_POST['NOTER'])."', '".$SOGER->UserData['core_usersID_USR']."', '".$UniqueString."');"; 
	$FEDIT->SDBWrite($sql,true,false);

	## 'Log' query
	$LogSql  = "INSERT INTO core_logs ";
        $LogSql .= "(`table` , `pri_key` , `ID_USR` , `ID_IMP` , `sqlType` , `sqlCommand` ) ";
        $LogSql .= "VALUES ";
	$LogSql .= "('" . $TableName . "','" .  $FEDIT->DbLastID . "','";
	$LogSql .= $SOGER->UserData["core_usersID_USR"] . "','";
	$LogSql .= $SOGER->UserData["core_usersID_IMP"] . "','creazione','" . addslashes($sql) . "')";
	$FEDIT->SDBWrite($LogSql,true,false);

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>