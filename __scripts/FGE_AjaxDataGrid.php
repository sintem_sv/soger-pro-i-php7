<?php
session_start();
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__libs/SQLFunct.php");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
include("../__includes/USER_EtichetteBiancoNero.inc");

global $SOGER;
	
	$Pars = json_decode(utf8_encode($_GET["params"]),true);
	//print_R($_GET["params"]);
	$ObjName = $Pars["Obj"];
	$GridID = $Pars["GridID"];
	if($Pars["Obj"]=="FEDIT") {
		require_once("../__includes/COMMON_wakeForgEdit.php");
	} else {
		require_once("../__includes/COMMON_wakeForgEditDG.php");
	}

//	var_dump($$ObjName);

	$Datasql = $$ObjName->FGE_SQL_MakeSelect($Pars["Paging"]);
	#	echo $Datasql;
	$tmp = "";

	
	## WHERE $FIELD LIKE '%FILTER%'
	
		$Table=$Pars['TName'];
		switch($Table){
			default:
				$SearchField="description";
				break;
			case "core_users":
				$SearchField="usr";
				break;
			case "user_autorizzazioni_trasp":
			case "user_autorizzazioni_dest":
                        case "user_autorizzazioni_pro":
			case "user_autorizzazioni_interm":
				$SearchField="num_aut";
				break;
			case "user_schede_rifiuti":
				$SearchField="descrizione";
				break;
			case "lov_cer":
				$SearchField="COD_CER";
				break;
			}

		if($Pars['Filter']!=""){
			if(!strpos($Datasql,"WHERE"))
				$like = " WHERE ";
			else
				$like = " AND ";
			$like.= $Table. "." .$SearchField. " LIKE '%".addslashes($Pars['Filter'])."%' ";
			}
		else
			$like=" ";
	
	//print_r($Datasql);


	

	# PAGINAZIONE
	#/*

	if($Pars["Paging"]=="1") {
		$tmp.="<div class=\"FGEDataGridPaging_container\">";

		foreach($$ObjName->FGE_TableInfo as $TName=>$FArray) {
			foreach($FArray as $FName=>$FInfo) { 
				//print_r($Field);
				if($TName==$Table && $FInfo["Field"]==$SearchField){
					$FieldLabel=$FInfo["FGE_Label"];
					}
				}
			}

                # FILTRO DI RICERCA
                $tmp.="<div class=\"FGEDataGridFilter\" id=\"FGE_DGF_$GridCount\">";
                $tmp.="<form name=\"FilterForm\" action=\"__scripts/status.php\" method=\"POST\">";
                $tmp.="<span style=\"font-weight:bold; color:black;\">Chiave di ricerca:</span>";
                $tmp.="<input style=\"margin-left:50px;\" type=\"text\" name=\"filter\" value=\"".$Pars['Filter']."\" id=\"filter\" />";
                $tmp.="<input class=\"FiscalButton\" type=\"button\" onclick=\"document.FilterForm.submit();\" name=\"filter_submit\" value=\"CERCA\" id=\"filter_submit\" />"; 
                $tmp.="<input class=\"FiscalButton\" type=\"button\" onclick=\"document.getElementById('filter').value='';document.FilterForm.submit();\" name=\"filter_submit\" value=\"MOSTRA TUTTI\" id=\"filter_submit\" />"; 
                $tmp.="<br /><br /><b>Attenzione:</b> la ricerca viene eseguita sul campo \"".$FieldLabel."\"";
                $tmp.="</form>";
                $tmp.="</div>";
                unset($_SESSION['SearchingFilter']);

		if(isset($_SESSION["sqlOhneLimit"])) {
			$sql = $_SESSION["sqlOhneLimit"];
			$sql.= $like;
			$$ObjName->SDBRead($sql,"DbRecordSet",true,false);
			$Records = $$ObjName->DbRecsNum;
			} 
		else {
			$sql = "SELECT COUNT(*) AS RecNum FROM " . $Pars["TName"];
			$$ObjName->SDBRead($sql,"DbRecordSet",true,false);
			$Records = $$ObjName->DbRecordSet[0]["RecNum"];
			}

		$Pages = ceil($Records/$$ObjName->FGE_Preferences["FGE_RecordsPerPage"]);
		$Pars['Filter'] = urlencode($Pars['Filter']);

		if($Pages==0) {
			$Pages=1;
		}
		$c=1;
		while($c<=$Pages) {
			if($c>1) {
				$Parameters = $$ObjName->FGE_DgParseParameters($Pars,"StartPoint",(($c-1)*$$ObjName->FGE_Preferences["FGE_RecordsPerPage"]+1));
				$url = "javascript:FGE_AjaxDgRefresh('params=" . json_encode($Parameters) . "','$GridID')";
				if($Pars["StartPoint"]==(($c-1)*$$ObjName->FGE_Preferences["FGE_RecordsPerPage"]+1)) {
					$tmp .= "<span class=\"FGEDataGridActualPage\" style=\"font-size: 9px;\">$c</span> ";
				} else {
					$tmp .= "<span class=\"FGEDataGridPage\" style=\"font-size: 9px;\"><a href=$url class=\"FGEDataGridPagingLink\">$c</a></span>";
				}
			} else {
				$Parameters = $$ObjName->FGE_DgParseParameters($Pars,"StartPoint",1);
				$url = "javascript:FGE_AjaxDgRefresh('params=" . json_encode($Parameters) . "','$GridID')";
				if($Pars["StartPoint"]=="1") {
					$tmp .= "<span  class=\"FGEDataGridActualPage\" style=\"font-size: 9px;\">$c</span> ";
				} else {
					$tmp .= "<span class=\"FGEDataGridPage\" style=\"font-size: 9px;\"><a href=$url class=\"FGEDataGridPagingLink\">$c</a></span> ";
				}
			}
			$c++;
		}
		$tmp.="</div>";
	} 

#	*/
	# HEADERS
//{{{ 
	$tmp .= "<table class=\"FGEDataGridTable\">";
	$tmp .= "<tr>";
	
	//$ColCount = 0;
	//$ColCount lo inizializziamo a 1 cos� teniamo conto della colonna 'operazioni'
	$ColCount = 1;
	
	#
	foreach($$ObjName->FGE_TableInfo as $TName=>$FArray) {
		foreach($FArray as $FName=>$FInfo) { 
			//print_r($FArray);
			if(!is_null($FInfo["FGE_ToBeSelected"]) && is_null($FInfo["FGE_Hidden"])) {

				if(($Pars["TName"]!='user_automezzi' && $Pars["TName"]!='user_rimorchi') OR $FInfo["FGE_Label"]!='codice'){
					$Parameters = $$ObjName->FGE_DgParseParameters($Pars,"StartPoint",1);
					$Parameters = $$ObjName->FGE_DgParseParameters($Pars,"OrderBy","$TName:$FName");
					
					#
					#	AGGIUNGI GESTIONE ASC /DESC
					#
					$tmp .= "<th class=\"FGEDataGridTableHeader\" id=\"" . str_replace(" ","_",$FInfo["FGE_Label"]) . "\">";
					$url = "javascript:FGE_AjaxDgRefresh('params=" . json_encode($Parameters) . "','$GridID')";
					$tmp .= "<a href=$url title=\"Ordina per " . strtolower($FInfo["FGE_Label"])  . "\" class=\"FGEDataGridOrderLink\">";
					$tmp .= strtolower($FInfo["FGE_Label"]) . "</a></th>";
					$ColCount++;
					}
				}
			}
		}


	#echo $Datasql . "<hr>";
	#print_r( $Pars);
	#echo $tmp;
	#return;

	$AllowEdit = false;		// E
	$AllowDelete = false;	// D
	$AllowClone = false;	// C
	$AllowPrint = false;	// P
	##
	$AllowTMP_Anag = false;	// T
	$AllowSync = false;		// Y
	$AllowSomething = false;

	$$ObjName->FGE_DgParseConfig($Pars["Config"],$AllowEdit,$AllowDelete,$AllowClone,$AllowPrint,$AllowTMP_Anag,$AllowSync);
	if($AllowEdit | $AllowDelete | $AllowClone | $AllowPrint | $AllowTMP_Anag | $AllowSync) {
		$AllowSomething = true;
                $width = $Pars["TName"]=='user_schede_rifiuti'? '130px':'auto';
		$tmp.="<th class=\"FGEDataGridTableHeader\" style=\"width:".$width."\">operazioni</th>";
		}

	if($SOGER->AppLocation=="AdempimentiRifiuti"){
		$tmp.="<th class=\"FGEDataGridTableHeader\">caratterizzazione<br />e classificazione</th>";
		$tmp.="<th class=\"FGEDataGridTableHeader\">pericolosit�</th>";
		$tmp.="<th class=\"FGEDataGridTableHeader\">deposito</th>";
		}
		
	if($SOGER->AppLocation=="UserSchedeRifiuti_caratterizzazioni"){
		$tmp.="<th class=\"FGEDataGridTableHeader\">caratterizzazione</th>";
		}
        
        if($SOGER->AppLocation=="UserSchedeRifiuti_classificazioni"){
		$tmp.="<th class=\"FGEDataGridTableHeader\">classificazione</th>";
		}
                
        if($SOGER->AppLocation=="UserSchedeRifiuti_analisi"){
		$tmp.="<th class=\"FGEDataGridTableHeader\">lista analiti</th>";
		$tmp.="<th class=\"FGEDataGridTableHeader\">analisi</th>";
		}

	if($SOGER->AppLocation=="UserSchedeRifiuti_pericolosita"){
		$tmp.="<th class=\"FGEDataGridTableHeader\">etichetta</th>";
		$tmp.="<th class=\"FGEDataGridTableHeader\">pericolosit�</th>";
		}

	$tmp .= "</tr>\n";	 //}}}
	


############## questo funge #######################
	if(!is_null($Pars["OrderBy"])) {
		$TabAndField = explode(":",$Pars["OrderBy"]);
		$ordinamento = " ORDER BY " . $TabAndField[0] . "." . $TabAndField[1] . $Pars["OrderMode"] . " ";
		if($Pars["Paging"]=="1") {
			$Datasql = substr($Datasql,0,strpos($Datasql,"LIMIT")) . $like . $ordinamento . substr($Datasql,strpos($Datasql,"LIMIT"));
			} 
		else {
			$Datasql .= " " . $like . $ordinamento;
			}
		
		}
	else{
		//print_r($Pars["TName"]);
		## dovrebbe esserci la funzione FGE_SetOrder, ma non funge...
		switch($Pars["TName"]){
			case "core_dbversion":
				$Field="version";
				break;
			case "user_schede_rifiuti":
				$Field="ID_CER";
				break;
			case "lov_cer":
				$Field="COD_CER";
				break;
			case "user_impianti_produttori":
			case "user_impianti_destinatari":
			case "user_impianti_trasportatori":
			case "user_impianti_intermediari":
				$Field="ID_COM";
				break;
			case "lov_comuni_istat":
			case "lov_num_onu":
			case "core_users":
			case "core_impianti":
			case "core_gruppi":
			case "core_intestatari":
			case "user_aziende_produttori":
			case "user_aziende_destinatari":
			case "user_aziende_intermediari":
			case "user_aziende_trasportatori":
			case "user_aree_stoccaggio_dest":
			case "user_automezzi":
			case "user_autisti":
			case "user_rimorchi":
			case "user_contratti_destinatari":
			case "user_contratti_produttori":
			case "user_contratti_intermediari":
			case "user_contratti_trasportatori":
			case "user_contratti_dest_cond":
			case "user_contratti_pro_cond":
			case "user_contratti_int_cond":
			case "user_contratti_trasp_cond":
				$Field="description";
				break;
			case "user_autorizzazioni_pro":
			case "user_autorizzazioni_dest":
			case "user_autorizzazioni_interm":
			case "user_autorizzazioni_trasp":
			case "user_contratti_budget";
				$Field="ID_RIF"; ## da controllare, dovrebbe essere CER !!
				break;
			default:
				$Field="";
				break;
			}
		$ordinamento = " ORDER BY " . $Pars["TName"] . "." . $Field . $Pars["OrderMode"] . " ";
		if($Pars["Paging"]=="1") {
			$Datasql = substr($Datasql,0,strpos($Datasql,"LIMIT")) . $like . $ordinamento . substr($Datasql,strpos($Datasql,"LIMIT"));
			} 
		else {
			$Datasql .= " " . $like . $ordinamento;
			}
		}
	//print_r($Datasql);

	# DATA

	$$ObjName->SDBRead($Datasql,"DbRecordSet",true,false);
	if($$ObjName->DbRecsNum==0) {
		if(is_null($$ObjName->FGE_PdfBuffer)) {
			$tmp .= "<tr><td colspan=\"" . $ColCount . "\" class=\"Tcell\"><br/><b>" . FGE_NoRecord . "</b><br/><br/></td></tr></table>";
			//return $tmp;
			} 
		}



	if($$ObjName->DbRecsNum>0) {

		$PrimaryKey = null;
		$PrimaryTable = null;
		
		foreach($$ObjName->DbRecordSet as $k=>$values) {
			$tmp .= "<tr>";
			foreach($$ObjName->FGE_TableInfo as $TName=>$FArray) {
				foreach($FArray as $FName=>$FInfo) {
					if($FInfo["FGE_Type"]=="FGEprimary" && is_null($PrimaryKey)) {
						$PrimaryKey=$FInfo["Field"];
					}
					if(is_null($PrimaryTable)) {
						$PrimaryTable=$TName;
					}
					if(!is_null($FInfo["FGE_ToBeSelected"]) && is_null($FInfo["FGE_Hidden"])) {
						if(($Pars["TName"]!='user_automezzi' && $Pars["TName"]!='user_rimorchi') OR $FInfo["FGE_Label"]!='codice'){
                                                    
							$AuthAppStyle = "";
                                                        if (strpos($Pars["TName"], 'user_autorizzazioni') !== false) {
                                                            if($values['user_schede_rifiutiapproved']=='0' && $FInfo["FGE_Label"]=='cer conferito')
                                                                $AuthAppStyle = " background-color: #fcd2d1; ";
                                                        }
                                                    
							$tmp .= "<td class=\"Tcell\" style=\"".$AuthAppStyle."\" headers=\"" .  str_replace(" ","_",$FInfo["FGE_Label"]) . "\">";
							if($FInfo["FGE_Label"]=='descrizione' && $values[$TName."cod_pro"]!='' && ($Pars["TName"]=='user_automezzi' OR $Pars["TName"]=='user_rimorchi')){
								$tmp .= $$ObjName->FGE_DgRenderValue($values[$TName."cod_pro"],$$ObjName->FGE_TableInfo[$TName]["cod_pro"]);
								$tmp .=" - ";
								}														
							$tmp .= $$ObjName->FGE_DgRenderValue($values[$TName.$FName],$$ObjName->FGE_TableInfo[$TName][$FName]);
							$tmp .= "</td>";
						}
					}
				}
			}

			if($AllowEdit | $AllowDelete | $AllowClone | $AllowPrint | $AllowTMP_Anag) {
				if(strstr($Pars["DestURL"],"?")) {
					$UrlSym = "&amp;";
					} else {
					$UrlSym = "?";
				}
				
				$url = $Pars["DestURL"] . $UrlSym; 
				$RecData = "table=$PrimaryTable&amp;pri=$PrimaryKey&amp;filter=" . $values[$PrimaryTable.$PrimaryKey];
				$RecData .= "&amp;hash=" . MakeUrlHash($PrimaryTable,$PrimaryKey,$values[$PrimaryTable.$PrimaryKey]);
				$url .= $RecData;
				$tmp .= "<td class=\"Tcell\">";
			}

			if($AllowEdit) {
				//print_r($PrimaryTable);
				if($PrimaryTable=="user_aziende_produttori" | $PrimaryTable=="user_aziende_destinatari" | $PrimaryTable=="user_aziende_trasportatori" | $PrimaryTable=="user_aziende_intermediari") {
					$urlE = $url . "&amp;name=" . urlencode($values[$PrimaryTable . "description"]);	
				} else {
					$urlE = $url;
				}
				if($PrimaryTable=="core_users") {
					$urlE .= "&amp;UserIsResponsabile=".$values[$PrimaryTable . "sresponsabile"];
					}
				$tmp .= "<a href=\"$urlE&amp;FGE_action=edit\" title=\"apri / modifica\">";
				$tmp .= "<img src=\"__css/FGE_edit.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"apri / modifica\"/>";
				$tmp .= "</a>";
			}

			if($PrimaryTable=="user_contratti_destinatari" | $PrimaryTable=="user_contratti_produttori" | $PrimaryTable=="user_contratti_intermediari" | $PrimaryTable=="user_contratti_trasportatori"){
				
				switch($PrimaryTable){
					
					case "user_contratti_destinatari":
						$contractual_conds="__scripts/SOGER_standardMenu.php?area=UserDestinatarioContrattoCondizioni&amp;table=user_contratti_dest_cond&amp;pri=ID_CNT_D&amp;filter=".$values[$PrimaryTable.$PrimaryKey]."&amp;contractname=" . urlencode($values[$PrimaryTable . "description"])."&amp;hash=".MakeUrlHash("user_contratti_dest_cond","ID_CNT_D",$values[$PrimaryTable.$PrimaryKey]);
						break;

					case "user_contratti_produttori":
						$contractual_conds="__scripts/SOGER_standardMenu.php?area=UserProduttoreContrattoCondizioni&amp;table=user_contratti_pro_cond&amp;pri=ID_CNT_P&amp;filter=".$values[$PrimaryTable.$PrimaryKey]."&amp;contractname=" . urlencode($values[$PrimaryTable . "description"])."&amp;hash=".MakeUrlHash("user_contratti_pro_cond","ID_CNT_P",$values[$PrimaryTable.$PrimaryKey]);
						break;

					case "user_contratti_intermediari":
						$contractual_conds="__scripts/SOGER_standardMenu.php?area=UserIntermediarioContrattoCondizioni&amp;table=user_contratti_int_cond&amp;pri=ID_CNT_I&amp;filter=".$values[$PrimaryTable.$PrimaryKey]."&amp;contractname=" . urlencode($values[$PrimaryTable . "description"])."&amp;hash=".MakeUrlHash("user_contratti_int_cond","ID_CNT_I",$values[$PrimaryTable.$PrimaryKey]);
						break;

					case "user_contratti_trasportatori":
						$contractual_conds="__scripts/SOGER_standardMenu.php?area=UserTrasportatoreContrattoCondizioni&amp;table=user_contratti_trasp_cond&amp;pri=ID_CNT_T&amp;filter=".$values[$PrimaryTable.$PrimaryKey]."&amp;contractname=" . urlencode($values[$PrimaryTable . "description"])."&amp;hash=".MakeUrlHash("user_contratti_trasp_cond","ID_CNT_T",$values[$PrimaryTable.$PrimaryKey]);
						break;
					}

				$tmp .= "<a href=\"".$contractual_conds."\" title=\"condizioni contrattuali\">";
				$tmp .= "<img src=\"__css/euro.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"condizioni contrattuali\"/>";
				$tmp .= "</a>";
				}

			if($AllowPrint) {
				$tmp .= "<a href=\"$url&amp;FGE_action=print\" title=\"crea Pdf\" >";
				$tmp .= "<img src=\"__css/FGE_pdf.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"crea Pdf\"/>";
				$tmp .= "</a>";
				}

			if($AllowClone) {
				//duplicazione
				if($SOGER->AppLocation=='UserSchedeRifiuti'){
					$tmp .= "<a href=\"javascript:showDuplicazioneRifiuto('".$values[$PrimaryTable.$PrimaryKey]."', '".$url."');\" title=\"duplica\">";
					}
				else{
					$tmp .= "<a href=\"$url&amp;FGE_action=clone\" title=\"duplica\">";
					}
				$tmp .= "<img src=\"__css/FGE_clone.png\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left:	1px; margin-right: 2px;\" alt=\"duplica\" class=\"NoBorder\" />";
				$tmp .= "</a>";

				//duplicazione rifiuto e associazione a fornitori
				/*
				if($SOGER->AppLocation=='UserSchedeRifiuti'){
					$tmp .= "<a href=\"javascript:if(confirm('Desideri duplicare la scheda rifiuto e associare la nuova scheda agli stessi fornitori dell\'originale?')){document.location='$url&amp;FGE_action=cloneMatch';}\" title=\"duplica e associa a fornitori\">";
					$tmp .= "<img src=\"__css/FGE_cloneMatch.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"duplica\"/>";
					$tmp .= "</a>";				
					}
				*/
			}

			## PROCEDURA SCARICO-RICARICO x NUOVA CLASSIFICAZIONE
			if($SOGER->UserData["workmode"] == "produttore" && strtotime(date('Y-m-d'))>=strtotime('2015-06-01')){
				if($SOGER->UserData['core_usersO5']=="1" && $SOGER->AppLocation=='UserSchedeRifiuti'){
					$tmp .= "<a href=\"javascript:showScaricoRicaricoRifiuto('".$values[$PrimaryTable.$PrimaryKey]."');\" alt=\"Procedura di scarico e ricarico del rifiuto a seguito di nuova classificazione\" title=\"Procedura di scarico e ricarico del rifiuto a seguito di nuova classificazione\">";
					$tmp .= "<img src=\"__css/arrow-return-090-left.png\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"Procedura di scarico e ricarico del rifiuto a seguito di nuova classificazione\" title=\"Procedura di scarico e ricarico del rifiuto a seguito di nuova classificazione\" class=\"NoBorder\" />";
					$tmp .= "</a>";
					}
				}

			## BUDGET
			if($SOGER->UserData["core_impiantiMODULO_ECO"]=="1" && $SOGER->UserData['core_usersG3']=="1" && $SOGER->AppLocation=='UserSchedeRifiuti'){
				
				$sql="select ID_BUDG from user_contratti_budget where ID_IMP='".$SOGER->UserData['core_impiantiID_IMP']."' and ID_RIF='".$values[$PrimaryTable.$PrimaryKey]."';";

				$$ObjName->SDBRead($sql,"DbRecordSet2",true,false);
				
				if(isset($$ObjName->DbRecordSet2[0]['ID_BUDG'])){
					$urlBudget ="__scripts/FGE_DataGridEdit.php?table=user_contratti_budget&amp;ID_RIF=".$values[$PrimaryTable.$PrimaryKey]."&amp;pri=ID_BUDG&filter=".$$ObjName->DbRecordSet2[0]['ID_BUDG']."&amp;FGE_action=edit&amp;name=". urlencode($values[$PrimaryTable . "descrizione"])."&amp;hash=".MakeUrlHash("user_contratti_budget","ID_BUDG",$$ObjName->DbRecordSet2[0]['ID_BUDG']);
					$tmp .= "<a href=\"".$urlBudget."\" title=\"modifica budget rifiuto\">";
					$tmp .= "<img src=\"__css/euro.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"modifica budget rifiuto\"/>";
					$tmp .= "</a>";
					}
				else{
					$urlBudget="__scripts/FGE_DataGridEdit.php?table=user_contratti_budget&amp;ID_RIF=".$values[$PrimaryTable.$PrimaryKey]."&amp;FGE_action=edit&amp;name=". urlencode($values[$PrimaryTable . "descrizione"]);
					$tmp .= "<a href=\"".$urlBudget."\" title=\"assegna budget rifiuto\">";
					$tmp .= "<img src=\"__css/euro_vuoto.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"assegna budget rifiuto\"/>";
					$tmp .= "</a>";
					}
				}


			if($AllowDelete) {
				## tabelle per cui eliminando un record elimino tutti quelli con stesso valore se non usati in movimenti
				$InheritTables=array("user_autorizzazioni_pro", 
									"user_autorizzazioni_trasp", 
									"user_autorizzazioni_dest", 
									"user_autorizzazioni_interm", 
									"user_automezzi", 
									"user_rimorchi");
				$tmp .= "<a href=\"javascript:if(confirm('" . FGE_DeleteConfirm . "')){ ";
					if(in_array($PrimaryTable, $InheritTables)){
						$tmp .= "if(confirm('Desidera eliminare i record affini non impiegati nella movimentazione?')){ ";
							$tmp .= "document.location='$url&amp;FGE_action=delete&amp;inherit=1';";
						$tmp .= "}else{ ";
							$tmp .= "document.location='$url&amp;FGE_action=delete';";
						$tmp .= "} ";
						}
					else{
						$tmp .= "document.location='$url&amp;FGE_action=delete';";
						}


					$tmp.="}\" title=\"cancella\">";
					//$tmp.="document.location='$url&amp;FGE_action=delete'};\" title=\"cancella\">";
				$tmp .= "<img src=\"__css/FGE_delete.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"cancella\"/>";
				$tmp .= "</a>";
			}

			if($AllowTMP_Anag && ($SOGER->UserData['core_usersG1']==1 || $SOGER->UserData['core_usersG14']==1) ){
				if($values[$PrimaryTable . "approved"]==1){
					$tmp.="<a href=\"$urlE&amp;FGE_action=disapprove\" title=\"record approvato\">";
					$tmp.="<img src=\"__css/FGE_thumb_up.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"record approvato\" />";
					$tmp.="</a>";
					}
				else{
					$tmp.="<a href=\"$urlE&amp;FGE_action=approve\" title=\"record non approvato\">";
					$tmp.="<img src=\"__css/FGE_thumb_down.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"record non approvato\" />";
					$tmp.="</a>";
					}
				}


			if($AllowSync && $SOGER->UserData['core_impiantiMODULO_SIS']==1){

				if(!is_null($values[$PrimaryTable."idSIS"])) $SISTRI_icon = "SIS_sincronizza_on.gif";	else $SISTRI_icon = "SIS_sincronizza_off.gif";

				$tmp.="<img src=\"__css/".$SISTRI_icon."\" class=\"NoBorder IMG_SyncroImpianto\" style=\"cursor:pointer; margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" title=\"Sincronizza impianto\" alt=\"Sincronizza impianto\" id=\"".$PrimaryTable.":".$values[$PrimaryTable.$PrimaryKey]."\" current_idSIS=\"".$values[$PrimaryTable."idSIS"]."\" />";
				
				}


			if( $SOGER->UserData['core_usersG2']==1 && (strpos($PrimaryTable, "user_aziende_")!==false | strpos($PrimaryTable, "user_impianti_")!==false) ){
				if(!is_null($values[$PrimaryTable . "latitude"])){
					$latitudine		= $values[$PrimaryTable . "latitude"];
					$longitudine	= $values[$PrimaryTable . "longitude"];
					$icon			= 'marker_on.gif';
					}
				else{
					$latitudine		= 'false';
					$longitudine	= 'false';
					$icon			= 'marker_off.gif';
					}
				
				$tmp.="<img rel=\"".$PrimaryTable.":".$values[$PrimaryTable.$PrimaryKey]."\" src=\"__css/".$icon."\" class=\"NoBorder jq_LatLong\" style=\"cursor:pointer;margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"Posizione sulla mappa\" />";

				}


			if(($PrimaryTable=="user_autorizzazioni_dest" | $PrimaryTable=="user_autorizzazioni_trasp" | $PrimaryTable=="user_automezzi" | $PrimaryTable=="user_rimorchi") && $SOGER->AppLocation!="AssociaMezzi"){
				switch($values[$PrimaryTable."ID_ORIGINE_DATI"]){
					default:
					case "1":
						$rosette="rosette_gray.png";
						break;
					case "2":
						$rosette="rosette_red.png";
						break;
					case "3":
						$rosette="rosette_orange.png";
						break;
					case "4":
						$rosette="rosette_green.png";
						break;
					}

				if($PrimaryTable=="user_rimorchi" | $PrimaryTable=="user_automezzi"){
					$tmp.="<a href=\"javascript:if(confirm('Associare la coccarda ai record con targa ".$values[$PrimaryTable."description"]."')){document.location='$url&amp;FGE_action=setRosette&amp;RosetteID=".$values[$PrimaryTable."ID_ORIGINE_DATI"]."&amp;ID_UIMT=".$values["user_impianti_trasportatoriID_UIMT"]."&amp;description=".$values[$PrimaryTable."description"]."'}\" title=\"associa la coccarda ai record con targa ".$values[$PrimaryTable."description"]."\" >";
					$tmp.="<img src=\"__css/".$rosette."\" class=\"NoBorder\" style=\"margin-bottom: 1px;\" />";
					$tmp.="</a>";

					$tmp.="<a href=\"javascript:if(confirm('Estendere le limitazioni al trasporto ai record con targa ".$values[$PrimaryTable."description"]."')){document.location='$url&amp;FGE_action=setID_LIM&amp;ID_LIM=".$values["lov_limitazioni_mezziID_LIM"]."&amp;ID_UIMT=".$values["user_impianti_trasportatoriID_UIMT"]."&amp;description=".$values[$PrimaryTable."description"]."'}\" title=\"estende le limitazioni al trasporto ai record con targa ".$values[$PrimaryTable."description"]."\" >";
					$tmp.="<img src=\"__css/license.png\" class=\"NoBorder\" style=\"margin-bottom: 1px;\" />";
					$tmp.="</a>";
					}
				else
					$tmp.="<img src=\"__css/".$rosette."\" class=\"NoBorder\" style=\"margin-bottom: 1px;\" />";
				}

			if(($PrimaryTable=="user_automezzi" | $PrimaryTable=="user_rimorchi") && $AllowSomething) {
				$Prs = "$RecData&action=edit&ID_RIF=" . $values["user_schede_rifiutiID_RIF"] . "&ID_IMP=" . $values["user_impianti_trasportatoriID_UIMT"];
				$Prs .= "&TGN=" . urlencode($values[$PrimaryTable . "description"]); 
				$tmp .= "<a href=\"__scripts/status.php?area=AssociaMezzi&$Prs\" title=\"associa il mezzo ad altri codici cer\" >";
				$tmp .= "<img src=\"__css/altricer.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"associa il mezzo ad altri codici cer\"/>";
				$tmp .= "</a>";	
			}

                        

			if($PrimaryTable=="user_schede_rifiuti") {
				
				## APP LOCATION: UserSchedeRifiuti_caratterizzazioni
				## - approved
				## - budget
				## - analisi chimica
				## - coccarda
				## - caratterizzazione

				if($SOGER->AppLocation=="AdempimentiRifiuti"){
					
					$tmp.="<td class=\"Tcell\">";

					## CARATTERIZZAZIONE ##
					
					if($values["user_schede_rifiutiID_FONTE_RIF"]!=0){
						$icona		= "camera_on.gif"; 
						}
					else{ 
						$icona		= "camera_off.gif";
						}
					$tmp .= "<a href=\"__scripts/statusCUSTOM_SchedaRifiutoCaratt.php?Adempimento&amp;table=user_schede_rifiuti&amp;pri=ID_RIF&amp;filter=".$values["user_schede_rifiutiID_RIF"]."&amp;hash=".MakeUrlHash("user_schede_rifiuti","ID_RIF",$values["user_schede_rifiutiID_RIF"])."\" title=\"caratterizzazione rifiuto\" >";
					//$tmp .= "<a href=\"__scripts/status.php?action=edit&amp;area=UserSchedaRifiutoCaratt&amp;Adempimento&amp;table=user_schede_rifiuti&amp;pri=ID_RIF&amp;filter=".$values["user_schede_rifiutiID_RIF"]."&amp;hash=".MakeUrlHash("user_schede_rifiuti","ID_RIF",$values["user_schede_rifiutiID_RIF"])."\" title=\"caratterizzazione rifiuto\" >";
					$tmp .= "<img src=\"__css/".$icona."\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"caratterizzazione rifiuto\"/>";
					$tmp .= "</a>";
					$ID_RIF=$values['user_schede_rifiutiID_RIF'];				
					
					$caratterizzazione_doc = "../__upload/caratterizzazioni/".$ID_RIF.".doc";
					$caratterizzazione_rtf = "../__upload/caratterizzazioni/".$ID_RIF.".rtf";
					$caratterizzazione_pdf = "../__upload/caratterizzazioni/".$ID_RIF.".pdf";
					
					if($SOGER->UserData['core_usersG7']=='1'){
						if(!file_exists($caratterizzazione_doc) && !file_exists($caratterizzazione_rtf) && !file_exists($caratterizzazione_pdf)){

							## upload caratterizzazione
							$tmp.="<a href=\"javascript:document.UploadCaratterizzazione['ID_RIF'].value='".$ID_RIF."';javascript:scroll(0,0);upload('UPL_caratterizzazione');\"><img src=\"__css/camera_up_on.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"carica caratterizzazione\" title=\"carica caratterizzazione\" /></a>";

							## download caratterizzazione	
							$tmp.="<a href=\"javascript:alert('Impossibile scaricare la caratterizzazione dal server: caratterizzazione non caricata.');\"><img src=\"__css/camera_down_off.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"scarica caratterizzazione\" title=\"scarica caratterizzazione\" /></a>";
							}

						else{

							## upload caratterizzazione
							$tmp.="<a href=\"javascript:document.UploadCaratterizzazione['ID_RIF'].value='".$ID_RIF."';javascript:scroll(0,0);upload('UPL_caratterizzazione');\"><img src=\"__css/camera_up_on.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"carica caratterizzazione\" title=\"carica caratterizzazione\" /></a>";
							
							## download caratterizzazione
							if(file_exists($caratterizzazione_doc)) $ext=".doc";
							if(file_exists($caratterizzazione_rtf)) $ext=".rtf";
							if(file_exists($caratterizzazione_pdf)) $ext=".pdf";
							$tmp.="<a href=\"__upload/caratterizzazioni/".$ID_RIF.$ext."\" target=\"_blank\"><img src=\"__css/camera_down_on.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"scarica caratterizzazione\" title=\"scarica caratterizzazione\" /></a>";
							}
						}

					## ANALISI

					$ID_RIF=$values['user_schede_rifiutiID_RIF'];				
					
					$analisi_pdf = "../__upload/analisi/".$ID_RIF.".pdf";
					if($SOGER->UserData['core_usersG7']=='1'){
						if(!file_exists($analisi_pdf)){

							## upload analisi
							$tmp.="<a href=\"javascript:document.UploadAnalisi['ID_RIF'].value='".$ID_RIF."';javascript:scroll(0,0);upload('UPL_analisi');\"><img src=\"__css/analisi_up.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"carica analisi\" title=\"carica analisi\" /></a>";

							## download analisi	
							$tmp.="<a href=\"javascript:alert('Impossibile scaricare l\'analisi dal server: analisi non caricata.');\"><img src=\"__css/analisi_down_off.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"scarica analisi\" title=\"scarica analisi\" /></a>";
							}

						else{

							## upload analisi
							$tmp.="<a href=\"javascript:document.UploadAnalisi['ID_RIF'].value='".$ID_RIF."';javascript:scroll(0,0);upload('UPL_analisi');\"><img src=\"__css/analisi_up.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"carica analisi\" title=\"carica analisi\" /></a>";
							
							## download analisi
							if(file_exists($analisi_pdf)) $ext=".pdf";
							$tmp.="<a href=\"__upload/analisi/".$ID_RIF.$ext."\" target=\"_blank\"><img src=\"__css/analisi_down.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"scarica analisi\" title=\"scarica analisi\" /></a>";
							}
						}


					## CLASSIFICAZIONE
					
					$classificazione_doc = "../__upload/classificazioni/".$ID_RIF.".doc";
					$classificazione_rtf = "../__upload/classificazioni/".$ID_RIF.".rtf";
					$classificazione_pdf = "../__upload/classificazioni/".$ID_RIF.".pdf";
					
					if($SOGER->UserData['core_usersG7']=='1'){
						if(!file_exists($classificazione_doc) && !file_exists($classificazione_rtf) && !file_exists($classificazione_pdf)){

							## upload classificazione
							$tmp.="<a href=\"javascript:document.UploadClassificazione['ID_RIF'].value='".$ID_RIF."';javascript:scroll(0,0);upload('UPL_classificazione');\"><img src=\"__css/classificazione_up.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"carica classificazione\" title=\"carica classificazione\" /></a>";

							## download classificazione	
							$tmp.="<a href=\"javascript:alert('Impossibile scaricare la classificazione dal server: classificazione non caricata.');\"><img src=\"__css/classificazione_down_off.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"scarica classificazione\" title=\"scarica classificazione\" /></a>";
							}

						else{

							## upload classificazione
							$tmp.="<a href=\"javascript:document.UploadClassificazione['ID_RIF'].value='".$ID_RIF."';javascript:scroll(0,0);upload('UPL_classificazione');\"><img src=\"__css/classificazione_up.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"carica classificazione\" title=\"carica classificazione\" /></a>";
							
							## download classificazione
							if(file_exists($classificazione_doc)) $ext=".doc";
							if(file_exists($classificazione_rtf)) $ext=".rtf";
							if(file_exists($classificazione_pdf)) $ext=".pdf";
							$tmp.="<a href=\"__upload/classificazioni/".$ID_RIF.$ext."\" target=\"_blank\"><img src=\"__css/classificazione_down_on.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"scarica classificazione\" title=\"scarica classificazione\" /></a>";
							}
						}



					$tmp.="</td>";
					
					$tmp.="<td class=\"Tcell\">";

					## ETICHETTA ##
					$tmp .= "<a href=\"javascript:EtichettePrintShowInterface('".$values["user_schede_rifiutiID_RIF"]."');\" title=\"stampa etichetta\" >";
					$tmp .= "<img src=\"__css/etichetta2".$values['user_schede_rifiutipericoloso'].".gif\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"stampa etichetta\"/>";
					$tmp .= "</a>";

					## PITTOGRAMMI x ETICHETTA ##
					$sql="select ID_R_PITT from user_schede_rifiuti_pittogrammi where ID_IMP='".$SOGER->UserData['core_impiantiID_IMP']."' and ID_RIF='".$values['user_schede_rifiutiID_RIF']."';";

					$$ObjName->SDBRead($sql,"DbRecordSetPittogrammi",true,false);

					if(isset($$ObjName->DbRecordSetPittogrammi[0]['ID_R_PITT'])){
						$urlPitt ="__scripts/FGE_DataGridEdit.php?Adempimento&amp;table=user_schede_rifiuti_pittogrammi&amp;ID_RIF=".$values['user_schede_rifiutiID_RIF']."&amp;pri=ID_R_PITT&filter=".$$ObjName->DbRecordSetPittogrammi[0]['ID_R_PITT']."&amp;FGE_action=edit&amp;name=". urlencode($values[$PrimaryTable . "descrizione"])."&amp;hash=".MakeUrlHash("user_schede_rifiuti_pittogrammi","ID_R_PITT",$$ObjName->DbRecordSetPittogrammi[0]['ID_R_PITT']);
						$tmp .= "<a href=\"".$urlPitt."\" title=\"pittogrammi di pericolo\">";
						$tmp .= "<img src=\"__css/pittogrammi_on.gif\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"pittogrammi di pericolo\"/>";
						$tmp .= "</a>";
						}
					else{
						$urlPitt="__scripts/FGE_DataGridEdit.php?Adempimento&amp;table=user_schede_rifiuti_pittogrammi&amp;ID_RIF=".$values['user_schede_rifiutiID_RIF']."&amp;FGE_action=edit&amp;name=". urlencode($values[$PrimaryTable . "descrizione"]);
						$tmp .= "<a href=\"".$urlPitt."\" title=\"pittogrammi di pericolo\">";
						$tmp .= "<img src=\"__css/pittogrammi_off.gif\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"pittogrammi di pericolo\"/>";
						$tmp .= "</a>";
						}


					## PROCEDURA DI SICUREZZA
					$sql="select ID_SK from user_schede_sicurezza where ID_IMP='".$SOGER->UserData['core_impiantiID_IMP']."' and ID_RIF='".$values['user_schede_rifiutiID_RIF']."';";

					$ID_RIF=$values['user_schede_rifiutiID_RIF'];

					$$ObjName->SDBRead($sql,"DbRecordSet3",true,false);

					if(isset($$ObjName->DbRecordSet3[0]['ID_SK'])){

						## configura scheda sicurezza
						$urlSchedaSic ="__scripts/FGE_DataGridEdit.php?Adempimento&amp;table=user_schede_sicurezza&amp;ID_RIF=".$values['user_schede_rifiutiID_RIF']."&amp;pri=ID_SK&filter=".$$ObjName->DbRecordSet3[0]['ID_SK']."&amp;FGE_action=edit&amp;name=". urlencode($values[$PrimaryTable . "descrizione"])."&amp;hash=".MakeUrlHash("user_schede_sicurezza","ID_SK",$$ObjName->DbRecordSet3[0]['ID_SK']);
						$tmp .= "<a href=\"".$urlSchedaSic."\" title=\"procedura di sicurezza\">";
						$tmp .= "<img src=\"__css/elmetto.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"procedura di sicurezza\"/>";
						$tmp .= "</a>";

						if($SOGER->UserData['core_usersG7']=='1'){
							## upload scheda sicurezza
							$tmp.="<a href=\"javascript:document.UploadSicurezza['ID_RIF'].value='".$ID_RIF."';javascript:scroll(0,0);upload('UPL_sicurezza');\"><img src=\"__css/elmetto_up_on.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" title=\"carica procedura di sicurezza\" alt=\"carica procedura di sicurezza\"/></a>";
							
							## download scheda sicurezza
							$scheda_doc = "../__upload/sicurezza/".$ID_RIF.".doc";
							$scheda_rtf = "../__upload/sicurezza/".$ID_RIF.".rtf";
							$scheda_pdf = "../__upload/sicurezza/".$ID_RIF.".pdf";
							if(!file_exists($scheda_doc) && !file_exists($scheda_rtf) && !file_exists($scheda_pdf))
								$tmp.="<a href=\"javascript:alert('Impossibile scaricare procedura di sicurezza dal server: procedura di sicurezza non caricata.');\"><img src=\"__css/elmetto_down_off.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" title=\"scarica procedura di sicurezza\" alt=\"scarica procedura di sicurezza\"/></a>";
							else{
								if(file_exists($scheda_doc)) $ext=".doc";
								if(file_exists($scheda_rtf)) $ext=".rtf";
								if(file_exists($scheda_pdf)) $ext=".pdf";
								$tmp.="<a href=\"__upload/sicurezza/".$ID_RIF.$ext."\" target=\"_blank\"><img src=\"__css/elmetto_down_on.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"scarica procedura sicurezza\"/></a>";
								}
							}
						}
					else{
						## configura scheda sicurezza
						$urlSchedaSic="__scripts/FGE_DataGridEdit.php?Adempimento&amp;table=user_schede_sicurezza&amp;ID_RIF=".$values['user_schede_rifiutiID_RIF']."&amp;FGE_action=edit&amp;name=". urlencode($values[$PrimaryTable . "descrizione"]);
						$tmp .= "<a href=\"".$urlSchedaSic."\" title=\"procedura di sicurezza\">";
						$tmp .= "<img src=\"__css/elmetto_gray.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"procedura di sicurezza\"/>";
						$tmp .= "</a>";

						if($SOGER->UserData['core_usersG7']=='1'){

							## upload scheda sicurezza
							$tmp.="<a href=\"javascript:document.UploadSicurezza['ID_RIF'].value='".$ID_RIF."';javascript:scroll(0,0);upload('UPL_sicurezza');\"><img src=\"__css/elmetto_up_on.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" title=\"carica procedura di sicurezza\" alt=\"carica procedura di sicurezza\"/></a>";

							## download scheda sicurezza
							$scheda_doc = "../__upload/sicurezza/".$ID_RIF.".doc";
							$scheda_rtf = "../__upload/sicurezza/".$ID_RIF.".rtf";
							$scheda_pdf = "../__upload/sicurezza/".$ID_RIF.".pdf";
							if(!file_exists($scheda_doc) && !file_exists($scheda_rtf) && !file_exists($scheda_pdf))
								$tmp.="<a href=\"javascript:alert('Impossibile scaricare procedura di sicurezza dal server: procedura di sicurezza non caricata.');\"><img src=\"__css/elmetto_down_off.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" title=\"scarica procedura di sicurezza\" alt=\"scarica procedura di sicurezza\"/></a>";
							else{
								if(file_exists($scheda_doc)) $ext=".doc";
								if(file_exists($scheda_rtf)) $ext=".rtf";
								if(file_exists($scheda_pdf)) $ext=".pdf";
								$tmp.="<a href=\"__upload/sicurezza/".$ID_RIF.$ext."\" target=\"_blank\"><img src=\"__css/elmetto_down_on.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"scarica procedura sicurezza\"/></a>";
								}
							}

						}


					## SEGNALETICA DI SICUREZZA x ETICHETTA

					if($SOGER->UserData['core_usersG4']==1){
						$sql="select ID_R_SYM from user_schede_rifiuti_etsym where ID_IMP='".$SOGER->UserData['core_impiantiID_IMP']."' and ID_RIF='".$values['user_schede_rifiutiID_RIF']."';";

						$$ObjName->SDBRead($sql,"DbRecordSet2",true,false);

						if(isset($$ObjName->DbRecordSet2[0]['ID_R_SYM'])){
							$urlSym ="__scripts/FGE_DataGridEdit.php?Adempimento&amp;table=user_schede_rifiuti_etsym&amp;ID_RIF=".$values['user_schede_rifiutiID_RIF']."&amp;pri=ID_R_SYM&filter=".$$ObjName->DbRecordSet2[0]['ID_R_SYM']."&amp;FGE_action=edit&amp;name=". urlencode($values[$PrimaryTable . "descrizione"])."&amp;hash=".MakeUrlHash("user_schede_rifiuti_etsym","ID_R_SYM",$$ObjName->DbRecordSet2[0]['ID_R_SYM']);
							$tmp .= "<a href=\"".$urlSym."\" title=\"segnaletica di sicurezza\">";
							$tmp .= "<img src=\"__css/sicurezza_on.gif\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"segnaletica di sicurezza\"/>";
							$tmp .= "</a>";
							}
						else{
							$urlSym="__scripts/FGE_DataGridEdit.php?Adempimento&amp;table=user_schede_rifiuti_etsym&amp;ID_RIF=".$values['user_schede_rifiutiID_RIF']."&amp;FGE_action=edit&amp;name=". urlencode($values[$PrimaryTable . "descrizione"]);
							$tmp .= "<a href=\"".$urlSym."\" title=\"segnaletica di sicurezza\">";
							$tmp .= "<img src=\"__css/sicurezza_off.gif\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"segnaletica di sicurezza\"/>";
							$tmp .= "</a>";
							}
						}
	
					$tmp.="</td>";


					$tmp.="<td class=\"Tcell\">";
					
					## CONTENITORI
					$sql="select ID_DEP from user_schede_rifiuti_deposito where ID_IMP='".$SOGER->UserData['core_impiantiID_IMP']."' and ID_RIF='".$values['user_schede_rifiutiID_RIF']."';";

					$$ObjName->SDBRead($sql,"DbRecordSet2",true,false);

					//print_r($$ObjName->DbRecordSet2[0]['ID_BUDG']);
					if(isset($$ObjName->DbRecordSet2[0]['ID_DEP'])){
						$urlDep ="__scripts/FGE_DataGridEdit.php?Adempimento&amp;table=user_schede_rifiuti_deposito&amp;ID_RIF=".$values['user_schede_rifiutiID_RIF']."&amp;pri=ID_DEP&filter=".$$ObjName->DbRecordSet2[0]['ID_DEP']."&amp;FGE_action=edit&amp;name=". urlencode($values[$PrimaryTable . "descrizione"])."&amp;hash=".MakeUrlHash("user_schede_rifiuti_deposito","ID_DEP",$$ObjName->DbRecordSet2[0]['ID_DEP']);
						$tmp .= "<a href=\"".$urlDep."\" title=\"organizza deposito\">";
						$tmp .= "<img src=\"__css/squadretta.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"organizza deposito\"/>";
						$tmp .= "</a>";
						}
					else{
						$urlDep="__scripts/FGE_DataGridEdit.php?Adempimento&amp;table=user_schede_rifiuti_deposito&amp;ID_RIF=".$values['user_schede_rifiutiID_RIF']."&amp;FGE_action=edit&amp;name=". urlencode($values[$PrimaryTable . "descrizione"]);
						$tmp .= "<a href=\"".$urlDep."\" title=\"organizza deposito\">";
						$tmp .= "<img src=\"__css/squadretta_gray.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"organizza deposito\"/>";
						$tmp .= "</a>";
						}


					## DEPOSITO 1 RIFIUTO

					$sql="SELECT ID_CONT FROM user_schede_rifiuti_deposito WHERE ID_RIF='".$values["user_schede_rifiutiID_RIF"]."'";
					$FEDIT->SDBRead($sql,"IdCont",true,false);

					if($FEDIT->IdCont[0]['ID_CONT']>0){
						
						$sql ="SELECT user_schede_rifiuti.FKEdisponibilita, user_schede_rifiuti.ID_UMIS, user_schede_rifiuti_deposito.NumCont, user_schede_rifiuti_deposito.MaxStock, lov_contenitori.portata, lov_contenitori.m_cubi ";
						$sql.="FROM user_schede_rifiuti_deposito ";
						$sql.="JOIN user_schede_rifiuti ON user_schede_rifiuti_deposito.ID_RIF=user_schede_rifiuti.ID_RIF ";
						$sql.="JOIN lov_contenitori ON user_schede_rifiuti_deposito.ID_CONT=lov_contenitori.ID_CONT ";
						$sql.="WHERE user_schede_rifiuti_deposito.ID_RIF='".$values["user_schede_rifiutiID_RIF"]."'";
						$FEDIT->SDBRead($sql,"InfoDep",true,false);
						
						if($FEDIT->InfoDep[0]['FKEdisponibilita']=='0')
							$percentuale=0;
						else{
							if($FEDIT->InfoDep[0]['MaxStock']>0) 
								$MAX=$FEDIT->InfoDep[0]['MaxStock']*$FEDIT->InfoDep[0]['NumCont'];
							else{
								switch($FEDIT->InfoDep[0]['ID_UMIS']){
									case 1:
										$MAX=$FEDIT->InfoDep[0]['portata']*$FEDIT->InfoDep[0]['NumCont'];
										break;
									case 2:
										$MAX=$FEDIT->InfoDep[0]['m_cubi']*1000*$FEDIT->InfoDep[0]['NumCont'];
										break;
									case 3:
										$MAX=$FEDIT->InfoDep[0]['m_cubi']*$FEDIT->InfoDep[0]['NumCont'];
										break;
									}
								}
							$percentuale=$FEDIT->InfoDep[0]['FKEdisponibilita'] / $MAX * 100;
							}

						if($percentuale>=90)					$color = "red";
						if($percentuale>=60 && $percentuale<90) $color = "orange";
						if($percentuale<60)						$color = "green";


						$tmp .= "<a href=\"__scripts/STATS_ContenitoreX1.php?ID_RIF=".$values["user_schede_rifiutiID_RIF"]."\" title=\"stato deposito singolo rifiuto\" >";
						$tmp .= "<img src=\"__css/brick_".$color.".gif\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"stato deposito singolo rifiuto\"/>";
						$tmp .= "</a>";
						}
					else{
						$tmp .= "<a href=\"javascript:window.alert('Attenzione! Non � stato selezionato alcun contenitore per il rifiuto');\" title=\"stato deposito singolo rifiuto\" >";
						$tmp .= "<img src=\"__css/brick_off.png\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"stato deposito singolo rifiuto\"/>";
						$tmp .= "</a>";
						}
					$tmp.="</td>";


					}
	

				if($SOGER->AppLocation=="UserSchedeRifiuti_caratterizzazioni"){
					
					$tmp.="<td class=\"Tcell\">";

					## CARATTERIZZAZIONE ##

					$ID_RIF=$values["user_schede_rifiutiID_RIF"];
					
					$caratterizzazione_doc = "../__upload/caratterizzazioni/".$ID_RIF.".doc";
					$caratterizzazione_rtf = "../__upload/caratterizzazioni/".$ID_RIF.".rtf";
					$caratterizzazione_pdf = "../__upload/caratterizzazioni/".$ID_RIF.".pdf";
					
					if(file_exists($caratterizzazione_doc) OR file_exists($caratterizzazione_rtf) OR file_exists($caratterizzazione_pdf))
						$gotCaratterizzazione = true;
					else
						$gotCaratterizzazione = false;

					if($values["user_schede_rifiutiID_FONTE_RIF"]!=0) $icona = "camera_on.gif"; else $icona = "camera_off.gif";
					$tmp .= "<a href=\"__scripts/statusCUSTOM_SchedaRifiutoCaratt.php?table=user_schede_rifiuti&amp;pri=ID_RIF&amp;filter=".$values["user_schede_rifiutiID_RIF"]."&amp;hash=".MakeUrlHash("user_schede_rifiuti","ID_RIF",$values["user_schede_rifiutiID_RIF"])."\" title=\"caratterizzazione rifiuto\" >";
					//$tmp .= "<a href=\"__scripts/status.php?action=edit&amp;area=UserSchedaRifiutoCaratt&amp;table=user_schede_rifiuti&amp;pri=ID_RIF&amp;filter=".$values["user_schede_rifiutiID_RIF"]."&amp;hash=".MakeUrlHash("user_schede_rifiuti","ID_RIF",$values["user_schede_rifiutiID_RIF"])."\" title=\"caratterizzazione rifiuto\" >";
					$tmp .= "<img src=\"__css/".$icona."\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"caratterizzazione rifiuto\"/>";
					$tmp .= "</a>";
					
					if($SOGER->UserData['core_usersG7']=='1'){

						if(!$gotCaratterizzazione){

							## upload caratterizzazione
							$tmp.="<a href=\"javascript:document.UploadCaratterizzazione['ID_RIF'].value='".$ID_RIF."';javascript:scroll(0,0);upload('UPL_caratterizzazione');\"><img src=\"__css/camera_up_on.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"carica caratterizzazione\" title=\"carica caratterizzazione\" /></a>";

							## download caratterizzazione	
							$tmp.="<a href=\"javascript:alert('Impossibile scaricare la caratterizzazione dal server: caratterizzazione non caricata.');\"><img src=\"__css/camera_down_off.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"scarica caratterizzazione\" title=\"scarica caratterizzazione\" /></a>";
							}

						else{

							## upload caratterizzazione
							$tmp.="<a href=\"javascript:document.UploadCaratterizzazione['ID_RIF'].value='".$ID_RIF."';javascript:scroll(0,0);upload('UPL_caratterizzazione');\"><img src=\"__css/camera_up_on.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"carica caratterizzazione\" title=\"carica caratterizzazione\" /></a>";
							
							## download caratterizzazione
							if(file_exists($caratterizzazione_doc)) $ext=".doc";
							if(file_exists($caratterizzazione_rtf)) $ext=".rtf";
							if(file_exists($caratterizzazione_pdf)) $ext=".pdf";
							$tmp.="<a href=\"__upload/caratterizzazioni/".$ID_RIF.$ext."\" target=\"_blank\"><img src=\"__css/camera_down_on.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"scarica caratterizzazione\" title=\"scarica caratterizzazione\" /></a>";
							}
						}
                                                
                                        ##approvazione##
					if($SOGER->UserData['core_usersR4']==1){
						
						if(!$gotCaratterizzazione)
							$onClick	= " onclick=\"javascript:alert('Impossibile procedere con l\'approvazione del documento: il documento non e\' stato caricato!');\" ";
						else
							$onClick	= " onclick=\"javascript:OpenISODialog('CAR', '".$values["user_schede_rifiutiID_RIF"]."', ".$values["user_schede_rifiutiCAR_approved"].", '".$values["user_schede_rifiutiCAR_approved_start"]."', '".$values["user_schede_rifiutiCAR_approved_end"]."');\" ";
						
						if($values["user_schede_rifiutiCAR_approved"]==1)
							$icon		= "iso_on.gif";
						else
							$icon		= "iso_off.gif";

						$tmp .= "<a style=\"cursor:pointer;\" ".$onClick." title=\"approva caratterizzazione rifiuto\" >";
						$tmp .= "<img src=\"__css/".$icon."\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"approva caratterizzazione rifiuto\"/>";
						$tmp .= "</a>";
						}

					$tmp.="</td>";
					}
                                        
                                ## APP LOCATION: UserSchedeRifiuti_classificazioni
				## - classificazioni
                                if($SOGER->AppLocation=="UserSchedeRifiuti_classificazioni"){
					$tmp.="<td class=\"Tcell\">";

					## CLASSIFICAZIONE ##

					$ID_RIF=$values["user_schede_rifiutiID_RIF"];

					$classificazione_doc = "../__upload/classificazioni/".$ID_RIF.".doc";
					$classificazione_rtf = "../__upload/classificazioni/".$ID_RIF.".rtf";
					$classificazione_pdf = "../__upload/classificazioni/".$ID_RIF.".pdf";

					if(file_exists($classificazione_doc) OR file_exists($classificazione_rtf) OR file_exists($classificazione_pdf))
						$gotClassificazione = true;
					else
						$gotClassificazione = false;

					if($SOGER->UserData['core_usersG7']=='1'){

						if(!$gotClassificazione){

							## upload classificazione
							$tmp.="<a href=\"javascript:document.UploadClassificazione['ID_RIF'].value='".$ID_RIF."';javascript:scroll(0,0);upload('UPL_classificazione');\"><img src=\"__css/classificazione_up.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"carica classificazione\" title=\"carica classificazione\" /></a>";

							## download classificazione	
							$tmp.="<a href=\"javascript:alert('Impossibile scaricare la classificazione dal server: classificazione non caricata.');\"><img src=\"__css/classificazione_down_off.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"scarica classificazione\" title=\"scarica classificazione\" /></a>";
							}

						else{

							## upload classificazione
							$tmp.="<a href=\"javascript:document.UploadClassificazione['ID_RIF'].value='".$ID_RIF."';javascript:scroll(0,0);upload('UPL_classificazione');\"><img src=\"__css/classificazione_up.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"carica classificazione\" title=\"carica classificazione\" /></a>";
							
							## download classificazione
							if(file_exists($classificazione_doc)) $ext=".doc";
							if(file_exists($classificazione_rtf)) $ext=".rtf";
							if(file_exists($classificazione_pdf)) $ext=".pdf";
							$tmp.="<a href=\"__upload/classificazioni/".$ID_RIF.$ext."\" target=\"_blank\"><img src=\"__css/classificazione_down_on.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"scarica classificazione\" title=\"scarica classificazione\" /></a>";
							}
						}
                                                
                                        ##approvazione##
					if($SOGER->UserData['core_usersR4']==1){
						
						if(!$gotClassificazione)
							$onClick	= " onclick=\"javascript:alert('Impossibile procedere con l\'approvazione del documento: il documento non e\' stato caricato!');\" ";
						else
							$onClick	= " onclick=\"javascript:OpenISODialog('CLASS', '".$values["user_schede_rifiutiID_RIF"]."', ".$values["user_schede_rifiutiCLASS_approved"].", '".$values["user_schede_rifiutiCLASS_approved_start"]."', '".$values["user_schede_rifiutiCLASS_approved_end"]."');\" ";
						
						if($values["user_schede_rifiutiCLASS_approved"]==1)
							$icon		= "iso_on.gif";
						else
							$icon		= "iso_off.gif";

						$tmp .= "<a style=\"cursor:pointer;\" ".$onClick." title=\"approva classificazione rifiuto\" >";
						$tmp .= "<img src=\"__css/".$icon."\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"approva classificazione rifiuto\"/>";
						$tmp .= "</a>";
						}

					$tmp.="</td>";                                
                                }
                                
                                ## APP LOCATION: UserSchedeRifiuti_analisi
                                ## - liste analiti
				## - analisi di laboratorio                                
                                if($SOGER->AppLocation=="UserSchedeRifiuti_analisi"){
                                    
                                    $tmp.="<td class=\"Tcell\">";

					## LISTA ANALITI ##

					$ID_RIF=$values["user_schede_rifiutiID_RIF"];

					$analiti_doc = "../__upload/liste_analiti/".$ID_RIF.".doc";
					$analiti_rtf = "../__upload/liste_analiti/".$ID_RIF.".rtf";
					$analiti_pdf = "../__upload/liste_analiti/".$ID_RIF.".pdf";

					if(file_exists($analiti_doc) OR file_exists($analiti_rtf) OR file_exists($analiti_pdf))
						$gotAnaliti = true;
					else
						$gotAnaliti = false;

					if($SOGER->UserData['core_usersG7']=='1'){

						if(!$gotAnaliti){

							## upload lista analiti
							$tmp.="<a href=\"javascript:document.UploadAnaliti['ID_RIF'].value='".$ID_RIF."';javascript:scroll(0,0);upload('UPL_analiti');\"><img src=\"__css/analiti_up.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"carica lista analiti\" title=\"carica lista analiti\" /></a>";

							## download lista analiti	
							$tmp.="<a href=\"javascript:alert('Impossibile scaricare la lista analiti dal server: lista analiti non caricata.');\"><img src=\"__css/analiti_down_off.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"scarica lista analiti\" title=\"scarica lista analiti\" /></a>";
							}

						else{

							## upload lista analiti
							$tmp.="<a href=\"javascript:document.UploadAnaliti['ID_RIF'].value='".$ID_RIF."';javascript:scroll(0,0);upload('UPL_analiti');\"><img src=\"__css/analiti_up.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"carica lista analiti\" title=\"carica lista analiti\" /></a>";
							
							## download lista analiti
							if(file_exists($analiti_doc)) $ext=".doc";
							if(file_exists($analiti_rtf)) $ext=".rtf";
							if(file_exists($analiti_pdf)) $ext=".pdf";
							$tmp.="<a href=\"__upload/liste_analiti/".$ID_RIF.$ext."\" target=\"_blank\"><img src=\"__css/analiti_down_on.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"scarica lista analiti\" title=\"scarica lista analiti\" /></a>";
							}
						}
                                                
                                        $tmp.="</td>";
                                    
					$tmp.="<td class=\"Tcell\">";

					## ANALISI
					$ID_RIF=$values['user_schede_rifiutiID_RIF'];				
					
					$analisi_pdf = "../__upload/analisi/".$ID_RIF.".pdf";
					
					if(file_exists($analisi_pdf))
						$gotAnalisi = true;
					else
						$gotAnalisi = false;


					if($SOGER->UserData['core_usersG7']=='1'){
						if(!$gotAnalisi){

							## upload analisi
							$tmp.="<a href=\"javascript:document.UploadAnalisi['ID_RIF'].value='".$ID_RIF."';javascript:scroll(0,0);upload('UPL_analisi');\"><img src=\"__css/analisi_up.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"carica analisi\" title=\"carica analisi\" /></a>";

							## download analisi	
							$tmp.="<a href=\"javascript:alert('Impossibile scaricare l'analisi dal server: analisi non caricata.');\"><img src=\"__css/analisi_down_off.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"scarica analisi\" title=\"scarica analisi\" /></a>";
							}

						else{

							## upload analisi
							$tmp.="<a href=\"javascript:document.UploadAnalisi['ID_RIF'].value='".$ID_RIF."';javascript:scroll(0,0);upload('UPL_analisi');\"><img src=\"__css/analisi_up.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"carica analisi\" title=\"carica analisi\" /></a>";
							
							## download analisi
							if(file_exists($analisi_pdf)) $ext=".pdf";
							$tmp.="<a href=\"__upload/analisi/".$ID_RIF.$ext."\" target=\"_blank\"><img src=\"__css/analisi_down.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"scarica analisi\" title=\"scarica analisi\" /></a>";
							}
						}
                                                
					##approvazione##
					if($SOGER->UserData['core_usersR4']==1){
						
						if(!$gotAnalisi)
							$onClick	= " onclick=\"javascript:alert('Impossibile procedere con l\'approvazione del documento: il documento non e\' stato caricato!');\" ";
						else
							$onClick	= " onclick=\"javascript:OpenISODialog('CAR_Analisi', '".$values["user_schede_rifiutiID_RIF"]."', ".$values["user_schede_rifiutiCAR_Analisi_approved"].", '".$values["user_schede_rifiutiCAR_Analisi_approved_start"]."', '".$values["user_schede_rifiutiCAR_Analisi_approved_end"]."');\" ";
						
						if($values["user_schede_rifiutiCAR_Analisi_approved"]==1)
							$icon		= "iso_on.gif";
						else
							$icon		= "iso_off.gif";

						$tmp .= "<a style=\"cursor:pointer;\" ".$onClick." title=\"approva analisi rifiuto\" >";
						$tmp .= "<img src=\"__css/".$icon."\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"approva analisi rifiuto\"/>";
						$tmp .= "</a>";
						}

					$tmp.="</td>";					                                
                                }

				## APP LOCATION: UserSchedeRifiuti_pericolosita
				## - etichetta
				## - procedure di sicurezza
				## - segnaletica di sicurezza
				## - upload scheda sicurezza
				## - download scheda sicurezza

				if($SOGER->AppLocation=="UserSchedeRifiuti_pericolosita"){

					$tmp.="<td class=\"Tcell\">";

					## ETICHETTA ##

					## selezione pittogrammi ##
					$sql="select ID_R_PITT from user_schede_rifiuti_pittogrammi where ID_IMP='".$SOGER->UserData['core_impiantiID_IMP']."' and ID_RIF='".$values['user_schede_rifiutiID_RIF']."';";

					$$ObjName->SDBRead($sql,"DbRecordSetPittogrammi",true,false);

					if(isset($$ObjName->DbRecordSetPittogrammi[0]['ID_R_PITT'])){
						$urlPitt ="__scripts/FGE_DataGridEdit.php?table=user_schede_rifiuti_pittogrammi&amp;ID_RIF=".$values['user_schede_rifiutiID_RIF']."&amp;pri=ID_R_PITT&filter=".$$ObjName->DbRecordSetPittogrammi[0]['ID_R_PITT']."&amp;FGE_action=edit&amp;name=". urlencode($values[$PrimaryTable . "descrizione"])."&amp;hash=".MakeUrlHash("user_schede_rifiuti_pittogrammi","ID_R_PITT",$$ObjName->DbRecordSetPittogrammi[0]['ID_R_PITT']);
						$tmp .= "<a href=\"".$urlPitt."\" title=\"pittogrammi di pericolo\">";
						$tmp .= "<img src=\"__css/pittogrammi_on.gif\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"pittogrammi di pericolo\"/>";
						$tmp .= "</a>";
						}
					else{
						$urlPitt="__scripts/FGE_DataGridEdit.php?table=user_schede_rifiuti_pittogrammi&amp;ID_RIF=".$values['user_schede_rifiutiID_RIF']."&amp;FGE_action=edit&amp;name=". urlencode($values[$PrimaryTable . "descrizione"]);
						$tmp .= "<a href=\"".$urlPitt."\" title=\"pittogrammi di pericolo\">";
						$tmp .= "<img src=\"__css/pittogrammi_off.gif\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"pittogrammi di pericolo\"/>";
						$tmp .= "</a>";
						}
                                                
                                        ## stampa ##
					$tmp .= "<a href=\"javascript:EtichettePrintShowInterface('".$values["user_schede_rifiutiID_RIF"]."');\" title=\"stampa etichetta\" >";
					$tmp .= "<img src=\"__css/etichetta2".$values['user_schede_rifiutipericoloso'].".gif\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"stampa etichetta\"/>";
					$tmp .= "</a>";
                                        
                                        ## approvazione ##
					if($SOGER->UserData['core_usersR4']==1){
						
						$onClick	= " onclick=\"javascript:OpenISODialog('et', '".$values["user_schede_rifiutiID_RIF"]."', ".$values["user_schede_rifiutiet_approved"].", '".$values["user_schede_rifiutiet_approved_start"]."', '".$values["user_schede_rifiutiet_approved_end"]."');\" ";
						
						if($values["user_schede_rifiutiet_approved"]==1)
							$icon		= "iso_on.gif";
						else
							$icon		= "iso_off.gif";

						$tmp .= "<a style=\"cursor:pointer;\" ".$onClick." title=\"approva etichetta rifiuto\" >";
						$tmp .= "<img src=\"__css/".$icon."\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"approva etichetta rifiuto\"/>";
						$tmp .= "</a>";
						}
					
					$tmp.="</td>";
					
					$tmp.="<td class=\"Tcell\">";


					## PROCEDURA DI SICUREZZA

					$ID_RIF=$values['user_schede_rifiutiID_RIF'];

					$scheda_doc = "../__upload/sicurezza/".$ID_RIF.".doc";
					$scheda_rtf = "../__upload/sicurezza/".$ID_RIF.".rtf";
					$scheda_pdf = "../__upload/sicurezza/".$ID_RIF.".pdf";
					if(file_exists($scheda_doc) OR file_exists($scheda_rtf) OR file_exists($scheda_pdf))
						$gotSicurezza = true;
					else
						$gotSicurezza = false;

					$sql="select ID_SK from user_schede_sicurezza where ID_IMP='".$SOGER->UserData['core_impiantiID_IMP']."' and ID_RIF='".$values['user_schede_rifiutiID_RIF']."';";
					$$ObjName->SDBRead($sql,"DbRecordSet3",true,false);

					if(isset($$ObjName->DbRecordSet3[0]['ID_SK'])){

						## configura scheda sicurezza
						$urlSchedaSic ="__scripts/FGE_DataGridEdit.php?table=user_schede_sicurezza&amp;ID_RIF=".$values['user_schede_rifiutiID_RIF']."&amp;pri=ID_SK&filter=".$$ObjName->DbRecordSet3[0]['ID_SK']."&amp;FGE_action=edit&amp;name=". urlencode($values[$PrimaryTable . "descrizione"])."&amp;hash=".MakeUrlHash("user_schede_sicurezza","ID_SK",$$ObjName->DbRecordSet3[0]['ID_SK']);
						$tmp .= "<a href=\"".$urlSchedaSic."\" title=\"procedura di sicurezza\">";
						$tmp .= "<img src=\"__css/elmetto.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"procedura di sicurezza\"/>";
						$tmp .= "</a>";
					
						if($SOGER->UserData['core_usersG7']=='1'){

							## upload scheda sicurezza
							$tmp.="<a href=\"javascript:document.UploadSicurezza['ID_RIF'].value='".$ID_RIF."';javascript:scroll(0,0);upload('UPL');\"><img src=\"__css/elmetto_up_on.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" title=\"carica procedura di sicurezza\" alt=\"carica procedura di sicurezza\"/></a>";
							
							## download scheda sicurezza
							if(!$gotSicurezza)
								$tmp.="<a href=\"javascript:alert('Impossibile scaricare procedura di sicurezza dal server: procedura di sicurezza non caricata.');\"><img src=\"__css/elmetto_down_off.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" title=\"scarica procedura di sicurezza\" alt=\"scarica procedura di sicurezza\"/></a>";
							else{
								if(file_exists($scheda_doc)) $ext=".doc";
								if(file_exists($scheda_rtf)) $ext=".rtf";
								if(file_exists($scheda_pdf)) $ext=".pdf";
								$tmp.="<a href=\"__upload/sicurezza/".$ID_RIF.$ext."\" target=\"_blank\"><img src=\"__css/folder_page.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"scarica procedura sicurezza\"/></a>";
								}
							}
						}
					else{
						## configura scheda sicurezza
						$urlSchedaSic="__scripts/FGE_DataGridEdit.php?table=user_schede_sicurezza&amp;ID_RIF=".$values['user_schede_rifiutiID_RIF']."&amp;FGE_action=edit&amp;name=". urlencode($values[$PrimaryTable . "descrizione"]);
						$tmp .= "<a href=\"".$urlSchedaSic."\" title=\"procedura di sicurezza\">";
						$tmp .= "<img src=\"__css/elmetto_gray.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"procedura di sicurezza\"/>";
						$tmp .= "</a>";

						if($SOGER->UserData['core_usersG7']=='1'){

							## upload scheda sicurezza
							$tmp.="<a href=\"javascript:document.UploadSicurezza['ID_RIF'].value='".$ID_RIF."';javascript:scroll(0,0);upload('UPL');\"><img src=\"__css/elmetto_up_on.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" title=\"carica procedura di sicurezza\" alt=\"carica procedura di sicurezza\"/></a>";

							## download scheda sicurezza
							if(!$gotSicurezza)
								$tmp.="<a href=\"javascript:alert('Impossibile scaricare procedura di sicurezza dal server: procedura di sicurezza non caricata.');\"><img src=\"__css/elmetto_down_off.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" title=\"scarica procedura di sicurezza\" alt=\"scarica procedura di sicurezza\"/></a>";
							else{
								if(file_exists($scheda_doc)) $ext=".doc";
								if(file_exists($scheda_rtf)) $ext=".rtf";
								if(file_exists($scheda_pdf)) $ext=".pdf";
								$tmp.="<a href=\"__upload/sicurezza/".$ID_RIF.$ext."\" target=\"_blank\"><img src=\"__css/elmetto_down_on.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"scarica procedura sicurezza\"/></a>";
								}
							}

						}


					## SEGNALETICA DI SICUREZZA x ETICHETTA
					$sql="select ID_R_SYM from user_schede_rifiuti_etsym where ID_IMP='".$SOGER->UserData['core_impiantiID_IMP']."' and ID_RIF='".$values['user_schede_rifiutiID_RIF']."';";

					$$ObjName->SDBRead($sql,"DbRecordSet2",true,false);

					if(isset($$ObjName->DbRecordSet2[0]['ID_R_SYM'])){
						$urlSym ="__scripts/FGE_DataGridEdit.php?table=user_schede_rifiuti_etsym&amp;ID_RIF=".$values['user_schede_rifiutiID_RIF']."&amp;pri=ID_R_SYM&filter=".$$ObjName->DbRecordSet2[0]['ID_R_SYM']."&amp;FGE_action=edit&amp;name=". urlencode($values[$PrimaryTable . "descrizione"])."&amp;hash=".MakeUrlHash("user_schede_rifiuti_etsym","ID_R_SYM",$$ObjName->DbRecordSet2[0]['ID_R_SYM']);
						$tmp .= "<a href=\"".$urlSym."\" title=\"segnaletica di sicurezza\">";
						$tmp .= "<img src=\"__css/sicurezza_on.gif\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"segnaletica di sicurezza\"/>";
						$tmp .= "</a>";
						}
					else{
						$urlSym="__scripts/FGE_DataGridEdit.php?table=user_schede_rifiuti_etsym&amp;ID_RIF=".$values['user_schede_rifiutiID_RIF']."&amp;FGE_action=edit&amp;name=". urlencode($values[$PrimaryTable . "descrizione"]);
						$tmp .= "<a href=\"".$urlSym."\" title=\"segnaletica di sicurezza\">";
						$tmp .= "<img src=\"__css/sicurezza_off.gif\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"segnaletica di sicurezza\"/>";
						$tmp .= "</a>";
						}
                                                
                                        ##approvazione##
					if($SOGER->UserData['core_usersR4']==1){
						
						if(!$gotSicurezza)
							$onClick	= " onclick=\"javascript:alert('Impossibile procedere con l\'approvazione del documento: il documento non e\' stato caricato!');\" ";
						else
							$onClick	= " onclick=\"javascript:OpenISODialog('sk', '".$values["user_schede_rifiutiID_RIF"]."', ".$values["user_schede_rifiutisk_approved"].", '".$values["user_schede_rifiutisk_approved_start"]."', '".$values["user_schede_rifiutisk_approved_end"]."');\" ";
						
						if($values["user_schede_rifiutisk_approved"]==1)
							$icon		= "iso_on.gif";
						else
							$icon		= "iso_off.gif";

						$tmp .= "<a style=\"cursor:pointer;\" ".$onClick." title=\"approva procedura di sicurezza\" >";
						$tmp .= "<img src=\"__css/".$icon."\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"approva procedura di sicurezza\"/>";
						$tmp .= "</a>";
						}

					$tmp.="</td>";

					}


				## APP LOCATION: UserGestioneDeposito
				## - richiesta carico
				## - richiesta scarico
				## - richiesta conferimento
				## - stato deposito x 1
				## - stato deposito x tutti

				if($SOGER->AppLocation=="UserGestioneDeposito"){

					$tmp.="<td class=\"Tcell\">";


					## RICHIESTA DI CARICO
					$tmp .= "<a href=\"__scripts/RichiesteCaricoScaricoRTF.php?table=user_schede_rifiuti&amp;pri=ID_RIF&amp;filter=" . $values["user_schede_rifiutiID_RIF"] . "&amp;FGE_action=carico\" title=\"richiesta di carico\" >";
					$tmp .= "<img src=\"__css/rich_carico.png\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"richiesta di carico\"/>";
					$tmp .= "</a>";

					## RICHIESTA DI SCARICO
					$tmp .= "<a href=\"__scripts/RichiesteCaricoScaricoRTF.php?table=user_schede_rifiuti&amp;pri=ID_RIF&amp;filter=" . $values["user_schede_rifiutiID_RIF"] . "&amp;FGE_action=scarico\" title=\"richiesta di scarico\" >";
					$tmp .= "<img src=\"__css/rich_scarico.png\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"richiesta di scarico\"/>";
					$tmp .= "</a>";

				
					## RICHIESTA DI CONFERIMENTO
					if($SOGER->UserData["workmode"] == "produttore"){
						$tmp .= "<a href=\"__scripts/status.php?area=ConferimentoRifiuto&filter=" . $values["user_schede_rifiutiID_RIF"] . "\" title=\"conferimento rifiuto\" >";
						$tmp .= "<img src=\"__css/conferimento.gif\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"conferimento rifiuto\"/>";
						$tmp .= "</a>";	
						}

					## DEPOSITO 1 RIFIUTO

					$sql="SELECT ID_CONT FROM user_schede_rifiuti_deposito WHERE ID_RIF='".$values["user_schede_rifiutiID_RIF"]."'";
					$FEDIT->SDBRead($sql,"IdCont",true,false);

					if($FEDIT->IdCont[0]['ID_CONT']>0){
						
						$sql ="SELECT user_schede_rifiuti.FKEdisponibilita, user_schede_rifiuti.ID_UMIS, user_schede_rifiuti_deposito.NumCont, user_schede_rifiuti_deposito.MaxStock, lov_contenitori.portata, lov_contenitori.m_cubi ";
						$sql.="FROM user_schede_rifiuti_deposito ";
						$sql.="JOIN user_schede_rifiuti ON user_schede_rifiuti_deposito.ID_RIF=user_schede_rifiuti.ID_RIF ";
						$sql.="JOIN lov_contenitori ON user_schede_rifiuti_deposito.ID_CONT=lov_contenitori.ID_CONT ";
						$sql.="WHERE user_schede_rifiuti_deposito.ID_RIF='".$values["user_schede_rifiutiID_RIF"]."'";
						$FEDIT->SDBRead($sql,"InfoDep",true,false);
						
						if($FEDIT->InfoDep[0]['FKEdisponibilita']=='0')
                                                    $percentuale=0;
						else{
                                                    if($FEDIT->InfoDep[0]['MaxStock']>0) 
                                                        $MAX=$FEDIT->InfoDep[0]['MaxStock']*$FEDIT->InfoDep[0]['NumCont'];
                                                    else{
                                                        switch($FEDIT->InfoDep[0]['ID_UMIS']){
                                                            case 1:
                                                                $MAX=$FEDIT->InfoDep[0]['portata']*$FEDIT->InfoDep[0]['NumCont'];
                                                                break;
                                                            case 2:
                                                                $MAX=$FEDIT->InfoDep[0]['m_cubi']*1000*$FEDIT->InfoDep[0]['NumCont'];
                                                                break;
                                                            case 3:
                                                                $MAX=$FEDIT->InfoDep[0]['m_cubi']*$FEDIT->InfoDep[0]['NumCont'];
                                                                break;
                                                        }
                                                    }
                                                    $percentuale=$MAX>0? $FEDIT->InfoDep[0]['FKEdisponibilita'] / $MAX * 100 : 0;
                                                }

						if($percentuale>=90)			$color = "red"; 
						if($percentuale>=60 && $percentuale<90) $color = "orange";
						if($percentuale<60)			$color = "green";


						$tmp .= "<a href=\"__scripts/STATS_ContenitoreX1.php?ID_RIF=".$values["user_schede_rifiutiID_RIF"]."\" title=\"stato deposito singolo rifiuto\" >";
						$tmp .= "<img src=\"__css/brick_".$color.".gif\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"stato deposito singolo rifiuto\"/>";
						$tmp .= "</a>";                                                
						}
					else{
						$tmp .= "<a href=\"javascript:window.alert('Attenzione! Non � stato selezionato alcun contenitore per il rifiuto');\" title=\"stato deposito singolo rifiuto\" >";
						$tmp .= "<img src=\"__css/brick_off.png\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"stato deposito singolo rifiuto\"/>";
						$tmp .= "</a>";
						}

					## DEPOSITO X RIFIUTI
					/*
					$tmp .= "<a href=\"#\" title=\"stato deposito rifiuti\" >";
					$tmp .= "<img src=\"__css/bricks_off.png\" class=\"NoBorder\" style=\"margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"stato deposito rifiuti\"/>";
					$tmp .= "</a>";
					*/


					$tmp .="</td>";

					}

				}
				
				
				
				
				## APP LOCATION: UserGestioneDeposito_organizza
				## - configura contenitori

				if($SOGER->AppLocation=="UserGestioneDeposito_organizza"){

					## CONTENITORI
					$sql="select ID_DEP from user_schede_rifiuti_deposito where ID_IMP='".$SOGER->UserData['core_impiantiID_IMP']."' and ID_RIF='".$values['user_schede_rifiutiID_RIF']."';";

					$$ObjName->SDBRead($sql,"DbRecordSet2",true,false);

					//print_r($$ObjName->DbRecordSet2[0]['ID_BUDG']);
					$tmp .= "<td class=\"Tcell\">";
					if(isset($$ObjName->DbRecordSet2[0]['ID_DEP'])){
						$urlDep ="__scripts/FGE_DataGridEdit.php?table=user_schede_rifiuti_deposito&amp;ID_RIF=".$values['user_schede_rifiutiID_RIF']."&amp;pri=ID_DEP&filter=".$$ObjName->DbRecordSet2[0]['ID_DEP']."&amp;FGE_action=edit&amp;name=". urlencode($values[$PrimaryTable . "descrizione"])."&amp;hash=".MakeUrlHash("user_schede_rifiuti_deposito","ID_DEP",$$ObjName->DbRecordSet2[0]['ID_DEP']);
						$tmp .= "<a href=\"".$urlDep."\" title=\"organizza deposito\">";
						$tmp .= "<img src=\"__css/squadretta.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"organizza deposito\"/>";
						$tmp .= "</a>";
						}
					else{
						$urlDep="__scripts/FGE_DataGridEdit.php?table=user_schede_rifiuti_deposito&amp;ID_RIF=".$values['user_schede_rifiutiID_RIF']."&amp;FGE_action=edit&amp;name=". urlencode($values[$PrimaryTable . "descrizione"]);
						$tmp .= "<a href=\"".$urlDep."\" title=\"organizza deposito\">";
						$tmp .= "<img src=\"__css/squadretta_gray.gif\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"organizza deposito\"/>";
						$tmp .= "</a>";
						}

					## STAT PREV_NextS

					$tmp .= "<a href=\"__scripts/status.php?area=UserStatsCfg&action=Gogo&Stat=PrevisioneConferimenti\" title=\"previsione conferimenti\">";
					$tmp .= "<img src=\"__css/time.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"previsione conferimenti\"/>";
					$tmp .= "</a>";

					$tmp .="</td>";

					}			


			if($AllowEdit | $AllowDelete | $AllowClone | $AllowPrint | $SOGER->AppLocation=="UserGestioneDeposito_organizza") {
				$tmp .= "</tr>\n";
			}
		} 
	}

	$tmp .= "</table>";

	echo utf8_encode($tmp);
	if($ObjName=="FEDIT") {
		require_once("../__includes/COMMON_sleepForgEdit.php");
	} else {
		require_once("../__includes/COMMON_sleepForgEditDG.php");		
	}
	return;
?>
