<?php
session_start();
require_once("ForgEdit_includes.inc");
require_once("../__libs/SQLFunct.php");
require_once("../__includes/COMMON_wakeForgEdit.php");

	$TableName=$_GET["TableName"];
	if($TableName=="user_movimenti") $pri="ID_MOV"; else $pri="ID_MOV_F";

	# RICAVO ULTIMO CARICO
	$SQL = "SELECT ".$pri.", produttore FROM ".$TableName." WHERE TIPO='C' AND ID_IMP='".$_GET['ID_IMP']."' AND ID_RIF=".$_GET['ID_RIF']." AND NMOV<".$_GET['NMOV']." AND produttore=".$_GET['produttore']." AND trasportatore=".$_GET['trasportatore']." AND destinatario=".$_GET['destinatario']." AND intermediario=".$_GET['intermediario']." ORDER BY NMOV DESC LIMIT 0, 1;";
	$FEDIT->SDBRead($SQL,"DbRecordSet",true,false);
	$ID_CARICO = $FEDIT->DbRecordSet[0][$pri];

	# AGGIORNO QUANTITA
	$SQL = "UPDATE ".$TableName." SET quantita=quantita+" . $_GET["DIFF"] . ", qta_hidden=qta_hidden+".$_GET["DIFF"].", pesoN=pesoN+" . $_GET["KG"] . ",tara=0,pesoL=0 WHERE ".$pri."=" . $ID_CARICO;
	$FEDIT->SDBWrite($SQL,true,false);
		
	# SE HO FATTO LA MODIFICA SULLA TABELLA user_movimenti_fiscalizzati, DEVO AGGIORNARE ANCHE L'INDUSTRIALE!!!
	if($_GET[$pri]!='' AND $TableName=='user_movimenti_fiscalizzati' AND $FEDIT->DbRecordSet[0]['produttore']==1){
	
		$ID_SCARICO = $_GET[$pri];

		// Ricavo carico industriale // |0000222041||0000222082|
		$SQL ="SELECT FKErifInd FROM user_movimenti_fiscalizzati WHERE ID_MOV_F=".$ID_CARICO.";";
		$FEDIT->SDBRead($SQL,"DbRecordSet",true,false);
		$FKErifInd			= str_replace("||", "|", $FEDIT->DbRecordSet[0]['FKErifInd']);
		$FKErifIndArray		= explode("|", $FKErifInd);
		$FKErifIndArray		= array_filter($FKErifIndArray, 'strlen');
		$FKErifIndArray		= array_values($FKErifIndArray);
		$ID_MOV_IND			= $FKErifIndArray[0];
		$SQL = "UPDATE user_movimenti SET quantita=quantita+" . $_GET["DIFF"] . ", qta_hidden=qta_hidden+".$_GET["DIFF"].", pesoN=pesoN+" . $_GET["KG"] . ",tara=0,pesoL=0 WHERE ID_MOV=" . $ID_MOV_IND;
		$FEDIT->SDBWrite($SQL,true,false);

		// Ricavo scarico industriale // |0000222041|
		$SQL ="SELECT FKErifInd FROM user_movimenti_fiscalizzati WHERE ID_MOV_F=".$ID_SCARICO.";";
		$FEDIT->SDBRead($SQL,"DbRecordSet",true,false);
		$FKErifInd			= str_replace("||", "|", $FEDIT->DbRecordSet[0]['FKErifInd']);
		$FKErifIndArray		= explode("|", $FKErifInd);
		$FKErifIndArray		= array_filter($FKErifIndArray, 'strlen');
		$FKErifIndArray		= array_values($FKErifIndArray);
		$ID_MOV_IND			= $FKErifIndArray[0];
		$SQL = "UPDATE user_movimenti SET quantita=quantita+" . $_GET["DIFF"] . ", qta_hidden=qta_hidden+".$_GET["DIFF"].", pesoN=pesoN+" . $_GET["KG"] . ",tara=".$_GET['tara'].",pesoL=".$_GET['pesoL']." WHERE ID_MOV=" . $ID_MOV_IND;
		$FEDIT->SDBWrite($SQL,true,false);

		}



	/*
	$sql = "UPDATE ".$TableName." SET quantita=quantita+" . $_GET["DIFF"] . ", qta_hidden=qta_hidden+".$_GET["DIFF"].", pesoN=pesoN+" . $_GET["KG"] . ",tara=0,pesoL=0 WHERE ".$pri."='" . $_GET[$pri] . "'";
	$FEDIT->SDBWrite($sql,true,false);
	*/

require_once("../__includes/COMMON_sleepForgEdit.php");

?>