<?php

$counter = 0;
$showGiacIni=false;
$DbRecsNum = $FEDIT->DbRecsNum;

if($SOGER->UserData["workmode"]!="produttore") $showGiacIni=true;

foreach($FEDIT->DbRecordSet as $k=>$dati) {
	if($PrintImpianto) {
		if(($tmpCER!=$dati["CERref"] | $dati["IMPref"]!=$tmpIMP | $dati["AZref"]!=$tmpAZ) && $counter>0) {
			if($counterVar>0) $VarMedia=$TotVar/$counterVar; else $VarMedia=$counterVar;
			SommaViaggi($Ypos,$TotScarico,$TotCosto,$TotKm,$TotTime,$FEDIT->FGE_PdfBuffer,$asCSV,$CSVbuf);
		}
	} else {
		if(($tmpCER!=$dati["CERref"] | $dati["AZref"]!=$tmpAZ) && $counter>0) {
			if($counterVar>0) $VarMedia=$TotVar/$counterVar; else $VarMedia=$counterVar;
			SommaViaggi($Ypos,$TotScarico,$TotCosto,$TotKm,$TotTime,$FEDIT->FGE_PdfBuffer,$asCSV,$CSVbuf);
		}		
	}
	if($dati["AZref"]!=$tmpAZ) {
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
		# dati azienda
		$Ypos  += 10;
		$tmpAZ  = $dati["AZref"];
		$tmpIMP = "";
		$tmpCER = "";
//{{{ 
		if(!$asCSV) {		
			$Ypos=20;
			$FEDIT->FGE_PdfBuffer->AddPage();
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
			$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$dati["AZdes"],0,"L");
			$Ypos += 4;
			$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
			$AzAddr = $dati["AZind"] . " - " . $dati["AZcom"] . " (" . $dati["AZshprov"] . ") - " . $dati["AZnaz"];    
			$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$AzAddr,0,"L");
			$Ypos += 4; 
			} 
		else {
			$azienda = $dati["AZdes"] . " " . $dati["AZind"] . " - " . $dati["AZcom"] . " (" . $dati["AZshprov"] . ") - " . $dati["AZnaz"];
			$CSVbuf .= AddCSVcolumn($azienda);
			$CSVbuf .= AddCSVRow();
			}
	}
	if($PrintImpianto) {
		if($dati["IMPref"]!=$tmpIMP) {
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
			$tmpIMP = $dati["IMPref"];
			$tmpCER = "";
			# dati impianto
	//{{{ 
			if(!$asCSV) {
				$Ypos += 3;
				$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
				$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Impianto di:",0,"L");
				$ImpAddr = $dati["IMPdes"] . " - " . $dati["IMPcom"] . " (" . $dati["IMPshprov"] . ") - " . $dati["IMPnaz"];
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
				$FEDIT->FGE_PdfBuffer->SetXY(32,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$ImpAddr,0,"L");
				$Ypos += 4;
			} else {
				$impianto = "Impianto di: " .  $dati["IMPdes"] . " - " . $dati["IMPcom"] . " (" . $dati["IMPshprov"] . ") - " . $dati["IMPnaz"];
				$CSVbuf .= AddCSVcolumn($impianto);
				$CSVbuf .= AddCSVRow();
				$CSVbuf .= AddCSVRow();
			}
			//}}}
		}
	}
	if($tmpCER!=$dati["CERref"]) {
		$counterVar = 0;
		$TotVar = 0;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
		$tmpCER = $dati["CERref"];
		$CERdes = $dati["COD_CER"] . " - " . $dati["descrizione"];
		# dati cer
//{{{ 
		if(!$asCSV) {
			$Ypos += 1;
			$FEDIT->FGE_PdfBuffer->Line(17,$Ypos,180, $Ypos);
			$Ypos += 1;
			$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
			$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$CERdes,0,"L");
			$Ypos += 6; 
		} else {
			$CSVbuf .= AddCSVRow();
			$CSVbuf .= AddCSVcolumn($CERdes);			
		}
		
		//}}}
		# intestazione movimenti + Giacenza Iniziale
//{{{ 
		if(is_null($dati["giac_ini"])) {
			$giacINI = 0;	
		} else {
			$giacINI = $dati["giac_ini"];
		}
		if(!$asCSV) {
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
			$FEDIT->FGE_PdfBuffer->SetXY(5,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Movimento",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(10,4,"Tipo",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(35,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Data Mov.",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(70,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Scarico",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(90,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Distanza",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(110,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Durata",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(140,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,4,"Totale",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(155,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(25,4,"Formulario",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Costo viaggio �",0,"C");

			$Ypos += 4;
			$FEDIT->FGE_PdfBuffer->SetXY(70,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,4,$dati["UnMis"],0,"C");

			if($showGiacIni){
				$Ypos += 4;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
				$FEDIT->FGE_PdfBuffer->SetXY(5,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"-",0,"C");
				$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(10,4,"-",0,"C");
				$FEDIT->FGE_PdfBuffer->SetXY(35,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Giac.Iniz.",0,"C");
				$FEDIT->FGE_PdfBuffer->SetXY(55,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(15,4,$giacINI,0,"R");
				$FEDIT->FGE_PdfBuffer->SetXY(70,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(15,4,"-",0,"R");
				
				#destino
				$FEDIT->FGE_PdfBuffer->SetXY(85,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(25,4,"-",0,"R");

				$FEDIT->FGE_PdfBuffer->SetXY(110,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(15,4,"-",0,"R");
				$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(15,4,"-",0,"R");

				$FEDIT->FGE_PdfBuffer->SetXY(140,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(15,4,$giacINI,0,"R");
				$FEDIT->FGE_PdfBuffer->SetXY(155,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(25,4,"-",0,"C");
				$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"-",0,"C");
				$TotCarico += $giacINI;
				}


			$Ypos += 4;
		} else {
			$CSVbuf .= AddCSVRow();
			$CSVbuf .= AddCSVcolumn("Movimento");
			$CSVbuf .= AddCSVcolumn("Tipo");
			$CSVbuf .= AddCSVcolumn("Data Movimento");			
			$CSVbuf .= AddCSVcolumn("Scarico");			
			$CSVbuf .= AddCSVcolumn("Totale");			
			$CSVbuf .= AddCSVcolumn("Formulario");
			$CSVbuf .= AddCSVcolumn("Costo viaggio �");
			$CSVbuf .= AddCSVRow();
			$CSVbuf .= AddCSVcolumn("");			
			$CSVbuf .= AddCSVcolumn("");		
			$CSVbuf .= AddCSVcolumn("");
			$CSVbuf .= AddCSVcolumn($dati["UnMis"]);
			$CSVbuf .= AddCSVcolumn($dati["UnMis"]);
			$CSVbuf .= AddCSVcolumn("Kg.");
			
			if($showGiacIni){
				$CSVbuf .= AddCSVRow();
				$CSVbuf .= AddCSVcolumn("");			
				$CSVbuf .= AddCSVcolumn("");
				$CSVbuf .= AddCSVcolumn("Giac.Iniz.");	
				$CSVbuf .= AddCSVcolumn($giacINI);
				$CSVbuf .= AddCSVcolumn("");			
				$CSVbuf .= AddCSVcolumn("");			
				$CSVbuf .= AddCSVcolumn("");			
				$CSVbuf .= AddCSVcolumn("");			
				$CSVbuf .= AddCSVcolumn("");
				$CSVbuf .= AddCSVcolumn($giacINI);
				$TotCarico += $giacINI;
				}

		}
		//}}}	
	}
		# dati movimenti	
//{{{ 
		if($dati["TIPO"]=="S") {
			$scarico = $dati["quantita"];
			$TotScarico += $dati["quantita"];
			$totale = " -" . $dati["quantita"];
		} else {
			$scarico = "-";
		}

		if(!is_null($dati["NFORM"])) {
			$formulario = $dati["NFORM"]; 	
		} else {
			$formulario = " - ";
		}

		$costo=$dati['costo_calcolato'];
		$TotCosto+=$dati['costo_calcolato'];
		$TotKm+=$dati['distanza'];
		$TotTime+=$dati['durata'];

		if(!$asCSV) {
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
				$FEDIT->FGE_PdfBuffer->SetXY(5,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(20,4,$dati["NMOV"],0,"C");
				$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(10,4,$dati["TIPO"],0,"C");
				$FEDIT->FGE_PdfBuffer->SetXY(35,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(20,4,date("d/m/Y",strtotime($dati["DTMOV"])),0,"C");
				$FEDIT->FGE_PdfBuffer->SetXY(70,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(20,4,number_format_unlimited_precision($scarico),0,"R");
				$FEDIT->FGE_PdfBuffer->SetXY(90,$Ypos);
				$distanza=$dati['distanza']/1000;
				$FEDIT->FGE_PdfBuffer->MultiCell(20,4,number_format($distanza,"2",",","")." Km",0,"R");
				$FEDIT->FGE_PdfBuffer->SetXY(110,$Ypos);
				$durata=formatTime($dati['durata']);
				$FEDIT->FGE_PdfBuffer->MultiCell(20,4,$durata,0,"R");
				$FEDIT->FGE_PdfBuffer->SetXY(140,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($totale),0,"R");
				$FEDIT->FGE_PdfBuffer->SetXY(155,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(25,4,$formulario,0,"C");
				$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(20,4,number_format($costo,"2",",",""),0,"R");
				$Ypos += 4;
				}
		else {
				$CSVbuf .= AddCSVRow();
				$CSVbuf .= AddCSVcolumn($dati["NMOV"]);
				$CSVbuf .= AddCSVcolumn($dati["TIPO"]);
				$CSVbuf .= AddCSVcolumn(date("d/m/Y",strtotime($dati["DTMOV"])));			
				$CSVbuf .= AddCSVcolumn(number_format_unlimited_precision($carico));			
				$CSVbuf .= AddCSVcolumn(number_format_unlimited_precision($scarico));			
				$CSVbuf .= AddCSVcolumn($pesoDestino);			
				$CSVbuf .= AddCSVcolumn(number_format_unlimited_precision($dati["tara"]));
				$CSVbuf .= AddCSVcolumn(number_format_unlimited_precision($dati["lordo"]));
				$CSVbuf .= AddCSVcolumn(number_format_unlimited_precision($totale));			
				$CSVbuf .= AddCSVcolumn($formulario);
				$CSVbuf .= AddCSVcolumn($costo);
			}//}}}
	if($counter==($DbRecsNum-1)) {
		if($counterVar>0) $VarMedia=$TotVar/$counterVar; else $VarMedia=$counterVar;
		SommaViaggi($Ypos,$TotScarico,$TotCosto,$TotKm,$TotTime,$FEDIT->FGE_PdfBuffer,$asCSV,$CSVbuf);
	}		
	$counter++;
}
?>