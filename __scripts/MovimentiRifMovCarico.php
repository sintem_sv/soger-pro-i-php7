<?php

/*
	REBUILDS ALL REFERENCES
*/

$debugMode = false;
//$LegamiSISTRI = false;
$UpdSQL = array();

if(!isset($RIF_REF)) {    
    $sql = "SELECT DISTINCT ID_RIF from ".$TableName."  WHERE ID_IMP='" . $SOGER->UserData["core_usersID_IMP"] . "' AND ".$SOGER->UserData['workmode']."=1";
    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);
    $rifs = array();    
    if(isset($FEDIT->DbRecordSet)){
        foreach($FEDIT->DbRecordSet as $k=>$v){
            RebuildRefs($debugMode,$v["ID_RIF"],$SOGER->UserData["core_usersID_IMP"],$FEDIT,$UpdSQL,$TableName,$LegamiSISTRI);
            }
        }
    }
else
    RebuildRefs($debugMode,$RIF_REF,$SOGER->UserData["core_usersID_IMP"],$FEDIT,$UpdSQL,$TableName,$LegamiSISTRI);

foreach($UpdSQL as $k=>$query) {
    if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
        echo "<li>$query";
        }
    $FEDIT->SDBWrite($query,"DbRecordSet",false,false);
    }

    
    
    
    
    
function RebuildRefs($debugMode,$ID_RIF,$IMP,&$OBJ,&$UpdSQL,$TableName,$LegamiSISTRI) {

    global $SOGER;
    $pri            = ($TableName=="user_movimenti"? "ID_MOV":"ID_MOV_F");
    $refComplexSQL_GiacenzeAnnoPrecedente  = array();
    $refComplexSQL_CarichiAnnoCorrente  = array();
//    $Carichi        = array();
//    $SubTot         = 0;
//    $DaScaricare    = 0;
    
    // fix 14.09.2016
    // posso escludere dal rebuild refs i movimenti precedenti ad uno scarico che ha azzerato la disponibilita
    // determino NMOV per scarico corrente e cerco questo scarico
    // essendo routine richiamata anche in altri frangenti, potrei non conoscere lo scarico corrente (niente get)
    // in questo caso, rebuild tutto
    $PuntoZero = null;
    $giacenzeIniziali = $giacenzeInizialiManuali = array();
    
    if(isset($_GET['pri']) && $_GET['pri']==$pri){
        $sql = "SELECT NMOV FROM ".$TableName." WHERE ".$pri."=".$_GET['filter']." AND ".$TableName.".ID_IMP='".$IMP."' AND ".$SOGER->UserData['workmode']."=1;";
        $OBJ->SDBRead($sql,"RebuildToNMOV",true,false);
        $RebuildUntilNMOV = $OBJ->RebuildToNMOV[0]['NMOV'];
        if($RebuildUntilNMOV){
            $sql ="SELECT MAX(NMOV) AS NMOV FROM ".$TableName." WHERE pesoN=FKEdisponibilita AND ID_RIF=".$ID_RIF." AND NMOV<".$RebuildUntilNMOV." AND TIPO='S';";
            $OBJ->SDBRead($sql,"PuntoZero",true,false);
            $PuntoZero = $OBJ->PuntoZero[0]['NMOV'];
        }
    }
    
    if(is_null($PuntoZero)){
        $PuntoZero = 0;
        
        // giacenze iniziali
        $sql ="SELECT NMOV AS ".$TableName."NMOV, 1 AS isGiacenza, quantita AS ".$TableName."quantita, pesoN AS ".$TableName."pesoN, TIPO AS ".$TableName."TIPO, ID_UIMP AS ".$TableName."ID_UIMP, ID_MOV_F AS ".$TableName."ID_MOV_F, idSIS AS ".$TableName."idSIS, user_schede_rifiuti.pericoloso AS user_schede_rifiutipericoloso ";
        $sql.=" FROM user_movimenti_giacenze_iniziali ";
        $sql.=" JOIN user_schede_rifiuti ON user_movimenti_giacenze_iniziali.ID_RIF=user_schede_rifiuti.ID_RIF ";
        $sql.=" WHERE user_movimenti_giacenze_iniziali.ID_RIF='$ID_RIF' AND user_movimenti_giacenze_iniziali.quantita>0 ";
        $sql.=" ORDER BY NMOV ASC";
        $OBJ->SDBRead($sql,"DbRecordSetGiacenzeIniziali",true,false);
        $giacenzeIniziali_counter = $OBJ->DbRecsNum;
        $giacenzeIniziali = ($giacenzeIniziali_counter>0? $OBJ->DbRecordSetGiacenzeIniziali:array());

        // giacenze iniziali manuali
        if($giacenzeIniziali_counter==0){
            $sql = "SELECT giac_ini, pericoloso, peso_spec, ID_UMIS FROM user_schede_rifiuti WHERE ID_RIF=".$ID_RIF.";";
            $OBJ->SDBRead($sql,"DbRecordSet",true,false);
            if($OBJ->DbRecordSet[0]["giac_ini"]>0) {
                if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
                    echo "<li>got giacenza iniziale \"manuale\":" . $OBJ->DbRecordSet[0]["giac_ini"];
                    }
                //$SubTot += $OBJ->DbRecordSet[0]["giac_ini"];
                switch($OBJ->DbRecordSet[0]["ID_UMIS"]){
                    // kg
                    case 1: 
                        $pesoN = $OBJ->DbRecordSet[0]["giac_ini"];
                        break;
                    // litri
                    case 2:
                        $pesoN = $OBJ->DbRecordSet[0]["giac_ini"] / $OBJ->DbRecordSet[0]["peso_spec"];
                        break;
                    // mc
                    case 3:
                        $pesoN = $OBJ->DbRecordSet[0]["giac_ini"] / $OBJ->DbRecordSet[0]["peso_spec"] / 1000;
                        break;
                    }
                $giacenzeInizialiManuali[0] = array($TableName."NMOV"=>'0', 
                                                "isGiacenza"=>'0',
                                                $TableName."quantita"=>$OBJ->DbRecordSet[0]["giac_ini"],
                                                $TableName."pesoN"=>$pesoN,
                                                $TableName."TIPO"=>'C',
                                                $TableName."ID_UIMP"=>'0137325',   // !!!SICON EXCEPTION!!!
                                                $TableName.$pri=>'0',
                                                $TableName."idSIS"=>'0',
                                                $TableName."pericoloso"=>$OBJ->DbRecordSet[0]["pericoloso"],
                                                );
                }
            }        
    }
    
    // movimentazioni
    $sql ="SELECT ".$TableName.".NMOV AS ".$TableName."NMOV, 0 AS isGiacenza, ".$TableName.".quantita AS ".$TableName."quantita, ".$TableName.".pesoN AS ".$TableName."pesoN, ".$TableName.".TIPO AS ".$TableName."TIPO, ".$TableName.".ID_UIMP AS ".$TableName."ID_UIMP, ".$TableName.".".$pri." AS ".$TableName.$pri.", ".$TableName.".idSIS AS ".$TableName."idSIS, user_schede_rifiuti.pericoloso AS user_schede_rifiutipericoloso ";
    $sql.=" FROM ".$TableName." ";
    $sql.=" JOIN user_schede_rifiuti ON ".$TableName.".ID_RIF=user_schede_rifiuti.ID_RIF ";	
    $sql.=" WHERE ".$TableName.".ID_RIF='$ID_RIF' AND ".$TableName.".ID_IMP='$IMP' AND ".$TableName.".quantita>0 ";
    $sql.=" AND NMOV>".$PuntoZero." ";
    $sql.=" ORDER BY NMOV ASC";
    $OBJ->SDBRead($sql,"DbRecordSetMovimentazioni",true,false);
    $movimentazioni_counter = $OBJ->DbRecsNum;
    $movimentazioni = ($movimentazioni_counter>0? $OBJ->DbRecordSetMovimentazioni:array());
    
    $merged_ref = array_merge($giacenzeIniziali, $giacenzeInizialiManuali, $movimentazioni);
    $record = array();
    
    if($SOGER->UserData['core_usersID_IMP']=='001SICIM'){
        foreach ($merged_ref as $value)
            $record_uimp[$value[$TableName.'ID_UIMP']][] = $value;
        $record = array_values($record_uimp);
    }
    else{
        $record[0] = $merged_ref; 
    }

    for($r=0;$r<count($record);$r++){
        $SubTot         = 0;
        $DaScaricare    = 0;
        $Carichi        = array();
        foreach($record[$r] as $k=>$dati) {
            
            if($dati[$TableName."TIPO"]=="C"){
                $Carichi[$dati[$TableName.$pri]]['quantita'] = round($dati[$TableName."quantita"], 2);
                $Carichi[$dati[$TableName.$pri]]['NMOV'] = $dati[$TableName."NMOV"];
                $Carichi[$dati[$TableName.$pri]]['isGiacenza'] = $dati["isGiacenza"];
                if($dati['isGiacenza']=='1')
                    $refComplexSQL_GiacenzeAnnoPrecedente[] = $dati[$TableName.'NMOV'];
                else
                    $refComplexSQL_CarichiAnnoCorrente[] = $dati[$TableName.'NMOV'];

                $SubTot += round($dati[$TableName."quantita"], 2);
                if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
                    echo "<li>Trovato Carico numero ".$dati[$TableName."NMOV"]." di quantit� ".round($dati[$TableName."quantita"], 2);
                    echo "<li>Quantit� totale in carico (Subtotal): ".$SubTot;
                    }
                }
            else {
                $DaScaricare += round($dati[$TableName."quantita"], 2);
                if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
                    echo "<li>Trovato Scarico numero ".$dati[$TableName."NMOV"]." di quantit� ".round($dati[$TableName."quantita"], 2);
                    echo "<li>Devo scaricare in tutto ". $DaScaricare;
                    }

                // sta scaricando tutta la mia giacenza
                if(round($dati[$TableName."quantita"], 2)==round($SubTot, 2)) {

                    if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
                        echo "<li>Questo scarico (numero ".$dati[$TableName."NMOV"]." di quantit� ".round($dati[$TableName."quantita"], 2).") azzera la quantit� totale in carico (subtotal ".$SubTot.")";
                        }

                    $GiacenzeAnnoPrecedente = array();
                    $CarichiAnnoCorrente = array();
                    foreach($Carichi as $Id=>$data) {
                        if($Id>0){
                            if($data['isGiacenza']=='1')
                                $GiacenzeAnnoPrecedente[] = $data['NMOV']=='0'? 'Giac.Iniz.':$data['NMOV'];
                            else
                                $CarichiAnnoCorrente[] = $data['NMOV'];
                        }
                        else{
                            // scarico giacenza manuale
                            $CarichiAnnoCorrente[] = 'Giac.Iniz.';
                        }
                    }
                    $prevYear = substr($_SESSION["DbInUse"],-4) -1;
                    $NMOVs = array();
                    if($GiacenzeAnnoPrecedente)
                            $NMOVs[] = 'Giac.Iniz. (mov.'.implode(", ",$GiacenzeAnnoPrecedente).' del reg.'.$prevYear.')';
                    if($CarichiAnnoCorrente)
                            $NMOVs[] = implode(", ",$CarichiAnnoCorrente);
                    $UpdSQL[] = "UPDATE ".$TableName." SET FKErifCarico='" . implode(", ",$NMOVs) . "' WHERE NMOV='" . $dati[$TableName."NMOV"] . "' AND ".$TableName.".ID_IMP ='".$IMP."' AND ".$TableName.".".$pri."='" . $dati[$TableName.$pri] . "' LIMIT 1";
                    $refComplexSQL_GiacenzeAnnoPrecedente  = array();
                    $refComplexSQL_CarichiAnnoCorrente  = array();
                    $NMOVs = array();
                    $GiacenzeAnnoPrecedente = array();
                    $CarichiAnnoCorrente = array();

                    ## SISTRI user_legami_carico_scarico
                    if($LegamiSISTRI){
                        $sql="DELETE FROM user_legami_carico_scarico WHERE MOV_KEY='".$pri."' AND SCARICO_ID=".$dati[$TableName.$pri];
                        $OBJ->SDBWrite($sql,"DbRecordSet",false,false);
                        if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
                            echo "<li>Cancello tutti i riferimenti per lo scarico numero ".$dati[$TableName."NMOV"]." id ". $dati[$TableName.$pri];
                            echo "<li>".$sql;
                            }
                        foreach($Carichi as $idMov=>$data) {

                            $quantitaKg = $data['quantita'];

                            // user_legami_carico_scarico
                            $sql_idc ="(";
                            $sql_idc.="SELECT idSIS, FKEpesospecifico, FKEumis, pesoN ";
                            $sql_idc.="FROM ".$TableName." ";
                            $sql_idc.="WHERE ".$pri."=".$idMov;
                            $sql_idc.=") UNION (";
                            $sql_idc.="SELECT idSIS, FKEpesospecifico, FKEumis, pesoN ";
                            $sql_idc.="FROM user_movimenti_giacenze_iniziali ";
                            $sql_idc.="WHERE ".$pri."=".$idMov;
                            $sql_idc.=")";

                            $OBJ->SDBRead($sql_idc,"DbRecordSet",true,false);

                            if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
                                echo "<li>".$sql_idc."<br />";
                                }

                            if($OBJ->DbRecsNum==0) 
                                $isGiacIni=true; else $isGiacIni=false;

                            if(!$isGiacIni){
                                // info carico
                                $caricoID_MOV	= $idMov;
                                $caricoidSIS	= $OBJ->DbRecordSet[0]['idSIS'];
                                $caricoKg           = $OBJ->DbRecordSet[0]['pesoN'];
                                $caricoKg_mg	= round($OBJ->DbRecordSet[0]['pesoN'], 2)*1000000;

                                // pesoN convertito in mg
                                $qScarico_mg	= round($dati[$TableName."pesoN"], 2)*1000000;

                                // $quantitaKg � in realt� espresso nella UM del rifiuto, non in kg
                                switch($OBJ->DbRecordSet[0]['FKEumis']){
                                    default:
                                    case 'Kg.':
                                        $qta_prelevata = $quantitaKg;
                                        break;
                                    case 'Litri':
                                        $qta_prelevata = $quantitaKg * $OBJ->DbRecordSet[0]['FKEpesospecifico'];
                                        break;
                                    case 'Mc.':
                                        $qta_prelevata = $quantitaKg * $OBJ->DbRecordSet[0]['FKEpesospecifico'] * 1000;
                                        break;
                                    }
                                $quantitaKg_mg	= round($qta_prelevata, 2)*1000000;

                                // inserisco legame c-s
                                $sql="INSERT INTO user_legami_carico_scarico VALUES(NULL, '".$pri."', ".$dati[$TableName.$pri].", '".$dati[$TableName.'idSIS']."', ".$qScarico_mg.", ".$caricoID_MOV.", '".$caricoidSIS."', ".$caricoKg_mg.", ".$quantitaKg_mg.");";
                                if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
                                    echo "<li>".$sql;
                                    }
                                $OBJ->SDBWrite($sql,"DbRecordSet",false,false);
                                $Legame=$OBJ->DbLastID;
                                }
                            }
                        }
                    $Carichi = array();
                    $SubTot = 0;
                    }

                // resta qualcosa in giacenza
                else {
                    $SubTot -= round($dati[$TableName."quantita"], 2);
                    if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
                        echo "<li>Nonostante questo scarico (numero ".$dati[$TableName."NMOV"]." di quantit� ".round($dati[$TableName."quantita"], 2)."), resta qualcosa in giacenza (subtotal ".$SubTot.")";
                        }
                    $Carichi = reCheckCarichi($debugMode,$Carichi,round($dati[$TableName."quantita"], 2),$UpdSQL,$dati[$TableName."NMOV"],$dati[$TableName.$pri],$dati[$TableName.'idSIS'],$TableName,$OBJ,$dati['user_schede_rifiutipericoloso'],$LegamiSISTRI);
                    }
                }
            } 
        }
    }

function reCheckCarichi($debugMode,$arrayCarichi,$qScarico,&$SQLs,$scaricoNmov,$scaricoID_MOV,$scaricoid_SIS,$TableName,&$OBJ,$pericoloso,$LegamiSISTRI) {
  
    if($TableName=="user_movimenti") $pri="ID_MOV"; else $pri="ID_MOV_F";
    $fullyScaricato	= false;
    $AltfullyScaricato	= false;
    $newArrayCarichi	= array();
    $refComplexSQL_GiacenzeAnnoPrecedente  = array();
    $refComplexSQL_CarichiAnnoCorrente  = array();
    $isGiacIni		= false;
    $ancoraDaScaricare	= $qScarico;
    $Scaricati		= 0;
    global $SOGER;

    if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
        echo "<li>parsing carichi about scarico N� $scaricoNmov, ID $scaricoID_MOV di qta: $qScarico";
        }

    ## SISTRI - Tabella user_legami_carico_scarico - Cancello tutti i riferimenti per lo scarico corrente
    if($LegamiSISTRI){
        if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
            echo "<li>Cancello tutti i riferimenti per lo scarico numero ".$scaricoNmov." id ". $scaricoID_MOV;
        }
        $sql="DELETE FROM user_legami_carico_scarico WHERE MOV_KEY='".$pri."' AND SCARICO_ID=". $scaricoID_MOV;
        $OBJ->SDBWrite($sql,"DbRecordSet",false,false);
    }
    foreach($arrayCarichi as $idMov=>$data) {
        $quantitaKg = $data['quantita'];
        if($fullyScaricato) {
            # se abbiamo gi� scaricato tutto, aggiungo movimenti senza fare altro (OK)
            if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
                echo "<li> aggiungo movimenti senza toccare $idMov";
            }
            $newArrayCarichi[$idMov]['quantita'] = $quantitaKg;
            $newArrayCarichi[$idMov]['NMOV'] = $data['NMOV'];
            $newArrayCarichi[$idMov]['isGiacenza'] = $data['isGiacenza'];
        } 
        else {
            $GiacenzeAnnoPrecedente = array();
            $CarichiAnnoCorrente = array();
            
            if($idMov>0){
                if($data['isGiacenza']=='1')
                    $refComplexSQL_GiacenzeAnnoPrecedente[] = $data['NMOV']=='0'? 'Giac.Iniz.':$data['NMOV'];
                else
                    $refComplexSQL_CarichiAnnoCorrente[] = $data['NMOV'];
            }
            else{
                // scarico giacenza manuale
                $refComplexSQL_CarichiAnnoCorrente[] = 'Giac.Iniz.';
            }

            if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
                echo "<li>Scarico N� ".$scaricoNmov.", ID ".$scaricoID_MOV." di qta: ".$qScarico." non completamente scaricato ( mancano ".$ancoraDaScaricare." ), prendo da carico ID ".$idMov;
            }

            ## SISTRI - Tabella user_legami_carico_scarico	
            if($LegamiSISTRI && $idMov>0){

                // user_legami_carico_scarico
                $sql_idc ="(";
                $sql_idc.="SELECT pesoN, ".$pri.", FKEumis, FKEpesospecifico, idSIS ";
                $sql_idc.="FROM ".$TableName." ";
                $sql_idc.="WHERE ".$pri."=".$idMov;
                $sql_idc.=") UNION (";
                $sql_idc.="SELECT pesoN, ".$pri.", FKEumis, FKEpesospecifico, idSIS ";
                $sql_idc.="FROM user_movimenti_giacenze_iniziali ";
                $sql_idc.="WHERE ".$pri."=".$idMov;
                $sql_idc.=")";                    
                $OBJ->SDBRead($sql_idc,"DbRecordSet",true,false);

                $caricoID_MOV	= $idMov;
                $caricoidSIS	= $OBJ->DbRecordSet[0]['idSIS'];
                $caricoKg       = $OBJ->DbRecordSet[0]['pesoN'];
                $caricoKg_mg	= round($OBJ->DbRecordSet[0]['pesoN'], 2)*1000000;

                // pesoN convertito in mg
                $qScarico_mg	= $qScarico*1000000;
                $quantitaKg_mg	= $quantitaKg*1000000;

                $sql="INSERT INTO user_legami_carico_scarico VALUES(NULL, '".$pri."', ".$scaricoID_MOV.", '".$scaricoid_SIS."', ".$qScarico_mg.", ".$caricoID_MOV.", '".$caricoidSIS."', ".$caricoKg_mg.", 0);";
                if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
                    echo "<li>".$sql;
                }
                $OBJ->SDBWrite($sql,"DbRecordSet",false,false);
                $Legame=$OBJ->DbLastID;
            }

            if($Scaricati<=$qScarico) {
                $Residuo = $quantitaKg - $ancoraDaScaricare; if($Residuo<0) $Residuo=0;
                if($Residuo==0) $Prelevati=$quantitaKg; else $Prelevati=$ancoraDaScaricare;

                ## SISTRI - Tabella user_legami_carico_scarico	
                if($LegamiSISTRI && $idMov>0){
                    // $Prelevati � in realt� espresso nella UM del rifiuto, non in kg
                    switch($OBJ->DbRecordSet[0]['FKEumis']){
                        default:
                        case 'Kg.':
                            $qta_prelevata = $Prelevati;
                            break;
                        case 'Litri':
                            $qta_prelevata = $Prelevati * $OBJ->DbRecordSet[0]['FKEpesospecifico'];
                            break;
                        case 'Mc.':
                            $qta_prelevata = $Prelevati * $OBJ->DbRecordSet[0]['FKEpesospecifico'] * 1000;
                            break;
                    }
                $qta_prelevata_mg = round($qta_prelevata, 2)*1000000;
                }

                if(!$fullyScaricato && ($ancoraDaScaricare - $quantitaKg>=0)) {
                    $ancoraDaScaricare -= $quantitaKg;
                    if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
                        echo "<li>ancora da scaricare: ".$ancoraDaScaricare;
                        echo "<li>Ho scaricato completamente il carico ID ".$idMov." e devo ancora scaricare quantit�: ".$ancoraDaScaricare;
                        }
                    if($ancoraDaScaricare==0) $fullyScaricato=true; 						
                }
                else {
                    if(!$fullyScaricato) {
                        if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
                            echo "<li>--";
                        }
                        $newArrayCarichi[$idMov]['quantita'] = $quantitaKg-$ancoraDaScaricare;
                        $fullyScaricato = true;
                    } 
                    else {
                        if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
                            echo "<li>--";
                        }
                        $newArrayCarichi[$idMov]['quantita'] = $quantitaKg;
                    }
                    $newArrayCarichi[$idMov]['NMOV'] = $data['NMOV'];
                    $newArrayCarichi[$idMov]['isGiacenza'] = $data['isGiacenza'];
                }
            $Scaricati += $quantitaKg;
            }

            ## SISTRI user_legami_carico_scarico
            // aggiorno quantita prelevata su riga legame
            // pesoN convertito in mg

            if($LegamiSISTRI && $idMov>0){

                $sql="UPDATE user_legami_carico_scarico SET QTA_PRELEVATA='".$qta_prelevata_mg."' WHERE ID_LCS=".$Legame;
                if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
                    echo "<li>".$sql."<br/>";
                }
                $OBJ->SDBWrite($sql,"DbRecordSet",false,false);

                if($SOGER->UserData['core_usersusr']=='sintem' AND $debugMode){
                    echo "<li>La quantit� residua del carico ID ".$idMov." � ".$Residuo;
                }
            }
        }
    }
    $prevYear = substr($_SESSION["DbInUse"],-4) -1;
    $NMOVs = array();
    if($refComplexSQL_GiacenzeAnnoPrecedente)
            $NMOVs[] = 'Giac.Iniz. (mov.'.implode(", ",$refComplexSQL_GiacenzeAnnoPrecedente).' del reg.'.$prevYear.')';
    if($refComplexSQL_CarichiAnnoCorrente)
            $NMOVs[] = implode(", ",$refComplexSQL_CarichiAnnoCorrente);
    $SQLs[] = "UPDATE ".$TableName." SET FKErifCarico='" . implode(", ",$NMOVs) . "' WHERE NMOV='" . $scaricoNmov . "' AND ".$pri."='" .$scaricoID_MOV. "' AND ".$TableName.".ID_IMP ='" . $SOGER->UserData["core_usersID_IMP"] . "' LIMIT 1";
    $refComplexSQL_GiacenzeAnnoPrecedente  = array();
    $refComplexSQL_CarichiAnnoCorrente  = array();
    $NMOVs = array();
    $GiacenzeAnnoPrecedente = array();
    $CarichiAnnoCorrente = array();
    #print_r($SQLs);
    $defArray = array();
    foreach($newArrayCarichi as $k=>$v) {
        if($v['quantita']!=0) {
            $defArray[$k] = $v;			
        }
    }
    return $defArray; 
}

?>