<?php
session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/fpdf.php");
include("../__libs/SQLFunct.php");
require_once("STATS_funct.php");
#
global $SOGER;


$orientation="L";
$statTitle = "CONFIGURAZIONE NON CONFORMITA'";
$statDesc = "Configurazione dei livelli di non conformit� nella documentazione prodotta nell' adempimento della normativa rifiuti";
$DcName = date("d/m/Y") . "--Configurazione_non_conformit�";

$PrintImpianto = true;
$PrintCER = false;
$SkipStandardNoRecs = true;

$um="mm";
$Format = array(210,297);
$ZeroMargin = true;
$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin,$DcName);

## stats title
$Ypos+=10;
$FEDIT->FGE_PdfBuffer->SetXY(0,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',14);
$FEDIT->FGE_PdfBuffer->MultiCell(0,15,$statTitle,0,"C"); 


## stat desc
if(!isset($statDesc)) $statDesc=" -- ";
$Xpos=20;
$Ypos+=15;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Descrizione:",0,"L");
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(40,25);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$statDesc,0,"L");

## intestatario
$intestatario = strtolower($SOGER->UserData["core_impiantidescription"]) . " (" . $SOGER->UserData["workmode"] . ")";
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Intestatario:",0,"L");
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY(40,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$intestatario,0,"L");


$Ypos+=20;


$sql="SELECT * FROM core_impianti_nc WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1;";
$FEDIT->SDBRead($sql,"DbRecordset");

$SQL="SELECT DISTINCT condizione FROM lov_nc ORDER BY ID_NC";
$FEDIT->SdbRead($SQL, "DbRecordSetConds");

for($c=0;$c<count($FEDIT->DbRecordSetConds);$c++){

		# header
		$Ypos += 10;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(277,10,$FEDIT->DbRecordSetConds[$c]['condizione'],"TLBR","C");
		$Ypos += 10;
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(150,5,"Condizione","TLBR","L");
		$FEDIT->FGE_PdfBuffer->SetXY(160,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(67,5,"Tipo di documento","TLBR","L");
		$FEDIT->FGE_PdfBuffer->SetXY(227,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,5,"Non conformit�","TLBR","C");
		$FEDIT->FGE_PdfBuffer->SetXY(247,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,5,"Osservazione","TLBR","C");
		$FEDIT->FGE_PdfBuffer->SetXY(267,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,5,"Suggerimento","TLBR","C");
		$Ypos += 5;


		# rows
		$SQL="SELECT * FROM lov_nc WHERE condizione='".addslashes($FEDIT->DbRecordSetConds[$c]['condizione'])."'";
		$FEDIT->SdbRead($SQL, "DbRecordSet");
		for($r=0;$r<count($FEDIT->DbRecordSet);$r++){
			
			$frase ="Rifiuto ";
			
			if($FEDIT->DbRecordSet[$r]['pericoloso']=='1')
				$frase.="pericoloso ";
			else
				$frase.="non pericoloso ";

			if($FEDIT->DbRecordSet[$r]['miscela_reazione_lega']=='1')
				$frase.="( miscela, da reazione o lega )";
			else
				$frase.="( non miscela, da reazione o lega )";

			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(150,5,$frase,"TLBR","L");
			$FEDIT->FGE_PdfBuffer->SetXY(160,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(67,5,$FEDIT->DbRecordSet[$r]['documento'],"TLBR","L");
			
			$SQL="SELECT ID_NCL FROM core_impianti_nc WHERE ID_NC=".$FEDIT->DbRecordSet[$r]['ID_NC']." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
			$FEDIT->SdbRead($SQL, "RadioValue");
			switch($FEDIT->RadioValue[0]['ID_NCL']){
				case '1':
					$checked1="s�";
					$checked2="no";
					$checked3="no";
					break;
				case '2':
					$checked1="no";
					$checked2="s�";
					$checked3="no";
					break;
				case '3':
					$checked1="no";
					$checked2="no";
					$checked3="s�";
					break;
				}
			
			$FEDIT->FGE_PdfBuffer->SetXY(227,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$checked1,"TLBR","C");
			$FEDIT->FGE_PdfBuffer->SetXY(247,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$checked2,"TLBR","C");
			$FEDIT->FGE_PdfBuffer->SetXY(267,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$checked3,"TLBR","C");
			$Ypos += 5;
			}


	}

	PDFOut($FEDIT->FGE_PdfBuffer,$DcName);

?>