<?php

$aztraspBuffer		='###';
$automezzoBuffer	='###';
$rimorchioBuffer	='###';

$Ypos+=10;

for($c=0;$c<count($FEDIT->DbRecordSet);$c++){
	if(is_null($FEDIT->DbRecordSet[$c]['automezzo'])) $automezzo = "non specificato"; else $automezzo = $FEDIT->DbRecordSet[$c]['automezzo'];
	if(is_null($FEDIT->DbRecordSet[$c]['rimorchio'])) $rimorchio = "non specificato"; else $rimorchio = $FEDIT->DbRecordSet[$c]['rimorchio'];

	# Nuovo trasportatore
	if($FEDIT->DbRecordSet[$c]['ID_AZT'] != $aztraspBuffer){
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(260,8,$FEDIT->DbRecordSet[$c]['trasportatore'],"TLBR","C");
		$Ypos+=10;
		$aztraspBuffer = $FEDIT->DbRecordSet[$c]['ID_AZT'];
		}
		
	# Nuova combo automezzo+rimorchio
	if($automezzo != $automezzoBuffer | $rimorchio != $rimorchioBuffer){
		$Ypos+=5;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Targa automezzo: ","B","L");
		$FEDIT->FGE_PdfBuffer->SetXY(50,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,4,$automezzo,"B","L");
		$FEDIT->FGE_PdfBuffer->SetXY(80,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Targa rimorchio: ","B","L");
		$FEDIT->FGE_PdfBuffer->SetXY(110,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,4,$rimorchio,"B","L");
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		//intestazione formulari
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',7);		
		$FEDIT->FGE_PdfBuffer->SetXY(50,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Formulario",0,"C");
		$FEDIT->FGE_PdfBuffer->SetXY(80,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Data",0,"C");
		$FEDIT->FGE_PdfBuffer->SetXY(110,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(70,4,"Rifiuto",0,"C");
		$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Tara (kg)",0,"C");
		$FEDIT->FGE_PdfBuffer->SetXY(210,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Lordo (kg)",0,"C");
		$FEDIT->FGE_PdfBuffer->SetXY(240,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Netto (kg)",0,"C");
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$automezzoBuffer = $automezzo;
		$rimorchioBuffer = $rimorchio;
		}
	
	# Formulari
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);		
	$FEDIT->FGE_PdfBuffer->SetXY(50,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,$FEDIT->DbRecordSet[$c]['NFORM'],0,"C");
	$FEDIT->FGE_PdfBuffer->SetXY(80,$Ypos);
	$dt=explode("-", $FEDIT->DbRecordSet[$c]['DTFORM']);
	$DTFORM=$dt[2]."/".$dt[1]."/".$dt[0];
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,$DTFORM,0,"C");
	$FEDIT->FGE_PdfBuffer->SetXY(110,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(70,4,$FEDIT->DbRecordSet[$c]['COD_CER']." - ".$FEDIT->DbRecordSet[$c]['descrizione'],0,"C");
	$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,$FEDIT->DbRecordSet[$c]['tara'],0,"C");
	$FEDIT->FGE_PdfBuffer->SetXY(210,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,$FEDIT->DbRecordSet[$c]['pesoL'],0,"C");
	$FEDIT->FGE_PdfBuffer->SetXY(240,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,$FEDIT->DbRecordSet[$c]['pesoN'],0,"C");
	$Ypos+=7;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	}	

?>