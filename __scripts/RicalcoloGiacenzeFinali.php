<?php
session_start();
global $SOGER;
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/SQLFunct.php");

function RemoveCarichiScaricati($rif, $CarichiRifiuto){

	if(isset($rif['totS'])) $TotS=$rif['totS']; else $TotS=0;
	$giacOK=false;
	$PrimoDaScaricare=0;

	# 0 - giacenza iniziale
	if($rif['giac_ini']>0 && !$giacOK){
		$CarichiRifiuto[$rif['RifID']][0]['qta']=$rif['giac_ini'];
		$CarichiRifiuto[$rif['RifID']][0]['num']=0;
		$CarichiRifiuto[$rif['RifID']][0]['ID_MOV_F']=0;
		$CarichiRifiuto[$rif['RifID']][0]['idSIS']=0;
		$CarichiRifiuto[$rif['RifID']][0]['FKEpesospecifico']=$rif['FKEpesospecifico'];
		$CarichiRifiuto[$rif['RifID']][0]['FKEumis']=$rif['FKEumis'];
		$CarichiRifiuto[$rif['RifID']][0]['ID_IMP']=$rif['ID_IMP'];
		$CarichiRifiuto[$rif['RifID']][0]['workmode']=$rif['workmode'];
		$CarichiRifiuto[$rif['RifID']][0]['data']="0000-00-00";
		$CarichiRifiuto[$rif['RifID']][0]['CER']=$rif['COD_CER'];
		$CarichiRifiuto[$rif['RifID']][0]['desc']=$rif['descrizione'];
		$giacOK=true;
	}

	if(array_key_exists('carico', $rif)){
		# 1 - array carichi rifiuto
		for($k=0;$k<count($rif['carico']);$k++){
			if(isset($CarichiRifiuto[$rif['RifID']])) $index=count($CarichiRifiuto[$rif['RifID']]); else $index=0;
			$CarichiRifiuto[$rif['RifID']][$index]['qta']=$rif['carico'][$k]['qta'];
			$CarichiRifiuto[$rif['RifID']][$index]['num']=$rif['carico'][$k]['num'];
			$CarichiRifiuto[$rif['RifID']][$index]['ID_MOV_F']=$rif['carico'][$k]['ID_MOV_F'];
			$CarichiRifiuto[$rif['RifID']][$index]['idSIS']=$rif['carico'][$k]['idSIS'];
			$CarichiRifiuto[$rif['RifID']][$index]['FKEpesospecifico']=$rif['FKEpesospecifico'];
			$CarichiRifiuto[$rif['RifID']][$index]['FKEumis']=$rif['FKEumis'];
			$CarichiRifiuto[$rif['RifID']][$index]['ID_IMP']=$rif['ID_IMP'];
			$CarichiRifiuto[$rif['RifID']][$index]['ID_UIMP']=$rif['carico'][$k]['ID_UIMP'];
			$CarichiRifiuto[$rif['RifID']][$index]['workmode']=$rif['workmode'];
			$CarichiRifiuto[$rif['RifID']][$index]['data']=$rif['carico'][$k]['data'];
			$CarichiRifiuto[$rif['RifID']][$index]['CER']=$rif['COD_CER'];
			$CarichiRifiuto[$rif['RifID']][$index]['desc']=$rif['descrizione'];
		}

		# 2 - sottraggo qta da scaricato fino a quando scaricato resta positivo
		for($i=0;$i<count($CarichiRifiuto[$rif['RifID']]);$i++){
			if($TotS>=0){
				$TotS-=$CarichiRifiuto[$rif['RifID']][$i]['qta'];
				//print_r("<br />Rifiuto ".$rif['RifID'].", elimino il carico ".$CarichiRifiuto[$rif['RifID']][$i]['num']." ( ".$i." ) restano da scaricare: ".$TotS."<br /><hr>");
				$CarichiRifiuto[$rif['RifID']][$i]['qta']=abs($TotS);
				if($TotS<0){
					$PrimoDaScaricare=$i;
					//print_r("Primo da scaricare: ".$i."<br />");
				}
				//else{
					//print_r("Non mi restano carichi da scaricare");
				//}
			}
		}

		$count=count($CarichiRifiuto[$rif['RifID']]);
		for($i=0;$i<$count;$i++){
			//print_r("<br />Indice: ".$i." elimino se minore di ".$PrimoDaScaricare." ( count=".count($CarichiRifiuto[$rif['RifID']])." ) oppure ho scaricato tutto (Primo da scaricare = 0)");
			if($i<$PrimoDaScaricare || ($PrimoDaScaricare==0 && $TotS)){
				//print_r("<br />Carico eliminato<br />");
				unset($CarichiRifiuto[$rif['RifID']][$i]);
			}
		}
	}

	if(is_null($CarichiRifiuto[$rif['RifID']])) $CarichiRifiuto[$rif['RifID']] = [];

	$CarichiRifiuto[$rif['RifID']]=array_merge($CarichiRifiuto[$rif['RifID']]);

	return($CarichiRifiuto);
}

$TableName  = 'user_movimenti_fiscalizzati';
$PrintOut   = 'false';
$IDMov      = "ID_MOV_F";

$YEAR_in_session = substr($FEDIT->DbServerData["db"], -4);
$YEAR = $YEAR_in_session+1;

# aggiorno la disponibilit� sulla tabella schede rifiuti dell'anno corrente
$sqlDisponibilita ="SELECT ID_RIF, ID_UMIS, peso_spec FROM user_schede_rifiuti WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1;";
$FEDIT->SDBRead($sqlDisponibilita,"DispoRif",true,false);
for($r=0;$r<count($FEDIT->DispoRif);$r++){

    # pars for dispo recalculation
    $IMP_FILTER         = true;
    $DISPO_PRINT_OUT    = false;
    $IDRIF              = $FEDIT->DispoRif[$r]['ID_RIF'];
    $TableName          = "user_movimenti_fiscalizzati";
    $IDMov		= "ID_MOV_F";
    $EXCLUDE_9999999	= true;

    # aggiorno la disponibilità dell'anno passato
    $FEDIT->DbConn('soger'.$YEAR_in_session);
    require("ChiusuraAnno-MovimentiDispoRIF.php");
    if($Disponibilita<0) $Disponibilita=0;
    $FEDIT->SDBWrite("UPDATE user_schede_rifiuti SET FKEdisponibilita='$Disponibilita' WHERE ID_RIF='" . $FEDIT->DispoRif[$r]['ID_RIF'] . "'",true,false);

    # sul database anno corrente...
    $FEDIT->DbConn('soger'.$YEAR);

    # ...azzero giac_ini e disponibilita, le devo reimpostare
    $FEDIT->SDBWrite("UPDATE user_schede_rifiuti SET FKEdisponibilita=0, giac_ini=0 WHERE ID_RIF='" . $FEDIT->DispoRif[$r]['ID_RIF'] . "'",true,false);

    # ...aggiorno la giacenza iniziale dell'anno corrente (=disponibilità anno passato)
    if($Disponibilita<0) $Disponibilita=0;
    // la disponibilità è in kg, la giac_ini è nell'um del rifiuto
    switch($FEDIT->DispoRif[$r]['ID_UMIS']){
        case '1':
            $GiacINI=$Disponibilita;
            break;
        case '2':
            $GiacINI=$Disponibilita/$FEDIT->DispoRif[$r]['peso_spec'];
            break;
        case '3':
            $GiacINI=$Disponibilita/$FEDIT->DispoRif[$r]['peso_spec']/1000;
            break;
    }
    $FEDIT->SDBWrite("UPDATE user_schede_rifiuti SET giac_ini='$GiacINI' WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ID_RIF='" . $FEDIT->DispoRif[$r]['ID_RIF'] . "'",true,false);

    # ...aggiorno la disponibilità dell'anno corrente
    require("ChiusuraAnno-MovimentiDispoRIF.php");

    if($Disponibilita<0) $Disponibilita=0;
    $FEDIT->SDBWrite("UPDATE user_schede_rifiuti SET FKEdisponibilita='$Disponibilita' WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ID_RIF='" . $FEDIT->DispoRif[$r]['ID_RIF'] . "'",true,false);
}

$FEDIT->DbConn('soger'.$YEAR);
$sql ="SELECT originalID_RIF, ID_RIF FROM user_schede_rifiuti WHERE giac_ini>0 AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1;";
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
$rifs = array();
$rifiuti = array();
$CarichiRifiuto = array();
for($r=0;$r<count($FEDIT->DbRecordSet);$r++){
    $rifs[] = $FEDIT->DbRecordSet[$r]['originalID_RIF'];
}

if(!empty($rifs)){
    $id_rifs = implode(",", $rifs);

    $FEDIT->DbConn('soger'.$YEAR_in_session);

    $sql  = "SELECT ID_MOV_F,idSIS,DTMOV,NMOV,quantita,user_movimenti_fiscalizzati.ID_UIMP,user_movimenti_fiscalizzati.originalID_RIF AS originalRifID,user_movimenti_fiscalizzati.ID_RIF AS RifID,TIPO,";
    $sql .= " user_schede_rifiuti.ID_CER as CERref,user_schede_rifiuti.descrizione,user_schede_rifiuti.ID_RIFPROD_F,lov_cer.COD_CER,user_schede_rifiuti.giac_ini, ";
    $sql .= " user_movimenti_fiscalizzati.ID_IMP, FKEumis, FKEpesospecifico, (CASE WHEN user_movimenti_fiscalizzati.produttore=1 THEN 'produttore' WHEN user_movimenti_fiscalizzati.trasportatore=1 THEN 'trasportatore' WHEN user_movimenti_fiscalizzati.destinatario=1 THEN 'destinatario' WHEN user_movimenti_fiscalizzati.intermediario=1 THEN 'intermediario' ELSE 'produttore' END) AS workmode ";
    $sql .= " FROM user_movimenti_fiscalizzati";
    $sql .= " JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF";
    $sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
    $sql .= " WHERE user_movimenti_fiscalizzati.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND user_movimenti_fiscalizzati.".$SOGER->UserData['workmode']."=1 AND user_movimenti_fiscalizzati.NMOV<>9999999 ";
    $sql .= " AND user_movimenti_fiscalizzati.originalID_RIF IN (".$id_rifs.") ";
	if($FEDIT->DbRecordSet[$r]['produttore'])
		$sql .= " ORDER BY originalRifID ASC, user_movimenti_fiscalizzati.ID_UIMP ASC, TIPO ASC, NMOV ASC, DTMOV ASC ";
	else
		$sql .= " ORDER BY originalRifID ASC, TIPO ASC, NMOV ASC, DTMOV ASC ";
    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);

    //die($sql);
    $TotC = 0;
    $TotS = 0;
    $RIFbuf = "";
    $UIMPbuf = "";

    for($i=0;$i<count($FEDIT->DbRecordSet);$i++){
        $indice=count($rifiuti);
        if($FEDIT->DbRecordSet[$i]['originalRifID']!=$RIFbuf || ($FEDIT->DbRecordSet[$i]['workmode']=='produttore' && $FEDIT->DbRecordSet[$i]['ID_UIMP']!=$UIMPbuf)) {
            $rifiuti[$indice]['totC']=$FEDIT->DbRecordSet[$i]['giac_ini'];
            $rifiuti[$indice]['COD_CER']=$FEDIT->DbRecordSet[$i]['COD_CER'];
            $rifiuti[$indice]['CERref']=$FEDIT->DbRecordSet[$i]['CERref'];
            $rifiuti[$indice]['RifID']=$FEDIT->DbRecordSet[$i]['RifID'];
            $rifiuti[$indice]['originalRifID']=$FEDIT->DbRecordSet[$i]['originalRifID'];
            $rifiuti[$indice]['giac_ini']=$FEDIT->DbRecordSet[$i]['giac_ini'];
            $rifiuti[$indice]['descrizione']=$FEDIT->DbRecordSet[$i]['descrizione'];
            $rifiuti[$indice]['ID_RIFPROD_F']=$FEDIT->DbRecordSet[$i]['ID_RIFPROD_F'];
            $rifiuti[$indice]['FKEpesospecifico']=$FEDIT->DbRecordSet[$i]['FKEpesospecifico'];
            $rifiuti[$indice]['FKEumis']=$FEDIT->DbRecordSet[$i]['FKEumis'];
            $rifiuti[$indice]['ID_IMP']=$FEDIT->DbRecordSet[$i]['ID_IMP'];
            $rifiuti[$indice]['workmode']=$FEDIT->DbRecordSet[$i]['workmode'];
            if($FEDIT->DbRecordSet[$i]['TIPO']=="C"){
                $rifiuti[$indice]['carico'][0]['qta']=$FEDIT->DbRecordSet[$i]['quantita'];
                $rifiuti[$indice]['carico'][0]['FKEpesospecifico']=$FEDIT->DbRecordSet[$i]['FKEpesospecifico'];
                $rifiuti[$indice]['carico'][0]['FKEumis']=$FEDIT->DbRecordSet[$i]['FKEumis'];
                $rifiuti[$indice]['carico'][0]['num']=$FEDIT->DbRecordSet[$i]['NMOV'];
                $rifiuti[$indice]['carico'][0]['ID_MOV_F']=$FEDIT->DbRecordSet[$i]['ID_MOV_F'];
                $rifiuti[$indice]['carico'][0]['idSIS']=$FEDIT->DbRecordSet[$i]['idSIS'];
                $rifiuti[$indice]['carico'][0]['data']=$FEDIT->DbRecordSet[$i]['DTMOV'];
                $rifiuti[$indice]['carico'][0]['ID_IMP']=$FEDIT->DbRecordSet[$i]['ID_IMP'];
                $rifiuti[$indice]['carico'][0]['ID_UIMP']=$FEDIT->DbRecordSet[$i]['ID_UIMP'];
                $rifiuti[$indice]['carico'][0]['workmode']=$FEDIT->DbRecordSet[$i]['workmode'];
                $rifiuti[$indice]['totC']+=$FEDIT->DbRecordSet[$i]['quantita'];
            }
            if($FEDIT->DbRecordSet[$i]['TIPO']=="S")
                @$rifiuti[$indice]['totS']=$FEDIT->DbRecordSet[$i]['quantita'];
        }
        else{
            $rifiuti[$indice-1]['RifID']=$FEDIT->DbRecordSet[$i]['RifID'];
            # somme
            $countCarichi=(isset($rifiuti[$indice-1]['carico'])? count($rifiuti[$indice-1]['carico']) : 0);
            if($FEDIT->DbRecordSet[$i]['TIPO']=="C"){

                // dati quantitativi
                $rifiuti[$indice-1]['carico'][$countCarichi]['qta']=$FEDIT->DbRecordSet[$i]['quantita'];
                $rifiuti[$indice-1]['carico'][$countCarichi]['FKEpesospecifico']=$FEDIT->DbRecordSet[$i]['FKEpesospecifico'];
                $rifiuti[$indice-1]['carico'][$countCarichi]['FKEumis']=$FEDIT->DbRecordSet[$i]['FKEumis'];
                $rifiuti[$indice-1]['totC']+=$FEDIT->DbRecordSet[$i]['quantita'];

                // per riferimenti movimenti di carico
                if($FEDIT->DbRecordSet[$i]['PerRiclassificazione']==0){
                    $rifiuti[$indice-1]['carico'][$countCarichi]['num']=$FEDIT->DbRecordSet[$i]['NMOV'];
                    $rifiuti[$indice-1]['carico'][$countCarichi]['ID_MOV_F']=$FEDIT->DbRecordSet[$i]['ID_MOV_F'];
                    $rifiuti[$indice-1]['carico'][$countCarichi]['idSIS']=$FEDIT->DbRecordSet[$i]['idSIS'];
                }

                // per calcolo deposito temporaneo
                $rifiuti[$indice-1]['carico'][$countCarichi]['data']=$FEDIT->DbRecordSet[$i]['DTMOV'];

                // dati identificativi
                $rifiuti[$indice-1]['carico'][$countCarichi]['ID_IMP']=$FEDIT->DbRecordSet[$i]['ID_IMP'];
                $rifiuti[$indice-1]['carico'][$countCarichi]['ID_UIMP']=$FEDIT->DbRecordSet[$i]['ID_UIMP'];
                $rifiuti[$indice-1]['carico'][$countCarichi]['workmode']=$FEDIT->DbRecordSet[$i]['workmode'];
            }

            if($FEDIT->DbRecordSet[$i]['TIPO']=="S")
                @$rifiuti[$indice-1]['totS']+=$FEDIT->DbRecordSet[$i]['quantita'];
            }
        $UIMPbuf=$FEDIT->DbRecordSet[$i]['ID_UIMP'];
        $RIFbuf=$FEDIT->DbRecordSet[$i]['originalRifID'];
        }

    //if($SOGER->UserData['core_usersusr']=='sintem')	die(var_dump($rifiuti));

    $FEDIT->DbConn('soger'.$YEAR);
}

# elimino i record gi� presenti in user_movimenti_giacenze_iniziali
$sql = "DELETE FROM user_movimenti_giacenze_iniziali WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND workmode='".$SOGER->UserData['workmode']."';";
$FEDIT->SDBWrite($sql,true,false);

if(!empty($rifiuti)){
    for($r=0;$r<count($rifiuti);$r++){
        $carichi=RemoveCarichiScaricati($rifiuti[$r], $CarichiRifiuto);
        if($carichi){
            foreach($carichi AS $rif=>$carico){
                # calcola pesoN
                for($c=0;$c<count($carico);$c++){
                    $carico[$c]['pesoN']=$carico[$c]['qta'];
                    if($carico[$c]['FKEumis']=='Litri') $carico[$c]['pesoN']=$carico[$c]['qta']*$carico[$c]['FKEpesospecifico'];
                    if($carico[$c]['FKEumis']=='Mc.') $carico[$c]['pesoN']=$carico[$c]['qta']*$carico[$c]['FKEpesospecifico']*1000;
                    $sql ="INSERT INTO user_movimenti_giacenze_iniziali ";
                    $sql.="(ID_RIF, ID_MOV_F, NMOV, DTMOV, idSIS, quantita, pesoN, TIPO, ID_UIMP, FKEpesospecifico, FKEumis, ID_IMP, workmode) ";
                    $sql.="VALUES (".$rif.", ".$carico[$c]['ID_MOV_F'].", ".$carico[$c]['num'].", '".$carico[$c]['data']."', '".$carico[$c]['idSIS']."', ".$carico[$c]['qta'].", ".$carico[$c]['pesoN'].", 'C', ".$carico[$c]['ID_UIMP'].", ".$carico[$c]['FKEpesospecifico'].", '".$carico[$c]['FKEumis']."', '".$carico[$c]['ID_IMP']."', '".$carico[$c]['workmode']."');";
                    $FEDIT->SDBWrite($sql,true,false);
                }
            }
        }
    }
}

$sql = "UPDATE user_movimenti_giacenze_iniziali SET idSIS=NULL WHERE idSIS='' OR idSIS=0 AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND workmode='".$SOGER->UserData['workmode']."';";
$FEDIT->SDBWrite($sql,true,false);

$sql = "UPDATE user_movimenti_giacenze_iniziali SET NMOV='Giac.Iniz.', DTMOV='".$YEAR."-01-01' WHERE NMOV=0 AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND workmode='".$SOGER->UserData['workmode']."';";
$FEDIT->SDBWrite($sql,true,false);

$sql ="UPDATE `soger".$YEAR."`.`user_movimenti_giacenze_iniziali` ";
$sql.="JOIN `soger".$YEAR_in_session."`.`user_movimenti_fiscalizzati` ON ";
$sql.="`soger".$YEAR."`.`user_movimenti_giacenze_iniziali`.`ID_MOV_F`=`soger".$YEAR_in_session."`.`user_movimenti_fiscalizzati`.`ID_MOV_F` ";
$sql.="SET `soger".$YEAR."`.`user_movimenti_giacenze_iniziali`.`ID_UIMP`=`soger".$YEAR_in_session."`.`user_movimenti_fiscalizzati`.`ID_UIMP`;";
$FEDIT->SDBWrite($sql,true,false);

// torno al db dell'anno precedente da cui ho lanciato la funzione
$FEDIT->DbConn('soger'.$YEAR_in_session);


require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>