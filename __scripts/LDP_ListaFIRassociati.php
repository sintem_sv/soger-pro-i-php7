<?php
session_start();
global $SOGER;
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/SQLFunct.php");

## READ FIR

if($_POST['LINK_DestProd']!=''){
	$SQL = "SELECT ID_MOV_F, NMOV, user_aziende_produttori.description AS FORNITORE, IF(DTMOV IS NULL, 'n.d.', DTMOV) AS DTMOV, NFORM, CONCAT(lov_cer.COD_CER, ' - ', user_schede_rifiuti.descrizione) AS rifiuto, CONCAT(quantita, ' ', FKEumis) AS quantita ";
	$SQL.= "FROM user_movimenti_fiscalizzati ";
	$SQL.= "JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_movimenti_fiscalizzati.ID_RIF ";
	$SQL.= "JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
	$SQL.= "JOIN user_aziende_produttori ON user_aziende_produttori.ID_AZP=user_movimenti_fiscalizzati.ID_AZP ";
	$SQL.= "WHERE ID_MOV_F IN (".$_POST['LINK_DestProd'].") AND user_movimenti_fiscalizzati.destinatario=1 AND user_movimenti_fiscalizzati.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' ";
	$SQL.= "ORDER BY NMOV ASC ";
	$FEDIT->SDBRead($SQL,"DbRecordSetFormulari",true,false);
	}

$Formulari = (isset($FEDIT->DbRecordSetFormulari) ? $FEDIT->DbRecordSetFormulari : array());

echo json_encode($Formulari);

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>