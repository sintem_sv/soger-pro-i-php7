<?php

//print_r($FEDIT->DbRecordSet);
$rifiuti=array();

# creo array "cumulativo"
/*
for($r=0;$r<count($FEDIT->DbRecordSet);$r++){
	$counter=count($rifiuti);	
	if($counter>0){
		if($rifiuti[$counter-1]['ID_RIF'] != $FEDIT->DbRecordSet[$r]['ID_RIF']){
			$rifiuti[$counter]=$FEDIT->DbRecordSet[$r];
			}
		else{
			if($rifiuti[$counter-1]['ID_MOV_F'] != $FEDIT->DbRecordSet[$r]['ID_MOV_F']){
				$rifiuti[$counter-1]['pesoN']+=$FEDIT->DbRecordSet[$r]['pesoN'];	
				}
			$rifiuti[$counter-1]['euro']+=$FEDIT->DbRecordSet[$r]['euro'];	
			}
		}
	else{
		$rifiuti[$counter]=$FEDIT->DbRecordSet[$r];
		}

	}
*/

$ID_MOV_F_buffer	= 0;
$ID_RIF_buffer		= 0;

for($m=0;$m<count($FEDIT->DbRecordSet);$m++){
	if($FEDIT->DbRecordSet[$m]['ID_RIF']!=$ID_RIF_buffer){
		$rifiuti[$FEDIT->DbRecordSet[$m]['ID_RIF']]['descrizione'] = $FEDIT->DbRecordSet[$m]['descrizione'];
		$rifiuti[$FEDIT->DbRecordSet[$m]['ID_RIF']]['COD_CER'] = $FEDIT->DbRecordSet[$m]['COD_CER'];
		$rifiuti[$FEDIT->DbRecordSet[$m]['ID_RIF']]['pesoN'] = $FEDIT->DbRecordSet[$m]['pesoN'];
		$rifiuti[$FEDIT->DbRecordSet[$m]['ID_RIF']]['euro']	= $FEDIT->DbRecordSet[$m]['euro'];
		$ID_RIF_buffer = $FEDIT->DbRecordSet[$m]['ID_RIF'];
		$ID_MOV_F_buffer = $FEDIT->DbRecordSet[$m]['ID_MOV_F'];
		}
	else{
		if($FEDIT->DbRecordSet[$m]['ID_MOV_F']!=$ID_MOV_F_buffer){
			$rifiuti[$FEDIT->DbRecordSet[$m]['ID_RIF']]['pesoN'] += $FEDIT->DbRecordSet[$m]['pesoN'];
			$rifiuti[$FEDIT->DbRecordSet[$m]['ID_RIF']]['euro']	+= $FEDIT->DbRecordSet[$m]['euro'];
			$ID_MOV_F_buffer = $FEDIT->DbRecordSet[$m]['ID_MOV_F'];
			}
		else{
			$rifiuti[$FEDIT->DbRecordSet[$m]['ID_RIF']]['euro']	+= $FEDIT->DbRecordSet[$m]['euro'];
			}
		}
	//echo "dopo rifiuto ".$FEDIT->DbRecordSet[$m]['ID_RIF']." e movimento ".$FEDIT->DbRecordSet[$m]['ID_MOV_F'].":<br />";
	//var_dump($rifiuti);
	}

//var_dump($rifiuti); die();


if(!$asCSV) {
	$FEDIT->FGE_PdfBuffer->AddPage();
	$Ypos = 10;
	}

# intestazione "tabella"

	if(!$asCSV) {
		$Ypos+=10;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
		$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,7,"CER","LBTR","C");
		$FEDIT->FGE_PdfBuffer->SetXY(30,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(120,7,"Descrizione","BTR","C");
		$FEDIT->FGE_PdfBuffer->SetXY(150,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,7,"Kg Movimentati","BTR","C");
		$FEDIT->FGE_PdfBuffer->SetXY(190,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,7,"Spesa / Ricavo (�)","BTR","C");
		$FEDIT->FGE_PdfBuffer->SetXY(230,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,7,"Costo al Kg (�)","BTR","C");
		$Ypos+=7;
		}
	else{
		$CSVbuf .= AddCSVRow();	
		$CSVbuf .= AddCSVcolumn("CER");
		$CSVbuf .= AddCSVcolumn("Descrizione");
		$CSVbuf .= AddCSVcolumn("Kg Movimentati");
		$CSVbuf .= AddCSVcolumn("Spesa / Ricavo (�)");
		$CSVbuf .= AddCSVcolumn("Costo al Kg (�)");
		$CSVbuf .= AddCSVRow();	
		}


# calcolo costo al Kg

foreach($rifiuti as $k => $v){
	$rifiuti[$k]['costoKg']=$v['euro'] / $v['pesoN'];
	}


foreach($rifiuti as $k => $v){
	if(!$asCSV) {
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
		$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,5,$v['COD_CER'],"LBR","C");
		$FEDIT->FGE_PdfBuffer->SetXY(30,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(120,5,$v['descrizione'],"BR","L");
		$FEDIT->FGE_PdfBuffer->SetXY(150,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,5,$v['pesoN'],"BR","R");
		$FEDIT->FGE_PdfBuffer->SetXY(190,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,5,number_format(round($v['euro'], 2), 2, ",", ""),"BR","R");
		$FEDIT->FGE_PdfBuffer->SetXY(230,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,5,number_format(round($v['costoKg'], 2), 2, ",", ""),"BR","R");
		$Ypos+=5;
		}
	else{
		$CSVbuf .= AddCSVcolumn($v['COD_CER']);
		$CSVbuf .= AddCSVcolumn($v['descrizione']);
		$CSVbuf .= AddCSVcolumn($v['pesoN']);
		$CSVbuf .= AddCSVcolumn(number_format(round($v['euro'], 2), 2, ",", ""));
		$CSVbuf .= AddCSVcolumn(number_format(round($v['costoKg'], 2), 2, ",", ""));
		$CSVbuf .= AddCSVRow();	
		}
	}

?>