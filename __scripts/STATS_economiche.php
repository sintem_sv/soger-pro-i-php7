<?php

//print_r($FEDIT->DbRecordSet);

$RecordSet[0]['id_azienda']=$FEDIT->DbRecordSet[0][$AzKey];
$RecordSet[0]['azienda']=$FEDIT->DbRecordSet[0]['description'];
$RecordSet[0]['indirizzo']=$FEDIT->DbRecordSet[0]['indirizzo'];
$RecordSet[0]['comune']=$FEDIT->DbRecordSet[0]['comune'];
$RecordSet[0]['prov']=$FEDIT->DbRecordSet[0]['PROV'];
$RecordSet[0]['cap']=$FEDIT->DbRecordSet[0]['CAP'];

$RecordSet[0]['id_rifiuto']=$FEDIT->DbRecordSet[0]['ID_RIF'];
$RecordSet[0]['rifiuto'][0]['descrizione']=$FEDIT->DbRecordSet[0]['descrizione'];
$RecordSet[0]['rifiuto'][0]['CER']=$FEDIT->DbRecordSet[0]['COD_CER'];

$RecordSet[0]['rifiuto'][0]['id_cond']=$FEDIT->DbRecordSet[0][$cond_table_key];
$RecordSet[0]['rifiuto'][0]['cond'][0]['condizione']=$FEDIT->DbRecordSet[0]['condizione'];
$RecordSet[0]['rifiuto'][0]['cond'][0]['euro']=$FEDIT->DbRecordSet[0]['euro'];
$RecordSet[0]['rifiuto'][0]['cond'][0]['metodo']=$FEDIT->DbRecordSet[0]['FKE_UM'];

$counter=count($RecordSet)-1;

for($t=1;$t<count($FEDIT->DbRecordSet);$t++){

	if($FEDIT->DbRecordSet[$t][$AzKey]==$RecordSet[$counter]['id_azienda']){
		# stessa azienda, aggiungo solo se rifiuto � diverso

		if($FEDIT->DbRecordSet[$t]['ID_RIF']!=$RecordSet[$counter]['id_rifiuto']){
			
			$RecordSet[$counter]['id_rifiuto']=$FEDIT->DbRecordSet[$t]['ID_RIF'];
				$cr=count($RecordSet[$counter]['rifiuto']);
				$RecordSet[$counter]['rifiuto'][$cr]['descrizione']=$FEDIT->DbRecordSet[$t]['descrizione'];
				$RecordSet[$counter]['rifiuto'][$cr]['CER']=$FEDIT->DbRecordSet[$t]['COD_CER'];

			$RecordSet[$counter]['rifiuto'][$cr]['id_cond']=$FEDIT->DbRecordSet[$t][$cond_table_key];
				$RecordSet[$counter]['rifiuto'][$cr]['cond'][0]['condizione']=$FEDIT->DbRecordSet[$t]['condizione'];
				$RecordSet[$counter]['rifiuto'][$cr]['cond'][0]['euro']=$FEDIT->DbRecordSet[$t]['euro'];
				$RecordSet[$counter]['rifiuto'][$cr]['cond'][0]['metodo']=$FEDIT->DbRecordSet[$t]['FKE_UM'];
			}
		else{ #stesso rifiuto, aggiungo solo se cond. cont. diversa
			$cr=count($RecordSet[$counter]['rifiuto']);
			$cc=count($RecordSet[$counter]['rifiuto'][$cr-1]['cond']);
			if($FEDIT->DbRecordSet[$t][$cond_table_key]!=$RecordSet[$counter]['rifiuto'][$cr-1]['id_cond']){
				$RecordSet[$counter]['rifiuto'][$cr-1]['id_cond']=$FEDIT->DbRecordSet[$t][$cond_table_key];
				$RecordSet[$counter]['rifiuto'][$cr-1]['cond'][$cc]['condizione']=$FEDIT->DbRecordSet[$t]['condizione'];
				$RecordSet[$counter]['rifiuto'][$cr-1]['cond'][$cc]['euro']=$FEDIT->DbRecordSet[$t]['euro'];
				$RecordSet[$counter]['rifiuto'][$cr-1]['cond'][$cc]['metodo']=$FEDIT->DbRecordSet[$t]['FKE_UM'];
				}
			else{ #stessa cond. cont., sommo!
				$RecordSet[$counter]['rifiuto'][$cr-1]['cond'][$cc-1]['euro']+=$FEDIT->DbRecordSet[$t]['euro'];
				}
			}

		}
	else{ #diversa azienda, aggiungo..
		$counter++;

		$RecordSet[$counter]['id_azienda']=$FEDIT->DbRecordSet[$t][$AzKey];
		$RecordSet[$counter]['azienda']=$FEDIT->DbRecordSet[$t]['description'];
		$RecordSet[$counter]['indirizzo']=$FEDIT->DbRecordSet[$t]['indirizzo'];
		$RecordSet[$counter]['comune']=$FEDIT->DbRecordSet[$t]['comune'];
		$RecordSet[$counter]['prov']=$FEDIT->DbRecordSet[$t]['PROV'];
		$RecordSet[$counter]['cap']=$FEDIT->DbRecordSet[$t]['CAP'];

		$RecordSet[$counter]['id_rifiuto']=$FEDIT->DbRecordSet[$t]['ID_RIF'];
		$RecordSet[$counter]['rifiuto'][0]['descrizione']=$FEDIT->DbRecordSet[$t]['descrizione'];
		$RecordSet[$counter]['rifiuto'][0]['CER']=$FEDIT->DbRecordSet[$t]['COD_CER'];

		$RecordSet[$counter]['rifiuto'][0]['id_cond']=$FEDIT->DbRecordSet[$t][$cond_table_key];
		$RecordSet[$counter]['rifiuto'][0]['cond'][0]['condizione']=$FEDIT->DbRecordSet[$t]['condizione'];
		$RecordSet[$counter]['rifiuto'][0]['cond'][0]['euro']=$FEDIT->DbRecordSet[$t]['euro'];
		$RecordSet[$counter]['rifiuto'][0]['cond'][0]['metodo']=$FEDIT->DbRecordSet[$t]['FKE_UM'];
		}
	
	}

//print_r($RecordSet);

$Ypos+=10;

for($a=0;$a<count($RecordSet);$a++){
	$Ypos+=20;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(180,4,$RecordSet[$a]['azienda'],0,"L");
	
	$Ypos+=3;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(180,4,$RecordSet[$a]['indirizzo'],0,"L");
	$Ypos+=3;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(180,4,$RecordSet[$a]['cap']." ".$RecordSet[$a]['comune']." ( ".$RecordSet[$a]['prov']." )",0,"L");

	for($r=0;$r<count($RecordSet[$a]['rifiuto']);$r++){
		$Ypos+=10;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(180,4,$RecordSet[$a]['rifiuto'][$r]['CER']." - ".$RecordSet[$a]['rifiuto'][$r]['descrizione'],"B","C");

		$Ypos+=5;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(180,4,"Tipologia",0,"L");
		
		$FEDIT->FGE_PdfBuffer->SetXY(80,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(115,4,"Costo / Ricavo ( � )",0,"L");
		
		$totale=0;
		for($c=0;$c<count($RecordSet[$a]['rifiuto'][$r]['cond']);$c++){
			$Ypos+=5;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
			$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(180,4,$RecordSet[$a]['rifiuto'][$r]['cond'][$c]['condizione'],0,"L");
			$FEDIT->FGE_PdfBuffer->SetXY(80,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(115,4,number_format(round($RecordSet[$a]['rifiuto'][$r]['cond'][$c]['euro'], 2), 2, ",", ""),0,"L");
			$totale+=round($RecordSet[$a]['rifiuto'][$r]['cond'][$c]['euro'], 2);
			}

		$Ypos+=5;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(180,4,"Totale:","T","L");
		
		$FEDIT->FGE_PdfBuffer->SetXY(80,$Ypos);
		//$FEDIT->FGE_PdfBuffer->MultiCell(115,4,number_format($totale, 2, ',', ''),"T","L");
		$FEDIT->FGE_PdfBuffer->MultiCell(115,4,number_format(round($totale,2), 2, ',', ''),"T","L");
		}
	
	}	

?>