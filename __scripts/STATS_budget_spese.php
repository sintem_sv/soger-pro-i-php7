<?php

//print_r($FEDIT->DbRecordSet);

$last_rifiuto=0;
$rifiuti=array();

for($s=0;$s<count($FEDIT->DbRecordSet);$s++){
	$index=count($rifiuti);
	if($FEDIT->DbRecordSet[$s]['ID_RIF']!=$last_rifiuto){ # nuovo rifiuto
		$last_rifiuto=$FEDIT->DbRecordSet[$s]['ID_RIF'];
		$rifiuti[$index]['CER']=$FEDIT->DbRecordSet[$s]['COD_CER']; 
		$rifiuti[$index]['rifiuto']=$FEDIT->DbRecordSet[$s]['descrizione']; 
		$rifiuti[$index]['budget_anno']=$FEDIT->DbRecordSet[$s]['euro_anno']; 
		$rifiuti[$index]['budget_gennaio']=$FEDIT->DbRecordSet[$s]['euro_gen']; 
		$rifiuti[$index]['budget_febbraio']=$FEDIT->DbRecordSet[$s]['euro_feb']; 
		$rifiuti[$index]['budget_marzo']=$FEDIT->DbRecordSet[$s]['euro_mar']; 
		$rifiuti[$index]['budget_aprile']=$FEDIT->DbRecordSet[$s]['euro_apr'];
		$rifiuti[$index]['budget_maggio']=$FEDIT->DbRecordSet[$s]['euro_mag'];
		$rifiuti[$index]['budget_giugno']=$FEDIT->DbRecordSet[$s]['euro_giu'];
		$rifiuti[$index]['budget_luglio']=$FEDIT->DbRecordSet[$s]['euro_lug'];
		$rifiuti[$index]['budget_agosto']=$FEDIT->DbRecordSet[$s]['euro_ago'];
		$rifiuti[$index]['budget_settembre']=$FEDIT->DbRecordSet[$s]['euro_set'];
		$rifiuti[$index]['budget_ottobre']=$FEDIT->DbRecordSet[$s]['euro_ott'];
		$rifiuti[$index]['budget_novembre']=$FEDIT->DbRecordSet[$s]['euro_nov'];
		$rifiuti[$index]['budget_dicembre']=$FEDIT->DbRecordSet[$s]['euro_dic'];
		$rifiuti[$index]['spesa_anno']=$FEDIT->DbRecordSet[$s]['euro'];
		$mese=explode("-", $FEDIT->DbRecordSet[$s]['DT']);
		switch($mese[1]){
			case '01':
				$rifiuti[$index]['spesa_gennaio']=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '02':
				$rifiuti[$index]['spesa_febbraio']=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '03':
				$rifiuti[$index]['spesa_marzo']=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '04':
				$rifiuti[$index]['spesa_aprile']=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '05':
				$rifiuti[$index]['spesa_maggio']=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '06':
				$rifiuti[$index]['spesa_giugno']=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '07':
				$rifiuti[$index]['spesa_luglio']=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '08':
				$rifiuti[$index]['spesa_agosto']=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '09':
				$rifiuti[$index]['spesa_settembre']=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '10':
				$rifiuti[$index]['spesa_ottobre']=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '11':
				$rifiuti[$index]['spesa_novembre']=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '12':
				$rifiuti[$index]['spesa_dicembre']=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			}
		}
	else{ # rifiuto uguale al precedente
		$last_rifiuto=$FEDIT->DbRecordSet[$s]['ID_RIF'];
		$rifiuti[$index-1]['spesa_anno']+=$FEDIT->DbRecordSet[$s]['euro'];
		$mese=explode("-", $FEDIT->DbRecordSet[$s]['DT']);
		switch($mese[1]){
			case '01':
				$rifiuti[$index-1]['spesa_gennaio']+=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '02':
				$rifiuti[$index-1]['spesa_febbraio']+=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '03':
				$rifiuti[$index-1]['spesa_marzo']+=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '04':
				$rifiuti[$index-1]['spesa_aprile']+=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '05':
				$rifiuti[$index-1]['spesa_maggio']+=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '06':
				$rifiuti[$index-1]['spesa_giugno']+=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '07':
				$rifiuti[$index-1]['spesa_luglio']+=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '08':
				$rifiuti[$index-1]['spesa_agosto']+=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '09':
				$rifiuti[$index-1]['spesa_settembre']+=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '10':
				$rifiuti[$index-1]['spesa_ottobre']+=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '11':
				$rifiuti[$index-1]['spesa_novembre']+=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			case '12':
				$rifiuti[$index-1]['spesa_dicembre']+=$FEDIT->DbRecordSet[$s]['euro'];
				break;
			}
		}
	}

//print_r($rifiuti);

$Ypos+=10;


for($r=0;$r<count($rifiuti);$r++){
	$Ypos+=20;

	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);

	$FEDIT->FGE_PdfBuffer->MultiCell(180,4,$rifiuti[$r]['CER']." - ".$rifiuti[$r]['rifiuto'],"B","C");
	
	$Ypos+=8;
	
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Periodo",0,"L");
	$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Budget (�)",0,"L");
	$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Spesa (�)",0,"L");
	$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(135,4,"Differenza (�)",0,"L");
	
	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Gennaio","T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_gennaio'], 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format(abs(@$rifiuti[$r]['spesa_gennaio']), 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_gennaio'] - abs(@$rifiuti[$r]['spesa_gennaio']), 2, ',', ''),"T","L");

	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Febbraio","T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_febbraio'], 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format(abs(@$rifiuti[$r]['spesa_febbraio']), 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_febbraio'] - abs(@$rifiuti[$r]['spesa_febbraio']), 2, ',', ''),"T","L");

	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Marzo","T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_marzo'], 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format(abs(@$rifiuti[$r]['spesa_marzo']), 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_marzo'] - abs(@$rifiuti[$r]['spesa_marzo']), 2, ',', ''),"T","L");

	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Aprile","T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_aprile'], 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format(abs(@$rifiuti[$r]['spesa_aprile']), 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_aprile'] - abs(@$rifiuti[$r]['spesa_aprile']), 2, ',', ''),"T","L");

	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Maggio","T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_maggio'], 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format(abs(@$rifiuti[$r]['spesa_maggio']), 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_maggio'] - abs(@$rifiuti[$r]['spesa_maggio']), 2, ',', ''),"T","L");

	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Giugno","T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_giugno'], 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format(abs(@$rifiuti[$r]['spesa_giugno']), 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_giugno'] - abs(@$rifiuti[$r]['spesa_giugno']), 2, ',', ''),"T","L");

	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Luglio","T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_luglio'], 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format(abs(@$rifiuti[$r]['spesa_luglio']), 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_luglio'] - abs(@$rifiuti[$r]['spesa_luglio']), 2, ',', ''),"T","L");

	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Agosto","T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_agosto'], 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format(abs(@$rifiuti[$r]['spesa_agosto']), 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_agosto'] - abs(@$rifiuti[$r]['spesa_agosto']), 2, ',', ''),"T","L");

	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Settembre","T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_settembre'], 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format(abs(@$rifiuti[$r]['spesa_settembre']), 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_settembre'] - abs(@$rifiuti[$r]['spesa_settembre']) , 2, ',', ''),"T","L");

	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Ottobre","T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_ottobre'], 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format(abs(@$rifiuti[$r]['spesa_ottobre']), 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_ottobre'] - abs(@$rifiuti[$r]['spesa_ottobre']), 2, ',', ''),"T","L");

	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Novembre","T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_novembre'], 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format(abs(@$rifiuti[$r]['spesa_novembre']), 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_novembre'] - abs(@$rifiuti[$r]['spesa_novembre']), 2, ',', ''),"T","L");

	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Dicembre","T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_dicembre'], 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format(abs(@$rifiuti[$r]['spesa_dicembre']), 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_dicembre'] - abs(@$rifiuti[$r]['spesa_dicembre']), 2, ',', ''),"T","L");


	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
	$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,"Anno","T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(45,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_anno'], 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(75,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format(abs(@$rifiuti[$r]['spesa_anno']), 2, ',', ''),"T","L");
	$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,4,number_format($rifiuti[$r]['budget_anno'] - abs(@$rifiuti[$r]['spesa_anno']), 2, ',', ''),"T","L");

	}


?>