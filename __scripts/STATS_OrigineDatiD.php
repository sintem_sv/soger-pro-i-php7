<?php

//print_r($FEDIT->DbRecordSet);
//print_r($_POST["Stat"]);

$aziende=array();
$AzIDbuffer=0;
$c=-1;
for($a=0;$a<count($FEDIT->DbRecordSet);$a++){
	if($FEDIT->DbRecordSet[$a]['AzID']!=$AzIDbuffer){
		$AzIDbuffer=$FEDIT->DbRecordSet[$a]['AzID'];
		$c++;
		$aziende[$c]['azienda']=$FEDIT->DbRecordSet[$a]['azienda'];
		$aziende[$c]['indirizzo']=$FEDIT->DbRecordSet[$a]['indirizzoSL']." - ".$FEDIT->DbRecordSet[$a]['CAPSL'].", ".$FEDIT->DbRecordSet[$a]['comuneSL']." (".$FEDIT->DbRecordSet[$a]['provSL'].")";
		$aziende[$c]['autorizzazione'][0]=$FEDIT->DbRecordSet[$a]['num_aut'];
		$aziende[$c]['ID_AUTH'][0]=$FEDIT->DbRecordSet[$a]['ID_AUTH'];
		$aziende[$c]['origine_dati'][0]=$FEDIT->DbRecordSet[$a]['origine_dati'];
		$aziende[$c]['IdOD'][0]=$FEDIT->DbRecordSet[$a]['ID_ORIGINE_DATI'];
		$aziende[$c]['rifiuto'][0]=$FEDIT->DbRecordSet[$a]['COD_CER']." - ".$FEDIT->DbRecordSet[$a]['descrizione'];
		}
	else{
		$aziende[$c]['autorizzazione'][count($aziende[$c]['autorizzazione'])]=$FEDIT->DbRecordSet[$a]['num_aut'];
		$aziende[$c]['ID_AUTH'][count($aziende[$c]['ID_AUTH'])]=$FEDIT->DbRecordSet[$a]['ID_AUTH'];
		$aziende[$c]['origine_dati'][count($aziende[$c]['origine_dati'])]=$FEDIT->DbRecordSet[$a]['origine_dati'];
		$aziende[$c]['IdOD'][count($aziende[$c]['IdOD'])]=$FEDIT->DbRecordSet[$a]['ID_ORIGINE_DATI'];
		$aziende[$c]['rifiuto'][count($aziende[$c]['rifiuto'])]=$FEDIT->DbRecordSet[$a]['COD_CER']." - ".$FEDIT->DbRecordSet[$a]['descrizione'];
		}
	}

//print_r($aziende);

$Xpos = 20;
$Ypos = $FEDIT->FGE_PdfBuffer->GetY();
$Ypos+= 10;


for($a=0;$a<count($aziende);$a++){
	$Xpos = 20;
	$Ypos+= 10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(265,4,$aziende[$a]['azienda'],"LRT","C");
	$Ypos+=3;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',6);
	$FEDIT->FGE_PdfBuffer->MultiCell(265,4,$aziende[$a]['indirizzo'],"LRB","C");
	$Ypos+=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',7);
	$FEDIT->FGE_PdfBuffer->MultiCell(265,4,"Autorizzazioni:","","C");
	$Ypos+=3;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$AuthIdODBuffer=0;
	for($auth=0;$auth<count($aziende[$a]['autorizzazione']);$auth++){
		$Xpos = 20;
		if($auth==0){
			$Ypos+=5;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			}
		
		if($AuthIdODBuffer!=$aziende[$a]['IdOD'][$auth]){
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',7);
			$FEDIT->FGE_PdfBuffer->MultiCell(85,4,$aziende[$a]['origine_dati'][$auth].":","","L");			
			$Ypos+=4;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			}
		$AuthIdODBuffer=$aziende[$a]['IdOD'][$auth];	

		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
		$FEDIT->FGE_PdfBuffer->MultiCell(265,4,$aziende[$a]['autorizzazione'][$auth]." per il rifiuto ".$aziende[$a]['rifiuto'][$auth],"","L");
		$Ypos+=4;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		}
	}

?>