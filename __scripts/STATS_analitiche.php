<?php
$counter = 0;
$showGiacIni=true;


if(isset($_POST["MovDa"]) && $_POST["MovDa"]!="") {
	$RawDa = explode("/",$_POST["MovDa"]);
	if($RawDa[0]!="01" | $RawDa[1]!="01"){
		$showGiacIni=false;
		}
	}

$DbRecsNum = $FEDIT->DbRecsNum;

# ma � giusto???
//if($SOGER->UserData["workmode"]!="produttore") $showGiacIni=true;

# non mostro giacenza iniziale se la statistica � per un workmode diverso dal mio ( es: wm=produttore & stat=analitica_trasp )
switch($_POST["Stat"]){
	case "AnPRO":
		$ref="produttore";
		break;
	case "AnTRA":
		$ref="trasportatore";
		break;
	case "AnDES":
		$ref="destinatario";
		break;
	case "AnINT":
		$ref="intermediario";
		break;
	}
if($SOGER->UserData['workmode']!=$ref) $showGiacIni=false;

foreach($FEDIT->DbRecordSet as $k=>$dati) {
	if($PrintImpianto) {
		if(($tmpCER!=$dati["CERref"] | $dati["IMPref"]!=$tmpIMP | $dati["AZref"]!=$tmpAZ) && $counter>0) {
			if($counterVar>0) $VarMedia=$TotVar/$counterVar; else $VarMedia=$counterVar;
			SommaMovimenti($Ypos,$TotCarico,$TotScarico,$TotPesoDestino,$TotTara,$TotLordo,$TotCosto,$VarMedia,$FEDIT->FGE_PdfBuffer,$asCSV,$CSVbuf,$SOGER->UserData['core_usersG3']);
		}
	} else {
		if(($tmpCER!=$dati["CERref"] | $dati["AZref"]!=$tmpAZ) && $counter>0) {
			if($counterVar>0) $VarMedia=$TotVar/$counterVar; else $VarMedia=$counterVar;
			SommaMovimenti($Ypos,$TotCarico,$TotScarico,$TotPesoDestino,$TotLordo,$TotLordo,$TotCosto,$VarMedia,$FEDIT->FGE_PdfBuffer,$asCSV,$CSVbuf,$SOGER->UserData['core_usersG3']);
		}		
	}
	if($dati["AZref"]!=$tmpAZ) {
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
		# dati azienda
		$Ypos  += 10;
		$tmpAZ  = $dati["AZref"];
		$tmpIMP = "";
		$tmpCER = "";
//{{{ 
		if(!$asCSV) {		
			$Ypos=20;
			$FEDIT->FGE_PdfBuffer->AddPage();
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
			$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$dati["AZdes"]." ( C.F. ".$dati['AZCF'].")",0,"L");
			$Ypos += 4;
			$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
			$AzAddr = $dati["AZind"] . " - " . $dati['AZCAP'] . " - " . $dati["AZcom"] . " (" . $dati["AZshprov"] . ") - " . $dati["AZnaz"];    
			$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$AzAddr,0,"L");
			$Ypos += 4; 
			} 
		else {
			$azienda = $dati["AZdes"] ." ( C.F. ".$dati['AZCF']." ) " . $dati["AZind"] . " - " . $dati['AZCAP'] . " - " . $dati["AZcom"] . " (" . $dati["AZshprov"] . ") - " . $dati["AZnaz"];
			$CSVbuf .= AddCSVcolumn($azienda);
			$CSVbuf .= AddCSVRow();
			}
	}
	if($PrintImpianto) {
		if($dati["IMPref"]!=$tmpIMP) {
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
			$tmpIMP = $dati["IMPref"];
			$tmpCER = "";
			# dati impianto
	//{{{ 
			if(!$asCSV) {
				$Ypos += 3;
				$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
				$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Impianto di:",0,"L");
				$ImpAddr = $dati["IMPdes"] . " - " . $dati['IMPCAP'] . " - " . $dati["IMPcom"] . " (" . $dati["IMPshprov"] . ") - " . $dati["IMPnaz"];
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
				$FEDIT->FGE_PdfBuffer->SetXY(32,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$ImpAddr,0,"L");
				$Ypos += 4;
			} else {
				$impianto = "Impianto di: " .  $dati["IMPdes"] . " - " . $dati['IMPCAP'] . " - " . $dati["IMPcom"] . " (" . $dati["IMPshprov"] . ") - " . $dati["IMPnaz"];
				$CSVbuf .= AddCSVcolumn($impianto);
				$CSVbuf .= AddCSVRow();
				$CSVbuf .= AddCSVRow();
			}
			//}}}
		}
	}
	if($tmpCER!=$dati["CERref"]) {
		$counterVar = 0;
		$TotVar = 0;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
		$tmpCER = $dati["CERref"];
		$CERdes = $dati["COD_CER"] . " - " . $dati["descrizione"];
		# dati cer
//{{{ 
		if(!$asCSV) {
			# AMGA vuole che il cer e i movimenti siano sulla stessa pagina.. quindi controllo che ci stiano, altrimenti addpage

			$BottomPage = 280;
			if($FEDIT->FGE_PdfBuffer->GetY() + 30 > $BottomPage){ 
				$FEDIT->FGE_PdfBuffer->AddPage(); 
				$Ypos = 10;
				}
			$Ypos += 1;
			$FEDIT->FGE_PdfBuffer->Line(17,$Ypos,180, $Ypos);
			$Ypos += 1;
			$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
			$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$CERdes,0,"L");
			$Ypos += 6; 
		} else {
			$CSVbuf .= AddCSVRow();
			$CSVbuf .= AddCSVcolumn($CERdes);			
		}
		
		//}}}
		# intestazione movimenti + Giacenza Iniziale
//{{{ 
		if(is_null($dati["giac_ini"])) {
			$giacINI = 0;	
		} else {
			$giacINI = $dati["giac_ini"];
		}
		if(!$asCSV) {
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
			$FEDIT->FGE_PdfBuffer->SetXY(5,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Movimento",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(10,4,"Tipo",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(35,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Data Mov.",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(55,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,4,"Carico",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(70,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,4,"Scarico",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(85,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(25,4,"Peso a Destino",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(110,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,4,"Tara",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,4,"Lordo",0,"C");
			//$FEDIT->FGE_PdfBuffer->SetXY(130,$Ypos);
			//$FEDIT->FGE_PdfBuffer->MultiCell(17,4,"Variazione %",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(140,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,4,"Totale",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(155,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(25,4,"Formulario",0,"C");
			if($SOGER->UserData["core_impiantiMODULO_ECO"]=="1" AND $SOGER->UserData['core_usersG3']){
				$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Costi/Ricavi �",0,"C");
				}
			$Ypos += 4;
			$FEDIT->FGE_PdfBuffer->SetXY(55,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,4,$dati["UnMis"],0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(70,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,4,$dati["UnMis"],0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(85,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(25,4,"Kg.",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(110,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,4,$dati["UnMis"],0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,4,$dati["UnMis"],0,"C");

			if($showGiacIni && $dati["FuoriUnitaLocale"]=="0"){
                            
				$Ypos += 4;
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
				$FEDIT->FGE_PdfBuffer->SetXY(5,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"-",0,"C");
				$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(10,4,"-",0,"C");
				$FEDIT->FGE_PdfBuffer->SetXY(35,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Giac.Iniz.",0,"C");
				$FEDIT->FGE_PdfBuffer->SetXY(55,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(15,4,$giacINI,0,"R");
				$FEDIT->FGE_PdfBuffer->SetXY(70,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(15,4,"-",0,"R");
				
				#destino
				$FEDIT->FGE_PdfBuffer->SetXY(85,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(25,4,"-",0,"R");

				$FEDIT->FGE_PdfBuffer->SetXY(110,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(15,4,"-",0,"R");
				$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(15,4,"-",0,"R");

				
				# var %
				//$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
				//$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"-",0,"R");
				
				$FEDIT->FGE_PdfBuffer->SetXY(140,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(15,4,$giacINI,0,"R");
				$FEDIT->FGE_PdfBuffer->SetXY(155,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(25,4,"-",0,"C");
				if($SOGER->UserData["core_impiantiMODULO_ECO"]=="1" AND $SOGER->UserData['core_usersG3']){
					$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"-",0,"C");
					}
				$TotCarico += $giacINI;
				}


			$Ypos += 4;
		} else {
			$CSVbuf .= AddCSVRow();
			$CSVbuf .= AddCSVcolumn("Movimento");
			$CSVbuf .= AddCSVcolumn("Tipo");
			$CSVbuf .= AddCSVcolumn("Data Movimento");			
			$CSVbuf .= AddCSVcolumn("Carico");			
			$CSVbuf .= AddCSVcolumn("Scarico");			
			$CSVbuf .= AddCSVcolumn("Peso a Destino");			
			$CSVbuf .= AddCSVcolumn("Tara");
			$CSVbuf .= AddCSVcolumn("Lordo");
			//$CSVbuf .= AddCSVcolumn("Variazione %");	
			$CSVbuf .= AddCSVcolumn("Totale");			
			$CSVbuf .= AddCSVcolumn("Formulario");
			if($SOGER->UserData["core_impiantiMODULO_ECO"]=="1" AND $SOGER->UserData['core_usersG3']){
				$CSVbuf .= AddCSVcolumn("Costi/Ricavi �");
				}
			$CSVbuf .= AddCSVRow();
			$CSVbuf .= AddCSVcolumn("");			
			$CSVbuf .= AddCSVcolumn("");		
			$CSVbuf .= AddCSVcolumn("");
			$CSVbuf .= AddCSVcolumn($dati["UnMis"]);
			$CSVbuf .= AddCSVcolumn($dati["UnMis"]);
			$CSVbuf .= AddCSVcolumn("Kg.");
			
			if($showGiacIni && $dati["FuoriUnitaLocale"]=="0"){
				$CSVbuf .= AddCSVRow();
				$CSVbuf .= AddCSVcolumn("");			
				$CSVbuf .= AddCSVcolumn("");
				$CSVbuf .= AddCSVcolumn("Giac.Iniz.");	
				$CSVbuf .= AddCSVcolumn($giacINI);
				$CSVbuf .= AddCSVcolumn("");			
				$CSVbuf .= AddCSVcolumn("");			
				$CSVbuf .= AddCSVcolumn("");			
				$CSVbuf .= AddCSVcolumn("");			
				$CSVbuf .= AddCSVcolumn("");
				$CSVbuf .= AddCSVcolumn($giacINI);
				$TotCarico += $giacINI;
				}

		}
		//}}}	
	}
		# dati movimenti	
//{{{ 
		if($dati["TIPO"]=="C") {
			$carico = $dati["quantita"];	
			$TotCarico += $dati["quantita"]; 
			$totale = $dati["quantita"];
		} else {
			$carico = "-";
		}
		if($dati["TIPO"]=="S") {
			$scarico = $dati["quantita"];
			$TotScarico += $dati["quantita"];
			$totale = " -" . $dati["quantita"];
		} else {
			$scarico = "-";
		}
		if(!is_null($dati["PS_DESTINO"])) {
			$pesoDestino = $dati["PS_DESTINO"];
			$TotPesoDestino += $dati["PS_DESTINO"];
		} else {
			$pesoDestino = "-";
		}

		if(!is_null($dati["PS_DESTINO"]) && $dati["TIPO"]=="S") {
			$percDes = @round($dati["PS_DESTINO"]*100/$dati["quantita"],2);
			//$percDesAbs="� " . abs(100-$percDes) . "%";
			$percDesAbs= $percDes - 100 . "%";
			$counterVar++;
			$TotVar+=abs(100-$percDes);
			}
		else{
			$percDesAbs = "-";			
			}
		
		if(!is_null($dati["tara"])) {
			$TotTara+=$dati["tara"];
			}

		if(!is_null($dati["lordo"])) {
			$TotLordo+=$dati["lordo"];
			}

		if(!is_null($dati["NFORM"])) {
			$formulario = $dati["NFORM"]; 	
		} else {
			$formulario = " - ";
		}

		## ECO: CALCOLO COSTO MOVIMENTO
		if($SOGER->UserData["core_impiantiMODULO_ECO"]=="1" AND $SOGER->UserData['core_usersG3']){
			$SQL="SELECT SUM(euro) AS costo FROM user_movimenti_costi WHERE ID_MOV_F='".$dati["ID_MOV_F"]."' ";
			
			switch($_POST['Stat']){
				case "AnPRO":
					$SQL.="AND ID_CNT_COND_P IS NOT NULL ";
					break;
				case "AnTRA":
					$SQL.="AND ID_CNT_COND_T IS NOT NULL ";
					break;
				case "AnDES":
					$SQL.="AND ID_CNT_COND_D IS NOT NULL ";
					break;
				case "AnINT":
					$SQL.="AND ID_CNT_COND_I IS NOT NULL ";
					break;
				}
			$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
			if(isset($FEDIT->DbRecordSet))
				$costo=number_format($FEDIT->DbRecordSet[0]['costo'], 2, ",", "");
			else
				$costo="0,00";
			$TotCosto+=$FEDIT->DbRecordSet[0]['costo'];
			}
		## END ECO

		if(!$asCSV) {
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
				$FEDIT->FGE_PdfBuffer->SetXY(5,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(20,4,$dati["NMOV"],0,"C");
				$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(10,4,$dati["TIPO"],0,"C");
				$FEDIT->FGE_PdfBuffer->SetXY(35,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(20,4,date("d/m/Y",strtotime($dati["DTMOV"])),0,"C");
				$FEDIT->FGE_PdfBuffer->SetXY(55,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($carico),0,"R");
				$FEDIT->FGE_PdfBuffer->SetXY(70,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($scarico),0,"R");
				$FEDIT->FGE_PdfBuffer->SetXY(85,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(25,4,$pesoDestino,0,"R");
				$FEDIT->FGE_PdfBuffer->SetXY(110,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($dati["tara"]),0,"R");
				$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($dati["lordo"]),0,"R");
				//$FEDIT->FGE_PdfBuffer->SetXY(125,$Ypos);
				//$FEDIT->FGE_PdfBuffer->MultiCell(20,4,$percDesAbs,0,"R");
				$FEDIT->FGE_PdfBuffer->SetXY(140,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(15,4,number_format_unlimited_precision($totale),0,"R");
				$FEDIT->FGE_PdfBuffer->SetXY(155,$Ypos);
				$FEDIT->FGE_PdfBuffer->MultiCell(25,4,$formulario,0,"C");
				if($SOGER->UserData["core_impiantiMODULO_ECO"]=="1" AND $SOGER->UserData['core_usersG3']){
					$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
					$FEDIT->FGE_PdfBuffer->MultiCell(20,4,$costo,0,"R");
					}
				$Ypos += 4;
		} else {
				$CSVbuf .= AddCSVRow();
				$CSVbuf .= AddCSVcolumn($dati["NMOV"]);
				$CSVbuf .= AddCSVcolumn($dati["TIPO"]);
				$CSVbuf .= AddCSVcolumn(date("d/m/Y",strtotime($dati["DTMOV"])));			
				$CSVbuf .= AddCSVcolumn(number_format_unlimited_precision($carico));			
				$CSVbuf .= AddCSVcolumn(number_format_unlimited_precision($scarico));			
				$CSVbuf .= AddCSVcolumn($pesoDestino);			
				$CSVbuf .= AddCSVcolumn(number_format_unlimited_precision($dati["tara"]));
				$CSVbuf .= AddCSVcolumn(number_format_unlimited_precision($dati["lordo"]));
				//$CSVbuf .= AddCSVcolumn($percDesAbs);
				$CSVbuf .= AddCSVcolumn(number_format_unlimited_precision($totale));			
				$CSVbuf .= AddCSVcolumn($formulario);
				if($SOGER->UserData["core_impiantiMODULO_ECO"]=="1" AND $SOGER->UserData['core_usersG3']){
					$CSVbuf .= AddCSVcolumn($costo);
					}
			}//}}}
	if($counter==($DbRecsNum-1)) {
		if($counterVar>0) $VarMedia=$TotVar/$counterVar; else $VarMedia=$counterVar;
		SommaMovimenti($Ypos,$TotCarico,$TotScarico,$TotPesoDestino,$TotTara,$TotLordo,$TotCosto,$VarMedia,$FEDIT->FGE_PdfBuffer,$asCSV,$CSVbuf);
	}		
	$counter++;
}
?>
