<?php

//print_r($FEDIT->DbRecordSet);
//print_r($_POST["Stat"]);

$aziende=array();
$AzIDbuffer=0;
$c=-1;
for($a=0;$a<count($FEDIT->DbRecordSet);$a++){
	if($FEDIT->DbRecordSet[$a]['AzID']!=$AzIDbuffer){
		$AzIDbuffer=$FEDIT->DbRecordSet[$a]['AzID'];
		$c++;
		$aziende[$c]['azienda']=$FEDIT->DbRecordSet[$a]['azienda'];
		$aziende[$c]['indirizzo']=$FEDIT->DbRecordSet[$a]['indirizzoSL']." - ".$FEDIT->DbRecordSet[$a]['CAPSL'].", ".$FEDIT->DbRecordSet[$a]['comuneSL']." (".$FEDIT->DbRecordSet[$a]['provSL'].")";
		$aziende[$c]['autorizzazione'][0]=$FEDIT->DbRecordSet[$a]['num_aut'];
		$aziende[$c]['ID_AUTH'][0]=$FEDIT->DbRecordSet[$a]['ID_AUTH'];
		$aziende[$c]['origine_dati'][0]=$FEDIT->DbRecordSet[$a]['origine_dati'];
		$aziende[$c]['IdOD'][0]=$FEDIT->DbRecordSet[$a]['ID_ORIGINE_DATI'];
		$aziende[$c]['rifiuto'][0]=$FEDIT->DbRecordSet[$a]['COD_CER']." - ".$FEDIT->DbRecordSet[$a]['descrizione'];
		}
	else{
		$aziende[$c]['autorizzazione'][count($aziende[$c]['autorizzazione'])]=$FEDIT->DbRecordSet[$a]['num_aut'];
		$aziende[$c]['ID_AUTH'][count($aziende[$c]['ID_AUTH'])]=$FEDIT->DbRecordSet[$a]['ID_AUTH'];
		$aziende[$c]['origine_dati'][count($aziende[$c]['origine_dati'])]=$FEDIT->DbRecordSet[$a]['origine_dati'];
		$aziende[$c]['IdOD'][count($aziende[$c]['IdOD'])]=$FEDIT->DbRecordSet[$a]['ID_ORIGINE_DATI'];
		$aziende[$c]['rifiuto'][count($aziende[$c]['rifiuto'])]=$FEDIT->DbRecordSet[$a]['COD_CER']." - ".$FEDIT->DbRecordSet[$a]['descrizione'];
		}
	}

//print_r($aziende);

$Xpos = 20;
$Ypos = $FEDIT->FGE_PdfBuffer->GetY();
$Ypos+= 10;


for($a=0;$a<count($aziende);$a++){
	$Xpos = 20;
	$Ypos+= 10;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	$FEDIT->FGE_PdfBuffer->MultiCell(265,4,$aziende[$a]['azienda'],"LRT","C");
	$Ypos+=3;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',6);
	$FEDIT->FGE_PdfBuffer->MultiCell(265,4,$aziende[$a]['indirizzo'],"LRB","C");
	$Ypos+=3;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$AuthIdODBuffer=0;
	for($auth=0;$auth<count($aziende[$a]['autorizzazione']);$auth++){
		$Xpos = 20;
		$Ypos+=5;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$AuthIdODBuffer=$aziende[$a]['IdOD'][$auth];	
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->MultiCell(135,4,"Origine autorizzazioni","","L");
		$Xpos=150;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->MultiCell(130,4,"Origine dati automezzi","","L");
		$Ypos+=8;
		$YposAutomezzi=$Ypos;
		$Xpos = 20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',7);
		$FEDIT->FGE_PdfBuffer->MultiCell(130,4,$aziende[$a]['origine_dati'][$auth].":","","L");			
		$Ypos+=4;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
		$FEDIT->FGE_PdfBuffer->MultiCell(130,4,$aziende[$a]['autorizzazione'][$auth]." per il rifiuto ".$aziende[$a]['rifiuto'][$auth],"","L");


		$sql ="SELECT user_automezzi.description AS targa, lov_origine_dati_auth.ID_ORIGINE_DATI AS IdOR, lov_origine_dati_auth.description as origine ";
		$sql.="FROM user_automezzi JOIN lov_origine_dati_auth ON user_automezzi.ID_ORIGINE_DATI=lov_origine_dati_auth.ID_ORIGINE_DATI ";
		$sql.="WHERE ID_AUTHT='".$aziende[$a]['ID_AUTH'][$auth]."' ";
		$sql.="ORDER BY IdOR asc, targa asc";
		$FEDIT->SdbRead($sql,"DbRecordSet",true,false);
		
		$colonne=8;
		$righe=count($FEDIT->DbRecordSet)/$colonne;
		$Ypos=$YposAutomezzi;
		$counter_colonne=1;
		$IdOrBuf=0;
		if(count($FEDIT->DbRecordSet)==0){
			$Xpos=150;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);				
			$FEDIT->FGE_PdfBuffer->MultiCell(130,4,"Nessun automezzo associato al rifiuto","","L");
			}
		for($t=0;$t<count($FEDIT->DbRecordSet);$t++){
			if($IdOrBuf!=$FEDIT->DbRecordSet[$t]['IdOR']){
				$Xpos=150;
				if($FEDIT->DbRecordSet[$t]['IdOR']>1){
					$Ypos+=5;
					CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
					}
				$IdOrBuf=$FEDIT->DbRecordSet[$t]['IdOR'];
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',7);				
				$FEDIT->FGE_PdfBuffer->MultiCell(130,4,$FEDIT->DbRecordSet[$t]['origine'].":","","L");
				$Ypos+=4;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
				$FEDIT->FGE_PdfBuffer->MultiCell(30,4,$FEDIT->DbRecordSet[$t]['targa'],"","L");
				}
			else{
				if($counter_colonne<$colonne){
					$Xpos+=15;
					$counter_colonne++;
					}
				else{
					$Xpos=150;
					$Ypos+=3;
					CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
					$counter_colonne=1;
				}
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
				$FEDIT->FGE_PdfBuffer->MultiCell(30,4,$FEDIT->DbRecordSet[$t]['targa'],"","L");
				}
			}
			$Ypos+=8;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

			$Xpos=150;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
			$FEDIT->FGE_PdfBuffer->MultiCell(130,4,"Origine dati rimorchi","","L");
			$Ypos+=5;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);


			$sql ="SELECT user_rimorchi.description AS targa, lov_origine_dati_auth.ID_ORIGINE_DATI AS IdOR, lov_origine_dati_auth.description as origine ";
			$sql.="FROM user_rimorchi JOIN lov_origine_dati_auth ON user_rimorchi.ID_ORIGINE_DATI=lov_origine_dati_auth.ID_ORIGINE_DATI ";
			$sql.="WHERE ID_AUTHT='".$aziende[$a]['ID_AUTH'][$auth]."' ";
			$sql.="ORDER BY IdOR asc, targa asc";
			$FEDIT->SdbRead($sql,"DbRecordSet",true,false);
			
			$colonne=8;
			$righe=ceil(count($FEDIT->DbRecordSet)/$colonne);
			$riga=1;


			$counter_colonne=1;			
			$IdOrBuf=0;

			if(count($FEDIT->DbRecordSet)==0){
				$Xpos=150;
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);				
				$FEDIT->FGE_PdfBuffer->MultiCell(130,4,"Nessun rimorchio associato al numero albo","","L");
				$Xpos=20;
				$Ypos+=4;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);				
				$FEDIT->FGE_PdfBuffer->MultiCell(265,4,"","B","L");
				$Ypos+=8;
				CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
				}
			
			for($t=0;$t<count($FEDIT->DbRecordSet);$t++){
				if($IdOrBuf!=$FEDIT->DbRecordSet[$t]['IdOR']){
					$Xpos=150;
					if($FEDIT->DbRecordSet[$t]['IdOR']>1){
						$Ypos+=3;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
						}
					$IdOrBuf=$FEDIT->DbRecordSet[$t]['IdOR'];
					$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
					$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',7);				
					$FEDIT->FGE_PdfBuffer->MultiCell(130,4,$FEDIT->DbRecordSet[$t]['origine'].":","","L");
					$Ypos+=4;
					CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
					$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
					$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
					$FEDIT->FGE_PdfBuffer->MultiCell(30,4,$FEDIT->DbRecordSet[$t]['targa'],"","L");
					}
				else{
					if($counter_colonne<$colonne){
						$Xpos+=15;
						$counter_colonne++;
						}
					else{
						$Xpos=150;
						$Ypos+=3;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
						$counter_colonne=1;
						$riga++;
						}
					$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
					$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
					$FEDIT->FGE_PdfBuffer->MultiCell(30,4,$FEDIT->DbRecordSet[$t]['targa'],"","L");
					if($riga==$righe && !isset($FEDIT->DbRecordSet[$t+1]['targa'])){
						$Ypos+=4;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
						$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
						$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
						$FEDIT->FGE_PdfBuffer->MultiCell(265,4,"","B","L");
						$Ypos+=4;
						CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
						}
					}
				}
		}
	}

?>