<?php

/*
error_reporting(E_ALL);
ini_set("display_errors", 1);
*/


session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/fpdf.php");
require_once("STATS_funct.php");

if($SOGER->UserData["core_usersG5"]=="0") {
	$SOGER->SetFeedback("L'utente non ha i permessi necessari per la stampa delle etichette","1");
	require_once("../__includes/COMMON_sleepSoger.php");
	header("location: ../");
}


# aggiorno data ultimo print etichetta
$sql="UPDATE user_schede_rifiuti SET et_last_print='".date('Y-m-d')."' WHERE ID_RIF='".$_GET["filter"]."';";
$FEDIT->SDBWrite($sql,true,false);
#

if($_GET['colori']==0) $bw="bw_"; else $bw="";

//$clH = array( 
//	"H1"=>array("P"=>5,"img"=>$bw."esplosivo.png","DES"=>"Esplosivo","EXCL"=>array(7,6)),
//	"H2"=>array("P"=>6,"img"=>$bw."comburente.png","DES"=>"Comburente","EXCL"=>array(7)),
//	"H3A"=>array("P"=>7,"img"=>$bw."reallyinfiammabile.png","DES"=>"Facilmente infiammabile","EXCL"=>null),
//	"H3B"=>array("P"=>99,"img"=>null,"DES"=>"Infiammabile","EXCL"=>null),
//	"H4"=>array("P"=>4,"img"=>$bw."nocivo.png","DES"=>"Irritante","EXCL"=>null),
//	"H5"=>array("P"=>4,"img"=>$bw."nocivo.png","DES"=>"Nocivo","EXCL"=>null),
//	"H6"=>array("P"=>2,"img"=>$bw."tossico.png","DES"=>"Tossico","EXCL"=>array(4,3)),
//	"H7"=>array("P"=>2,"img"=>$bw."tossico.png","DES"=>"Cancerogeno","EXCL"=>array(4,3)),
//	"H8"=>array("P"=>3,"img"=>$bw."corrosivo.png","DES"=>"Corrosivo","EXCL"=>array(4)),
//	"H9"=>array("P"=>99,"img"=>$bw."infettivo.png","DES"=>"Infettivo","EXCL"=>null),
//	"H10"=>array("P"=>2,"img"=>$bw."tossico.png","DES"=>"Tossico per il ciclo riproduttivo","EXCL"=>array(4,3)),
//	"H11"=>array("P"=>2,"img"=>$bw."tossico.png","DES"=>"Mutageno","EXCL"=>array(4,3)),
//	"H12"=>array("P"=>99,"img"=>null,"DES"=>"Sostanze sprigionanti gas","EXCL"=>null),
//	"H13"=>array("P"=>99,"img"=>null,"DES"=>"Sensibilizzante","EXCL"=>null),
//	"H14"=>array("P"=>1,"img"=>$bw."ambiente.png","DES"=>"Ecotossico","EXCL"=>null),
//	"H15"=>array("P"=>99,"img"=>null,"DES"=>"Sostanze che danno origine ad altre sostanze","EXCL"=>null),
//);


//print_r($_GET['colori']);

#
$sql  = "SELECT user_schede_rifiuti.pericoloso,adr,descrizione,COD_CER,user_schede_rifiuti.ID_SF,MaxStock as contenitore,";
$sql .= " et_comp_per,lov_num_onu.description as onu, lov_misure.description AS UM,";
$sql .= " HP1,HP2,HP3,HP4,HP5,HP6,HP7,HP8,HP9,HP10,HP11,HP12,HP13,HP14,HP15";
$sql .= " FROM user_schede_rifiuti JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER"; 
$sql .= " LEFT JOIN lov_num_onu on user_schede_rifiuti.ID_ONU = lov_num_onu.ID_ONU";
$sql .= " LEFT JOIN user_schede_rifiuti_deposito on user_schede_rifiuti.ID_RIF = user_schede_rifiuti_deposito.ID_RIF";
$sql .= " JOIN lov_misure ON lov_misure.ID_UMIS = user_schede_rifiuti.ID_UMIS ";
$sql .= " WHERE user_schede_rifiuti.ID_RIF='" . $_GET["filter"] . "'";

$FEDIT->SdbRead($sql,"DbRecordSet",true,false);
#
$DcName = date("d/m/Y") . "--CER_" . $FEDIT->DbRecordSet[0]["COD_CER"] . "_Etichetta";
$PER = $FEDIT->DbRecordSet[0]["pericoloso"];
//$UM = $FEDIT->DbRecordSet[0]["UM"];
$UM = "kg"; // la capacit� massima del contenitore � espressa in kg....
$SF = $FEDIT->DbRecordSet[0]["ID_SF"];
$DES = $FEDIT->DbRecordSet[0]["descrizione"]; 
$ADR = $FEDIT->DbRecordSet[0]["adr"];
$ONU = $FEDIT->DbRecordSet[0]["onu"];
if(!is_null($FEDIT->DbRecordSet[0]["contenitore"])) {
	$CONT = $FEDIT->DbRecordSet[0]["contenitore"];
} else {
	$CONT = " -- ";
}

#contenuto pericoloso
if(!is_null($FEDIT->DbRecordSet[0]["et_comp_per"])) {
	$CONT_PER = $FEDIT->DbRecordSet[0]["et_comp_per"];
} else {
	$CONT_PER = " -- ";
}

if($SOGER->UserData["core_usersCER_AXT"]=="1" && $PER=="1")
	$axt="*";
else
	$axt="";


$CER = $FEDIT->DbRecordSet[0]["COD_CER"];
$st = substr($CER,0,2);
$nd = substr($CER,2,2);
$rd = substr($CER,4,2);
$CER = $st . " " . $nd . " " . $rd . " " . $axt;

$directory = "etichette/GHS/";
$HPimg = array();
$sql = "select * from user_schede_rifiuti_pittogrammi where ID_IMP='".$SOGER->UserData['core_impiantiID_IMP']."' and ID_RIF='".$_GET["filter"]."';";
$FEDIT->SdbRead($sql,"DbRecordSetPittogrammi",true,false);
	
if(is_array($FEDIT->DbRecordSetPittogrammi)){
	foreach($FEDIT->DbRecordSetPittogrammi[0] as $key=>$value) {
		if( (substr($key,0,3)=="GHS" || $key=='M') && $value=="1") {
			$HPimg[] = $key.'.png';
		}
	}
}
else{
	if($FEDIT->DbRecordSet[0]["HP1"]=="1")
		$HPimg[] = 'GHS01.png';

	if($FEDIT->DbRecordSet[0]["HP2"]=="1")
		$HPimg[] = 'GHS03.png';

	if($FEDIT->DbRecordSet[0]["HP3"]=="1")
		$HPimg[] = 'GHS02.png';

	if($FEDIT->DbRecordSet[0]["HP9"]=="1")
		$HPimg[] = 'M.png';

	if($FEDIT->DbRecordSet[0]["HP14"]=="1")
		$HPimg[] = 'GHS09.png';
}

$orientation="L";
$um="mm";
$Format = array(210,297);
$ZeroMargin = true;
#
$Ystart=25;
$Xstart=15;
#
$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin,$DcName);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',14);
$FEDIT->FGE_PdfBuffer->SetXY($Xstart,$Ystart);
$FEDIT->FGE_PdfBuffer->MultiCell(270,112,"",1);						# BORDO ESTERNO
#
$FEDIT->FGE_PdfBuffer->SetXY($Xstart,$Ystart);
if($PER=="1" && $_GET['colori']==1 ) {											# "R"
	$FEDIT->FGE_PdfBuffer->SetFillColor(255,255,0);
} else {
	$FEDIT->FGE_PdfBuffer->SetFillColor(255,255,255);	
}
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',120);
$FEDIT->FGE_PdfBuffer->MultiCell(50,112,"R",1,"C",1);	
#
$FEDIT->FGE_PdfBuffer->SetXY($Xstart+50,$Ystart);				# TESTO "CODICE CER"
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',20);
$FEDIT->FGE_PdfBuffer->MultiCell(55,13,"Codice C.E.R.",1,"C",1);
#
$FEDIT->FGE_PdfBuffer->SetXY($Xstart+50+55,$Ystart);
if($PER=="1") {											# "RIFIUTO PERICOLOSO / NON PERICOLOSO"
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',16);
	$FEDIT->FGE_PdfBuffer->SetXY($Xstart+50+55,$Ystart);
	$FEDIT->FGE_PdfBuffer->MultiCell(165,8,"RIFIUTO PERICOLOSO",1,"C",1);
	$FEDIT->FGE_PdfBuffer->SetXY($Xstart+50+55,$Ystart+8);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,5,"Codice Pericolo",1,"C",1);
	
	# CLASSI HP
	$Hyshift = 5;
	$x=0;

	if($SOGER->UserData['core_usersPRNT_HP4_HP8']==0){
		if($FEDIT->DbRecordSet[0]['HP4'] == 1 && $FEDIT->DbRecordSet[0]['HP8'] == 1){
			$FEDIT->DbRecordSet[0]['HP4'] = 0;
		}
	}

	foreach($FEDIT->DbRecordSet[0] as $key=>$value) {
		if($x>4) { # numero max di classi H (-1)
			break;	
		}
		if($key[0]=="H" && $key[1]=="P" && $value=="1") {
			$SQL = "SELECT DESCR FROM lov_classi_hp WHERE CL_HP='".$key."';";
			$FEDIT->SdbRead($SQL,"DbRecordSetHPdesc",true,false);
			$DESCR_EXTENDED = $FEDIT->DbRecordSetHPdesc[0]['DESCR'];
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',14);
			$FEDIT->FGE_PdfBuffer->SetXY($Xstart+50+55,$Ystart+8+$Hyshift);
			$FEDIT->FGE_PdfBuffer->MultiCell(30,7.3,$key,0,"C");
			if($key<>'HP5' && $key<>'HP15')
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',12);
			else
				$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
			$FEDIT->FGE_PdfBuffer->SetXY($Xstart+50+55+30,$Ystart+8+$Hyshift);
			$FEDIT->FGE_PdfBuffer->MultiCell(135,7.3,$DESCR_EXTENDED,0,"C");	
			$Hyshift+=7.3;
			$x++;
		}
	}
	
	$FEDIT->FGE_PdfBuffer->SetXY($Xstart+50+55+30,$Ystart+8);
	$FEDIT->FGE_PdfBuffer->MultiCell(135,5,"Caratteristiche di Pericolo",1,"C",1);
} 
else {
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',22);
	$FEDIT->FGE_PdfBuffer->MultiCell(165,13,"RIFIUTO NON PERICOLOSO",1,"C",1);	
}

for($x=0;$x<3;$x++) {								# IMMAGINI
	if($x>0) {
		$XRef = $Xstart+50+($x*33.3);
		$FEDIT->FGE_PdfBuffer->SetXY($XRef,$Ystart+13+38+15+7+7);
		}
	else {
		$XRef = $Xstart+50;
		$FEDIT->FGE_PdfBuffer->SetXY($XRef,$Ystart+13+38+15+7+7);
		}
	$FEDIT->FGE_PdfBuffer->MultiCell(33.3,32,"",1,"C",0);
	
	if($PER=="1" && !empty($HPimg) && isset($HPimg[$x])){
		$img = $bw.$HPimg[$x];
		$FEDIT->FGE_PdfBuffer->Image($directory.$img,$XRef+4.1,$Ystart+13+38+15+7+7+3.6,25,25);
	}
} 




#
$FEDIT->FGE_PdfBuffer->SetXY($Xstart+50,$Ystart+13);				# CODICE CER
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',30);
$FEDIT->FGE_PdfBuffer->MultiCell(55,38,$CER,1,"C",0);
#
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',16);
$FEDIT->FGE_PdfBuffer->SetXY($Xstart+50,$Ystart+13+38);				# DESCRIZIONE RIFIUTO
$FEDIT->FGE_PdfBuffer->MultiCell(220,15,"",1,"",0);
$FEDIT->FGE_PdfBuffer->SetXY($Xstart+50+1,$Ystart+13+42);
$FEDIT->FGE_PdfBuffer->MultiCell(218,6,$DES,0,"L",0);	

#
$FEDIT->FGE_PdfBuffer->SetXY($Xstart+50,$Ystart+13+38+15);			# "STATO FISICO"
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(55,7,"Stato fisico:",1,"C",0);
#


switch($SF){
	case 1:
		$statoFis = "Solido pulverulento";
		break;
	case 2:
		$statoFis = "Solido non pulverulento";
		break;
	case 3:
		$statoFis = "Fangoso palabile";
		break;
	case 4:
		$statoFis = "Liquido";
		break;
	}
$FEDIT->FGE_PdfBuffer->SetXY($Xstart+50+55,$Ystart+13+38+15);
$FEDIT->FGE_PdfBuffer->MultiCell(55,7,$statoFis,1,"C",0);	


#
$FEDIT->FGE_PdfBuffer->SetXY($Xstart+50+55+55,$Ystart+13+38+15);		# "CAPACITA' COLLO"
$FEDIT->FGE_PdfBuffer->MultiCell(55,7,"Capacit� collo:",1,"C",0);

$FEDIT->FGE_PdfBuffer->SetXY($Xstart+50+55+55+55,$Ystart+13+38+15);
if($CONT>0)
	$FEDIT->FGE_PdfBuffer->MultiCell(55,7,$CONT." ".$UM,1,"C",0);
else
	$FEDIT->FGE_PdfBuffer->MultiCell(55,7,"N.D.",1,"C",0);
#

$FEDIT->FGE_PdfBuffer->SetXY($Xstart+50,$Ystart+13+38+15+7);			# "CONTIENE"
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
$FEDIT->FGE_PdfBuffer->MultiCell(55,7,"Contiene:",1,"C",0);
$FEDIT->FGE_PdfBuffer->SetXY($Xstart+50+55,$Ystart+13+38+15+7);
$FEDIT->FGE_PdfBuffer->MultiCell(165,7,$CONT_PER,1,"C",0);

																		# GRIGLIE ADR SI / NO
$FEDIT->FGE_PdfBuffer->SetFillColor(255,255,255);
$FEDIT->FGE_PdfBuffer->SetXY($Xstart+50+100,$Ystart+13+38+15+7+7);
$FEDIT->FGE_PdfBuffer->MultiCell(120,7,"ADR",1,"C",0);

$FEDIT->FGE_PdfBuffer->SetXY($Xstart+50+100,$Ystart+13+38+15+7+7+7);		
if($ADR=="1") {
	if($FEDIT->DbRecordSet[0]["COD_CER"]!='150110' | $ONU=='3509')
		$stringa = "UN ".$ONU;
	else
		$stringa = "SI'";
} else {
	$stringa = "NO";
}
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',65);
$FEDIT->FGE_PdfBuffer->MultiCell(120,25,$stringa,1,"C",0);
























## SEGNALETICA SICUREZZA




# controllare se c'� record x segnaletica di sicurezza, altrimenti faccio insert
$sql = "SELECT * FROM user_schede_rifiuti_etsym WHERE ID_RIF='".$_GET["filter"]."';";
$FEDIT->SdbRead($sql,"DbRecordSetSym",true,false);

# NESSUN RECORD, CALCOLO SEGNALETICA PARTENDO DA CLASSI H
if(!isset($FEDIT->DbRecordSetSym[0])){
	//$sql = "SELECT ID_SF, pericoloso, H1, H2, H3A, H3B, H4, H5, H6, H7, H8, H9, H10, H11, H12, H13, H14, H15 FROM user_schede_rifiuti WHERE ID_RIF='".$_GET['filter']."';";
        $sql = "SELECT ID_SF, pericoloso, HP1, HP2, HP3, HP4, HP5, HP6, HP7, HP8, HP9, HP10, HP11, HP12, HP13, HP14, HP15 FROM user_schede_rifiuti WHERE ID_RIF='".$_GET['filter']."';";
	$FEDIT->SdbRead($sql,"DbRecordSetInfoRif",true,false);

	## SEMPRE
	$segnaletica["P002"]="1";	
	$segnaletica["M009"]="1";
	$segnaletica["M008"]="1";
	
	## INDEFINITO
	$segnaletica["W003"]="0";
	$segnaletica["M014"]="0";
	$segnaletica["M001"]="0";
	$segnaletica["E013"]="0";
	
	if($FEDIT->DbRecordSetInfoRif[0]["HP3"]=="1"){
		$segnaletica["P003"]="1";
		$segnaletica["W021"]="1";
		}
	else{
		$segnaletica["P003"]="0";
		$segnaletica["W021"]="0";
		}

	if($FEDIT->DbRecordSetInfoRif[0]["HP2"]=="1"){
		$segnaletica["P011"]="1";
		$segnaletica["W028"]="1";
		}
	else{
		$segnaletica["P011"]="0";
		$segnaletica["W028"]="0";
		}

	if($FEDIT->DbRecordSetInfoRif[0]["HP1"]=="1")
		$segnaletica["W002"]="1";
	else
		$segnaletica["W002"]="0";

	if($FEDIT->DbRecordSetInfoRif[0]["HP6"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP7"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP10"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP11"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP12"]=="1")
		$segnaletica["W016"]="1";
	else
		$segnaletica["W016"]="0";

	if($FEDIT->DbRecordSetInfoRif[0]["HP8"]=="1")
		$segnaletica["W023"]="1";
	else
		$segnaletica["W023"]="0";

	if($FEDIT->DbRecordSetInfoRif[0]["ID_SF"]=="4" | $FEDIT->DbRecordSetInfoRif[0]["pericoloso"]=="1")
		$segnaletica["M004"]="1";
	else
		$segnaletica["M004"]="0";

	if($FEDIT->DbRecordSetInfoRif[0]["ID_SF"]=="2" | $FEDIT->DbRecordSetInfoRif[0]["pericoloso"]=="1")
		$segnaletica["M016"]="1";
	else
		$segnaletica["M016"]="0";

	if($FEDIT->DbRecordSetInfoRif[0]["ID_SF"]=="4" | $FEDIT->DbRecordSetInfoRif[0]["HP7"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP10"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP11"]=="1")
		$segnaletica["M010"]="1";
	else
		$segnaletica["M010"]="0";

	if($FEDIT->DbRecordSetInfoRif[0]["ID_SF"]=="4" | $FEDIT->DbRecordSetInfoRif[0]["HP8"]=="1"){
		$segnaletica["M013"]="1";
		$segnaletica["E012"]="1";
		}
	else{
		$segnaletica["M013"]="0";
		$segnaletica["E012"]="0";
		}

	if($FEDIT->DbRecordSetInfoRif[0]["pericoloso"]=="1"){
		$segnaletica["W001"]="1";
		$segnaletica["E003"]="1";
		$segnaletica["E004"]="1";
		}
	else{
		$segnaletica["W001"]="0";
		$segnaletica["E003"]="0";
		$segnaletica["E004"]="0";
		}

	if($FEDIT->DbRecordSetInfoRif[0]["HP4"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP8"]=="1")
		$segnaletica["E011"]="1";
	else
		$segnaletica["E011"]="0";

	if($segnaletica["M013"]="1") 
		$segnaletica["M004"]="0";


	}
else{
	$segnaletica["P003"]=$FEDIT->DbRecordSetSym[0]["P003"];
	$segnaletica["P011"]=$FEDIT->DbRecordSetSym[0]["P011"];
	$segnaletica["P002"]=$FEDIT->DbRecordSetSym[0]["P002"];
	$segnaletica["M004"]=$FEDIT->DbRecordSetSym[0]["M004"];
	$segnaletica["M016"]=$FEDIT->DbRecordSetSym[0]["M016"];
	$segnaletica["M013"]=$FEDIT->DbRecordSetSym[0]["M013"];
	$segnaletica["M009"]=$FEDIT->DbRecordSetSym[0]["M009"];
	$segnaletica["M008"]=$FEDIT->DbRecordSetSym[0]["M008"];
	$segnaletica["M001"]=$FEDIT->DbRecordSetSym[0]["M001"];
	$segnaletica["M014"]=$FEDIT->DbRecordSetSym[0]["M014"];
	$segnaletica["M010"]=$FEDIT->DbRecordSetSym[0]["M010"];
	$segnaletica["M015"]=$FEDIT->DbRecordSetSym[0]["M015"];
	$segnaletica["M003"]=$FEDIT->DbRecordSetSym[0]["M003"];
	$segnaletica["W003"]=$FEDIT->DbRecordSetSym[0]["W003"];
	$segnaletica["W016"]=$FEDIT->DbRecordSetSym[0]["W016"];
	$segnaletica["W021"]=$FEDIT->DbRecordSetSym[0]["W021"];
	$segnaletica["W023"]=$FEDIT->DbRecordSetSym[0]["W023"];
	$segnaletica["W028"]=$FEDIT->DbRecordSetSym[0]["W028"];
	$segnaletica["W002"]=$FEDIT->DbRecordSetSym[0]["W002"];
	$segnaletica["W001"]=$FEDIT->DbRecordSetSym[0]["W001"];
	$segnaletica["W022"]=$FEDIT->DbRecordSetSym[0]["W022"];
	$segnaletica["E003"]=$FEDIT->DbRecordSetSym[0]["E003"];
	$segnaletica["E012"]=$FEDIT->DbRecordSetSym[0]["E012"];
	$segnaletica["E011"]=$FEDIT->DbRecordSetSym[0]["E011"];
	$segnaletica["E013"]=$FEDIT->DbRecordSetSym[0]["E013"];
	$segnaletica["E004"]=$FEDIT->DbRecordSetSym[0]["E004"];
	}

if($segnaletica["P003"]=="1" | $segnaletica["P011"]=="1" | $segnaletica["P002"]=="1") $divieto=true; else $divieto=false;
if($segnaletica["M004"]=="1" | $segnaletica["M016"]=="1" | $segnaletica["M013"]=="1" | $segnaletica["M009"]=="1" | $segnaletica["M008"]=="1" | $segnaletica["M001"]=="1" | $segnaletica["M014"]=="1" | $segnaletica["M010"]=="1" | $segnaletica["M015"]=="1" | $segnaletica["M003"]=="1") $prescrizione=true; else $prescrizione=false;
if($segnaletica["W003"]=="1" | $segnaletica["W016"]=="1" | $segnaletica["W021"]=="1" | $segnaletica["W023"]=="1" | $segnaletica["W028"]=="1" | $segnaletica["W002"]=="1" | $segnaletica["W001"]=="1" | $segnaletica["W022"]=="1") $avvertimento=true; else $avvertimento=false;
if($segnaletica["E003"]=="1" | $segnaletica["E012"]=="1" | $segnaletica["E011"]=="1" | $segnaletica["E013"]=="1" | $segnaletica["E004"]=="1") $salvataggio=true; else $salvataggio=false;

	$Ypos=137;
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
	$FEDIT->FGE_PdfBuffer->SetXY($Xstart,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(101.25,7.3,"Segnali di divieto:","TLBR","L");
	$Ypos+=7.3;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xstart,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(101.25,40,"",1,"C",0);
	$Xpos=$Xstart-32;
	$Ypos+=5;
	
	$divieti=0;
	$directory="etichette/sicurezza/divieto/";
	if($_GET['colori']==0) $directory.="bw/";
	
	if($segnaletica["P003"]=="1" && $divieti<3){
		$Xpos+=33;
		$FEDIT->FGE_PdfBuffer->Image($directory."P003.jpg",$Xpos,$Ypos,30,30);
		$divieti++;
		}
	if($segnaletica["P011"]=="1" && $divieti<3){
		$Xpos+=33;
		$FEDIT->FGE_PdfBuffer->Image($directory."P011.jpg",$Xpos,$Ypos,30,30);
		$divieti++;
		}
	if($segnaletica["P002"]=="1" && $divieti<3){
		$Xpos+=33;
		$FEDIT->FGE_PdfBuffer->Image($directory."P002.jpg",$Xpos,$Ypos,30,30);
		$divieti++;
		}
	
	$Ypos-=12.3;
	$Xpos=101.25+$Xstart;

	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(168.75,7.3,"Segnali di obbligo:","TLBR","L");
	$Ypos+=7.3;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(168.75,40,"",1,"C",0);

	$Xpos=$Xpos-32;
	$Ypos+=5;

	
	$obbligo=0;
	$directory="etichette/sicurezza/obbligo/";
	if($_GET['colori']==0) $directory.="bw/";

	if($segnaletica["M004"]=="1" && $obbligo<5){
		$Xpos+=33;
		$FEDIT->FGE_PdfBuffer->Image($directory."M004.jpg",$Xpos,$Ypos,30,30);
		$obbligo++;
		}
	if($segnaletica["M016"]=="1" && $obbligo<5){
		$Xpos+=33;
		$FEDIT->FGE_PdfBuffer->Image($directory."M016.jpg",$Xpos,$Ypos,30,30);
		$obbligo++;
		}
	if($segnaletica["M013"]=="1" && $obbligo<5){
		$Xpos+=33;
		$FEDIT->FGE_PdfBuffer->Image($directory."M013.jpg",$Xpos,$Ypos,30,30);
		$obbligo++;
		}
	if($segnaletica["M009"]=="1" && $obbligo<5){
		$Xpos+=33;
		$FEDIT->FGE_PdfBuffer->Image($directory."M009.jpg",$Xpos,$Ypos,30,30);
		$obbligo++;
		}
	if($segnaletica["M008"]=="1" && $obbligo<5){
		$Xpos+=33;
		$FEDIT->FGE_PdfBuffer->Image($directory."M008.jpg",$Xpos,$Ypos,30,30);
		$obbligo++;
		}
	if($segnaletica["M001"]=="1" && $obbligo<5){
		$Xpos+=33;
		$FEDIT->FGE_PdfBuffer->Image($directory."M001.jpg",$Xpos,$Ypos,30,30);
		$obbligo++;
		}
	if($segnaletica["M014"]=="1" && $obbligo<5){
		$Xpos+=33;
		$FEDIT->FGE_PdfBuffer->Image($directory."M014.jpg",$Xpos,$Ypos,30,30);
		$obbligo++;
		}
	if($segnaletica["M010"]=="1" && $obbligo<5){
		$Xpos+=33;
		$FEDIT->FGE_PdfBuffer->Image($directory."M010.jpg",$Xpos,$Ypos,30,30);
		$obbligo++;
		}
	if($segnaletica["M015"]=="1" && $obbligo<5){
		$Xpos+=33;
		$FEDIT->FGE_PdfBuffer->Image($directory."M015.jpg",$Xpos,$Ypos,30,30);
		$obbligo++;
		}	
	if($segnaletica["M003"]=="1" && $obbligo<5){
		$Xpos+=33;
		$FEDIT->FGE_PdfBuffer->Image($directory."M003.jpg",$Xpos,$Ypos,30,30);
		$obbligo++;
		}
	

#
#
#	PDF output
#
PDFOut($FEDIT->FGE_PdfBuffer,$DcName);

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>
