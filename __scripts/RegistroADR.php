<?php
session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/fpdf.php");
include("../__libs/SQLFunct.php");
require_once("STATS_funct.php");

$orientation= "P";
$statTitle	= "REGISTRO ADR";
$DcName		= date("d/m/Y") . "--Registro_ADR";
$PesoAnno	= 0;
$FirAnno	= 0;

$AnnoDB		= substr($_SESSION["DbInUse"], -4);

$ID_USR		= $SOGER->UserData['core_usersID_USR'];
$ID_IMP		= $SOGER->UserData['core_usersID_IMP'];
$WORKMODE	= $SOGER->UserData['workmode'];
$Month		= array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$Mese		= array("Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre");


	$um="mm";
	$Format = array(210,297);
	$ZeroMargin = true;
	$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin,$DcName);

	$Ypos		= 30;
	$Xpos		= 10;

	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',12);

	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(190,12,"REGISTRO ADR ".$AnnoDB,"TLBR","C");
	$Ypos+=7;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	for($m=0;$m<count($Month);$m++){
		$sql ="SELECT DTFORM, NFORM, lov_num_onu.description AS ONU, SUBSTRING(des, 1, 100) as des, imb, pesoN FROM user_movimenti_fiscalizzati ";
		$sql.="JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF = user_schede_rifiuti.ID_RIF ";
		$sql.="JOIN lov_cer ON lov_cer.ID_CER = user_schede_rifiuti.ID_CER ";
		$sql.="JOIN lov_num_onu ON lov_num_onu.ID_ONU = user_movimenti_fiscalizzati.ID_ONU ";
		$sql.="WHERE user_movimenti_fiscalizzati.ID_IMP = '".$ID_IMP."' ";
		$sql.="AND user_movimenti_fiscalizzati.adr = 1 ";
		$sql.="AND user_movimenti_fiscalizzati.QL = 0 ";
		$sql.="AND user_movimenti_fiscalizzati.".$WORKMODE." = 1 ";
		$sql.="AND MONTHNAME(DTFORM) = '".$Month[$m]."' AND NMOV<>9999999 ";
		$sql.="ORDER BY DTFORM;";
		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);


		$PesoDelMese	= 0;
		$FirDelMese		= 0;
			
		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
		$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
		$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,7,$Mese[$m],"TRBL","L",1);
		
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',6);
		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

		// intestazione colonne
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,7,"Data","TBRL","C");
		$Xpos+=20;
		
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,7,"Formulario","TBRL","C");
		$Xpos+=20;
		
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(130,7,"N. ONU","TBRL","L");
		$Xpos+=130;
		
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,7,"Peso netto","TBRL","C");
		$Xpos=10;


		for($r=0;$r<count($FEDIT->DbRecordSet);$r++){
			$Ypos+=7;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			
			
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$DTFORM_array = explode("-",$FEDIT->DbRecordSet[$r]['DTFORM']);
			$DTFORM=$DTFORM_array[2]."/".$DTFORM_array[1]."/".$DTFORM_array[0];
			$FEDIT->FGE_PdfBuffer->MultiCell(20,7,$DTFORM,"TBRL","C");
			$Xpos+=20;
			
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,7,$FEDIT->DbRecordSet[$r]['NFORM'],"TBRL","C");
			$Xpos+=20;
			
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			//$FEDIT->FGE_PdfBuffer->MultiCell(120,7,$FEDIT->DbRecordSet[$r]['COD_CER'] . " - " . $FEDIT->DbRecordSet[$r]['descrizione'],"TBRL","L");
			$FEDIT->FGE_PdfBuffer->MultiCell(130,7,$FEDIT->DbRecordSet[$r]['ONU'] . " - " . $FEDIT->DbRecordSet[$r]['des'] . "..., " . $FEDIT->DbRecordSet[$r]['imb'],"TBRL","L");
			$Xpos+=130;
			
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,7,$FEDIT->DbRecordSet[$r]['pesoN']." kg","TBRL","R");
			$Xpos=10;

			$PesoDelMese+=$FEDIT->DbRecordSet[$r]['pesoN'];
			$FirDelMese++;

			}
		
		// totale mese
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

		$Xpos=10;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(95,7,"TOTALE","TBRL","C");
		
		$Xpos+=95;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(47.5,7,$FirDelMese." formulari","TBRL","C");
		
		$Xpos+=47.5;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(47.5,7,$PesoDelMese." kg","TBRL","C");
			
		$PesoAnno+=$PesoDelMese;
		$FirAnno+=$FirDelMese;
		
		$Xpos=10;
		}

	// totale anno

	$Ypos+=20;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);
	$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
	$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(190,7,"ANNO ".$AnnoDB,"TRBL","L",1);
	$Ypos+=7;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',6);
	$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);

	$Xpos=10;
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(95,7,"TOTALE","TBRL","C");

	$Xpos+=95;
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(47.5,7,$FirAnno." formulari","TBRL","C");

	$Xpos+=47.5;
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(47.5,7,$PesoAnno." kg","TBRL","C");

	$Xpos=10;


	PDFOut($FEDIT->FGE_PdfBuffer,$DcName);

?>