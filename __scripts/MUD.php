<?php
error_reporting(E_ALL & ~E_NOTICE);
ini_set("display_errors", 1);

session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/zip3.php");
require_once("STATS_funct.php");

require_once("MUD_funct.php");

if($SOGER->UserData["core_usersD7"]=="0") {
    $SOGER->SetFeedback("L'utente non ha i permessi necessari per l'estrazione dei dati utili al MUD","1");
    require_once("../__includes/COMMON_sleepSoger.php");
    header("location: ../");
}

$returnDb = $_SESSION["DbInUse"];
$MUD2021 = createMudFile();

### SALVA ZIP zip3.php ###
$tmpMUDfile	= "../__updates/MUD2021.000";
$tmpMUD		= fopen($tmpMUDfile, "w+");
fwrite($tmpMUD, $MUD2021);
fclose($tmpMUD);

$settings = array (
    "path" => "../__updates"
);

$fileName="SoGeR_Pro_-_ExportMUD";

$zipFile = new zipFile($settings);

$zipFile->files = array (
    "../__updates/MUD2021.000"
);

list($success, $error) = $zipFile->create($fileName);
$archiveName="../__updates/".$fileName.".zip";

unlink($tmpMUDfile);

# force download
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
header("Content-Type: application/zip");
header("Content-Disposition: attachment; filename=".basename($archiveName).";" );
header("Content-Transfer-Encoding: binary");
header("Content-Length: ".filesize($archiveName));
readfile("$archiveName");

unlink($archiveName);

$FEDIT->DbServerData["db"]=$returnDb;

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>