<?php
	//include("../__libs/SQLFunct.php");
	
	//print_r($FEDIT->DbRecordSet);	

	$Ypos+=20;

	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	
	$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,10,"Cer","BTLR","C");
	
	$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(120,10,"Descrizione","BTLR","C");

	$FEDIT->FGE_PdfBuffer->SetXY(145,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,5,"Previsione prossimo conferimento","BTLR","C");

	$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,5,"Giorni al prossimo conferimento","BTLR","C");


	$Ypos +=15;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);

	foreach($FEDIT->DbRecordSet as $k=>$dati) {	
		$data=explode("-",$dati["PREV_NextS"]);

		if($data!="00/00/0000"){
			$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,3,$dati["COD_CER"],0,"C");

			$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(120,3,$dati["descrizione"],0,"L");

			$FEDIT->FGE_PdfBuffer->SetXY(145,$Ypos);		
			$FEDIT->FGE_PdfBuffer->MultiCell(30,5,$data[2]."/".$data[1]."/".$data[0],0,"C");

			$FEDIT->FGE_PdfBuffer->SetXY(175,$Ypos);
			$Oggi =mktime(0, 0, 0, date('m'), date('d'), date('Y'));
			$NextS=mktime(0, 0, 0, $data[1], $data[2], $data[0]);
			$Giorni=($NextS - $Oggi)/(60*60*24);
			$FEDIT->FGE_PdfBuffer->MultiCell(30,5,$Giorni,0,"C");

			$Ypos+=10;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			}

		}

?>