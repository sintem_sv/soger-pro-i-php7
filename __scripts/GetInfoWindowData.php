<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");

$Resource = explode(":", $_POST['ref']);
$Table = $Resource[0];
$Index = $Resource[1];

switch($Table){
	case "user_aziende_produttori":
		$Primary = "ID_AZP";
		break;
	case "user_aziende_trasportatori":
		$Primary = "ID_AZT";
		break;
	case "user_aziende_destinatari":
		$Primary = "ID_AZD";
		break;
	case "user_aziende_intermediari":
		$Primary = "ID_AZI";
		break;
	case "user_impianti_produttori":
		$Primary = "ID_UIMP";
		$Parent = "user_aziende_produttori";
		$ParentPrimary = "ID_AZP";
		break;
	case "user_impianti_trasportatori":
		$Primary = "ID_UIMT";
		$Parent = "user_aziende_trasportatori";
		$ParentPrimary = "ID_AZT";
		break;
	case "user_impianti_destinatari":
		$Primary = "ID_UIMD";
		$Parent = "user_aziende_destinatari";
		$ParentPrimary = "ID_AZD";
		break;
	case "user_impianti_intermediari":
		$Primary = "ID_UIMI";
		$Parent = "user_aziende_intermediari";
		$ParentPrimary = "ID_AZI";
		break;
	}

if( strpos($Table, "user_aziende_")!==false){
	$SQL="SELECT ".$Table.".description as azienda, indirizzo, latitude, longitude, lov_comuni_istat.description AS comune, shdes_prov AS provincia, nazione FROM ".$Table." JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM=".$Table.".ID_COM WHERE ".$Primary."=".$Index;
	}

if( strpos($Table, "user_impianti_")!==false){
	$SQL="SELECT ".$Parent.".description as azienda, ".$Table.".description as indirizzo, ".$Table.".latitude, ".$Table.".longitude, lov_comuni_istat.description AS comune, shdes_prov AS provincia, nazione FROM ".$Table." JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM=".$Table.".ID_COM JOIN ".$Parent." ON ".$Parent.".".$ParentPrimary."=".$Table.".".$ParentPrimary." WHERE ".$Primary."=".$Index;
	}

$FEDIT->SdbRead($SQL,"InfoData");

if(is_null($FEDIT->InfoData[0]['latitude'])){
	$FEDIT->InfoData[0]['StdLatLng']	= true;
	}
else{
	$FEDIT->InfoData[0]['StdLatLng']	= false;
	}

echo json_encode($FEDIT->InfoData[0]);

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>