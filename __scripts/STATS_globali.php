<?php
foreach($FEDIT->DbRecordSet as $k=>$dati) {
	
	if($dati["AZref"]!=$tmpAZ) {
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
		# dati azienda
		$Ypos += 10;
		$tmpAZ =$dati["AZref"];
		$tmpIMP = "";
		$tmpCER = "";
//{{{ 
		$AzAddr = $dati["AZind"] . " - " . $dati["AZcom"] . " (" . $dati["AZshprov"] . ") - " . $dati["AZnaz"];
		if(!$asCSV) {		
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',10);
			$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$dati["AZdes"],0,"L");
			$Ypos += 4;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
			$FEDIT->FGE_PdfBuffer->SetXY(15,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);		    
			$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$AzAddr,0,"L");
			$Ypos += 6; 
			# intestazione dati
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
			$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,4,"CER",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(125,4,"Descrizione",0,"L");
			$FEDIT->FGE_PdfBuffer->SetXY(150,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Giacenza iniziale",0,"L");
			$FEDIT->FGE_PdfBuffer->SetXY(170,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Carico",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(190,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Scarico",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(210,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Giacenza",0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(230,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Peso a Destino",0,"C");	
			$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"Variazione %",0,"C");	
			$Ypos += 8; 
		} else {
			$CSVbuf .= AddCSVRow();			
			$CSVbuf .= AddCSVRow();			
			$CSVbuf .= AddCSVcolumn($dati["AZdes"]);
			$CSVbuf .= AddCSVcolumn($AzAddr);
			$CSVbuf .= AddCSVRow();
			$CSVbuf .= AddCSVcolumn("CER");
			if($SOGER->UserData['core_usersID_IMP']=='001MRCIM' AND $SOGER->UserData['workmode']=='destinatario')
				$CSVbuf .= AddCSVcolumn("Lotto");
			$CSVbuf .= AddCSVcolumn("Descrizione");
			$CSVbuf .= AddCSVcolumn("Giacenza iniziale");
			$CSVbuf .= AddCSVcolumn("Unit� di misura");
			$CSVbuf .= AddCSVcolumn("Carico");
			$CSVbuf .= AddCSVcolumn("Unit� di misura");
			$CSVbuf .= AddCSVcolumn("Scarico");
			$CSVbuf .= AddCSVcolumn("Unit� di misura");
			$CSVbuf .= AddCSVcolumn("Giacenza");
			$CSVbuf .= AddCSVcolumn("Unit� di misura");
			$CSVbuf .= AddCSVcolumn("Peso a Destino");
			$CSVbuf .= AddCSVcolumn("Unit� di misura");
			$CSVbuf .= AddCSVcolumn("Variazione %");
		}
//}}}
	}	
	if($tmpCER!=$dati["CERref"]) {
		$gINI = $dati["giac_ini"];
		$tmpCER = $dati["CERref"];			
		
		if(isset($_POST["MovDa"]) && $_POST["MovDa"]!="" && isset($_POST["MovA"]) && $_POST["MovA"]!="") {
			$RawDa = explode("/",$_POST["MovDa"]);
			$Da = $RawDa[2] . "-" . $RawDa[1] . "-" . $RawDa[0];
			$RawA = explode("/",$_POST["MovA"]);
			$A = $RawA[2] . "-" . $RawA[1] . "-" . $RawA[0];
			}
		else{
			$Da="";
			$A="";
			}

		if(!$asCSV) {
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer,$asCSV);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
			$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,4,$dati["COD_CER"],0,"C");
			$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(125,3,$dati["descrizione"],0,"L");
			GetTotaliGlobali($TotCarico,$TotScarico,$TotPesoDestino,$TotVar,$FEDIT,$dati["RifID"],$SOGER->UserData["core_impiantiID_IMP"],$dati["AZref"],$AzKey,$Da,$A,$MovTable, $dati["UnMis"], $dati["peso_spec"], $_POST['Stat']);
			$FEDIT->FGE_PdfBuffer->SetXY(150,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,$gINI . " " . $dati["UnMis"],0,"R");
			$FEDIT->FGE_PdfBuffer->SetXY(170,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,$TotCarico . " " . $dati["UnMis"],0,"R");
			$FEDIT->FGE_PdfBuffer->SetXY(190,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,$TotScarico . " " . $dati["UnMis"],0,"R");
			$FEDIT->FGE_PdfBuffer->SetXY(210,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,($TotCarico+$gINI-$TotScarico) . " " . $dati["UnMis"],0,"R");
			$FEDIT->FGE_PdfBuffer->SetXY(230,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,4,$TotPesoDestino . " Kg.",0,"R");	
			$FEDIT->FGE_PdfBuffer->SetXY(250,$Ypos);
			if($TotScarico>$TotPesoDestino)
				$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"- " . $TotVar . "%",0,"R");	
			if($TotScarico<$TotPesoDestino)
				$FEDIT->FGE_PdfBuffer->MultiCell(20,4,"+ " . $TotVar . "%",0,"R");	
			if($TotScarico==$TotPesoDestino)
				$FEDIT->FGE_PdfBuffer->MultiCell(20,4,$TotVar . "%",0,"R");	
			$TotVar=0;

			$Ypos += 6;
		} else {
			$CSVbuf .= AddCSVRow();
			$CSVbuf .= AddCSVcolumn($dati["COD_CER"]);
			if($SOGER->UserData['core_usersID_IMP']=='001MRCIM' AND $SOGER->UserData['workmode']=='destinatario')
				$CSVbuf .= AddCSVcolumn($dati["lotto"]);
			$CSVbuf .= AddCSVcolumn($dati["descrizione"]);
			GetTotaliGlobali($TotCarico,$TotScarico,$TotPesoDestino,$TotVar,$FEDIT,$dati["RifID"],$SOGER->UserData["core_impiantiID_IMP"],$dati["AZref"],$AzKey,$Da,$A,$MovTable,$dati["UnMis"], $dati["peso_spec"], $_POST['Stat']);
			
			//$CSVbuf .= AddCSVcolumn($gINI . " " . $dati["UnMis"]);
			$CSVbuf .= AddCSVcolumn($gINI);
			$CSVbuf .= AddCSVcolumn($dati["UnMis"]);
			//$CSVbuf .= AddCSVcolumn($TotCarico . " " . $dati["UnMis"]);
			$CSVbuf .= AddCSVcolumn($TotCarico);
			$CSVbuf .= AddCSVcolumn($dati["UnMis"]);
			//$CSVbuf .= AddCSVcolumn($TotScarico . " " . $dati["UnMis"]);
			$CSVbuf .= AddCSVcolumn($TotScarico);
			$CSVbuf .= AddCSVcolumn($dati["UnMis"]);
			//$CSVbuf .= AddCSVcolumn(($TotCarico+$gINI-$TotScarico) . " " . $dati["UnMis"]);
			$CSVbuf .= AddCSVcolumn($TotCarico+$gINI-$TotScarico);
			$CSVbuf .= AddCSVcolumn($dati["UnMis"]);
			//$CSVbuf .= AddCSVcolumn($TotPesoDestino . " Kg.");
			$CSVbuf .= AddCSVcolumn($TotPesoDestino);
			$CSVbuf .= AddCSVcolumn(" Kg.");

			$CSVbuf .= AddCSVcolumn("� " . $TotVar . "%");
			$TotVar=0;
		}
	}
}
?>