<?php

session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/fpdf.php");
require_once("STATS_funct.php");
require_once("../__libs/SQLFunct.php");


$AzTable = $_POST["AzTable"];
$AzKey = $_POST["AzKey"];
$ImpTable = $_POST["ImpTable"];
$ImpKey = $_POST["ImpKey"];
$PrintIntestazione = true;
if(isset($_POST['AuthTable'])){
	$AuthTable=$_POST['AuthTable'];
	$AuthKey=$_POST['AuthKey'];
	}
if(!isset($_POST["asCSV"])) {
	$asCSV = false;
	$CSVbuf = "";
} else {
	$asCSV = true;
}

if(isset($_POST['MovTable'])){
	$MovTable=$_POST['MovTable'];
	if($MovTable=="user_movimenti")
		$IDMov="ID_MOV";
	else
		$IDMov="ID_MOV_F";
	}
else{
	$MovTable="user_movimenti_fiscalizzati";
	$IDMov="ID_MOV_F";
	}

#
#	TITOLO STATISTICA, NOME FILE, ORIENTAMENTO PAGINA
#
switch($_POST["Stat"]) {
//{{{
	case "ECO_Contratti":
	$orientation="P";
	$statTitle = "PROSPETTO CONTRATTI FORNITORI";
	$DcName = date("d/m/Y") . "--Dati_Economici-Prospetto_contratti_fornitori";
	break;
	case "ECO_KG":
	$orientation="L";
	$statTitle = "COSTO AL KG";
	$DcName = date("d/m/Y") . "--Dati_Economici-Costo_al_Kg";
	break;
	case "ECO_BUD":
	$orientation="P";
	$statTitle = "RAFFRONTO BUDGET / SPESA";
	$DcName = date("d/m/Y") . "--Dati_Economici-Budget_e_Spesa";
	break;
	case "ECO_PRO":
	$orientation="P";
	$statTitle = "DATI ECONOMICI PRODUTTORE";
	$DcName = date("d/m/Y") . "--Dati_Economici-Produttore";
	$cond_table="user_contratti_pro_cond";
	$cond_table_key="ID_CNT_COND_P";
	break;
	case "ECO_TRA":
	$orientation="P";
	$statTitle = "DATI ECONOMICI TRASPORTATORE";
	$DcName = date("d/m/Y") . "--Dati_Economici-Trasportatore";
	$cond_table="user_contratti_trasp_cond";
	$cond_table_key="ID_CNT_COND_T";
	break;
	case "ECO_DES":
	$orientation="P";
	$statTitle = "DATI ECONOMICI DESTINATARIO";
	$DcName = date("d/m/Y") . "--Dati_Economici-Destinatario";
	$cond_table="user_contratti_dest_cond";
	$cond_table_key="ID_CNT_COND_D";
	break;
	case "ECO_INT":
	$orientation="P";
	$statTitle = "DATI ECONOMICI INTERMEDIARIO";
	$DcName = date("d/m/Y") . "--Dati_Economici-Intermediario";
	$cond_table="user_contratti_int_cond";
	$cond_table_key="ID_CNT_COND_I";
	break;


	case "ECO_PRO_CAFC":
	$orientation="L";
	$statTitle = "DATI ECONOMICI PRODUTTORE";
	$DcName = date("d/m/Y") . "--Dati_Economici-Produttore";
	$cond_table="user_contratti_pro_cond";
	$cond_table_key="ID_CNT_COND_P";
	break;
	case "ECO_TRA_CAFC":
	$orientation="L";
	$statTitle = "DATI ECONOMICI TRASPORTATORE";
	$DcName = date("d/m/Y") . "--Dati_Economici-Trasportatore";
	$cond_table="user_contratti_trasp_cond";
	$cond_table_key="ID_CNT_COND_T";
	break;
	case "ECO_DES_CAFC":
	$orientation="L";
	$statTitle = "DATI ECONOMICI DESTINATARIO";
	$DcName = date("d/m/Y") . "--Dati_Economici-Destinatario";
	$cond_table="user_contratti_dest_cond";
	$cond_table_key="ID_CNT_COND_D";
	break;
	case "ECO_INT_CAFC":
	$orientation="L";
	$statTitle = "DATI ECONOMICI INTERMEDIARIO";
	$DcName = date("d/m/Y") . "--Dati_Economici-Intermediario";
	$cond_table="user_contratti_int_cond";
	$cond_table_key="ID_CNT_COND_I";
	break;



	case "ListaTargheViaggiAdr":
	$orientation="L";
	$statTitle = "LISTA TARGHE / VIAGGI ADR";
	$statDesc = "Lista di controllo dei viaggi effettuati effettivamente con rifiuti assoggettati alla normativa ADR (D.M. 40/2000)";
	$DcName = date("d/m/Y") . "--CDM-Lista_Targhe_-_Viaggi_ADR";
	break;
	case "ControlloPesiDestinatari":
	$orientation="P";
	$statTitle = "STATISTICA CONTROLLO PESI DESTINATARI";
	$statDesc = "Controllo delle differenze percentuali tra i quantitativi di rifiuti spediti e quelli riscontrati dal Destinatario";
	$DcName = date("d/m/Y") . "--CDM-Controllo_Pesi_Destinatari";
	break;
	case "AnPRO":
	$orientation="P";
	$statTitle = "MOVIMENTAZIONE ANALITICA PRODUTTORE";
	$statDesc = "Lista dei singoli movimenti di carico o scarico di rifiuti effettuati";
	$DcName = date("d/m/Y") . "--Movimentazione_Analitica-Produttore";
	break;
	case "AnTRA":
	$orientation="P";
	$statTitle = "MOVIMENTAZIONE ANALITICA TRASPORTATORE";
	$DcName = date("d/m/Y") . "--Movimentazione_Analitica-Trasportatore";
	$statDesc = "Lista dei singoli movimenti di carico o scarico di rifiuti effettuati";
	break;
	case "AnDES":
	$orientation="P";
	$statTitle = "MOVIMENTAZIONE ANALITICA DESTINATARIO";
	$DcName = date("d/m/Y") . "--Movimentazione_Analitica-Destinatario";
	$statDesc = "Lista dei singoli movimenti di carico o scarico di rifiuti effettuati";
	break;
	case "AnINT":
	$orientation="P";
	$statTitle = "MOVIMENTAZIONE ANALITICA INTERMEDIARIO";
	$DcName = date("d/m/Y") . "--Movimentazione_Analitica-Intermediario";
	$statDesc = "Lista dei singoli movimenti di carico o scarico di rifiuti effettuati";
	break;
	case "ADC":
	$orientation="L";
	$statTitle = "ANALISI DATI CONFERIMENTI - WMS 2012";
	$DcName = date("d/m/Y") . "--CDM-ANALISI_DATI_CONFERIMENTI_-_WMS_2012";
	$statDesc = "Statistica di analisi dei dati di conferimento dei rifiuti in base ai criteri di identificazione e raggruppamento per stato fisico, pericolosit� e specificit� o di a-specificit� - Waste Management System 2012";
	break;
	case "ComuniConferitori":
	$orientation="P";
	$statTitle = "COMUNI CONFERITORI";
	$DcName = date("d/m/Y") . "--Comuni_conferitori";
	$statDesc = "Statistica di analisi dei dati di conferimento dei rifiuti in base al comune di provenienza";
	break;
	case "CSVmovimentazione":
	$orientation="P";
	$statTitle = "CSV Movimentazione";
	$DcName = date("d/m/Y") . "--CSV_Movimentazione";
	$statDesc = "Estrazione in formato csv dei dati di movimentazione";
	break;
	case "GlPRO":
	$orientation="L";
	$statTitle = "MOVIMENTAZIONE GLOBALE PRODUTTORE";
	$DcName = date("d/m/Y") . "--Movimentazione_Globale-Produttore";
	$statDesc = "Consuntivo movimentazione di carico e scarico di rifiuti effettuata";
	break;
	case "GlDES":
	$orientation="L";
	$statTitle = "MOVIMENTAZIONE GLOBALE DESTINATARIO";
	$DcName = date("d/m/Y") . "--Movimentazione_Globale-Destinatario";
	$statDesc = "Consuntivo movimentazione di carico e scarico di rifiuti effettuata";
	break;
	case "GlTRA":
	$orientation="L";
	$statTitle = "MOVIMENTAZIONE GLOBALE TRASPORTATORE";
	$DcName = date("d/m/Y") . "--Movimentazione_Globale-Trasportatore";
	$statDesc = "Consuntivo movimentazione di carico e scarico di rifiuti effettuata";
	break;
	case "GlINT":
	$orientation="L";
	$statTitle = "MOVIMENTAZIONE GLOBALE INTERMEDIARIO";
	$DcName = date("d/m/Y") . "--Movimentazione_Globale-Intermediario";
	$statDesc = "Consuntivo movimentazione di carico e scarico di rifiuti effettuata";
	break;
	case "Giacenze":
	$orientation="L";
	$statTitle = "GIACENZA RIFIUTI";
	$statDesc = "Saldo complessivo della movimentazione dei rifiuti";
	$DcName = date("d/m/Y") . "--Statistica_Giacenza_Rifiuti";
	break;
	case "GiacenzeAS":
	$orientation="P";
	$statTitle = "SALDO AREE STOCCAGGIO";
	$statDesc = "Saldo complessivo della giacenza dei rifiuti per area di stoccaggio";
	$DcName = date("d/m/Y") . "--Statistica_Giacenza_Rifiuti_Aree_Stoccaggio";
	break;
	case "Saldo":
	$orientation="P";
	$statTitle = "STAMPA GIACENZE FINALE";
	$statDesc = "Saldo complessivo della giacenza dei rifiuti a registro, al 31 dicembre ".substr($_SESSION["DbInUse"], -4);
	$DcName = date("d/m/Y") . "--Statistica_Saldo_Chiusura_registrazioni";
	break;
	case "VerificaCarichi":
	$orientation="L";
	$statTitle = "VERIFICA CARICHI A REGISTRO";
	$statDesc = "Identificazione nel registro c/s dei rifiuti in \"deposito temporaneo\" - pericolosi e non pericolosi - tempo di giacenza raggiunto";
	$DcName = date("d/m/Y") . "--Statistica_Verifica_Carichi_A_Registro";
	break;
	case "OrganizzazioneDeposito":
	$orientation="L";
	$statTitle = "GESTIONE DEPOSITO RIFIUTI";
	$statDesc = "Riepilogo dei valori di organizzazione e gestione del deposito rifiuti";
	$DcName = date("d/m/Y") . "--Statistica_Gestione_Deposito_Rifiuti";
	break;
	case "PrevisioneConferimenti":
	$orientation="P";
	$statTitle = "PREVISIONE CONFERIMENTI";
	$statDesc = "Lista dei viaggi e delle date correlate previsti in base all'andamento di riempimento dei contenitori";
	$DcName = date("d/m/Y") . "--Statistica_Previsione_Conferimenti";
	break;
	case "ActualStock":
	$orientation="L";
	$statTitle = "DEPOSITO RIFIUTI";
	$statDesc = "Visione dei rifiuti in \"deposito temporaneo\" - rapporto volumetrico";
	$DcName = date("d/m/Y") . "--Statistica_Deposito_Rifiuti";
	break;
	case "ActualStockControl":
	$orientation="L";
	$statTitle = "CONTROLLO DEPOSITO RIFIUTI";
	$statDesc = "Riepilogo dei dati di pericolosit� ed etichettatura correntemente utilizzati per il Deposito Rifiuti";
	$DcName = date("d/m/Y") . "--Statistica_Controllo_Deposito_Rifiuti";
	break;
	case "RecSmalt":
	$orientation="L";
	$statTitle = "LISTA MOVIMENTI RECUPERO / SMALTIMENTO";
	$statDesc = "Lista dei soli movimenti di scarico di rifiuti e relativi trattamenti - arrivit� di Recupero (R1-R13) - avvio in Discarica (D1; D5) - avvio a  Stoccaggio (D13; D14; D15) - altri Smaltimenti (D)";
	$DcName = date("d/m/Y") . "--Statistica_Lista_Movimenti_Recupero-Smaltimento";
	break;
	case "DistCER":
	$orientation="P";
	$statTitle = "GRAFICO DISTRIBUZIONE RIFIUTI PRODOTTI/SMALTITI";
	$statDesc = "Grafico consuntivo percentuale del quantitativo di rifiuti caricati o smaltiti";
	$DcName = date("d/m/Y") . "--Statistica_Distribuzione_CER";
	break;
	case "CERgraph":
	$orientation="L";
	$statTitle = "GRAFICO ANDAMENTO CER";
	$statDesc = "Grafico dell'andamento cronologico del quantitativo di rifiuti conferiti o trattati";
	$DcName = date("d/m/Y") . "--Statistica_Grafico_Andamento_CER";
	break;
	case "SoggMOV":
	$orientation="L";
	$statTitle = "DETTAGLIO TRASPORTI";
	$statDesc = "Lista dei singoli movimenti di carico o scarico di rifiuti con identificazione del Produttore - Destinatario - Trasportatore - Intermediario";
	$DcName = date("d/m/Y") . "--Dettaglio_Trasporti";
	break;
	case "SoggCER":
	$orientation="P";
	$statTitle = "LISTA FORNITORI";
	$statDesc = "Visione dei Fornitori disponibili per ciascun rifiuto prodotto - Trasportatori, Destinatari e Intermediari";
	$DcName = date("d/m/Y") . "--Prospetto_parco_fornitori";
	break;
	case "Autisti":
	$orientation="P";
	$statTitle = "ELENCO AUTISTI";
	$statDesc = "Lista degli autisti e delle abilitazioni a condurre merci pericolose in ADR - scadenze";
	$DcName = date("d/m/Y") . "--Statistica_Elenco_Autisti";
	break;
	case "PesiNonConformi":
	$orientation="P";
	$statTitle = "ELENCO PESI RISCONTRATI NON CONFORMI";
	$statDesc = "Lista dei viaggi effettuati con riscontro dei pesi dichiarati dai Destinatari non conformi al livello di controllo aziendale";
	$DcName = date("d/m/Y") . "--CDM-Elenco_Pesi_Riscontrati_Non_Conformi";
	break;
	case "BilancioECO":
	$orientation="P";
	$statTitle = "BILANCIO ECOLOGICO";
	$statDesc = "Statistica e grafico dei rifiuti conferiti o gestiti in base ai trattamenti specifici";
	$DcName = date("d/m/Y") . "--Statistica_Bilancio_Ecologico";
	break;
	case "BilancioRD":
	$orientation="L";
	$statTitle = "Bilancio ambientale";
	$statDesc = "Statistica e grafico dei rifiuti conferiti o gestiti in base ai trattamenti - Categorie R oppure D - dettaglio per Azienda destinataria";
	$DcName = date("d/m/Y") . "--Statistica_Bilancio_Ambientale";
	break;
	case "DestinazioneRifiuti":
	$orientation="P";
	$statTitle = "DESTINAZIONE RIFIUTI";
	$statDesc = "Verifica statistica  (comparativa) delle differenti destinazioni di recupero o smaltimento dei rifiuti, in base al trattamento svolto dai Destinatari";
	$DcName = date("d/m/Y") . "--CDM-Destinazione_Rifiuti";
	break;
	case "GraficoRifiuti":
	$orientation="P";
	$statTitle = "GRAFICO DESTINAZIONE RIFIUTI";
	$statDesc = "Visualizzazione grafica delle differenti destinazioni dei rifiuti, in base al trattamento svolto dai Recuperatori/Destinatari";
	$DcName = date("d/m/Y") . "--Statistica_Grafico_Destinazione_Rifiuti";
	break;
	case "TraspIncoerenti":
	$orientation="L";
	$statTitle = "ABILITAZIONI AL TRASPORTO NON COERENTI";
	$statDesc = "Lista dei viaggi che presentano incoerenze nelle abilitazioni conto terzi / conto proprio dei trasportatori";
	$DcName = date("d/m/Y") . "--CDM-Abilitazioni_Trasporto_Non_Coerenti";
	break;
	case "CostoViaggi":
	$orientation="P";
	$statTitle = "COSTO VIAGGI ESEGUITI";
	$statDesc = "Lista dei viaggi, dei relativi kilometraggi e costi stimati";
	$DcName = date("d/m/Y") . "--CDM-Costo_Viaggi";
	break;
	case "IVcopie":
	$orientation="L";
	$statTitle = "IV COPIE MANCANTI";
	$statDesc = "Elenco dei formulari per i quali non � stato segnalato il rientro della IV copia";
	$DcName = date("d/m/Y") . "--CDM-Elenco_FIR_senza_IV_copia";
	break;
	case "IVcopieDaRestituire":
	$orientation="P";
	$statTitle = "IV COPIE DA RESTITUIRE";
	$statDesc = "Elenco dei formulari per i quali non � stato segnalato il ritorno della IV copia";
	$DcName = date("d/m/Y") . "--CDM-Elenco_IV_copie_da_restituire";
	$PrintIntestazione = false;
	break;
    	case "DurataViaggi":
	$orientation="L";
	$statTitle = "DURATA VIAGGI";
	$statDesc = "Elenco dei formulari emessi con evidenza del tempo di viaggio calcolato dall'inizio trasporto all'arrivo presso l'impianto";
	$DcName = date("d/m/Y") . "--CDM-Durata_viaggi";
	break;
	case "PesoMezzi":
	$orientation="L";
	$statTitle = "Controllo peso automezzi";
	$statDesc = "Elenco dei pesi - lordi, tare e netti - relativi ai mezzi (autocarri o semi rimorchi) impiegati nella movimentazione dei CER.";
	$DcName = date("d/m/Y") . "--CDM-Controllo_Peso_Automezzi";
	break;
	case "OrigineDatiD":
	$orientation="L";
	$statTitle = "ORIGINE DEI DATI AUTORIZZATIVI - DESTINATARI";
	$statDesc = "Verifica dell'origine dei documenti dei Fornitori (autorizzazioni, iscrizioni e integrazioni) utilizzati per generare i formulari e conferire i rifiuti";
	$DcName = date("d/m/Y") . "--CDM-Origine_dei_dati_autorizzativi_-_Destinatari";
	break;
	case "OrigineDatiT":
	$orientation="L";
	$statTitle = "ORIGINE DEI DATI AUTORIZZATIVI - TRASPORTATORI";
	$statDesc = "Verifica dell'origine dei documenti dei Fornitori (autorizzazioni, iscrizioni e integrazioni) utilizzati per generare i formulari e conferire i rifiuti";
	$DcName = date("d/m/Y") . "--CDM-Origine_dei_dati_autorizzativi_-_Trasportatori";
	break;
	case "ControlloDeposito":
	$orientation="L";
	$statTitle = "CONTROLLO DEPOSITO";
	$statDesc = "Controllo del deposito (criterio temporale) e andamento dei carichi";
	$DcName = date("d/m/Y") . "--Controllo_Deposito";
	break;
//}}}
}
#
#	STRINGHE SQL
#


switch($_POST["Stat"]) {

		case "SoggCER":
		if(isset($_SESSION['firstquery']))	unset($_SESSION['firstquery']);
		if(isset($_SESSION['secondquery'])) unset($_SESSION['secondquery']);
		if(isset($_SESSION['thirdquery']))	unset($_SESSION['thirdquery']);
		$PrintImpianto = true;
		$PrintCER = true;
		$SkipStandardNoRecs = true;


		$sql = "SELECT";

		$sql.=" user_aziende_trasportatori.description as trasportatore,
				user_aziende_trasportatori.indirizzo as indirizzo,

				user_autorizzazioni_trasp.num_aut,
				user_autorizzazioni_trasp.rilascio,
				user_autorizzazioni_trasp.scadenza,

				lov_comuni_istat.description as comune,
				lov_comuni_istat.CAP as CAP,
				lov_comuni_istat.shdes_prov as provincia,

				user_schede_rifiuti.descrizione,
				user_schede_rifiuti.ID_RIF,
				lov_cer.COD_CER as CER,

				user_automezzi.description as targa,
				user_rimorchi.description as rimorchio

				from user_impianti_trasportatori

				join user_autorizzazioni_trasp on user_impianti_trasportatori.ID_UIMT=user_autorizzazioni_trasp.ID_UIMT
				join user_aziende_trasportatori on user_impianti_trasportatori.ID_AZT=user_aziende_trasportatori.ID_AZT
				join lov_comuni_istat on user_aziende_trasportatori.ID_COM=lov_comuni_istat.ID_COM
				join user_schede_rifiuti on user_autorizzazioni_trasp.ID_RIF=user_schede_rifiuti.ID_RIF
				join lov_cer on user_schede_rifiuti.ID_CER=lov_cer.ID_CER
				left join user_automezzi on user_autorizzazioni_trasp.ID_AUTHT=user_automezzi.ID_AUTHT
				left join user_rimorchi on user_autorizzazioni_trasp.ID_AUTHT=user_rimorchi.ID_AUTHT ";

		$sql2 = "SELECT";

		$sql2.=" user_aziende_destinatari.description as destinatario,
				 user_impianti_destinatari.description as impianto,

 				 lov_comuni_istat.description as comune,
				 lov_comuni_istat.CAP as CAP,
				 lov_comuni_istat.shdes_prov as provincia,


				 user_autorizzazioni_dest.num_aut,
				 user_autorizzazioni_dest.rilascio,
				 user_autorizzazioni_dest.scadenza,

				 user_schede_rifiuti.descrizione,
				 user_schede_rifiuti.ID_RIF,
				 lov_cer.COD_CER as CER,

  				 lov_operazioni_rs.description as codice_op1,
				 lov_operazioni_rs.longdes as desc_op1

				 from user_autorizzazioni_dest

				 join user_impianti_destinatari on user_autorizzazioni_dest.ID_UIMD=user_impianti_destinatari.ID_UIMD
				 join user_aziende_destinatari on user_impianti_destinatari.ID_AZD=user_aziende_destinatari.ID_AZD
				 join user_schede_rifiuti on user_autorizzazioni_dest.ID_RIF=user_schede_rifiuti.ID_RIF
				 left join lov_cer on user_schede_rifiuti.ID_CER=lov_cer.ID_CER
			 	 left join lov_operazioni_rs on user_autorizzazioni_dest.ID_OP_RS=lov_operazioni_rs.ID_OP_RS
				 join lov_comuni_istat on user_impianti_destinatari.ID_COM=lov_comuni_istat.ID_COM

				";

		$sql3 = "SELECT";

		$sql3.=" user_aziende_intermediari.description as intermediario,
				 user_impianti_intermediari.description as impianto,

 				 lov_comuni_istat.description as comune,
				 lov_comuni_istat.CAP as CAP,
				 lov_comuni_istat.shdes_prov as provincia,

				 user_autorizzazioni_interm.num_aut,
				 user_autorizzazioni_interm.rilascio,
				 user_autorizzazioni_interm.scadenza,

				 user_schede_rifiuti.descrizione,
				 user_schede_rifiuti.ID_RIF,
				 lov_cer.COD_CER as CER

				 from user_autorizzazioni_interm

				 join user_impianti_intermediari on user_autorizzazioni_interm.ID_UIMI=user_impianti_intermediari.ID_UIMI
				 join user_aziende_intermediari on user_impianti_intermediari.ID_AZI=user_aziende_intermediari.ID_AZI
				 join user_schede_rifiuti on user_autorizzazioni_interm.ID_RIF=user_schede_rifiuti.ID_RIF
				 left join lov_cer on user_schede_rifiuti.ID_CER=lov_cer.ID_CER
				 join lov_comuni_istat on user_impianti_intermediari.ID_COM=lov_comuni_istat.ID_COM

				";

		$order =" ORDER BY CER asc, trasportatore asc, targa asc, rimorchio asc ";
		$order2=" ORDER BY CER asc, destinatario asc, impianto asc ";
		$order3=" ORDER BY CER asc, intermediario asc, impianto asc ";

		$sql .= " WHERE user_aziende_trasportatori.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		$sql2.= " WHERE user_aziende_destinatari.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		$sql3.= " WHERE user_aziende_intermediari.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";

		$TableSpec = "user_aziende_trasportatori.";
		require("../__includes/SOGER_DirectProfilo.php");
		$TableSpec= "user_aziende_destinatari.";
		require("../__includes/SOGER_DirectProfilo_sql2.php");
		$TableSpec= "user_aziende_intermediari.";
		require("../__includes/SOGER_DirectProfilo_sql3.php");


		break;


	case "ECO_KG":

		$PrintImpianto = true;
		$PrintCER = true;
		$SkipStandardNoRecs = false;

		$sql = "SELECT user_movimenti_fiscalizzati.ID_MOV_F, ROUND(pesoN, 2) AS pesoN, ROUND(user_movimenti_costi.euro, 2 ) AS euro, user_movimenti_fiscalizzati.ID_RIF, user_schede_rifiuti.ID_CER, user_schede_rifiuti.descrizione, lov_cer.COD_CER ";

		$sql.= "FROM user_movimenti_costi ";

		$sql.= "JOIN user_movimenti_fiscalizzati ON user_movimenti_costi.ID_MOV_F = user_movimenti_fiscalizzati.ID_MOV_F ";
		$sql.= "JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF ";
		$sql.= "JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";

		$sql.= "WHERE user_movimenti_costi.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "' ";

		$sql.= "AND user_movimenti_fiscalizzati.TIPO='S' ";

		// exclude NMOV=9999999
		$sql.= "AND user_movimenti_fiscalizzati.NMOV<>9999999 ";

		$TableSpec = "user_schede_rifiuti.";
		require("../__includes/SOGER_DirectProfilo.php");
		$order = " ORDER BY user_schede_rifiuti.ID_RIF, user_movimenti_fiscalizzati.ID_MOV_F ";


		break;


	case "ECO_BUD":

		$PrintImpianto = true;
		$PrintCER = true;
		$SkipStandardNoRecs = false;

		$sql = "SELECT user_contratti_budget.euro_anno, user_contratti_budget.euro_gen, user_contratti_budget.euro_feb, user_contratti_budget.euro_mar, user_contratti_budget.euro_apr, user_contratti_budget.euro_mag, user_contratti_budget.euro_giu, user_contratti_budget.euro_lug, user_contratti_budget.euro_ago, user_contratti_budget.euro_set, user_contratti_budget.euro_ott, user_contratti_budget.euro_nov, user_contratti_budget.euro_dic, user_movimenti_costi.euro, user_movimenti_costi.DT, user_movimenti_costi.ID_MOV_F, user_movimenti_costi.ID_MOV_CST, ";
		$sql.= "user_schede_rifiuti.ID_CER, user_schede_rifiuti.descrizione, user_movimenti_fiscalizzati.ID_RIF, lov_cer.COD_CER ";
		$sql.= "FROM user_contratti_budget ";
		$sql.= "JOIN user_schede_rifiuti ON user_contratti_budget.ID_RIF=user_schede_rifiuti.ID_RIF ";
		$sql.= "JOIN user_movimenti_fiscalizzati ON user_schede_rifiuti.ID_RIF=user_movimenti_fiscalizzati.ID_RIF ";
		$sql.= "JOIN user_movimenti_costi ON user_movimenti_fiscalizzati.ID_MOV_F=user_movimenti_costi.ID_MOV_F ";
		$sql.= "JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";

		$sql.= "WHERE user_contratti_budget.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "' ";

		$TableSpec = "user_schede_rifiuti.";
		require("../__includes/SOGER_DirectProfilo.php");
		$order =  " ORDER BY  lov_cer.COD_CER, user_schede_rifiuti.ID_RIF,user_schede_rifiuti.descrizione ";


		break;

	case "ECO_PRO_CAFC":
	case "ECO_TRA_CAFC":
	case "ECO_DES_CAFC":
	case "ECO_INT_CAFC":

		$PrintImpianto		= true;
		$PrintCER			= true;
		$SkipStandardNoRecs = false;

		$sql = "SELECT ID_MOV_F, NMOV, DTFORM, pesoN, ComuneConferitore.ID_COM AS id_comune_conferitore, ComuneConferitore.description AS comune_conferitore, NFORM ";
		$sql.= ",".$MovTable.".$AzKey AS AZref,user_schede_rifiuti.ID_RIF as CERref,".$MovTable.".$ImpKey AS IMPref";
		$sql.= ",user_schede_rifiuti.descrizione,lov_cer.COD_CER,user_schede_rifiuti.giac_ini";
		$sql.= ",$AzTable.description AS AZdes,$AzTable.indirizzo AS AZind,$AzTable.codfisc AS AZCF";
		$sql.= ",AZcomTable.description AS AZcom,AZcomTable.des_prov AS AZprov,AZcomTable.shdes_prov AS AZshprov,AZcomTable.nazione AS AZnaz,AZcomTable.CAP AS AZCAP";
		$sql.= ",$ImpTable.description AS IMPdes,IMPcomTable.description AS IMPcom,IMPcomTable.des_prov AS IMPprov,IMPcomTable.shdes_prov AS IMPshprov,IMPcomTable.nazione AS IMPnaz, IMPcomTable.CAP AS IMPCAP ";
		$sql.= "FROM ".$MovTable." ";
		$sql.= "JOIN user_impianti_produttori ON user_impianti_produttori.ID_UIMP = ".$MovTable.".ID_UIMP ";
		$sql.= "JOIN user_schede_rifiuti ON ".$MovTable.".ID_RIF=user_schede_rifiuti.ID_RIF ";
		$sql.= "JOIN $AzTable ON $AzTable.$AzKey=".$MovTable.".$AzKey ";
		if($ImpTable!='user_impianti_produttori')
			$sql.= "JOIN $ImpTable ON $ImpTable.$ImpKey=".$MovTable.".$ImpKey ";
		$sql.= "JOIN lov_comuni_istat AS AZcomTable ON $AzTable.ID_COM=AZcomTable.ID_COM ";
		$sql.= "JOIN lov_comuni_istat AS IMPcomTable ON $ImpTable.ID_COM=IMPcomTable.ID_COM ";
		$sql.= "JOIN lov_comuni_istat AS ComuneConferitore ON ComuneConferitore.ID_COM = user_impianti_produttori.ID_COM ";
		$sql.= "JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";

		$sql.= "WHERE ".$MovTable.".ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "' ";

		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");

		// exclude NMOV=9999999
		$sql.= "AND user_movimenti_fiscalizzati.NMOV<>9999999 ";
		//$sql.= "AND user_movimenti_fiscalizzati.TIPO='C' ";
		$order =  " ORDER BY $AzTable.description, ".$MovTable.".$AzKey,".$MovTable.".$ImpKey,lov_cer.COD_CER,".$MovTable.".ID_RIF,".$MovTable.".NMOV";
		break;

	case "ECO_PRO":
	case "ECO_TRA":
	case "ECO_DES":
	case "ECO_INT":
//{{{
		$PrintImpianto = true;
		$PrintCER = true;
		$SkipStandardNoRecs = false;


		$sql = "SELECT user_movimenti_costi.euro, user_movimenti_costi.FKE_UM, user_movimenti_costi.".$cond_table_key.", ";
		$sql.= "user_movimenti_costi.".$AzKey.", user_movimenti_costi.ID_MOV_F, ".$cond_table.".description AS condizione, ";
		$sql.= $AzTable.".description, ".$AzTable.".indirizzo, ".$AzTable.".ID_COM, lov_cer.COD_CER, ";
		$sql.= "lov_comuni_istat.description AS comune, lov_comuni_istat.CAP, lov_comuni_istat.shdes_prov AS PROV, ";
		$sql.= "user_schede_rifiuti.ID_CER, user_schede_rifiuti.descrizione, user_movimenti_fiscalizzati.ID_RIF ";
		$sql.= "FROM user_movimenti_costi ";
		$sql.= "JOIN ".$cond_table." ON user_movimenti_costi.".$cond_table_key."=".$cond_table.".".$cond_table_key." ";
		$sql.= "JOIN ".$AzTable." ON user_movimenti_costi.".$AzKey."=".$AzTable.".".$AzKey." ";
		$sql.= "JOIN user_movimenti_fiscalizzati ON user_movimenti_costi.ID_MOV_F=user_movimenti_fiscalizzati.ID_MOV_F ";
		$sql.= "JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF ";
		$sql.= "JOIN lov_comuni_istat ON ".$AzTable.".ID_COM=lov_comuni_istat.ID_COM ";
		$sql.= "JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";

		$sql.= "WHERE user_movimenti_costi.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "' ";

		// exclude NMOV=9999999
		$sql.= "AND user_movimenti_fiscalizzati.NMOV<>9999999 ";

		#
		$TableSpec = "user_movimenti_fiscalizzati.";
		require("../__includes/SOGER_DirectProfilo.php");
		$order =  " ORDER BY ".$AzTable.".description, lov_cer.COD_CER, user_schede_rifiuti.descrizione, ".$cond_table.".".$cond_table_key."";
	break;

	case "ListaTargheViaggiAdr":
		$sql = "SELECT ".$MovTable.".pesoN as quantita, ".$MovTable.".NFORM as formulario, ".$MovTable.".FKEpesospecifico AS pesospecifico, ";
		$sql.= "user_schede_rifiuti.descrizione as rifiuto, lov_cer.COD_CER as cer, lov_misure.description as um, ";
		$sql.= "user_automezzi.description as targa, lov_num_onu.description as onu, user_aziende_trasportatori.description AS trasportatore ";

		$sql.= "FROM ".$MovTable." ";

		$sql.= "JOIN user_schede_rifiuti ON ".$MovTable.".ID_RIF=user_schede_rifiuti.ID_RIF ";
		$sql.= "LEFT JOIN lov_num_onu ON user_schede_rifiuti.ID_ONU=lov_num_onu.ID_ONU ";
		$sql.= "LEFT JOIN user_automezzi ON ".$MovTable.".ID_AUTO=user_automezzi.ID_AUTO ";
		$sql.= "JOIN lov_misure ON user_schede_rifiuti.ID_UMIS=lov_misure.ID_UMIS ";
		$sql.= "JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
		$sql.= "JOIN user_aziende_trasportatori ON ".$MovTable.".ID_AZT=user_aziende_trasportatori.ID_AZT ";

		$sql.= "WHERE ".$MovTable.".ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "' ";
		$sql.= "AND ".$MovTable.".ADR='1' AND ".$MovTable.".TIPO='S' ";

		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";

		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");

		$order = " ORDER BY trasportatore, targa, cer, rifiuto ";


	break;

	case "ControlloPesiDestinatari":
//{{{
		$PrintImpianto = true;
		$PrintCER = true;
		$SkipStandardNoRecs = false;
		$sql = "SELECT NMOV,DTMOV,TIPO,quantita,PS_DESTINO,NFORM";
		$sql .= ",user_movimenti_fiscalizzati.$AzKey AS AZref,user_schede_rifiuti.ID_RIF as CERref,user_movimenti_fiscalizzati.$ImpKey AS IMPref";
		$sql .= ",user_schede_rifiuti.descrizione,lov_cer.COD_CER,user_schede_rifiuti.giac_ini,user_movimenti_fiscalizzati.FKEpesospecifico AS peso_spec";
		$sql .= ",lov_misure.description AS UnMis";
		$sql .= ",$AzTable.description AS AZdes,$AzTable.indirizzo AS AZind";
		$sql .= ",AZcomTable.description AS AZcom,AZcomTable.des_prov AS AZprov,AZcomTable.shdes_prov AS AZshprov,AZcomTable.nazione AS AZnaz";
		$sql .= ",$ImpTable.description AS IMPdes";
		$sql .= ",IMPcomTable.description AS IMPcom,IMPcomTable.des_prov AS IMPprov,IMPcomTable.shdes_prov AS IMPshprov,IMPcomTable.nazione AS IMPnaz";
		$sql .= " FROM user_movimenti_fiscalizzati";
		$sql .= " JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF";
		$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
		$sql .= " JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS";
		$sql .= " JOIN $AzTable ON $AzTable.$AzKey=user_movimenti_fiscalizzati.$AzKey";
		$sql .= " JOIN $ImpTable ON $ImpTable.$ImpKey=user_movimenti_fiscalizzati.$ImpKey";
		$sql .= " JOIN lov_comuni_istat AS AZcomTable ON $AzTable.ID_COM=AZcomTable.ID_COM";
		$sql .= " JOIN lov_comuni_istat AS IMPcomTable ON $ImpTable.ID_COM=IMPcomTable.ID_COM";
		$sql .= " WHERE user_movimenti_fiscalizzati.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		$sql .= " AND TIPO='S' ";
		// exclude NMOV=9999999
		$sql.= "AND user_movimenti_fiscalizzati.NMOV<>9999999 ";
		#
		$TableSpec = "user_movimenti_fiscalizzati.";
		require("../__includes/SOGER_DirectProfilo.php");
		//$order =  " ORDER BY user_movimenti_fiscalizzati.$AzKey,user_movimenti_fiscalizzati.$ImpKey,user_movimenti_fiscalizzati.ID_RIF,user_movimenti_fiscalizzati.NMOV";
		$order =  " ORDER BY $AzTable.description, user_movimenti_fiscalizzati.$AzKey,user_movimenti_fiscalizzati.$ImpKey,lov_cer.COD_CER,user_movimenti_fiscalizzati.ID_RIF,user_movimenti_fiscalizzati.NMOV";
	break;

	case "AnPRO":
	case "AnTRA":
	case "AnDES":
//{{{
		$PrintImpianto = true;
		$PrintCER = true;
		$SkipStandardNoRecs = false;
		$sql = "SELECT ".$IDMov.",NMOV,DTMOV,TIPO,quantita,tara,pesoL as lordo,PS_DESTINO,NFORM";
		$sql .= ",".$MovTable.".$AzKey AS AZref,user_schede_rifiuti.ID_RIF as CERref,".$MovTable.".$ImpKey AS IMPref";
		$sql .= ",user_schede_rifiuti.descrizione,lov_cer.COD_CER,user_schede_rifiuti.giac_ini";
		$sql .= ",lov_misure.description AS UnMis";
		$sql .= ",$AzTable.description AS AZdes,$AzTable.indirizzo AS AZind,$AzTable.codfisc AS AZCF";
		$sql .= ",AZcomTable.description AS AZcom,AZcomTable.des_prov AS AZprov,AZcomTable.shdes_prov AS AZshprov,AZcomTable.nazione AS AZnaz,AZcomTable.CAP AS AZCAP,$ImpTable.description AS IMPdes";
		if($ImpTable == 'user_impianti_produttori')
			$sql .= ",$ImpTable.FuoriUnitaLocale AS FuoriUnitaLocale";
		else
			$sql .= ",1 AS FuoriUnitaLocale";
		$sql .= ",IMPcomTable.description AS IMPcom,IMPcomTable.des_prov AS IMPprov,IMPcomTable.shdes_prov AS IMPshprov,IMPcomTable.nazione AS IMPnaz, IMPcomTable.CAP AS IMPCAP";
		$sql .= " FROM ".$MovTable;
		$sql .= " JOIN user_schede_rifiuti ON ".$MovTable.".ID_RIF=user_schede_rifiuti.ID_RIF";
		$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
		$sql .= " JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS";
		$sql .= " JOIN $AzTable ON $AzTable.$AzKey=".$MovTable.".$AzKey";
		$sql .= " JOIN $ImpTable ON $ImpTable.$ImpKey=".$MovTable.".$ImpKey";
		$sql .= " JOIN lov_comuni_istat AS AZcomTable ON $AzTable.ID_COM=AZcomTable.ID_COM";
		$sql .= " JOIN lov_comuni_istat AS IMPcomTable ON $ImpTable.ID_COM=IMPcomTable.ID_COM";
		$sql .= " WHERE ".$MovTable.".ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		#
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";

		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		//$order =  " ORDER BY ".$MovTable.".$AzKey,".$MovTable.".$ImpKey,".$MovTable.".ID_RIF,".$MovTable.".NMOV";
		$order =  " ORDER BY $AzTable.description, ".$MovTable.".$AzKey,".$MovTable.".$ImpKey,lov_cer.COD_CER,".$MovTable.".ID_RIF,".$MovTable.".NMOV";
	break;

	case	"AnINT":
//{{{
		$PrintImpianto = false;
		$PrintCER = true;
		$SkipStandardNoRecs = false;
		$sql = "SELECT ".$IDMov.",NMOV,DTMOV,TIPO,quantita,PS_DESTINO,NFORM";
		$sql .= ",".$MovTable.".$AzKey AS AZref,user_schede_rifiuti.ID_RIF as CERref";
		$sql .= ",user_schede_rifiuti.descrizione,lov_cer.COD_CER,user_schede_rifiuti.giac_ini";
		$sql .= ",lov_misure.description AS UnMis";
		$sql .= ",$AzTable.description AS AZdes,$AzTable.indirizzo AS AZind";
		$sql .= ",AZcomTable.description AS AZcom,AZcomTable.des_prov AS AZprov,AZcomTable.shdes_prov AS AZshprov,AZcomTable.nazione AS AZnaz, 0 AS FuoriUnitaLocale";
		$sql .= " FROM ".$MovTable;
		$sql .= " JOIN user_schede_rifiuti ON ".$MovTable.".ID_RIF=user_schede_rifiuti.ID_RIF";
		$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
		$sql .= " JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS";
		$sql .= " JOIN $AzTable ON $AzTable.$AzKey=".$MovTable.".$AzKey";
		$sql .= " JOIN lov_comuni_istat AS AZcomTable ON $AzTable.ID_COM=AZcomTable.ID_COM";
		$sql .= " WHERE ".$MovTable.".ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";

		#
		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		//$order =  " ORDER BY ".$MovTable.".$AzKey,".$MovTable.".ID_RIF,".$MovTable.".NMOV";
		$order =  " ORDER BY $AzTable.description, ".$MovTable.".$AzKey,lov_cer.COD_CER, ".$MovTable.".ID_RIF,".$MovTable.".NMOV";
	break;

	case "ADC":
		$PrintImpianto = false;
		$PrintCER = false;
		$SkipStandardNoRecs = false;
		$sql ="SELECT COD_CER, descrizione, lov_stato_fisico.description AS SF, lov_stato_fisico.ID_SF, COUNT( ID_MOV_F ) AS FIR,";
		$sql.=" SUM( IF( PS_DESTINO >0, PS_DESTINO, pesoN ) ) AS KG, user_schede_rifiuti.pericoloso, lov_spec_rifiuti.description AS SPEC";
		$sql.=" FROM ".$MovTable;
		$sql.=" JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF = ".$MovTable.".ID_RIF";
		$sql.=" JOIN lov_cer ON lov_cer.ID_CER = user_schede_rifiuti.ID_CER";
		$sql.=" JOIN lov_spec_rifiuti ON lov_spec_rifiuti.ID_SPEC = user_schede_rifiuti.ID_SPEC";
		$sql.=" JOIN lov_stato_fisico ON lov_stato_fisico.ID_SF=user_schede_rifiuti.ID_SF";
		$sql.=" WHERE ".$MovTable.".ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		$sql.=" AND TIPO='S' ";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";
		#
		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		$group = " GROUP BY user_movimenti_fiscalizzati.ID_RIF";
	break;

	case "ComuniConferitori":
		$PrintImpianto = true;
		$PrintCER = true;
		$SkipStandardNoRecs = false;

		$sql ="SELECT lov_comuni_istat.ID_COM, lov_comuni_istat.description AS comune, shdes_prov AS provincia, lov_cer.COD_CER AS CER, user_schede_rifiuti.descrizione AS rifiuto, ";
		$sql.="SUM(pesoN) AS quantita FROM ".$MovTable." ";
		$sql.="JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=".$MovTable.".ID_RIF ";
		$sql.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
		$sql.="JOIN user_impianti_produttori ON user_impianti_produttori.ID_UIMP=".$MovTable.".ID_UIMP ";
		$sql.="JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM=user_impianti_produttori.ID_COM ";
		$sql.="WHERE ".$MovTable.".ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "' AND TIPO='C' ";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";
		#
		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		$group = " GROUP BY lov_comuni_istat.ID_COM, ".$MovTable.".ID_RIF ";
		$order = " ORDER BY comune ASC, CER ASC, rifiuto ASC ";
	break;

	case "CSVmovimentazione":
		$PrintImpianto		= false;
		$PrintCER			= false;
		$SkipStandardNoRecs = false;

		$sql ="SELECT ID_MOV_F, TIPO, ".$MovTable.".NOTEF AS NoteFormulario, DTMOV AS DataMovimento, NMOV AS NumeroMovimento, DTFORM AS DataFormulario, NFORM AS NumeroFormulario, ".$MovTable.".lotto AS NumeroLotto, lov_cer.COD_CER AS CER, user_schede_rifiuti.descrizione AS Descrizione, IF(SUBSTRING(lov_operazioni_rs.description, 1, 1)='R', 'Recupero', 'Smaltimento') AS Recupero_Smaltimento, lov_operazioni_rs.description AS Operazione_Recupero_Smaltimento, user_aziende_produttori.description AS Produttore, CONCAT(user_impianti_produttori.description, ' - ', ImpP.description, ' (', ImpP.shdes_prov, ')') AS ImpiantoP, user_aziende_destinatari.description AS Destinatario, CONCAT(user_impianti_destinatari.description, ' - ', ImpD.description, ' (', ImpD.shdes_prov, ')') AS Impianto, pesoN AS Peso_Partito, PS_DESTINO AS Peso_Destino, pesoL AS Peso_Lordo, tara AS Tara, colli AS Colli, user_aziende_trasportatori.description AS Trasportatore, user_aziende_intermediari.description AS Intermediario, IF(lov_cer.PERICOLOSO=1, 's�', 'no') AS Pericoloso, CONCAT( IF(user_schede_rifiuti.H1=1, 'H1 ', ''), IF(user_schede_rifiuti.H2=1, 'H2 ', ''), IF(user_schede_rifiuti.H3A=1, 'H3A ', ''), IF(user_schede_rifiuti.H3B=1, 'H3B ', ''), IF(user_schede_rifiuti.H4=1, 'H4 ', ''), IF(user_schede_rifiuti.H5=1, 'H5 ', ''), IF(user_schede_rifiuti.H6=1, 'H6 ', ''), IF(user_schede_rifiuti.H7=1, 'H7 ', ''), IF(user_schede_rifiuti.H8=1, 'H8 ', ''), IF(user_schede_rifiuti.H9=1, 'H9 ', ''), IF(user_schede_rifiuti.H10=1, 'H10 ', ''), IF(user_schede_rifiuti.H11=1, 'H11 ', ''), IF(user_schede_rifiuti.H12=1, 'H12 ', ''), IF(user_schede_rifiuti.H13=1, 'H13 ', ''), IF(user_schede_rifiuti.H14=1, 'H14 ', ''), IF(user_schede_rifiuti.H15=1, 'H15 ', '') ) AS Classi_H, CONCAT( IF(user_schede_rifiuti.HP1=1, 'HP1 ', ''), IF(user_schede_rifiuti.HP2=1, 'HP2 ', ''), IF(user_schede_rifiuti.HP3=1, 'HP3 ', ''), IF(user_schede_rifiuti.HP4=1, 'HP4 ', ''), IF(user_schede_rifiuti.HP5=1, 'HP5 ', ''), IF(user_schede_rifiuti.HP6=1, 'HP6 ', ''), IF(user_schede_rifiuti.HP7=1, 'HP7 ', ''), IF(user_schede_rifiuti.HP8=1, 'HP8 ', ''), IF(user_schede_rifiuti.HP9=1, 'HP9 ', ''), IF(user_schede_rifiuti.HP10=1, 'HP10 ', ''), IF(user_schede_rifiuti.HP11=1, 'HP11 ', ''), IF(user_schede_rifiuti.HP12=1, 'HP12 ', ''), IF(user_schede_rifiuti.HP13=1, 'HP13 ', ''), IF(user_schede_rifiuti.HP14=1, 'HP14 ', ''), IF(user_schede_rifiuti.HP15=1, 'HP15 ', '') ) AS Classi_HP, PerRiclassificazione,  lov_num_onu.description AS Classificazione_ADR_N_ONU, FKESF AS Stato_Fisico, FANGHI_ss AS secco, user_automezzi.description AS automezzo, user_rimorchi.description AS rimorchio FROM ".$MovTable." LEFT JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=".$MovTable.".ID_RIF LEFT JOIN user_aziende_produttori ON user_aziende_produttori.ID_AZP=".$MovTable.".ID_AZP LEFT JOIN user_aziende_destinatari ON user_aziende_destinatari.ID_AZD=".$MovTable.".ID_AZD LEFT JOIN user_impianti_produttori ON user_impianti_produttori.ID_UIMP=".$MovTable.".ID_UIMP LEFT JOIN user_impianti_destinatari ON user_impianti_destinatari.ID_UIMD=".$MovTable.".ID_UIMD JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER LEFT JOIN lov_operazioni_rs ON lov_operazioni_rs.ID_OP_RS=".$MovTable.".ID_OP_RS LEFT JOIN lov_num_onu ON lov_num_onu.ID_ONU=user_movimenti_fiscalizzati.ID_ONU LEFT JOIN lov_comuni_istat AS ImpP ON ImpP.ID_COM=user_impianti_produttori.ID_COM LEFT JOIN lov_comuni_istat AS ImpD ON ImpD.ID_COM=user_impianti_destinatari.ID_COM LEFT JOIN user_aziende_trasportatori ON ".$MovTable.".ID_AZT=user_aziende_trasportatori.ID_AZT LEFT JOIN user_aziende_intermediari ON ".$MovTable.".ID_AZI=user_aziende_intermediari.ID_AZI LEFT JOIN user_automezzi ON ".$MovTable.".ID_AUTO=user_automezzi.ID_AUTO LEFT JOIN user_rimorchi ON ".$MovTable.".ID_RMK=user_rimorchi.ID_RMK ";

		$sql .= " WHERE ".$MovTable.".ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		$sql .= " AND ".$MovTable.".NMOV<>9999999 ";
		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");

		$order = " ORDER BY NMOV ASC ";
	break;

	case "GlPRO":
	case "GlDES":
	case "GlTRA":
	case "GlINT":
//{{{
		$PrintImpianto = false;
		$PrintCER = true;
		$SkipStandardNoRecs = false;
		$sql = "SELECT quantita,pesoN,PS_DESTINO,".$MovTable.".ID_RIF AS RifID, ".$MovTable.".FKEpesospecifico AS peso_spec";
		$sql .= ",".$MovTable.".$AzKey AS AZref,user_schede_rifiuti.ID_RIF as CERref";
		$sql .= ",user_schede_rifiuti.lotto,user_schede_rifiuti.descrizione,lov_cer.COD_CER,user_schede_rifiuti.giac_ini,user_schede_rifiuti.MaxVar";
		$sql .= ",lov_misure.description AS UnMis";
		$sql .= ",$AzTable.description AS AZdes,$AzTable.indirizzo AS AZind";
		$sql .= ",AZcomTable.description AS AZcom,AZcomTable.des_prov AS AZprov,AZcomTable.shdes_prov AS AZshprov,AZcomTable.nazione AS AZnaz";
		$sql .= " FROM ".$MovTable."";
		$sql .= " JOIN user_schede_rifiuti ON ".$MovTable.".ID_RIF=user_schede_rifiuti.ID_RIF";
		$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
		$sql .= " JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS";
		$sql .= " JOIN $AzTable ON $AzTable.$AzKey=".$MovTable.".$AzKey";
		$sql .= " JOIN lov_comuni_istat AS AZcomTable ON $AzTable.ID_COM=AZcomTable.ID_COM";
		$sql .= " WHERE ".$MovTable.".ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";
		#
		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		//$order = "ORDER BY ".$MovTable.".$AzKey,".$MovTable.".ID_RIF ";
		$order = "ORDER BY $AzTable.description, ".$MovTable.".$AzKey,lov_cer.COD_CER,".$MovTable.".ID_RIF ";
	break;

	case "Giacenze":

		$PrintImpianto = false;
		$PrintCER = false;
		$SkipStandardNoRecs = false;

		$sql = "SELECT ";
		$sql.= "user_schede_rifiuti.ID_RIF, user_schede_rifiuti.descrizione, user_schede_rifiuti.ID_CER, lov_cer.COD_CER, ";
		$sql.= "user_schede_rifiuti.giac_ini, user_schede_rifiuti.peso_spec, lov_misure.description AS UnMis, ";
		$sql.= "lov_cer.PERICOLOSO, H1, H2, H3A, H3B, H4, H5, H6, H7, H8, H9, H10, H11, H12, H13, H14, H15, ";
		$sql.= "HP1, HP2, HP3, HP4, HP5, HP6, HP7, HP8, HP9, HP10, HP11, HP12, HP13, HP14, HP15, ";
		$sql.= "lov_stato_fisico.description AS StatoFisico ";
		$sql.= "FROM user_schede_rifiuti ";
		$sql.= "JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
		$sql.= "JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS ";
		$sql.= "JOIN lov_stato_fisico ON lov_stato_fisico.ID_SF=user_schede_rifiuti.ID_SF ";
		$sql.= "WHERE ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";

		#
		$TableSpec = "user_schede_rifiuti.";
		require("../__includes/SOGER_DirectProfilo.php");
		$order =  "ORDER BY lov_cer.COD_CER, user_schede_rifiuti.descrizione ASC "; //}}}

	break;

	case "GiacenzeAS":

		$PrintImpianto = false;
		$PrintCER = false;
		$SkipStandardNoRecs = false;

		$sql = "SELECT ";
		$sql.= "lov_cer.COD_CER, user_schede_rifiuti.descrizione, ";
		$sql.= "user_schede_rifiuti.FKEdisponibilita AS giacenza, ";
		$sql.= "user_aree_stoccaggio_dest.ID_ASD, user_aree_stoccaggio_dest.description AS AreaStoccaggio, user_aree_stoccaggio_dest.max_stock ";
		$sql.= "FROM user_schede_rifiuti ";
		$sql.= "JOIN user_aree_stoccaggio_dest ON user_aree_stoccaggio_dest.ID_ASD=user_schede_rifiuti.ID_ASD ";
		$sql.= "JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
		$sql.= "JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS ";
		$sql.= "JOIN lov_stato_fisico ON lov_stato_fisico.ID_SF=user_schede_rifiuti.ID_SF ";
		$sql.= "WHERE ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		$sql.= "AND user_schede_rifiuti.ID_ASD IS NOT NULL ";

		#
		$TableSpec = "user_schede_rifiuti.";
		require("../__includes/SOGER_DirectProfilo.php");
		$order =  "ORDER BY user_aree_stoccaggio_dest.ID_ASD, lov_cer.COD_CER, user_schede_rifiuti.descrizione ASC "; //}}}

	break;

	case "Saldo":

		$PrintImpianto = false;
		$PrintCER = false;
		$SkipStandardNoRecs = false;

		$sql = "SELECT ";
		$sql.= "user_schede_rifiuti.ID_RIF, user_schede_rifiuti.descrizione, user_schede_rifiuti.ID_CER, lov_cer.COD_CER, ";
		$sql.= "user_schede_rifiuti.giac_ini, lov_misure.description AS UnMis, ";
		$sql.= "user_schede_rifiuti.pericoloso, H1, H2, H3A, H3B, H4, H5, H6, H7, H8, H9, H10, H11, H12, H13, H14, H15 ";
		$sql.= "FROM user_schede_rifiuti ";
		$sql.= "JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
		$sql.= "JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS ";
		$sql.= "WHERE ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";

		#
		$TableSpec = "user_schede_rifiuti.";
		require("../__includes/SOGER_DirectProfilo.php");
		$order =  "ORDER BY lov_cer.COD_CER, user_schede_rifiuti.descrizione ASC "; //}}}
	break;



	case "OrganizzazioneDeposito":
//{{{
		$PrintImpianto = false;
		$PrintCER = false;
		$SkipStandardNoRecs = false;

		$sql = "SELECT ";
		$sql.= "user_schede_rifiuti.descrizione, user_schede_rifiuti.FKEdisponibilita AS giacenza, lov_stato_fisico.description AS SF, lov_cer.COD_CER, lov_contenitori.description AS contenitore, lov_contenitori.m_cubi, lov_contenitori.portata, user_schede_rifiuti_deposito.NumCont, user_schede_rifiuti_deposito.carico_automatico, user_schede_rifiuti_deposito.intervallo, user_schede_rifiuti_deposito.ID_ICA,user_schede_rifiuti_deposito.scarico_medio, user_schede_rifiuti_deposito.MaxStock, user_schede_rifiuti_deposito.media_gg_stima, user_schede_rifiuti_deposito.media_settimanale_stima, lov_misure.description as UM ";
		$sql.= "FROM user_schede_rifiuti ";
		$sql.= "JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
		$sql.= "JOIN lov_stato_fisico ON user_schede_rifiuti.ID_SF=lov_stato_fisico.ID_SF ";
		$sql.= "JOIN lov_misure ON user_schede_rifiuti.ID_UMIS=lov_misure.ID_UMIS ";
		$sql.= "LEFT JOIN user_schede_rifiuti_deposito ON user_schede_rifiuti.ID_RIF=user_schede_rifiuti_deposito.ID_RIF ";
		$sql.= "LEFT JOIN lov_contenitori ON user_schede_rifiuti_deposito.ID_CONT=lov_contenitori.ID_CONT ";
		$sql.= "WHERE user_schede_rifiuti_deposito.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		#
		$TableSpec = "user_schede_rifiuti.";
		require("../__includes/SOGER_DirectProfilo.php");
		$order =  "ORDER BY lov_cer.COD_CER ASC,user_schede_rifiuti.descrizione ASC ";

	break;

	case "PrevisioneConferimenti":
//{{{
		$PrintImpianto = true;
		$PrintCER = false;
		$SkipStandardNoRecs = false;

		$sql = "SELECT ";
		$sql.= "user_schede_rifiuti.descrizione, lov_cer.COD_CER, user_schede_rifiuti.PREV_NextS ";
		$sql.= "FROM user_schede_rifiuti ";
		$sql.= "JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
		$sql.= "WHERE user_schede_rifiuti.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "' ";
		$sql.= "AND user_schede_rifiuti.PREV_NextS<>''";
		#
		$TableSpec = "user_schede_rifiuti.";
		require("../__includes/SOGER_DirectProfilo.php");
		$order =  "ORDER BY user_schede_rifiuti.PREV_NextS ASC, lov_cer.COD_CER ASC ";

	break;

	case "ActualStock":
//{{{
		$PrintImpianto = false;
		$PrintCER = false;
		$SkipStandardNoRecs = false;

		$sql = "SELECT ";
		$sql.= "user_schede_rifiuti.ID_RIF as CERref, user_schede_rifiuti.ID_RIF AS RifID, user_schede_rifiuti.originalID_RIF, user_schede_rifiuti.descrizione, user_schede_rifiuti.cod_pro, user_schede_rifiuti.pericoloso AS PER, user_schede_rifiuti.adr, user_schede_rifiuti.peso_spec, user_schede_rifiuti.q_limite, user_schede_rifiuti.t_limite,user_schede_rifiuti.ID_UMIS,H1,H2,H3A,H3B,H4,H5,H6,H7,H8,H9,H10,H11,H12,H13,H14,H15,HP1,HP2,HP3,HP4,HP5,HP6,HP7,HP8,HP9,HP10,HP11,HP12,HP13,HP14,HP15, user_schede_rifiuti.ID_CER, lov_cer.COD_CER, user_schede_rifiuti.giac_ini, lov_misure.description AS UnMis, user_schede_rifiuti.MaxVar, lov_stato_fisico.description AS stato_fisico, ".$SOGER->UserData['core_usersID_GDEP']." AS ID_GDEP ";
		$sql.= "FROM user_schede_rifiuti ";
		$sql.= "JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
		$sql.= "JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS ";
		$sql.= "JOIN lov_stato_fisico ON lov_stato_fisico.ID_SF=user_schede_rifiuti.ID_SF ";
		$sql.= "WHERE ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		#
		$TableSpec = "user_schede_rifiuti.";
		require("../__includes/SOGER_DirectProfilo.php");
		$order =  "ORDER BY lov_cer.COD_CER,user_schede_rifiuti.descrizione ASC ";

	break;

	case "ActualStockControl":

		$PrintImpianto = false;
		$PrintCER = false;
		$SkipStandardNoRecs = false;
		//$sql  = "SELECT DTMOV,NMOV,quantita,user_schede_rifiuti.ID_RIF AS RifID,TIPO";
		$sql  = "SELECT user_movimenti_fiscalizzati.DTMOV,user_movimenti_fiscalizzati.NMOV,user_movimenti_fiscalizzati.quantita,user_movimenti_fiscalizzati.TIPO,user_schede_rifiuti.ID_RIF AS RifID";
		$sql .= ",user_schede_rifiuti.ID_CER as CERref,user_schede_rifiuti.pericoloso AS PER,user_schede_rifiuti.cod_pro,user_schede_rifiuti.adr";
		$sql .= ",user_schede_rifiuti.descrizione,lov_cer.COD_CER,user_schede_rifiuti.giac_ini,peso_spec";
		$sql .= ",user_schede_rifiuti.ID_UMIS, user_schede_rifiuti.q_limite, user_schede_rifiuti.t_limite";
		$sql .= ",user_schede_rifiuti.MaxVar, user_schede_rifiuti.et_comp_per, user_schede_rifiuti.et_last_print";
		$sql .= ",H1,H2,H3A,H3B,H4,H5,H6,H7,H8,H9,H10,H11,H12,H13,H14,H15";
		$sql .= ",lov_misure.description AS UnMis, lov_num_onu.description AS ONU";
		$sql .= " FROM user_schede_rifiuti ";
		$sql .= " LEFT JOIN user_movimenti_fiscalizzati ON user_schede_rifiuti.ID_RIF=user_movimenti_fiscalizzati.ID_RIF ";
		$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
		$sql .= " JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS";
		$sql .= " LEFT JOIN lov_num_onu ON user_schede_rifiuti.ID_ONU = lov_num_onu.ID_ONU ";
		$sql .= " WHERE user_schede_rifiuti.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";
		#
		$TableSpec = "user_schede_rifiuti.";
		require("../__includes/SOGER_DirectProfilo.php");
		//$order =  "ORDER BY user_schede_rifiuti.pericoloso DESC,lov_cer.COD_CER, user_schede_rifiuti.descrizione ASC,user_movimenti_fiscalizzati.TIPO ASC, user_movimenti_fiscalizzati.NMOV ASC ";
		$order =  "ORDER BY user_schede_rifiuti.pericoloso DESC,lov_cer.COD_CER, user_schede_rifiuti.descrizione ASC ";
		break;

	case "VerificaCarichi":

		$PrintImpianto = false;
		$PrintCER = false;
		$SkipStandardNoRecs = false;
		$sql = "SELECT 0 AS isGiacenzaIniziale,DTMOV,NMOV,quantita,".$MovTable.".originalID_RIF AS RifID,TIPO";
		$sql .= ",user_schede_rifiuti.ID_CER as CERref,user_schede_rifiuti.pericoloso AS PER,user_schede_rifiuti.cod_pro,user_schede_rifiuti.adr";
		$sql .= ",user_schede_rifiuti.descrizione,lov_cer.COD_CER,user_schede_rifiuti.giac_ini,peso_spec,user_schede_rifiuti.ID_UMIS";
		$sql .= ",H1,H2,H3A,H3B,H4,H5,H6,H7,H8,H9,H10,H11,H12,H13,H14,H15";
		$sql .= ",HP1,HP2,HP3,HP4,HP5,HP6,HP7,HP8,HP9,HP10,HP11,HP12,HP13,HP14,HP15";
		$sql .= ",lov_misure.description AS UnMis";
		$sql .= " FROM ".$MovTable;
		$sql .= " JOIN user_schede_rifiuti ON ".$MovTable.".ID_RIF=user_schede_rifiuti.ID_RIF";
		$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
		$sql .= " JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS";
		$sql .=" WHERE ".$MovTable.".ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";
		$sql.= " AND ".$MovTable.".PerRiclassificazione=0 ";

		$sql2  = "SELECT 1 AS isGiacenzaIniziale,DTMOV,NMOV,quantita,user_movimenti_giacenze_iniziali.ID_RIF AS RifID,TIPO";
		$sql2 .= ",user_schede_rifiuti.ID_CER as CERref,user_schede_rifiuti.pericoloso AS PER,user_schede_rifiuti.cod_pro,user_schede_rifiuti.adr";
		$sql2 .= ",user_schede_rifiuti.descrizione,lov_cer.COD_CER,user_schede_rifiuti.giac_ini,peso_spec,user_schede_rifiuti.ID_UMIS";
		$sql2 .= ",H1,H2,H3A,H3B,H4,H5,H6,H7,H8,H9,H10,H11,H12,H13,H14,H15";
		$sql2 .= ",HP1,HP2,HP3,HP4,HP5,HP6,HP7,HP8,HP9,HP10,HP11,HP12,HP13,HP14,HP15";
		$sql2 .= ",lov_misure.description AS UnMis";
		$sql2 .= " FROM user_movimenti_giacenze_iniziali";
		$sql2 .= " JOIN user_schede_rifiuti ON user_movimenti_giacenze_iniziali.ID_RIF=user_schede_rifiuti.ID_RIF";
		$sql2 .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
		$sql2 .= " JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS";
		$sql2 .= " WHERE user_movimenti_giacenze_iniziali.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";

		#
		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		//$order = "ORDER BY user_schede_rifiuti.originalID_RIF asc, ".$MovTable.".NMOV ASC, user_schede_rifiuti.pericoloso DESC,lov_cer.COD_CER, user_schede_rifiuti.descrizione ASC, ".$MovTable.".TIPO ASC ";
		break;


	case "RecSmalt":

		$PrintImpianto = false;
		$PrintCER = false;
		$SkipStandardNoRecs = true;
		$sql = "SELECT quantita,tara,pesoL,NMOV,DTMOV,NFORM";
		$sql .= ",lov_misure.description AS UnMis,COD_CER,descrizione";
		$sql .= ",lov_stato_fisico.description AS SF";
		$sql.= ",lov_operazioni_rs.description AS operazione";
		$sql .= " FROM ".$MovTable;
		$sql .= " JOIN user_schede_rifiuti ON ".$MovTable.".ID_RIF=user_schede_rifiuti.ID_RIF";
		$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
		$sql .= " JOIN lov_stato_fisico ON user_schede_rifiuti.ID_SF=lov_stato_fisico.ID_SF";
		$sql .= " LEFT JOIN lov_operazioni_rs ON ".$MovTable.".ID_OP_RS=lov_operazioni_rs.ID_OP_RS";
		$sql .= " JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS";
		$sql .=" WHERE ".$MovTable.".ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		$sql .= " AND TIPO='S'";
		$sql .= " AND  (".$MovTable.".ID_OP_RS='0' OR ISNULL(".$MovTable.".ID_OP_RS)) ";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";
		#
		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		$order =  " ORDER BY NMOV ASC ";
	break;

	case "DistCER":

		$PrintImpianto = false;
		$PrintCER = true;
		$SkipStandardNoRecs = false;
		$sql = "SELECT COD_CER, SUM( IF( PS_DESTINO >0, PS_DESTINO, pesoN ) ) AS Totale, lov_cer.description AS CERdes";
		$sql .= " FROM ".$MovTable;
		$sql .= " JOIN user_schede_rifiuti ON ".$MovTable.".ID_RIF=user_schede_rifiuti.ID_RIF";
		$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
		$sql .=" WHERE ".$MovTable.".ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";
		#
		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		//$sql .= " AND TIPO='C'";
		$group = " GROUP BY user_schede_rifiuti.ID_RIF ";
		$order = " ORDER BY COD_CER ASC ";
		break; //}}}

	case "DistCERSCAR":
//{{{
		$PrintImpianto = false;
		$PrintCER = true;
		$SkipStandardNoRecs = false;
		$sql = "SELECT COD_CER, SUM(pesoN) AS Totale, lov_cer.description AS CERdes";
		$sql .= " FROM ".$MovTable."";
		$sql .= " JOIN user_schede_rifiuti ON ".$MovTable.".ID_RIF=user_schede_rifiuti.ID_RIF";
		$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
		$sql .=" WHERE ".$MovTable.".ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";
		#
		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		//$sql .= " AND TIPO='S'";
		$group = " GROUP BY user_schede_rifiuti.ID_RIF ";
		$order = " ORDER BY COD_CER ASC";
		break; //}}}
	break;
	case "CERgraph":
//{{{
		$PrintImpianto = false;
		$PrintCER = false;
		$SkipStandardNoRecs = false;
		$sql = "SELECT pesoN,NMOV,DTMOV";
		$sql .= ",user_schede_rifiuti.ID_RIF,COD_CER,descrizione";
		$sql .= " FROM ".$MovTable;
		$sql .= " JOIN user_schede_rifiuti ON ".$MovTable.".ID_RIF=user_schede_rifiuti.ID_RIF";
		$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
		$sql .=" WHERE ".$MovTable.".ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";
		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		$sql .= " AND TIPO='S'";
		$order =  "ORDER BY lov_cer.COD_CER, user_schede_rifiuti.ID_RIF ASC,".$MovTable.".NMOV ASC ";
		break; //}}}

	case "SoggMOV":
//{{{
		$PrintImpianto = true;
		$PrintCER = false;
		$SkipStandardNoRecs = false;
		$sql = "SELECT NMOV,NFORM,DTMOV,DTFORM,NFORM,TIPO,quantita,PS_DESTINO,lov_operazioni_rs.description AS RD";
		$sql .= ",user_schede_rifiuti.descrizione,lov_cer.COD_CER,lov_misure.description AS UnMis";
		$sql .= ",user_aziende_produttori.description AS PRODdes";
		$sql .= ",user_impianti_produttori.description AS PRODimpdes";
		$sql .= ",PRODIMPcomTable.description AS PRODIMPcom, PRODIMPcomTable.shdes_prov AS PRODIMPprov";
		$sql .= ",user_aziende_destinatari.description AS DESTdes";
		$sql .= ",user_impianti_destinatari.description AS DESTimpdes";
		$sql .= ",DESTIMPcomTable.description AS DESTIMPcom, DESTIMPcomTable.shdes_prov AS DESTIMPprov";
		$sql .= ",user_aziende_trasportatori.description AS TRASPdes";
		$sql .= ",user_impianti_trasportatori.description AS TRASPimpdes";
		$sql .= ",TRASPIMPcomTable.description AS TRASPIMPcom, TRASPIMPcomTable.shdes_prov AS TRASPIMPprov";
		$sql .= ",user_aziende_intermediari.description AS INTdes, user_aziende_intermediari.indirizzo AS INTind";
		$sql .= ",INTcomTable.description AS INTcom, INTcomTable.shdes_prov AS INTprov";
		$sql .= " FROM ".$MovTable;
		$sql .= " JOIN user_schede_rifiuti ON ".$MovTable.".ID_RIF=user_schede_rifiuti.ID_RIF";
		$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
		$sql .= " JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS";
		$sql .= " JOIN user_aziende_produttori ON user_aziende_produttori.ID_AZP=".$MovTable.".ID_AZP";
		$sql .= " JOIN user_impianti_produttori ON user_impianti_produttori.ID_UIMP=".$MovTable.".ID_UIMP";
		$sql .= " JOIN lov_comuni_istat AS PRODIMPcomTable ON user_impianti_produttori.ID_COM=PRODIMPcomTable.ID_COM";
		$sql .= " LEFT JOIN user_aziende_destinatari ON ".$MovTable.".ID_AZD=user_aziende_destinatari.ID_AZD";
		$sql .= " LEFT JOIN user_impianti_destinatari ON ".$MovTable.".ID_UIMD=user_impianti_destinatari.ID_UIMD";
		$sql .= " LEFT JOIN lov_comuni_istat AS DESTIMPcomTable ON user_impianti_destinatari.ID_COM=DESTIMPcomTable.ID_COM";
		$sql .= " LEFT JOIN user_aziende_trasportatori ON ".$MovTable.".ID_AZT=user_aziende_trasportatori.ID_AZT";
		$sql .= " LEFT JOIN user_impianti_trasportatori ON ".$MovTable.".ID_UIMT=user_impianti_trasportatori.ID_UIMT";
		$sql .= " LEFT JOIN lov_comuni_istat AS TRASPIMPcomTable ON user_impianti_trasportatori.ID_COM=TRASPIMPcomTable.ID_COM";
		$sql .= " LEFT JOIN user_aziende_intermediari ON ".$MovTable.".ID_AZI=user_aziende_intermediari.ID_AZI";
		$sql .= " LEFT JOIN lov_comuni_istat AS INTcomTable ON user_aziende_intermediari.ID_COM=INTcomTable.ID_COM";
		$sql .= " LEFT JOIN lov_operazioni_rs ON ".$MovTable.".ID_OP_RS=lov_operazioni_rs.ID_OP_RS";
		$sql .= " WHERE ".$MovTable.".ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";
		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		$order =  " ORDER BY ".$MovTable.".NMOV ASC "; //}}}
	break;

	case "Autisti":
		$PrintImpianto = true;
		$PrintCER = false;
		$SkipStandardNoRecs = false;


		$sql = "SELECT";

		$sql.= " user_autisti.nome AS NOME, user_autisti.description AS COGNOME, user_autisti.patente AS PATENTE,";
		$sql.= " user_autisti.rilascio AS RILASCIO, user_autisti.scadenza AS SCADENZA, user_autisti.ID_UIMT,";
		$sql.= " user_autisti.adr AS ADR, user_aziende_trasportatori.description AS AZIENDA";
		$sql.= " FROM user_autisti";
		$sql.= " JOIN user_impianti_trasportatori ON user_autisti.ID_UIMT=user_impianti_trasportatori.ID_UIMT ";
		$sql.= " JOIN user_aziende_trasportatori ON user_impianti_trasportatori.ID_AZT=user_aziende_trasportatori.ID_AZT ";

		$order =" ORDER BY AZIENDA asc, COGNOME asc, NOME asc ";

		$sql .= " WHERE user_aziende_trasportatori.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";

		$TableSpec = "user_aziende_trasportatori.";
		require("../__includes/SOGER_DirectProfilo.php");

		break;


	case "PesiNonConformi":
		$PrintImpianto = true;
		$PrintCER = true;
		$SkipStandardNoRecs = false;


		$sql = "SELECT NMOV,DTMOV,TIPO,quantita,PS_DESTINO,NFORM";
		$sql .= ",user_movimenti_fiscalizzati.$AzKey AS AZref,user_schede_rifiuti.ID_RIF as CERref,user_movimenti_fiscalizzati.$ImpKey AS IMPref, user_movimenti_fiscalizzati.VER_DESTINO,user_movimenti_fiscalizzati.FKEpesospecifico as peso_spec";
		$sql .= ",user_schede_rifiuti.descrizione,user_schede_rifiuti.MaxVar,lov_cer.COD_CER,user_schede_rifiuti.giac_ini";
		$sql .= ",user_movimenti_fiscalizzati.adr, lov_misure.description AS UnMis";
		$sql .= ",$AzTable.description AS AZdes,$AzTable.indirizzo AS AZind";
		$sql .= ",AZcomTable.description AS AZcom,AZcomTable.des_prov AS AZprov,AZcomTable.shdes_prov AS AZshprov,AZcomTable.nazione AS AZnaz";
		$sql .= ",$ImpTable.description AS IMPdes";
		$sql .= ",IMPcomTable.description AS IMPcom,IMPcomTable.des_prov AS IMPprov,IMPcomTable.shdes_prov AS IMPshprov,IMPcomTable.nazione AS IMPnaz";
		$sql .= " FROM user_movimenti_fiscalizzati";
		$sql .= " JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF";
		$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
		$sql .= " JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS";
		$sql .= " JOIN $AzTable ON $AzTable.$AzKey=user_movimenti_fiscalizzati.$AzKey";
		$sql .= " JOIN $ImpTable ON $ImpTable.$ImpKey=user_movimenti_fiscalizzati.$ImpKey";
		$sql .= " JOIN lov_comuni_istat AS AZcomTable ON $AzTable.ID_COM=AZcomTable.ID_COM";
		$sql .= " JOIN lov_comuni_istat AS IMPcomTable ON $ImpTable.ID_COM=IMPcomTable.ID_COM";
		$sql .= " WHERE user_movimenti_fiscalizzati.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		$sql .= " AND user_movimenti_fiscalizzati.TIPO='S' ";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";
		$TableSpec = "user_movimenti_fiscalizzati.";
		require("../__includes/SOGER_DirectProfilo.php");
		$order =  " ORDER BY $AzTable.description, user_movimenti_fiscalizzati.$AzKey,user_movimenti_fiscalizzati.$ImpKey,lov_cer.COD_CER,user_movimenti_fiscalizzati.ID_RIF,user_movimenti_fiscalizzati.NMOV";
		break;

	case "BilancioECO":
		$PrintImpianto = true;
		$PrintCER = true;
		$SkipStandardNoRecs = false;


		$sql = "SELECT lov_cer.COD_CER, user_schede_rifiuti.descrizione AS rifiuto, ".$MovTable.".quantita AS quantita, ";
		$sql.= "lov_tipo_trattamento.description AS trattamento, user_aziende_destinatari.description AS destinatario, ";
		$sql.= "lov_operazioni_rs.description as RS ";
		$sql.= "FROM ".$MovTable." ";
		$sql.= "LEFT JOIN user_autorizzazioni_dest ON ".$MovTable.".ID_AUTHD=user_autorizzazioni_dest.ID_AUTHD ";
		$sql.= "LEFT JOIN lov_tipo_trattamento ON ".$MovTable.".ID_TRAT=lov_tipo_trattamento.ID_TRAT ";
		$sql.= "LEFT JOIN lov_operazioni_rs ON ".$MovTable.".ID_OP_RS=lov_operazioni_rs.ID_OP_RS ";
		$sql.= "JOIN user_schede_rifiuti ON ".$MovTable.".ID_RIF=user_schede_rifiuti.ID_RIF ";
		$sql.= "JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
		$sql.= "JOIN user_aziende_destinatari ON ".$MovTable.".ID_AZD=user_aziende_destinatari.ID_AZD ";
		$sql.= "WHERE ";
		$sql.= "".$MovTable.".TIPO='S' ";
		$sql.= "AND ".$MovTable.".ID_IMP='".$SOGER->UserData["core_impiantiID_IMP"]."'";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";

		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		$order =  " ORDER BY trattamento, RS, lov_cer.COD_CER, rifiuto, destinatario, lov_cer.COD_CER";
		break;

	case "BilancioRD":
		$PrintImpianto = true;
		$PrintCER = true;
		$SkipStandardNoRecs = false;

		$sql = "SELECT lov_cer.COD_CER, user_schede_rifiuti.descrizione AS rifiuto, ".$MovTable.".pesoN AS quantita, IF( ".$MovTable.".PS_DESTINO >0, ".$MovTable.".PS_DESTINO, ".$MovTable.".pesoN) AS quantitaDestino, ";
		$sql.= "lov_operazioni_rs.description AS operazione, lov_operazioni_rs.longdes AS operazione_des, user_aziende_destinatari.description AS destinatario ";
		$sql.= "FROM ".$MovTable." ";
		//$sql.= "JOIN user_autorizzazioni_dest ON ".$MovTable.".ID_AUTHD=user_autorizzazioni_dest.ID_AUTHD ";
		$sql.= "LEFT JOIN lov_operazioni_rs ON ".$MovTable.".ID_OP_RS=lov_operazioni_rs.ID_OP_RS ";
		$sql.= "JOIN user_schede_rifiuti ON ".$MovTable.".ID_RIF=user_schede_rifiuti.ID_RIF ";
		$sql.= "JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
		$sql.= "JOIN user_aziende_destinatari ON ".$MovTable.".ID_AZD=user_aziende_destinatari.ID_AZD ";
		$sql.= "WHERE ".$MovTable.".TIPO='S' ";
		$sql.= "AND ".$MovTable.".ID_IMP='".$SOGER->UserData["core_impiantiID_IMP"]."'";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";

		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		$order =  " ORDER BY operazione, lov_cer.COD_CER, rifiuto, destinatario, lov_cer.COD_CER";
		break;

	case "TraspIncoerenti":
		$PrintImpianto = true;
		$PrintCER = true;
		$SkipStandardNoRecs = false;

		$sql = "SELECT ".$MovTable.".NMOV, ".$MovTable.".TIPO, ".$MovTable.".NFORM, ".$MovTable.".DTFORM, ".$MovTable.".FKEcfiscD AS CF_D, ";
		$sql.= "".$MovTable.".FKEcfiscT AS CF_T, user_aziende_trasportatori.description AS trasportatore, ";
		$sql.= "user_aziende_trasportatori.NumAlboAutotraspProprio AS proprio, user_aziende_trasportatori.NumAlboAutotrasp as terzi, ";
		$sql.= "user_automezzi.description AS AUTO, user_automezzi.ID_LIM AS LIM_AUTO, user_rimorchi.description AS RMK, user_rimorchi.ID_LIM AS LIM_RMK ";

		$sql.= "FROM ".$MovTable." ";

		$sql.= "JOIN user_aziende_trasportatori ON ".$MovTable.".ID_AZT=user_aziende_trasportatori.ID_AZT ";
		$sql.= "LEFT JOIN user_automezzi ON ".$MovTable.".ID_AUTO=user_automezzi.ID_AUTO ";
		$sql.= "LEFT JOIN user_rimorchi ON ".$MovTable.".ID_RMK=user_rimorchi.ID_RMK ";

		//$sql.= "WHERE ".$MovTable.".FKEcfiscD<>'' AND ".$MovTable.".FKEcfiscT<>'' AND ".$MovTable.".collettame='0' ";
		$sql.= "WHERE ".$MovTable.".FKEcfiscD<>'' AND ".$MovTable.".FKEcfiscT<>'' ";

		$sql.= "AND ".$MovTable.".ID_IMP='".$SOGER->UserData["core_impiantiID_IMP"]."'";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";

		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		$order =  " ORDER BY ".$MovTable.".NMOV ASC";
		break;

	case "CostoViaggi":
		$PrintImpianto = true;
		$PrintCER = false;
		$SkipStandardNoRecs = false;
		$sql = "SELECT ".$IDMov.",NMOV,DTMOV,TIPO,quantita,tara,pesoL as lordo,NFORM, ".$MovTable.".ID_VIAGGIO, user_viaggi.durata, user_viaggi.distanza, user_viaggi.costo_calcolato";
		$sql .= ",".$MovTable.".$AzKey AS AZref,user_schede_rifiuti.ID_RIF as CERref,".$MovTable.".$ImpKey AS IMPref";
		$sql .= ",user_schede_rifiuti.descrizione,lov_cer.COD_CER,user_schede_rifiuti.giac_ini";
		$sql .= ",lov_misure.description AS UnMis";
		$sql .= ",$AzTable.description AS AZdes,$AzTable.indirizzo AS AZind";
		$sql .= ",AZcomTable.description AS AZcom,AZcomTable.des_prov AS AZprov,AZcomTable.shdes_prov AS AZshprov,AZcomTable.nazione AS AZnaz";
		$sql .= ",$ImpTable.description AS IMPdes";
		$sql .= ",IMPcomTable.description AS IMPcom,IMPcomTable.des_prov AS IMPprov,IMPcomTable.shdes_prov AS IMPshprov,IMPcomTable.nazione AS IMPnaz";
		$sql .= " FROM ".$MovTable;
		$sql .= " JOIN user_schede_rifiuti ON ".$MovTable.".ID_RIF=user_schede_rifiuti.ID_RIF";
		$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
		$sql .= " JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS";
		$sql .= " JOIN $AzTable ON $AzTable.$AzKey=".$MovTable.".$AzKey";
		$sql .= " JOIN $ImpTable ON $ImpTable.$ImpKey=".$MovTable.".$ImpKey";
		$sql .= " JOIN lov_comuni_istat AS AZcomTable ON $AzTable.ID_COM=AZcomTable.ID_COM";
		$sql .= " JOIN lov_comuni_istat AS IMPcomTable ON $ImpTable.ID_COM=IMPcomTable.ID_COM";
		$sql .= " JOIN user_viaggi ON ".$MovTable.".ID_VIAGGIO=user_viaggi.ID_VIAGGIO ";
		$sql .= " WHERE ".$MovTable.".ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";
		#
		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		//$order =  " ORDER BY ".$MovTable.".$AzKey,".$MovTable.".$ImpKey,".$MovTable.".ID_RIF,".$MovTable.".NMOV";
		$order =  " ORDER BY $AzTable.description, ".$MovTable.".$AzKey,".$MovTable.".$ImpKey,lov_cer.COD_CER,".$MovTable.".ID_RIF,".$MovTable.".NMOV";
	break;

	case "IVcopie":
		$PrintImpianto = true;
		$PrintCER = false;
		$SkipStandardNoRecs = false;
		$sql="SELECT NMOV, IF(`dt_in_trasp` IS NOT NULL, `dt_in_trasp`, `DTMOV`) AS DTFORM, NFORM, quantita, FKEUMIS, COD_CER, descrizione, user_aziende_destinatari.description AS destinatario FROM user_movimenti_fiscalizzati JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER JOIN user_aziende_destinatari ON user_movimenti_fiscalizzati.ID_AZD=user_aziende_destinatari.ID_AZD WHERE (DT_FORM IS NULL OR DT_FORM='0000-00-00') AND TIPO='S' AND user_movimenti_fiscalizzati.ID_AZD<>0 AND user_movimenti_fiscalizzati.ID_AZT<>0 AND TRIM(NFORM)<>'' AND user_movimenti_fiscalizzati.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		#
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";
		$TableSpec = "user_movimenti_fiscalizzati.";
		require("../__includes/SOGER_DirectProfilo.php");
		$order =  " ORDER BY IF(`dt_in_trasp` IS NOT NULL, `dt_in_trasp`, `DTMOV`) ASC, NMOV ASC";
	break;

	case "IVcopieDaRestituire":
		$PrintImpianto = true;
		$PrintCER = false;
		$SkipStandardNoRecs = false;
		$sql="SELECT DTFORM, NFORM, user_aziende_produttori.description AS PROD_RAGSOC, user_movimenti_fiscalizzati.ID_UIMP, user_impianti_produttori.description AS PROD_INDIRIZZO1, CONCAT(lov_comuni_istat.cap, ' - ', lov_comuni_istat.description, ' (', lov_comuni_istat.shdes_prov, ')') AS PROD_INDIRIZZO2 FROM user_movimenti_fiscalizzati JOIN user_aziende_produttori ON user_movimenti_fiscalizzati.ID_AZP=user_aziende_produttori.ID_AZP JOIN user_impianti_produttori ON user_impianti_produttori.ID_UIMP=user_movimenti_fiscalizzati.ID_UIMP JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM=user_impianti_produttori.ID_COM WHERE (DT_FORM IS NULL OR DT_FORM='0000-00-00') AND TIPO='C' AND user_movimenti_fiscalizzati.ID_AZP<>0 AND user_movimenti_fiscalizzati.ID_AZT<>0 AND TRIM(NFORM)<>'' AND user_movimenti_fiscalizzati.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		#
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";
		$TableSpec = "user_movimenti_fiscalizzati.";
		require("../__includes/SOGER_DirectProfilo.php");
		$order =  " ORDER BY user_aziende_produttori.description, user_movimenti_fiscalizzati.ID_UIMP, user_movimenti_fiscalizzati.DTFORM ASC, NMOV ASC";
	break;

        case "DurataViaggi":
		$PrintImpianto = true;
		$PrintCER = true;
		$SkipStandardNoRecs = false;
		$sql="SELECT NMOV, NFORM, quantita, FKEUMIS, COD_CER, descrizione, dt_in_trasp, hr_in_trasp, DEST_ARRIVO, user_movimenti_fiscalizzati.ID_AZT, user_aziende_trasportatori.description AS trasportatore FROM user_movimenti_fiscalizzati JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER JOIN user_aziende_trasportatori ON user_movimenti_fiscalizzati.ID_AZT=user_aziende_trasportatori.ID_AZT WHERE TIPO='S' AND user_movimenti_fiscalizzati.ID_AZT<>0 AND TRIM(NFORM)<>'' AND user_movimenti_fiscalizzati.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		#
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";
		$TableSpec = "user_movimenti_fiscalizzati.";
		require("../__includes/SOGER_DirectProfilo.php");
		$order =  " ORDER BY user_movimenti_fiscalizzati.ID_UIMT, user_movimenti_fiscalizzati.DTFORM ASC, NMOV ASC";
	break;

	case "PesoMezzi":
		$PrintImpianto = true;
		$PrintCER = false;
		$SkipStandardNoRecs = false;
		$sql="SELECT NFORM, DTFORM, lov_cer.COD_CER, user_schede_rifiuti.descrizione, tara, pesoN, pesoL, user_aziende_trasportatori.ID_AZT, user_aziende_trasportatori.description AS trasportatore, user_automezzi.description AS automezzo, user_rimorchi.description AS rimorchio FROM user_movimenti_fiscalizzati JOIN user_aziende_trasportatori ON user_movimenti_fiscalizzati.ID_AZT=user_aziende_trasportatori.ID_AZT JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER LEFT JOIN user_automezzi ON user_movimenti_fiscalizzati.ID_AUTO=user_automezzi.ID_AUTO LEFT JOIN user_rimorchi ON user_movimenti_fiscalizzati.ID_RMK=user_rimorchi.ID_RMK WHERE  user_movimenti_fiscalizzati.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";
		#
		$TableSpec = "user_movimenti_fiscalizzati.";
		require("../__includes/SOGER_DirectProfilo.php");
		$order = " ORDER BY trasportatore, automezzo, rimorchio, lov_cer.COD_CER, user_schede_rifiuti.descrizione, DTFORM";
	break;

	case "DestinazioneRifiuti":
		$PrintImpianto = true;
		$PrintCER = true;
		$SkipStandardNoRecs = false;

		$sql = "SELECT lov_cer.COD_CER, user_schede_rifiuti.descrizione AS rifiuto, ".$MovTable.".quantita AS quantita, IF(PS_DESTINO>0, PS_DESTINO, pesoN) AS destino, ";
		$sql.= "lov_operazioni_rs.description AS operazione, lov_operazioni_rs.longdes AS operazione_des, user_aziende_destinatari.description AS destinatario ";
		$sql.= "FROM ".$MovTable." ";
		$sql.= "JOIN user_autorizzazioni_dest ON ".$MovTable.".ID_AUTHD=user_autorizzazioni_dest.ID_AUTHD ";
		$sql.= "LEFT JOIN lov_operazioni_rs ON ".$MovTable.".ID_OP_RS=lov_operazioni_rs.ID_OP_RS ";
		$sql.= "JOIN user_schede_rifiuti ON ".$MovTable.".ID_RIF=user_schede_rifiuti.ID_RIF ";
		$sql.= "JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
		$sql.= "JOIN user_aziende_destinatari ON ".$MovTable.".ID_AZD=user_aziende_destinatari.ID_AZD ";
		$sql.= "WHERE ".$MovTable.".TIPO='S' ";
		$sql.= "AND ".$MovTable.".ID_IMP='".$SOGER->UserData["core_impiantiID_IMP"]."'";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 ";

		$TableSpec = $MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		$order =  " ORDER BY operazione, lov_cer.COD_CER, rifiuto, destinatario, lov_cer.COD_CER";
		break;


	case "OrigineDatiD":
	case "OrigineDatiT":
		$PrintImpianto = true;
		$PrintCER = false;
		$SkipStandardNoRecs = false;
		$sql = "SELECT $AzTable.$AzKey as AzID, $AzTable.description AS azienda, $AzTable.indirizzo as indirizzoSL, ";
		$sql.= "lov_comuni_istat.description as comuneSL, lov_comuni_istat.CAP as CAPSL, lov_comuni_istat.shdes_prov as provSL, ";
		$sql.= "$AuthTable.$AuthKey AS ID_AUTH, $AuthTable.num_aut, lov_origine_dati_auth.description AS origine_dati, $AuthTable.ID_ORIGINE_DATI, lov_cer.COD_CER, user_schede_rifiuti.descrizione ";
		$sql.= "FROM $AuthTable ";
		$sql.= "JOIN $ImpTable ON $AuthTable.$ImpKey=$ImpTable.$ImpKey ";
		$sql.= "JOIN $AzTable ON $ImpTable.$AzKey=$AzTable.$AzKey ";
		$sql.= "JOIN user_schede_rifiuti ON $AuthTable.ID_RIF=user_schede_rifiuti.ID_RIF ";
		$sql.= "JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
		$sql.= "JOIN lov_comuni_istat ON $AzTable.ID_COM=lov_comuni_istat.ID_COM ";
		$sql.= "JOIN lov_origine_dati_auth ON $AuthTable.ID_ORIGINE_DATI=lov_origine_dati_auth.ID_ORIGINE_DATI ";
		$TableSpec = $AzTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		$order = " ORDER BY azienda, origine_dati, $AuthTable.num_aut, lov_cer.COD_CER, user_schede_rifiuti.descrizione ASC";
		break;

	case "ControlloDeposito":
		$PrintImpianto = true;
		$PrintCER = false;
		$SkipStandardNoRecs = true;

		$sql  = "SELECT 0 AS isGiacenzaIniziale,DTMOV,NMOV,quantita,".$MovTable.".originalID_RIF AS RifID,TIPO, ";
		$sql .= " user_schede_rifiuti.ID_CER as CERref,user_schede_rifiuti.descrizione,user_schede_rifiuti.ID_RIFPROD_F,lov_cer.COD_CER,user_schede_rifiuti.giac_ini ";
		$sql .= " FROM ".$MovTable;
		$sql .= " JOIN user_schede_rifiuti ON ".$MovTable.".originalID_RIF=user_schede_rifiuti.ID_RIF";
		$sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
		$sql .= " WHERE ".$MovTable.".ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		// exclude NMOV=9999999
		$sql.= " AND ".$MovTable.".NMOV<>9999999 AND PerRiclassificazione=0 ";

		$sql2  = "SELECT 1 AS isGiacenzaIniziale,DTMOV,NMOV,quantita,user_movimenti_giacenze_iniziali.ID_RIF AS RifID,TIPO, ";
		$sql2 .= " user_schede_rifiuti.ID_CER as CERref,user_schede_rifiuti.descrizione,user_schede_rifiuti.ID_RIFPROD_F,lov_cer.COD_CER,user_schede_rifiuti.giac_ini ";
		$sql2 .= " FROM user_movimenti_giacenze_iniziali";
		$sql2 .= " JOIN user_schede_rifiuti ON user_movimenti_giacenze_iniziali.ID_RIF=user_schede_rifiuti.ID_RIF";
		$sql2 .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
		$sql2 .= " WHERE user_movimenti_giacenze_iniziali.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";

		#
		$TableSpec=$MovTable.".";
		require("../__includes/SOGER_DirectProfilo.php");
		//$order = " ORDER BY COD_CER, descrizione ASC, RifID ASC, TIPO ASC, NMOV ASC ";

		break;

}
#
#	FILTRI
#
#	RIFIUTI
//{{{
if(isset($_POST["RifIDs"])) {

	switch($_POST['Stat']){
		case "SoggCER":
			$sql .= " AND user_autorizzazioni_trasp.ID_RIF ";
				$tmpRif = " IN (";
				foreach($_POST["RifIDs"] as $k=>$ID_RIF) {
					$tmpRif .= "$ID_RIF,";
				}
				$tmpRif = substr($tmpRif,0,-1) . ")";
				$sql .= $tmpRif;
				$LinesCER = 0;
				if(!isset($_POST["SkipCER"])) {
					$ListaCER = GetCerName($tmpRif,$FEDIT,$LinesCER);
				}
			$sql2.= " AND user_autorizzazioni_dest.ID_RIF ";
				$tmpRif = " IN (";
				foreach($_POST["RifIDs"] as $k=>$ID_RIF) {
					$tmpRif .= "$ID_RIF,";
				}
				$tmpRif = substr($tmpRif,0,-1) . ")";
				$sql2.= $tmpRif;
				$LinesCER = 0;
				if(!isset($_POST["SkipCER"])) {
					$ListaCER = GetCerName($tmpRif,$FEDIT,$LinesCER);
				}
			$sql3.= " AND user_autorizzazioni_interm.ID_RIF ";
				$tmpRif = " IN (";
				foreach($_POST["RifIDs"] as $k=>$ID_RIF) {
					$tmpRif .= "$ID_RIF,";
				}
				$tmpRif = substr($tmpRif,0,-1) . ")";
				$sql3.= $tmpRif;
				$LinesCER = 0;
				if(!isset($_POST["SkipCER"])) {
					$ListaCER = GetCerName($tmpRif,$FEDIT,$LinesCER);
				}

#			print_r($sql);
#			print_r("<hr>");
#			print_r($sql2);
#			print_r("<hr>");
#			print_r($sql3);
			break;

		case "Saldo":
		case "Giacenze":
		case "GiacenzeAS":
		case "OrganizzazioneDeposito":
		case "PrevisioneConferimenti":
		case "ActualStock":
		case "ActualStockControl":
				$sql .= " AND user_schede_rifiuti.ID_RIF ";
				$tmpRif = " IN (";
				foreach($_POST["RifIDs"] as $k=>$ID_RIF) {
					$tmpRif .= "$ID_RIF,";
				}
				$tmpRif = substr($tmpRif,0,-1) . ")";
				$sql .= $tmpRif;
				$LinesCER = 0;
				if(!isset($_POST["SkipCER"])) {
					$ListaCER = GetCerName($tmpRif,$FEDIT,$LinesCER);
				}

			break;

		default:
			$id_rif_rule = " AND ".$MovTable.".ID_RIF ";
			$tmpRif = " IN (";
			foreach($_POST["RifIDs"] as $k=>$ID_RIF) {
				$tmpRif .= "$ID_RIF,";
			}
			$tmpRif = substr($tmpRif,0,-1) . ")";
			$id_rif_rule .= $tmpRif;
			$LinesCER = 0;
			if(!isset($_POST["SkipCER"])) {
				$ListaCER = GetCerName($tmpRif,$FEDIT,$LinesCER);
			}
			$sql.=$id_rif_rule;
			break;

	}




	//print_r($sql);

} //}}}
# 	AZIENDE
//{{{
if(isset($_POST["AzIDs"])) {
	switch($_POST['Stat']){

		case "OrigineDatiD":
		case "OrigineDatiT":
			$sql .= " AND $AzTable.$AzKey ";
			$tmpAz = "IN (";
			foreach($_POST["AzIDs"] as $k=>$ID_AZ) {
				$tmpAz .= "$ID_AZ,";
			}
			$tmpAz = substr($tmpAz,0,-1) . ")";
			$ListaAziende = GetAziendeName($AzTable,$AzKey,$tmpAz,$FEDIT);
			$sql .= $tmpAz;
			break;

		case "Autisti":
			$sql .= " AND user_aziende_trasportatori.$AzKey ";
			$tmpAz = "IN (";
			foreach($_POST["AzIDs"] as $k=>$ID_AZ) {
				$tmpAz .= "$ID_AZ,";
			}
			$tmpAz = substr($tmpAz,0,-1) . ")";
			$ListaAziende = GetAziendeName($AzTable,$AzKey,$tmpAz,$FEDIT);
			$sql .= $tmpAz;
			break;
		default:
			$sql .= " AND ".$MovTable.".$AzKey ";
			$tmpAz = "IN (";
			foreach($_POST["AzIDs"] as $k=>$ID_AZ) {
				$tmpAz .= "$ID_AZ,";
			}
			$tmpAz = substr($tmpAz,0,-1) . ")";
			$ListaAziende = GetAziendeName($AzTable,$AzKey,$tmpAz,$FEDIT);
			$sql .= $tmpAz;
			break;
		}
} //}}}
# 	DATA MOVIMENTI
//{{{
if(isset($_POST["MovDa"]) && $_POST["MovDa"]!="") {
	$RawDa = explode("/",$_POST["MovDa"]);
	$Da = $RawDa[2] . "-" . $RawDa[1] . "-" . $RawDa[0];
	$sql .= " AND (DTMOV>='" . $Da . "') ";
} //}}}

if(isset($_POST["MovA"]) && $_POST["MovA"]!="") {
	$RawA = explode("/",$_POST["MovA"]);
	$A = $RawA[2] . "-" . $RawA[1] . "-" . $RawA[0];
	$sql .= " AND (DTMOV<='" . $A . "') ";
} //}}}


# TIPO MOVIMENTI
if(isset($_POST['cs'])){
	$MovType=Array();
	// exclusion or selection?
	if(isset($_POST['MovType'])){ // exclusion
		$sql .=" AND ".$MovTable.".TIPO='".$_POST['MovType']."' ";
		}
	else{
		if(isset($_POST['filtra_carichi']))
			$MovType[]=$_POST['filtra_carichi'];
		if(isset($_POST['filtra_scarichi']))
			$MovType[]=$_POST['filtra_scarichi'];
		$MovTypeIn="";
		for($t=0;$t<count($MovType);$t++)
			$MovTypeIn.="'".$MovType[$t]."',";
		$MovTypeIn=substr($MovTypeIn,0,-1);
		$sql .=" AND ".$MovTable.".TIPO IN (".$MovTypeIn.")";
		}
	}



##############################################
#
#	dati impianti per intestazione statistica ( leggo prima di dati per report! )
$queryImp = "SELECT indirizzo, lov_comuni_istat.description AS comune, shdes_prov FROM core_impianti JOIN lov_comuni_istat ON ";
$queryImp.= "lov_comuni_istat.ID_COM = core_impianti.ID_COM ";
$queryImp.= "WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."';";
$FEDIT->SdbRead($queryImp,"DbRecordSetImpianto",true,false);

$impianto = $FEDIT->DbRecordSetImpianto[0]['indirizzo']." - ".$FEDIT->DbRecordSetImpianto[0]['comune']." (".$FEDIT->DbRecordSetImpianto[0]['shdes_prov'].")";
##############################################


#
#	AGGIUNGE ORDINAMENTO ED ESEGUE LA QUERY
#

## GROUP BY
$sql .= $group;

## ORDER BY
$sql .= $order;

if($_POST['Stat']=="SoggCER"){
	$sql2.=$order2;
	$sql3.=$order3;
	}

## EXCEPTION FOR STATS USING DAY BTWEEN GIAC_INI "AUTO" AND TODAY
if($_POST['Stat']=="ControlloDeposito"){
	$sql2.=" AND user_movimenti_giacenze_iniziali.workmode='".$SOGER->UserData['workmode']."' ";
	$sql2.=str_replace("user_movimenti_fiscalizzati", "user_movimenti_giacenze_iniziali",$id_rif_rule);
	$sqlUnion = "(".$sql.") UNION (".$sql2.")";
	$sqlUnion.= " ORDER BY COD_CER, descrizione ASC, RifID ASC, TIPO ASC, isGiacenzaIniziale DESC, NMOV ASC, DTMOV ASC ";
	$sql = $sqlUnion;
	}
if($_POST['Stat']=="VerificaCarichi"){
	$sql2.=" AND user_movimenti_giacenze_iniziali.workmode='".$SOGER->UserData['workmode']."' ";
	$sql2.=str_replace("user_movimenti_fiscalizzati", "user_movimenti_giacenze_iniziali",$id_rif_rule);
	$sqlUnion = "(".$sql.") UNION (".$sql2.")";
	$sqlUnion.= "ORDER BY RifID asc, isGiacenzaIniziale DESC, DTMOV ASC, NMOV ASC, PER DESC, COD_CER, descrizione ASC, TIPO ASC ";
	$sql = $sqlUnion;
	}

if($_POST['Stat']=="SoggCER"){
	$FEDIT->SdbRead($sql,"DbRecordSet",true,false);
	if(isset($FEDIT->DbRecordSet))
		$_SESSION['firstquery']=$FEDIT->DbRecordSet;
	$FEDIT->SdbRead($sql2,"DbRecordSet",true,false);
	if(isset($FEDIT->DbRecordSet))
		$_SESSION['secondquery']=$FEDIT->DbRecordSet;
	$FEDIT->SdbRead($sql3,"DbRecordSet",true,false);
	if(isset($FEDIT->DbRecordSet))
		$_SESSION['thirdquery']=$FEDIT->DbRecordSet;
	}
else
	$FEDIT->SdbRead($sql,"DbRecordSet",true,false);

//print_r($sql);

//die("service temporary unavailable");

#
#	CREA OGGETTO PDF
#
//{{{
if(!$asCSV) {
	$um="mm";
	$Format = array(210,297);
	$ZeroMargin = true;
	$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin,$DcName);
}

$Xpos=20;
$Ypos=0;

if($PrintIntestazione){

	#
	#	STAT TITLE
	#
	if(!$asCSV) {
		$Ypos+=10;
		$FEDIT->FGE_PdfBuffer->SetXY(0,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',14);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,15,$statTitle,0,"C");
		}
	else{
		if(!$_POST['Stat']=='CSVmovimentazione'){
			$CSVbuf = AddCSVcolumn($statTitle);
			$CSVbuf .= AddCSVRow();
			$CSVbuf .= AddCSVRow();
			}
		}


	#
	#	DESCRIZIONE STAT
	#
	if(!isset($statDesc)) $statDesc=" -- ";
	if(!$asCSV) {
		$Xpos=20;
		$Ypos+=15;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Descrizione:",0,"L");
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY(40,25);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$statDesc,0,"L");
	} else {
		if(!$_POST['Stat']=='CSVmovimentazione'){
			$CSVbuf .= AddCSVcolumn("Descrizione: $statDesc");
			$CSVbuf .= AddCSVRow();
			}
		}

	#
	# 	INTESTATARIO
	#
	$intestatario = strtolower($SOGER->UserData["core_impiantidescription"]) . " (" . $SOGER->UserData["workmode"] . ")";
	if(!$asCSV) {
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Intestatario:",0,"L");
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY(40,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$intestatario,0,"L");
	} else {
		if(!$_POST['Stat']=='CSVmovimentazione'){
			$CSVbuf .= AddCSVcolumn("Intestatario: $intestatario");
			$CSVbuf .= AddCSVRow();
			}
		}
	#
	# 	INDIRIZZO IMPIANTO
	#
	if(!$asCSV) {
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Impianto:",0,"L");
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY(40,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$impianto,0,"L");
	} else {
		if(!$_POST['Stat']=='CSVmovimentazione'){
			$CSVbuf .= AddCSVcolumn("Impianto: $impianto");
			$CSVbuf .= AddCSVRow();
			}
		}

	#
	#	DATA MOVIMENTI
	#
	switch($MovTable){
		case "user_movimenti":
			$registro="industriale";
			break;
		case "user_movimenti_fiscalizzati":
			$registro="fiscale";
			break;
		}

	$Movz ="Registro ".$registro.", ";

	if($_POST["MovDa"]!="") {
		$Movz.= "movimenti dal " . $_POST["MovDa"] . " al " . $_POST["MovA"];
		}
	else {
		$Movz.= "nessun filtro per data applicato";
		}

	if(!$asCSV) {
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Movimenti:",0,"L");
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY(40,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$Movz,0,"L");
	} else {
		if(!$_POST['Stat']=='CSVmovimentazione'){
			$CSVbuf .= AddCSVcolumn("Movimenti: $Movz");
			$CSVbuf .= AddCSVRow();
			}
		}

	#
	#	ELENCO AZIENDE
	#
	if(!isset($_POST["AzIDs"]))
		$ListaAziende = " -- ";

	if(!$asCSV && $_POST['Stat']!="CERgraph") {
		$charInLine		= 90; # ~ stimato ~
		$char			= strlen($ListaAziende);
		$AzLines		= floor($char/$charInLine);
			$Ypos+=7;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Aziende:",0,"L");
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetXY(40,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
			$FEDIT->FGE_PdfBuffer->MultiCell(150,4,$ListaAziende,0,"L");
			$Ypos+=$AzLines*4;
			$Ypos+=7;
		}

	#
	#	ELENCO CER
	#
	if($PrintCER) {
		if(!$asCSV) {
			//$Ypos+=15;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(0,5,"CER:",0,"L");
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetXY(40,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
			$FEDIT->FGE_PdfBuffer->MultiCell(150,4,$ListaCER,0,"L");
			$Ypos = 80+($LinesCER*4);
		}
	}


} // PrintIntestazione


#
#	CICLO STATISTICA - SHARED
#
if(!isset($Ypos)) {
	$Ypos = 40;
}
if($FEDIT->DbRecsNum==0 && !$SkipStandardNoRecs) {
	if(!$asCSV) {
		$Ypos += 20;
		$FEDIT->FGE_PdfBuffer->SetXY(0,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',11);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"nessun record",0,"C");
		PDFOut($FEDIT->FGE_PdfBuffer,$DcName);
	} else {
		$CSVbuf .= AddCSVRow();
		$CSVbuf .= AddCSVRow();
		$CSVbuf .= AddCSVcolumn("nessun record");
	}
}
$tmpAZ = "";
$tmpIMP = "";
$tmpCER = "";
#
#	INCLUDES SPECIFICI
#

switch($_POST["Stat"]) {
//{{{

	case "ECO_KG":
		include("STATS_costo_Kg.php");
		break;

	case "ECO_BUD":
		include("STATS_budget_spese.php");
		break;

	case "ECO_PRO":
	case "ECO_TRA":
	case "ECO_DES":
	case "ECO_INT":
		include("STATS_economiche.php");
	break;

	case "ECO_PRO_CAFC":
	case "ECO_TRA_CAFC":
	case "ECO_DES_CAFC":
	case "ECO_INT_CAFC":
		include("STATS_economiche_CAFC.php");
	break;

	case "ListaTargheViaggiAdr":
		include("STATS_ListaTargheViaggiAdr.php");
		break;

	case "ControlloPesiDestinatari":
		$TotCarico = 0;
		$TotScarico = 0;
		$TotPesoDestino = 0;
		include("STATS_ControlloPesiDestinatari.php");
	break;

	case "AnPRO":
	case "AnTRA":
	case "AnDES":
	case "AnINT":
		$TotCarico = 0;
		$TotScarico = 0;
		$TotPesoDestino = 0;
		$TotTara = 0;
		$TotLordo = 0;
		$TotCosto = 0;
		include("STATS_analitiche.php");
	break;
	case "ADC":
		include("STATS_ADC.php");
		break;
	case "ComuniConferitori":
		include("STATS_ComuniConferitori.php");
		break;
	case "CSVmovimentazione":
		include("STATS_CSVmovimentazione.php");
		break;
	case "GlPRO":
	case "GlDES":
	case "GlTRA":
	case "GlINT":
		include("STATS_globali.php");
	break;
	case "Giacenze":
		$TotCarico = 0;
		$TotScarico = 0;
		$TotPesoDestino = 0;
		include("STATS_giacenze.php");
	break;
	case "GiacenzeAS":
		$TotCarico = 0;
		$TotScarico = 0;
		$TotPesoDestino = 0;
		include("STATS_giacenze_aree_stoccaggio.php");
	break;
	case "Saldo":
		$TotCarico = 0;
		$TotScarico = 0;
		$TotPesoDestino = 0;
		include("STATS_saldo.php");
	break;
	case "RecSmalt":
		include("STATS_RecSmalt.php");
	break;
	case "DistCER":
	//case "DistCERSCAR":
		include("STATS_DistCER.php");
	break;
	case "PrevisioneConferimenti":
		include("STATS_PrevisioneConferimenti.php");
	break;
	case "OrganizzazioneDeposito":
		include("STATS_OrganizzazioneDeposito.php");
	break;
	case "ActualStock":
		include("STATS_ActualStock.php");
	break;
	case "ActualStockControl":
		include("STATS_ActualStockControl.php");
	break;
	case "VerificaCarichi":
		include("STATS_VerificaCarichi.php");
	break;
	case "CERgraph":
		include("STATS_CERgraph.php");
	break;
	case "SoggMOV":
		include("STATS_SoggMOV.php");
	break;
	case "SoggCER":
		include("STATS_SoggCER.php");
	break;
	case "Autisti":
		include("STATS_Autisti.php");
	break;
	case "PesiNonConformi":
		$TotCarico = 0;
		$TotScarico = 0;
		$TotPesoDestino = 0;
		include("STATS_PesiNonConformi.php");
	break;
	case "BilancioECO":
		include("STATS_BilancioECO.php");
	break;
	case "BilancioRD":
		include("STATS_BilancioRD.php");
	break;
	case "DestinazioneRifiuti":
		include("STATS_DestinazioneRifiuti.php");
	break;
	case "GraficoRifiuti":
		include("STATS_GraficoRifiuti.php");
	break;
	case "TraspIncoerenti":
		include("STATS_TraspIncoerenti.php");
	break;
	case "CostoViaggi":
		$TotScarico = 0;
		$TotKm = 0;
		$TotTime = 0;
		include("STATS_CostoViaggi.php");
	break;
	case "IVcopie":
		include("STATS_IVcopie.php");
		break;
	case "IVcopieDaRestituire":
		include("STATS_IVcopieDaRestituire.php");
		break;
        case "DurataViaggi":
		include("STATS_DurataViaggi.php");
		break;
	case "PesoMezzi":
		include("STATS_PesoMezzi.php");
		break;
	case "OrigineDatiD":
		include("STATS_OrigineDatiD.php");
		break;
	case "OrigineDatiT":
		include("STATS_OrigineDatiT.php");
	break;

	case "ControlloDeposito":
		$TotC = 0;
		$TotS = 0;
		$RIFbuf = "";
		$rifiuti = array();
		$CarichiRifiuto = array();
		include("STATS_ControlloDeposito.php");
	break;

	//}}}
}
#
#	PDF output
#

if(!$asCSV) {
	PDFOut($FEDIT->FGE_PdfBuffer,$DcName);
} else {
	CSVOut($CSVbuf,$DcName);
}

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>