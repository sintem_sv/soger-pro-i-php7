<?php
	//include("../__libs/SQLFunct.php");
	
	//print_r($FEDIT->DbRecordSet);	

	$Ypos+=20;

	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
	
	$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,15,"Cer","BTLR","C");
	
	$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(60,15,"Descrizione","BTLR","C");

	$FEDIT->FGE_PdfBuffer->SetXY(85,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,15,"Stato fisico","BTLR","C");

	$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(10,15,"N�","BTLR","C");

	$FEDIT->FGE_PdfBuffer->SetXY(115,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,15,"Contenitore","BTLR","C");

	$FEDIT->FGE_PdfBuffer->SetXY(145,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(18,7.5,"Carico automatico","BTLR","C");

	$FEDIT->FGE_PdfBuffer->SetXY(163,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(22,7.5,"Intervallo tra carichi","BTLR","C");

	$FEDIT->FGE_PdfBuffer->SetXY(185,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,7.5,"Massimo stoccabile","BTLR","C");

	$FEDIT->FGE_PdfBuffer->SetXY(205,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,7.5,"Giacenza attuale","BTLR","C");

	$FEDIT->FGE_PdfBuffer->SetXY(225,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,5,"Prod. media giornaliera stimata","BTLR","C");

	$FEDIT->FGE_PdfBuffer->SetXY(245,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,5,"Prod. media settimanale stimata","BTLR","C");

	$FEDIT->FGE_PdfBuffer->SetXY(265,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(20,7.5,"Scarico medio","BTLR","C");

	$Ypos +=5;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);

	foreach($FEDIT->DbRecordSet as $k=>$dati) {	

		$Ypos+=15;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

		
		$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,3,$dati["COD_CER"],0,"C");

		$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(60,3,$dati["descrizione"],0,"L");

		$FEDIT->FGE_PdfBuffer->SetXY(85,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,3,$dati["SF"],0,"L");
		
		$FEDIT->FGE_PdfBuffer->SetXY(105,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(10,3,$dati["NumCont"],0,"C");

		$FEDIT->FGE_PdfBuffer->SetXY(115,$Ypos);
		if($dati["contenitore"]=="") $dati["contenitore"]="non specificato";
		$FEDIT->FGE_PdfBuffer->MultiCell(30,3,$dati["contenitore"],0,"L");

		$FEDIT->FGE_PdfBuffer->SetXY(145,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(18,3,$dati["carico_automatico"]." ".$dati["UM"],0,"R");
	
		$FEDIT->FGE_PdfBuffer->SetXY(163,$Ypos);
		
		if($dati["ID_ICA"]=='1'){
			if($dati["intervallo"]==1) $value=" giorno"; else $value=" giorni";
			}
		else{
			if($dati["intervallo"]==1) $value=" settimana"; else $value=" settimane";
			}		
		$FEDIT->FGE_PdfBuffer->MultiCell(22,3,$dati["intervallo"].$value,0,"C");
	
		## STOCCAGGIO MASSIMO ( CONTEGGIANDO TUTTI I CONTENITORI )

		$FEDIT->FGE_PdfBuffer->SetXY(185,$Ypos);
		if($dati["MaxStock"]<=0){
			// MaxStock deve essere espresso nell' UM del rifiuto
			switch($dati["UM"]){
				case "Kg.":
					$MaxStock=number_format($dati["portata"] * $dati["NumCont"],0,'.','');
					break;
				case "Litri":
					$MaxStock=$dati["m_cubi"] * 1000 * $dati["NumCont"];
					break;
				case "Mc.":
					$MaxStock=$dati["m_cubi"] * $dati["NumCont"];
					break;
				}
			}
		else
			$MaxStock=$dati["MaxStock"];

		$MaxStock	= $MaxStock * $dati["NumCont"];
		
		$FEDIT->FGE_PdfBuffer->MultiCell(20,3,$MaxStock." ".$dati["UM"],0,"R");

		$FEDIT->FGE_PdfBuffer->SetXY(205,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,3,$dati["giacenza"]." kg",0,"R");

		$FEDIT->FGE_PdfBuffer->SetXY(225,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,3,$dati["media_gg_stima"]." kg",0,"R");

		$FEDIT->FGE_PdfBuffer->SetXY(245,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,3,$dati["media_settimanale_stima"]." kg",0,"R");

		$FEDIT->FGE_PdfBuffer->SetXY(265,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,3,$dati["scarico_medio"]." kg",0,"R");


	}

?>