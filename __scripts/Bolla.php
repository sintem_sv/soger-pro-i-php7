<?php
session_start();
require_once("ForgEdit_includes.inc");
require_once("Soger_includes.inc");
require_once("../__libs/SQLFunct.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");

global $SOGER;

//die(var_dump($SOGER->UserData));

if($SOGER->UserData["core_usersO7"]=="0") {
	$SOGER->SetFeedback("L'utente non ha i permessi necessari per la generazione e stampa del documento di accompagnamento [cod. O7]","1");	
	require_once("../__includes/COMMON_sleepSoger.php");
	header("location: ../");
	} 
else {

	$TableName	= $_GET['table'];
	$PRI		= $_GET['pri'];
	$filter		= $_GET['filter'];

	$sql ="SELECT NFORM, ID_COMM, ID_CAR, pesoN, pesoL, tara, FKESF AS stato_fisico, QL, DTFORM, ";
	$sql.="user_aziende_produttori.description AS PRODUTTORE, user_aziende_produttori.piva AS P_piva, user_aziende_produttori.codfisc AS P_codfisc, ";
	$sql.="user_aziende_trasportatori.description AS TRASPORTATORE, user_aziende_trasportatori.piva AS T_piva, user_aziende_trasportatori.codfisc AS T_codfisc, user_aziende_trasportatori.NumAlboAutotrasp, user_aziende_trasportatori.NumAlboAutotraspProprio, ";
	$sql.="user_aziende_destinatari.description AS DESTINATARIO, user_aziende_destinatari.piva AS D_piva, user_aziende_destinatari.codfisc AS D_codfisc, ";
	$sql.="user_aziende_intermediari.description AS INTERMEDIARIO, user_aziende_intermediari.piva AS I_piva, user_aziende_intermediari.codfisc AS I_codfisc, ";
	$sql.="user_impianti_produttori.description AS P_indirizzo, user_impianti_produttori.telefono AS P_telefono, Pcom.description AS P_comune, Pcom.shdes_prov AS P_prov, ";
	$sql.="user_impianti_trasportatori.description AS T_indirizzo, user_impianti_trasportatori.telefono AS T_telefono, Tcom.description AS T_comune, Tcom.shdes_prov AS T_prov, ";
	$sql.="user_impianti_destinatari.description AS D_indirizzo, user_impianti_destinatari.telefono AS D_telefono, Dcom.description AS D_comune, Dcom.shdes_prov AS D_prov, ";
	$sql.="user_impianti_intermediari.description AS I_indirizzo, user_impianti_intermediari.telefono AS I_telefono, Icom.description AS I_comune, Icom.shdes_prov AS I_prov, ";
	//$sql.="lov_cer.COD_CER, lov_cer.description AS DESC_CER, lov_cer.PERICOLOSO, user_schede_rifiuti.descrizione AS DESC_RIFIUTO, sis_tipi_imballaggi.TIPO_IMBALLAGGIO AS contenitore, ALTRO_TIPO_IMBALLAGGIO, ";
	$sql.="lov_cer.COD_CER, lov_cer.description AS DESC_CER, lov_cer.PERICOLOSO, user_schede_rifiuti.descrizione AS DESC_RIFIUTO, ";
	$sql.="user_schede_rifiuti.H1, user_schede_rifiuti.H2, user_schede_rifiuti.H3A, user_schede_rifiuti.H3B, user_schede_rifiuti.H4, ";
	$sql.="user_schede_rifiuti.H5, user_schede_rifiuti.H6, user_schede_rifiuti.H7, user_schede_rifiuti.H8, user_schede_rifiuti.H9, ";
	$sql.="user_schede_rifiuti.H10, user_schede_rifiuti.H11, user_schede_rifiuti.H12, user_schede_rifiuti.H13, user_schede_rifiuti.H14, user_schede_rifiuti.H15, ";
	$sql.="user_schede_rifiuti.ONU_DES, lov_num_onu.eti, lov_num_onu.imb, lov_num_onu.description as num, lov_num_onu.des, ";
	$sql.="CONCAT(user_autisti.nome, ' ', user_autisti.description) AS autista, ";
	$sql.="CONCAT(USER_nome, ' ', USER_cognome) AS compilatore ";
	$sql.="FROM ".$TableName." ";
	$sql.="JOIN user_schede_rifiuti ON ".$TableName.".ID_RIF=user_schede_rifiuti.ID_RIF ";
	$sql.="JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
	$sql.="JOIN user_aziende_produttori ON ".$TableName.".ID_AZP=user_aziende_produttori.ID_AZP ";
	$sql.="JOIN user_impianti_produttori ON ".$TableName.".ID_UIMP=user_impianti_produttori.ID_UIMP ";
	$sql.="JOIN lov_comuni_istat AS Pcom ON user_impianti_produttori.ID_COM=Pcom.ID_COM ";
	$sql.="LEFT JOIN user_aziende_trasportatori ON ".$TableName.".ID_AZT=user_aziende_trasportatori.ID_AZT ";
	$sql.="LEFT JOIN user_impianti_trasportatori ON ".$TableName.".ID_UIMT=user_impianti_trasportatori.ID_UIMT ";
	$sql.="LEFT JOIN lov_comuni_istat AS Tcom ON user_impianti_trasportatori.ID_COM=Tcom.ID_COM ";
	$sql.="LEFT JOIN user_aziende_destinatari ON ".$TableName.".ID_AZD=user_aziende_destinatari.ID_AZD ";
	$sql.="LEFT JOIN user_impianti_destinatari ON ".$TableName.".ID_UIMD=user_impianti_destinatari.ID_UIMD ";
	$sql.="LEFT JOIN lov_comuni_istat AS Dcom ON user_impianti_destinatari.ID_COM=Dcom.ID_COM ";
	$sql.="LEFT JOIN user_aziende_intermediari ON ".$TableName.".ID_AZI=user_aziende_intermediari.ID_AZI ";
	$sql.="LEFT JOIN user_impianti_intermediari ON ".$TableName.".ID_UIMI=user_impianti_intermediari.ID_UIMI ";
	$sql.="LEFT JOIN lov_comuni_istat AS Icom ON user_impianti_intermediari.ID_COM=Icom.ID_COM ";
	$sql.="LEFT JOIN user_autisti ON ".$TableName.".ID_AUTST=user_autisti.ID_AUTST ";
	//$sql.="LEFT JOIN sis_tipi_imballaggi ON ".$TableName.".ID_TIPO_IMBALLAGGIO=sis_tipi_imballaggi.ID_TIPO_IMBALLAGGIO ";
	$sql.="LEFT JOIN lov_num_onu ON ".$TableName.".ID_ONU=lov_num_onu.ID_ONU ";
	$sql.="WHERE ".$TableName.".".$PRI."=".$filter.";";

	$FEDIT->SDbRead($sql,"DbRecordSet");

	//die(var_dump($FEDIT->DbRecordSet));

	$ReplacedWith = array();
	$ToBeReplaced = array(
		0=>"[NFORM]",
		1=>"[PRODUTTORE]",
		2=>"[P_piva]",
		3=>"[TRASPORTATORE]",
		4=>"[T_piva]",
		5=>"[T_aut]",
		6=>"[COMMITTENTE]",
		7=>"[Com_piva]",
		8=>"[CARICATORE]",
		9=>"[Car_piva]",
		10=>"[DESTINATARIO]",
		11=>"[D_piva]",
		12=>"[COD_CER]",
		13=>"[DESC_CER]",
		14=>"[DESC_RIFIUTO]",
		15=>"[pesoN]",
		16=>"[pesoL]",
		17=>"[tara]",
		18=>"[contenitore]",
		19=>"[stato_fisico]",
		20=>"[pericoloso]",
		21=>"[H]",
		22=>"[ONU]",
		23=>"[DESC_ADR]",
		24=>"[QL]",
		25=>"[P_comune]",
		26=>"[DTFORM]",
		27=>"[compilatore]",
		28=>"[autista]"
		);

	# NFORM
	$ReplacedWith[0] = $FEDIT->DbRecordSet[0]['NFORM'];

	# PRODUTTORE
	$PRODUTTORE	= $FEDIT->DbRecordSet[0]['PRODUTTORE']." \line ";
	$PRODUTTORE.= $FEDIT->DbRecordSet[0]['P_indirizzo'].", ";
	$PRODUTTORE.= $FEDIT->DbRecordSet[0]['P_comune']." (";
	$PRODUTTORE.= $FEDIT->DbRecordSet[0]['P_prov'].")";
	$ReplacedWith[1] = $PRODUTTORE;
	if(!is_null($FEDIT->DbRecordSet[0]['P_piva']) && $FEDIT->DbRecordSet[0]['P_piva']!='')
		$ReplacedWith[2] = $FEDIT->DbRecordSet[0]['P_piva'];
	else
		$ReplacedWith[2] = $FEDIT->DbRecordSet[0]['P_codfisc'];

	# TRASPORTATORE
	$TRASPORTATORE = $FEDIT->DbRecordSet[0]['TRASPORTATORE']." \line ";
	$TRASPORTATORE.= $FEDIT->DbRecordSet[0]['T_indirizzo'].", ";
	$TRASPORTATORE.= $FEDIT->DbRecordSet[0]['T_comune']." (";
	$TRASPORTATORE.= $FEDIT->DbRecordSet[0]['T_prov'].")";
	$ReplacedWith[3] = $TRASPORTATORE;
	if(!is_null($FEDIT->DbRecordSet[0]['T_piva']) && $FEDIT->DbRecordSet[0]['T_piva']!='')
		$ReplacedWith[4] = $FEDIT->DbRecordSet[0]['T_piva'];
	else
		$ReplacedWith[4] = $FEDIT->DbRecordSet[0]['T_codfisc'];
	$ReplacedWith[5] = $FEDIT->DbRecordSet[0]['NumAlboAutotrasp'];

	# COMMITTENTE
	// 1 - produttore/detentore
	// 2 - intermediario
	// 3 - destinatario

	switch($FEDIT->DbRecordSet[0]['ID_COMM']){
		default:
		case '1':
			$COMMITTENTE = $FEDIT->DbRecordSet[0]['PRODUTTORE']." \line ";
			$COMMITTENTE.= $FEDIT->DbRecordSet[0]['P_indirizzo'].", ";
			$COMMITTENTE.= $FEDIT->DbRecordSet[0]['P_comune']." (";
			$COMMITTENTE.= $FEDIT->DbRecordSet[0]['P_prov'].")";
			$ReplacedWith[6] = $COMMITTENTE;
			if(!is_null($FEDIT->DbRecordSet[0]['P_piva']) && $FEDIT->DbRecordSet[0]['P_piva']!='')
				$ReplacedWith[7] = $FEDIT->DbRecordSet[0]['P_piva'];
			else
				$ReplacedWith[7] = $FEDIT->DbRecordSet[0]['P_codfisc'];
			break;
		case '2':
			$COMMITTENTE = $FEDIT->DbRecordSet[0]['INTERMEDIARIO']." \line ";
			$COMMITTENTE.= $FEDIT->DbRecordSet[0]['I_indirizzo'].", ";
			$COMMITTENTE.= $FEDIT->DbRecordSet[0]['I_comune']." (";
			$COMMITTENTE.= $FEDIT->DbRecordSet[0]['I_prov'].")";
			$ReplacedWith[6] = $COMMITTENTE;
			if(!is_null($FEDIT->DbRecordSet[0]['I_piva']) && $FEDIT->DbRecordSet[0]['I_piva']!='')
				$ReplacedWith[7] = $FEDIT->DbRecordSet[0]['I_piva'];
			else
				$ReplacedWith[7] = $FEDIT->DbRecordSet[0]['I_codfisc'];
			break;
		case '3':
			$COMMITTENTE = $FEDIT->DbRecordSet[0]['DESTINATARIO']." \line ";
			$COMMITTENTE.= $FEDIT->DbRecordSet[0]['D_indirizzo'].", ";
			$COMMITTENTE.= $FEDIT->DbRecordSet[0]['D_comune']." (";
			$COMMITTENTE.= $FEDIT->DbRecordSet[0]['D_prov'].")";
			$ReplacedWith[6] = $COMMITTENTE;
			if(!is_null($FEDIT->DbRecordSet[0]['D_piva']) && $FEDIT->DbRecordSet[0]['D_piva']!='')
				$ReplacedWith[7] = $FEDIT->DbRecordSet[0]['D_piva'];
			else
				$ReplacedWith[7] = $FEDIT->DbRecordSet[0]['D_codfisc'];
			break;
		}

	# CARICATORE
	// 1 - produttore/detentore
	// 2 - trasportatore
	// 3 - intermediario

	switch($FEDIT->DbRecordSet[0]['ID_CAR']){
		default:
		case '1':
			$CARICATORE = $FEDIT->DbRecordSet[0]['PRODUTTORE']." \line ";
			$CARICATORE.= $FEDIT->DbRecordSet[0]['P_indirizzo'].", ";
			$CARICATORE.= $FEDIT->DbRecordSet[0]['P_comune']." (";
			$CARICATORE.= $FEDIT->DbRecordSet[0]['P_prov'].")";
			$ReplacedWith[8] = $CARICATORE;
			if(!is_null($FEDIT->DbRecordSet[0]['P_piva']) && $FEDIT->DbRecordSet[0]['P_piva']!='')
				$ReplacedWith[9] = $FEDIT->DbRecordSet[0]['P_piva'];
			else
				$ReplacedWith[9] = $FEDIT->DbRecordSet[0]['P_codfisc'];
			break;
		case '2':
			$CARICATORE = $FEDIT->DbRecordSet[0]['TRASPORTATORE']." \line ";
			$CARICATORE.= $FEDIT->DbRecordSet[0]['T_indirizzo'].", ";
			$CARICATORE.= $FEDIT->DbRecordSet[0]['T_comune']." (";
			$CARICATORE.= $FEDIT->DbRecordSet[0]['T_prov'].")";
			$ReplacedWith[8] = $CARICATORE;
			if(!is_null($FEDIT->DbRecordSet[0]['T_piva']) && $FEDIT->DbRecordSet[0]['T_piva']!='')
				$ReplacedWith[9] = $FEDIT->DbRecordSet[0]['T_piva'];
			else
				$ReplacedWith[9] = $FEDIT->DbRecordSet[0]['T_codfisc'];
			break;
		case '3':
			$CARICATORE = $FEDIT->DbRecordSet[0]['INTERMEDIARIO']." \line ";
			$CARICATORE.= $FEDIT->DbRecordSet[0]['I_indirizzo'].", ";
			$CARICATORE.= $FEDIT->DbRecordSet[0]['I_comune']." (";
			$CARICATORE.= $FEDIT->DbRecordSet[0]['I_prov'].")";
			$ReplacedWith[8] = $CARICATORE;
			if(!is_null($FEDIT->DbRecordSet[0]['I_piva']) && $FEDIT->DbRecordSet[0]['I_piva']!='')
				$ReplacedWith[9] = $FEDIT->DbRecordSet[0]['I_piva'];
			else
				$ReplacedWith[9] = $FEDIT->DbRecordSet[0]['I_codfisc'];
			break;
		}



	# DESTINATARIO
	$DESTINATARIO = $FEDIT->DbRecordSet[0]['DESTINATARIO']." \line ";
	$DESTINATARIO.= $FEDIT->DbRecordSet[0]['D_indirizzo'].", ";
	$DESTINATARIO.= $FEDIT->DbRecordSet[0]['D_comune']." (";
	$DESTINATARIO.= $FEDIT->DbRecordSet[0]['D_prov'].")";
	$ReplacedWith[10] = $DESTINATARIO;
	if(!is_null($FEDIT->DbRecordSet[0]['D_piva']))
		$ReplacedWith[11] = $FEDIT->DbRecordSet[0]['D_piva'];
	else
		$ReplacedWith[11] = $FEDIT->DbRecordSet[0]['D_codfisc'];

	# CER
	$ReplacedWith[12] = $FEDIT->DbRecordSet[0]['COD_CER'];

	# DESCRIZIONE CER
	$ReplacedWith[13] = $FEDIT->DbRecordSet[0]['DESC_CER'];

	# DESCRIZIONE RIFIUTO
	$ReplacedWith[14] = $FEDIT->DbRecordSet[0]['DESC_RIFIUTO'];

	# PESO NETTO
	$ReplacedWith[15] = $FEDIT->DbRecordSet[0]['pesoN'];

	# PESO LORDO
	$ReplacedWith[16] = $FEDIT->DbRecordSet[0]['pesoL'];

	# TARA
	$ReplacedWith[17] = $FEDIT->DbRecordSet[0]['tara'];

	# IMBALLAGGIO
	/*
	if(!is_null($FEDIT->DbRecordSet[0]['contenitore']) && $FEDIT->DbRecordSet[0]['contenitore']!='')
		$ReplacedWith[18] = $FEDIT->DbRecordSet[0]['contenitore'];
	else
		$ReplacedWith[18] = $FEDIT->DbRecordSet[0]['ALTRO_TIPO_IMBALLAGGIO'];
	*/
	$ReplacedWith[18] = '';

	# STATO FISICO
	$ReplacedWith[19] = $FEDIT->DbRecordSet[0]['stato_fisico'];

	# PERICOLOSO
	if($FEDIT->DbRecordSet[0]['PERICOLOSO']=="0") $pericoloso="No"; else $pericoloso="S�";
	$ReplacedWith[20] = $pericoloso;

	# CLASSI H
	$CLASSIH = "";
	foreach($FEDIT->DbRecordSet[0] as $k=>$v) {
		if($k[0]=="H" && $v=="1") {
			$CLASSIH .= $k . " ";
			}
		}
	$ReplacedWith[21] = $CLASSIH;

	# ONU
	$ReplacedWith[22] = $FEDIT->DbRecordSet[0]['num'];

	# DESCRIZIONE ADR
	if(!is_null($FEDIT->DbRecordSet[0]['ONU_DES']) && $FEDIT->DbRecordSet[0]['ONU_DES']!='')
		$ONU_DES = $FEDIT->DbRecordSet[0]['ONU_DES'];
	else{
		$ONU_DES = $FEDIT->DbRecordSet[0]['des'];
		}
	if(!is_null($FEDIT->DbRecordSet[0]["eti"]) && $FEDIT->DbRecordSet[0]["eti"]!="") {
		$ONU_DES.= ", " . $FEDIT->DbRecordSet[0]["eti"];
		}
	if(!is_null($FEDIT->DbRecordSet[0]["imb"]) && $FEDIT->DbRecordSet[0]["imb"]!="") {
		$ONU_DES.= ", " . $FEDIT->DbRecordSet[0]["imb"];
		}
	$ReplacedWith[23] = $ONU_DES;

	# QL
	if($FEDIT->DbRecordSet[0]['QL']==0) $QL="No"; else $QL="S�";
	$ReplacedWith[24] = $QL;

	# LUOGO_PRODUTTORE
	$ReplacedWith[25] = $FEDIT->DbRecordSet[0]['P_comune'];

	# DTFORM
	$dt=explode('-', $FEDIT->DbRecordSet[0]['DTFORM']);
	$DTFORM=$dt[2]."/".$dt[1]."/".$dt[0];
	$ReplacedWith[26] = $DTFORM;

	# COMPILATORE
	$ReplacedWith[27] = $FEDIT->DbRecordSet[0]['compilatore'];

	# AUTISTA
	$ReplacedWith[28] = $FEDIT->DbRecordSet[0]['autista'];


	$FileName = "Documento_di_accompagnamento.rtf";
	$template = "BOLLA.rtf";

	StreamOut($template,$FileName,$ToBeReplaced,$ReplacedWith);
	}

require_once("../__includes/COMMON_sleepForgEdit.php");
?>