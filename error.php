<!DOCTYPE html>
<html><head>
<title>SOGER � Errore nell'applicazione</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="Soger - Software Fiscale per la movimentazione dei rifiuti"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<meta name="keywords" content="Rifiuti, Mud, Gestionale Rifiuti, Formulario, Registro, Carico, Scarico, ISO, Normativa Rifiuti, Rifiuti Pericolosi, CER"/>
<meta name="generator" content="http://www.sintem.it"/>
<link href="__css/SogerHeader.css?1407240914" rel="stylesheet" type="text/css"/>
<link href="__css/sogerDEF.css?1407240914" rel="stylesheet" type="text/css"/>
</head>

<body>
<div class="Header" id="Header">
	<div id="Logo">&nbsp;</div>
</div>

<div id="FGEblock">
	
	<div class="FGEDataGridTitle">
		<div>ERRORE NELL'APPLICAZIONE</div>
	</div>

	<br />

	<div style="width:98%;margin:1%;">

	
	<?php 
	
	$ERROR_ICON		= "error_icon_".$_GET['error_code'].".png"; 

	switch ($_GET['error_code']){

		case 'unknown':
			$ERROR_DETAIL	= $_GET['query_string'];
			$ERROR_MESSAGE	= "Si � verificato un errore la cui causa non � nota. Per favore, apri un ticket e segnalaci questo errore:<br /><br />".$ERROR_DETAIL."<br /><br />.E' ora possibile riprendere l'utilizzo del programma cliccando sul seguente link: <a href=\"http://".$_SERVER['HTTP_HOST']."/soger/__scripts/status.php?area=UserMainMenu\">http://".$_SERVER['HTTP_HOST']."/soger/__scripts/status.php?area=UserMainMenu</a>.";
			break;

		case 1062:
			$ERROR_MESSAGE	= "A causa dei rallentamenti nella linea Internet e/o un uso improprio delle funzioni \"Avanti\", \"Indietro\", \"Aggiorna\" del browser, il Sistema di verifica dell'integrit� del database ha impedito l'inserimento doppio del medesimo record. Per ulteriori informazioni si prega di contattare l'assistenza tecnica.<br /><br />			
			E' ora possibile riprendere l'utilizzo del programma cliccando sul seguente link: <a href=\"http://".$_SERVER['HTTP_HOST']."/soger/__scripts/status.php?area=UserMainMenu\">http://".$_SERVER['HTTP_HOST']."/soger/__scripts/status.php?area=UserMainMenu</a>.<br /><br />Ti invitiamo a verificare l'elenco dei movimenti per constatarne l'esattezza.";
			break;
	
		case 1046:
			$ERROR_MESSAGE	= "A causa deila prolungata inattivit� sul Sistema, la tua sessione di lavoro � scaduta.<br /><br />E' possibile riprendere l'utilizzo del programma eseguendo nuovamente il login cliccando sul seguente link: <a href=\"http://".$_SERVER['HTTP_HOST']."/soger/__scripts/status.php?area=SOGER_logoff&amp;SessionExpired\">http://".$_SERVER['HTTP_HOST']."/soger/__scripts/status.php?area=SOGER_logoff&amp;SessionExpired</a>.";
			break;

		}

	?>
	

	<table id="ApplicationError">

		<tr>
			<td class="ApplicationError_image"><img src="__css/<?php echo $ERROR_ICON; ?>" alt="Errore nell'applicazione" title="Errore nell'applicazione" /></td>
			<td class="ApplicationError_text">

			<h3>Attenzione!</h3>
			
			<?php
			
			echo $ERROR_MESSAGE; 
			
			?>

			</td>
		</tr>

	</table>

	</div>
	
</div>
</body></html>