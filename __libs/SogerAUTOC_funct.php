<?php

function giornilavorativi($dataInizio,$dataFine){
	// Calcolo del giorno di Pasqua fino all'ultimo anno valido
	for ($i=2006; $i<=2037; $i++) {
		$pasqua = date("Y-m-d", easter_date($i));
		$array_pasqua[] = $pasqua;
		}

	// Calcolo le rispettive pasquette
	foreach($array_pasqua as $pasqua){
		list ($anno,$mese,$giorno) = explode("-",$pasqua);
		$pasquetta = mktime (0,0,0,date($mese),date($giorno)+1,date($anno));
		$array_pasquetta[] = $pasquetta;
		}
	 
	// questi giorni son sempre festivi a prescindere dall'anno
	$giorniFestivi = array("01-01",
						   "01-06",
						   "04-25",
						   "05-01",
						   "06-02",
						   "08-15",
						   "11-01",
						   "12-08",
						   "12-25",
						   "12-26",
						   );

		$lavorativi = 0;

		for($i = strtotime($dataInizio); $i<=strtotime($dataFine); $i = strtotime("+1 day",$i)){
			$giorno_data = date("w",$i); //verifico il giorno: da 0 (dom) a 6 (sab)
			$mese_giorno = date('m-d',$i); // confronto con gg sempre festivi
			// Infine verifico che il giorno non sia sabato,domenica,festivo fisso o festivo variabile (pasquetta);         
			if ($giorno_data !=0 && $giorno_data != 6 && !in_array($mese_giorno,$giorniFestivi) && !in_array($i,$array_pasquetta) ){
				$lavorativi++;
				}
			}

return $lavorativi;
} 


?>