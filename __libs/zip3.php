<?php
	/*
		By: 		Matt Ford

		Purpose:	Basic class to create zipfiles
	*/



class zipFile {
	public $files = array();
	public $settings = NULL;
	public $fileInfo = array (
			"name" => "",
			"numFiles" => 0,
			"fullFilePath" => ""
		);
	private $fileHash = "";
	private $zip = "";

	public function __construct($settings) {
		$this->createZipFile($settings);
	}

	public function createZipFile($settings) {
		$this->zip = new ZipArchive();
		$this->settings = new stdClass();


		foreach ($settings as $k => $v) {
			$this->settings->$k = $v;
		}
	}

	public function create($archiveName) {
		//$this->fileHash = md5(implode(",", $this->files));
		$this->fileHash = $archiveName;
		$this->fileInfo["name"] = $this->fileHash . ".zip";
		$this->fileInfo["numFiles"] = count($this->files);
		$this->fileInfo["fullFilePath"] = $this->settings->path . "/" . $this->fileInfo["name"];
		if (file_exists($this->fileInfo["fullFilePath"])) {
			return array (
					false,
					"already created: " . $this->fileInfo["fullFilePath"]
					);
		}
		else {
			$this->zip->open($this->fileInfo["fullFilePath"], ZIPARCHIVE::CREATE);
			$this->addFiles();
			$this->zip->close();
			return array (
					true,
					"new file created: " . $this->fileInfo["fullFilePath"]
					);
		}
	}

	private function addFiles() {
		foreach ($this->files as $k) {
			$this->zip->addFile($k, basename($k));
		}
	}
}

?>