<?php
function sql_Movimento2Costi($idMOV,$soggetto,$ID_AZI,$DTMOV){
			
//{{{ 
	require("SogerECO_SoggettiSwitch.php");
	
	$sql = "SELECT user_movimenti_fiscalizzati.$tabAzKey,";
	$sql .= "peso_spec,user_movimenti_fiscalizzati.ID_RIF,DTMOV,pesoN AS pesoKG,PS_DESTINO, COLLI, Km, ID_MOV_F,
		$idCont,lov_contratti_um.motodo_calcolo,euro AS costoUM";
	if($soggetto=='trasp' || $soggetto=='dest' || $soggetto=='inter')
		$sql .= ", $tabContrCond.ID_UIMD AS ID_UIMD_VINCOLANTE ";
	$sql .= " FROM user_movimenti_fiscalizzati";
	$sql .= " LEFT JOIN $tabAZ ON $tabAZ.$tabAzKey=user_movimenti_fiscalizzati.$tabAzKey ";
	$sql .= " LEFT JOIN $tabContr ON $tabContr.$tabAzKey=$tabAZ.$tabAzKey ";
	$sql .= " LEFT JOIN $tabContrCond ON $tabContrCond.$tabContryKey=$tabContr.$tabContryKey ";
	$sql .= " LEFT JOIN lov_contratti_um ON lov_contratti_um.ID_UM_CNT=$tabContrCond.ID_UM_CNT ";
	$sql .= " LEFT JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF";
	$sql .= " WHERE ID_MOV_F='$idMOV' AND $tabContr.ID_RIF=user_movimenti_fiscalizzati.ID_RIF AND $tabContrCond.ID_UM_CNT<>'3'";
	//$sql .= " AND $tabContrCond.$tabContryKey NOT IN (SELECT $tabContryKey FROM user_movimenti_costi WHERE ID_MOV_F=$idMOV)";
	$sql .= " AND $tabContr.DT_INIZIO<='".$DTMOV."' AND ($tabContr.DT_SCADENZA>='".$DTMOV."' OR $tabContr.DT_SCADENZA IS NULL OR $tabContr.DT_SCADENZA='0000-00-00')";
	if($ID_AZI!='0000000')
		$sql .= " AND $tabContrCond.APPLY_W_INT = 1 ";
	$sql .= " ORDER BY $tabAzKey,$tabContr.$tabContryKey";
	return $sql; //}}}

}
function sql_estraiCostiFissi($idIMP, $soggetto) {
//{{{ 
		
	require("SogerECO_SoggettiSwitch.php");
	
	$sql = "SELECT $tabAZ.ID_IMP,$tabContrCond.euro, $tabContrCond.description As desCondCont, ";
	$sql .= " $tabAZ.$tabAzKey, $tabContrCond.$idCont, lov_contratti_um.description AS UMdes, lov_contratti_um.motodo_calcolo, DT_INIZIO";
	$sql .= " FROM ($tabAZ JOIN $tabContr ON $tabAZ.$tabAzKey = $tabContr.$tabAzKey) ";
	$sql .= " LEFT JOIN $tabContrCond ON $tabContrCond.$tabContryKey=$tabContr.$tabContryKey";
	$sql .= " JOIN lov_contratti_um ON lov_contratti_um.ID_UM_CNT=$tabContrCond.ID_UM_CNT";
	$sql .= " WHERE $tabAZ.ID_IMP='$idIMP' AND motodo_calcolo='FIXED'";
	$sql .=" ORDER BY $tabAZ.$tabAzKey,$tabContr.$tabContryKey";
	return $sql; //}}}

}
function CreaArrayScadenze($dateStart) {
//{{{ 
	$montCounter = $monthStart = date("n",strtotime($dateStart));
	$dayStart = date("j",strtotime($dateStart));
	$monthNow = date("m");
	$yearNow = date("Y");
	$tokenDates = array();
	while($montCounter<=$monthNow) {
		if($montCounter<10) {
			$zero = "0";
		} else {
			$zero = "";
		}
		if($monthStart==$montCounter) {
			if($dayStart==1) {
				$tokenDates["$yearNow-$zero$montCounter-01"] = null;
			}
		} else {
			$tokenDates["$yearNow-$zero$montCounter-01"] = null;
		}
		$montCounter ++;		
	}
	return $tokenDates; //}}}

}
function costiFissiSqlVerifica(&$fObj,$soggetto) {
	
//{{{ 
	require("SogerECO_SoggettiSwitch.php");
	
	$counter = 0;
	$sql = array();
	$Scadenze = array();
	foreach($fObj->DbRecordSet as $k=>$v) {
		$scadenze = CreaArrayScadenze($v["DT_INIZIO"]);
		$sql[$counter]["sqlVerifica"] = "SELECT * FROM user_movimenti_costi WHERE $tabAzKey='" . $v[$tabAzKey] . "' AND FKE_UM='FIXED' AND $idCont='" . $v[$idCont] . "' AND (DT='" . implode("' OR DT='",array_keys($scadenze)) . "') ORDER BY DT ASC";
		$sql[$counter]["arrScadenze"] = $scadenze;
		$sql[$counter]["costoFisso"] = $v["euro"];
		$sql[$counter]["refKeyCont"] = $idCont;
		$sql[$counter]["refKeyContValue"] = $v[$idCont];
		$sql[$counter]["refKeyAz"] = $tabAzKey;
		$sql[$counter]["refKeyAzValue"] = $v[$tabAzKey];
		$counter++;		
	}
	return $sql; //}}}

}

function CostiFissiDoTheDamnThing($dataArr,&$FeditObj,$Impianto) {
//{{{ 
	foreach($dataArr as $k=>$realData) {
		foreach($realData as $k1=>$data) {
			$FeditObj->SDbRead($data["sqlVerifica"],"DbRecordSet",true,false);
			if($FeditObj->DbRecsNum>0) {
				$dateCheck = $data["arrScadenze"];
				foreach($FeditObj->DbRecordSet as $recK=>$recV) {
					$dateCheck[$recV["DT"]] = "X";
				}
				foreach($dateCheck as $DT=>$checkValue) {
					if(is_null($checkValue)) {
						$sql = "INSERT INTO user_movimenti_costi VALUES (null,'$DT','$Impianto',null," . $data["costoFisso"] . ",'FIXED','" . $data["costoFisso"] . "',";
						require("SogerECO_CostiFissiSwitch.php");
						$FeditObj->SDBWrite($sql,"DbRecordSet",false,false);
					}
				}
			} else {
				foreach($data["arrScadenze"] as $DT=>$nullValue) {
					$sql = "INSERT INTO user_movimenti_costi VALUES (null,'$DT','$Impianto',null," . $data["costoFisso"] . ",'FIXED','" . $data["costoFisso"] . "',";
					require("SogerECO_CostiFissiSwitch.php");
					$FeditObj->SDBWrite($sql,"DbRecordSet",false,false);
				}
			}
		}
	} //}}}

}

function sql_ElencoCosti($idMov) {
//{{{ 
	$sql = "SELECT ID_MOV_F,ID_MOV_CST,user_movimenti_costi.euro,";
	$sql .=" user_aziende_destinatari.description AS AZDdes, user_aziende_trasportatori.description AS AZTdes, 
			user_aziende_intermediari.description AS AZIdes , user_aziende_produttori.description AS AZPdes,";
	$sql .=" user_contratti_destinatari.description AS CONTDdes, user_contratti_trasportatori.description AS CONTTdes,
			user_contratti_produttori.description AS CONTPdes, user_contratti_intermediari.description AS CONTIdes,";
	$sql .=" user_contratti_dest_cond.description AS CONDDdes, user_contratti_int_cond.description AS CONDIdes,
			user_contratti_trasp_cond.description AS CONDTdes, user_contratti_pro_cond.description AS CONDPdes,";
	$sql .=" UMD.description AS UMDDdes, UMT.description AS UMTDdes, UMI.description AS UMIDdes, UMP.description AS UMIPdes,";
	$sql .=" UMD.ID_UM_CNT AS UMDkey, UMT.ID_UM_CNT AS UMTkey, UMI.ID_UM_CNT AS UMIkey, UMP.ID_UM_CNT AS UMPkey,";
	$sql .=" user_contratti_dest_cond.note AS Dnote, user_contratti_trasp_cond.note AS Tnote, user_contratti_int_cond.note AS Inote, user_contratti_pro_cond.note AS Pnote";
	$sql .=" FROM user_movimenti_costi";
	$sql .= " LEFT JOIN user_aziende_destinatari ON user_aziende_destinatari.ID_AZD=user_movimenti_costi.ID_AZD ";
	$sql .= " LEFT JOIN user_aziende_trasportatori ON user_aziende_trasportatori.ID_AZT=user_movimenti_costi.ID_AZT ";
	$sql .= " LEFT JOIN user_aziende_intermediari ON user_aziende_intermediari.ID_AZI=user_movimenti_costi.ID_AZI ";
	$sql .= " LEFT JOIN user_aziende_produttori ON user_aziende_produttori.ID_AZP=user_movimenti_costi.ID_AZP ";
	$sql .= " LEFT JOIN user_contratti_dest_cond ON user_movimenti_costi.ID_CNT_COND_D=user_contratti_dest_cond.ID_CNT_COND_D ";
	$sql .= " LEFT JOIN user_contratti_int_cond ON user_movimenti_costi.ID_CNT_COND_I=user_contratti_int_cond.ID_CNT_COND_I ";
	$sql .= " LEFT JOIN user_contratti_trasp_cond ON user_movimenti_costi.ID_CNT_COND_T=user_contratti_trasp_cond.ID_CNT_COND_T ";
	$sql .= " LEFT JOIN user_contratti_pro_cond ON user_movimenti_costi.ID_CNT_COND_P=user_contratti_pro_cond.ID_CNT_COND_P ";
	$sql .= " LEFT JOIN user_contratti_destinatari ON user_contratti_destinatari.ID_CNT_D=user_contratti_dest_cond.ID_CNT_D ";
	$sql .= " LEFT JOIN user_contratti_trasportatori ON user_contratti_trasportatori.ID_CNT_T=user_contratti_trasp_cond.ID_CNT_T ";
	$sql .= " LEFT JOIN user_contratti_produttori ON user_contratti_produttori.ID_CNT_P=user_contratti_pro_cond.ID_CNT_P ";
	$sql .= " LEFT JOIN user_contratti_intermediari ON user_contratti_intermediari.ID_CNT_I=user_contratti_int_cond.ID_CNT_I ";
	$sql .= " LEFT JOIN lov_contratti_um AS UMD ON UMD.ID_UM_CNT=user_contratti_dest_cond.ID_UM_CNT ";
	$sql .= " LEFT JOIN lov_contratti_um AS UMT ON UMT.ID_UM_CNT=user_contratti_trasp_cond.ID_UM_CNT ";
	$sql .= " LEFT JOIN lov_contratti_um AS UMI ON UMI.ID_UM_CNT=user_contratti_int_cond.ID_UM_CNT ";
	$sql .= " LEFT JOIN lov_contratti_um AS UMP ON UMP.ID_UM_CNT=user_contratti_pro_cond.ID_UM_CNT ";
	$sql .= " WHERE ID_MOV_F='$idMov' ";	
	$sql .= " GROUP BY ID_MOV_CST";
	$sql .= " ORDER BY user_aziende_destinatari.ID_AZD,
		user_aziende_trasportatori.ID_AZT,
		user_aziende_intermediari.ID_AZI,
		user_aziende_produttori.ID_AZP";
	return $sql; //}}}
	}


function GrigliaCosti(&$FeditObj) {
//{{{ 
	global $SOGER;
	
	$BLOCK_EDIT_O12 = false;
	$disabled		= '';
	$DbRecsNum		= $FeditObj->DbRecsNum;


	if($SOGER->UserData['core_usersO12']=='0'){		
		$sql = "SELECT ID_USR FROM ".$_GET['table']." WHERE ".$_GET['pri']."=".$_GET['filter'].";";
		$FeditObj->SDBRead($sql,"DbRecordSetO12",true,false);		
		if($FEDIT->DbRecordSetO12[0]['ID_USR']!='0000000' AND ($SOGER->UserData['core_usersID_USR']!=$FeditOb->DbRecordSetO12[0]['ID_USR'])){
			$BLOCK_EDIT_O12 = true;
			$disabled = ' disabled ';
			}
		echo "<div id=\"Errors\" class=\"ErrType1\" onclick=\"javascript:document.getElementById('Errors').style.zIndex=-1\">";
		echo "L'utente non ha i permessi necessari per modificare i movimenti inseriti da altri utenti [cod. O12]";
		echo "</div>";
		}

	$tmp  = "<div class=\"FGEDataGridTitle\"><div>Costi Movimento</div></div>";
	if($SOGER->AppLocation!="login" && $SOGER->AppLocation!="login_documents" && count($SOGER->AppNavDIVs)>0) {
		$tmp .= "<div id=\"NavDiv\">\n";
		$tmp .= "<!-- NAV DIVs BEGIN -->\n[NAV_STUCTURE]\n<!-- NAV DIVs END -->\n";
		$SOGER->DIVmake_Navigation();
		$tmp .= "</div>\n";
		}

	$tmp .= "<form name=\"costiGrid\" id=\"costiGrid\" method=\"post\" action=\"__mov_snipplets/MOV_FastSaveCostiVariabili.php\">";
	$tmp .= "<input type=\"hidden\" name=\"keyCOSTO\" id=\"keyCOSTO\" value=\"\"/>";
	$tmp .= "<input type=\"hidden\" name=\"COSTO\" id=\"COSTO\" value=\"\"/>";
	
	$tmp .= "\n<div class=\"FGEDataGridPaging\" id=\"FGE_DG_1\">";
	$tmp .= "<table class=\"FGEDataGridTable\">";
	
	if($DbRecsNum==0){
		$tmp .= "<tr><td class=\"Tcell\"><br/><b>" . FGE_NoRecord . "</b><br/><br/></td></tr>";
		}
	else{
		$tmp .= "<input type=\"hidden\" name=\"filter\" id=\"filter\" value=\"" . $FeditObj->DbRecordSet[0]["ID_MOV_F"] . "\"/>";
		$tmp .= "<tr><th class=\"FGEDataGridTableHeader\">azienda</th>";
		$tmp .= "<th class=\"FGEDataGridTableHeader\">contratto</th>";
		$tmp .= "<th class=\"FGEDataGridTableHeader\">condizione contrattuale</th>";
		$tmp .= "<th class=\"FGEDataGridTableHeader\">note</th>";
		$tmp .= "<th class=\"FGEDataGridTableHeader\">euro</th>";
		$tmp .= "<th class=\"FGEDataGridTableHeader\">unit� di misura</th>";
		if(!$BLOCK_EDIT_O12)
			$tmp .= "<th class=\"FGEDataGridTableHeader\">&nbsp;</th></tr>";
		
		foreach ($FeditObj->DbRecordSet as $k=>$v) {
			if(!is_null($v["AZDdes"])) {
				$shDes = "(dest.)";
				$azKey = "AZDdes";
				$contKey = "CONTDdes";
				$condContDes = "CONDDdes";
				$umKey = "UMDDdes";
				$umRefKey = "UMDkey";
				$note = "Dnote";
			}
			if(!is_null($v["AZTdes"])) {
				$shDes = "(trasp.)";
				$azKey = "AZTdes";
				$contKey = "CONTTdes";
				$condContDes = "CONDTdes";
				$umKey = "UMTDdes";
				$umRefKey = "UMTkey";
				$note = "Tnote";
			}
			if(!is_null($v["AZIdes"])) {
				$shDes = "(interm.)";
				$azKey = "AZIdes";
				$contKey = "CONTIdes";
				$condContDes = "CONDIdes";
				$umKey = "UMIDdes";
				$umRefKey = "UMIkey";
				$note = "Inote";
			}
			if(!is_null($v["AZPdes"])) {
				$shDes = "(prod.)";
				$azKey = "AZPdes";
				$contKey = "CONTPdes";
				$condContDes = "CONDPdes";
				$umKey = "UMIPdes";
				$umRefKey = "UMPkey";
				$note = "Pnote";
				}
			$tmp .= "<tr><td class=\"Tcell\">". $v[$azKey] . " $shDes</td>"; 
			$tmp .= "<td class=\"Tcell\">". $v[$contKey] . "</td>";
			$tmp .= "<td class=\"Tcell\">". $v[$condContDes] . "</td>";
			$tmp .= "<td class=\"Tcell\">". $v[$note] . "</td>";
			if($v[$umRefKey]=='9') {
				$tmp .= "<td class=\"Tcell\"><input ".$disabled." type=\"text\" size=\"6\" name=\"" . $v["ID_MOV_CST"] . "\" id=\"" . $v["ID_MOV_CST"] . "\" class=\"FGEinput\" value=\"" . $v["euro"] . "\"/></td>";
			} else {
				$tmp .= "<td class=\"Tcell\">". $v["euro"] . "</td>";
			}
			$tmp .= "<td class=\"Tcell\">". $v[$umKey] . "</td>";	
			
			if(!$BLOCK_EDIT_O12){
				$tmp .= "<td class=\"Tcell\">&nbsp;";
				$url = "__scripts/FGE_DataGridEdit.php?";
				$url .= "table=user_movimenti_costi&amp;pri=ID_MOV_CST&amp;filter=" . $v["ID_MOV_CST"] . "&amp;ID_MOV_F=" . $v["ID_MOV_F"];
				$url .= "&amp;hash=" . MakeUrlHash("user_movimenti_costi","ID_MOV_CST",$v["ID_MOV_CST"]);
				
				if($v[$umRefKey]=='9') {
					$tmp .= "<a href=\"#\">";
					$tmp .= "<img src=\"__css/disk.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"cancella\"
					onclick=\"javascript:if(confirm('Aggiornare il costo?')){
						document.getElementById('keyCOSTO').value='" . $v["ID_MOV_CST"] . "';
						document.getElementById('COSTO').value=document.getElementById('" . $v["ID_MOV_CST"] . "').value;
						document.costiGrid.submit();
					};\"/>";
					$tmp .= "</a>";				
				}
				
				$tmp .= "<a href=\"javascript:if(confirm('" . FGE_DeleteConfirm . "')){document.location='$url&amp;FGE_action=delete'};\" title=\"cancella\">";
				$tmp .= "<img src=\"__css/FGE_delete.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-top: 1px; margin-left: 1px; margin-right: 2px;\" alt=\"cancella\"/>";
				$tmp .= "</a>";
				$tmp .= "&nbsp;</td>";
				}
			
			$tmp .= "</tr>";
		}
	}
	
	$tmp .= "</table></div></form>";

	# form "nascosto" per aggiornamento costi

	$tmp.="<form id=\"UpdateCosti\" name=\"UpdateCosti\" method=\"POST\" action=\"__mov_snipplets/MOV_RefreshCosti.php\">\n";
	$tmp.="<input type=\"hidden\" name=\"FGE_PrimaryKeyRef\" value=\"user_movimenti_fiscalizzati:ID_MOV_F\" />\n";
	$tmp.="<input type=\"hidden\" name=\"user_movimenti_fiscalizzati:ID_MOV_F\" value=\"".$_GET['filter']."\" />\n";
	$tmp.="<input type=\"hidden\" name=\"RefreshCosti\" value=\"1\" />\n";
	$tmp.="<input type=\"hidden\" name=\"hash\" value=\"".md5(session_id().$SOGER->UserData["core_usersID_IMP"])."\" />\n";
		
	if(!$BLOCK_EDIT_O12)
		$tmp.="<input type=\"button\" name=\"BTN_RefreshCosti\" class=\"FiscalButton\" value=\"Aggiorna costi / ricavi\" onClick=\"javascript:if(window.confirm('Attenzione! I costi verranno aggiornati in base alle condizioni contrattuali attuali e non a quelle in vigore alla data del movimento. L\' operazione e\' irreversibile. Proseguire?')) document.UpdateCosti.submit();\">";

	$tmp.="</form>";

	echo $tmp; //}}}

}

function ConcatenaCondizContratt($dataArray,&$resArray, $ID_UIMD='0000000', $chi='') {
	foreach($dataArray as $k=>$v) {
		if($chi=='trasp' || $chi=='dest' || $chi=='inter'){
			if( ($v['ID_UIMD_VINCOLANTE']!='0000000' AND $v['ID_UIMD_VINCOLANTE']==$ID_UIMD) OR $v['ID_UIMD_VINCOLANTE']=='0000000' OR is_null($v['ID_UIMD_VINCOLANTE']))
				$resArray[]=$v;
			}
		else
			$resArray[]=$v;
		}
	}

function CreaCosti($souceArray,&$destArray) {
//{{{ 
	$c=0;
	foreach($souceArray as $k=>$v) {
		switch($v["motodo_calcolo"]) {
			case "KG":
				$destArray[$c]["CostoCalcolato"] = round( ($destArray[$c]["pesoKG"]*$destArray[$c]["costoUM"]) , 2); 
			break;
			case "KM":
				$destArray[$c]["CostoCalcolato"] = round( ($destArray[$c]["Km"]*$destArray[$c]["costoUM"]) , 2); 
			break;
			case "KGD":
				$destArray[$c]["CostoCalcolato"] = round( ($destArray[$c]["PS_DESTINO"]*$destArray[$c]["costoUM"]) , 2);  
			break;  
			case "COLLO":
				$destArray[$c]["CostoCalcolato"] = round( ($destArray[$c]["COLLI"]*$destArray[$c]["costoUM"]) , 2);  
			break;
			case "M3":
				$destArray[$c]["CostoCalcolato"] = round ( (KgQConvert($destArray[$c]["pesoKG"],"M3",$destArray[$c]["peso_spec"]) * $destArray[$c]["costoUM"]) , 2); 
			break;
			case "LTR":
				$destArray[$c]["CostoCalcolato"] = round ( (KgQConvert($destArray[$c]["pesoKG"],"Litri",$destArray[$c]["peso_spec"]) * $destArray[$c]["costoUM"]) , 2);  
			break;
			case "VRB":
			case "FRMU":
			case "VIAGG":
				$destArray[$c]["CostoCalcolato"] = $destArray[$c]["costoUM"]; 
			break;
		}
		$c++;
	} //}}}

}

function CostiSQL($dArr,&$sql,$impianto,$condsP,$condsT,$condsI,$condsD) {

	foreach($dArr as $k=>$v) {
		
		// produttore
		if(!isset($v["ID_CNT_COND_P"]) | @is_null($v["ID_CNT_COND_P"])) {
			$idContP = 'null'; 
			$idAZP = 'null'; 	
			}
		else {
			$idContP =$v["ID_CNT_COND_P"];
			$idAZP =$v["ID_AZP"];
			}

		// trasportatore
		if(!isset($v["ID_CNT_COND_T"]) | @is_null($v["ID_CNT_COND_T"])) {
			$idContT = 'null';
			$idAZT = 'null'; 	
			}
		else {
			$idContT = $v["ID_CNT_COND_T"];
			$idAZT = $v["ID_AZT"];
			}

		// intermediario
		if(!isset($v["ID_CNT_COND_I"])  | @is_null($v["ID_CNT_COND_I"]) ) {
			$idContI = 'null'; 	
			$idAZI = 'null'; 
			}
		else {
			$idContI =$v["ID_CNT_COND_I"];
			$idAZI =$v["ID_AZI"];
			}
		
		// destinatario
		if(!isset($v["ID_CNT_COND_D"])  | @is_null($v["ID_CNT_COND_D"])) {
			$idContD = 'null'; 
			$idAZD = 'null';
			}
		else {
			$idContD =$v["ID_CNT_COND_D"];
			$idAZD =$v["ID_AZD"];
			}

		if(!is_null($idContP) AND !in_array($idContP,$condsP) AND !is_null($idContT) AND !in_array($idContT,$condsT) AND !is_null($idContI) AND !in_array($idContI,$condsI) AND !is_null($idContD) AND !in_array($idContD,$condsD)){
			$tmp = "insert into user_movimenti_costi VALUES (null,'${v["DTMOV"]}','$impianto','${v["ID_MOV_F"]}','${v["CostoCalcolato"]}','${v["motodo_calcolo"]}',";
			$tmp .= "'${v["costoUM"]}',$idContP,$idContT,$idContD,$idContI,";
			$tmp .= "$idAZP,$idAZT,$idAZD,$idAZI)";
			$sql[] = $tmp;
			}
		}
	}
?>