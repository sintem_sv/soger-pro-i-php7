<?php

function setUrlVariables() {
  $arg = array();
  $string = "?";
  $vars = $_GET;
  for ($i = 0; $i < func_num_args(); $i++)
    $arg[func_get_arg($i)] = func_get_arg(++$i);
  foreach (array_keys($arg) as $key)
    $vars[$key] = $arg[$key];
  foreach (array_keys($vars) as $key)
    if ($vars[$key] != "") $string.= $key . "=" . $vars[$key] . "&";
  if (SID != "" && SID != "SID" && $_GET["PHPSESSID"] == "")
    $string.= htmlspecialchars(SID) . "&";

  return htmlspecialchars(substr($string, 0, -1));
}


function SniffBrowser() {

   /***************************************************************************
   This function is a browser sniffer that returns a string-indexed array.
   The call:
      $browser = SniffBrowser();
   will return the following:
   $browser['type'] = Netscape, Explorer, Opera, Amaya, Unknown
   $browser['version'] = version number (float)
   $browser['platform'] = Windows, Mac, Other
   $browser['css'] = CSS version
   $browser['dom'] = DOM type: NS, IE, W3C, 0
   ***************************************************************************/

   global $HTTP_USER_AGENT;
   $b = $HTTP_USER_AGENT; $vers = 0.0;

   // detect browser brand and version number
   if (eregi('Opera[ \/]([0-9\.]+)' , $b, $a)) {
      $type = 'Opera';}
   elseif (eregi('Netscape[[:alnum:]]*[ \/]([0-9\.]+)', $b, $a)) {
      $type = 'Netscape';}
   elseif (eregi('MSIE[ \/]([0-9\.]+)', $b, $a)) {
      $type = 'Explorer';}
   elseif (eregi('Mozilla[ \/]([0-9\.]+)' , $b, $a)) {
      if (eregi('compatible' , $b)) {
         $type = 'Unknown';}
      else {
         $type = 'Netscape';}}
   elseif (eregi('([[:alnum:]]+)[ \/v]*([0-9\.]+)' , $b, $a)) {
      $type = $a[1]; $vers = $a[2];}
   else {
      $type = 'Unknown';}
   if (!$vers) $vers = $a[1];
   $browser['type'] = $type;
   $browser['version'] = $vers;

   // detect platform
   if (eregi('Win',$b)) $browser['platform'] = 'Windows';
   elseif (eregi('Mac',$b)) $browser['platform'] = 'Mac';
   else $browser['platform'] = 'Other';

   // find CSS version
   // note: it is unknown which future versions will support CSS2
   if ($type == 'Netscape' && $vers >= 4 ||
   $type == 'Explorer' && $vers >= 3 ||
   $type == 'Opera' && $vers >= 3) {
      $browser['css'] = 1;
      if ($type == 'Netscape' && $vers >= 5 ||
      $type == 'Explorer' && $vers >= 6 ||
      $type == 'Opera' && $vers >= 4) {
         $browser['css'] = 2;}}

   // detect DOM version
   $browser['dom'] = '0';
   if ($type == 'Explorer' && $vers >= 4 && $browser['platform'] == 'Windows') {
      // note: it is unknown which DOM model future versions of Explorer will use
      $browser['dom'] = 'IE';}
   elseif ($type == 'Opera' && $vers >= 5) {
      $browser['dom'] = 'W3C';}
   elseif ($type == 'Netscape') {
      if ($vers >= 5) $browser['dom'] = 'W3C';
      elseif ($vers >= 4) $browser['dom'] = 'NS';}

   // return array of answers
   return $browser;
   
   }


function import_sql($data,$path,&$Link,$DeleteFile=false) {
//{{{

    if(!($lines = file($_SERVER['DOCUMENT_ROOT'] . "/soger/" . $path. "/" . $data[1]))) {
		die ("impossibile aprire il file ${data[1]} Contattare l'assistenza tecnica");
	}
	#
	$scriptfile = "";
	foreach($lines as $line) {
		$line = trim($line);
		if(!ereg('^--', $line))  {
			$scriptfile.=" ".$line;
		}
	}
	$queries = explode(';', $scriptfile);
	$ImgC = ceil(count($queries)/50);
	$staticImg = "<img src=\"../__css/table_add.png\" style=\"float: left;\" alt=\"working\"/>";
	$doneImg = "<img src=\"../__css/tick.png\" style=\"float: left;\" alt=\"done\"/>";
	 @ob_end_flush();
	 echo "<h2>${data[0]}</h2>";
	$x = 0;
	$WrittenImg = 0;
	$doneWithImg = false;
	foreach($queries as $query) {
		$query = trim($query);
		if($query != "") {
			#echo "<hr>$query";
			$Link->SDBWrite($query,true,false,$data[2]);
			ob_start();
			if(ceil(count($queries)>=50)) {
				$doneWithImg = true;
				if($x>=$ImgC) {
					   echo $staticImg;
					   $WrittenImg++;
					   $x=0;
				}
			}
			$tmp = ob_get_contents();
			ob_clean();
			ob_end_flush();
			echo $tmp;
		}
		$x++;
	}
	if(!$doneWithImg) {
		for($y=1;$y<=50;$y++) {
			echo $staticImg;	
		}
	} else {
		if($WrittenImg<50) {
			for($WrittenImg=$WrittenImg;$WrittenImg<50;$WrittenImg++) {
				echo $staticImg;	
			}			
		}
	}
	echo $doneImg;
	if($DeleteFile) {
		unset($file);
	    $fileToDelete = $_SERVER["DOCUMENT_ROOT"] . "/soger/" . $path. "/" . $data[1];
        $TxtfileToDelete = $_SERVER["DOCUMENT_ROOT"] . "/soger/" . $path. "/" . substr($data[1],0,strlen($data[1])-3)."txt";
		unlink($fileToDelete);
		if(file_exists($TxtfileToDelete)) {
			unlink($TxtfileToDelete);
		}
	}
   //}}}
}
function StreamOut($Template,$FileName,$ToBeReplaced,$ToBeReplacedWith,$CType="RTF") {
//{{{
	$tmp = file_get_contents("../__templates/$Template");
	switch($CType) {
		case "RTF":
		Header("Content-Type: application/rtf");
		break;
	}
	Header("Content-Disposition: attachment; filename=$FileName");
	header('Content-Type: application/force-download');

	// Under HTTPS and IE8, those headers fix the download problem:
	// http://stackoverflow.com/questions/1242900/problems-with-header-when-displaying-a-pdf-file-in-ie8
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
	header("Pragma: public");
	$tmp = str_replace($ToBeReplaced,$ToBeReplacedWith,$tmp);
	echo $tmp;
}

// le date devono essere in formato m/d/Y
function dateDiff($interval,$dateTimeBegin,$dateTimeEnd) {
//{{{
	$dateTimeBegin=strtotime($dateTimeBegin);
	$dateTimeEnd=strtotime($dateTimeEnd);

	if($dateTimeEnd === -1 | $dateTimeBegin===-1) {
		return("either start or end date is invalid");
	}
	$dif=$dateTimeEnd - $dateTimeBegin;
	switch($interval) {
		case "d"://days
		return(floor($dif/86400)); //86400s=1d
		case "ww"://Week
		return(floor($dif/604800)); //604800s=1week=1semana
		case "m": //similar result "m" dateDiff Microsoft
		$monthBegin=(date("Y",$dateTimeBegin)*12)+
		date("n",$dateTimeBegin);
		$monthEnd=(date("Y",$dateTimeEnd)*12)+
		date("n",$dateTimeEnd);
		$monthDiff=$monthEnd-$monthBegin;
		return($monthDiff);
		case "yyyy": //similar result "yyyy" dateDiff Microsoft
		return(date("Y",$dateTimeEnd) - date("Y",$dateTimeBegin));
		default:
		return(floor($dif/86400)); //86400s=1d
	} //}}}
}
function DateScreenConvert($dt) {
//{{{
			if($dt!="0000-00-00") {
				$DataConvertita = strtotime($dt);
				$tmp = date(FGE_SCREEN_DATE, $DataConvertita);
				$tmp .= "|" . dateDiff("d",date("m/d/Y",$DataConvertita),date("m/d/Y"));
			} else {
				$tmp = "0000-00-00|0";
			}
			return $tmp; //}}}
}

function ScaricoInvolved($primary=null, $idCarico=null, $Table=null){

	global $FEDIT,$SOGER;

	if(is_null($idCarico))
		$idCarico	= $_GET['filter'];

	if(is_null($primary))
		$primary	= $_GET['pri'];

	if(is_null($Table))
		$table		= $_GET['table'];

	// leggo NMOV carico e workmode
	$sql="SELECT NMOV, produttore, trasportatore, destinatario, intermediario FROM ".$table." WHERE ".$primary."=".$idCarico;
	$FEDIT->SDbRead($sql,"DbRecordSet",true,false);
	$nmov			= $FEDIT->DbRecordSet[0]['NMOV'];
	$produttore		= $FEDIT->DbRecordSet[0]['produttore'];
	$trasportatore  = $FEDIT->DbRecordSet[0]['trasportatore'];
	$destinatario	= $FEDIT->DbRecordSet[0]['destinatario'];
	$intermediario	= $FEDIT->DbRecordSet[0]['intermediario'];

	// cerco eventuali scarichi legati
	$sql ="SELECT * FROM ".$table." WHERE ";
	$sql.="produttore='".$produttore."' AND trasportatore='".$trasportatore."' AND ";
	$sql.="destinatario='".$destinatario."' AND intermediario='".$intermediario."' AND ";
	$sql.="( FKErifCarico='".$nmov."' OR FKErifCarico LIKE '%,".$nmov."' OR ";
	$sql.="FKErifCarico LIKE '%,".$nmov.",%' OR FKErifCarico LIKE '".$nmov.",%' )";
	$sql.="AND ID_IMP='".$SOGER->UserData['core_impiantiID_IMP']."'";
        die($sql);
	$FEDIT->SDbRead($sql,"DbRecordSet",true,false);

	if($FEDIT->DbRecsNum>0)
		return true;
	else
		return false;

	}

function MovimentoInvolved($Field=null,$Value=null,$Table=null) {
//{{{
	$CheckedTables = array("user_schede_rifiuti","user_aziende_trasportatori","user_autorizzazioni_trasp",
						"user_impianti_trasportatori","user_aziende_intermediari","user_impianti_intermediari","user_autorizzazioni_interm","user_impianti_destinatari","user_autorizzazioni_dest",
						"user_aziende_destinatari","user_autorizzazioni_pro","user_impianti_produttori","user_aziende_produttori",
						"user_automezzi","user_autisti","user_rimorchi");
	if(is_null($Field)) {
		$Field = $_GET["pri"];
	}
	if(is_null($Table)) {
		$Table = $_GET["table"];
	}
	if(is_null($Value)) {
		$Value = $_GET["filter"];
	}
	if(array_search($Table,$CheckedTables)===false) {
		return false;
	}
	global $FEDIT,$SOGER;
	if($SOGER->UserData['core_impiantiREG_IND']==1){
		$TableMovimenti="user_movimenti";
                $Primary = "ID_MOV";
            }
	else{
		$TableMovimenti="user_movimenti_fiscalizzati";
                $Primary = "ID_MOV_F";
            }
	$sql = "SELECT ".$Primary." FROM ".$TableMovimenti." WHERE ";
	$sql .= $Field . "='" . $Value . "'";
	$sql .= " AND ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "' "; // dovrebbe essere superfluo
	#
	$FEDIT->SDbRead($sql,"DbRecordSet",true,false);
	if($FEDIT->DbRecsNum>0) {
		return true;
	} else {
                // ulteriore controllo nel caso di rifiuti; verifico giacenze iniziali
                if($Table=='user_schede_rifiuti'){
                    $sql ="SELECT id FROM user_movimenti_giacenze_iniziali WHERE ID_RIF = ".$Value.";";
                    $FEDIT->SDbRead($sql,"DbRecordSet",true,false);
                    if($FEDIT->DbRecsNum>0)
                        return true;
                    else
                        return false;
                    }
                else
                    return false;
	} //}}}
}



function number_format_unlimited_precision($number,$decimal = '.'){
	$broken_number = explode($decimal,$number);
	if((float)$broken_number[1]==0){
		return number_format((float)$broken_number[0], 0, ".", "");
		}
	else{
		return number_format((float)$broken_number[0], 0, "", "").$decimal.(float)$broken_number[1];
		}
	}
	

function KgQConvert($in,$Um,$PesoSpec) {
//{{{
	if($in=="0") {
		return 0;
	}
        if($PesoSpec==0)
            $PesoSpec=1;
	switch($Um) {
		# regole per arrotondare:
		# - se il valore da arrotondare � <1 arrotondo per eccesso con due decimali
		# - se il valore da arrotondare � >1 arrotondo per eccesso o difetto senza decimali
		case "M3":
			$arrotondato=round(($in/$PesoSpec/1000),2);
			if($arrotondato<1){
				if($arrotondato==0)
					return 0.01;
				else
					return round(($in/$PesoSpec/1000),2);
				}
			else
				return round(($in/$PesoSpec/1000),0);
			//return round(($in/$PesoSpec/1000),2);
		break;
		case "Litri":
			$arrotondato=round(($in/$PesoSpec),2);
			if($arrotondato<1){
				if($arrotondato==0)
					return 0.01;
				else
					return round(($in/$PesoSpec),2);
				}
			else
				return round(($in/$PesoSpec),0);
			//return round(($in/$PesoSpec),2);
		break;
		case "Kg":
			return "Kg";
		break;
	} //}}}
}
function MakeUrlHash($Table,$Key,$KeyValue) {
	$hashBase = "$Table|$Key|$KeyValue|SrgGarbler|"  . session_id();
	return md5($hashBase);
}
?>
