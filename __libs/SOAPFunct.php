<?php


$URL_SOAP_CLIENT = 'http://sis.sistri.it/SIS/services/SIS?wsdl';
$URI = 'http://sis.sistri.it/SIS/services/SIS/';
/*
$URL_SOAP_CLIENT = 'https://sisssl.sistri.it/SIS/services/SIS?wsdl';
$URI = 'https://sisssl.sistri.it/SIS/services/SIS/';
*/
$clientSISTRI = new SoapClient(null, array(
    'location' => $URL_SOAP_CLIENT,
    'uri'      => $URI,
    'trace'    => 1,
    ));


/**
 * Stampa qualsiasi variabile in una formattazione comoda
 *
 * @param Stringa/Array $mixed
 * @return NULL
 */
function var_dump_pre($mixed = null) {
	echo '<pre>';
	var_dump($mixed);
	echo '</pre>';
	return null;
	}


/**
 * Converte un oggetto stdClass in un array associativo
 *
 * @param Oggetto $object
 * @return Array
 */
function objectToArray( $object ){
	
	if( !is_object( $object ) && !is_array( $object ) ) {
		return $object;
		}
	
	if( is_object( $object ) ) {
		$object = get_object_vars( $object );
		}
	
	return array_map( 'objectToArray', $object );
    
	}

?>