<?php

$_SESSION['MudFileSize']=0;
$_SESSION['EOL']=chr(13);

function createMudFile(){

	global $SOGER;
	global $FEDIT;
	$FEDIT->DbServerData["db"]="soger2009";
	//$FEDIT->DbServerData["db"]="soger2010";
	$MUD2009 = "";

#AA#

	## costante tipo record
	$AARecord ="AA;2009;";

	## leggo codice fiscale e ragione sociale - core_gruppi
	$sql ="SELECT core_gruppi.codfisc as codice, core_gruppi.description AS rag_soc, core_gruppi.indirizzo AS via, lov_comuni_istat.cod_comISTAT AS istatC, lov_comuni_istat.cod_prov AS istatP, lov_comuni_istat.CAP as cap ";
	$sql.="FROM core_gruppi ";
	$sql.="JOIN lov_comuni_istat ON core_gruppi.ID_COM=lov_comuni_istat.ID_COM ";
	$sql.="WHERE core_gruppi.ID_GRP='".$SOGER->UserData["core_gruppiID_GRP"]."'";
	$FEDIT->SDBRead($sql,"DbRecordset",true,false);

		#info che user� di seguito
		$SL_codice   = spaceFiller(strtoupper($FEDIT->DbRecordset[0]["codice"]),16);
		$SL_istatP	 = zeroFiller($FEDIT->DbRecordset[0]["istatP"],3);
		$SL_istatC   = zeroFiller($FEDIT->DbRecordset[0]["istatC"],3);
		$SL_via		 = spaceFiller(strtoupper($FEDIT->DbRecordset[0]["via"]),30);
		$SL_civico	 = "      ";
		$SL_cap		 = zeroFiller($FEDIT->DbRecordset[0]["cap"],5);
		$SL_prefisso = "     ";
		$SL_telefono = "          ";
		$SL_LRC		 = "                         ";
		$SL_LRN		 = "                         ";
		

	## codice fiscale
	$AARecord.=spaceFiller(strtoupper($FEDIT->DbRecordset[0]["codice"]),16).";";

	## codice di identificazione univoca dell'unit� locale - PM mette 0
	$AARecord.=spaceFiller(strtoupper($SOGER->UserData["core_usersID_IMP"]),15).";";

	## codice istat attivit� svolta ( senza punti e lettere )
	$AARecord.="01   ;";

	## N� iscrizione Rep.Notizie Econ.Amm. (REA)
	$AARecord.="000000000;";

	## Totale addetti nell' unit� locale
	$AARecord.="00000;";

	## Descrizione della ragione sociale
	$AARecord.=spaceFiller(strtoupper($FEDIT->DbRecordset[0]["rag_soc"]),60).";";

	## leggo dati unit� locale - core_impianti
	$sql ="SELECT core_impianti.indirizzo as via, lov_comuni_istat.cod_comISTAT as istatC, lov_comuni_istat.cod_prov as istatP, lov_comuni_istat.CAP as cap ";
	$sql.="FROM core_impianti ";
	$sql.="JOIN lov_comuni_istat ON core_impianti.ID_COM=lov_comuni_istat.ID_COM ";
	$sql.="WHERE core_impianti.ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."'";
	$FEDIT->SDBRead($sql,"DbRecordset",true,false);

		## info che user� di seguito
		$UL_id = spaceFiller(strtoupper($SOGER->UserData["core_usersID_IMP"]),15);

	## ISTAT Provincia dell' unit� locale
	$AARecord.=zeroFiller($FEDIT->DbRecordset[0]["istatP"],3).";";

	## ISTAT Comune dell' unit� locale
	$AARecord.=zeroFiller($FEDIT->DbRecordset[0]["istatC"],3).";";

	## via dell'unit� locale
	$AARecord.=spaceFiller($FEDIT->DbRecordset[0]["via"],30).";";

	## civico unit� locale
	$AARecord.="      ;";

	## cap unit� locale
	$AARecord.=zeroFiller($FEDIT->DbRecordset[0]["cap"],5).";";

	## prefisso telefonico unit� locale
	$AARecord.="     ;";

	## numero telefonico unit� locale
	$AARecord.="          ;";

	## ISTAT provincia sede legale
	$AARecord.=$SL_istatP.";";

	## ISTAT comune sede legale
	$AARecord.=$SL_istatC.";";

	## via sede legale
	$AARecord.=$SL_via.";";

	## civico sede legale
	$AARecord.=$SL_civico.";";

	## cap sede legale
	$AARecord.=$SL_cap.";";

	## prefisso sede legale
	$AARecord.=$SL_prefisso.";";

	## numero sede legale
	$AARecord.=$SL_telefono.";";

	## cognome legale rappresentante
	$AARecord.=$SL_LRC.";";

	## nome legale rappresentante
	$AARecord.=$SL_LRN.";";

	## data compilazione
	$AARecord.=date('Ymd').";";


#BA#

	$sql ="SELECT lov_cer.COD_CER as cer, ID_SF, user_schede_rifiuti.ID_RIF, user_schede_rifiuti.produttore, user_schede_rifiuti.trasportatore, user_schede_rifiuti.destinatario, user_schede_rifiuti.ID_UMIS ";
	$sql.="FROM user_schede_rifiuti ";
	$sql.="JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
	$sql.="WHERE user_schede_rifiuti.ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."' AND user_schede_rifiuti.mud='0' ";
	$sql.="ORDER BY cer, ID_SF ";
	$FEDIT->SDBRead($sql,"DbRecordset",true,false);

	$BufCER		= 0;
	$BufSF		= 0;
	$SchedeRif	= array();
	$c			= 0;
	$counterRT  = 0;
	$counterDR  = 0;
	$counterTE  = 0;
	

	for($r=0;$r<count($FEDIT->DbRecordset);$r++){
		if($BufCER!=$FEDIT->DbRecordset[$r]['cer'] | ($BufCER==$FEDIT->DbRecordset[$r]['cer'] && $BufSF!=$FEDIT->DbRecordset[$r]['ID_SF'])){
			$c++;
			if($BufSF!=$FEDIT->DbRecordset[$r]['ID_SF']) $BufSF=$FEDIT->DbRecordset[$r]['ID_SF'];
			$BufCER=$FEDIT->DbRecordset[$r]['cer'];
			$SchedeRif[$c]['cer']=$FEDIT->DbRecordset[$r]['cer'];
			switch($FEDIT->DbRecordset[$r]['ID_SF']){
				case '1':
					$SchedeRif[$c]['snp']='0';
					$SchedeRif[$c]['sp']='1';
					$SchedeRif[$c]['fangoso']='0';
					$SchedeRif[$c]['liquido']='0';
					$SchedeRif[$c]['aeriforme']='0';
					break;
				case '2':
					$SchedeRif[$c]['snp']='1';
					$SchedeRif[$c]['sp']='0';
					$SchedeRif[$c]['fangoso']='0';
					$SchedeRif[$c]['liquido']='0';
					$SchedeRif[$c]['aeriforme']='0';
					break;
				case '3':
					$SchedeRif[$c]['snp']='0';
					$SchedeRif[$c]['sp']='0';
					$SchedeRif[$c]['fangoso']='1';
					$SchedeRif[$c]['liquido']='0';
					$SchedeRif[$c]['aeriforme']='0';
					break;
				case '4':
					$SchedeRif[$c]['snp']='0';
					$SchedeRif[$c]['sp']='0';
					$SchedeRif[$c]['fangoso']='0';
					$SchedeRif[$c]['liquido']='1';
					$SchedeRif[$c]['aeriforme']='0';
					break;
				}
			$SchedeRif[$c]['ID_RIF']=$FEDIT->DbRecordset[$r]['ID_RIF'];
 			}
		else{ // cer uguale e stato fisico uguale, aggiungo ID_RIF
			$SchedeRif[$c]['ID_RIF'].="|".$FEDIT->DbRecordset[$r]['ID_RIF'];
			}
		}

		//print_r($SchedeRif);


		$BARecord="";
		$counterBA=0;

		for($sr=1;$sr<=count($SchedeRif);$sr++){

			# costante
			$BARecord.="BA;2009;";

			# codice fiscale identificativo
			$BARecord.=$SL_codice.";";

			# codice identificativo unit� locale
			$BARecord.=$UL_id.";";

			# numero ordine progressivo scheda rif
			//$BARecord.=zerofiller($sr,4).";";
			$BARecord.="PROG;";

			# codice CER
			$BARecord.=$SchedeRif[$sr]['cer'].";";

			# stato fisico: solido polverulento
			$BARecord.=$SchedeRif[$sr]['sp'].";";

			# stato fisico: solido non polverulento
			$BARecord.=$SchedeRif[$sr]['snp'].";";

			# stato fisico: fangoso
			$BARecord.=$SchedeRif[$sr]['fangoso'].";";

			# stato fisico: liquido
			$BARecord.=$SchedeRif[$sr]['liquido'].";";

			# stato fisico: aeriforme
			$BARecord.=$SchedeRif[$sr]['aeriforme'].";";

			# schede rifiuto soger che confluiscono in scheda rif
			$ID_RIF=explode("|", $SchedeRif[$sr]['ID_RIF']);
			
			
			# formula: giacenza al 31/12 ( FKEdisponibilita ) + SUM(destino) oppure SUM(pesoN)    //scarichi

			for($s=0;$s<count($ID_RIF);$s++){
				# ricavo disponibilit� rifiuto
				$sql ="SELECT FKEdisponibilita, produttore, trasportatore, destinatario, ID_UMIS, peso_spec FROM user_schede_rifiuti WHERE ID_RIF='".$ID_RIF[$s]."' ";
				$FEDIT->SDBRead($sql,"DispoRif",true,false);
				$SchedeRif[$sr]['ID_UMIS']=$FEDIT->DispoRif[0]['ID_UMIS'];
				if($FEDIT->DispoRif[0]['peso_spec']==0) $peso_spec=1; else $peso_spec=$FEDIT->DispoRif[0]['peso_spec'];
				$SchedeRif[$sr]['peso_spec']=$peso_spec;

				if($FEDIT->DispoRif[0]['produttore']==1){
					$SchedeRif[$sr]['prodotto']=$FEDIT->DispoRif[0]['FKEdisponibilita'];
					$SchedeRif[$sr]['consegnato']=$FEDIT->DispoRif[0]['FKEdisponibilita'];
					}
				else{
					$SchedeRif[$sr]['prodotto']=0;
					$SchedeRif[$sr]['consegnato']=0;
					}
							
				if($FEDIT->DispoRif[0]['trasportatore']==1)
					$SchedeRif[$sr]['consegnato']=$FEDIT->DispoRif[0]['FKEdisponibilita'];
				else
					$SchedeRif[$sr]['consegnato']=0;
			
				if($FEDIT->DispoRif[0]['destinatario']==1)
					$SchedeRif[$sr]['ricevuto']=$FEDIT->DispoRif[0]['FKEdisponibilita'];
				else
					$SchedeRif[$sr]['ricevuto']=0;

			}

			# ricavo qta prodotte
			$sql ="SELECT user_movimenti.produttore, user_movimenti.pesoN as stimato, user_movimenti.PS_DESTINO as destino, user_movimenti.VER_DESTINO ";
			$sql.="FROM user_movimenti ";
			$sql.="WHERE user_movimenti.ID_RIF IN (".str_replace("|",",",$SchedeRif[$sr]['ID_RIF']).") AND ";
			$sql.="user_movimenti.TIPO='S' AND ";
			$sql.="user_movimenti.produttore='1' AND ";
			$sql.="(user_movimenti.DTMOV>='".str_replace("soger","",$FEDIT->DbServerData["db"])."-01-01' AND user_movimenti.DTMOV<='".str_replace("soger","",$FEDIT->DbServerData["db"])."-12-31') ";
			$FEDIT->SDBRead($sql,"DbRecordset",true,false);
			
			if(isset($FEDIT->DbRecordset)){
				for($c=0;$c<count($FEDIT->DbRecordset);$c++){
					if(!is_null($FEDIT->DbRecordset[$c]['destino']) && $FEDIT->DbRecordset[$c]['destino']>0){
						$SchedeRif[$sr]['prodotto']+=$FEDIT->DbRecordset[$c]['destino'];
						$SchedeRif[$sr]['consegnato']+=$FEDIT->DbRecordset[$c]['destino'];
						}
					else{
						$SchedeRif[$sr]['prodotto']+=$FEDIT->DbRecordset[$c]['stimato'];
						$SchedeRif[$sr]['consegnato']+=$FEDIT->DbRecordset[$c]['stimato'];
						}
					}
				}
			else
				$SchedeRif[$sr]['prodotto']+=0;


			# ricavo qta ricevute
			$sql ="SELECT user_movimenti.destinatario, user_movimenti.pesoN as stimato, user_movimenti.PS_DESTINO as destino, user_movimenti.VER_DESTINO ";
			$sql.="FROM user_movimenti ";
			$sql.="WHERE user_movimenti.ID_RIF IN (".str_replace("|",",",$SchedeRif[$sr]['ID_RIF']).") AND ";
			$sql.="user_movimenti.TIPO='C' AND ";
			$sql.="user_movimenti.destinatario='1' AND ";
			$sql.="(user_movimenti.DTMOV>='".str_replace("soger","",$FEDIT->DbServerData["db"])."-01-01' AND user_movimenti.DTMOV<='".str_replace("soger","",$FEDIT->DbServerData["db"])."-12-31') ";
			$FEDIT->SDBRead($sql,"DbRecordset",true,false);
			
			if(isset($FEDIT->DbRecordset)){
				for($c=0;$c<count($FEDIT->DbRecordset);$c++){
					if(!is_null($FEDIT->DbRecordset[$c]['destino']) && $FEDIT->DbRecordset[$c]['destino']>0)
						$SchedeRif[$sr]['ricevuto']+=$FEDIT->DbRecordset[$c]['destino'];
					else
						$SchedeRif[$sr]['ricevuto']+=$FEDIT->DbRecordset[$c]['stimato'];
					}
				}
			else
				$SchedeRif[$sr]['ricevuto']+=0;

			# ricavo qta trasportate
			$sql ="SELECT user_movimenti.trasportatore, user_movimenti.pesoN as stimato, user_movimenti.PS_DESTINO as destino, user_movimenti.VER_DESTINO ";
			$sql.="FROM user_movimenti ";
			$sql.="WHERE user_movimenti.ID_RIF IN (".str_replace("|",",",$SchedeRif[$sr]['ID_RIF']).") AND ";
			$sql.="user_movimenti.TIPO='S' AND ";
			$sql.="user_movimenti.trasportatore='1' AND ";
			$sql.="(user_movimenti.DTMOV>='".str_replace("soger","",$FEDIT->DbServerData["db"])."-01-01' AND user_movimenti.DTMOV<='".str_replace("soger","",$FEDIT->DbServerData["db"])."-12-31') ";
			$FEDIT->SDBRead($sql,"DbRecordset",true,false);
			
			if(isset($FEDIT->DbRecordset)){
				for($c=0;$c<count($FEDIT->DbRecordset);$c++){
					if(!is_null($FEDIT->DbRecordset[$c]['destino']) && $FEDIT->DbRecordset[$c]['destino']>0)
						$SchedeRif[$sr]['consegnato']+=$FEDIT->DbRecordset[$c]['destino'];
					else
						$SchedeRif[$sr]['consegnato']+=$FEDIT->DbRecordset[$c]['stimato'];
					}
				}
			else
				$SchedeRif[$sr]['consegnato']+=0;


	/*

		IL PESON NELLA TABELLA DEI MOVIMENTI E' GIA' IN KG, NON C'E' BISOGNO DI CONVERSIONI!

		switch($SchedeRif[$sr]['ID_UMIS']){
			default:
			case 1: //kg ...obsoleto
				$SchedeRif[$sr]['prodotto']=$SchedeRif[$sr]['prodotto'];
				$SchedeRif[$sr]['ricevuto']=$SchedeRif[$sr]['ricevuto'];
				$SchedeRif[$sr]['consegnato']=$SchedeRif[$sr]['consegnato'];
				break;
			case 2: //litri
				$SchedeRif[$sr]['prodotto']=$SchedeRif[$sr]['prodotto']*$SchedeRif[$sr]['peso_spec'];
				$SchedeRif[$sr]['ricevuto']=$SchedeRif[$sr]['ricevuto']*$SchedeRif[$sr]['peso_spec'];
				$SchedeRif[$sr]['consegnato']=$SchedeRif[$sr]['consegnato']*$SchedeRif[$sr]['peso_spec'];
			break;
			case 3://mc
				$SchedeRif[$sr]['prodotto']=$SchedeRif[$sr]['prodotto']*$SchedeRif[$sr]['peso_spec']*1000;
				$SchedeRif[$sr]['ricevuto']=$SchedeRif[$sr]['ricevuto']*$SchedeRif[$sr]['peso_spec']*1000;
				$SchedeRif[$sr]['consegnato']=$SchedeRif[$sr]['consegnato']*$SchedeRif[$sr]['peso_spec']*1000;
			break;
			}		
	*/

		# rifiuto prodotto in unit� locale
		$BARecord.=zeroFiller(number_format($SchedeRif[$sr]['prodotto'],3,",",""),11).";";
		# unit� di misura - Kg
		$BARecord.="1;";

		# rifiuto ricevuto da terzi
		$BARecord.=zeroFiller(number_format($SchedeRif[$sr]['ricevuto'],2,",",""),10).";";
		# unit� di misura - Kg
		$BARecord.="1;";

		# numero moduli RT allegati		
		if($SchedeRif[$sr]['ricevuto']=="0") 
			$BARecord.="00000;"; 
		else{
			$sql ="SELECT DISTINCT user_movimenti.ID_UIMP, user_aziende_produttori.description as azienda, user_aziende_produttori.codfisc, ";
			$sql.="user_impianti_produttori.description as indirizzo, lov_comuni_istat.cod_prov, lov_comuni_istat.cod_comISTAT as cod_com, ";
			$sql.="lov_comuni_istat.CAP ";
			$sql.="FROM user_movimenti ";
			$sql.="JOIN user_impianti_produttori ON user_movimenti.ID_UIMP=user_impianti_produttori.ID_UIMP ";
			$sql.="JOIN user_aziende_produttori ON user_impianti_produttori.ID_AZP=user_aziende_produttori.ID_AZP ";
			$sql.="JOIN lov_comuni_istat ON user_impianti_produttori.ID_COM=lov_comuni_istat.ID_COM ";
			$sql.="WHERE ID_RIF IN (".str_replace("|",",",$SchedeRif[$sr]['ID_RIF']).") AND ";
			if($SchedeRif[$sr]['ricevuto']>0)
				$sql.="user_movimenti.TIPO='C' AND ";
			$sql.="(user_movimenti.DTMOV>='".str_replace("soger","",$FEDIT->DbServerData["db"])."-01-01' AND user_movimenti.DTMOV<='".str_replace("soger","",$FEDIT->DbServerData["db"])."-12-31') ";

			$FEDIT->SDBRead($sql,"DbRecordset",true,false);
			if(isset($FEDIT->DbRecordset)){
				$RTrset=$FEDIT->DbRecordset;
				$BARecord.=zeroFiller(count($RTrset),5).";";
				}
			else
				$BARecord.="00000;";
			}

		# rifiuto prodotto fuori unit� locale
		$BARecord.="0000000,00;";
		# unit� di misura - Kg
		$BARecord.="1;";

		# numero moduli RE allegati
		$BARecord.="00000;";
		
		# rifiuto consegnato a terzi
		$BARecord.=zeroFiller(number_format($SchedeRif[$sr]['consegnato'],2,",",""),10).";";
		# unit� di misura - Kg
		$BARecord.="1;";

		# numero moduli DR allegati
		$sql ="SELECT DISTINCT user_movimenti.ID_UIMD, user_aziende_destinatari.description as azienda, user_aziende_destinatari.codfisc, ";
		$sql.="user_impianti_destinatari.description as indirizzo, lov_comuni_istat.cod_prov, lov_comuni_istat.cod_comISTAT as cod_com, ";
		$sql.="lov_comuni_istat.CAP ";
		$sql.="FROM user_movimenti ";
		$sql.="JOIN user_impianti_destinatari ON user_movimenti.ID_UIMD=user_impianti_destinatari.ID_UIMD ";
		$sql.="JOIN user_aziende_destinatari ON user_impianti_destinatari.ID_AZD=user_aziende_destinatari.ID_AZD ";
		$sql.="JOIN lov_comuni_istat ON user_impianti_destinatari.ID_COM=lov_comuni_istat.ID_COM ";
		$sql.="WHERE ID_RIF IN (".str_replace("|",",",$SchedeRif[$sr]['ID_RIF']).") AND ";
		if($SchedeRif[$sr]['prodotto']>0 | $SchedeRif[$sr]['consegnato']>0)
			$sql.="user_movimenti.TIPO='S' AND ";
		$sql.="(user_movimenti.DTMOV>='".str_replace("soger","",$FEDIT->DbServerData["db"])."-01-01' AND user_movimenti.DTMOV<='".str_replace("soger","",$FEDIT->DbServerData["db"])."-12-31') ";

		$FEDIT->SDBRead($sql,"DbRecordset",true,false);
		if(isset($FEDIT->DbRecordset)){
			$DRrset=$FEDIT->DbRecordset;
			$BARecord.=zeroFiller(count($DRrset),5).";";
			}
		else
			$BARecord.="00000;";

		# numero moduli TE allegati

		$sql ="SELECT DISTINCT user_movimenti.ID_UIMT, user_aziende_trasportatori.description AS azienda,
		user_aziende_trasportatori.codfisc, ";
		$sql.="user_impianti_trasportatori.description AS indirizzo, lov_comuni_istat.cod_prov, lov_comuni_istat.cod_comISTAT AS cod_com, ";
		$sql.="lov_comuni_istat.CAP ";
		$sql.="FROM user_movimenti ";
		$sql.="JOIN user_impianti_trasportatori ON user_movimenti.ID_UIMT=user_impianti_trasportatori.ID_UIMT ";
		$sql.="JOIN user_aziende_trasportatori ON user_impianti_trasportatori.ID_AZT=user_aziende_trasportatori.ID_AZT ";
		$sql.="JOIN lov_comuni_istat ON user_impianti_trasportatori.ID_COM=lov_comuni_istat.ID_COM ";
		$sql.="WHERE ID_RIF IN (".str_replace("|",",",$SchedeRif[$sr]['ID_RIF']).") AND ";
		if($SchedeRif[$sr]['prodotto']>0)
				$sql.="user_movimenti.TIPO='S' AND ";
		$sql.="(user_movimenti.DTMOV>='".str_replace("soger","",$FEDIT->DbServerData["db"])."-01-01' AND user_movimenti.DTMOV<='".str_replace("soger","",$FEDIT->DbServerData["db"])."-12-31') ";
		$FEDIT->SDBRead($sql,"DbRecordset",true,false);

		if(isset($FEDIT->DbRecordset)){
			$TErset=$FEDIT->DbRecordset;
			$BARecord.=zeroFiller(count($TErset),5).";";
			}
		else
			$BARecord.="00000;";


		# attivit� r/s svolte presso unit� locale in forma ordinaria
		if($FEDIT->DispoRif[0]['destinatario']=='1') $BARecord.="1;"; else $BARecord.="0;";

		# attivit� di smaltimento svolte presso unit� locale in forma speciale su ordinanza
		$BARecord.="0;";

		# l'impresa ha svolto attivit� di solo trasporto?
		if($FEDIT->DispoRif[0]['trasportatore']=='1' && $FEDIT->DispoRif[0]['produttore']=='0' && $FEDIT->DispoRif[0]['destinatario']=='0') $BARecord.="1;"; else $BARecord.="0;";

		# per eliminare schede qta 0
		if($SchedeRif[$sr]['prodotto']+$SchedeRif[$sr]['consegnato']+$SchedeRif[$sr]['ricevuto'] <= 0){
			unset($RTrset);
			unset($DRrset);
			unset($TErset);			
			$BARecord = substr($BARecord,0,strlen($BARecord)-146);
			$BBRecord="";
			}
		else{
			$BBRecord=$_SESSION['EOL'];
			$counterBA++;
			$BARecord = str_replace("PROG",zeroFiller($counterBA,4),$BARecord);
			}
		
	
		//$BBRecord=$_SESSION['EOL'];

		//if(isset($SchedeRif[$sr+1])) $BARecord.=$_SESSION['EOL'];


#BB# tanti quanti sono gli allegati RT DR TE del record BA
		$allegati=0;
		
#RT#

		$RTRecord="";
		if(isset($RTrset)){
			
			$counterRT+=count($RTrset);		

			for($rt=0;$rt<count($RTrset);$rt++){

				# costanti
				$RTRecord.="BB;2009;";

				# codice fiscale del dichiarante
				$RTRecord.=spaceFiller($SL_codice,16).";";

				# codice id unit� locale
				$RTRecord.=spaceFiller($UL_id,15).";";

				# numero progressivo scheda rif
				//$RTRecord.=zerofiller($sr,4).";";
				$RTRecord.=zerofiller($counterBA,4).";";

				# codice del rifiuto CER
				$RTRecord.=$SchedeRif[$sr]['cer'].";";

				# tipo allegato
				$RTRecord.="RT;";

				# numero progressivo allegato
				$RTRecord.=zeroFiller($rt+1 , 5 ) .";";
				$allegati+=$rt+1;

				# codice fiscale soggetto 
				$RTRecord.=spaceFiller($RTrset[$rt]['codfisc'],16).";";

				# nome o rag. sociale
				$RTRecord.=spaceFiller($RTrset[$rt]['azienda'],60).";";

				# codice istat provincia
				$RTRecord.=zeroFiller($RTrset[$rt]['cod_prov'],3).";";

				# codice istat comune
				$RTRecord.=zeroFiller($RTrset[$rt]['cod_com'],3).";";

				# via
				$RTRecord.=spaceFiller($RTrset[$rt]['indirizzo'],30).";";

				# civico
				$RTRecord.="      ;";

				# cap
				$RTRecord.=spaceFiller($RTrset[$rt]['CAP'],5).";";

				# quantit� dichiarata
				# somma di tutti i carichi con produttore Y
				$sql ="SELECT SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS dichiarato, user_schede_rifiuti.peso_spec, user_schede_rifiuti.ID_UMIS ";
				$sql.="FROM user_movimenti ";
				$sql.="JOIN user_schede_rifiuti ON user_movimenti.ID_RIF=user_schede_rifiuti.ID_RIF ";
				$sql.="WHERE user_movimenti.ID_UIMP='".$RTrset[$rt]['ID_UIMP']."' ";
				$sql.="AND user_movimenti.ID_RIF IN (".str_replace("|",",",$SchedeRif[$sr]['ID_RIF']).")";
				$sql.="AND user_movimenti.destinatario='1' ";
				$sql.="AND user_movimenti.TIPO='C' ";
				$sql.="AND (user_movimenti.DTMOV>='".str_replace("soger","",$FEDIT->DbServerData["db"])."-01-01' AND user_movimenti.DTMOV<='".str_replace("soger","",$FEDIT->DbServerData["db"])."-12-31') ";
				$sql.="GROUP BY user_movimenti.destinatario ";
				$FEDIT->SDBRead($sql,"DbRecordset",true,false);
				
				if(isset($FEDIT->DbRecordset)){
					/*
					switch($FEDIT->DbRecordset[0]['ID_UMIS']){
						default:
						case '1': // kg
							$dichiarato=$FEDIT->DbRecordset[0]['dichiarato'];
							break;
						case '2': // litri
							$dichiarato=$FEDIT->DbRecordset[0]['dichiarato']*$FEDIT->DbRecordset[0]['peso_spec'];
							break;
						case '3': // mc
							$dichiarato=$FEDIT->DbRecordset[0]['dichiarato']*$FEDIT->DbRecordset[0]['peso_spec']*1000;
							break;
						}
					$RTRecord.=zeroFiller(number_format($dichiarato,3,",",""),11).";";
					*/
					$dichiarato=$FEDIT->DbRecordset[0]['dichiarato'];
					$RTRecord.=zeroFiller(number_format($dichiarato,3,",",""),11).";";
					}
				else
					$RTRecord.="0000000,000;";

				# unit� di misura
				$RTRecord.="1;";

				# nome della nazione - se estero
				$RTRecord.="                    ;";

				# codice convenzione Basilea
				$RTRecord.="Y  ;";

				# codice regolamento CEE
				$RTRecord.="      ;";

				$RTRecord.=$_SESSION['EOL'];
				}
			}

		$BBRecord.=$RTRecord;


#DR#
		$DRRecord="";

		if(isset($DRrset)){

			$counterDR+=count($DRrset);

			for($dr=0;$dr<count($DRrset);$dr++){

				# costanti
				$DRRecord.="BB;2009;";

				# codice fiscale del dichiarante
				$DRRecord.=spaceFiller($SL_codice,16).";";

				# codice id unit� locale
				$DRRecord.=spaceFiller($UL_id,15).";";

				# numero progressivo scheda rif
				//$DRRecord.=zerofiller($sr,4).";";
				$DRRecord.=zerofiller($counterBA,4).";";

				# codice del rifiuto CER
				$DRRecord.=$SchedeRif[$sr]['cer'].";";

				# tipo allegato
				$DRRecord.="DR;";

				# numero progressivo allegato
				$DRRecord.=zeroFiller($dr+1 , 5) .";";
				$allegati+=$dr+1;

				# codice fiscale soggetto 
				$DRRecord.=spaceFiller($DRrset[$dr]['codfisc'],16).";";

				# nome o rag. sociale
				$DRRecord.=spaceFiller($DRrset[$dr]['azienda'],60).";";

				# codice istat provincia
				$DRRecord.=zeroFiller($DRrset[$dr]['cod_prov'],3).";";

				# codice istat comune
				$DRRecord.=zeroFiller($DRrset[$dr]['cod_com'],3).";";

				# via
				$DRRecord.=spaceFiller($DRrset[$dr]['indirizzo'],30).";";

				# civico
				$DRRecord.="      ;";

				# cap
				$DRRecord.=spaceFiller($DRrset[$dr]['CAP'],5).";";

				# quantit� dichiarata
				# somma di tutti gli scarichi con destinatario Y
				$sql ="SELECT SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS dichiarato, user_schede_rifiuti.peso_spec, user_schede_rifiuti.ID_UMIS ";
				$sql.="FROM user_movimenti ";
				$sql.="JOIN user_schede_rifiuti ON user_movimenti.ID_RIF=user_schede_rifiuti.ID_RIF ";
				$sql.="WHERE user_movimenti.ID_UIMD='".$DRrset[$dr]['ID_UIMD']."' ";
				$sql.="AND user_movimenti.ID_RIF IN (".str_replace("|",",",$SchedeRif[$sr]['ID_RIF']).")";
				$sql.="AND user_movimenti.produttore='1' ";
				$sql.="AND user_movimenti.TIPO='S' ";
				$sql.="AND (user_movimenti.DTMOV>='".str_replace("soger","",$FEDIT->DbServerData["db"])."-01-01' AND user_movimenti.DTMOV<='".str_replace("soger","",$FEDIT->DbServerData["db"])."-12-31') ";
				$sql.="GROUP BY user_movimenti.produttore ";

				$FEDIT->SDBRead($sql,"DbRecordset",true,false);
				
				if(isset($FEDIT->DbRecordset)){
					/*
					switch($FEDIT->DbRecordset[0]['ID_UMIS']){
						default:
						case '1': // kg
							$dichiarato=$FEDIT->DbRecordset[0]['dichiarato'];
							break;
						case '2': // litri
							$dichiarato=$FEDIT->DbRecordset[0]['dichiarato']*$FEDIT->DbRecordset[0]['peso_spec'];
							break;
						case '3': // mc
							$dichiarato=$FEDIT->DbRecordset[0]['dichiarato']*$FEDIT->DbRecordset[0]['peso_spec']*1000;
							break;
						}
					
					$DRRecord.=zeroFiller(number_format($dichiarato,3,",",""),11).";";
					*/
					$dichiarato=$FEDIT->DbRecordset[0]['dichiarato'];
					$DRRecord.=zeroFiller(number_format($dichiarato,3,",",""),11).";";
					}
				else
					$DRRecord.="0000000,000;";
	
				# unit� di misura
				$DRRecord.="1;";

				# nome della nazione - se estero
				$DRRecord.="                    ;";

				# codice convenzione Basilea
				$DRRecord.="Y  ;";

				# codice regolamento CEE
				$DRRecord.="      ;";


				$DRRecord.=$_SESSION['EOL'];
				}
			}

		$BBRecord.=$DRRecord;

#TE#

		$TERecord="";
		if(isset($TErset)){

			$counterTE+=count($TErset);

			for($te=0;$te<count($TErset);$te++){

				# costanti
				$TERecord.="BB;2009;";

				# codice fiscale del dichiarante
				$TERecord.=spaceFiller($SL_codice,16).";";

				# codice id unit� locale
				$TERecord.=spaceFiller($UL_id,15).";";

				# numero progressivo scheda rif
				//$TERecord.=zerofiller($sr,4).";";
				$TERecord.=zerofiller($counterBA,4).";";

				# codice del rifiuto CER
				$TERecord.=$SchedeRif[$sr]['cer'].";";

				# tipo allegato
				$TERecord.="TE;";

				# numero progressivo allegato
				$TERecord.=zeroFiller($te+1 , 5) .";";
				$allegati+=$te+1;

				# codice fiscale soggetto 
				$TERecord.=spaceFiller($TErset[$te]['codfisc'],16).";";

				# nome o rag. sociale
				$TERecord.=spaceFiller($TErset[$te]['azienda'],60).";";

				# codice istat provincia
				$TERecord.=zeroFiller($TErset[$te]['cod_prov'],3).";";

				# codice istat comune
				$TERecord.=zeroFiller($TErset[$te]['cod_com'],3).";";

				# via
				$TERecord.=spaceFiller($TErset[$te]['indirizzo'],30).";";

				# civico
				$TERecord.="      ;";

				# cap
				$TERecord.=spaceFiller($TErset[$te]['CAP'],5).";";

				# quantit� dichiarata
				# somma di tutti i carichi con trasportatore Y
				$sql ="SELECT SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS dichiarato, user_schede_rifiuti.peso_spec, user_schede_rifiuti.ID_UMIS ";
				$sql.="FROM user_movimenti ";
				$sql.="JOIN user_schede_rifiuti ON user_movimenti.ID_RIF=user_schede_rifiuti.ID_RIF ";
				$sql.="WHERE user_movimenti.ID_UIMT='".$TErset[$te]['ID_UIMT']."' ";
				$sql.="AND user_movimenti.ID_RIF IN (".str_replace("|",",",$SchedeRif[$sr]['ID_RIF']).")";
				//$sql.="AND user_movimenti.produttore='1' ";
				//$sql.="AND user_movimenti.TIPO='C' ";
				$sql.="AND (user_movimenti.DTMOV>='".str_replace("soger","",$FEDIT->DbServerData["db"])."-01-01' AND user_movimenti.DTMOV<='".str_replace("soger","",$FEDIT->DbServerData["db"])."-12-31') ";
				$sql.="GROUP BY user_movimenti.ID_RIF ";
				$FEDIT->SDBRead($sql,"DbRecordset",true,false);
				
				if(isset($FEDIT->DbRecordset)){
					/*
					switch($FEDIT->DbRecordset[0]['ID_UMIS']){
						default:
						case '1': // kg
							$dichiarato=$FEDIT->DbRecordset[0]['dichiarato'];
							break;
						case '2': // litri
							$dichiarato=$FEDIT->DbRecordset[0]['dichiarato']*$FEDIT->DbRecordset[0]['peso_spec'];
							break;
						case '3': // mc
							$dichiarato=$FEDIT->DbRecordset[0]['dichiarato']*$FEDIT->DbRecordset[0]['dichiarato']*1000;
							break;
						}
					
					$TERecord.=zeroFiller(number_format($dichiarato,3,",",""),11).";";
					*/
					$dichiarato=$FEDIT->DbRecordset[0]['dichiarato'];
					$TERecord.=zeroFiller(number_format($dichiarato,3,",",""),11).";";
					}
				else
					$TERecord.="0000000,000;";

				# unit� di misura
				$TERecord.="1;";

				# nome della nazione - se estero
				$TERecord.="                    ;";

				# codice convenzione Basilea
				$TERecord.="Y  ;";

				# codice regolamento CEE
				$TERecord.="      ;";

				$TERecord.=$_SESSION['EOL'];
				}
			}

		$BBRecord.=$TERecord;

		$BARecord.=$BBRecord;

		
		} //chiude for schede BA // $SchedeRif

//print_r($SchedeRif);

#AB#

	# costante
	$ABRecord ="AB;2009;";

	# codice fiscale sede legale
	$ABRecord.=$SL_codice.";";

	# codice identificativo unit� locale
	$ABRecord.=$UL_id.";";

	# tipo di versamento - 0=ND 1=singolo 2=cumulativo
	$ABRecord.="0;";

	# numero di schede rif
	//$ABRecord.=zeroFiller(count($SchedeRif),6).";";
	$ABRecord.=zeroFiller($counterBA,6).";";

	# numero moduli RT
	$ABRecord.=zeroFiller($counterRT,6).";";

	# numero moduli RE - 0
	$ABRecord.="000000;";

	# numero moduli TE
	$ABRecord.=zeroFiller($counterTE,6).";";

	# numero moduli DR
	$ABRecord.=zeroFiller($counterDR,6).";";

	# numero moduli GESTIONE
	$ABRecord.="0000;";

	# numero moduli ART13
	$ABRecord.="0000;";

	# numero schede CS
	$ABRecord.="0000;";

	# scheda RU?
	$ABRecord.="0;";

	# numero moduli RST
	$ABRecord.="0000;";

	# numero moduli DRU
	$ABRecord.="0000;";

	# scheda costi COSGE
	$ABRecord.="0;";

	# scheda costi CG
	$ABRecord.="0;";

	# numero moduli MDCR
	$ABRecord.="0000;";

	# numero schede INT
	$ABRecord.="0000;";

	# numero moduli UO
	$ABRecord.="0000;";

	# numero moduli UD
	$ABRecord.="0000;";

	# sez. imballaggi - scheda materiali
	$ABRecord.="0;";

	# sez. imballaggi - scheda tipologie
	$ABRecord.="0;";

	# sez. imballaggi - scheda riutilizzo
	$ABRecord.="0;";

	# campi relativi alla scheda COSGE
	$ABRecord.="0000000000;0000000000;0000000000;0000000000;";







	
	
	
	
	## unisco record in MUD2009
	$MUD2009 =$AARecord.$_SESSION['EOL'];
	$MUD2009.=$ABRecord.$_SESSION['EOL'];
	$MUD2009.=$BARecord.$_SESSION['EOL'];

	## devo salvare $MUD2009 in file di testo temporaneo
	$tmpMUDfile="../__updates/TMPMUD.txt";
	$tmpMUD=fopen($tmpMUDfile, "w+");
	fwrite($tmpMUD, $MUD2009);

	## conto le righe tranne XX.. attenzione a non mettere \n\r in ultima riga, oppure -2
	$recordTotali=count(file($tmpMUDfile));
	
	## trasformo testo MUD2009 in array ogni \n\r
	$records=explode($_SESSION['EOL'], $MUD2009);

	## array tipologia righe
	$recordType = array("AA", "AB", "AC", "BA", "BB", "BC", "BD", "BE", "CA", "CB", "CX", "CC", "CG", "CH", "DA", "DB", "EA", "FA");
	$recordTypeCounter = array();
	foreach($recordType as $type){
		$recordTypeCounter[$type]=0;
		}
	

	## dimensioni del file
	$_SESSION['MudFileSize'] = filesize($tmpMUDfile);

	fclose($tmpMUD); 
	unlink($tmpMUDfile);

#XX#
		
	## costanti
	$XXRecord = "XX;3.00/98;01;";

	## data e ora
	$data=date("Ymd");
	$ora=date("His");
	$XXRecord.= $data.";".$ora.";";
	
	## numero record totali
	$XXRecord.= zeroFiller($recordTotali,8).";";

	## numero record per type
	foreach($recordType as $type){
		for($r=0;$r<count($records);$r++){
			if($type==substr($records[$r],0,2))
				$recordTypeCounter[$type]++;
			}
		}

	## integro record XX
	foreach($recordTypeCounter as $counter){
		$XXRecord.= zeroFiller($counter,5).";";
		}

	## intestatario MUD - info da user_gruppi
	$sql = "SELECT codfisc, core_gruppi.description AS rag_soc, indirizzo, lov_comuni_istat.CAP, lov_comuni_istat.description AS comune, lov_comuni_istat.shdes_prov AS provincia ";
	$sql.= "FROM core_gruppi ";
	$sql.= "JOIN lov_comuni_istat ON core_gruppi.ID_COM=lov_comuni_istat.ID_COM ";
	$sql.= "WHERE core_gruppi.ID_GRP='".$SOGER->UserData["core_gruppiID_GRP"]."'";
	$FEDIT->SDBRead($sql,"DbRecordset",true,false);

	## integro record XX
	$XXRecord.= spaceFiller(strtoupper($FEDIT->DbRecordset[0]["codfisc"]),16).";";		# codice fiscale
	$XXRecord.= spaceFiller(strtoupper($FEDIT->DbRecordset[0]["rag_soc"]),60).";";		# ragione sociale
	$XXRecord.= spaceFiller(strtoupper($FEDIT->DbRecordset[0]["indirizzo"]),30).";";	# via
	$XXRecord.= "          ;";															# civico
	$XXRecord.= spaceFiller(strtoupper($FEDIT->DbRecordset[0]["CAP"]),5).";";			# cap
	$XXRecord.= spaceFiller(strtoupper($FEDIT->DbRecordset[0]["comune"]),30).";";		# citt�
	$XXRecord.= spaceFiller(strtoupper($FEDIT->DbRecordset[0]["provincia"]),2).";";		# sigla provincia
	$XXRecord.= "     ;";																# prefisso
	$XXRecord.= "          ;";															# telefono
	$XXRecord.= "                              ;";										# riservato (?)
	$XXRecord.= "00000;";																# numero di nastri a cartuccia consegnati
	$XXRecord.= "00000;";																# numero di nastri a bobina consegnati
	$XXRecord.= "00000;";																# numero di dischetti consegnati
	$XXRecord.= "00000;";																# numero attestati versamento
	$XXRecord.= "00000;";																# numero stampe sezioni anagrafiche SA1 e SA2




	$ultimateMUD2009 =$XXRecord.$_SESSION['EOL'];
	$ultimateMUD2009.=$AARecord.$_SESSION['EOL'];
	$ultimateMUD2009.=$ABRecord.$_SESSION['EOL'];
	$ultimateMUD2009.=$BARecord;

	## Restituisce contenuto MUD2009.001

	$FEDIT->DbServerData["db"]="soger2010";
	//$FEDIT->DbServerData["db"]="soger2009";
	return  strtoupper($ultimateMUD2009);
	}


function createControlFile(){
	
	$CONTROL = "";

	## Valori costanti
	$CONTROL.= "2.00;MUD2009;";

	## Dimensioni in bytes del file 9int 0dec
	$bytes=getMudFileSize();
	$CONTROL.= $bytes.";";

	## data AAAAMMGG e ora HH:MM:SS
	$data=date("Ymd");
	$ora=date("H:i:s");
	$CONTROL.=$data.";".$ora.";";

	## numero dischetti utilizzati -costante-
	$disk="001;";
	$CONTROL.=$disk;

	## numero dischetto corrente -costante-
	$currentDisk="001;";
	$CONTROL.=$currentDisk;

	## Restituisce contenuto CONTROL.001
	return $CONTROL;
	
	## DEBUG: strlen($CONTROL) deve restituire 49.

	}

function getMudFileSize(){
	return zeroFiller($_SESSION['MudFileSize'],9);
	}

function zeroFiller($value,$char,$int=true){
	# $int � true o false a seconda che sia int o float
	//$filled = sprintf("%01.2f", $value);
	if($int)
		$filled = str_pad(substr($value,0,$char), $char, "0", STR_PAD_LEFT);
	return $filled;
	}

function spaceFiller($value,$char){
	$toReplace=array('�','�','�','�','�','�');
	$replaceWith=array("E'", "E'", "A'", "O'", "I'", "U'");
	$filled = str_pad(substr(str_replace($toReplace,$replaceWith,$value),0,$char), $char, " ", STR_PAD_RIGHT);
	return $filled;
	}

?>