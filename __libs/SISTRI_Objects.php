<?php

class RegistrazioneCrono_Base{

	var $descrizioneRifiuto;				// descrizioneRifiuto - string
	var $numeroNotifica;					// ?? - string
	var $numeroSerieSpedizione;				// ?? - string
	var $annotazioni;						// annotazioni - string
	var $idSISSede_impiantoOrigine;		 	// id impianto produttore - string
	var $idSISSede_impiantoDestinazione;	// id impianto destinatario - string
	var $descrizioneAltroStatoFisico;		// stato fisico custom - string
	var $idSISSede_consegnatoA;				// ?? - string
	var $statoFisicoRifiuto;				// OBJ Catalogo - stato fisico
	var $codiceCerIIILivello;				// OBJ Catalogo - Codice CER
	var $codRec_1013;						// OBJ Catalogo - ??
	var $categoriaRAEE;						// OBJ Catalogo - Categoria RAEE
	var $tipologiaRAEE;						// OBJ Catalogo - Tipologia RAEE
	var $operazioneImpianto;				// OBJ Catalogo - Operazione eseguita dall' impianto
	var $caratteristicaPericolo;			// OBJ Catalogo - ??
	var $quantita;							// OBJ LongNumber - Peso in milligrammi
	var $numeroVeicoliConferiti;			// OBJ LongNumber - Numero veicoli
	var $flagVeicoli_Dlgs_209_2003;			// OBJ Flag - Ci sono veicoli del DL?
	var $flagVeicoli_Dlgs_Art231_152_2006;	// OBJ Flag - Ci sono veicoli del DL?
	var $flagRiutilizzoAppIntera;			// OBJ Flag - ??
	var $flagOpRecuperoEnergia;				// OBJ Flag - ??
	var $flagOpRecuperoMateria;				// OBJ Flag - ??

	function set_RegistrazioneCrono_Base($desc, $n_notifica, $n_spedizione, $note, $imp_origine, $imp_destino, $stato_fisico, $imp_consegna){
		$this->descrizioneRifiuto=$desc;
		$this->numeroNotifica=$n_notifica;
		$this->numeroSerieSpedizione=$n_spedizione;
		$this->annotazioni=$note;
		$this->idSISSede_impiantoOrigine=$imp_origine;
		$this->idSISSede_impiantoDestinazione=$imp_destino;
		$this->descrizioneAltroStatoFisico=$stato_fisico;
		$this->idSISSede_consegnatoA=$imp_consegna;
		}

	function set_statoFisicoRifiuto($objStatoFisico){
		$this->statoFisicoRifiuto=$objStatoFisico;
		}

	function set_codiceCerIIILivello($objCER){
		$this->codiceCerIIILivello=$objCER;
		}

	function set_codRec_1013($objREC1013){
		$this->codRec_1013=$objREC1013;
		}

	function set_categoriaRAEE($objRAEE_CAT){
		$this->categoriaRAEE=$objRAEE_CAT;
		}

	function set_tipologiaRAEE($OBJ_RAEE_TYP){
		$this->tipologiaRAEE=$OBJ_RAEE_TYP;
		}

	function set_operazioneImpianto($OBJ_OP_IMP){
		$this->operazioneImpianto=$OBJ_OP_IMP;
		}

	function set_caratteristicaPericolo($OBJ_CAR_PERIC){
		$this->caratteristicaPericolo=$OBJ_CAR_PERIC;
		}

	function set_quantita($OBJ_QTA){
		$this->quantita=$OBJ_QTA;
		}

	function set_numeroVeicoliConferiti($OBJ_N_VEICOLI){
		$this->numeroVeicoliConferiti=$OBJ_N_VEICOLI;
		}

	function set_flagVeicoli_Dlgs_209_2003($OBJ_Veicoli_Dlgs_209_2003){
		$this->flagVeicoli_Dlgs_209_2003=$OBJ_Veicoli_Dlgs_209_2003;
		}

	function set_flagVeicoli_Dlgs_Art231_152_2006($OBJ_Veicoli_Dlgs_Art231_152_2006){
		$this->flagVeicoli_Dlgs_Art231_152_2006=$OBJ_Veicoli_Dlgs_Art231_152_2006;
		}

	function set_flagRiutilizzoAppIntera($OBJ_RiutilizzoAppIntera){
		$this->flagRiutilizzoAppIntera=$OBJ_RiutilizzoAppIntera;
		}

	function set_flagOpRecuperoEnergia($OBJ_OpRecuperoEnergia){
		$this->flagOpRecuperoEnergia=$OBJ_OpRecuperoEnergia;
		}

	function set_flagOpRecuperoMateria($OBJ_OpRecuperoMateria){
		$this->flagOpRecuperoMateria=$OBJ_OpRecuperoMateria;
		}


	}


class Catalogo{

	var $idCatalogo;						// id del catalogo sistri
	var $description;						// voce del catalogo

	function set_Catalogo($idCatalogo, $description){
		$this->idCatalogo=$idCatalogo;
		$this->description=$description;
		}

	}

class LongNumber{

	var $long;								// quantita / numero intero


	function set_LongNumber($long){
		$this->long=$long;
		}

	}

class Flag{
	
	var $boolean;							// true / false come stringhe!

	function set_Flag($boolean){
		$this->boolean=$boolean;
		}

	}



?>