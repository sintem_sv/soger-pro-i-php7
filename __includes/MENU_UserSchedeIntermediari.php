<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require("__includes/COMMON_wakeForgEdit.php");
require("USER_SchedeIntermediariIDcheck.php");
//print_r($this->AppLocation);
$FEDIT->SDbRead($sql,"DbRecordSet",true,false);

//print_r($FEDIT->DbRecordSet[0]);

if($FEDIT->DbRecsNum==0) {
	$urlRS   = "#";
	$urlIMP  = "javascript:alert('Inserire%20prima%20la%20ragione%20sociale')";
	$urlAUTH = "javascript:alert('Inserire%20prima%20i%20dati%20Impianto')";
	$urlECO  = "javascript:alert('Inserire%20prima%20le%20autorizzazioni')";
	$urlALL  = "javascript:alert('Inserire%20prima%20la%20ragione%20sociale')";

} else {
	$urlRS = "__scripts/SOGER_standardMenu.php?area=UserNuovoIntermediario&amp;table=user_aziende_intermediari&amp;pri=ID_AZI&amp;filter=" . $FEDIT->DbRecordSet[0]["ID_AZI"] . "&hash=" . MakeUrlHash("user_aziende_intermediari","ID_AZI",$FEDIT->DbRecordSet[0]["ID_AZI"]);
	
	$urlALL = "__scripts/SOGER_standardMenu.php?area=UserNuovoIntermediarioAllegati&amp;table=user_aziende_intermediari&amp;pri=ID_AZI&amp;filter=" . $FEDIT->DbRecordSet[0]["ID_AZI"] . "&hash=" . MakeUrlHash("user_aziende_intermediari","ID_AZI",$FEDIT->DbRecordSet[0]["ID_AZI"]);


	if(!is_null($FEDIT->DbRecordSet[0]["ID_UIMI"])) {
		$urlIMP = "__scripts/SOGER_standardMenu.php?area=UserNuovoIntermediarioImpianto&amp;table=user_impianti_intermediari&amp;pri=ID_UIMI&amp;filter=" . $FEDIT->DbRecordSet[0]["ID_UIMI"] . "&hash=" . MakeUrlHash("user_impianti_intermediari","ID_UIMI",$FEDIT->DbRecordSet[0]["ID_UIMI"]);
		if(!is_null($FEDIT->DbRecordSet[0]["ID_AUTHI"])) {
			$urlAUTH = "__scripts/SOGER_standardMenu.php?area=UserNuovoIntermediarioAutorizzazioni&amp;table=user_autorizzazioni_interm&amp;pri=ID_AUTHI&amp;filter=" . $FEDIT->DbRecordSet[0]["ID_AUTHI"] . "&hash=" . MakeUrlHash("user_autorizzazioni_interm","ID_AUTHI",$FEDIT->DbRecordSet[0]["ID_AUTHI"]);
			if(!is_null($FEDIT->DbRecordSet[0]["ID_CNT_I"])) {
				$urlECO = "__scripts/SOGER_standardMenu.php?area=UserIntermediarioContratti&amp;table=user_contratti_intermediari&amp;pri=ID_AZI&amp;filter=" . $FEDIT->DbRecordSet[0]["ID_AZI"]."&amp;hash=" . MakeUrlHash("user_contratti_intermediari","ID_AZI",$FEDIT->DbRecordSet[0]["ID_AZI"]);
				}
			else{
				$_SESSION['ID_AZI_CONTRACT']=$_SESSION["FGE_IDs"]["user_aziende_intermediari"];
				$urlECO = "__scripts/SOGER_standardMenu.php?area=UserNuovoContrattoIntermediario";
				}
		} else {
			$urlAUTH = "__scripts/status.php?area=UserNuovoIntermediarioAutorizzazioni";
			$urlECO = "javascript:alert('Inserire%20prima%20le%20autorizzazioni')";
		}
	} else {
		$urlAUTH = "javascript:alert('Inserire%20prima%20i%20dati%20Impianto')";
		$urlECO = "javascript:alert('Inserire%20prima%20le%20autorizzazioni')";
		$urlIMP = "__scripts/status.php?area=UserNuovoIntermediarioImpianto";
	}
}
?>


<?php
//print_r($SOGER->AppLocation);
switch($SOGER->AppLocation){
	case "UserNuovoIntermediario":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-ON.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_userSchedeProduttoriAUT-OFF.gif";
		$BT_contratti		= "BT_contratti-OFF.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";
		break;
	case "UserNuovoIntermediarioImpianto":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-ON.gif";
		$BT_autorizzazioni	= "BT_userSchedeProduttoriAUT-OFF.gif";
		$BT_contratti		= "BT_contratti-OFF.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";
		break;
	case "UserNuovoIntermediarioAutorizzazioni":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_userSchedeProduttoriAUT-ON.gif";
		$BT_contratti		= "BT_contratti-OFF.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";
		break;
	case "UserIntermediarioContratti":
	case "UserNuovoContrattoIntermediario":
	case "UserIntermediarioContrattoCondizioni":
	case "UserCondizioneContrattoIntermediario":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_userSchedeProduttoriAUT-OFF.gif";
		$BT_contratti		= "BT_contratti-ON.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";
		break;
	case "UserNuovoIntermediarioAllegati":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_userSchedeProduttoriAUT-OFF.gif";
		$BT_contratti		= "BT_contratti-OFF.gif";
		$BT_allegati		= "BT_allegati-ON.gif";
		break;

	}
?>


<a href="<?php echo $urlRS ?>" title="Ragione Sociale"><img src="__css/<?php echo $BT_ragione; ?>" class="NoBorder" alt="Ragione Sociale" /></a>
<a href="<?php echo $urlIMP ?>" title="Dati Impianto"><img src="__css/<?php echo $BT_impianti; ?>" class="NoBorder" alt="Dati Impianto"/></a>
<a href="<?php echo $urlAUTH ?>" title="Autorizzazioni"><img src="__css/<?php echo $BT_autorizzazioni; ?>" class="NoBorder" alt="Autorizzazioni"/></a>
<?php if($this->UserData["core_impiantiMODULO_ECO"]=="1" AND $this->UserData["core_usersG3"]=="1"){ ?>
<a href="<?php echo $urlECO ?>" title="Contratti"><img src="__css/<?php echo $BT_contratti; ?>" class="NoBorder" alt="Contratti"/></a>
<?php } ?>

<a href="<?php echo $urlALL ?>" title="Allegati"><img src="__css/<?php echo $BT_allegati; ?>" class="NoBorder" alt="Allegati"/></a>
