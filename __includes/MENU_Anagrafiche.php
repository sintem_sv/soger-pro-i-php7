<?php
switch($this->AppLocation){
	case "UserSchedeProduttori":
		$BT_schede			= "BT_schede_archivio-ON.gif";
		$BT_comuni			= "BT_comuni-OFF.gif";
		$area_schede		= $this->AppLocation;
		$area_comuni		= "UserSchedeComuniP";
		break;
	case "UserSchedeTrasportatori":
		$BT_schede			= "BT_schede_archivio-ON.gif";
		$BT_comuni			= "BT_comuni-OFF.gif";
		$area_schede		= $this->AppLocation;
		$area_comuni		= "UserSchedeComuniT";
		break;
	case "UserSchedeDestinatari":
		$BT_schede			= "BT_schede_archivio-ON.gif";
		$BT_comuni			= "BT_comuni-OFF.gif";
		$area_schede		= $this->AppLocation;
		$area_comuni		= "UserSchedeComuniD";
		break;
	case "UserSchedeIntermediari":
		$BT_schede			= "BT_schede_archivio-ON.gif";
		$BT_comuni			= "BT_comuni-OFF.gif";
		$area_schede		= $this->AppLocation;
		$area_comuni		= "UserSchedeComuniI";
		break;
	case "UserSchedeComuniP":
		$BT_schede			= "BT_schede_archivio-OFF.gif";
		$BT_comuni			= "BT_comuni-ON.gif";
		$area_schede		= "UserSchedeProduttori";
		$area_comuni		= $this->AppLocation;
		break;
	case "UserSchedeComuniT":
		$BT_schede			= "BT_schede_archivio-OFF.gif";
		$BT_comuni			= "BT_comuni-ON.gif";
		$area_schede		= "UserSchedeTrasportatori";
		$area_comuni		= $this->AppLocation;
		break;
	case "UserSchedeComuniD":
		$BT_schede			= "BT_schede_archivio-OFF.gif";
		$BT_comuni			= "BT_comuni-ON.gif";
		$area_schede		= "UserSchedeDestinatari";
		$area_comuni		= $this->AppLocation;
		break;
	case "UserSchedeComuniI":
		$BT_schede			= "BT_schede_archivio-OFF.gif";
		$BT_comuni			= "BT_comuni-ON.gif";
		$area_schede		= "UserSchedeIntermediari";
		$area_comuni		= $this->AppLocation;
		break;
	}
?>

<a href="__scripts/status.php?area=<?php echo $area_schede; ?>" title="Schede in archivio">
<img src="__css/<?php echo $BT_schede; ?>" class="NoBorder" alt="Schede in archivio" /></a>

<a href="__scripts/status.php?area=<?php echo $area_comuni; ?>" title="Schede comuni">
<img src="__css/<?php echo $BT_comuni; ?>" class="NoBorder" alt="Schede comuni" /></a>