<div class="divName"><span>GESTIONE REGISTRI</span></div>

<?php

require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$TableName="user_movimenti";
$LegamiSISTRI	= false;
require("__scripts/MovimentiRifMovCarico.php");
require_once("__includes/COMMON_sleepForgEdit.php");
require("__includes/USER_RegistroFiscaleSiNo.inc");

$year=substr($_SESSION["DbInUse"], -4);

function FirstLastWeek($data) {

list($giorno, $mese, $anno) = explode('/', $data);

$w = date('w', mktime(0,0,0, $mese, $giorno, $anno));
$day['W'] = date('W', mktime(0,0,0, $mese, $giorno, $anno));

$giorni=array(0=>'Domenica', 1=>'Luned�', 2=>'Marted�',3=>'Mercoled�',
              4=>'Gioved�', 5=>'Venerd�', 6=>'Sabato');

$day['giorno'] = $giorni[$w];
$day['anno'] = $anno;

if($w == 0 )  {
      $day['lunedi']   = date('d/m/Y', mktime(0,0,0, $mese, $giorno - 6, $anno));
      $day['domenica'] = date('d/m/Y', mktime(0,0,0, $mese, $giorno, $anno));
      }  else {
              $day['lunedi']   = date('d/m/Y', mktime(0,0,0, $mese, $giorno - $w + 1, $anno));
              $day['domenica'] = date('d/m/Y', mktime(0,0,0, $mese, $giorno - $w + 7, $anno));
              }
return $day;
}

$optList="";



//$start	= '01/01/'.date('Y');
//$end	= '31/12/'.date('Y');

$start	= '01/01/'.$year;
$end	= '31/12/'.$year;

$startCompare	= mktime(0,0,0, 1, 1, $year);
$endCompare		= mktime(0,0,0, 12, 31, $year);

while($startCompare<=$endCompare){
	$day = FirstLastWeek(date('d/m/Y', $startCompare));
	$optList.="<option value=\"$day[lunedi]|$day[domenica]\" ";

	# start day
	list($giorno, $mese, $anno) = explode('/', $day['lunedi']);
	$lun=mktime(0,0,0, $mese, $giorno, $anno); 
	
	# end day
	list($giorno, $mese, $anno) = explode('/', $day['domenica']);
	$dom=mktime(0,0,0, $mese, $giorno, $anno); 

	# today
	$today	= mktime(0,0,0, date('m'), date('d'), $year);

	if($today>=$lun && $today<=$dom) $optList.=" selected ";

	$optList.=">Settimana $day[W]: dal $day[lunedi] al $day[domenica]";
	$optList.="</option>";
	$startCompare += 60*60*24*7;
	}

## ha senso visulizzare settimane successive alla corrente? ##

?> 






<form name="StampaRegistroInd2" method="get" action="" style="width: 90%">
<fieldset class="FGEfieldset">
	<legend class="FGElegend" style="font-size:x-small;">Stampa Registro Industriale Settimanale</legend>
<!--
<table style="width:100%;">

<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="da" class="">Da movimento n�</label></td>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><input type="text" value="1" id="da" name="da" size="4" class="FGEinput" tabindex="50"/></td>
</tr>

<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="a" class="">A movimento n�</label></td>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><input type="text" value="" id="a" name="a" size="4" class="FGEinput" tabindex="51"/></td>
</tr>

</table>
-->
<select id="week_settimanale" name="week_settimanale"><?php echo $optList; ?></select>

<br/><br/>
<input type="hidden" id="author" name="author" value="<?php echo $SOGER->UserData["core_usersdescription"]; ?>" />
<input type="button" class="FGEbutton" tabindex="55" value="stampa registro settimanale" onclick="javascript:RegistroIndustriale2(StampaRegistroInd2);"/>

</fieldset>
</form>