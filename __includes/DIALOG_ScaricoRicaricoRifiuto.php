<?php

// elenco rifiuti
$sql = "SELECT ID_RIF,descrizione,cod_pro,COD_CER,ClassificazioneHP,approved FROM user_schede_rifiuti JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
$sql.= "WHERE ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "' AND ".$SOGER->UserData["workmode"]."=1 ";
$sql.= "ORDER BY COD_CER ASC;";
$FEDIT->SdbRead($sql,"DbRecordSetRifImp",true,false);
$optListID_RIF = '';
for($r=0;$r<count($FEDIT->DbRecordSetRifImp);$r++)
	$optListID_RIF.="<option approved=\"".$FEDIT->DbRecordSetRifImp[$r]['approved']."\" ClassificazioneHP=\"".$FEDIT->DbRecordSetRifImp[$r]['ClassificazioneHP']."\" value=\"".$FEDIT->DbRecordSetRifImp[$r]['ID_RIF']."\">".$FEDIT->DbRecordSetRifImp[$r]['COD_CER']." ".$FEDIT->DbRecordSetRifImp[$r]['cod_pro']." ".$FEDIT->DbRecordSetRifImp[$r]['descrizione']."</option>\n";

// elenco unit� locali
$sql = "SELECT user_impianti_produttori.ID_AZP, user_impianti_produttori.ID_UIMP, user_aziende_produttori.description AS RagioneSociale, user_impianti_produttori.description AS Impianto, codfisc, lov_comuni_istat.description AS Comune, shdes_prov AS Provincia ";
$sql.= "FROM user_impianti_produttori ";
$sql.= "JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM=user_impianti_produttori.ID_COM ";
$sql.= "JOIN user_aziende_produttori ON user_aziende_produttori.ID_AZP=user_impianti_produttori.ID_AZP ";
$sql.= "WHERE user_aziende_produttori.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "' AND user_aziende_produttori.".$SOGER->UserData["workmode"]."=1 ";
$sql.= "AND user_aziende_produttori.approved=1 AND user_impianti_produttori.approved=1 ";
$sql.= "ORDER BY RagioneSociale, Impianto;";
$FEDIT->SdbRead($sql,"DbRecordSetImp",true,false);
$optListID_UIMP = '';
for($r=0;$r<count($FEDIT->DbRecordSetImp);$r++){
	$label = $FEDIT->DbRecordSetImp[$r]['RagioneSociale']." - ".$FEDIT->DbRecordSetImp[$r]['Impianto']." ".$FEDIT->DbRecordSetImp[$r]['Comune']." (".$FEDIT->DbRecordSetImp[$r]['Provincia'].")";
	$optListID_UIMP.="<option ID_AZP=\"".$FEDIT->DbRecordSetImp[$r]['ID_AZP']."\" codfisc=\"".$FEDIT->DbRecordSetImp[$r]['codfisc']."\" value=\"".$FEDIT->DbRecordSetImp[$r]['ID_UIMP']."\">".$label."</option>\n";
	}


$dialog = <<<DIALOG

<div id="Popup_ScaricoRicaricoRifiuto" title="">

	<table cellpadding="0" cellspacing="0" style="width:460px;">

		<tr>
			<td colspan="3">La procedura consente di azzerare la giacenza di un rifiuto in carico che ha subito una variazione di classificazione. Il sistema provveder� ad eseguire una operazione di scarico interno e ricaricher� il rifiuto sulla scheda riclassificata. Tali movimenti non potranno essere modificati o cancellati dal registro.<br /><br /></td>
		</tr>
		
		<tr>
			<td rowspan="3" style="width:120px;text-align:center;"><img src="__css/bidoncino.png" /></td>
			<td style="width:240px;">Quantit� di rifiuto caricata a registro:</td>
			<td style="width:100px;text-align:right;padding-right:15px;"><input style="width:50px; text-align:right;" type="text" name="KG" id="KG" value="" /> kg</td>
		</tr>

		<tr>
			<td colspan="2" style="width:340px;">Scheda rifiuto sulla quale verr� ricaricato il quantitativo in giacenza:</td>
		</tr>

		<tr>
			<td colspan="2">
			<select style="width:340px;" id="ricaricoID_RIF" name="ricaricoID_RIF">
			$optListID_RIF
			</select>
			</td>
		</tr>

		<tr>
			<td colspan="3" style="margin-top:20px;">Seleziona l'unita' locale che ha prodotto il rifiuto:</td>
		</tr>
	
		<tr>
			<td colspan="3">
			<select style="width:460px;" id="ricaricoID_UIMP" name="ricaricoID_UIMP">
			$optListID_UIMP
			</select>
			</td>
		</tr>

		<tr>
			<td colspan="3" style="padding-top:20px;">Annotazione sul registro:</td>
		</tr>

		<tr>
			<td colspan="3">
				<textarea style="width:460px;" name="PerRiclassificazione_nota" id="PerRiclassificazione_nota">Movimento interno per nuova classificazione come disposto dal Regolamento UE 1357/2014.</textarea>
			</td>
		</tr>

	</table>

	<div id="ScaricoRicaricoRifiuto_progress" style="display:none;margin-bottom:20px;margin-top:20px;">
		<table style="width:100%;">
			<tr>
				<td style="width:50%;"><b>Stato di avanzamento:</b></td>
				<td style="width:50%;text-align:right;" id="ScaricoRicaricoRifiuto_response"></td>
			</tr>
		</table>
	</div>

</div>

<input type="hidden" id="originalID_RIF" value="" />
<input type="hidden" id="KGhidden" value="" />

DIALOG;

echo $dialog;

?>