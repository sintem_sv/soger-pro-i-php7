<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;

## js
$JS ="<script type=\"text/javascript\">\n";
$JS.= <<<ENDJS
	function upload(id){
		var obj=document.getElementById(id);
		if(obj.style.display=='none'){
			obj.style.display='block';
			obj.style.zindex=10;
			}
		else{
			obj.style.display='none';
			obj.style.zindex=-1;
			}
		}
ENDJS;
$JS.="</script>\n";
echo $JS;

					#css firefox
					$css="<style type=\"text/css\">\n";
					$css.="div#UPL{\n";
					$css.="position:absolute;margin-left:300px;margin-top:50px;z-index: 10; height: 225px;  padding: 3px; border: 2px #5C7D99 SOLID; background-color: #FFFFFF;";
					$css.="}\n";
					$css.="</style>\n";

					#css ie
					$css.="<!--[if IE]>";
					$css.="<style type=\"text/css\">\n";
					$css.="div#UPL{\n";
					$css.="position:absolute;margin-left:-600px;margin-top:50px;z-index: 10; height: 225px;  padding: 3px; border: 2px #5C7D99 SOLID; background-color: #FFFFFF;";
					$css.="}\n";
					$css.="</style>\n";
					$css.="<![endif]-->";

					echo $css;

					$divUpload ="<div id=\"UPL\" style=\"display:none;\">\n";

					$divUpload.="<form name=\"UploadSicurezza\" method=\"post\" action=\"__mov_snipplets/UploadSicurezza.php\" style=\"width:400px;\" enctype=\"multipart/form-data\">\n";
						$divUpload.="<fieldset class=\"FGEfieldset\" style=\"width:80%;margin-left:30px;\">\n";
						$divUpload.="<legend class=\"FGElegend\">CARICA PROCEDURA SICUREZZA</legend>\n";
							
							$divUpload.="<div class=\"FGE1Col\" style=\"margin-left:30px;margin-top:30px;\">\n";
								$divUpload.="<label for=\"file\" class=\"FGEmandatory\">Percorso</label><br/>\n";
								$divUpload.="<input type=\"file\" tabindex=\"1000\" class=\"FGEinput\" id=\"scheda_sicurezza\" name=\"scheda_sicurezza\" size=\"30\" maxlength=\"30\" value=\"\" onfocus=\"this.style.background='yellow';\" onblur=\"this.style.background='white';\"/>\n";
							$divUpload.="</div>\n";
							
							$divUpload.="<div class=\"FGESubmitRow\" style=\"margin-left:30px;margin-top:30px;\">\n";

								$divUpload.="<input type=\"submit\" class=\"FGEbutton\" value=\"carica\" onclick=\"javascript:upload('UPL');\" onfocus=\"this.style.background='yellow';this.style.color='#034373';\" onblur=\"this.style.background='#034373';this.style.color='white';\"/>\n";

								$divUpload.="<input type=\"button\" class=\"FGEbutton\" value=\"annulla\" onclick=\"javascript:upload('UPL');\" onfocus=\"this.style.background='yellow';this.style.color='#034373';\" onblur=\"this.style.background='#034373';this.style.color='white';\"/>\n";

								$divUpload.="<input type=\"hidden\" name=\"ID_RIF\" value=\"\" />";
							$divUpload.="</div>\n";
						$divUpload.="</fieldset>\n";
					$divUpload.="</form>\n";

					$divUpload.="</div>\n";

					echo $divUpload;



$FEDIT->FGE_FlushTableInfo();
#
$FEDIT->FGE_UseTables("user_schede_rifiuti");
$FEDIT->FGE_SetSelectFields(array("ID_CER","cod_pro","descrizione","ID_SF","ID_FONTE_RIF","pericoloso","trasportatore","destinatario","produttore","et_approved","et_approved_start","et_approved_end","sk_approved","sk_approved_start","sk_approved_end","approved"),"user_schede_rifiuti");
$FEDIT->FGE_DescribeFields();
$FEDIT->FGE_SetOrder("user_schede_rifiuti:ID_CER");

$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
$ProfiloFields = "user_schede_rifiuti";
include("SOGER_FiltriProfilo.php");
#
$FEDIT->FGE_HideFields(array("trasportatore","destinatario","produttore","et_approved","et_approved_start","et_approved_end","sk_approved","sk_approved_start","sk_approved_end","approved","ID_FONTE_RIF"),"user_schede_rifiuti");
$FEDIT->FGE_LookUpDefault("ID_SF","user_schede_rifiuti");
#
$FEDIT->FGE_LookUpCFG("ID_CER","user_schede_rifiuti");
$FEDIT->FGE_UseTables("lov_cer");
$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();
#

echo($FEDIT->FGE_DataGrid($SOGER->AppDescriptiveLocation,"__scripts/FGE_DataGridEdit.php","-----",false));

include("__includes/DIALOG_ApprovazioneISO.php");

#
require_once("__includes/COMMON_sleepForgEdit.php");
?>