<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require("__includes/COMMON_wakeForgEditDG.php");
#
global $SOGER;
$FEDG->FGE_FlushTableInfo();
$FEDG->FGE_UseTables(array("user_automezzi","user_autorizzazioni_trasp","user_aziende_trasportatori","lov_cer","user_schede_rifiuti","user_impianti_trasportatori","lov_limitazioni_mezzi"));
$FEDG->FGE_SetSelectFields(array("ID_AZT"),"user_aziende_trasportatori");	
$FEDG->FGE_SetSelectFields(array("num_aut"),"user_autorizzazioni_trasp");
$FEDG->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
$FEDG->FGE_SetSelectFields(array("cod_pro", "descrizione","ID_RIF"),"user_schede_rifiuti");
//$FEDG->FGE_SetSelectFields(array("description"),"user_impianti_trasportatori");
$FEDG->FGE_SetSelectFields(array("description"),"lov_limitazioni_mezzi");
$FEDG->FGE_SetSelectFields(array("description","adr","BlackBox","ID_MZ_TRA","ID_ORIGINE_DATI"),"user_automezzi");
$FEDG->FGE_HideFields(array("ID_ORIGINE_DATI"),"user_automezzi");
#
$FEDG->FGE_DescribeFields();
#$FEDG->FGE_SetOrder("lov_cer:COD_CER");

$FEDG->FGE_SetFilter("ID_AZT",$_SESSION["FGE_IDs"]["user_aziende_trasportatori"],"user_aziende_trasportatori");
$FEDG->FGE_LookUpDefault("ID_MZ_TRA","user_automezzi");
#
echo($FEDG->FGE_DataGrid("Automezzi in Archivio","__scripts/FGE_DataGridEdit.php","EDC--",true,"FEDG",$_SESSION['SearchingFilter']));
require_once("__includes/COMMON_sleepForgEditDG.php");
?>
<div class="FGE4Col">
<input type="button" name="nuovo" class="FGEbutton" value="crea nuovo automezzo" onclick="javascript: document.location='<?php echo $_SERVER["PHP_SELF"] ?>'"/>
</div>