<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;




$FEDIT->FGE_FlushTableInfo();
#
$FEDIT->FGE_UseTables("user_schede_rifiuti");
$FEDIT->FGE_SetSelectFields(array("ID_RIF","ID_CER","cod_pro","descrizione","trasportatore","destinatario","produttore","intermediario","approved"),"user_schede_rifiuti");
$FEDIT->FGE_DescribeFields();
$FEDIT->FGE_SetOrder("user_schede_rifiuti:ID_CER");

$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
$ProfiloFields = "user_schede_rifiuti";
include("SOGER_FiltriProfilo.php");
#
$FEDIT->FGE_HideFields(array("ID_RIF","trasportatore","destinatario","produttore","intermediario","approved"),"user_schede_rifiuti");
#
$FEDIT->FGE_LookUpCFG("ID_CER","user_schede_rifiuti");
$FEDIT->FGE_UseTables("lov_cer");
$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();
#

echo($FEDIT->FGE_DataGrid($SOGER->AppDescriptiveLocation,"__scripts/FGE_DataGridEdit.php","-----",false));

#
require_once("__includes/COMMON_sleepForgEdit.php");
?>

<!-- HIDDEN DIALOG BOX -->

<div id='jqpopup_EvalRequest' title='Richiesta di valutazione'></div>

<div id='jqpopup_EvalRequestHistory' title='Storico delle richieste di valutazione'></div>