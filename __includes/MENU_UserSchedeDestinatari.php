<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require("__includes/COMMON_wakeForgEdit.php");
require("USER_SchedeDestinatariIDcheck.php");
//print_r($this->AppLocation);
$FEDIT->SDbRead($sql,"DbRecordSet",true,false);

//print_r($FEDIT->DbRecordSet[0]);

if($FEDIT->DbRecsNum==0) {
	$urlRS   = "#";
	$urlIMP  = "javascript:alert('Inserire%20prima%20la%20ragione%20sociale')";
	$urlAUTH = "javascript:alert('Inserire%20prima%20i%20dati%20Impianto')";
	$urlASTK = "javascript:alert('Inserire%20prima%20i%20dati%20Impianto')";
	$urlECO  = "javascript:alert('Inserire%20prima%20le%20autorizzazioni')";
	$urlALL  = "javascript:alert('Inserire%20prima%20la%20ragione%20sociale')";

} else {
	$urlRS = "__scripts/SOGER_standardMenu.php?area=UserNuovoDestinatario&amp;table=user_aziende_destinatari&amp;pri=ID_AZD&amp;filter=" . $FEDIT->DbRecordSet[0]["ID_AZD"] . "&hash=" . MakeUrlHash("user_aziende_destinatari","ID_AZD",$FEDIT->DbRecordSet[0]["ID_AZD"]);
	
	$urlALL = "__scripts/SOGER_standardMenu.php?area=UserNuovoDestinatarioAllegati&amp;table=user_aziende_destinatari&amp;pri=ID_AZD&amp;filter=" . $FEDIT->DbRecordSet[0]["ID_AZD"] . "&hash=" . MakeUrlHash("user_aziende_destinatari","ID_AZD",$FEDIT->DbRecordSet[0]["ID_AZD"]);


	if(!is_null($FEDIT->DbRecordSet[0]["ID_UIMD"])) {
		$urlIMP = "__scripts/SOGER_standardMenu.php?area=UserNuovoDestinatarioImpianto&amp;table=user_impianti_destinatari&amp;pri=ID_UIMD&amp;filter=" . $FEDIT->DbRecordSet[0]["ID_UIMD"] . "&hash=" . MakeUrlHash("user_impianti_destinatari","ID_UIMD",$FEDIT->DbRecordSet[0]["ID_UIMD"]);

		if(!is_null($FEDIT->DbRecordSet[0]["ID_ASD"])) {
			$urlASTK = "__scripts/SOGER_standardMenu.php?area=UserNuovoDestinatarioAreeStoccaggio&amp;table=user_aree_stoccaggio_dest&amp;pri=ID_ASD&amp;filter=" . $FEDIT->DbRecordSet[0]["ID_ASD"] . "&hash=" . MakeUrlHash("user_aree_stoccaggio_dest","ID_ASD",$FEDIT->DbRecordSet[0]["ID_ASD"]);
			}
		else{
			$urlASTK = "__scripts/status.php?area=UserNuovoDestinatarioAreeStoccaggio";
			}

		if(!is_null($FEDIT->DbRecordSet[0]["ID_AUTHD"])) {
			$urlAUTH = "__scripts/SOGER_standardMenu.php?area=UserNuovoDestinatarioAutorizzazioni&amp;table=user_autorizzazioni_dest&amp;pri=ID_AUTHD&amp;filter=" . $FEDIT->DbRecordSet[0]["ID_AUTHD"] . "&hash=" . MakeUrlHash("user_autorizzazioni_dest","ID_AUTHD",$FEDIT->DbRecordSet[0]["ID_AUTHD"]);
			
			if(!is_null($FEDIT->DbRecordSet[0]["ID_CNT_D"])) {
				$urlECO = "__scripts/SOGER_standardMenu.php?area=UserDestinatarioContratti&amp;table=user_contratti_destinatari&amp;pri=ID_AZD&amp;filter=" . $FEDIT->DbRecordSet[0]["ID_AZD"]."&amp;hash=" . MakeUrlHash("user_contratti_destinatari","ID_AZD",$FEDIT->DbRecordSet[0]["ID_AZD"]);
				}
			else{
				$_SESSION['ID_AZD_CONTRACT']=$_SESSION["FGE_IDs"]["user_aziende_destinatari"];
				$urlECO = "__scripts/SOGER_standardMenu.php?area=UserNuovoContrattoDestinatario";
				}
		} else {
			$urlAUTH = "__scripts/status.php?area=UserNuovoDestinatarioAutorizzazioni";
			$urlECO = "javascript:alert('Inserire%20prima%20le%20autorizzazioni')";
		}
	} else {
		$urlAUTH = "javascript:alert('Inserire%20prima%20i%20dati%20Impianto')";
		$urlECO = "javascript:alert('Inserire%20prima%20le%20autorizzazioni')";
		$urlIMP = "__scripts/status.php?area=UserNuovoDestinatarioImpianto";
	}
}
?>


<?php
//print_r($SOGER->AppLocation);
switch($SOGER->AppLocation){
	case "UserNuovoDestinatario":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-ON.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_userSchedeProduttoriAUT-OFF.gif";
		$BT_aree_stoccaggio	= "BT_userSchedeProduttoriAS-OFF.gif";
		$BT_contratti		= "BT_contratti-OFF.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";
		break;
	case "UserNuovoDestinatarioImpianto":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-ON.gif";
		$BT_autorizzazioni	= "BT_userSchedeProduttoriAUT-OFF.gif";
		$BT_aree_stoccaggio	= "BT_userSchedeProduttoriAS-OFF.gif";
		$BT_contratti		= "BT_contratti-OFF.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";
		break;
	case "UserNuovoDestinatarioAutorizzazioni":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_userSchedeProduttoriAUT-ON.gif";
		$BT_aree_stoccaggio	= "BT_userSchedeProduttoriAS-OFF.gif";
		$BT_contratti		= "BT_contratti-OFF.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";
		break;
	case "UserNuovoDestinatarioAreeStoccaggio":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_userSchedeProduttoriAUT-OFF.gif";
		$BT_aree_stoccaggio	= "BT_userSchedeProduttoriAS-ON.gif";
		$BT_contratti		= "BT_contratti-OFF.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";
		break;
	case "UserDestinatarioContratti":
	case "UserNuovoContrattoDestinatario":
	case "UserDestinatarioContrattoCondizioni":
	case "UserCondizioneContrattoDestinatario":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_userSchedeProduttoriAUT-OFF.gif";
		$BT_aree_stoccaggio	= "BT_userSchedeProduttoriAS-OFF.gif";
		$BT_contratti		= "BT_contratti-ON.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";
		break;
	case "UserNuovoDestinatarioAllegati":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_userSchedeProduttoriAUT-OFF.gif";
		$BT_aree_stoccaggio	= "BT_userSchedeProduttoriAS-OFF.gif";
		$BT_contratti		= "BT_contratti-OFF.gif";
		$BT_allegati		= "BT_allegati-ON.gif";
		break;

	}
?>


<a href="<?php echo $urlRS ?>" title="Ragione Sociale"><img src="__css/<?php echo $BT_ragione; ?>" class="NoBorder" alt="Ragione Sociale" /></a>
<a href="<?php echo $urlIMP ?>" title="Dati Impianto"><img src="__css/<?php echo $BT_impianti; ?>" class="NoBorder" alt="Dati Impianto"/></a>
<a href="<?php echo $urlAUTH ?>" title="Autorizzazioni"><img src="__css/<?php echo $BT_autorizzazioni; ?>" class="NoBorder" alt="Autorizzazioni"/></a>
<?php if($this->UserData['workmode']=='destinatario'){ ?>
<a href="<?php echo $urlASTK ?>" title="Aree di stoccaggio"><img src="__css/<?php echo $BT_aree_stoccaggio; ?>" class="NoBorder" alt="Aree di stoccaggio"/></a>
<?php } ?>
<?php if($this->UserData["core_impiantiMODULO_ECO"]=="1" AND $this->UserData["core_usersG3"]=="1"){ ?>
<a href="<?php echo $urlECO ?>" title="Contratti"><img src="__css/<?php echo $BT_contratti; ?>" class="NoBorder" alt="Contratti"/></a>
<?php } ?>

<a href="<?php echo $urlALL ?>" title="Allegati"><img src="__css/<?php echo $BT_allegati; ?>" class="NoBorder" alt="Allegati"/></a>
