<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;

$reload='index.php';
$get=setUrlVariables();
$reload.=$get;

?>
<div class="FGEDataGridTitle"><div>ANAGRAFICHE � SCHEDE RIFIUTI PRODOTTI � CARATTERIZZAZIONE RIFIUTO</div></div><br>
<?php
if($SOGER->AppLocation!="login" && $SOGER->AppLocation!="login_documents" && count($SOGER->AppNavDIVs)>0) {
	$tmp .= "<div id=\"NavDiv\">\n";
	$tmp .= "<!-- NAV DIVs BEGIN -->\n[NAV_STUCTURE]\n<!-- NAV DIVs END -->\n";
	$SOGER->DIVmake_Navigation();
	$tmp .= "</div>\n";
	echo $tmp;
	}
?>

	<form id="FGEForm_1" name="UploadImmagine" method="post" action="__mov_snipplets/UploadImg.php" enctype="multipart/form-data">

		<input type="hidden" name="saveMeMac" value="1" />
		<input type="hidden" name="folder" value="caratterizzazioni" />

		<fieldset class="FGEfieldset">

			<div class="FGEFormTitle FGE4Col">Immagine</div>

				<table>
					<tr>
						<td>
			
							<?php
							$imagecaratt=array();
							$imageCaratt[0]='__upload/caratterizzazioni/'.$_SESSION["IDRifiutoCaratt"].'.JPG';
							$imageCaratt[1]='__upload/caratterizzazioni/'.$_SESSION["IDRifiutoCaratt"].'.JPEG';
							$imageCaratt[2]='__upload/caratterizzazioni/'.$_SESSION["IDRifiutoCaratt"].'.jpg';
							$imageCaratt[3]='__upload/caratterizzazioni/'.$_SESSION["IDRifiutoCaratt"].'.jpeg';
							$gotImage=false;
							for($f=0;$f<count($imageCaratt);$f++){
								if(file_exists($imageCaratt[$f])){
									$gotImage=true;
									$image=$imageCaratt[$f];
									break;
									}
								}
							if($gotImage){
								echo '<img src="__upload/caratterizzazioni/imageResize.php?width=250&height=250&image=/soger/'.$image.'">';
								}
							else{
								echo '<img src="__upload/noimage250.gif" />';
								}
							?>
						</td>
						<td>
							<div class="FGE1Col" style="margin-left:30px;margin-top:30px;">
								<label for="file" class="FGEmandatory">Percorso</label><br/>
								<input type="file" tabindex="1000" class="FGEinput" id="immagine:nome" name="immagine" size="30" maxlength="30" value="" onfocus="this.style.background='yellow';" onblur="this.style.background='white';"/>
							</div>
							
							<div class="FGESubmitRow" style="margin-left:30px;margin-top:30px;">
								<input type="submit" class="FGEbutton" value="carica la foto sul server" onfocus="this.style.background='yellow';this.style.color='#034373';" onblur="this.style.background='#034373';this.style.color='white';"/>
								<input type="button" class="FGEbutton" value="aggiorna preview immagine" onfocus="this.style.background='yellow';this.style.color='#034373';" onblur="this.style.background='#034373';this.style.color='white';" onclick="window.location='<?php echo $reload; ?>'"/>
								<input type="hidden" name="ID_RIF" value="<?php echo $_SESSION["IDRifiutoCaratt"]; ?>" />
							</div>
						</td>
					</tr>
				</table>

		</fieldset>

	</form>


<?php
require_once("__includes/COMMON_sleepForgEdit.php");
?>