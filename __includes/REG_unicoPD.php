<div class="divName"><span>GESTIONE REGISTRI</span></div>


<?php
	require_once("__includes/COMMON_ForgEditClassFiles.php");
	require_once("__includes/COMMON_wakeForgEdit.php");
	#
	global $SOGER;
	$TableName="user_movimenti_fiscalizzati";
	//  inutile perch� dovrebbe operare su due workmode
	//	$LegamiSISTRI	= false;
	//	require("__scripts/MovimentiRifMovCarico.php");
	require_once("__includes/COMMON_sleepForgEdit.php");
	require("__includes/USER_RegistroFiscaleSiNo.inc");
?>

<form name="StampaRegistroUnicoPD" id="StampaRegistroUnicoPD" method="get" action="" style="width: 90%">
<fieldset class="FGEfieldset">
	<legend class="FGElegend" style="font-size:x-small;">Stampa Registro Unico (P+D)</legend>

<table style="width:100%;">


<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="da_data" class="">Dalla data</label></td>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><input type="text" value="01/01/<?php echo date('Y'); ?>" id="da_data" name="da_data" size="10" class="FGEinput" tabindex="50"/></td>
</tr>

<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="a_data" class="">Alla data</label></td>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><input type="text" value="<?php echo date('d/m/Y'); ?>" id="a_data" name="a_data" size="10" class="FGEinput" tabindex="51"/></td>
</tr>

<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="cartaBianca" class="">Stampa su carta bianca</label></td>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><input type="checkbox" value="Cbianca" name="cartaBianca" id="cartaBianca" class="FGEinput" tabindex="52" checked /></td>
</tr>

<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="Npagina" class="">Stampa numeri di pagina</label></td>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><input type="checkbox" value="StampaNpag" name="Npagina" id="Npagina" class="FGEinput" tabindex="53" checked /></td>
</tr>

<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="RegVidimato" class="">Registro vidimato</label></td>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><input type="radio" value="1" name="Vidimato" id="Vidimato1" class="FGEinput" tabindex="54" />S�
	<input type="radio" value="0" name="Vidimato" id="Vidimato0" class="FGEinput" tabindex="55" style="margin-left:10px;" checked />No</td>
</tr>

<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="paginaIniz" class="">Numero di pagina iniziale</label></td>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><input type="text" size="4" value="1" name="paginaIniz" id="paginaIniz" class="FGEinput" tabindex="56"/></td>
</tr>

</table>


<br/><br/>

<input type="button" class="FGEbutton" tabindex="55" value="stampa registro" onclick="javascript:RegistroUnicoPDPrintGo();"/>

</fieldset>
</form>