<?php

require_once("../__libs/fpdf.php");
require_once("../__scripts/STATS_funct.php");

$Xpos = 2;
$Ypos = 0;

$Data=explode("/",$_GET['da']);
$Data1=$Data[2]."-".$Data[1]."-".$Data[0];

$Data=explode("/",$_GET['a']);
$Data2=$Data[2]."-".$Data[1]."-".$Data[0];


#
# TITOLO
#
$statTitle = "Registro di gestione dei rifiuti industriale - Registro Cronologico - ".$SOGER->UserData["workmode"];
$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',14);
$FEDIT->FGE_PdfBuffer->MultiCell(293,15,$statTitle,"TLBR","C"); 

#
# AZIENDA
#
$Ypos+=12;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$sql="SELECT core_impianti.description, indirizzo, lov_comuni_istat.description as comune, lov_comuni_istat.CAP, lov_comuni_istat.shdes_prov as prov ";
$sql.="FROM core_impianti JOIN lov_comuni_istat ON core_impianti.ID_COM = lov_comuni_istat.ID_COM ";
$sql.="WHERE core_impianti.ID_IMP='".$SOGER->UserData['core_impiantiID_IMP']."';";
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
$InfoAZ=$FEDIT->DbRecordSet[0]['description']." - ".$FEDIT->DbRecordSet[0]['indirizzo']." - ".$FEDIT->DbRecordSet[0]['CAP']." - ".$FEDIT->DbRecordSet[0]['comune']." (".$FEDIT->DbRecordSet[0]['prov'].")";
$FEDIT->FGE_PdfBuffer->MultiCell(200,15,$InfoAZ,0,"L"); 

#
# PERIODO
#
$Ypos+=6;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(200,15,"Settimana dal ".$_GET['da']." al ".$_GET['a'],0,"L"); 


#
# ULTIMA FISCALIZZAZIONE
#
if($SOGER->UserData["workmode"]=="produttore") $Field="Plast_fisc"; else $Field="Dlast_fisc";
$sql="SELECT ".$Field." FROM core_impianti WHERE ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."';";
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
$dataExp=explode("-",$FEDIT->DbRecordSet[0][$Field]);
$data=$dataExp[2]."/".$dataExp[1]."/".$dataExp[0];
$Ypos+=6;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(200,15,"Data ultima fiscalizzazione: ".$data,0,"L"); 



#
# SELEZIONO MOVIMENTI REG. INDUSTRIALE
#
$sql  = " SELECT ";
$sql .= " TIPO, DTMOV, NMOV, FKESF, quantita, FKErifCarico, FKEumis, user_movimenti.adr, ";
$sql .= " user_schede_rifiuti.descrizione, lov_cer.COD_CER, ";
$sql .= " user_schede_rifiuti.H1, user_schede_rifiuti.H2, user_schede_rifiuti.H3A, user_schede_rifiuti.H3B, user_schede_rifiuti.H4, ";
$sql .= " user_schede_rifiuti.H5, user_schede_rifiuti.H6, user_schede_rifiuti.H7, user_schede_rifiuti.H8, user_schede_rifiuti.H9, ";
$sql .= " user_schede_rifiuti.H10, user_schede_rifiuti.H11, user_schede_rifiuti.H12, user_schede_rifiuti.H13, user_schede_rifiuti.H14, user_schede_rifiuti.H15,  ";
$sql .= " user_aziende_trasportatori.description AS trasportatore, user_autorizzazioni_trasp.num_aut AS numAlbo, ";
$sql .= " user_aziende_destinatari.description AS destinatario, user_autorizzazioni_dest.num_aut AS numAut, ";
$sql .= " user_aziende_intermediari.description AS intermediario ";
$sql .= " FROM user_movimenti"; 
$sql .= " JOIN user_schede_rifiuti ON user_movimenti.ID_RIF=user_schede_rifiuti.ID_RIF";
$sql .= " JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER";
$sql .= " LEFT JOIN user_aziende_trasportatori ON user_movimenti.ID_AZT=user_aziende_trasportatori.ID_AZT ";
$sql .= " LEFT JOIN user_aziende_destinatari ON user_movimenti.ID_AZD=user_aziende_destinatari.ID_AZD ";
$sql .= " LEFT JOIN user_aziende_intermediari ON user_movimenti.ID_AZI=user_aziende_intermediari.ID_AZI ";
$sql .= " LEFT JOIN user_autorizzazioni_trasp ON user_movimenti.ID_AUTHT=user_autorizzazioni_trasp.ID_AUTHT ";
$sql .= " LEFT JOIN user_autorizzazioni_dest ON user_movimenti.ID_AUTHD=user_autorizzazioni_dest.ID_AUTHD ";
$sql .= " WHERE (DTMOV>='" . $Data1 . "' AND DTMOV<='" . $Data2 . "') AND NMOV<>9999999 ";
$TableSpec = "user_movimenti.";
include("SOGER_DirectProfilo.php");
$sql .= " AND user_movimenti.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
$sql .= " ORDER BY NMOV ASC";
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

//die($sql);
//die(var_dump($FEDIT->DbRecordSet));

#
# DIVIDO CARICHI DA SCARICHI
#
$IndC=array();
$IndS=array();
for($i=0;$i<count($FEDIT->DbRecordSet);$i++){
	if($FEDIT->DbRecordSet[$i]['TIPO']=="C")
		$IndC[]=$FEDIT->DbRecordSet[$i];
	else
		$IndS[]=$FEDIT->DbRecordSet[$i];
	}



#
# INTESTAZIONE CARICHI
#
$Ypos+=15;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',12);
$FEDIT->FGE_PdfBuffer->SetFillColor(250,248,59);
$FEDIT->FGE_PdfBuffer->MultiCell(293,7,"Rifiuto Prodotto - Carichi","TLBR","C",1);
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',11);
$FEDIT->FGE_PdfBuffer->SetFillColor(239,238,236);
$FEDIT->FGE_PdfBuffer->MultiCell(155,7,"Registrazione industriale","TLBR","C",1);
$Xpos+=158;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFillColor(250,248,59);
$FEDIT->FGE_PdfBuffer->MultiCell(135,7,"Registrazione fiscale","TLBR","C",1);
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
#
# CICLO CARICHI
#
$Xpos=2;
$YposFiscC=array();
$FiscC=array();
$FiscC_ID=array();
if(count($IndC)>0){
		
	## Seleziono movimenti fiscali di riferimento
	for($c=0;$c<count($IndC);$c++){
		$sql ="SELECT ID_MOV_F, NMOV, FKEumis, lov_cer.COD_CER, user_schede_rifiuti.descrizione, ";
		$sql.="DTMOV, quantita, FKErifIND FROM user_movimenti_fiscalizzati ";
		$sql.="JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_movimenti_fiscalizzati.ID_RIF  ";
		$sql.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
		$sql.="WHERE FKErifIND LIKE '%|".$IndC[$c]['NMOV']."|%' AND user_movimenti_fiscalizzati.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND user_movimenti_fiscalizzati.".$SOGER->UserData['workmode']."='1';";
		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
		## note: un mov ind non pu� generare + di 1 mov fisc
		if(!is_null($FEDIT->DbRecordSet) && !in_array($FEDIT->DbRecordSet[0]['ID_MOV_F'], $FiscC_ID)){
			$FiscC[]=$FEDIT->DbRecordSet[0];
			$FiscC_ID[]=$FEDIT->DbRecordSet[0]['ID_MOV_F'];
			}
		}
	
	$DTbuf="xxxx-xx-xx";
	for($c=0;$c<count($IndC);$c++){
	
		if($IndC[$c]['DTMOV']!=$DTbuf){
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			## data ##
			$Xpos=2;
			$dataExp=explode("-",$IndC[$c]['DTMOV']);
			$data=$dataExp[2]."/".$dataExp[1]."/".$dataExp[0];
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',11);
			$FEDIT->FGE_PdfBuffer->MultiCell(155,7,"Data: ".$data,"TLBR","L");
			$DTbuf=$IndC[$c]['DTMOV'];
			$Xpos+=158;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',11);
			$FEDIT->FGE_PdfBuffer->MultiCell(135,7,"","TLBR","L");
			## intestazione colonne industriale ##
			$Xpos=2;
			$Ypos+=7;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(10,7,"Rigo Ind","TLBR","C");
			$Xpos+=10;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(70,14,"Scheda rifiuto","TLBR","L");
			$Xpos+=70;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,14,"Stato fisico","TLBR","L");
			$Xpos+=20;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,14,"Quantit�","TLBR","C");
			$Xpos+=20;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(10,14,"ADR","TLBR","C");
			$Xpos+=10;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(25,14,"Pericolo (H)","TLBR","L");
			$Xpos+=28;
			## intestazione colonne industriale ##
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,14,"Data","TLBR","C");
			$Xpos+=20;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(70,14,"Scheda rifiuto","TLBR","L");
			$Xpos+=70;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,14,"Quantit�","TLBR","C");
			$Xpos+=20;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(10,7,"Rigo Fisc","TLBR","C");
			$Xpos+=10;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,14,"Mov.Ind.","TLBR","L");
			$Ypos+=14;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			}
		$Xpos=2;
		$YposFiscC[]=$Ypos;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		## riga ##
		$Ypos+=5;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$Ypos-=5;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(10,20,$IndC[$c]['NMOV'],"TLBR","C");
		$Xpos+=10;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(70,20,"","TLBR","L");
		$Ypos+=5;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(70,5,$IndC[$c]['COD_CER']." - ".$IndC[$c]['descrizione'],"","L");
		$Ypos-=5;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$Xpos+=70;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,20,"","TLBR","L");
		$Ypos+=5;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$IndC[$c]['FKESF'],"","L");
		$Ypos-=5;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$Xpos+=20;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,20,number_format_unlimited_precision($IndC[$c]['quantita'])." ".$IndC[$c]['FKEumis'],"TLBR","C");
		$Xpos+=20;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		if($IndC[$c]['adr']=="1") $adr="S�"; else $adr="No";
		$FEDIT->FGE_PdfBuffer->MultiCell(10,20,$adr,"TLBR","C");
		$Xpos+=10;
		if($IndC[$c]['H1']==1) $classiH[]="H1";
		if($IndC[$c]['H2']==1) $classiH[]="H2";
		if($IndC[$c]['H3A']==1) $classiH[]="H3A";
		if($IndC[$c]['H3B']==1) $classiH[]="H3B";
		if($IndC[$c]['H4']==1) $classiH[]="H4";
		if($IndC[$c]['H5']==1) $classiH[]="H5";
		if($IndC[$c]['H6']==1) $classiH[]="H6";
		if($IndC[$c]['H7']==1) $classiH[]="H7";
		if($IndC[$c]['H8']==1) $classiH[]="H8";
		if($IndC[$c]['H9']==1) $classiH[]="H9";
		if($IndC[$c]['H10']==1) $classiH[]="H10";
		if($IndC[$c]['H11']==1) $classiH[]="H11";
		if($IndC[$c]['H12']==1) $classiH[]="H12";
		if($IndC[$c]['H13']==1) $classiH[]="H13";
		if($IndC[$c]['H14']==1) $classiH[]="H14";
		if($IndC[$c]['H15']==1) $classiH[]="H15";
		$displayH="";
		for($h=0;$h<count($classiH);$h++)
			$displayH.=$classiH[$h]." ";
		unset($classiH);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,20,$displayH,"TLBR","L");
		$displayH="";
		$Xpos+=28; 

		if(isset($FiscC[$c])){

			## riga ##
			$Ypos+=5;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$Ypos-=5;
			$dataExp=explode("-",$FiscC[$c]['DTMOV']);
			$data=$dataExp[2]."/".$dataExp[1]."/".$dataExp[0];
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,20,$data,"TLBR","C");
			$Xpos+=20;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(70,20,"","TLBR","L");
			$Ypos+=5;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(70,5,$FiscC[$c]['COD_CER']." - ".$FiscC[$c]['descrizione'],"","L");
			$Ypos-=5;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$Xpos+=70;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,20,number_format_unlimited_precision($FiscC[$c]['quantita'])." ".$FiscC[$c]['FKEumis'],"TLBR","C");
			$Xpos+=20;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(10,20,$FiscC[$c]['NMOV'],"TLBR","C");
			$Xpos+=10;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,20,"","TLBR","L");
			$Ypos+=2;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$RifInd=str_replace("||", ", ", $FiscC[$c]['FKErifIND']);
			$RifInd=str_replace("|", "", $RifInd);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,3,$RifInd,"","L");
			$Ypos-=2;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

			}
		else{
			$RifIndArray=array();
			for($f=0;$f<count($FiscC);$f++){
				$RifInd=explode("|",$FiscC[$f]['FKErifIND']);
				for($r=0;$r<count($RifInd);$r++){
					if($RifInd[$r]!="" && !in_array($RifInd[$r],$RifIndArray)){
						$counter=count($RifIndArray);
						$RifIndArray[$counter]['rigoInd']=$RifInd[$r];
						$RifIndArray[$counter]['dtFisc']=$FiscC[$f]['DTMOV'];
						$RifIndArray[$counter]['rigoFisc']=$FiscC[$f]['NMOV'];
						}
					}
				}

			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);


			for($f=0;$f<count($RifIndArray);$f++){
				if($IndC[$c]['NMOV']==$RifIndArray[$f]['rigoInd'])
					$Findex=$f;
				}

			if(!isset($Findex))
				$FEDIT->FGE_PdfBuffer->MultiCell(135,20,"Movimento non ancora fiscalizzato","TLBR","C");
			else{
				$dataExp=explode("-",$RifIndArray[$Findex]['dtFisc']);
				$data=$dataExp[2]."/".$dataExp[1]."/".$dataExp[0];
				$FEDIT->FGE_PdfBuffer->MultiCell(135,20,"Movimento fiscalizzato il ".$data.", rigo ".$RifIndArray[$Findex]['rigoFisc']."","TLBR","C");
				unset($Findex);
				}
			}

		$Ypos+=20;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$YstartS=$Ypos;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		}
	}
else{
	$Ypos+=7;
	$Xpos=2;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',12);
	$FEDIT->FGE_PdfBuffer->MultiCell(293,7,"Non sono stati eseguiti carichi industriali nel periodo scelto","TLBR","C");
	$Ypos+=20;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$YstartS=$Ypos;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	}






$Xpos=2;
$Ypos=$YstartS;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
#
# INTESTAZIONE SCARICHI
#
$Ypos+=15;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',12);
$FEDIT->FGE_PdfBuffer->SetFillColor(250,248,59);
$FEDIT->FGE_PdfBuffer->MultiCell(293,7,"Rifiuto Allontanato - Scarichi","TLBR","C",1);
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',11);
$FEDIT->FGE_PdfBuffer->SetFillColor(239,238,236);
$FEDIT->FGE_PdfBuffer->MultiCell(155,7,"Registrazione industriale","TLBR","C",1);
$Xpos+=158;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFillColor(250,248,59);
$FEDIT->FGE_PdfBuffer->MultiCell(135,7,"Registrazione fiscale","TLBR","C",1);
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
#
# CICLO SCARICHI
#
$Xpos=2;
$YposFiscS=array();
$FiscS=array();
$FiscS_ID=array();
if(count($IndS)>0){
		
	## Seleziono movimenti fiscali di riferimento
	for($c=0;$c<count($IndS);$c++){
		$sql ="SELECT ID_MOV_F, NMOV, FKEumis, lov_cer.COD_CER, user_schede_rifiuti.descrizione, ";
		$sql.="DTMOV, quantita, FKErifCarico, FKErifIND FROM user_movimenti_fiscalizzati ";
		$sql.="JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_movimenti_fiscalizzati.ID_RIF  ";
		$sql.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
		$sql.="WHERE FKErifIND LIKE '%|".$IndS[$c]['NMOV']."|%' AND user_movimenti_fiscalizzati.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND user_movimenti_fiscalizzati.".$SOGER->UserData['workmode']."='1';";
		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
		## note: un mov ind non pu� generare + di 1 mov fisc
		if(!is_null($FEDIT->DbRecordSet) && !in_array($FEDIT->DbRecordSet[0]['ID_MOV_F'], $FiscS_ID)){
			$FiscS[]=$FEDIT->DbRecordSet[0];
			$FiscS_ID[]=$FEDIT->DbRecordSet[0]['ID_MOV_F'];
			}
		}

	$DTbuf="xxxx-xx-xx";
	for($c=0;$c<count($IndS);$c++){
	
		if($IndS[$c]['DTMOV']!=$DTbuf){
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			## data ##
			$Xpos=2;
			$dataExp=explode("-",$IndS[$c]['DTMOV']);
			$data=$dataExp[2]."/".$dataExp[1]."/".$dataExp[0];
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',11);
			$FEDIT->FGE_PdfBuffer->MultiCell(155,7,"Data: ".$data,"TLBR","L");
			$DTbuf=$IndS[$c]['DTMOV'];
			$Xpos+=158;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',11);
			$FEDIT->FGE_PdfBuffer->MultiCell(135,7,"","TLBR","L");
			## intestazione colonne industriale ##
			$Xpos=2;
			$Ypos+=7;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(10,7,"Rigo Ind","TLBR","C");
			$Xpos+=10;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(70,14,"Scheda rifiuto","TLBR","L");
			$Xpos+=70;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,14,"Stato fisico","TLBR","L");
			$Xpos+=20;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,7,"Quantit�","TLBR","C");

			$Ypos+=7;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,7,"Rif. Carico","TLBR","C");
			$Ypos-=7;

			$Xpos+=20;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(10,14,"ADR","TLBR","C");
			$Xpos+=10;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(25,14,"Pericolo (H)","TLBR","L");
			$Xpos+=28;
			## intestazione colonne industriale ##
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,14,"Data","TLBR","C");
			$Xpos+=20;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(70,14,"Scheda rifiuto","TLBR","L");
			$Xpos+=70;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,7,"Quantit�","TLBR","C");

			$Ypos+=7;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,7,"Rif. Carico","TLBR","C");
			$Ypos-=7;

			$Xpos+=20;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(10,7,"Rigo Fisc","TLBR","C");
			$Xpos+=10;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,14,"Mov.Ind.","TLBR","L");
			$Ypos+=14;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			}
		$Xpos=2;
		$YposFiscS[]=$Ypos;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		## riga ##
		if($Ypos>150){
			$FEDIT->FGE_PdfBuffer->addpage();
			$Ypos = 10;
			}
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(10,40,$IndS[$c]['NMOV'],"TLBR","C");
		$Xpos+=10;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(70,40,"","TLBR","L");
		$Ypos+=2;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(70,3,$IndS[$c]['COD_CER']." - ".$IndS[$c]['descrizione'],"","L");
		## soggetti
		$Soggetti ="\nTrasportatore: ".$IndS[$c]['trasportatore']." (".$IndS[$c]['numAlbo'].")\n";
		$Soggetti.="Destinatario: ".$IndS[$c]['destinatario']." (".$IndS[$c]['numAut'].")\n";
		$Soggetti.="Intermediario: ".$IndS[$c]['intermediario'];
		$Ypos+=7;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(70,4,$Soggetti,"T","L");
		$Ypos-=9;
		##
		$Xpos+=70;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,40,"","TLBR","L");
		$Ypos+=5;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,5,$IndS[$c]['FKESF'],"","L");
		$Ypos-=5;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$Xpos+=20;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,15,number_format_unlimited_precision($IndS[$c]['quantita'])." ".$IndS[$c]['FKEumis'],"TLBR","C");

		$Ypos+=15;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,25,"","TLBR","C");
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,5,str_replace(",",", ",$IndS[$c]['FKErifCarico']),"","C");
		$Ypos-=15;

		$Xpos+=20;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		if($IndS[$c]['adr']=="1") $adr="S�"; else $adr="No";
		$FEDIT->FGE_PdfBuffer->MultiCell(10,40,$adr,"TLBR","C");
		$Xpos+=10;
		if($IndS[$c]['H1']==1) $classiH[]="H1";
		if($IndS[$c]['H2']==1) $classiH[]="H2";
		if($IndS[$c]['H3A']==1) $classiH[]="H3A";
		if($IndS[$c]['H3B']==1) $classiH[]="H3B";
		if($IndS[$c]['H4']==1) $classiH[]="H4";
		if($IndS[$c]['H5']==1) $classiH[]="H5";
		if($IndS[$c]['H6']==1) $classiH[]="H6";
		if($IndS[$c]['H7']==1) $classiH[]="H7";
		if($IndS[$c]['H8']==1) $classiH[]="H8";
		if($IndS[$c]['H9']==1) $classiH[]="H9";
		if($IndS[$c]['H10']==1) $classiH[]="H10";
		if($IndS[$c]['H11']==1) $classiH[]="H11";
		if($IndS[$c]['H12']==1) $classiH[]="H12";
		if($IndS[$c]['H13']==1) $classiH[]="H13";
		if($IndS[$c]['H14']==1) $classiH[]="H14";
		if($IndS[$c]['H15']==1) $classiH[]="H15";
		$displayH="";
		for($h=0;$h<count($classiH);$h++)
			$displayH.=$classiH[$h]." ";
		unset($classiH);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,40,$displayH,"TLBR","L");
		$displayH="";
		$Xpos+=28; 
				
		if(isset($FiscS[$c])){
			## riga ##
			$dataExp=explode("-",$FiscS[$c]['DTMOV']);
			$data=$dataExp[2]."/".$dataExp[1]."/".$dataExp[0];
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,40,$data,"TLBR","C");
			$Xpos+=20;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(70,40,"","TLBR","L");
			$Ypos+=2;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(70,3,$FiscS[$c]['COD_CER']." - ".$FiscS[$c]['descrizione'],"","L");
			## soggetti
			$Soggetti ="\nTrasportatore: ".$IndS[$c]['trasportatore']." (".$IndS[$c]['numAlbo'].")\n";
			$Soggetti.="Destinatario: ".$IndS[$c]['destinatario']." (".$IndS[$c]['numAut'].")\n";
			$Soggetti.="Intermediario: ".$IndS[$c]['intermediario'];
			$Ypos+=7;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(70,4,$Soggetti,"T","L");
			$Ypos-=9;
			##

			$Xpos+=70;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,15,number_format_unlimited_precision($FiscS[$c]['quantita'])." ".$FiscS[$c]['FKEumis'],"TLBR","C");

			$Ypos+=15;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,25,"","TLBR","C");
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);
			$FEDIT->FGE_PdfBuffer->MultiCell(20,5,str_replace(",",", ",$FiscS[$c]['FKErifCarico']),"TLR","C");
			$Ypos-=15;

			$Xpos+=20;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(10,40,$FiscS[$c]['NMOV'],"TLBR","C");
			$Xpos+=10;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,40,"","TLBR","L");
			$Ypos+=2;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$RifInd=str_replace("||", ", ", $FiscS[$c]['FKErifIND']);
			$RifInd=str_replace("|", "", $RifInd);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,3,$RifInd,"","L");
			$Ypos-=2;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			}
		else{
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(135,40,"Movimento non ancora fiscalizzato","TLBR","C");
			}
		
		$Ypos+=40;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$YstartS=$Ypos;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		}
	}
else{
	$Ypos+=7;
	$Xpos=2;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',12);
	$FEDIT->FGE_PdfBuffer->MultiCell(293,7,"Non sono stati eseguiti scarichi industriali nel periodo scelto","TLBR","C");
	}

#
# DATA E FIRMA
#

$Ypos+=10;
$Xpos=200;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',14);
$FEDIT->FGE_PdfBuffer->MultiCell(50,15,"FIRMA","","C"); 

$Xpos=25;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',14);
$FEDIT->FGE_PdfBuffer->MultiCell(50,15,date('d/m/Y'),"","C"); 

?>