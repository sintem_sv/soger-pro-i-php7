<?php

require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
global $SOGER;

$_SESSION['LKUP_IDRIF_CHANGED'] = false;

if(isset($_GET['table'])){
	$TableName=$_GET['table'];
	$sql = "SELECT ID_RIF, PerRiclassificazione FROM ".$TableName." WHERE ".$_GET['pri']."=".$_GET['filter'].";";
	$FEDIT->SDBRead($sql,"DbRecordSetPerRiclassificazione",true,false);
	$PerRiclassificazione = $FEDIT->DbRecordSetPerRiclassificazione[0]['PerRiclassificazione'];
        $_SESSION['LKUP_IDRIF'] = $FEDIT->DbRecordSetPerRiclassificazione[0]['ID_RIF'];
	}
else{
	if($SOGER->UserData['core_impiantiREG_IND']=='1')
		$TableName="user_movimenti";
	else
		$TableName="user_movimenti_fiscalizzati";
	$PerRiclassificazione = 0;
        $_SESSION['LKUP_IDRIF'] = 0;
	}


#
#	SCARICO+FIR // SCHEDA SISTRI // SCARICO INTERNO
#
switch($SOGER->AppLocation){

	case "UserNuovoMovScarico":
	case "UserNuovoMovScaricoF":
	case "UserNuovoMovScaricoFiscale":
		$IS_SCARICO_INTERNO = false;
		if(!isset($_GET["table"]) && !isset($_GET["pri"]) && !isset($_GET["filter"])) {
			$IS_SCHEDA_SISTRI		= false;
			$IS_SCHEDA_SISTRI_OPEN	= false;
			}
		else{
			$SQL ="SELECT NMOV, TIPO_S_SCHEDA FROM ".$TableName." WHERE ".$_GET["pri"]."=".$_GET["filter"].";";
			$FEDIT->SDBRead($SQL,"CheckIsScheda",true,false);
			if($FEDIT->CheckIsScheda[0]['NMOV']=='9999999'){
				$IS_SCHEDA_SISTRI		= true;
				$IS_SCHEDA_SISTRI_OPEN	= true;
				}
			else{
				if($FEDIT->CheckIsScheda[0]['TIPO_S_SCHEDA']=='1')
					$IS_SCHEDA_SISTRI	= true;
				else
					$IS_SCHEDA_SISTRI	= false;
				$IS_SCHEDA_SISTRI_OPEN	= false;
				}
			}
		break;

	case "UserNuovaSchedaSistri":
	case "UserNuovaSchedaSistriF":
	case "UserNuovaSchedaSistriFiscale":
		$IS_SCARICO_INTERNO = false;
		$IS_SCHEDA_SISTRI	= true;
		if(!isset($_GET["table"]) && !isset($_GET["pri"]) && !isset($_GET["filter"])) {
			$IS_SCHEDA_SISTRI_OPEN	= true;
			}
		else{
			$SQL ="SELECT NMOV FROM ".$TableName." WHERE ".$_GET["pri"]."=".$_GET["filter"].";";
			$FEDIT->SDBRead($SQL,"CheckIsScheda",true,false);
			if($FEDIT->CheckIsScheda[0]['NMOV']=='9999999')
				$IS_SCHEDA_SISTRI_OPEN	= true;
			else
				$IS_SCHEDA_SISTRI_OPEN	= false;
			}
		break;

	case "UserNuovoMovScaricoInternoF":
		$IS_SCARICO_INTERNO		= true;
		$IS_SCHEDA_SISTRI		= false;
		$IS_SCHEDA_SISTRI_OPEN	= false;
		break;

	}

if(isset($_GET['SI'])){
	if($_GET['SI']==0) $IS_SCARICO_INTERNO = false;
	if($_GET['SI']==1) $IS_SCARICO_INTERNO = true;
	}

	//echo "<hr>Location: ".$SOGER->AppLocation."<br /> IS_SCHEDA_SISTRI: ".var_dump($IS_SCHEDA_SISTRI)."<br /> IS_SCHEDA_SISTRI_OPEN: ".var_dump($IS_SCHEDA_SISTRI_OPEN)."<br /> IS_SCARICO_INTERNO: ".var_dump($IS_SCARICO_INTERNO); 

##
#	RICAVA IL NUMERO DI MOVIMENTO E DI FORMULARIO
#
if(!isset($_GET["table"]) && !isset($_GET["pri"]) && !isset($_GET["filter"])) {
	
	require("__mov_snipplets/FRM_NumeroMOVIMENTO.php");
	
	$DtMov		= '';
	$NumForm	= '';
	$DtForm		= '';
	
	if(!$IS_SCHEDA_SISTRI){
		$DtMov		= date("m/d/Y");
		if(($SOGER->UserData["core_usersFRM_LAYOUT_DEST"]==0) || ($SOGER->UserData['workmode']=='produttore' AND !$IS_SCARICO_INTERNO)){
                        if($SOGER->UserData["core_usersFRM_DefaultNFORM"]==1)
                            require("__mov_snipplets/FRM_NumeroFormulario.php");
			$DtForm		= date("m/d/Y");
			}
		}
	
	}

#
#	CAMPI
#		
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables($TableName);
$FEDIT->FGE_SetFormFields(array("ID_IMP","TIPO","TIPO_S_SCHEDA","TIPO_S_INTERNO"),$TableName);
$FEDIT->FGE_SetFormFields("NMOV",$TableName);
if(!$IS_SCHEDA_SISTRI || !$IS_SCHEDA_SISTRI_OPEN)
	$FEDIT->FGE_SetFormFields("DTMOV",$TableName);
if((!$IS_SCHEDA_SISTRI || !$IS_SCHEDA_SISTRI_OPEN) AND !$IS_SCARICO_INTERNO)
	$FEDIT->FGE_SetFormFields(array("NFORM","DTFORM"),$TableName);

#
#	FANGHI
#
if($SOGER->UserData['core_usersFANGHI']==1)	
	$FEDIT->FGE_SetFormFields(array("FANGHI", "FANGHI_BOLLA"),$TableName);


#
#	RIFIUTO
#
$FEDIT->FGE_SetFormFields(array("ID_RIF","originalID_RIF","FKESF","FKEumis","FKEdisponibilita","lotto"),$TableName);
$FEDIT->FGE_SetFormFields(array("FKEpesospecifico","quantita","qta_hidden","tara","pesoL","pesoN","VER_DESTINO","FKEgiacINI","FKElastCarico","FKErifCarico","percorso"),$TableName);

#
#	IMBALLAGGIO
#
if($SOGER->UserData['core_impiantiMODULO_SIS']==1 AND $IS_SCHEDA_SISTRI)	
	$FEDIT->FGE_SetFormFields(array("ID_TIPO_IMBALLAGGIO", "ALTRO_TIPO_IMBALLAGGIO"),$TableName);

$FEDIT->FGE_SetFormFields("COLLI",$TableName);

#
#	ADR
#
$FEDIT->FGE_SetFormFields(array("adr","QL","ID_ONU"),$TableName);

#
#	PRODUTTORE
#
$FEDIT->FGE_SetFormFields(array("ID_AZP","FKEcfiscP","ID_UIMP"),$TableName);

#
#	TRASPORTATORE
#
$FEDIT->FGE_SetFormFields(array("ID_AZT","FKEcfiscT","ID_UIMT","ID_AUTHT","FKErilascioT","FKEscadenzaT","NumAlboAutotraspProprio","NumAlboAutotrasp","NPER","SenzaTrasporto"),$TableName);

#
#	DESTINATARIO
#
$FEDIT->FGE_SetFormFields(array("ID_AZD","FKEcfiscD","ID_UIMD","Transfrontaliero","ID_AUTHD","FKErilascioD","FKEscadenzaD","ID_OP_RS","ID_TRAT"),$TableName);

#
#	INTERMEDIARIO
#
$FEDIT->FGE_SetFormFields(array("ID_AZI","FKEcfiscI","ID_UIMI","ID_AUTHI","FKErilascioI","FKEscadenzaI"),$TableName);

#
#	INFORMAZIONI COMPILATORE
#
$FEDIT->FGE_SetFormFields("ID_USR",$TableName);
$FEDIT->FGE_SetFormFields(array("USER_nome","USER_cognome","USER_telefono","USER_email"),$TableName);

#
#	UNIQUE STRING TO BLOCK DUPLICATION
#
$FEDIT->FGE_SetFormFields("UniqueString",$TableName);

#
#	CAMPI DEL "DETTAGLIO" DA COMPILARE CON LAVORI DI DEFAULT
#
#	SCHEDA DI TRASPORTO
#
$FEDIT->FGE_SetFormFields(array("ID_COMM","ID_CAR"),$TableName);

#
#	RIFIUTO: CONFERIMENTO E PRESCRIZIONI
#
$FEDIT->FGE_SetFormFields(array("CAR_NumDocumento","CAR_Laboratorio","CAR_DataAnalisi","prescrizioni_mov"),$TableName);

$FEDIT->FGE_SetFormFields(array("StampaAnnua"),$TableName);

if(!$IS_SCARICO_INTERNO)
	$FEDIT->FGE_SetFormFields("NOTEF",$TableName);
$FEDIT->FGE_SetFormFields("NOTER",$TableName);

#
require("__mov_snipplets/FRM_Scarico_DisHide.php");
#

if((!$IS_SCHEDA_SISTRI || !$IS_SCHEDA_SISTRI_OPEN)) $DefaultClass='imageRif'; else $DefaultClass='imageRifDangerous';
	$imageRif = '<div id="imageRif" class="'.$DefaultClass.'">&nbsp;&nbsp;</div>';

$rosetteD = '<div id="rosetteD" class="rosette">&nbsp;&nbsp;</div>';
$rosetteT = '<div id="rosetteT" class="rosette">&nbsp;&nbsp;</div>';
$rosetteI = '<div id="rosetteI" class="rosette">&nbsp;&nbsp;</div>';

$FEDIT->FGE_SetTitle("ID_AZP","Produttore",$TableName);
$FEDIT->FGE_SetTitle("ID_RIF",$imageRif."Rifiuto",$TableName);
$FEDIT->FGE_SetTitle("adr","ADR",$TableName);
$FEDIT->FGE_SetTitle("ID_AZT",$rosetteT."Trasportatore",$TableName);

if($SOGER->UserData["core_usersFRM_LAYOUT_DEST"]==0 OR $SOGER->UserData['workmode']!='destinatario')
	if($IS_SCARICO_INTERNO)
		$FEDIT->FGE_SetTitle("ID_AZD",$rosetteD."Gestore",$TableName);
	else
		$FEDIT->FGE_SetTitle("ID_AZD",$rosetteD."Destinatario",$TableName);
else{
	if($SOGER->UserData["core_usersFRM_LAYOUT_DEST"]==1)
		$FEDIT->FGE_SetTitle("ID_AZD",$rosetteD."Gestore",$TableName);
	else
		$FEDIT->FGE_SetTitle("ID_AZD",$rosetteD."Destinatario",$TableName);
	}

$FEDIT->FGE_SetTitle("ID_AZI",$rosetteI."Intermediario",$TableName);

if(!$IS_SCARICO_INTERNO)
	$FEDIT->FGE_SetTitle("NOTEF","Annotazioni",$TableName);
else
	$FEDIT->FGE_SetTitle("NOTER","Annotazioni",$TableName);

$FEDIT->FGE_SetTitle("USER_nome","Informazione compilatore",$TableName);

#
$ProfiloFields = $TableName;
include("SOGER_CampiProfilo.php");
$FEDIT->FGE_DescribeFields();

#
#	LOOKUP
#
//{{{ 

$FEDIT->FGE_LookUpDefault("ID_COMM",$TableName);
$FEDIT->FGE_LookUpDefault("ID_CAR",$TableName);

if($SOGER->UserData['core_impiantiMODULO_SIS']==1 AND $IS_SCHEDA_SISTRI){
	$FEDIT->FGE_LookUpCFG("ID_TIPO_IMBALLAGGIO",$TableName);
	$FEDIT->FGE_UseTables("sis_tipi_imballaggi");
	$FEDIT->FGE_SetSelectFields(array("ID_TIPO_IMBALLAGGIO","TIPO_IMBALLAGGIO"),"sis_tipi_imballaggi");
	$FEDIT->FGE_HideFields(array("ID_TIPO_IMBALLAGGIO"),"sis_tipi_imballaggi");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	}

$autoTRADEST = true;
require("__mov_snipplets/LKUP_Rifiuto.php");
require("__mov_snipplets/LKUP_Produttori.php");
require("__mov_snipplets/LKUP_ProduttoriImp.php");
require("__mov_snipplets/LKUP_Destinatari.php");      
require("__mov_snipplets/LKUP_DestinatariImp.php");
require("__mov_snipplets/LKUP_DestinatariAuth.php");
require("__mov_snipplets/LKUP_Trasportatori.php");
require("__mov_snipplets/LKUP_TrasportatoriImp.php");
require("__mov_snipplets/LKUP_TrasportatoriAuth.php");
require("__mov_snipplets/LKUP_Intermediari.php");
require("__mov_snipplets/LKUP_IntermediariImp.php");
require("__mov_snipplets/LKUP_IntermediariAuth.php");
require("__mov_snipplets/LKUP_OpRS.php");

$FEDIT->FGE_LookUpDefault("ID_ONU",$TableName,true);

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {
	if(isset($_SESSION['Fiscalizzato'])) unset($_SESSION['Fiscalizzato']);
	if(isset($_SESSION['Fiscale'])) unset($_SESSION['Fiscale']);
	#
	#	FILTRI di DEFAULT 
	#
	require("__mov_snipplets/FLTR_MovimentoDefault.php");
	#
	#	VALORI di DEFAULT
	#
	$FEDIT->FGE_SetValue("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],$TableName);
	$FEDIT->FGE_SetValue("TIPO","S",$TableName);
	if(!$IS_SCHEDA_SISTRI)
		$FEDIT->FGE_SetValue("TIPO_S_SCHEDA",0,$TableName);
	else
		$FEDIT->FGE_SetValue("TIPO_S_SCHEDA",1,$TableName);
	if(!$IS_SCARICO_INTERNO)
		$FEDIT->FGE_SetValue("TIPO_S_INTERNO",0,$TableName);
	else
		$FEDIT->FGE_SetValue("TIPO_S_INTERNO",1,$TableName);
	$FEDIT->FGE_SetValue("NFORM",$NumForm,$TableName);
	$FEDIT->FGE_SetValue("NMOV",$NumMov,$TableName);
	$FEDIT->FGE_SetValue("quantita",0,$TableName);
	$FEDIT->FGE_SetValue("qta_hidden",0,$TableName);
	$FEDIT->FGE_SetValue("pesoN",0,$TableName);
	$FEDIT->FGE_SetValue("tara",0,$TableName);
	$FEDIT->FGE_SetValue("pesoL",0,$TableName);
	$FEDIT->FGE_SetValue("COLLI",1,$TableName);
	$FEDIT->FGE_SetValue("NPER",0,$TableName);
	$FEDIT->FGE_SetValue("adr",0,$TableName);
	$FEDIT->FGE_SetValue("QL",0,$TableName);
	$FEDIT->FGE_SetValue("ID_ONU",0,$TableName);
	$FEDIT->FGE_SetValue("Transfrontaliero",0,$TableName);
	$FEDIT->FGE_SetValue("FANGHI",0,$TableName);
	$FEDIT->FGE_SetValue("ID_TRAT",0,$TableName);
	//$FEDIT->FGE_SetValue("ID_COMM",$SOGER->UserData["core_usersID_COMM"],$TableName);
	//$FEDIT->FGE_SetValue("ID_CAR",$SOGER->UserData["core_usersID_CAR"],$TableName);
	$FEDIT->FGE_SetValue("DTMOV",$DtMov,$TableName);
	$FEDIT->FGE_SetValue("DTFORM",$DtForm,$TableName);
	$FEDIT->FGE_SetValue("VER_DESTINO",$SOGER->UserData["core_usersFRM_DefaultVD"],$TableName);
	
	if($SOGER->UserData["core_usersvia_diretta"]==1)
		$FEDIT->FGE_SetValue("percorso","VIA DIRETTA",$TableName);
	if(!$IS_SCARICO_INTERNO)
		$FEDIT->FGE_SetValue("SenzaTrasporto",0,$TableName);
	else
		$FEDIT->FGE_SetValue("SenzaTrasporto",1,$TableName);

	# DATI COMPILATORE
	$FEDIT->FGE_SetValue("ID_USR",$SOGER->UserData['core_usersID_USR'],$TableName);
	$FEDIT->FGE_SetValue("USER_nome",$SOGER->UserData['core_usersnome'],$TableName);
	$FEDIT->FGE_SetValue("USER_cognome",$SOGER->UserData['core_userscognome'],$TableName);
	$FEDIT->FGE_SetValue("USER_telefono",$SOGER->UserData['core_userstelefono'],$TableName);
	$FEDIT->FGE_SetValue("USER_email",$SOGER->UserData['core_usersemail'],$TableName);
	$FEDIT->FGE_DisableFields(array("USER_nome","USER_cognome","USER_telefono","USER_email"),$TableName);

	# UNIQUE STRING
	$Identifiers	= $SOGER->UserData['core_usersID_USR'].$SOGER->UserData['core_usersID_IMP'].$SOGER->UserData['workmode'];
	$UniqueString	= md5(uniqid($Identifiers, true));
	$FEDIT->FGE_SetValue("UniqueString", $UniqueString, $TableName);

	# DATI PRODUTTORE PER SCARICO INTERNO
	if($IS_SCARICO_INTERNO){
		$sql = "SELECT user_aziende_produttori.ID_AZP, codfisc, ID_UIMP FROM user_aziende_produttori JOIN user_impianti_produttori ON user_impianti_produttori.ID_AZP=user_aziende_produttori.ID_AZP WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND produttore=1 AND FuoriUnitaLocale=0;";
		$FEDIT->SDBRead($sql,"DbRecordSetProduttoreDefault",true,false);

		$FEDIT->FGE_SetValue("ID_AZP",$FEDIT->DbRecordSetProduttoreDefault[0]['ID_AZP'],$TableName);
		$FEDIT->FGE_SetValue("FKEcfiscP",$FEDIT->DbRecordSetProduttoreDefault[0]['codfisc'],$TableName);
		$FEDIT->FGE_SetValue("ID_UIMP",$FEDIT->DbRecordSetProduttoreDefault[0]['ID_UIMP'],$TableName);

		}

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"salva movimento");

	# SE DEVO FARE MOVIMENTO AUTOMATICO...
	if(isset($_SESSION['MovimentoRIF'])){
		
$js ="<script type=\"text/javascript\">\n";
$js.="var TableName='".$TableName."';";
$js.="var Rifiuto='".$_SESSION['MovimentoRIF']."';";
$js.= <<< ENDJS

$(document).ready ( function(){
   checkAndGo();
});

function checkAndGo() {
	var Element = document.getElementById(TableName+":ID_RIF");
    if (!Element) {
        setTimeout(checkAndGo, 500);
		} 
	else {
        AutoPopulate();
		}
	}


function AutoPopulate(){
	document.getElementById(TableName+":ID_RIF").value = Rifiuto;
	CUSTOM_RIFMOV('', document.getElementById(TableName+":ID_RIF"), '');
	}


ENDJS;
$js.= "</script>";
echo $js;
unset($_SESSION['MovimentoRIF']);
		
		}


	}
	
else {

	if(!isset($_SESSION['Fiscalizzato'])) $_SESSION['Fiscalizzato']=$_GET['Fiscalizzato'];
	if(!isset($_SESSION['Fiscale'])) $_SESSION['Fiscale']=$_GET['Fiscale'];

	$NotEditable = false;
	$NotEditableCausaSistri = false;

	# registro fiscale
	if($SOGER->UserData['core_impiantiREG_IND']=="1" && $_SESSION['Fiscalizzato']=="1") $NotEditable = true;
	if($SOGER->UserData['core_impiantiREG_IND']=="0" && $_SESSION['Fiscale']=="1")		$NotEditable = true;

	# sistri
	$sql = "SELECT idSIS, idSIS_scheda, DTMOV, NMOV, VER_DESTINO FROM ".$TableName." WHERE ".$_GET['pri']."=".$_GET['filter'].";";
	$FEDIT->SDBRead($sql,"DbRecordSetEditable",true,false);
	if(!is_null($FEDIT->DbRecordSetEditable[0]['idSIS']) || !is_null($FEDIT->DbRecordSetEditable[0]['idSIS_scheda'])) $NotEditableCausaSistri = true;
	# vecchio 14 gg
	$oggi			= mktime(0, 0, 0, date('m'), date('d'), date('Y'));
	$movimento		= explode('-', $FEDIT->DbRecordSetEditable[0]['DTMOV']);
	$movimento_dt	= $FEDIT->DbRecordSetEditable[0]['DTMOV']? mktime(0, 0, 0, $movimento[1], $movimento[2], $movimento[0]):$oggi;
	$differenza	= ($oggi - $movimento_dt)/(60*60*24);

	if($differenza>=$SOGER->UserData['core_usersGGedit_'.$SOGER->UserData['workmode']] AND $FEDIT->DbRecordSetEditable[0]['NMOV']!='9999999' )	$NotEditable = true;

	## COCCARDA DESTINATARIO
	$sql="SELECT ID_ORIGINE_DATI FROM user_autorizzazioni_dest JOIN ".$TableName." ON user_autorizzazioni_dest.ID_AUTHD=".$TableName.".ID_AUTHD WHERE ".$_GET['pri']."='".$_GET['filter']."';";
	$FEDIT->SDBRead($sql,"DbRecordSetRosetteD",true,false);
	$ID_ORIGINE_DATI_D="'".$FEDIT->DbRecordSetRosetteD[0]['ID_ORIGINE_DATI']."'";

	## COCCARDA TRASPORTATORE
	$sql="SELECT ID_ORIGINE_DATI FROM user_autorizzazioni_trasp JOIN ".$TableName." ON user_autorizzazioni_trasp.ID_AUTHT=".$TableName.".ID_AUTHT WHERE ".$_GET['pri']."='".$_GET['filter']."';";
	$FEDIT->SDBRead($sql,"DbRecordSetRosetteT",true,false);
	$ID_ORIGINE_DATI_T="'".$FEDIT->DbRecordSetRosetteT[0]['ID_ORIGINE_DATI']."'";

	## COCCARDA INTERMEDIARIO
	$sql="SELECT ID_ORIGINE_DATI FROM user_autorizzazioni_interm JOIN ".$TableName." ON user_autorizzazioni_interm.ID_AUTHI=".$TableName.".ID_AUTHI WHERE ".$_GET['pri']."='".$_GET['filter']."';";
	$FEDIT->SDBRead($sql,"DbRecordSetRosetteI",true,false);
	$ID_ORIGINE_DATI_I="'".$FEDIT->DbRecordSetRosetteI[0]['ID_ORIGINE_DATI']."'";

	if(isset($_SESSION["MovimentoEdit_aPosteriori"])) {
		$_SESSION["MovimentoEdit_aPosteriori"] = true;
		$FEDIT->FGE_DisableFields(array("ID_RIF","FKESF","FKEumis","FKEpesospecifico"),$TableName);
		}

	if($NotEditable){
		$FEDIT->FGE_DisableFields(array("ID_RIF","FKESF","FKEumis","FKEpesospecifico","lotto","NPER","FANGHI"),$TableName);
		$FEDIT->FGE_DisableFields(array("quantita","tara","pesoL", "COLLI"),$TableName);
		$FEDIT->FGE_DisableFields(array("VER_DESTINO", "adr", "QL", "ID_ONU"),$TableName);
		$FEDIT->FGE_DisableFields(array("NFORM", "DTFORM", "NMOV", "DTMOV"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_AZP","ID_UIMP"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_AZD","ID_UIMD","ID_AUTHD"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_AZT","ID_UIMT","ID_AUTHT"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_AZI","ID_UIMI","ID_AUTHI"),$TableName);
		if($SOGER->UserData['core_impiantiMODULO_SIS']==1 AND $IS_SCHEDA_SISTRI)
			$FEDIT->FGE_DisableFields(array("ID_TIPO_IMBALLAGGIO","ALTRO_TIPO_IMBALLAGGIO"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_OP_RS", "SenzaTrasporto"),$TableName);
		}
	elseif($NotEditableCausaSistri){
		$FEDIT->FGE_DisableFields(array("ID_RIF","FKESF","FKEumis","FKEpesospecifico","lotto","NPER","FANGHI"),$TableName);
		$FEDIT->FGE_DisableFields(array("adr", "QL", "ID_ONU"),$TableName);
		$FEDIT->FGE_DisableFields(array("NMOV", "DTMOV"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_AZP","ID_UIMP"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_AZD","ID_UIMD","ID_AUTHD"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_AZT","ID_UIMT","ID_AUTHT"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_AZI","ID_UIMI","ID_AUTHI"),$TableName);
		if($SOGER->UserData['core_impiantiMODULO_SIS']==1 AND $IS_SCHEDA_SISTRI)
			$FEDIT->FGE_DisableFields(array("ID_TIPO_IMBALLAGGIO","ALTRO_TIPO_IMBALLAGGIO"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_OP_RS", "SenzaTrasporto"),$TableName);
		}

	# verifico se devo bloccare movimento perch� aperto da altri
	if($SOGER->UserData['core_usersO12']=='0'){
	
	$sql = "SELECT ID_USR FROM ".$_GET['table']." WHERE ".$_GET['pri']."=".$_GET['filter'].";";
	$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
	
	if($FEDIT->DbRecordSet[0]['ID_USR']!='0000000' AND ($SOGER->UserData['core_usersID_USR']!=$FEDIT->DbRecordSet[0]['ID_USR'])){
		echo "<div id=\"Errors\" class=\"ErrType1\" onclick=\"javascript:document.getElementById('Errors').style.zIndex=-1\">";
		echo "L'utente non ha i permessi necessari per modificare i movimenti inseriti da altri utenti [cod. O12]";
		echo "</div>";
		$FEDIT->FGE_DisableFields(array("ID_RIF","FKESF","FKEumis","FKEpesospecifico","lotto","NPER","FANGHI"),$TableName);
		$FEDIT->FGE_DisableFields(array("quantita","tara","pesoL","pesoN","COLLI"),$TableName);
		$FEDIT->FGE_DisableFields(array("VER_DESTINO", "adr", "QL", "ID_ONU"),$TableName);
		$FEDIT->FGE_DisableFields(array("NMOV", "DTMOV", "NFORM", "DTFORM"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_AZP","FKEcfiscP","ID_UIMP"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_AZD","FKEcfiscD","ID_UIMD","ID_AUTHD","FKErilascioD","ID_OP_RS"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_AZT","FKEcfiscT","ID_UIMT","ID_AUTHT","FKErilascioT"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_AZI","FKEcfiscI","ID_UIMI","ID_AUTHI","FKErilascioI"),$TableName);
		if($SOGER->UserData['core_impiantiMODULO_SIS']==1 AND $IS_SCHEDA_SISTRI)
			$FEDIT->FGE_DisableFields(array("ID_TIPO_IMBALLAGGIO","ALTRO_TIPO_IMBALLAGGIO"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_OP_RS", "SenzaTrasporto"),$TableName);
		$FEDIT->FGE_DisableFields(array("NOTEF", "NOTER"),$TableName);
		}
	}

	
	$FEDIT->FGE_DisableFields(array("USER_nome","USER_cognome","USER_telefono","USER_email"),$TableName);

	$_SESSION["IDMovimento"] = $_GET["filter"];
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva movimento");
	require("__includes/USER_FormularioFiscaleSiNo.inc");
	
	$RIF_REF			= $FEDIT->DbRecordSet[0]["ID_RIF"];
	
	## IMMAGINE RIFIUTO e CLASSIFICAZIONE HP
	$sql="SELECT pericoloso, adr, ClassificazioneHP FROM user_schede_rifiuti WHERE ID_RIF='".$FEDIT->DbRecordSet[0]["ID_RIF"]."'";
	$FEDIT->SDBRead($sql,"DbRecordSetImageRif",true,false);
	$ADR		="'".$FEDIT->DbRecordSetImageRif[0]['adr']."'";
	$PERICOLOSO ="'".$FEDIT->DbRecordSetImageRif[0]['pericoloso']."'";
	$ClassificazioneHP =$FEDIT->DbRecordSetImageRif[0]['ClassificazioneHP'];
	$LegamiSISTRI	= false;
        //if($SOGER->UserData['workmode']=='produttore')
            require("__scripts/MovimentiRifMovCarico.php");
	}


echo '<form name="tmp_contoterzi">';
echo '<input type="hidden" value="0" id="canSave" name="canSave" />';
echo '</form>';
echo '<form name="tmp_unique">';
echo '<input type="hidden" value="0" id="isUnique" name="isUnique" />';
echo '</form>';

echo '<input type="hidden" value="'.$ClassificazioneHP.'" id="HiddenField_ClassificazioneHP" name="HiddenField_ClassificazioneHP" />';


#
require_once("__includes/COMMON_sleepForgEdit.php");
?>

<?php

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {
	$js = "<script type=\"text/javascript\">\n";
	$js.= "document.getElementById('".$TableName.":NFORM').style.background = 'yellow';";
	$js.= "document.getElementById('".$TableName.":NFORM').style.color = 'black';";
	$js.= "var NFORM = document.getElementById('".$TableName.":NFORM').value;";
	$js.= "var DTFORM = document.getElementById('".$TableName.":DTFORM').value;";

	$js.= "</script>";
	echo $js;
	}
else{


$js ="<script type=\"text/javascript\">\n";
$js.= <<< ENDJS
CUSTOM_ROSETTE('rosetteD',$ID_ORIGINE_DATI_D);
CUSTOM_ROSETTE('rosetteT',$ID_ORIGINE_DATI_T);
CUSTOM_ROSETTE('rosetteI',$ID_ORIGINE_DATI_I);
CUSTOM_IMAGE_RIF($PERICOLOSO,$ADR);

var NFORM = document.getElementById('$TableName:NFORM').value;
var DTFORM = document.getElementById('$TableName:DTFORM').value;

ENDJS;
$js.= "</script>";

if($SOGER->UserData["core_usersFRM_LAYOUT_DEST"]==0 OR $SOGER->UserData['workmode']=='produttore')
	echo $js;

}

?>