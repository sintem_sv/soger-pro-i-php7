<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
#
$FEDIT->FGE_UseTables("core_users","core_impianti");
$FEDIT->FGE_SetSelectFields("description","core_impianti");
$FEDIT->FGE_SetSelectFields(array("usr", "nome", "cognome", "sresponsabile", "description", "email", "telefono", "manutenzione"),"core_users");
$FEDIT->FGE_SetFilter("sresponsabile",0,"core_users");
$FEDIT->FGE_HideFields(array("sresponsabile"),"core_users");

#
$FEDIT->FGE_DescribeFields();
#
echo($FEDIT->FGE_DataGrid($SOGER->AppDescriptiveLocation,"__scripts/FGE_DataGridEdit.php","EDC--",true,"FEDIT",$_SESSION['SearchingFilter']));

#
require_once("__includes/COMMON_sleepForgEdit.php");
?>