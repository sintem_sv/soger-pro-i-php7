<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();

$FEDIT->FGE_UseTables("user_contratti_pro_cond");
$FEDIT->FGE_SetFormFields(array("ID_CNT_P", "description","euro","ID_UM_CNT","APPLY_W_INT"),"user_contratti_pro_cond");
$FEDIT->FGE_SetFilter("ID_CNT_P",$_SESSION['ID_CONTRACT'],"user_contratti_pro_cond");


if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {

	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_SetValue("description","","user_contratti_pro_cond");
	$FEDIT->FGE_SetValue("euro",0,"user_contratti_pro_cond");
	$FEDIT->FGE_SetValue("ID_CNT_P",$_SESSION['ID_CONTRACT'],"user_contratti_pro_cond");
	$FEDIT->FGE_HideFields("ID_CNT_P","user_contratti_pro_cond");
	
	#
	$FEDIT->FGE_LookUpDefault("ID_UM_CNT","user_contratti_pro_cond");
	#

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea condizione contrattuale");
$js ="<script type=\"text/javascript\">\n";
$js.= <<<JS
document.getElementById('user_contratti_pro_cond:APPLY_W_INTB').checked=true;
document.getElementById('user_contratti_pro_cond:APPLY_W_INTA').checked=false;
JS;
$js.="</script>\n";
echo $js;
} else {

	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_UM_CNT","user_contratti_pro_cond");
	$FEDIT->FGE_SetValue("ID_CNT_P",$_SESSION['ID_CONTRACT'],"user_contratti_pro_cond");
	$FEDIT->FGE_HideFields("ID_CNT_P","user_contratti_pro_cond");

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"salva condizione contrattuale");
}
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>