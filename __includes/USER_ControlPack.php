<?php
global $SOGER;
?>

<form name="00" method="post" action="" style="width: 95%">
<fieldset class="FGEfieldset">
<legend class="FGElegend">CONTROLLO DI MOVIMENTAZIONE</legend>
<table width="100%">

<tr>
	
	<td style="text-align: center">
		<input type="button" class="FGESTATbutton" tabindex="1" id="1" value="LISTA TARGHE/VIAGGI ADR" onclick="self.location='__scripts/status.php?area=UserStatsCfgCP&action=Gogo&Stat=ListaTargheViaggiAdr'" onmouseover="document.getElementById('1').className='FGESTATbutton_over'" onmouseout="document.getElementById('1').className='FGESTATbutton'" />
        </td>
	
	<td style="text-align: center">		
		<input type="button" class="FGESTATbutton" tabindex="3" id="3" value="ORIGINE DATI DESTINATARI" onclick="self.location='__scripts/status.php?area=UserStatsCfgCP&action=Gogo&Stat=OrigineDatiD'" onmouseover="document.getElementById('3').className='FGESTATbutton_over'" onmouseout="document.getElementById('3').className='FGESTATbutton'" />
	</td>

	<td style="text-align: center">		
		<input type="button" class="FGESTATbutton" tabindex="4" id="4" value="ORIGINE DATI TRASPORTATORI" onclick="self.location='__scripts/status.php?area=UserStatsCfgCP&action=Gogo&Stat=OrigineDatiT'" onmouseover="document.getElementById('4').className='FGESTATbutton_over'" onmouseout="document.getElementById('4').className='FGESTATbutton'" />
	</td>

</tr>

<tr>

	<td style="text-align: center">	
		<input type="button" class="FGESTATbutton" tabindex="5" id="5" value="PESI NON CONFORMI" onclick="self.location='__scripts/status.php?area=UserStatsCfgCP&action=Gogo&Stat=PesiNonConformi'" onmouseover="document.getElementById('5').className='FGESTATbutton_over'" onmouseout="document.getElementById('5').className='FGESTATbutton'" />
	</td>

	<td style="text-align: center">
		<input type="button" class="FGESTATbutton" tabindex="6" id="6" value="CONTROLLO PESI DESTINATARI" onclick="self.location='__scripts/status.php?area=UserStatsCfgCP&action=Gogo&Stat=ControlloPesiDestinatari'" onmouseover="document.getElementById('6').className='FGESTATbutton_over'" onmouseout="document.getElementById('6').className='FGESTATbutton'" />
	</td>

	<td style="text-align: center">		
		<input type="button" class="FGESTATbutton" tabindex="7" id="7" value="DESTINAZIONE RIFIUTI" onclick="self.location='__scripts/status.php?area=UserStatsCfgCP&action=Gogo&Stat=DestinazioneRifiuti'" onmouseover="document.getElementById('7').className='FGESTATbutton_over'" onmouseout="document.getElementById('7').className='FGESTATbutton'" />
	</td>

</tr>

<tr>
	
	<td style="text-align: center">
		<input type="button" class="FGESTATbutton" tabindex="9" id="9" value="BILANCIO AMBIENTALE" onclick="self.location='__scripts/status.php?area=UserStatsCfgCP&action=Gogo&Stat=BilancioRD'" onmouseover="document.getElementById('9').className='FGESTATbutton_over'" onmouseout="document.getElementById('9').className='FGESTATbutton'" />
	</td>

	<td style="text-align: center">	
		<input type="button" class="FGESTATbutton" tabindex="10" id="10" value="TRASPORTI NON COERENTI" onclick="self.location='__scripts/status.php?area=UserStatsCfgCP&action=Gogo&Stat=TraspIncoerenti'" onmouseover="document.getElementById('10').className='FGESTATbutton_over'" onmouseout="document.getElementById('10').className='FGESTATbutton'" />
	</td>

	<td style="text-align: center">	
		<input type="button" class="FGESTATbutton" tabindex="11" id="11" value="COSTO VIAGGI" onclick="self.location='__scripts/status.php?area=UserStatsCfgCP&action=Gogo&Stat=CostoViaggi'" onmouseover="document.getElementById('11').className='FGESTATbutton_over'" onmouseout="document.getElementById('11').className='FGESTATbutton'" />
	</td>

</tr>

<tr>
	<?php if($SOGER->UserData['workmode']=='produttore'){ ?>
	<td style="text-align: center">
		<input type="button" class="FGESTATbutton" tabindex="12" id="12" value="IV COPIE MANCANTI" onclick="self.location='__scripts/status.php?area=UserStatsCfgCP&action=Gogo&Stat=IVcopie'" onmouseover="document.getElementById('12').className='FGESTATbutton_over'" onmouseout="document.getElementById('12').className='FGESTATbutton'" />
	</td>
	<?php }else{ ?>
	<td style="text-align: center">
		<input type="button" class="FGESTATbutton" tabindex="12" id="12" value="IV COPIE DA RESTITUIRE" onclick="self.location='__scripts/status.php?area=UserStatsCfgCP&action=Gogo&Stat=IVcopieDaRestituire'" onmouseover="document.getElementById('12').className='FGESTATbutton_over'" onmouseout="document.getElementById('12').className='FGESTATbutton'" />
	</td>
	<?php } ?>
        
        <td style="text-align: center">
		<input type="button" class="FGESTATbutton" tabindex="13" id="13" value="DURATA VIAGGI" onclick="self.location='__scripts/status.php?area=UserStatsCfgCP&action=Gogo&Stat=DurataViaggi'" onmouseover="document.getElementById('13').className='FGESTATbutton_over'" onmouseout="document.getElementById('13').className='FGESTATbutton'" />
	</td>

	<td style="text-align: center">
		<input type="button" class="FGESTATbutton" tabindex="14" id="14" value="CONTROLLO PESO AUTOMEZZI" onclick="self.location='__scripts/status.php?area=UserStatsCfgCP&action=Gogo&Stat=PesoMezzi'" onmouseover="document.getElementById('14').className='FGESTATbutton_over'" onmouseout="document.getElementById('14').className='FGESTATbutton'" />
	</td>

</tr>

<tr>
	<td style="text-align: center">
		<input type="button" class="FGESTATbutton" tabindex="15" id="15" value="ANALISI DATI CONFERIMENTI - WMS 2012" onclick="self.location='__scripts/status.php?area=UserStatsCfgCP&action=Gogo&Stat=ADC'" onmouseover="document.getElementById('15').className='FGESTATbutton_over'" onmouseout="document.getElementById('15').className='FGESTATbutton'" />
	</td>
        
	<td style="text-align: center">
		<input type="button" class="FGESTATbutton" tabindex="16" id="16" value="COMUNI CONFERITORI" onclick="self.location='__scripts/status.php?area=UserStatsCfgCP&action=Gogo&Stat=ComuniConferitori'" onmouseover="document.getElementById('16').className='FGESTATbutton_over'" onmouseout="document.getElementById('16').className='FGESTATbutton'" />
	</td>

	<td style="text-align: center">
		<input type="button" class="FGESTATbutton" tabindex="17" id="17" value="CSV MOVIMENTAZIONE" onclick="self.location='__scripts/status.php?area=UserStatsCfgCP&action=Gogo&Stat=CSVmovimentazione'" onmouseover="document.getElementById('17').className='FGESTATbutton_over'" onmouseout="document.getElementById('17').className='FGESTATbutton'" />
	</td>

</tr>

<tr>

    <td style="text-align: center">
        <input type="button" class="FGESTATbutton" tabindex="18" id="18" value="FIR VERIFICATI PRESSO ALBO GESTORI" onclick="self.location='__FDA/FDA_report.php'" onmouseover="document.getElementById('18').className='FGESTATbutton_over'" onmouseout="document.getElementById('18').className='FGESTATbutton'" />
    </td>

    <td style="text-align: center">
        &nbsp;
    </td>

    <td style="text-align: center">
        &nbsp;
    </td>

</tr>

</table>
</fieldset>
</form>