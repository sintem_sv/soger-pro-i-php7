<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
#

$FEDIT->FGE_UseTables("core_users","core_impianti");
$FEDIT->FGE_SetSelectFields(array("ID_IMP","description"),"core_impianti");
$FEDIT->FGE_SetSelectFields(array("usr", "nome", "cognome", "description", "email", "telefono", "manutenzione","sresponsabile","isSintem"),"core_users");
$FEDIT->FGE_SetFilter("sresponsabile",0,"core_users");
$FEDIT->FGE_SetFilter("isSintem",0,"core_users");
$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData['core_usersID_IMP'],"core_impianti");
$FEDIT->FGE_HideFields(array("ID_IMP","description"),"core_impianti");
$FEDIT->FGE_HideFields(array("sresponsabile","isSintem","description"),"core_users");

#
$FEDIT->FGE_DescribeFields();
#
echo($FEDIT->FGE_DataGrid($SOGER->AppDescriptiveLocation,"__scripts/FGE_DataGridEdit.php","EDC--",true,"FEDIT",$_SESSION['SearchingFilter']));

echo "<div class='FGE4Col'>\n";
echo "<input type='button' name='nuovo' class='FGEbutton' value='crea nuovo utente' onclick='javascript: document.location=\"__scripts/new_utente.php\"' />\n";
echo "</div>\n";
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>

