<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);


session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__libs/SQLFunct.php");

	## SELEZIONO INFO SUI RIFIUTI CHE HANNO CONTENITORE OPPURE MAXSTOCK

	$SQL = "SELECT user_schede_rifiuti.ID_RIF, user_schede_rifiuti.ID_UMIS, user_schede_rifiuti_deposito.carico_automatico, user_schede_rifiuti_deposito.intervallo, user_schede_rifiuti_deposito.ID_ICA, user_schede_rifiuti_deposito.MaxStock, user_schede_rifiuti_deposito.NumCont, user_schede_rifiuti.FKEdisponibilita as disponibilita, ";
	$SQL.= "lov_contenitori.portata, lov_contenitori.m_cubi, user_schede_rifiuti_deposito.last_carico ";
	$SQL.= "FROM user_schede_rifiuti ";
	$SQL.= "JOIN user_schede_rifiuti_deposito ON user_schede_rifiuti_deposito.ID_RIF=user_schede_rifiuti.ID_RIF ";
	$SQL.= "LEFT JOIN lov_contenitori ON user_schede_rifiuti_deposito.ID_CONT=lov_contenitori.ID_CONT ";
	$SQL.= "WHERE user_schede_rifiuti_deposito.ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."' ";
	$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
	//die($SQL);
	$RIFS = $FEDIT->DbRecordSet;
	


	for($r=0;$r<count($RIFS);$r++){
	
		//echo "<h1>".date("H:i:s").": inizio ciclo for</h1>";
		
		// MaxStock
		if($RIFS[$r]['MaxStock']>0){
			$MaxStock=$RIFS[$r]['MaxStock'] * $RIFS[$r]['NumCont'];
			}
		else{ // portata o m.cubi contenitore
			switch($RIFS[$r]['ID_UMIS']){
				case "1": // Kg
					$MaxStock=$RIFS[$r]['portata'] * $RIFS[$r]['NumCont'];
					break;
				case "2": // Litri
					$MaxStock=$RIFS[$r]['m_cubi'] * $RIFS[$r]['NumCont'] * 1000;
					break;
				case "3": // Mc
					$MaxStock=$RIFS[$r]['m_cubi'] * $RIFS[$r]['NumCont'];
					break;
				}
			}

		//echo "<h1>".date("H:i:s").": ho definito MaxStock</h1>";

		// Disponibilit�
		$Dispo=$RIFS[$r]['disponibilita'];

		// Spazio
		$Spazio=$MaxStock-$Dispo;

		// Data di oggi
		$Oggi=mktime(0, 0, 0, date('m'), date('d'), date('Y'));

		// Data Last Carico
		$LastC=explode("-", $RIFS[$r]['last_carico']);
		$LastC=mktime(0, 0, 0, $LastC[1], $LastC[2], $LastC[0]);

		// Differenza
		$differenza=($Oggi - $LastC)/(60*60*24);

		//echo "<h1>".date("H:i:s").": differenza tra date fatta</h1>";

		if($RIFS[$r]['carico_automatico']>0){
			if($RIFS[$r]['ID_ICA']=='1') //1=giorni, 2=settimane
				$Intervallo=$RIFS[$r]['intervallo'];
			else
				$Intervallo=$RIFS[$r]['intervallo'] * 7;
			$Qta=$RIFS[$r]['carico_automatico'];
			if($Spazio>0){
				$NumCarichi = ceil($Spazio/$Qta);
				$Giorni = $Intervallo * $NumCarichi - $differenza;
				}
			else{
				$Giorni=0;
				}
			//echo "<h1>".date("H:i:s").": ho carichi automatici</h1>";
			}
		else{ 
			// mi baso sulla media giornaliera		
			$sql = "SELECT media_gg_eff FROM user_schede_rifiuti_deposito WHERE ID_RIF='".$RIFS[$r]['ID_RIF']."'";
			$FEDIT->SDBRead($sql,"DbRecordsetMGE",true,false);
			if($FEDIT->DbRecsNum>0){
				$MediaCggRound=$FEDIT->DbRecordsetMGE[0]['media_gg_eff'];
				}
			else{
				$MediaCggRound=0;
				}

			if($Spazio>0 && $MediaCggRound>0)
				$Giorni=ceil($Spazio/$MediaCggRound);
			else
				$Giorni=0;
			//echo "<h1>".date("H:i:s").": mi baso su media giornaliera</h1>";
			}

		// $Giorni indica tra quanti giorni LAVORATIVI avr� il contenitore pieno
		// Aggiungo $Giorni ad oggi escludendo i festivi

		//echo "<h1>".date("H:i:s").": Giorni = ".$Giorni."</h1><hr>";

		if($Giorni>0){
			// Calcolo del giorno di Pasqua fino all'ultimo anno valido
			for ($i=2006; $i<=2037; $i++) {
				$pasqua = date("Y-m-d", easter_date($i));
				$array_pasqua[] = $pasqua;
				}
			// Calcolo le rispettive pasquette
			foreach($array_pasqua as $pasqua){
				list ($anno,$mese,$giorno) = explode("-",$pasqua);
				$pasquetta = mktime (0,0,0,date($mese),date($giorno)+1,date($anno));
				$array_pasquetta[] = $pasquetta;
				}
			// Giorni festivi sempre
			$giorniFestivi = array("01-01",
						   "01-06",
						   "04-25",
						   "05-01",
						   "06-02",
						   "08-15",
						   "11-01",
						   "12-08",
						   "12-25",
						   "12-26",
						   );

			$G=1;
			$GiorniCounter=0;

			while($GiorniCounter<$Giorni){
				
				$Data = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d')+$G, date('Y')));
				$mese_giorno = date('m-d',strtotime($Data));

				//echo $Data." ( giorno ".$GiorniCounter." di ".$Giorni." ) <hr>";$GiorniCounter++;
				if( date('w',strtotime($Data))!='0' && date('w',strtotime($Data))!='6' && !in_array(strtotime($Data),$array_pasquetta) && !in_array($mese_giorno,$giorniFestivi) ){
					$GiorniCounter++;
					$PREV_NextS = $Data;
					}

				$G++;

				}

			// aggiorno tabella db
			$SQL ="UPDATE user_schede_rifiuti SET PREV_NextS='".$PREV_NextS."' WHERE ID_RIF='".$RIFS[$r]['ID_RIF']."'";
			$FEDIT->SDBWrite($SQL,true,false);
			}
		else{
			// $Giorni==0; il contenitore � GIA' pieno
			// aggiorno tabella db
			$SQL ="UPDATE user_schede_rifiuti SET PREV_NextS='".date('Y-m-d')."' WHERE ID_RIF='".$RIFS[$r]['ID_RIF']."'";
			$FEDIT->SDBWrite($SQL,true,false);
			}

		}

$SOGER->SetFeedback("Previsione prossimi conferimenti aggiornata","2");


require_once("../__includes/COMMON_sleepForgEdit.php");
require_once("../__includes/COMMON_sleepSoger.php");

header("location: ../__scripts/status.php?area=UserGestioneDeposito_organizza");
?>