<?php

$dialog = <<<DIALOG

<div id="Popup_DuplicazioneRifiuto" title="">

	<table cellpadding="0" cellspacing="0" style="width:460px;">

		<tr>
			<td rowspan="2" style="width:120px;text-align:center;"><img src="__css/bidoncino.png" /></td>
			<td style="width:280px;">Si tratta di una duplicazione a seguito di riclassificazione?</td>
			<td style="width:100px;text-align:right;">
				<input type="radio" name="riclassificazione" value="1" />S�
				<input type="radio" name="riclassificazione" value="0" checked />No
			</td>
		</tr>

		<tr>
			<td>Desideri associare la nuova scheda rifiuto ai medesimi fornitori dell'originale?</td>
			<td style="text-align:right;">
				<input type="radio" name="fornitori" value="1" />S�
				<input type="radio" name="fornitori" value="0" checked />No
			</td>
		</tr>

	</table>

</div>

<input type="hidden" id="originalID_RIF" value="" />
<input type="hidden" id="redirection" value="" />

DIALOG;

echo $dialog;

?>