<div id="Popup_RicalcoloGiacenzeFinali" title="">

    <table cellpadding="0" cellspacing="0" style="width:460px;">

        <tr>
            <td>La procedura consente al programma di ricalcolare le giacenze finali del database in uso e di aggiornare le giacenze iniziali del database dell'anno seguente.<br /><br /></td>
        </tr>

        <tr>
            <td style="width:120px;text-align:center;"><img src="__css/ricalcolo_giacenze_finali.png" /></td>
        </tr>

        <tr>
            <td style="width:340px;">Si raccomanda di utilizzare la funzione con la dovuta cautela, in particolare qualora nel database dell'anno seguente siano gi� state inserite delle registrazioni.</td>
        </tr>

    </table>

    <div id="RicalcoloGiacenzeFinali_progress" style="display:none;margin-bottom:20px;margin-top:20px;">
        <table style="width:100%;">
            <tr>
                <td style="width:50%;"><b>Stato di avanzamento:</b></td>
                <td style="width:50%;text-align:right;" id="RicalcoloGiacenzeFinali_response"></td>
            </tr>
        </table>
    </div>

</div>