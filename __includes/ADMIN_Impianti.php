<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("core_impianti");
$FEDIT->FGE_SetFormFields(array("ID_IMP","ID_GRP","description","indirizzo","ID_COM", "codfisc","ID_ATECO_07","IN_ITALIA","telefono","fax","ente","produttore","trasportatore","destinatario","intermediario", "MODULO_SIS", "MODULO_WMS", "MODULO_ECO", "MODULO_LDP"),"core_impianti");
$FEDIT->FGE_SetFormFields(array("MODULO_FDA","FDA_START_1","FDA_END_1","FDA_CREDITI_1","FDA_RESIDUI_1","FDA_START_2","FDA_END_2","FDA_CREDITI_2"),"core_impianti");
$FEDIT->FGE_SetBreak("FDA_START_1","core_impianti");

$FEDIT->FGE_SetTitle("ID_IMP", "BackEnd", "core_impianti");
$FEDIT->FGE_SetTitle("description", "Dati anagrafici", "core_impianti");
$FEDIT->FGE_SetTitle("produttore", "Registri carico/scarico", "core_impianti");
$FEDIT->FGE_SetTitle("MODULO_FDA", "InteroperabilitÓ Albo Gestori", "core_impianti");
$FEDIT->FGE_SetTitle("MODULO_SIS", "Moduli aggiuntivi", "core_impianti");

#
if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {

	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_SetValue("produttore",1,"core_impianti");
	$FEDIT->FGE_SetValue("trasportatore",0,"core_impianti");
	$FEDIT->FGE_SetValue("destinatario",0,"core_impianti");
	$FEDIT->FGE_SetValue("intermediario",0,"core_impianti");
	$FEDIT->FGE_LookUpDefault("ID_COM","core_impianti",true);
	$FEDIT->FGE_LookUpDefault("ID_GRP","core_impianti");
	$FEDIT->FGE_LookUpCFG("ID_ATECO_07","core_impianti",true);
	$FEDIT->FGE_UseTables("lov_ateco2007");
	$FEDIT->FGE_SetSelectFields(array("ATECO_07"),"lov_ateco2007");
	$FEDIT->FGE_SetOrder("lov_ateco2007:ATECO_07");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	$FEDIT->FGE_SetValue("ente","Ufficio Ecologia","core_impianti");
        $FEDIT->FGE_SetValue("MODULO_FDA",0,"core_impianti");
	$FEDIT->FGE_SetValue("MODULO_SIS",1,"core_impianti");
	$FEDIT->FGE_SetValue("MODULO_WMS",0,"core_impianti");
	$FEDIT->FGE_SetValue("MODULO_ECO",1,"core_impianti");
	$FEDIT->FGE_SetValue("MODULO_LDP",0,"core_impianti");
	$FEDIT->FGE_HideFields(array("IN_ITALIA"),"core_impianti");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea impianto");

} else {

	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_COM","core_impianti",true);
	$FEDIT->FGE_LookUpDefault("ID_GRP","core_impianti");
	$FEDIT->FGE_LookUpCFG("ID_ATECO_07","core_impianti",true);
	$FEDIT->FGE_UseTables("lov_ateco2007");
	$FEDIT->FGE_SetSelectFields(array("ATECO_07"),"lov_ateco2007");
	$FEDIT->FGE_SetOrder("lov_ateco2007:ATECO_07");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	$FEDIT->FGE_DisableFields("ID_IMP","core_impianti");
	$FEDIT->FGE_HideFields(array("IN_ITALIA"),"core_impianti");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica impianto");

}
require_once("__includes/COMMON_sleepForgEdit.php");
?>
