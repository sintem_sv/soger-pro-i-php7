<?php
$dialog = <<<DIALOG

<div id="Popup_ApprovazioneISO" title="Approvazione ISO del documento">

	<table cellpadding="0" cellspacing="0" style="width:460px;">

		<tr>
			<td rowspan="3" style="width:200px;"><img src="__css/iso.gif" /></td>
			<td style="width:130px;">Il documento �:</td>
			<td style="width:130px; text-align:right;">
				<select id="ISO_status" style="width:120px;">
					<option value="0">Non approvato</option>
					<option value="1">Approvato</option>
				</select>
			</td>
		</tr>

		<tr>
			<td style="width:130px;">Inizio approvazione:</td>
			<td style="width:130px;"><input type="text" id="ISO_start" style="text-align:right;" /></td>
		</tr>

		<tr>
			<td style="width:130px;">Fine approvazione:</td>
			<td style="width:130px;"><input type="text" id="ISO_end" style="text-align:right;" /></td>
		</tr>

	</table>

</div>

<input type="hidden" id="ISO_ID_RIF" value="" />
<input type="hidden" id="prefix" value="" />

DIALOG;
 
echo $dialog;
?>