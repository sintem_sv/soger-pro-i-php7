<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
require_once("__SIS/SIS_SOAP_SSL.php");
$FEDIT = unserialize($_SESSION['ForgEditBuffer']);
global $SOGER;
?>

<div style="width:280px;background-color:#FFFFFF;">

	<div class="divName" style="text-align:center;height:25px;width:100%;background-color: #4D6980;color: #ffffff;"><span style="font-weight: bold;padding:5px;font-size: 13pt;font-family: verdana;text-transform: uppercase;">PROFILO D'USO</span></div>

	<div>

		<table style="margin-bottom:20px;">
			<tr>
				<td width="50"><img src="__css/default_user.gif" /></td>
				<td style="font-size:8pt;padding-left:10px;">

				<!-- SELEZIONE IMPIANTO -->

				<form name="SelectImpianto" method="post" style="margin-top:0;" action="__scripts/login.php"><br />
					<?php
					$SQL ="SELECT core_impianti.ID_IMP, core_impianti.description, core_users.pwd ";
					$SQL.="FROM core_impianti JOIN core_users ON core_users.ID_IMP=core_impianti.ID_IMP ";
					$SQL.="WHERE core_users.usr='".$SOGER->UserData['core_usersusr']."' AND sadmin=0 AND manutenzione=0 AND bloccatoEcostudio=0 AND sresponsabile=0 ";
					$IMP=substr($SOGER->UserData['core_impiantiID_IMP'], 3);
					$SQL.="AND core_impianti.ID_IMP LIKE '%".$IMP."' ";
					$SQL.="GROUP BY (core_impianti.ID_IMP) ";
					$SQL.="ORDER BY core_impianti.ID_IMP ASC";
					$FEDIT->SdbRead($SQL,"DbRecordSetImp",true,false);

//					echo $SQL;



					echo "<select onactivate=\"this.style.width='auto';\" onblur=\"this.style.width='150px';\" style='width: 150px; margin-left:5px; font-size:10;' name='swap_impianto' onChange='javascript:imp_pwd=document.SelectImpianto[\"swap_impianto\"].value.split(\"|\");document.getElementById(\"core_userspwd\").value=imp_pwd[1];document.getElementById(\"core_impiantiID_IMP\").value=imp_pwd[0]; document.SelectImpianto.submit();' >\n";
					for($i=0;$i<count($FEDIT->DbRecordSetImp);$i++){
						echo "<option name='new_impianto' value='".$FEDIT->DbRecordSetImp[$i]['ID_IMP']."|".$FEDIT->DbRecordSetImp[$i]['pwd']."' id='' ";
						if($SOGER->UserData['core_impiantiID_IMP']==$FEDIT->DbRecordSetImp[$i]['ID_IMP'])
							echo "selected";
						echo " >".$FEDIT->DbRecordSetImp[$i]['ID_IMP']." - ".$FEDIT->DbRecordSetImp[$i]['description']."</option>";
						}
					echo "</select>\n";
					?>

					<input type="hidden" name="core_users:usr" id="core_usersusr" value="<?php echo $SOGER->UserData['core_usersusr']; ?>" />
					<input type="hidden" name="core_users:pwd" id="core_userspwd" value="<?php echo $SOGER->UserData['core_userspwd']; ?>" />
					<input type="hidden" name="core_users:ID_USR" id="core_usersID_USR" value="" />
					<input type="hidden" name="core_impianti:ID_IMP" id="core_impiantiID_IMP" value="" />
					<input type="hidden" name="FGE_PrimaryKeyRef" id="FGE_PrimaryKeyRef" value="core_users:ID_USR" />
					<input type="hidden" name="redirection" id="redirection" value="" />
				</form>


				<!-- SELEZIONE WORKMODE -->
					<form name="SelectProfile" method="post" style="margin-top:0;" action="__scripts/WorkMode.php"><br />

					<?php

					echo "<select style='width: 150px; margin-left:5px; font-size:10;' name=\"profile\" id=\"profile\" onChange=\"javascript:document.SelectProfile.submit();\" >\n";
					if($SOGER->UserData["core_impiantiproduttore"]=="1" AND $SOGER->UserData["core_usersproduttore"]=="1") {
						echo "<option name=\"profile\" value=\"produttore\" id=\"profileA\"" . checkProfile("produttore") . " >Produttore</option>\n";
					}
					if($SOGER->UserData["core_impiantitrasportatore"]=="1" AND $SOGER->UserData["core_userstrasportatore"]=="1") {
						echo "<option name=\"profile\" value=\"trasportatore\" id=\"profileB\"" . checkProfile("trasportatore") . " >Trasportatore</option>\n";
					}
					if($SOGER->UserData["core_impiantidestinatario"]=="1" AND $SOGER->UserData["core_usersdestinatario"]=="1") {
						echo "<option name=\"profile\" value=\"destinatario\" id=\"profileC\"" . checkProfile("destinatario") . " >Destinatario</option>\n";
					}
					if($SOGER->UserData["core_impiantiintermediario"]=="1" AND $SOGER->UserData["core_usersintermediario"]=="1") {
						echo "<option name=\"profile\" value=\"intermediario\" id=\"profileD\"" . checkProfile("intermediario") . " >Intermediario</option>\n";
					}

					echo "</select>\n";

					function checkProfile($value) {
						global $SOGER;
						if($SOGER->UserData["workmode"] == $value) {
							return " selected";
						}
					}
					?>

					<br/><br/>

					</form>
				</td>
			</tr>
<!--
			<tr>
				<td colspan="2">
				<div id="AssignAnag" style="width:264px;height:20px;text-align:center;margin-left:-10px;">
				<span style="background-color:#5C7D99;border:1px solid #ffffff;width:100%;padding:5px;">
				<a href="#" style="color:#ffffff;text-decoration:none;">Associa anagrafiche a reg. SISTRI</a></span>
				</div>
				</td>
			</tr>
-->
		</table>

	</div>



</div>



<!-- versione SISTRI -->
<?php
if($SOGER->UserData['core_impiantiMODULO_SIS']=='1')
	require_once("__SIS/View_DatiAccesso.php");
?>
<!-- -->

<!-- registro SISTRI -->
<?php
if($SOGER->UserData['core_impiantiMODULO_SIS']=='1')
	require_once("__SIS/Select_RegistroCronologico.php");
?>
<!-- -->

<!-- Crediti FDA -->
<?php
$isFDA          = $SOGER->UserData['core_impiantiMODULO_FDA']? true:false;
$isCurrentYear  = substr($FEDIT->DbServerData["db"], -4) == date('Y');
$isProduttore   = $SOGER->UserData['workmode'] == 'produttore';
if($isFDA && $isCurrentYear && $isProduttore)
    require_once("__FDA/CreditiResidui.php");
?>
<!-- -->




<?php
$_SESSION['ForgEditBuffer'] = serialize($FEDIT);
?>