<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;

$sql ="SELECT NumAlboAutotrasp, NumALboAutotraspProprio from user_aziende_trasportatori WHERE ID_AZT='".$_SESSION["FGE_IDs"]["user_aziende_trasportatori"]."' ";
include("SOGER_DirectProfilo.php");

$FEDIT->SDbRead($sql, "DbRecordSet");

$ContoProprio	= false;
$ContoTerzi		= false;

if(trim($FEDIT->DbRecordSet[0]["NumAlboAutotrasp"]!=""))			$ContoTerzi		=	true;
if(trim($FEDIT->DbRecordSet[0]["NumALboAutotraspProprio"]!=""))		$ContoProprio	=	true;

if($ContoProprio && $ContoTerzi)
	$case="case1";
if($ContoProprio && !$ContoTerzi)
	$case="case2";
if(!$ContoProprio && $ContoTerzi)
	$case="case3";
if(!$ContoProprio && !$ContoTerzi)
	$case="case4";

$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_automezzi");
$FEDIT->FGE_SetFormFields(array("ID_AUTHT","adr","description","PREV_description","ID_MZ_TRA","ID_ORIGINE_DATI","inherit_edits"),"user_automezzi");
if($case!='case4')
	$FEDIT->FGE_SetFormFields(array("ID_LIM"),"user_automezzi");
	
//$FEDIT->FGE_SetFormFields(array("BlackBox"),"user_automezzi");


$TableMezzi="user_automezzi"; //LKUP_LimitiMezzi.php

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {
	
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_MZ_TRA","user_automezzi");
	$FEDIT->FGE_LookUpDefault("ID_ORIGINE_DATI","user_automezzi");
	if($case!='case4')
		require("__mov_snipplets/LKUP_LimitiMezzi.php");
	#	
	$FEDIT->FGE_LookUpCFG("ID_AUTHT","user_automezzi");
	$FEDIT->FGE_UseTables(array("user_autorizzazioni_trasp","user_impianti_trasportatori","user_aziende_trasportatori","lov_cer","user_schede_rifiuti"));
	$FEDIT->FGE_SetSelectFields(array("num_aut","ID_RIF"),"user_autorizzazioni_trasp");
	$FEDIT->FGE_SetSelectFields(array("cod_pro","descrizione"),"user_schede_rifiuti");
	$FEDIT->FGE_HideFields(array("ID_RIF"),"user_autorizzazioni_trasp");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetSelectFields(array("ID_AZT"),"user_aziende_trasportatori");
	$FEDIT->FGE_SetFilter("ID_AZT",$_SESSION["FGE_IDs"]["user_aziende_trasportatori"],"user_aziende_trasportatori");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_SetOrder("lov_cer:COD_CER");
	$FEDIT->FGE_LookUpDone();
	#
	$FEDIT->FGE_HideFields(array("inherit_edits","PREV_description"),"user_automezzi");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea automezzo");
} else {
	if(isset($_GET["Involved"])) {
		$FEDIT->FGE_DisableFields(array("ID_AUTHT","description"),"user_automezzi");	
	}
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_MZ_TRA","user_automezzi");
	$FEDIT->FGE_LookUpDefault("ID_ORIGINE_DATI","user_automezzi");
	if($case!='case4')
		require("__mov_snipplets/LKUP_LimitiMezzi.php");
	#	
	$FEDIT->FGE_LookUpCFG("ID_AUTHT","user_automezzi");
	$FEDIT->FGE_UseTables(array("user_autorizzazioni_trasp","user_impianti_trasportatori","user_aziende_trasportatori","lov_cer","user_schede_rifiuti"));
	$FEDIT->FGE_SetSelectFields(array("num_aut","ID_RIF"),"user_autorizzazioni_trasp");
	$FEDIT->FGE_SetSelectFields(array("cod_pro","descrizione"),"user_schede_rifiuti");
	$FEDIT->FGE_HideFields(array("ID_RIF"),"user_autorizzazioni_trasp");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetSelectFields(array("ID_AZT"),"user_aziende_trasportatori");
	$FEDIT->FGE_SetFilter("ID_AZT",$_SESSION["FGE_IDs"]["user_aziende_trasportatori"],"user_aziende_trasportatori");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_SetOrder("lov_cer:COD_CER");
	$FEDIT->FGE_LookUpDone();
	#
	$FEDIT->FGE_HideFields(array("inherit_edits","PREV_description"),"user_automezzi");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva automezzo");
}
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>
