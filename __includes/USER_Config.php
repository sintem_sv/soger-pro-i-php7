<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;

$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("core_users");
$FEDIT->FGE_SetFormFields(array("ID_USR","ID_USR"),"core_users");

$FEDIT->FGE_SetFormFields(array("NMOVDEF","FRM_DefaultNFORM","NMOVSHOW","CER_AXT"),"core_users");
$YEAR = substr($FEDIT->DbServerData["db"], -4);
if($YEAR<2016){
    $FEDIT->FGE_SetFormFields(array("PRNT_CLASSIH"),"core_users");
    }
$FEDIT->FGE_SetFormFields(array("TargheOTF", "AutistaOTF"),"core_users");

$FEDIT->FGE_SetFormFields(array("FRM_DIS_PRO","FRM_DIS_DES","FRM_DIS_TRA","FRM_DIS_INT","FRM_DIS_QNT","FRM_PRINT_CER","OPRDsec","FRM_ADR","FRM_ADR_OLI","FRM_SET_HR","FRM_SET_DATE","via_diretta","FRM_DefaultVD","FRM_PRINT_CONTO_TERZI","FRM_PRNT_X","FRM_PRNT_Y","FRM_FONT_ID","FRM_FONT_SIZE_ID"),"core_users");


$FEDIT->FGE_SetFormFields(array("REG_DIS_PROD","REG_PRNT_X","REG_PRNT_Y","REG_NOMECER"), "core_users");
if($SOGER->UserData['core_impiantiMODULO_SIS']=='1')
	$FEDIT->FGE_SetFormFields(array("REG_IDSIS_SCHEDA", "REG_NUMERO_SCHEDA"), "core_users");

if($SOGER->UserData["workmode"]=="destinatario" || $SOGER->UserData["workmode"]=="trasportatore"){
	$pixel=836;	
	$FEDIT->FGE_SetFormFields(array("REG_RS_CARICHI","REG_PROD_NOTEC"),"core_users");
	}

$FEDIT->FGE_SetFormFields(array("ID_UMIS","ID_UMIS_INT","UMIS_RIF","REG_PS_DEST"),"core_users");

if($SOGER->UserData["workmode"]=="trasportatore"){
	$FEDIT->FGE_SetFormFields(array("reg_trasp_doublecheck"),"core_users");
	}

if($SOGER->UserData['core_impiantiMODULO_SIS']=='1')
	$FEDIT->FGE_SetFormFields(array("NOTER_SISTRI", "NOTEF_SISTRI"),"core_users");
	


#
$FEDIT->FGE_DescribeFields();

$FEDIT->FGE_LookUpDefault("FRM_FONT_ID","core_users");
$FEDIT->FGE_LookUpDefault("FRM_FONT_SIZE_ID","core_users");
#

if($SOGER->UserData["workmode"]!="intermediario"){
	$FEDIT->FGE_LookUpDefault("ID_UMIS","core_users");
	$FEDIT->FGE_HideFields(array("ID_UMIS_INT"),"core_users");
	}
else{
	$FEDIT->FGE_LookUpDefault("ID_UMIS_INT","core_users");
	$FEDIT->FGE_HideFields(array("ID_UMIS"),"core_users");
	}

$FEDIT->FGE_SetTitle("ck_AUT","Controlli","core_users");
$FEDIT->FGE_SetTitle("NMOVDEF","Impostazioni Movimenti","core_users");
$FEDIT->FGE_SetTitle("FRM_DIS_PRO","Stampa Formulario","core_users");
$FEDIT->FGE_SetTitle("REG_DIS_PROD","Stampa Registro","core_users");
$FEDIT->FGE_SetTitle("NOTER_SISTRI","SISTRI","core_users");

echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"Salva");

require_once("__includes/COMMON_sleepForgEdit.php");
?>