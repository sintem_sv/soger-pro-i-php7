<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
#
$FEDIT->FGE_UseTables(array("core_impianti","core_gruppi","core_intestatari"));
$FEDIT->FGE_SetSelectFields(array("description","indirizzo","ID_COM","telefono","fax","produttore","trasportatore","destinatario","intermediario"),"core_impianti");
$FEDIT->FGE_SetSelectFields(array("description"),"core_intestatari");
#
$FEDIT->FGE_DescribeFields();
$FEDIT->FGE_LookUpDefault("ID_COM","core_impianti");
#
echo($FEDIT->FGE_DataGrid($SOGER->AppDescriptiveLocation,"__scripts/FGE_DataGridEdit.php","EDC--"));
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>
