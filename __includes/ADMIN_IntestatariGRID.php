<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
#
$FEDIT->FGE_UseTables("core_intestatari");
$FEDIT->FGE_SetSelectFields(array("description","indirizzo","ID_COM","telefono","fax","email","piva","codfisc"),"core_intestatari");
#
$FEDIT->FGE_DescribeFields();
$FEDIT->FGE_LookUpDefault("ID_COM","core_intestatari");
#
echo($FEDIT->FGE_DataGrid($SOGER->AppDescriptiveLocation,"__scripts/FGE_DataGridEdit.php","EDC--"));
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>
