<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;

$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("core_users");
$FEDIT->FGE_SetFormFields(array("ID_USR","ID_USR"),"core_users");

$FEDIT->FGE_SetFormFields(array("ck_AUT","ck_AUTgg","ck_CNTR"),"core_users");
$FEDIT->FGE_SetFormFields(array("ck_ANALISI","ck_ANALISIgg"),"core_users");
$FEDIT->FGE_SetFormFields(array("ck_RIT_FORM","ck_RIT_FORMgg","ck_ADR","ck_validazione_conferimento"),"core_users");
if($SOGER->UserData['workmode']=='produttore' AND $SOGER->UserData['core_impiantiMODULO_SIS']==1)
	$FEDIT->FGE_SetFormFields(array("ck_MOV_SISTRI","ck_MOV_SISTRIgg"),"core_users");
$FEDIT->FGE_SetFormFields(array("ck_DEP_TEMP"),"core_users");

if($SOGER->UserData['core_usersID_GDEP']==1) // tempo
	$FEDIT->FGE_SetFormFields(array("ck_DEP_TEMPgg"),"core_users");
else	// volume
	$FEDIT->FGE_SetFormFields(array("q_limite_p","q_limite_t"),"core_users");


if($SOGER->UserData["core_usersR6"]=="0") {
	$FEDIT->FGE_DisableFields(array("ck_AUT","ck_AUTgg","ck_CNTR","ck_ANALISI","ck_ANALISIgg","ck_MOV_SISTRI","ck_MOV_SISTRIgg","ck_RIT_FORM","ck_RIT_FORMgg","ck_DEP_TEMP","ck_DEP_TEMPgg","q_limite_p","q_limite_t"),"core_users");
        }


#
$FEDIT->FGE_DescribeFields();


if($SOGER->UserData["workmode"]!="intermediario"){
	$FEDIT->FGE_LookUpDefault("ID_UMIS","core_users");
	$FEDIT->FGE_HideFields(array("ID_UMIS_INT"),"core_users");
	}
else{
	$FEDIT->FGE_LookUpDefault("ID_UMIS_INT","core_users");
	$FEDIT->FGE_HideFields(array("ID_UMIS"),"core_users");
	}



$FEDIT->FGE_SetTitle("ck_AUT","Fornitori","core_users");
$FEDIT->FGE_SetTitle("ck_ANALISI","Analisi di laboratorio","core_users");
$FEDIT->FGE_SetTitle("ck_RIT_FORM","Movimentazione","core_users");
if($SOGER->UserData['workmode']=='produttore')
	$FEDIT->FGE_SetTitle("ck_MOV_SISTRI","Movimentazione SISTRI","core_users");
$FEDIT->FGE_SetTitle("ck_DEP_TEMP","Deposito","core_users");

if($SOGER->UserData['core_usersID_GDEP']==1) // tempo
	$FEDIT->FGE_SetBreak("ck_DEP_TEMPgg","core_users");
else // volume
	$FEDIT->FGE_SetBreak("q_limite_p","core_users");

/*
#css firefox
$css="<style type=\"text/css\">\n";
$css.="div#btn_print_cfg{\n";
$css.="position: absolute; top: 459px; left: 70px; z-index: 10;";
$css.="}\n";
$css.="</style>\n";

#css ie
$css.="<!--[if IE]>";
$css.="<style type=\"text/css\">\n";
$css.="div#btn_print_cfg{\n";
$css.="position: absolute; top: 502px; left: 64px; z-index: 10;";
$css.="}\n";
$css.="</style>\n";
$css.="<![endif]-->";


echo $css;

$btn ="<div id=\"btn_print_cfg\">\n";
$btn.="<input type=\"button\" class=\"FGEbutton\" name=\"\" value=\"Stampa allarmi e controlli\" onclick=\"javascript:location.replace('__scripts/printCFG.php?filter=".$SOGER->UserData["core_usersID_USR"]."');\" />\n";
$btn.="</div>\n";	



echo $btn;
*/

echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"Salva");

#
require_once("__includes/COMMON_sleepForgEdit.php");
?>
