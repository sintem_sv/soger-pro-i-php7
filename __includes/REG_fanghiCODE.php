<?php
$Xcorr =  $SOGER->UserData["core_usersREG_PRNT_X"];
$Ycorr = $SOGER->UserData["core_usersREG_PRNT_Y"];

$RecsToShow			= $_GET['a']-$_GET['da'];
$RecsToShow++;

/*
echo "Inizio la stampa dal movimento FANGHI numero: ".$_GET['da']."<br />";
echo "Termino la stampa al movimento FANGHI numero: ".$_GET['a']."<br />";
echo "Devo stampare un totale di ".$RecsToShow." movimenti<br />";
echo "Sul database sono presenti ".$FEDIT->DbRecsNum." movimenti<br />";
*/
$LimitStart = $_GET['da']-1;
$LIMIT		= " LIMIT ".$LimitStart.", ".$RecsToShow." ";


//if($Xcorr>4) $Xcorr=4;
if($Ycorr>12) $Ycorr=12;

$sql  = " SELECT";
$sql .= " user_movimenti_fiscalizzati.ID_MOV_F,user_movimenti_fiscalizzati.TIPO";
$sql .= ",user_movimenti_fiscalizzati.FANGHI_BOLLA as bolla,user_movimenti_fiscalizzati.DTMOV as data,user_movimenti_fiscalizzati.quantita";
$sql .= ",lov_misure.ID_UMIS AS Umisura, user_schede_rifiuti.FANGHI_ss as ss, user_schede_rifiuti.CAR_NumDocumento as analisi";
$sql .= ",user_schede_rifiuti.CAR_Laboratorio as laboratorio, user_schede_rifiuti.CAR_DataAnalisi";
$sql .= ",user_aziende_produttori.description AS produttore_az,user_impianti_produttori.description AS produttore_imp";
$sql .= ",COMpro.description AS produttore_com_imp,COMpro.shdes_prov AS produttore_prov_imp";
$sql .= ",user_aziende_destinatari.description AS destinatario,user_impianti_destinatari.description AS destinatario_imp";
$sql .= ",COMdest.description AS destinatario_com_imp";

$sql .= " FROM user_movimenti_fiscalizzati"; 

$sql .= " JOIN user_schede_rifiuti on user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF";
$sql .= " JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS";
$sql .= " JOIN user_aziende_produttori ON user_movimenti_fiscalizzati.ID_AZP=user_aziende_produttori.ID_AZP";
$sql .= " JOIN user_impianti_produttori ON user_impianti_produttori.ID_UIMP=user_movimenti_fiscalizzati.ID_UIMP"; 
$sql .= " JOIN lov_comuni_istat AS COMpro ON user_impianti_produttori.ID_COM=COMpro.ID_COM";
$sql .= " LEFT JOIN user_aziende_destinatari ON user_aziende_destinatari.ID_AZD=user_movimenti_fiscalizzati.ID_AZD";
$sql .= " LEFT JOIN user_impianti_destinatari ON user_impianti_destinatari.ID_UIMD=user_movimenti_fiscalizzati.ID_UIMD"; 
$sql .= " LEFT JOIN lov_comuni_istat AS COMdest ON user_impianti_destinatari.ID_COM=COMdest.ID_COM";
$sql .= " WHERE user_movimenti_fiscalizzati.FANGHI=1 ";
$TableSpec = "user_movimenti_fiscalizzati.";
include("../__includes/SOGER_DirectProfilo.php");
#movimenti sono SEMPRE approved=1
$sql .= " AND user_movimenti_fiscalizzati.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "' AND NMOV<>9999999 ";
$sql .= " ORDER BY NMOV ASC";
$sql .= $LIMIT;
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

//print_r($FEDIT->DbRecordSet);



# in caso di stampa annua...
if($_GET['Fiscale']==1){
	for($c=0;$c<$FEDIT->DbRecsNum;$c++) {
		$sql = "UPDATE user_movimenti_fiscalizzati SET StampaAnnua = '1' WHERE ID_MOV_F = '".$FEDIT->DbRecordSet[$c]["ID_MOV"]."' ";
		$sql.= "AND user_movimenti_fiscalizzati.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
		$FEDIT->SDBWrite($sql,true,false);
		}
	}

$DcName = "Stampa_Registro_Fanghi";

if ($_GET["PaginaIniz"]=="1") {
	$PageNr			= 1;
	$CustomPageIniz = false;
	} 
else {
	$CustomPageIniz = true;
	$PageNr			= $_GET["PaginaIniz"]; 
	}


if($RecsToShow>$FEDIT->DbRecsNum)
	$RecsToShow=$FEDIT->DbRecsNum;

$PageRecs = 20;

$TotalPages = ceil($RecsToShow/$PageRecs);
//					print_r($TotalPages);

$i = $_GET['da']-1;

for($m=0; $m<$RecsToShow; $m++){
	
	$i++; // $i serve solo come contatore dei numeri di mov. che altrimenti partirebbero da 0

	// ogni $PageRecs record stampo intestazione pagina
	//if($m==0 | ((float)($m/$PageRecs) === (float)(int)($m/$PageRecs))){
	if($m==0 | $m%$PageRecs == 0){
		if($m>0)
			$FEDIT->FGE_PdfBuffer->AddPage();
		$Ypos=10;

		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',6);

		$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(190,5,"REGISTRO DI CARICO E SCARICO FANGHI","TRBL","C");

		$Ypos+=10;

		# colonna 1
		$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(10,10,"Progr.","TLB","C");

		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,10,"Data","TB","C");

		$FEDIT->FGE_PdfBuffer->SetXY(35,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,10,"N� bolla","TB","C");

		$FEDIT->FGE_PdfBuffer->SetXY(50,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,10,"Q.t� caricata","TBR","C");

		# colonna 2
		$FEDIT->FGE_PdfBuffer->SetXY(65,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(55,5,"Certificato di analisi","TLR","C");
		$Ypos+=5;

		$FEDIT->FGE_PdfBuffer->SetXY(65,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,5,"Numero","LB","C");

		$FEDIT->FGE_PdfBuffer->SetXY(80,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,5,"Data","B","C");

		$FEDIT->FGE_PdfBuffer->SetXY(95,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,5,"Laboratorio","B","C");

		$FEDIT->FGE_PdfBuffer->SetXY(110,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(10,5,"% S.S.","BR","C");
		$Ypos-=5;

		# colonna 3
		$FEDIT->FGE_PdfBuffer->SetXY(120,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(80,5,"Scarico","TLR","C");
		$Ypos+=5;

		$FEDIT->FGE_PdfBuffer->SetXY(120,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,5,"Q.t�","LB","C");

		$FEDIT->FGE_PdfBuffer->SetXY(135,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(35,5,"Destinatario","B","C");

		$FEDIT->FGE_PdfBuffer->SetXY(170,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(10,5,"App.","B","C");

		$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(20,5,"Comune","BR","C");

		$Ypos=30;			

		}

	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',6);

	# colonna 1
	$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(10,10,$i,"TLB","C");

	$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
	$DataMov=explode("-",$FEDIT->DbRecordSet[$m]['data']);
	$DataMov=$DataMov[2]."/".$DataMov[1]."/".$DataMov[0];
	$FEDIT->FGE_PdfBuffer->MultiCell(15,10,$DataMov,"TB","C");

	$FEDIT->FGE_PdfBuffer->SetXY(35,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,10,$FEDIT->DbRecordSet[$m]['bolla'],"TB","C");

	$FEDIT->FGE_PdfBuffer->SetXY(50,$Ypos);
	if($FEDIT->DbRecordSet[$m]['TIPO']=='C'){
		$qtaC=$FEDIT->DbRecordSet[$m]['quantita'];
		switch($FEDIT->DbRecordSet[$m]['Umisura']){
			case 1:
				$um="Kg.";
				break;
			case 2:
				$um="L.";
				break;
			case 3:
				$um="Mc.";
				break;
			}
		//$qtaC.=" ".$um;
		}
	else{
		$um		= "";
		$qtaC	= "";
		}
		
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,$qtaC,"LTBR","C");
	
	$Ypos+=5;
	$FEDIT->FGE_PdfBuffer->SetXY(50,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,$um,"LTBR","C");
	$Ypos-=5;

	# colonna 2
	$FEDIT->FGE_PdfBuffer->SetXY(65,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,10,$FEDIT->DbRecordSet[$m]['analisi'],"LB","C");

	$FEDIT->FGE_PdfBuffer->SetXY(80,$Ypos);
	$DataLab=explode("-",$FEDIT->DbRecordSet[$m]['CAR_DataAnalisi']);
	$DataLab=$DataLab[2]."/".$DataLab[1]."/".$DataLab[0];

	$FEDIT->FGE_PdfBuffer->MultiCell(15,10,$DataLab,"B","C");

	$FEDIT->FGE_PdfBuffer->SetXY(95,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,10,"","B","L");

	$FEDIT->FGE_PdfBuffer->SetXY(95,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,3.5,$FEDIT->DbRecordSet[$m]['laboratorio'],"","C");

	$FEDIT->FGE_PdfBuffer->SetXY(110,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(10,10,$FEDIT->DbRecordSet[$m]['ss'],"BR","C");

	# colonna 3
	$FEDIT->FGE_PdfBuffer->SetXY(120,$Ypos);
	if($FEDIT->DbRecordSet[$m]['TIPO']=='S'){
		$qtaS=$FEDIT->DbRecordSet[$m]['quantita'];
		switch($FEDIT->DbRecordSet[$m]['Umisura']){
			case 1:
				$um="Kg.";
				break;
			case 2:
				$um="L.";
				break;
			case 3:
				$um="Mc.";
				break;
			}
		//$qtaS.=" ".$um;
		}
	else{
		$um		= "";
		$qtaS	= "";
		}

	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,$qtaS,"LTBR","C");

	$Ypos+=5;
	$FEDIT->FGE_PdfBuffer->SetXY(120,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(15,5,$um,"LTBR","C");
	$Ypos-=5;

	$FEDIT->FGE_PdfBuffer->SetXY(135,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(35,10,"","BR","L");
	
	$FEDIT->FGE_PdfBuffer->SetXY(135,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(35,3.5,$FEDIT->DbRecordSet[$m]['destinatario'],"","L");

	$FEDIT->FGE_PdfBuffer->SetXY(170,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,10,"","LBR","L");

	$FEDIT->FGE_PdfBuffer->SetXY(170,$Ypos);
	$FEDIT->FGE_PdfBuffer->MultiCell(30,3.5,$FEDIT->DbRecordSet[$m]['destinatario_imp']." ".$FEDIT->DbRecordSet[$m]['destinatario_com_imp'],"","L");

	$Ypos+=10;

	}

?>