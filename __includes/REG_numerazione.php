<div class="divName"><span>GESTIONE REGISTRI</span></div>



<form name="StampaRegistroPerVidimazione" method="get" action="" style="width: 90%">
<fieldset class="FGEfieldset">
<legend class="FGElegend" style="font-size:x-small;">Numerazione Registro per Vidimazione</legend>

<table style="width:100%;">

<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="paginaTot" class="">Numero di pagine totali</label></td>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><input type="text" size="4" value="1" name="paginaTot" id="paginaTot" class="FGEinput" tabindex="70"/></td>
</tr>

<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="paginaIniz" class="">Numero di pagina iniziale</label></td>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><input type="text" size="4" value="1" name="paginaIniz" id="paginaIniz" class="FGEinput" tabindex="71"/></td>
</tr>

<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="RagSoc" class="">Stampa ragione sociale</label></td>
	<td class="FGEDataGridTableCellLeft" align="center"><input type="checkbox" value="1" name="RagSoc" id="RagSoc" class="FGEinput" tabindex="72"/></td>
</tr>

<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="AnnoRegistro" class="">Anno di riferimento</label></td>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><input type="text" size="4" value="<?php echo date('Y'); ?>" name="AnnoRegistro" id="AnnoRegistro" class="FGEinput" tabindex="73"/></td>
</tr>

<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="CodiceRegistro" class="">Codice identificativo registro</label></td>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><input type="text" size="10" value="" name="CodiceRegistro" id="CodiceRegistro" class="FGEinput" tabindex="74"/></td>
</tr>

</table>


<br/><br/>
<input type="button" class="FGEbutton" tabindex="73" value="stampa numerazione" onclick="javascript:RegistroVidimazione(this.form);"/>
</fieldset>
</form>