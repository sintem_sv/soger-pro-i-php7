<div style="position: absolute; left: 700px; top: 10px;">
<input type="button" name="nuovo" class="FGEbutton" value="nuovo movimento" onclick="javascript: document.location='__scripts/status.php?area=UserNuovoMovInter'"/>
<?php
if(isset($_GET['StampaAnnua'])){ # movimento presente su stampa annuale registro, non pi� editabili alcuni campi
	$_SESSION["StampaAnnua"] = $_GET["StampaAnnua"];
	}

?>
</div>
<?php
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
global $SOGER;

$_SESSION['LKUP_IDRIF_CHANGED'] = false;

if(isset($_GET['table'])){
	$TableName=$_GET['table'];
        $sql = "SELECT ID_RIF FROM ".$TableName." WHERE ".$_GET['pri']."=".$_GET['filter'].";";
	$FEDIT->SDBRead($sql,"DbRecordSetPerLkpFilter",true,false);
	$_SESSION['LKUP_IDRIF'] = $FEDIT->DbRecordSetPerLkpFilter[0]['ID_RIF'];
}
else{
	if($SOGER->UserData['core_impiantiREG_IND']=='1')
		$TableName="user_movimenti";
	else
		$TableName="user_movimenti_fiscalizzati";
        $_SESSION['LKUP_IDRIF'] = 0;
}

#
#	RICAVA IL NUMERO MOVIMENTO
#
if(!isset($_GET["table"]) && !isset($_GET["pri"]) && !isset($_GET["filter"])) {
	
	require("__mov_snipplets/FRM_NumeroMOVIMENTO.php");
        if($SOGER->UserData["core_usersFRM_DefaultNFORM"]==1)
            require("__mov_snipplets/FRM_NumeroFormulario.php");
}	
#
#	CAMPI
#		
//{{{ 
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_movimenti_fiscalizzati");
$FEDIT->FGE_SetFormFields(array("ID_IMP","TIPO","NFORM","DTFORM","NMOV","DTMOV"),"user_movimenti_fiscalizzati");
$FEDIT->FGE_SetFormFields(array("ID_RIF","FKESF","FKEumis","FKEdisponibilita","VER_DESTINO","FKEpesospecifico"),"user_movimenti_fiscalizzati");
$FEDIT->FGE_SetFormFields(array("quantita","qta_hidden","tara","pesoL","pesoN","FKEgiacINI","FKElastCarico","FKErifCarico","adr","QL","percorso"),"user_movimenti_fiscalizzati");
$FEDIT->FGE_SetFormFields(array("ID_AZP","FKEcfiscP","ID_UIMP"),"user_movimenti_fiscalizzati");
$FEDIT->FGE_SetFormFields(array("ID_AZD","FKEcfiscD","ID_UIMD","ID_AUTHD","FKErilascioD","FKEscadenzaD","ID_OP_RS","ID_TRAT"),"user_movimenti_fiscalizzati");
$FEDIT->FGE_SetFormFields(array("ID_AZT","FKEcfiscT","ID_UIMT","ID_AUTHT","FKErilascioT","FKEscadenzaT"),"user_movimenti_fiscalizzati");
$FEDIT->FGE_SetFormFields("ID_AZI","user_movimenti_fiscalizzati");

$FEDIT->FGE_SetFormFields(array("NPER","FANGHI"),"user_movimenti_fiscalizzati");
$FEDIT->FGE_SetFormFields(array("StampaAnnua"),"user_movimenti_fiscalizzati");

#
require("__mov_snipplets/FRM_Inter_DisHide.php");
#

$imageRif = '<div id="imageRif" class="imageRif">&nbsp;&nbsp;</div>';
$rosetteD = '<div id="rosetteD" class="rosette">&nbsp;&nbsp;</div>';
$rosetteT = '<div id="rosetteT" class="rosette">&nbsp;&nbsp;</div>';

$FEDIT->FGE_SetTitle("ID_AZP","Produttore","user_movimenti_fiscalizzati");
$FEDIT->FGE_SetTitle("ID_RIF",$imageRif."Rifiuto","user_movimenti_fiscalizzati");
$FEDIT->FGE_SetTitle("ID_AZD",$rosetteD."Destinatario","user_movimenti_fiscalizzati");
$FEDIT->FGE_SetTitle("ID_AZT",$rosetteT."Trasportatore","user_movimenti_fiscalizzati");
$FEDIT->FGE_SetBreak("quantita","user_movimenti_fiscalizzati");
#
$ProfiloFields = "user_movimenti_fiscalizzati";
include("SOGER_CampiProfilo.php");
$FEDIT->FGE_DescribeFields(); 

#
#	LOOKUP
#

$autoTRADEST = true;
require("__mov_snipplets/LKUP_Rifiuto.php");
require("__mov_snipplets/LKUP_Produttori.php");
require("__mov_snipplets/LKUP_ProduttoriImp.php");
require("__mov_snipplets/LKUP_Destinatari.php");      
require("__mov_snipplets/LKUP_DestinatariImp.php");
require("__mov_snipplets/LKUP_DestinatariAuth.php");
require("__mov_snipplets/LKUP_Trasportatori.php");
require("__mov_snipplets/LKUP_TrasportatoriImp.php");
require("__mov_snipplets/LKUP_TrasportatoriAuth.php");
require("__mov_snipplets/LKUP_OpRS.php");
require("__mov_snipplets/LKUP_Trattamenti.php");

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {
	#
	#	FILTRI di DEFAULT 
	#
	require("__mov_snipplets/FLTR_MovimentoDefault.php");

	#
	#	VALORI di DEFAULT
	#
	$FEDIT->FGE_SetValue("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_movimenti_fiscalizzati");
	$FEDIT->FGE_SetValue("TIPO","I","user_movimenti_fiscalizzati");
	$FEDIT->FGE_SetValue("NFORM",$NumForm,"user_movimenti_fiscalizzati");
	$FEDIT->FGE_SetValue("NMOV",$NumMov,"user_movimenti_fiscalizzati");
	$FEDIT->FGE_SetValue("quantita",0,"user_movimenti_fiscalizzati");
	$FEDIT->FGE_SetValue("qta_hidden",0,"user_movimenti_fiscalizzati");
	$FEDIT->FGE_SetValue("pesoN",0,"user_movimenti_fiscalizzati");
	$FEDIT->FGE_SetValue("tara",0,"user_movimenti_fiscalizzati");
	$FEDIT->FGE_SetValue("pesoL",0,"user_movimenti_fiscalizzati");
	$FEDIT->FGE_SetValue("NPER",0,"user_movimenti_fiscalizzati");
	$FEDIT->FGE_SetValue("adr",0,"user_movimenti_fiscalizzati");
	$FEDIT->FGE_SetValue("QL",0,"user_movimenti_fiscalizzati");
	$FEDIT->FGE_SetValue("DTMOV",date("m/d/Y"),"user_movimenti_fiscalizzati");
	$FEDIT->FGE_SetValue("DTFORM",date("m/d/Y"),"user_movimenti_fiscalizzati");

	#
	#	CONSIDERIAMO LA PRESENZA DI UN UNICO INTERMEDIARIO ( EVENTUALMENTE EDITO DA DETTAGLIO )
	#
	$sql ="SELECT user_aziende_intermediari.ID_AZI ";
	$sql.="FROM user_aziende_intermediari ";
	$sql.="WHERE user_aziende_intermediari.ID_IMP='".$SOGER->UserData["core_impiantiID_IMP"]."' AND ";
	$sql.="user_aziende_intermediari.intermediario=1 ";
	$sql.="LIMIT 0,1 ";
	$FEDIT->SdbRead($sql,"DbRecordSetInt",true,false);
	$ID_AZI=$FEDIT->DbRecordSetInt[0]["ID_AZI"];
	$FEDIT->FGE_SetValue("ID_AZI",$ID_AZI,"user_movimenti_fiscalizzati");



	if($SOGER->UserData["core_usersFRM_DefaultVD"]=="1") {
		$FEDIT->FGE_SetValue("VER_DESTINO",1,"user_movimenti_fiscalizzati");
	} else {
		$FEDIT->FGE_SetValue("VER_DESTINO",0,"user_movimenti_fiscalizzati");
	}
	#
	if($SOGER->UserData["core_usersvia_diretta"]==1) {
		$FEDIT->FGE_SetValue("percorso","VIA DIRETTA","user_movimenti_fiscalizzati");		
	}
	#
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"salva movimento");
	
} else {

	## COCCARDA DESTINATARIO
	$sql="SELECT ID_ORIGINE_DATI FROM user_autorizzazioni_dest JOIN user_movimenti_fiscalizzati ON user_autorizzazioni_dest.ID_AUTHD=user_movimenti_fiscalizzati.ID_AUTHD WHERE ID_MOV_F='".$ref."';";
	$FEDIT->SDBRead($sql,"DbRecordSetRosetteD",true,false);
	$ID_ORIGINE_DATI_D="'".$FEDIT->DbRecordSetRosetteD[0]['ID_ORIGINE_DATI']."'";

	## COCCARDA TRASPORTATORE
	$sql="SELECT ID_ORIGINE_DATI FROM user_autorizzazioni_trasp JOIN user_movimenti_fiscalizzati ON user_autorizzazioni_trasp.ID_AUTHT=user_movimenti_fiscalizzati.ID_AUTHT WHERE ID_MOV_F='".$ref."';";
	$FEDIT->SDBRead($sql,"DbRecordSetRosetteT",true,false);
	$ID_ORIGINE_DATI_T="'".$FEDIT->DbRecordSetRosetteT[0]['ID_ORIGINE_DATI']."'";

	if(isset($_SESSION["MovimentoEdit_aPosteriori"])) {
		$_SESSION["MovimentoEdit_aPosteriori"] = true;
		$FEDIT->FGE_DisableFields(array("ID_RIF","FKESF","FKEumis","FKEpesospecifico"),"user_movimenti_fiscalizzati");
		}

	if(isset($_SESSION['StampaAnnua']) && (@$_SESSION['StampaAnnua']==1)){ # movimento presente su stampa annuale registro
		##GENERALE
		#rifiuto#
		$FEDIT->FGE_DisableFields(array("DTMOV","DTFORM","NFORM","NMOV"),"user_movimenti_fiscalizzati");
		$FEDIT->FGE_DisableFields(array("quantita","tara","pesoL","pesoN"),"user_movimenti_fiscalizzati");
		#produttore#
		$FEDIT->FGE_DisableFields(array("ID_AZP","ID_UIMP"),"user_movimenti_fiscalizzati");
		#destinatario#
		$FEDIT->FGE_DisableFields("ID_OP_RS","user_movimenti_fiscalizzati");
		}

	$_SESSION["IDMovimento"] = $_GET["filter"];
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva movimento");
	require("__includes/USER_FormularioFiscaleSiNo.inc");
	$RIF_REF = $FEDIT->DbRecordSet[0]["ID_RIF"];	
	
	## IMMAGINE RIFIUTO
	$sql="SELECT pericoloso, adr FROM user_schede_rifiuti WHERE ID_RIF='".$FEDIT->DbRecordSet[0]["ID_RIF"]."'";
	$FEDIT->SDBRead($sql,"DbRecordSetImageRif",true,false);
	$ADR			= "'".$FEDIT->DbRecordSetImageRif[0]['adr']."'";
	$PERICOLOSO		= "'".$FEDIT->DbRecordSetImageRif[0]['pericoloso']."'";
	$TableName		= "user_movimenti_fiscalizzati";
	$LegamiSISTRI	= false;
	require("__scripts/MovimentiRifMovCarico.php");
}


#
require_once("__includes/COMMON_sleepForgEdit.php");
?>

<?php
if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {
?>
	<script type="text/javascript">
	//CUSTOM_CREATE_FORM();
	document.getElementById('user_movimenti_fiscalizzati:NFORM').style.background = 'yellow';
	document.getElementById('user_movimenti_fiscalizzati:NFORM').style.color = 'black';
	</script>
<?php

}else{

$js ="<script type=\"text/javascript\">\n";
$js.= <<< ENDJS
CUSTOM_ROSETTE('rosetteD',$ID_ORIGINE_DATI_D);
CUSTOM_ROSETTE('rosetteT',$ID_ORIGINE_DATI_T);
CUSTOM_IMAGE_RIF($PERICOLOSO,$ADR);
ENDJS;
$js.= "</script>";
echo $js;

} ?>