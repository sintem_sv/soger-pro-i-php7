<?php
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
global $SOGER;
global $SIS;

#
#		SEQUENZA DI BOOT
#
require("__config/BOOT__handler.php");


#########################################################################
#
#		JAVASCRIPT
#
#########################################################################


/*
Errore
-----------------------------
Il campo 'login' non deve essere vuoto
Il campo 'password' non deve essere vuoto
*/

$js ="<script type='text/javascript'>";
$js.= <<<JSTICKET

function KeyCheck(e) {
	var KeyID = (window.event) ? event.keyCode : e.keyCode;
	switch(KeyID) {
		case 13:
			FGEForm_1_SendCheck(document.FGEForm_1);
		break;
		}
	}

document.onkeypress = KeyCheck;

function FGEForm_1_SendCheck(FRef) {
	
	if (!FGE_AxajDoneCheck() | !SOGER_AutFormLock()) {
		return;
		}

	formRef = FRef
	
	if (document.FGEForm_1.CheckLicenza.checked){
		FGE_MandatoryTXTFields('login',formRef['core_users:usr'].value,'core_users:usr');
		FGE_MandatoryTXTFields('password',formRef['core_users:pwd'].value,'core_users:pwd');
		FGE_SendForm();
		}
	else{
		window.alert("Per utilizzare il software e' necessario accettarne la Licenza d'uso.");
		}
	
	}

function goTicket(){
	var printError=false;
	var errore='Errore \\n -----------------------------';
	if(document.FGEForm_1['core_users:usr'].value==''){
		printError=true;
		errore+=' \\n Il campo \'login\' non deve essere vuoto';
		}
	if(document.FGEForm_1['core_users:pwd'].value==''){
		printError=true;
		errore+=' \\n Il campo \'password\' non deve essere vuoto';
		}
	if(printError)
		window.alert(errore);
	else{
		document.getElementById("loginform-username").value=document.FGEForm_1['core_users:usr'].value;
		document.getElementById("loginform-password").value=document.FGEForm_1['core_users:pwd'].value;
		document.login.submit();
		}
	}

JSTICKET;
$js.="</script>";
echo $js;


#########################################################################
#
#		HTML
#
#########################################################################


$login.= <<<HTML

<div id="Wait" class="WaitDiv"><img src="__css/FGE_load.png" alt="attendere" style="margin: 3px;" align="absmiddle"> <b>attendere</b>....</div>

<div id="FGEblock">

    <table cellpadding="0" cellspacing="0" style="background-color:#8FA2B3; margin-top:15px;">

	<tr>
		<td style="text-align:center;background-color:#034373;padding-top:15px;padding-bottom:15px;width:500px">
			<img src="__css/header_img2011.png" />
		</td>
		<td style="background-color:#5C7D99;width:100px;">&nbsp;</td>
		<td style="text-align:center;background-color:#1C1C1C;padding-top:15px;padding-bottom:15px;width:500px">
			<img src="__css/logoStep.png" />
		</td>
	</tr>

	<tr>
		<td>
			<div style="margin-top:20px;padding:5px;">
				<form name="FGEForm_1" id="FGEForm_1" method="post" action="__scripts/login.php" style="padding:0;margin:0;">
						
					<input id="core_users:ID_USR" name="core_users:ID_USR" value="" type="hidden">
					<input name="FGE_PrimaryKeyRef" value="core_users:ID_USR" type="hidden">

					<table cellpadding="0" cellspacing="0" style="padding-left:30px;">
						<tr>
							<td>
								<div id="FGED_core_users:usr" class="FGE1Col">
									<label for="core_users:usr" class="FGEmandatory">login</label><br>
									<input class="FGEinput" id="core_users:usr" name="core_users:usr" tabindex="50" size="15" maxlength="15" value="" onfocus="this.style.background='yellow';" onblur="this.style.background='white';" type="text">
								</div>
							</td>
							<td>
								<div id="FGED_core_users:pwd" class="FGE1Col">
									<label for="core_users:pwd" class="FGEmandatory">password</label><br>
									<input id="core_users:pwd" name="core_users:pwd" tabindex="51" size="15" maxlength="15" value="" class="FGEinput" onfocus="this.style.background='yellow';" onblur="this.style.background='white';" type="password">
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="height:20px;">		
								<input type="checkbox" name="CheckLicenza" value="" checked />
								<span style="font-size: small;">Ho letto e accetto la <a href="./Licenza.pdf" target="_blank">Licenza</a> d'uso del software.</span>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="height:20px;">		
								<div class="FGESubmitRow">
									<input id="SubmitButton" class="FGEbutton" tabindex="52" value="Accedi al programma So.Ge.R. PRO" onclick="FGEForm_1_SendCheck(this.form)" onfocus="this.style.background='yellow';this.style.color='#034373';" onblur="this.style.background='#034373';this.style.color='white';" type="button" style="width:288px;">
								</div>
							</td>
						</tr>
					</table>

				</form>
			</div>
		</td>

		<td style="background-color:#5C7D99;">&nbsp;</td>

		<td style="background-color:#fff;">
			<div style="margin-top:20px;padding:5px;">
				<form name="FGEForm_2" id="FGEForm_2" method="post" action="http://195.88.7.213/step/index.php/site/login" style="padding:0;margin:0;">
						
					<table cellpadding="0" cellspacing="0" style="padding-left:30px;">
						<tr>
							<td>
								<div id="FGED_LoginForm[username]" class="FGE1Col">
									<label for="LoginForm[username]" class="FGEmandatory" style="background-color:#1C1C1C;color:#fff;">login</label><br>
										<input class="FGEinput" id="LoginForm_username" name="LoginForm[username]" tabindex="53" size="15" maxlength="15" value="" onfocus="this.style.background='yellow';" onblur="this.style.background='white';" type="text">
								</div>
							</td>
							<td>
								<div id="FGED_LoginForm[password]" class="FGE1Col">
									<label for="LoginForm[password]" class="FGEmandatory" style="background-color:#1C1C1C;color:#fff;">password</label><br>
										<input id="LoginForm_password" name="LoginForm[password]" tabindex="54" size="15" maxlength="15" value="" class="FGEinput" onfocus="this.style.background='yellow';" onblur="this.style.background='white';" type="password">
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="height:20px;">		
								&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan="2" style="height:20px;">		
								<div class="FGESubmitRow">
									<input id="SubmitButton" class="FGEbutton" tabindex="55" value="Accedi al programma STEP" onfocus="this.style.background='#1C1C1C';this.style.color='#fff';" onblur="this.style.background='#1C1C1C';this.style.color='white';" type="submit" style="background-color:#1C1C1C; width:288px;">
								</div>
							</td>
						</tr>
					</table>

				</form>
			</div>
		</td>

	</tr>

	<tr>
		<td valign="top" style="padding-left:30px;">
			<div id="ticket" style="padding:5px;margin-bottom:20px;">
				<form name="login" action="http://www.sogerpro.it/index.php/site/login" method="POST" target="_blank" style="padding-top:0;margin:0;">
				
					<input type="button" class="FGEbuttonTicket" name="" onclick="goTicket();" />
					<input type="hidden" id="loginform-username" name="LoginForm[username]" value="" />
					<input type="hidden" id="loginform-password" name="LoginForm[password]" value="" />
					<input type="hidden" value="0" name="LoginForm[rememberMe]">
					<input type="hidden" value="joswatsticket/index" name="redirection" />

				</form>
			</div>
		</td>
		
		<td style="background-color:#5C7D99;">&nbsp;</td>
                    
        <td style="background-color:#fff;">&nbsp;</td>
	</tr>
        
	<tr>
		<td colspan="3" style="background-color:#5C7D99;">
			<div style="width:1100px;padding-top:7px; padding-left:26px; margin-top:10px;border:2px solid #fff; height:21px;background-color:#fff; background-image: url('__css/information.png');background-repeat:no-repeat;background-position: 5px 5px;">
			Per ogni problematica non relativa all'assistenza tecnica del programma Soger Pro quale consulenza normativa, caratterizzazioni e classificazioni, ADR ecc potete contattarci all'indirizzo di posta <a href="mailto:assistenza@sintem.it">assistenza@sintem.it</a>.
			</div>
		</td>
	</tr>

	<tr>
		<td colspan="3" style="background-color:#5C7D99;">
			<div style="width:1100px;padding-left:26px; height:10px;background-color:#5C7D99;"></div>
		</td>
	</tr>

	<tr>
		<td style="text-align:center;background-color:#034373;padding-top:15px;padding-bottom:15px;width:500px">
			<img src="__css/logo_soger3.png" />
		</td>
		<td colspan="2" style="background-color:#5C7D99;width:100px;">&nbsp;</td>
	</tr>

	<tr>
		<td>
			<div class="FGESubmitRow" style="text-align:center;">
				<input class="FGEbutton" tabindex="56" value="Accedi al programma So.Ge.R. PRO 3.0" onclick="location.href = 'https://www.soger2018.it'" onfocus="this.style.background='yellow';this.style.color='#034373';" onblur="this.style.background='#034373';this.style.color='white';" type="button" style="width:288px;margin-top:50px;margin-bottom:50px;padding:15px;">
			</div>

			<div style="width:500px;padding-top:7px; padding-left:26px; margin-top:10px;border:2px solid #fff; height:30px;background-color:#fff; background-image: url('__css/information.png');background-repeat:no-repeat;background-position: 5px 5px;">
				L'accesso a SOGER PRO 3.0 � dedicato ai Clienti che ne hanno gi� richiesto l'attivazione.<br />E' possibile accedere per uso dimostrativo.
			</div>


		</td>
		<td colspan="2" style="background-color:#5C7D99;">&nbsp;</td>
	</tr>

    </table>
        
</div>
HTML;

echo $login;

#########################################################################
#
#		CSS
#
#########################################################################



$css="<style type=\"text/css\">\n";
$css.="div#Header{display:none;}\n";
$css.="</style>\n";
echo $css;


#
require_once("__includes/COMMON_sleepForgEdit.php");
require_once("__includes/COMMON_sleepSoger.php");
//require_once("__includes/COMMON_sleepSIS.php");
?>