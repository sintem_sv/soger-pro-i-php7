<?php 
global $SOGER;

$newRecordImg = 'BT_userMovNuovoScarico.gif';
$newRecordTxt = 'Nuovo scarico';
if(stripos($this->AppDescriptiveLocation, 'interno') !== false){
	$newRecordImg = 'BT_userMovNuovoScaricoInterno.gif';
	$newRecordTxt = 'Nuovo scarico interno';
	}
elseif(stripos($this->AppDescriptiveLocation, 'scarico') !== false){
	$newRecordImg = 'BT_userMovNuovoScarico.gif';
	$newRecordTxt = 'Nuovo scarico';
	}
elseif(stripos($this->AppDescriptiveLocation, 'formulario') !== false){
	$newRecordImg = 'BT_userMovNuovoFormulario.gif';
	$newRecordTxt = 'Nuovo formulario';
	}
elseif(stripos($this->AppDescriptiveLocation, 'scheda') !== false){
	$newRecordImg = 'BT_userMovNuovaSchedaSistri.gif';
	$newRecordTxt = 'Nuova Scheda Sistri';
	}


if(isset($_GET['table']))
	$TableName=$_GET['table'];
else{
	if($SOGER->UserData['core_impiantiREG_IND']=='1')
		$TableName="user_movimenti";
	else
		$TableName="user_movimenti_fiscalizzati";
	}

if($SOGER->UserData['core_impiantiREG_IND']=='0')
	$AppBackStatus="UserNuovoMovScarico";
else{
	if($TableName=="user_movimenti"){
		$AppBackStatus="UserNuovoMovScarico";
		}
	else{
		$AppBackStatus="UserNuovoMovScaricoF";
		}
	}

?>


<a href="#" title="Generale"><img src="__css/BT_userMovCaricoGen_anti90-ON.gif" class="NoBorder" alt="Generale" /></a>

<?php if($SOGER->UserData['workmode']=='produttore' OR $SOGER->UserData['core_usersFRM_LAYOUT_DEST']==0){ ?>
<a href="javascript:GoDetail();" title="Dati trasporto"><img src="__css/BT_userMovCaricoDet_anti90-OFF.gif" class="NoBorder" alt="Dati trasporto" /></a>
<?php } ?>

<?php 
if($SOGER->UserData["core_usersG3"] == "1" && $_GET['table']=="user_movimenti_fiscalizzati") {
?>
<a href="javascript:GoCosti();" title="Costi"><img src="__css/BT_costi-OFF.gif" class="NoBorder" alt="Costi" /></a>
<?php
}
?>

<?php 
if($SOGER->UserData['core_impiantiREG_IND']=='0') 
	$Area="UserNuovoMovScaricoF";
else{
	$Area="UserNuovoMovScarico";
	}
?>


<?php
	//if(isset($_GET["table"]) && isset($_GET["pri"]) && isset($_GET["filter"]) && @$_GET['Fiscale']==0 ) {
		if(isset($_GET["table"]) && isset($_GET["pri"]) && isset($_GET["filter"])) {
		$hash = MakeUrlHash($_GET["table"],$_GET["pri"],$_GET["filter"]);
		$ref = $_GET["filter"];
		if($_GET['table']=="user_movimenti")
			$url = "__scripts/FGE_DataGridEdit.php?table=user_movimenti&pri=ID_MOV&filter=$ref&hash=$hash&FGE_action=print&Fiscale=0";
		else
			$url = "__scripts/FGE_DataGridEdit.php?table=user_movimenti_fiscalizzati&pri=ID_MOV_F&filter=$ref&hash=$hash&FGE_action=print&Fiscale=0";
		
?>

<?php if($SOGER->UserData['workmode']=='produttore' OR $SOGER->UserData['core_usersFRM_LAYOUT_DEST']==0){ ?>
<a href="<?php echo $url; ?>" title="Stampa formulario"><img src="__css/BT_userMovStampaFormulario.gif" class="NoBorder" alt="Stampa formulario" /></a>
<?php } ?>


<!-- ALLEGATO VII -->
<?php if($SOGER->UserData['workmode']=='produttore' OR $SOGER->UserData['core_usersFRM_LAYOUT_DEST']==0){ ?>
<a href="javascript:GoAnnexVII();" title="Allegato VII"><img src="__css/BT_userMovAllegatoVII-OFF.gif" class="NoBorder" alt="Allegato VII" /></a>
<?php } ?>





<?php if($SOGER->UserData['core_impiantiREG_IND']=='0' | ($SOGER->UserData['core_impiantiREG_IND']=='1' && $TableName=="user_movimenti")){ ?>
	<a href="__scripts/status.php?area=<?php echo $Area; ?>" title="<?php echo $newRecordTxt; ?>"><img src="__css/<?php echo $newRecordImg; ?>" class="NoBorder" alt="<?php echo $newRecordTxt; ?>" /></a>
<?php } ?>



<?php } else{ ?>
	<a href="__scripts/status.php?area=<?php echo $Area; ?>" title="<?php echo $newRecordTxt; ?>"><img src="__css/<?php echo $newRecordImg; ?>" class="NoBorder" alt="<?php echo $newRecordTxt; ?>" /></a>
<?php } ?>




<script type="text/javascript">
function GoDetail() {
	if(confirm('Salvare i dati e passare alla scheda dettaglio?')) {
		document.forms[0]["redirection"].value = "statusCUSTOM_MovScaricoDettaglio.php";
		FGEForm_1_SendCheck(document.forms[0]);
	}
}
function GoAnnexVII() {
	if(confirm('Salvare i dati e passare alla compilazione dell\'Allegato VII?')) {
		document.forms[0]["redirection"].value = "statusCUSTOM_MovScaricoAllegatoVII.php";
		FGEForm_1_SendCheck(document.forms[0]);
	}
}
function GoCosti() {
	if(confirm('Salvare i dati e passare alla scheda costi?')) {
		document.forms[0]["redirection"].value = "statusCUSTOM_MovScaricoCosti.php";
		FGEForm_1_SendCheck(document.forms[0]);
	}
}

</script>