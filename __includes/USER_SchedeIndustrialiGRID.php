<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;

include("USER_MovFlushSessionVars.php");
require("__includes/USER_FormularioFiscaleSiNo.inc");
#
$FEDIT->FGE_FlushTableInfo();
#
$FEDIT->FGE_UseTables("user_movimenti","user_schede_rifiuti","lov_cer");
$FEDIT->FGE_SetSelectFields(array("FISCALIZZATO","ID_BAT","NFORM","DTFORM","ID_RIF","quantita","pesoN","FKEumis", "PS_DESTINO","FISCALE","StampaAnnua","ID_IMP","produttore","destinatario","trasportatore"),"user_movimenti");
$FEDIT->FGE_SetSelectFields(array("NMOV","TIPO","TIPO_S_SCHEDA","DTMOV","VER_DESTINO","DT_FORM","ID_OP_RS","ID_AUTST","ID_AUTO","ID_UIMP"),"user_movimenti");
$FEDIT->FGE_SetSelectFields(array("ID_AZP","ID_AZT","ID_AZD","ID_AZI"),"user_movimenti");
$FEDIT->FGE_SetSelectFields(array("descrizione","peso_spec"),"user_schede_rifiuti");
$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
$FEDIT->FGE_DescribeFields();
$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_movimenti");
$FEDIT->FGE_SetFilter("TIPO","S","user_movimenti");
$ProfiloFields = "user_movimenti";
include("SOGER_FiltriProfilo.php");
#
$FEDIT->FGE_HideFields(array("FISCALE","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_movimenti");
#
echo($FEDIT->SG_SchedeIndustrialiDataGrid($SOGER->AppDescriptiveLocation, "user_movimenti", "industriale"));
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>