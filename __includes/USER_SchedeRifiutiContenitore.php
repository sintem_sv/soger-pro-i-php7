<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;

function giornilavorativi($dataInizio,$dataFine)
{
    // Calcolo del giorno di Pasqua fino all'ultimo anno valido
    for ($i=2006; $i<=2037; $i++) {
    $pasqua = date("Y-m-d", easter_date($i));
    $array_pasqua[] = $pasqua;
    }

    // Calcolo le rispettive pasquette
    foreach($array_pasqua as $pasqua){
    list ($anno,$mese,$giorno) = explode("-",$pasqua);
    $pasquetta = mktime (0,0,0,date($mese),date($giorno)+1,date($anno));
    $array_pasquetta[] = $pasquetta;
    }
     
    // questi giorni son sempre festivi a prescindere dall'anno
    $giorniFestivi = array("01-01",
                           "01-06",
                           "04-25",
                           "05-01",
                           "06-02",
                           "08-15",
                           "11-01",
                           "12-08",
                           "12-25",
                           "12-26",
                           );

        $lavorativi = 0;

        for($i = strtotime($dataInizio); $i<=strtotime($dataFine); $i = strtotime("+1 day",$i))
        {
           $giorno_data = date("w",$i); //verifico il giorno: da 0 (dom) a 6 (sab)
           $mese_giorno = date('m-d',$i); // confronto con gg sempre festivi
             // Infine verifico che il giorno non sia sabato,domenica,festivo fisso o festivo variabile (pasquetta);         
            if ($giorno_data !=0 && $giorno_data != 6 && !in_array($mese_giorno,$giorniFestivi) && !in_array($i,$array_pasquetta) )
            {
            $lavorativi++;
            }

        }

return $lavorativi;
} 

$GiorniLavorati		=	giornilavorativi(date('Y')."-01-01", date('Y-m-d'));
$GiorniLavorativi	=	5;

$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_schede_rifiuti_deposito");
$FEDIT->FGE_SetFormFields(array("ID_RIF","ID_UMIS","ID_SF","peso_spec","NumCont","ID_CONT","MaxStock","AVVISO_SINGOLO","ID_IMP","eseguo_carico_automatico","carico_automatico","intervallo","ID_ICA","media_gg_stima","media_gg_eff","media_settimanale_stima","media_settimanale_eff","MaxStockClone","scarico_medio"),"user_schede_rifiuti_deposito");
$FEDIT->FGE_SetFormFields(array("last_carico", "last_check"),"user_schede_rifiuti_deposito");
if(!isset($_GET["pri"]) && !isset($_GET["filter"])) {

	

	## DERIVO ID_UMI E ID_SF
	$sql ="SELECT ID_UMIS, ID_SF from user_schede_rifiuti WHERE ID_RIF='".$_GET['ID_RIF']."'";
	$FEDIT->SDBRead($sql,"UMIS_SF",true,false);
	$ID_UMIS=$FEDIT->UMIS_SF[0]['ID_UMIS'];
	$ID_SF=$FEDIT->UMIS_SF[0]['ID_SF'];
	
	## DERIVO QUANTITA' SCARICO MEDIO
	$sql = "SELECT AVG(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS media FROM user_movimenti_fiscalizzati WHERE ID_RIF='".$_GET['ID_RIF']."' AND TIPO='S' AND produttore='1'";
	$FEDIT->SDBRead($sql,"DbRecordset",true,false);
	$MEDIA_S=number_format($FEDIT->DbRecordset[0]['media'], 2, ".", "");
	
	## DERIVO PRODUZIONE MEDIA GIORNALIERA E SETTIMANALE - scarichi
	$sql = "SELECT SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN )) AS scaricato FROM user_movimenti_fiscalizzati WHERE ID_RIF='".$_GET['ID_RIF']."' AND TIPO='S' AND produttore='1'";
	$FEDIT->SDBRead($sql,"DbRecordset",true,false);
	$PRODOTTO=$FEDIT->DbRecordset[0]['scaricato'];
	$MediaCggRound  = number_format($PRODOTTO / $GiorniLavorati, 2, ".", "");
	$MediaSettRound = number_format($PRODOTTO / $GiorniLavorati * $GiorniLavorativi, 2, ".", "");

	## DERIVO DISPO RIFIUTO
	$SQL="SELECT FKEdisponibilita FROM user_schede_rifiuti WHERE ID_RIF=(SELECT ID_RIF FROM user_schede_rifiuti_deposito WHERE ID_DEP='".$_GET['filter']."')";
	$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
	$DISPO=$FEDIT->DbRecordSet[0]['FKEdisponibilita'];

	$FEDIT->FGE_DescribeFields();

	$FEDIT->FGE_LookUpDefault("ID_CONT","user_schede_rifiuti_deposito",true);
	$FEDIT->FGE_LookUpDefault("ID_IMP","user_schede_rifiuti_deposito");
	$FEDIT->FGE_LookUpDefault("ID_ICA","user_schede_rifiuti_deposito",false,true);

	# rifiuto
	$FEDIT->FGE_LookUpCFG("ID_RIF","user_schede_rifiuti_deposito");
	$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
	//$FEDIT->FGE_SetSelectFields(array("ID_RIF","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("ID_RIF","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
	$ProfiloFields = "user_schede_rifiuti";
	include("SOGER_FiltriProfilo.php");
	$FEDIT->FGE_SetOrder("lov_cer:COD_CER");
	//$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_rifiuti");
	$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#

	# unit� misura
	$FEDIT->FGE_LookUpCFG("ID_UMIS","user_schede_rifiuti_deposito", false, true);
	$FEDIT->FGE_UseTables("lov_misure");
	$FEDIT->FGE_SetSelectFields(array("ID_UMIS","description"),"lov_misure");
	$FEDIT->FGE_SetFilter("ID_UMIS",$ID_UMIS,"lov_misure");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#

	# stato fisico
	$FEDIT->FGE_LookUpCFG("ID_SF","user_schede_rifiuti_deposito", false, true);
	$FEDIT->FGE_UseTables("lov_stato_fisico");
	$FEDIT->FGE_SetSelectFields(array("ID_SF","description"),"lov_stato_fisico");
	$FEDIT->FGE_SetFilter("ID_SF",$ID_SF,"lov_stato_fisico");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#
	
	$FEDIT->FGE_SetValue("AVVISO_SINGOLO",0,"user_schede_rifiuti_deposito");
	$FEDIT->FGE_SetValue("eseguo_carico_automatico",0,"user_schede_rifiuti_deposito");
	$FEDIT->FGE_SetValue("carico_automatico",0,"user_schede_rifiuti_deposito");
	$FEDIT->FGE_SetValue("intervallo",1,"user_schede_rifiuti_deposito");

	$FEDIT->FGE_SetValue("media_gg_stima",0,"user_schede_rifiuti_deposito");
	$FEDIT->FGE_SetValue("media_gg_eff",$MediaCggRound,"user_schede_rifiuti_deposito");

	$FEDIT->FGE_SetValue("media_settimanale_stima",0,"user_schede_rifiuti_deposito");
	$FEDIT->FGE_SetValue("media_settimanale_eff",$MediaSettRound,"user_schede_rifiuti_deposito");
	$FEDIT->FGE_SetValue("scarico_medio",$MEDIA_S,"user_schede_rifiuti_deposito");
	$FEDIT->FGE_SetValue("ID_RIF",$_GET['ID_RIF'],"user_schede_rifiuti_deposito");
	$FEDIT->FGE_SetValue("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti_deposito");

	$FEDIT->FGE_SetValue("last_check",date('d/m/Y'),"user_schede_rifiuti_deposito");
	$FEDIT->FGE_SetValue("last_carico",date('d/m/Y'),"user_schede_rifiuti_deposito");

	$FEDIT->FGE_HideFields(array("ID_IMP","last_carico","last_check"),"user_schede_rifiuti_deposito");
	$FEDIT->FGE_DisableFields(array("ID_RIF","media_gg_stima","media_gg_eff","media_settimanale_stima","media_settimanale_eff","scarico_medio","ID_UMIS","ID_SF","peso_spec","MaxStockClone"),"user_schede_rifiuti_deposito");

	$FEDIT->FGE_SetTitle("ID_RIF","Rifiuto","user_schede_rifiuti_deposito");
	$FEDIT->FGE_SetTitle("NumCont","Contenitore per lo stoccaggio","user_schede_rifiuti_deposito");
	$FEDIT->FGE_SetTitle("eseguo_carico_automatico","Carichi automatici","user_schede_rifiuti_deposito");

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"salva organizzazione deposito");

	echo '<form name="tmp_portata">';
	echo '<input type="hidden" value="" id="portata" name="portata" />';
	echo '<input type="hidden" value="" id="m_cubi" name="m_cubi" />';
	echo '<input type="hidden" value="'.$DISPO.'" id="dispo" name="dispo" />';
	echo '<input type="hidden" value="'.date('Y-m-d').'" id="lastc" name="lastc" />';
	echo '<input type="hidden" value="'.date('Y-m-d').'" id="lastck" name="lastck" />';
	echo '<input type="hidden" value="0" id="last_eseguo" name="last_eseguo" />';
	echo '<input type="hidden" value="'.$MediaCggRound.'" id="MediaCgg" name="MediaCgg" />';
	echo '</form>';


} else {

	## DERIVO ID_UMI E ID_SF
	$sql ="SELECT ID_UMIS, ID_SF from user_schede_rifiuti WHERE ID_RIF=(SELECT ID_RIF FROM user_schede_rifiuti_deposito WHERE ID_DEP='".$_GET["filter"]."')";
	$FEDIT->SDBRead($sql,"UMIS_SF",true,false);
	$ID_UMIS=$FEDIT->UMIS_SF[0]['ID_UMIS'];
	$ID_SF=$FEDIT->UMIS_SF[0]['ID_SF'];

	## DERIVO PORTATA e M CUBI CONTENITORE
	$SQL="SELECT portata, m_cubi FROM lov_contenitori WHERE ID_CONT=(SELECT ID_CONT FROM user_schede_rifiuti_deposito WHERE ID_DEP='".$_GET['filter']."')";
	$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
	$PORTATA=$FEDIT->DbRecordSet[0]['portata'];
	$MCUBI=$FEDIT->DbRecordSet[0]['m_cubi'];

	## DERIVO DISPO RIFIUTO
	$SQL="SELECT FKEdisponibilita FROM user_schede_rifiuti WHERE ID_RIF=(SELECT ID_RIF FROM user_schede_rifiuti_deposito WHERE ID_DEP='".$_GET['filter']."')";
	$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
	$DISPO=$FEDIT->DbRecordSet[0]['FKEdisponibilita'];
	
	## DERIVO DATA ULTIMO CARICO
	$SQL="SELECT last_carico, last_check, eseguo_carico_automatico FROM user_schede_rifiuti_deposito WHERE ID_DEP='".$_GET['filter']."'";
	$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
	$LASTC=$FEDIT->DbRecordSet[0]['last_carico'];
	$LASTCK=$FEDIT->DbRecordSet[0]['last_check'];
	$Eseguo=$FEDIT->DbRecordSet[0]['eseguo_carico_automatico'];


	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_CONT","user_schede_rifiuti_deposito",true);
	$FEDIT->FGE_LookUpDefault("ID_IMP","user_schede_rifiuti_deposito");
	$FEDIT->FGE_LookUpDefault("ID_ICA","user_schede_rifiuti_deposito",false,true);

	# rifiuto
	$FEDIT->FGE_LookUpCFG("ID_RIF","user_schede_rifiuti_deposito");
	$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
	//$FEDIT->FGE_SetSelectFields(array("ID_RIF","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("ID_RIF","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
	$ProfiloFields = "user_schede_rifiuti";
	include("SOGER_FiltriProfilo.php");
	$FEDIT->FGE_SetOrder("lov_cer:COD_CER");
	//$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_rifiuti");
	$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#
	
	# unit� misura
	$FEDIT->FGE_LookUpCFG("ID_UMIS","user_schede_rifiuti_deposito", false, true);
	$FEDIT->FGE_UseTables("lov_misure");
	$FEDIT->FGE_SetSelectFields(array("ID_UMIS","description"),"lov_misure");
	$FEDIT->FGE_SetFilter("ID_UMIS",$ID_UMIS,"lov_misure");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#

	# stato fisico
	$FEDIT->FGE_LookUpCFG("ID_SF","user_schede_rifiuti_deposito", false, true);
	$FEDIT->FGE_UseTables("lov_stato_fisico");
	$FEDIT->FGE_SetSelectFields(array("ID_SF","description"),"lov_stato_fisico");
	$FEDIT->FGE_SetFilter("ID_SF",$ID_SF,"lov_stato_fisico");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#


	$FEDIT->FGE_SetValue("media_settimanale_eff",$MediaSettRound,"user_schede_rifiuti_deposito");
	$FEDIT->FGE_SetValue("media_gg_eff",$MediaCggRound,"user_schede_rifiuti_deposito");

	$FEDIT->FGE_HideFields(array("ID_IMP","last_carico","last_check"),"user_schede_rifiuti_deposito");
	$FEDIT->FGE_DisableFields(array("ID_RIF","media_gg_stima","media_gg_eff","media_settimanale_stima","media_settimanale_eff","scarico_medio","ID_UMIS","ID_SF","peso_spec","MaxStockClone"),"user_schede_rifiuti_deposito");

	$FEDIT->FGE_SetTitle("ID_RIF","Rifiuto","user_schede_rifiuti_deposito");
	$FEDIT->FGE_SetTitle("NumCont","Contenitore per lo stoccaggio","user_schede_rifiuti_deposito");
	$FEDIT->FGE_SetTitle("eseguo_carico_automatico","Carichi automatici","user_schede_rifiuti_deposito");

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva organizzazione deposito");

	echo '<form name="tmp_portata">';
	echo '<input type="hidden" value="'.$PORTATA.'" id="portata" name="portata" />';
	echo '<input type="hidden" value="'.$MCUBI.'" id="m_cubi" name="m_cubi" />';
	echo '<input type="hidden" value="'.$DISPO.'" id="dispo" name="dispo" />';
	echo '<input type="hidden" value="'.$LASTC.'" id="lastc" name="lastc" />';
	echo '<input type="hidden" value="'.$LASTCK.'" id="lastck" name="lastc" />';
	echo '<input type="hidden" value="'.$MediaCggRound.'" id="MediaCgg" name="MediaCgg" />';
	echo '<input type="hidden" value="'.$Eseguo.'" id="last_eseguo" name="last_eseguo" />';
	echo '</form>';
	}

require_once("__includes/COMMON_sleepForgEdit.php");
?>
