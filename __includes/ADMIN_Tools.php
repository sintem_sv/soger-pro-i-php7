<?php
	require_once("__includes/COMMON_ForgEditClassFiles.php");
	require_once("__includes/COMMON_wakeForgEdit.php");
	$FEDIT->FGE_FlushTableInfo();
?>
<script type="text/javascript">
function copyChk() {
	if(document.forms["00"].soggettiSource.value=="###" | document.forms["00"].soggettiDest.value=="###") {
		alert("Selezionare sorgente e destinazione per la copia!");
		return;
	}
	if(document.forms["00"].soggettiSource.value==document.forms["00"].soggettiDest.value) {
		alert("Sorgente e destinazione sono identici!");
		return;
	}	
	document.forms["00"].submit();
}
</script>
<div class="FGEDataGridTitle"><div>COPIA ANAGRAFICHE</div></div><br />
<?php
global $SOGER;
if($SOGER->AppLocation!="login" && $SOGER->AppLocation!="login_documents" && count($SOGER->AppNavDIVs)>0) {
	$tmp .= "<div id=\"NavDiv\">\n";
	$tmp .= "<!-- NAV DIVs BEGIN -->\n[NAV_STUCTURE]\n<!-- NAV DIVs END -->\n";
	$SOGER->DIVmake_Navigation();
	$tmp .= "</div>\n";
	echo $tmp;
	}
?>

<form name="00" method="get" action="__scripts/statusCUSTOM_AdminAnagCopy.php" style="width: 95%; margin-top:100px;">
<fieldset class="FGEfieldset">

<table width="100%"><tr>
<td>
	<b>Copia Anagrafiche da</b>
	<?php
		echo ComboSoggetti($FEDIT,"soggettiSource");
	?>
</tr>
<tr>
<td>
	<b>a</b>
	<?php
		echo ComboSoggetti($FEDIT,"soggettiDest");
	?>
</td>
</tr>
<tr>
<td><input type="button" class="FGEbutton" value="copia" style="margin-top:20px;" onclick="javascript:copyChk();"/>
</td></tr>
</table>
</fieldset>
</form>
<?php
require_once("__includes/COMMON_sleepForgEdit.php");

function ComboSoggetti(&$obj,$name) {
	$sql = "SELECT core_intestatari.ID_INT AS IDint, core_intestatari.description AS INTdes, ";
	$sql .= "core_gruppi.ID_GRP as IDgrp, core_gruppi.description AS GRPdes, ";
	$sql .= "core_impianti.ID_IMP as IDimp, core_impianti.description AS IMPdes, produttore, trasportatore, destinatario, intermediario ";
	$sql .= "FROM core_intestatari JOIN core_gruppi ON core_intestatari.ID_INT=core_gruppi.ID_INT JOIN core_impianti ON core_impianti.ID_GRP=core_gruppi.ID_GRP ";  
	$sql .= "ORDER BY INTdes, GRPdes, IMPdes";  
	$obj->SDBRead($sql,"DbRecordSet");
	$buf = "<select name=\"$name\" id=\"$name\" class=\"FGEinput\">";
	$buf .= "<option value=\"###\"/>";
	$IMPref = "";
	foreach($obj->DbRecordSet as $k=>$dati) {
		if($dati["IDimp"]!=$IMPref) {
			$buf .= "<optgroup label=\"" . $dati["INTdes"] . " > " . $dati["GRPdes"]  ." > " .  $dati["IMPdes"]  . "\"/>";
			$IMPref = $dati["IDimp"]; 
		}
		if($dati["produttore"]=="1") {
			$buf .= "<option value=\"" . $dati["IDimp"] . "|P"  . "\">" . $dati["INTdes"] . " "  . $dati["IMPdes"] . "  (produttore)";
			$buf .= "</option>";
		}
		if($dati["destinatario"]=="1") {
			$buf .= "<option value=\"" . $dati["IDimp"] . "|D" . "\">" . $dati["INTdes"] . " " . $dati["IMPdes"] . " (destinatario)";
			$buf .= "</option>";
		}
		if($dati["trasportatore"]=="1") {
			$buf .= "<option value=\"" . $dati["IDimp"] . "|T" . "\">" . $dati["INTdes"] . " " . $dati["IMPdes"] . " (trasportatore)";
			$buf .= "</option>";
		}
		if($dati["intermediario"]=="1") {
			$buf .= "<option value=\"" . $dati["IDimp"] . "|I" . "\">" . $dati["INTdes"] . " " . $dati["IMPdes"] . " (intermediario)";
			$buf .= "</option>";
		}

	}
	$buf .= "</select>";
	return $buf;
}
?>
