<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;


## query info rifiuto ##

$sql = "SELECT descrizione, PREV_NextS, FKEdisponibilita as giacenza, ID_UMIS, peso_spec from user_schede_rifiuti where ID_RIF='".$_SESSION['CR_ID_IRF']."';";

$FEDIT->SDbRead($sql,"DbRecordSet");

$rifiuto=$FEDIT->DbRecordSet[0]['descrizione'];
switch($FEDIT->DbRecordSet[0]['ID_UMIS']){
	case "1": #Kg
		$rifiutoGiacenza=$FEDIT->DbRecordSet[0]['giacenza'];
		break;
	case "2": #Litri
		$rifiutoGiacenza=($FEDIT->DbRecordSet[0]['giacenza'] * $FEDIT->DbRecordSet[0]['peso_spec']);
		break;
	case "3": # Mc
		$rifiutoGiacenza=($FEDIT->DbRecordSet[0]['giacenza'] * $FEDIT->DbRecordSet[0]['peso_spec'] * 1000);
		break;
	}

## DATA PROSSIMO SCARICO
$NextS=$FEDIT->DbRecordSet[0]['PREV_NextS'];
if($NextS!="0000-00-00" && $NextS!=""){
	$NextS=explode("-", $NextS);
	$NextS=$NextS[2].'/'.$NextS[1].'/'.$NextS[0];
	$data=$NextS;
	}
else
	$data=date("d/m/Y");


## query info deposito ##
$sql ="SELECT scarico_medio FROM user_schede_rifiuti_deposito WHERE ID_RIF='".$_SESSION['CR_ID_IRF']."';";
$FEDIT->SDbRead($sql,"DbRecordSet");
$ScaricoMedio=$FEDIT->DbRecordSet[0]['scarico_medio'];
if($ScaricoMedio=="") $ScaricoMedio="0.00";


## query destinatari ##

$sql ="SELECT DISTINCT user_aziende_destinatari.ID_AZD, user_aziende_destinatari.ID_IMP, user_aziende_destinatari.description, indirizzo, lov_comuni_istat.description as comune ";
$sql.="FROM user_aziende_destinatari ";
$sql.="JOIN lov_comuni_istat on user_aziende_destinatari.ID_COM=lov_comuni_istat.ID_COM ";
$sql.="JOIN user_impianti_destinatari on user_aziende_destinatari.ID_AZD=user_impianti_destinatari.ID_AZD ";
$sql.="LEFT JOIN user_autorizzazioni_dest on user_impianti_destinatari.ID_UIMD=user_autorizzazioni_dest.ID_UIMD ";
$sql.="WHERE user_autorizzazioni_dest.ID_RIF='".$_SESSION['CR_ID_IRF']."' AND user_aziende_destinatari.ID_IMP='".$SOGER->UserData["core_impiantiID_IMP"] . "' ";
$TableSpec="user_aziende_destinatari.";
include("SOGER_DirectProfilo.php");
$sql.="ORDER BY user_aziende_destinatari.description ASC ";
$FEDIT->SDbRead($sql,"DbRecordSetDest");

## query trasportatori ##

$sql ="SELECT DISTINCT user_aziende_trasportatori.ID_AZT, user_aziende_trasportatori.ID_IMP, user_aziende_trasportatori.description, indirizzo, lov_comuni_istat.description as comune ";
$sql.="FROM user_aziende_trasportatori ";
$sql.="JOIN lov_comuni_istat on user_aziende_trasportatori.ID_COM=lov_comuni_istat.ID_COM ";
$sql.="JOIN user_impianti_trasportatori on user_aziende_trasportatori.ID_AZT=user_impianti_trasportatori.ID_AZT ";
$sql.="LEFT JOIN user_autorizzazioni_trasp on user_impianti_trasportatori.ID_UIMT=user_autorizzazioni_trasp.ID_UIMT ";
$sql.="WHERE user_autorizzazioni_trasp.ID_RIF='".$_SESSION['CR_ID_IRF']."' AND user_aziende_trasportatori.ID_IMP='".$SOGER->UserData["core_impiantiID_IMP"] . "' ";
$TableSpec="user_aziende_trasportatori.";
include("SOGER_DirectProfilo.php");
$sql.="ORDER BY user_aziende_trasportatori.description ASC ";
$FEDIT->SDbRead($sql,"DbRecordSetTrasp");


## query intermediari ##

$sql ="SELECT DISTINCT user_aziende_intermediari.ID_AZI, user_aziende_intermediari.ID_IMP, user_aziende_intermediari.description, indirizzo, lov_comuni_istat.description as comune ";
$sql.="FROM user_aziende_intermediari ";
$sql.="JOIN lov_comuni_istat on user_aziende_intermediari.ID_COM=lov_comuni_istat.ID_COM ";
$sql.="WHERE user_aziende_intermediari.ID_IMP='".$SOGER->UserData["core_impiantiID_IMP"] . "' ";
$TableSpec="user_aziende_intermediari.";
include("SOGER_DirectProfilo.php");
$sql.="ORDER BY user_aziende_intermediari.description ASC ";
$FEDIT->SDbRead($sql,"DbRecordSetInt");


## option destinatari ##
$OptD="<option value=\"0\">-- Destinatari --</option>";
for($od=0;$od<count(@$FEDIT->DbRecordSetDest);$od++){
	$OptD.="<option value=\"".$FEDIT->DbRecordSetDest[$od]['ID_AZD']."\">".$FEDIT->DbRecordSetDest[$od]['description']."</option>";
	}

## option trasportatori ##
$OptT="<option value=\"0\">-- Trasportatori --</option>";
for($ot=0;$ot<count(@$FEDIT->DbRecordSetTrasp);$ot++){
	$OptT.="<option value=\"".$FEDIT->DbRecordSetTrasp[$ot]['ID_AZT']."\">".$FEDIT->DbRecordSetTrasp[$ot]['description']."</option>";
	}

## option intermediari ##
$OptI="<option value=\"0\">-- Intermediari --</option>";
for($oi=0;$oi<count(@$FEDIT->DbRecordSetInt);$oi++){
	$OptI.="<option value=\"".$FEDIT->DbRecordSetInt[$oi]['ID_AZI']."\">".$FEDIT->DbRecordSetInt[$oi]['description']."</option>";
	}

$javascript ="<script type='text/javascript'>\n";
$javascript.= <<< JAVASCRIPT
function resetOtherSelect(selected){
	switch(selected){
		case 'd':
			document.FGEForm_1.trasportatore[0].selected = "1";
			document.FGEForm_1.intermediario[0].selected = "1";
			break;
		case 't':
			document.FGEForm_1.destinatario[0].selected = "1";
			document.FGEForm_1.intermediario[0].selected = "1";
			break;
		case 'i':
			document.FGEForm_1.trasportatore[0].selected = "1";
			document.FGEForm_1.destinatario[0].selected = "1";
			break;
		}
	}
JAVASCRIPT;
$javascript.="</script>";
echo $javascript;



$javascript2 ="<script type='text/javascript'>\n";
$javascript2.= <<<JAVASCRIPT2
	function validazione(FRef){
		var error='Attenzione:\\n';
		var printError=false;
		var RxpPattern = /^(0[1-9]|1[0-9]|2[0-9]|3[01]|[1-9])\/+(0[1-9]|1[0-2]|[1-9])\/+(19|20)[0-9]{2}$/;
		var RxpPatternNum = /^\-?([0-9])+\.?([0-9])*$/;
		
		if(document.getElementById('data').value!='') {			
			if(!document.getElementById('data').value.match(RxpPattern)) {		
				error+='- Il formato della data � errato\\n';
				printError=true;
				}
			}		
	
	if(!document.getElementById('quantita').value.match(RxpPatternNum)){
		error+='- La quantit� inserita deve essere un numero\\n';
		printError=true;
		}

	if(FRef.destinatario.value==0 && FRef.trasportatore.value==0 && FRef.intermediario.value==0){
		error+='- Occorre selezionare il soggetto destinatario della richiesta\\n';
		printError=true;
		}

	if(printError)
		window.alert(error);
	else
		FRef.submit();
		}

function KeyCheck(e) {
	var KeyID = (window.event) ? event.keyCode : e.keyCode;
	switch(KeyID) {
		case 13:
			validazione(document.FGEForm_1);
			break;
	}
}

document.onkeypress = KeyCheck;

JAVASCRIPT2;
$javascript2.="</script>";
echo $javascript2;

$html = <<< HTML
<div class="FGEDataGridTitle"><div>Deposito Rifiuti � Gestione Deposito � Richiesta conferimento rifiuto</div></div>
<form name="FGEForm_1" id="FGEForm_1" method="post" action="__scripts/ConferimentoRifiuto.php">

<fieldset  class="FGEfieldset">
<legend class="FGElegend">$rifiuto >> Richiesta conferimento rifiuto</legend>

<div class="FGEFormTitle FGE4Col">Informazioni Generali</div>

<div id="FGED_quantita" class="FGE1Col">
<label for="quantita" class="FGElabel">Quantit�</label><br/><input type="text" id="quantita" name="quantita" tabindex="1" size="10" maxlength="10" value="[GIACENZA]" class="FGEinput" onfocus="this.style.background='yellow';"  onblur="this.style.background='white';"/>
</div>

<div id="FGED_data" class="FGE1Col">
<label for="data" class="FGElabel">Conferimento richiesto per il</label><br/><input type="text" id="data" name="data" tabindex="2" size="10" maxlength="10" value="$data" class="FGEinput" onfocus="this.style.background='yellow';"  onblur="this.style.background='white';"/>
</div>

<div id="FGED_data" class="FGE1Col">
<label for="data" class="FGElabel">Scarico medio</label><br/><input type="text" id="data" name="data" tabindex="2" size="10" maxlength="10" value="[SCARICO_MEDIO]" class="FGEinput" disabled onfocus="this.style.background='yellow';"  onblur="this.style.background='white';"/>
</div>

<div class="FGEFormTitle FGE4Col">Destinatario della richiesta</div>

<!--
<table width="100%">
	<tr>
		<td><input type="button" class="FGEbutton" tabindex="3" value="destinatario" onclick="#" style="margin-right: 5px;width:250px;"/></td>
		<td><input type="button" class="FGEbutton" tabindex="4" value="trasportatore" onclick="#" style="margin-right: 5px;width:250px;"/></td>
		<td><input type="button" class="FGEbutton" tabindex="5" value="intermediario" onclick="#" style="margin-right: 5px;width:250px;"/></td>
	</tr>
</table>
-->

<table width="100%">
	<tr>
		<td><select name="destinatario" style="margin-right: 5px;width:250px;" onChange="resetOtherSelect('d');">[OPT_DEST]</select></td>
		<td><select name="trasportatore" style="margin-right: 5px;width:250px;" onChange="resetOtherSelect('t');">[OPT_TRASP]</select></td>
		<td><select name="intermediario" style="margin-right: 5px;width:250px;" onChange="resetOtherSelect('i');">[OPT_INT]</select></td>
	</tr>
</table>

<div class="FGEFormTitle FGE4Col">Destinatario del rifiuto</div>

<table class="FGEDataGridTable">
	<tr>
		<th class="FGEDataGridTableHeader" id="check">&nbsp;</th>
		<th class="FGEDataGridTableHeader" id="azienda">azienda</th>
		<th class="FGEDataGridTableHeader" id="indirizzo">indirizzo</th>
		<th class="FGEDataGridTableHeader" id="comune">comune</th>
	</tr>
	
HTML;

for($d=0;$d<count($FEDIT->DbRecordSetDest);$d++){
	$html.="<tr>";
	$html.="<td class=\"FGEDataGridTableCell\" headers=\"check\">";
	$html.="<input name=\"dest_rif\" onfocus=\"this.style.background='yellow';\"  onblur=\"this.style.background='white';\" tabindex=\"".$d."\" type=\"radio\" value=\"".$FEDIT->DbRecordSetDest[$d]['ID_AZD']."\" ";
	if($d==0) $html.=" checked ";
	$html.="/>";
	$html.="</td>";
	$html.="<td class=\"FGEDataGridTableCell\" headers=\"azienda\">".$FEDIT->DbRecordSetDest[$d]['description']."</td>";
	$html.="<td class=\"FGEDataGridTableCell\" headers=\"indirizzo\">".$FEDIT->DbRecordSetDest[$d]['indirizzo']."</td>";
	$html.="<td class=\"FGEDataGridTableCell\" headers=\"comune\">".$FEDIT->DbRecordSetDest[$d]['comune']."</td>";
	$html.="</tr>";
	}

$html.="</table>";

$html.="<input type=\"hidden\" name=\"ID_RIF\" value=\"".$_SESSION['CR_ID_IRF']."\" />";

$html.="<input type=\"button\" class=\"FGEbutton\" value=\"genera richiesta\" onclick=\"validazione(document.FGEForm_1);\" style=\"margin-top: 50px;width:200px;\"/>";

$html.="</fieldset></form>";

$html = str_replace("[OPT_DEST]", $OptD, $html);
$html = str_replace("[OPT_TRASP]", $OptT, $html);
$html = str_replace("[OPT_INT]", $OptI, $html);
$html = str_replace("[GIACENZA]", $rifiutoGiacenza, $html);
$html = str_replace("[SCARICO_MEDIO]", $ScaricoMedio, $html);

echo $html;

require_once("__includes/COMMON_sleepForgEdit.php");
?>