<?php

require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
global $SOGER;

$_SESSION['LKUP_IDRIF_CHANGED'] = false;

if(isset($_GET['table'])){
	$TableName=$_GET['table'];
        $sql = "SELECT ID_RIF FROM ".$TableName." WHERE ".$_GET['pri']."=".$_GET['filter'].";";
	$FEDIT->SDBRead($sql,"DbRecordSetPerLkpFilter",true,false);
	$_SESSION['LKUP_IDRIF'] = $FEDIT->DbRecordSetPerLkpFilter[0]['ID_RIF'];
}
else{
	if($SOGER->UserData['core_impiantiREG_IND']=='1')
		$TableName="user_movimenti";
	else
		$TableName="user_movimenti_fiscalizzati";
        $_SESSION['LKUP_IDRIF'] = 0;
}

#
#	RICAVA IL NUMERO MOVIMENTO
#
if(!isset($_GET["table"]) && !isset($_GET["pri"]) && !isset($_GET["filter"])) {
	require("__mov_snipplets/FRM_NumeroMOVIMENTO.php");
	if($SOGER->UserData['workmode']!='produttore'){
            if($SOGER->UserData["core_usersFRM_DefaultNFORM"]==1)
		require("__mov_snipplets/FRM_NumeroFormulario.php");
            }
	}

#
#	CAMPI
#	

$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables($TableName);

$FEDIT->FGE_SetFormFields(array("ID_IMP","TIPO","NMOV","DTMOV","NFORM","DTFORM"),$TableName);
if($SOGER->UserData['core_usersFANGHI']==1)	$FEDIT->FGE_SetFormFields(array("FANGHI"),$TableName);
$FEDIT->FGE_SetFormFields(array("ID_RIF","originalID_RIF","FKESF","FKEumis","FKEdisponibilita","lotto","FKEpesospecifico"),$TableName);
$FEDIT->FGE_SetFormFields(array("quantita","qta_hidden","tara","pesoL","pesoN","FKEgiacINI","FKElastCarico"),$TableName);
$FEDIT->FGE_SetFormFields(array("adr","QL","ID_ONU"),$TableName);
$FEDIT->FGE_SetFormFields(array("ID_AZP","FKEcfiscP","ID_UIMP"),$TableName);
$FEDIT->FGE_SetFormFields(array("ID_AZD","FKEcfiscD","ID_UIMD","Transfrontaliero","ID_AUTHD","FKErilascioD","FKEscadenzaD"),$TableName);

if($SOGER->UserData['workmode']!='produttore') 
	$FEDIT->FGE_SetFormFields(array("ID_OP_RS"),$TableName);

$FEDIT->FGE_SetFormFields(array("ID_AZT","FKEcfiscT","ID_UIMT","ID_AUTHT","FKErilascioT","FKEscadenzaT","NumAlboAutotraspProprio","NumAlboAutotrasp"),$TableName);
$FEDIT->FGE_SetFormFields(array("ID_AZI","FKEcfiscI","ID_UIMI","ID_AUTHI","FKErilascioI","FKEscadenzaI"),$TableName);
$FEDIT->FGE_SetFormFields(array("NPER"),$TableName);

$FEDIT->FGE_SetFormFields(array("NOTER"),$TableName);

# DATI COMPILATORE
$FEDIT->FGE_SetFormFields("ID_USR",$TableName);

#
#	UNIQUE STRING TO BLOCK DUPLICATION
#
$FEDIT->FGE_SetFormFields("UniqueString",$TableName);


#
require("__mov_snipplets/FRM_Carico_DisHide.php");
#

$imageRif = '<div id="imageRif" class="imageRif">&nbsp;&nbsp;</div>';
$rosetteD = '<div id="rosetteD" class="rosette">&nbsp;&nbsp;</div>';
$rosetteT = '<div id="rosetteT" class="rosette">&nbsp;&nbsp;</div>';
$rosetteI = '<div id="rosetteI" class="rosette">&nbsp;&nbsp;</div>';

$FEDIT->FGE_SetTitle("ID_AZP","Produttore",$TableName);
$FEDIT->FGE_SetTitle("ID_RIF",$imageRif."Rifiuto",$TableName);
$FEDIT->FGE_SetTitle("adr","ADR",$TableName);
$FEDIT->FGE_SetTitle("ID_AZD",$rosetteD."Destinatario",$TableName);
$FEDIT->FGE_SetTitle("ID_AZT",$rosetteT."Trasportatore",$TableName);
$FEDIT->FGE_SetTitle("ID_AZI",$rosetteI."Intermediario",$TableName);
$FEDIT->FGE_SetTitle("NOTER","Annotazioni",$TableName);
$FEDIT->FGE_SetBreak("quantita",$TableName);



$ProfiloFields = $TableName;
include("SOGER_CampiProfilo.php");
$FEDIT->FGE_DescribeFields();

#
#	LOOKUP
#

$autoTRADEST = true;
require("__mov_snipplets/LKUP_Rifiuto.php");
require("__mov_snipplets/LKUP_Produttori.php");
require("__mov_snipplets/LKUP_ProduttoriImp.php");
require("__mov_snipplets/LKUP_Destinatari.php");      
require("__mov_snipplets/LKUP_DestinatariImp.php");
require("__mov_snipplets/LKUP_DestinatariAuth.php");
require("__mov_snipplets/LKUP_Trasportatori.php");
require("__mov_snipplets/LKUP_TrasportatoriImp.php");
require("__mov_snipplets/LKUP_TrasportatoriAuth.php"); 
require("__mov_snipplets/LKUP_Intermediari.php");
require("__mov_snipplets/LKUP_IntermediariImp.php");
require("__mov_snipplets/LKUP_IntermediariAuth.php");
if($SOGER->UserData["core_usersREG_RS_CARICHI"]=="1" | $SOGER->UserData['workmode']!='produttore') 
	require("__mov_snipplets/LKUP_OpRS.php");
$FEDIT->FGE_LookUpDefault("ID_ONU",$TableName,true);

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {
	if(isset($_SESSION['Fiscalizzato'])) unset($_SESSION['Fiscalizzato']);
	if(isset($_SESSION['Fiscale'])) unset($_SESSION['Fiscale']);
	#
	#	FILTRI di DEFAULT 
	#
	require("__mov_snipplets/FLTR_MovimentoDefault.php");
	#
	#	VALORI di DEFAULT
	#
	$FEDIT->FGE_SetValue("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],$TableName);
	$FEDIT->FGE_SetValue("NMOV",$NumMov,$TableName);
	$FEDIT->FGE_SetValue("DTMOV",date("m/d/Y"),$TableName);
	if($SOGER->UserData['workmode']!='produttore'){
		$FEDIT->FGE_SetValue("NFORM",$NumForm,$TableName);
		$FEDIT->FGE_SetValue("DTFORM",date("m/d/Y"),$TableName);
		}
	$FEDIT->FGE_SetValue("TIPO","C",$TableName);
	$FEDIT->FGE_SetValue("NPER",0,$TableName);
	$FEDIT->FGE_SetValue("quantita",0,$TableName);
	$FEDIT->FGE_SetValue("qta_hidden",0,$TableName);
	$FEDIT->FGE_SetValue("tara",0,$TableName);
	$FEDIT->FGE_SetValue("pesoL",0,$TableName);
	$FEDIT->FGE_SetValue("pesoN",0,$TableName);
	$FEDIT->FGE_SetValue("adr",0,$TableName);
	$FEDIT->FGE_SetValue("QL",0,$TableName);
	$FEDIT->FGE_SetValue("ID_ONU",0,$TableName);
	$FEDIT->FGE_SetValue("Transfrontaliero",0,$TableName);
	$FEDIT->FGE_SetValue("FANGHI",0,$TableName);
	
	# DATI COMPILATORE
	$FEDIT->FGE_SetValue("ID_USR",$SOGER->UserData['core_usersID_USR'],$TableName);

	# UNIQUE STRING
	$Identifiers	= $SOGER->UserData['core_usersID_USR'].$SOGER->UserData['core_usersID_IMP'].$SOGER->UserData['workmode'];
	$UniqueString	= md5(uniqid($Identifiers, true));
	$FEDIT->FGE_SetValue("UniqueString", $UniqueString, $TableName);

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"salva movimento");

	# SE DEVO FARE MOVIMENTO AUTOMATICO...
	if(isset($_SESSION['MovimentoRIF'])){
		
$js ="<script type=\"text/javascript\">\n";
$js.="var TableName='".$TableName."';";
$js.="var Rifiuto='".$_SESSION['MovimentoRIF']."';";
$js.="var Quantita='".$_SESSION['MovimentoRIF_quantita']."';";
$js.= <<< ENDJS

$(document).ready ( function(){
   checkAndGo();
});

function checkAndGo() {
	var Element = document.getElementById(TableName+":ID_RIF");
    if (!Element) {
        setTimeout(checkAndGo, 500);
		} 
	else {
        SelectRifiuto();
		}
	}

function SelectRifiuto(){
	document.getElementById(TableName+":ID_RIF").value = Rifiuto;
	CUSTOM_RIFMOV('', document.getElementById(TableName+":ID_RIF"), '', Quantita);
	}

ENDJS;
$js.= "</script>";
echo $js;
unset($_SESSION['MovimentoRIF']);
		
		}

} else {

	if(!isset($_SESSION['Fiscalizzato'])) $_SESSION['Fiscalizzato']=$_GET['Fiscalizzato'];
	if(!isset($_SESSION['Fiscale'])) $_SESSION['Fiscale']=$_GET['Fiscale'];

	$NotEditable = false;
	# registro fiscale
	if($SOGER->UserData['core_impiantiREG_IND']=="1" && $_SESSION['Fiscalizzato']=="1") $NotEditable = true;
	if($SOGER->UserData['core_impiantiREG_IND']=="0" && $_SESSION['Fiscale']=="1")		$NotEditable = true;
	# sistri
	$sql = "SELECT idSIS, DTMOV FROM ".$TableName." WHERE ".$_GET['pri']."=".$_GET['filter'].";";
	$FEDIT->SDBRead($sql,"DbRecordSetEditable",true,false);
	if(!is_null($FEDIT->DbRecordSetEditable[0]['idSIS'])) $NotEditable = true;
	# vecchio 14 gg
	$oggi			= mktime(0, 0, 0, date('m'), date('d'), date('Y'));
	$movimento		= explode('-', $FEDIT->DbRecordSetEditable[0]['DTMOV']);
	$movimento_dt	= mktime(0, 0, 0, $movimento[1], $movimento[2], $movimento[0]);
	$differenza		= ($oggi - $movimento_dt)/(60*60*24);
	if($differenza>$SOGER->UserData['core_usersGGedit_'.$SOGER->UserData['workmode']])	$NotEditable = true;

	## COCCARDA DESTINATARIO
	$sql="SELECT ID_ORIGINE_DATI FROM user_autorizzazioni_dest JOIN ".$TableName." ON user_autorizzazioni_dest.ID_AUTHD=".$TableName.".ID_AUTHD WHERE ".$_GET['pri']."='".$ref."';";
	$FEDIT->SDBRead($sql,"DbRecordSetRosetteD",true,false);
	$ID_ORIGINE_DATI_D="'".$FEDIT->DbRecordSetRosetteD[0]['ID_ORIGINE_DATI']."'";

	## COCCARDA TRASPORTATORE
	$sql="SELECT ID_ORIGINE_DATI FROM user_autorizzazioni_trasp JOIN ".$TableName." ON user_autorizzazioni_trasp.ID_AUTHT=".$TableName.".ID_AUTHT WHERE ".$_GET['pri']."='".$ref."';";
	$FEDIT->SDBRead($sql,"DbRecordSetRosetteT",true,false);
	$ID_ORIGINE_DATI_T="'".$FEDIT->DbRecordSetRosetteT[0]['ID_ORIGINE_DATI']."'";
	
	## COCCARDA INTERMEDIARIO
	$sql="SELECT ID_ORIGINE_DATI FROM user_autorizzazioni_interm JOIN ".$TableName." ON user_autorizzazioni_interm.ID_AUTHI=".$TableName.".ID_AUTHI WHERE ".$_GET['pri']."='".$ref."';";
	$FEDIT->SDBRead($sql,"DbRecordSetRosetteI",true,false);
	$ID_ORIGINE_DATI_I="'".$FEDIT->DbRecordSetRosetteI[0]['ID_ORIGINE_DATI']."'";

	$_SESSION["IDMovimento"] = $_GET["filter"]; //serve???
	
	# devo impedire la modifica della scheda rifiuto se il carico � legato a scarichi
	if(isset($_GET['Involved'])){ //FGE_DataGridEdit.php $ScaricoBusy
		$FEDIT->FGE_DisableFields(array("ID_RIF"),$TableName);
		$FEDIT->FGE_DisableFields(array("quantita","tara","pesoL","pesoN"),$TableName);
		}

	if($NotEditable){
		$FEDIT->FGE_DisableFields(array("ID_RIF","FKESF","FKEumis","FKEpesospecifico","NPER","FANGHI"),$TableName);
		$FEDIT->FGE_DisableFields(array("quantita","tara","pesoL","pesoN"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_AZP","ID_UIMP"),$TableName);
		$FEDIT->FGE_DisableFields(array("DTMOV","DTFORM","NMOV","NFORM","lotto"),$TableName);
		$FEDIT->FGE_DisableFields("ID_OP_RS",$TableName);
		}
	
	# verifico se devo bloccare movimento perch� aperto da altri
	if($SOGER->UserData['core_usersO12']=='0'){
		
		$sql = "SELECT ID_USR FROM ".$_GET['table']." WHERE ".$_GET['pri']."=".$_GET['filter'].";";
		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
		
		if($FEDIT->DbRecordSet[0]['ID_USR']!='0000000' AND ($SOGER->UserData['core_usersID_USR']!=$FEDIT->DbRecordSet[0]['ID_USR'])){
			echo "<div id=\"Errors\" class=\"ErrType1\" onclick=\"javascript:document.getElementById('Errors').style.zIndex=-1\">";
			echo "L'utente non ha i permessi necessari per modificare i movimenti inseriti da altri utenti [cod. O12]";
			echo "</div>";
			$FEDIT->FGE_DisableFields(array("NMOV","DTMOV","NFORM","DTFORM"),$TableName);
			$FEDIT->FGE_DisableFields(array("ID_RIF","FKESF","FKEumis","FKEdisponibilita","lotto"),$TableName);
			$FEDIT->FGE_DisableFields(array("quantita","tara","pesoL","pesoN"),$TableName);
			$FEDIT->FGE_DisableFields(array("ID_AZP","FKEcfiscP","ID_UIMP"),$TableName);
			if($SOGER->UserData['workmode']!='produttore'){
				$FEDIT->FGE_DisableFields(array("QL","ID_ONU"),$TableName);
				$FEDIT->FGE_DisableFields(array("ID_AZD","FKEcfiscD","ID_UIMD","ID_AUTHD","FKErilascioD","ID_OP_RS"),$TableName);
				$FEDIT->FGE_DisableFields(array("ID_AZT","FKEcfiscT","ID_UIMT","ID_AUTHT","FKErilascioT"),$TableName);
				$FEDIT->FGE_DisableFields(array("ID_AZI","FKEcfiscI","ID_UIMI","ID_AUTHI","FKErilascioI"),$TableName);
				}
			$FEDIT->FGE_DisableFields(array("NOTER"),$TableName);
			}
		}


	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva movimento");

	## IMMAGINE RIFIUTO
	$sql="SELECT pericoloso, adr, ClassificazioneHP FROM user_schede_rifiuti WHERE ID_RIF='".$FEDIT->DbRecordSet[0]["ID_RIF"]."'";
	$FEDIT->SDBRead($sql,"DbRecordSetImageRif",true,false);
	$ADR		="'".$FEDIT->DbRecordSetImageRif[0]['adr']."'";
	$PERICOLOSO ="'".$FEDIT->DbRecordSetImageRif[0]['pericoloso']."'";
	$ClassificazioneHP =$FEDIT->DbRecordSetImageRif[0]['ClassificazioneHP'];

}
#


echo '<form name="tmp_unique">';
echo '<input type="hidden" value="0" id="isUnique" name="isUnique" />';
echo '</form>';

echo '<input type="hidden" value="'.$ClassificazioneHP.'" id="HiddenField_ClassificazioneHP" name="HiddenField_ClassificazioneHP" />';

require_once("__includes/COMMON_sleepForgEdit.php");
?>

<?php
if(isset($_GET["table"]) &&isset($_GET["pri"]) && isset($_GET["filter"])) {
$js ="<script type=\"text/javascript\">\n";
$js.="if(document.getElementById('".$TableName.":produttore').value==0){";
$js.= <<< ENDJS
//if(document.getElementById('user_movimenti:produttore').value==0){
	CUSTOM_ROSETTE('rosetteD',$ID_ORIGINE_DATI_D);
	CUSTOM_ROSETTE('rosetteT',$ID_ORIGINE_DATI_T);
	CUSTOM_ROSETTE('rosetteI',$ID_ORIGINE_DATI_I);
	}
CUSTOM_IMAGE_RIF($PERICOLOSO,$ADR);
ENDJS;
$js.= "</script>";
echo $js;

} ?>