<?php

$javascript ="<script type='text/javascript'>\n";
$javascript.= <<< JAVASCRIPT

function showMapInterface(canCreate, canAdd){
	
	if(!canAdd) document.MapConfig["action"][1].disabled=true;

	var obj=document.getElementById('MapInterface');
		if(obj.style.display=='none'){
			obj.style.display='block';
			obj.style.zindex=10;
			}
		else{
			obj.style.display='none';
			obj.style.zindex=-1;
			}
	}

function Go2Map(ID_MZ_TRA, DA_INDIRIZZO, DA_COMUNE, DA_PROVINCIA, DA_CAP, DA_NAZIONE, A_INDIRIZZO, A_COMUNE, A_PROVINCIA, A_CAP, A_NAZIONE, PRI, FILTER, ANNO, MATRIX){
	var action=document.MapConfig["hidden_action"].value;
	var ID_VIAGGIO=document.MapConfig["hidden_ID_VIAGGIO"].value;
	var MAPpars="ID_MZ_TRA=" + ID_MZ_TRA;
	MAPpars+="&da_indirizzo=" + DA_INDIRIZZO + "&da_comune=" + DA_COMUNE + "&da_provincia=" + DA_PROVINCIA + "&da_cap=" + DA_CAP + "&da_comune=" + DA_NAZIONE;
	MAPpars+="&a_indirizzo=" + A_INDIRIZZO + "&a_comune=" + A_COMUNE + "&a_provincia=" + A_PROVINCIA + "&a_cap=" + A_CAP + "&a_comune=" + A_NAZIONE;
	MAPpars+="&ID_VIAGGIO=" + ID_VIAGGIO + "&action=" + action + "&" + PRI + "=" + FILTER + "&anno=" + ANNO + "matrix=" + MATRIX;
	document.getElementById('MapInterface').style.display='none';
	//window.alert(MAPpars);
	location.replace('__scripts/statusCUSTOM_MovScaricoMap.php?'+MAPpars);
	}

JAVASCRIPT;
$javascript.="</script>";
echo $javascript;

	// sono in un movimento gi� salvato
	if(isset($_GET['table'])){ 
		$PRI=$_GET['pri'];
		$filter=$_GET['filter'];
		$TableName=$_GET['table'];

		$SQL ="SELECT lov_mezzi_trasporto.ID_MZ_TRA, user_impianti_produttori.description AS da, user_impianti_destinatari.description AS a, ";
		$SQL.="COMda.description comuneDa, COMda.shdes_prov provDa, COMda.CAP CAPDa, COMda.nazione nazioneDa, COMa.description comuneA, COMa.shdes_prov provA, COMa.CAP CAPA, COMa.nazione nazioneA, ID_VIAGGIO, ".$TableName.".ID_AUTO, DTMOV, ID_AUTST, ".$TableName.".ID_UIMP ";
		$SQL.="FROM ".$TableName." ";
		$SQL.="JOIN user_impianti_produttori ON ".$TableName.".ID_UIMP=user_impianti_produttori.ID_UIMP ";
		$SQL.="LEFT JOIN user_impianti_destinatari ON ".$TableName.".ID_UIMD=user_impianti_destinatari.ID_UIMD ";
		$SQL.="JOIN lov_comuni_istat COMda ON COMda.ID_COM=user_impianti_produttori.ID_COM ";
		$SQL.="LEFT JOIN lov_comuni_istat COMa ON COMa.ID_COM=user_impianti_destinatari.ID_COM ";
		$SQL.="LEFT JOIN user_automezzi ON ".$TableName.".ID_AUTO=user_automezzi.ID_AUTO ";
		$SQL.="LEFT JOIN lov_mezzi_trasporto ON user_automezzi.ID_MZ_TRA=lov_mezzi_trasporto.ID_MZ_TRA ";
		$SQL.="WHERE ".$PRI."=".$filter;

		require_once("__includes/COMMON_wakeForgEditDG.php");
		$FEDG->SDBRead($SQL,"DbRecordSetMAP",true,false);

		$ID_MZ_TRA		= $FEDG->DbRecordSetMAP[0]['ID_MZ_TRA'];
		
		$DA_INDIRIZZO	= urlencode($FEDG->DbRecordSetMAP[0]['da']);
		$DA_COMUNE		= urlencode($FEDG->DbRecordSetMAP[0]['comuneDa']);
		$DA_PROVINCIA	= urlencode($FEDG->DbRecordSetMAP[0]['provDa']);
		$DA_CAP			= urlencode($FEDG->DbRecordSetMAP[0]['CAPDa']);
		$DA_NAZIONE		= urlencode($FEDG->DbRecordSetMAP[0]['nazioneDa']);
		
		$A_INDIRIZZO	= urlencode($FEDG->DbRecordSetMAP[0]['a']);
		$A_COMUNE		= urlencode($FEDG->DbRecordSetMAP[0]['comuneA']);
		$A_PROVINCIA	= urlencode($FEDG->DbRecordSetMAP[0]['provA']);
		$A_CAP			= urlencode($FEDG->DbRecordSetMAP[0]['CAPA']);
		$A_NAZIONE		= urlencode($FEDG->DbRecordSetMAP[0]['nazioneA']);

		$ID_VIAGGIO		= $FEDG->DbRecordSetMAP[0]['ID_VIAGGIO'];

		# per univocit� viaggio
		$AUTISTA		= $FEDG->DbRecordSetMAP[0]['ID_AUTST'];
		$PRODUTTORE		= $FEDG->DbRecordSetMAP[0]['ID_UIMP'];
		$AUTOMEZZO		= $FEDG->DbRecordSetMAP[0]['ID_AUTO'];
		$DATA			= $FEDG->DbRecordSetMAP[0]['DTMOV'];
		
		# per selezione DB in modulo mappe
		$ANNO			= substr($_SESSION["DbInUse"], -4);
		$MATRIX			= str_replace($ANNO, '', $_SESSION["DbInUse"]);

		if(!is_null($ID_VIAGGIO)){
			$action = "show";
			$MapUrl = "__scripts/statusCUSTOM_MovScaricoMap.php?ID_MZ_TRA=".$ID_MZ_TRA;
			$MapUrl.= "&da_indirizzo=".$DA_INDIRIZZO."&da_comune=".$DA_COMUNE."&da_provincia=".$DA_PROVINCIA."&da_cap=".$DA_CAP."&da_nazione=".$DA_NAZIONE;
			$MapUrl.= "&a_indirizzo=".$A_INDIRIZZO."&a_comune=".$A_COMUNE."&a_provincia=".$A_PROVINCIA."&a_cap=".$A_CAP."&a_nazione=".$A_NAZIONE;
			$MapUrl.= "&ID_VIAGGIO=".$ID_VIAGGIO."&action=".$action."&".$PRI."=".$filter."&anno=".$ANNO."&matrix=".$MATRIX;
			}
		else{
			if(!isset($ID_MZ_TRA)) $ID_MZ_TRA=2;
//			if(isset($AUTISTA) && !is_null($AUTISTA) && isset($PRODUTTORE) && !is_null($PRODUTTORE) && isset($AUTOMEZZO) && !is_null($AUTOMEZZO)){
//				$SQL="SELECT ID_VIAGGIO, NFORM FROM ".$TableName." WHERE ID_AUTST=".$AUTISTA." AND ID_UIMP=".$PRODUTTORE." AND ID_AUTO=".$AUTOMEZZO." AND DTMOV=".$DATA;
//				$FEDG->SDBRead($SQL,"DbRecordSetVIAGGI",true,false);
//				if($FEDG->DbRecsNum>0) $gotViaggi='true'; else $gotViaggi='false';
//				}
//			else{
//				$gotViaggi='false';
//				}
                        $gotViaggi='false';
			$MapUrl = "javascript:showMapInterface(true, ".$gotViaggi.");";
			}

			$css ="<style type=\"text/css\">\n";
			$css.="div#MapInterface{";
			$css.="position: absolute;";
			$css.="margin-left:-600px;";
			$css.="margin-top:130px;";
			$css.="z-index:10;";
			$css.="height:260px;";
			$css.="padding:3px;";
			$css.="background-color:#ffffff;";
			$css.="}\n";
			$css.="div#ListaViaggi{";
			$css.="padding-left:60px;";
			$css.="padding-top:10px;";
			$css.="}\n";
			$css.="</style>\n";
			
			echo $css;
			
			$MapInterface ="<div id=\"MapInterface\" style=\"display:none;\">\n";
			$MapInterface.="<form id=\"MapConfig\" name=\"MapConfig\" style=\"width:400px;\" action=\"\" method=\"post\">\n";
			$MapInterface.="<fieldset class=\"FGEfieldset\" style=\"width:80%;margin-left:30px;\">\n";
			$MapInterface.="<legend class=\"FGElegend\">VISUALIZZA PERCORSO</legend>\n";
				
				$MapInterface.="<h3>Il movimento selezionato non fa parte di nessun viaggio, come si desidera procedere?</h3>";
				$MapInterface.="<input type=\"radio\" name=\"action\" value=\"create\" checked /> crea un nuovo viaggio<br />\n";
				$MapInterface.="<input type=\"radio\" name=\"action\" value=\"addTappa\" /> aggiungi ad altro viaggio:<br />\n";

				$MapInterface.="<div id=\"ListaViaggi\">\n";

				if($gotViaggi=='true'){
					$MapInterface.="elenco viaggi";
					}
				else
					$MapInterface.="nessun viaggio compatibile rilevato";

				$MapInterface.="</div>\n";

			$MapInterface.="<div class=\"FGESubmitRow\">";
			$MapInterface.="<input type=\"button\" onblur=\"this.style.background='#034373';this.style.color='white';\" onfocus=\"this.style.background='yellow';this.style.color='#034373';\" onclick=\"javascript:Go2Map(".$ID_MZ_TRA.", '".$DA_INDIRIZZO."', '".$DA_COMUNE."', '".$DA_PROVINCIA."', '".$DA_CAP."', '".$DA_NAZIONE."', '".$A_INDIRIZZO."', '".$A_COMUNE."', '".$A_PROVINCIA."', '".$A_CAP."', '".$A_NAZIONE."', '".$PRI."', '".$filter."', '".$ANNO."', '".$MATRIX."');\" value=\"conferma\" class=\"FGEbutton\">";
			$MapInterface.="<input type=\"button\" onblur=\"this.style.background='#034373';this.style.color='white';\" onfocus=\"this.style.background='yellow';this.style.color='#034373';\" onclick=\"javascript:showMapInterface(true, ".$gotViaggi.");\" value=\"annulla\" class=\"FGEbutton\" style=\"margin-left:8px;\">";
			$MapInterface.="</div>";

			$MapInterface.="</fieldset>\n";
			$MapInterface.="<input type=\"hidden\" name=\"hidden_action\" value=\"create\" />\n";
			$MapInterface.="<input type=\"hidden\" name=\"hidden_ID_VIAGGIO\" value=\"0\" />\n";
			$MapInterface.="</form>\n";
			$MapInterface.="</div>\n";
		
			echo $MapInterface;

			require_once("__includes/COMMON_sleepForgEditDG.php");
	}

?>