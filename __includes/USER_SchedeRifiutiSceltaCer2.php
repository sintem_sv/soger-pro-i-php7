<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) { 
	$FEDIT->FGE_FlushTableInfo();
	$FEDIT->FGE_UseTables("lov_cer");
	$FEDIT->FGE_SetSelectFields(array("ID_CER","description","PERICOLOSO"),"lov_cer");
	$FEDIT->FGE_SetFilter("ID_CER",$_GET["CERfilter"],"lov_cer");
	$FEDIT->SDBRead($FEDIT->FGE_SQL_MakeSelect(),"DbRecordSet",true,false);
	$pericoloso  = $FEDIT->DbRecordSet[0]["lov_cerPERICOLOSO"];
	// 18.01.03 max 30 giorni
	if($_GET['CERfilter']=='0001063')
		$TmaxDepTemp = 30;
	else
		$TmaxDepTemp = 90;
	$descrizione = $FEDIT->DbRecordSet[0]["lov_cerdescription"];
	$ID = $FEDIT->DbRecordSet[0]["lov_cerID_CER"];
} else {
	#
	#	DERIVA DISPONIBILITA RIFIUTO
	#
	$IMP_FILTER=true;
	$DISPO_PRINT_OUT = false;
	$IDRIF = $_GET["filter"];
	
	$SQL = "SELECT approved, pericoloso, ClassificazioneHP, user_schede_rifiuti.ID_ONU, lov_num_onu.description AS un, lov_num_onu_imballaggio.description AS imb ";
	$SQL.= "FROM user_schede_rifiuti ";
	$SQL.= "LEFT JOIN lov_num_onu ON user_schede_rifiuti.ID_ONU=lov_num_onu.ID_ONU ";
	$SQL.= "LEFT JOIN lov_num_onu_imballaggio ON user_schede_rifiuti.ID_IMBALLAGGIO_ADR=lov_num_onu_imballaggio.ID_IMBALLAGGIO_ADR ";
	$SQL.= "WHERE ID_RIF=".$IDRIF;
	$FEDIT->SDBRead($SQL,"DbRecordSetPericolo",true,false);

	$is3316 = $FEDIT->DbRecordSetPericolo[0]["un"]==3316;
	$is5_2_2_1_12 = (strpos($FEDIT->DbRecordSet[0]["eti"], '5.2.2.1.12') !== false);
	$pericoloso = $FEDIT->DbRecordSetPericolo[0]["pericoloso"];
	$ClassificazioneHP = $FEDIT->DbRecordSetPericolo[0]["ClassificazioneHP"];
	$approved = $FEDIT->DbRecordSetPericolo[0]["approved"];
	if($SOGER->UserData['core_impiantiREG_IND']=='1'){
		$TableName	= "user_movimenti";
		$IDMov		= "ID_MOV";
		}
	else{
		$TableName	= "user_movimenti_fiscalizzati";
		$IDMov		= "ID_MOV_F";
		}
	$DTMOV		= date("Y-m-d");
	$EXCLUDE_9999999	= true;
	require("__scripts/MovimentiDispoRIF.php");
	}
#
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_schede_rifiuti");
$FEDIT->FGE_SetFormFields(array("approved","ID_IMP","ID_CER","pericoloso","ClassificazioneHP","descrizione","cod_pro","lotto","ID_SPEC"),"user_schede_rifiuti");
if($SOGER->UserData['workmode']=='destinatario')
	$FEDIT->FGE_SetFormFields(array("ID_ASD"),"user_schede_rifiuti");	

$ProfiloFields = "user_schede_rifiuti";
include("SOGER_CampiProfilo.php");

$FEDIT->FGE_SetFormFields(array("ID_SF","peso_spec","ID_UMIS","giac_ini"),"user_schede_rifiuti");

if(isset($_GET["table"]) && isset($_GET["pri"]) && isset($_GET["filter"])) {
	$FEDIT->FGE_SetFormFields(array("FKEdisponibilita"),"user_schede_rifiuti");	
	$FEDIT->FGE_DisableFields("FKEdisponibilita","user_schede_rifiuti");	
	}

if($SOGER->UserData['workmode']=='destinatario')
	$FEDIT->FGE_SetFormFields(array("RifDaManutenzione"),"user_schede_rifiuti");	

$FEDIT->FGE_SetFormFields(array("caratteristiche"),"user_schede_rifiuti");

$FEDIT->FGE_SetFormFields(array("adr","ID_ONU","ID_ETICHETTA_ADR","ID_IMBALLAGGIO_ADR","ADR_2_1_3_5_5","ADR_PERICOLOSO_AMBIENTE","ONU_DES","ADR_NOTE","ONU_per","et_comp_per"),"user_schede_rifiuti");

if(!isset($_GET["table"]) && !isset($_GET["pri"]) && !isset($_GET["filter"])) {
	$FEDIT->FGE_DisableFields(array("ID_ETICHETTA_ADR", "ID_IMBALLAGGIO_ADR"),"user_schede_rifiuti");	
}

$FEDIT->FGE_SetFormFields(array("HP1","HP2","HP3","HP4","HP5","HP6","HP7","HP8","HP9","HP10","HP11","HP12","HP13","HP14","HP15"),"user_schede_rifiuti");


## controllo gestione temporale / volumetrica
if($SOGER->UserData['core_usersID_GDEP']==1)
	$FEDIT->FGE_SetFormFields("t_limite","user_schede_rifiuti");
else	
	$FEDIT->FGE_SetFormFields("q_limite","user_schede_rifiuti");

$FEDIT->FGE_SetFormFields(array("C_S_ID","avv_giorni","avv_quantita","MaxVar"),"user_schede_rifiuti");


## ANNOTAZIONI
$FEDIT->FGE_SetFormFields(array("NOTEF", "NOTER"),"user_schede_rifiuti");

if($SOGER->UserData['core_usersFANGHI']==1){
	$FEDIT->FGE_SetFormFields(array("FANGHI_reg","FANGHI_ss"),"user_schede_rifiuti");
	}

$FEDIT->FGE_DescribeFields();
#
$FEDIT->FGE_DisableFields("pericoloso","user_schede_rifiuti");

$FEDIT->FGE_HideFields(array("approved","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
#

$FEDIT->FGE_SetTitle("ID_CER","Caratteristiche Generali","user_schede_rifiuti");


$FEDIT->FGE_SetTitle("ID_SF","Natura - ProprietÓ","user_schede_rifiuti");
$FEDIT->FGE_SetTitle("adr","Classificazione ADR","user_schede_rifiuti");
$FEDIT->FGE_SetTitle("H1","Caratteristiche di PericolositÓ - Classi H","user_schede_rifiuti");
$FEDIT->FGE_SetTitle("et_comp_per","Caratteristiche di PericolositÓ - Classi HP","user_schede_rifiuti");
if($SOGER->UserData['core_usersID_GDEP']==1)
	$FEDIT->FGE_SetTitle("t_limite","Controlli","user_schede_rifiuti");
else
	$FEDIT->FGE_SetTitle("q_limite","Controlli","user_schede_rifiuti");
$FEDIT->FGE_SetTitle("NOTEF","Annotazioni","user_schede_rifiuti");
$FEDIT->FGE_SetBreak("C_S_ID","user_schede_rifiuti");
$FEDIT->FGE_SetBreak("HP1","user_schede_rifiuti");

if($SOGER->UserData['core_usersFANGHI']==1){
	$FEDIT->FGE_SetTitle("FANGHI_reg","Gestione Fanghi","user_schede_rifiuti");
	}

if($pericoloso=='0'){
	$FEDIT->FGE_DisableFields(array("H1","H2","H3A","H3B","H4","H5","H6","H7","H8","H9","H10","H11","H12","H13","H14","H15"),"user_schede_rifiuti");
	$FEDIT->FGE_DisableFields(array("HP1","HP2","HP3","HP4","HP5","HP6","HP7","HP8","HP9","HP10","HP11","HP12","HP13","HP14","HP15"),"user_schede_rifiuti");
	}

if(!$is5_2_2_1_12)
	$FEDIT->FGE_DisableFields(array("ID_ETICHETTA_ADR"), "user_schede_rifiuti");

if(!$is3316)
	$FEDIT->FGE_DisableFields(array("ID_IMBALLAGGIO_ADR"), "user_schede_rifiuti");


#
if(!isset($_GET["table"]) && !isset($_GET["pri"]) && !isset($_GET["filter"])) { 
	#sistri
	//$FEDIT->FGE_SetValue("idSIS_regCrono",$SOGER->UserData['idSIS_regCrono'],"user_schede_rifiuti");

	$FEDIT->FGE_SetValue("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
	$FEDIT->FGE_SetValue("adr","0","user_schede_rifiuti");
//	$FEDIT->FGE_SetValue("imballaggio","0","user_schede_rifiuti");
	$FEDIT->FGE_SetValue("ADR_2_1_3_5_5","0","user_schede_rifiuti");
	$FEDIT->FGE_SetValue("mud","0","user_schede_rifiuti");
//	$FEDIT->FGE_SetValue("H1","0","user_schede_rifiuti");
//	$FEDIT->FGE_SetValue("H2","0","user_schede_rifiuti");
//	$FEDIT->FGE_SetValue("H3A","0","user_schede_rifiuti");
//	$FEDIT->FGE_SetValue("H3B","0","user_schede_rifiuti");
//	$FEDIT->FGE_SetValue("H4","0","user_schede_rifiuti");
//	$FEDIT->FGE_SetValue("H5","0","user_schede_rifiuti");
//	$FEDIT->FGE_SetValue("H6","0","user_schede_rifiuti");
//	$FEDIT->FGE_SetValue("H7","0","user_schede_rifiuti");
//	$FEDIT->FGE_SetValue("H8","0","user_schede_rifiuti");
//	$FEDIT->FGE_SetValue("H9","0","user_schede_rifiuti");
//	$FEDIT->FGE_SetValue("H10","0","user_schede_rifiuti");
//	$FEDIT->FGE_SetValue("H11","0","user_schede_rifiuti");
//	$FEDIT->FGE_SetValue("H12","0","user_schede_rifiuti");
//	$FEDIT->FGE_SetValue("H13","0","user_schede_rifiuti");
//	$FEDIT->FGE_SetValue("H14","0","user_schede_rifiuti");
//	$FEDIT->FGE_SetValue("H15","0","user_schede_rifiuti");
	if(strtotime(date('Y-m-d'))>=strtotime('2015-06-01'))
		$FEDIT->FGE_SetValue("ClassificazioneHP","1","user_schede_rifiuti");
	else
		$FEDIT->FGE_SetValue("ClassificazioneHP","0","user_schede_rifiuti");
	$FEDIT->FGE_SetValue("HP1","0","user_schede_rifiuti");
	$FEDIT->FGE_SetValue("HP2","0","user_schede_rifiuti");
	$FEDIT->FGE_SetValue("HP3","0","user_schede_rifiuti");
	$FEDIT->FGE_SetValue("HP4","0","user_schede_rifiuti");
	$FEDIT->FGE_SetValue("HP5","0","user_schede_rifiuti");
	$FEDIT->FGE_SetValue("HP6","0","user_schede_rifiuti");
	$FEDIT->FGE_SetValue("HP7","0","user_schede_rifiuti");
	$FEDIT->FGE_SetValue("HP8","0","user_schede_rifiuti");
	$FEDIT->FGE_SetValue("HP9","0","user_schede_rifiuti");
	$FEDIT->FGE_SetValue("HP10","0","user_schede_rifiuti");
	$FEDIT->FGE_SetValue("HP11","0","user_schede_rifiuti");
	$FEDIT->FGE_SetValue("HP12","0","user_schede_rifiuti");
	$FEDIT->FGE_SetValue("HP13","0","user_schede_rifiuti");
	$FEDIT->FGE_SetValue("HP14","0","user_schede_rifiuti");
	$FEDIT->FGE_SetValue("HP15","0","user_schede_rifiuti");
	$FEDIT->FGE_SetValue("C_S_ID",1,"user_schede_rifiuti");
	$FEDIT->FGE_SetValue("descrizione",$descrizione,"user_schede_rifiuti");
	$FEDIT->FGE_SetValue("pericoloso",$pericoloso,"user_schede_rifiuti");
	$FEDIT->FGE_SetValue("t_limite",$TmaxDepTemp,"user_schede_rifiuti");
	if($SOGER->UserData['core_usersFANGHI']==1){
		$FEDIT->FGE_SetValue("FANGHI_reg",0,"user_schede_rifiuti");
		}
	} 
else {
	if($Disponibilita<0) $Disponibilita=0;
	$FEDIT->SDBWrite("UPDATE user_schede_rifiuti SET FKEdisponibilita='$Disponibilita' WHERE ID_RIF='" . $_GET["filter"] . "'",true,false);

	#
	#	blocco campi se movimentati
	#
	if(isset($_GET["Involved"])) {
		//$FEDIT->FGE_DisableFields(array("H1","H2","H3A","H3B","H4","H5","H6","H7","H8","H9","H10","H11","H12","H13","H14","H15"),"user_schede_rifiuti");
		$FEDIT->FGE_DisableFields(array("HP1","HP2","HP3","HP4","HP5","HP6","HP7","HP8","HP9","HP10","HP11","HP12","HP13","HP14","HP15"),"user_schede_rifiuti");
		$FEDIT->FGE_DisableFields(array("ID_CER","ID_SF","ID_UMIS","giac_ini"),"user_schede_rifiuti");
		}
	}
#

$FEDIT->FGE_LookUpDefault("ID_SPEC","user_schede_rifiuti");
if($SOGER->UserData['workmode']=='destinatario'){
	$FEDIT->FGE_LookUpCFG("ID_ASD","user_schede_rifiuti");
	$FEDIT->FGE_UseTables("user_aree_stoccaggio_dest", "user_impianti_destinatari", "user_aziende_destinatari");
	$FEDIT->FGE_SetSelectFields(array("description"),"user_aree_stoccaggio_dest");
	$FEDIT->FGE_SetSelectFields(array("ID_UIMD"),"user_impianti_destinatari"); //-> why?!
	$FEDIT->FGE_HideFields(array("ID_UIMD"),"user_impianti_destinatari");
	$FEDIT->FGE_SetSelectFields(array("ID_IMP"),"user_aziende_destinatari");
	$FEDIT->FGE_HideFields(array("ID_IMP"),"user_aziende_destinatari");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData['core_usersID_IMP'],"user_aziende_destinatari");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_SetOrder("user_aree_stoccaggio_dest:description");
	$FEDIT->FGE_LookUpDone();
	}
$FEDIT->FGE_LookUpDefault("ID_UMIS","user_schede_rifiuti");
$FEDIT->FGE_LookUpDefault("ID_ONU","user_schede_rifiuti",true);

// Gestione etichette per nuove rubriche ADR con nota 5.2.2.1.12
$FEDIT->FGE_LookUpCFG("ID_ETICHETTA_ADR","user_schede_rifiuti");
$FEDIT->FGE_UseTables("lov_num_onu_etichette");
$FEDIT->FGE_SetSelectFields(array("description"),"lov_num_onu_etichette");
$FEDIT->FGE_SetFilter("2_1_3_10","1","lov_num_onu_etichette");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_SetOrder("lov_num_onu_etichette:description");
$FEDIT->FGE_LookUpDone();

// Gestione gruppo imballaggio per UN 3316
$FEDIT->FGE_LookUpCFG("ID_IMBALLAGGIO_ADR","user_schede_rifiuti");
$FEDIT->FGE_UseTables("lov_num_onu_imballaggio");
$FEDIT->FGE_SetSelectFields(array("description"),"lov_num_onu_imballaggio");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_SetOrder("lov_num_onu_imballaggio:description");
$FEDIT->FGE_LookUpDone();

$FEDIT->FGE_LookUpDefault("ID_SF","user_schede_rifiuti");
$FEDIT->FGE_LookUpDefault("C_S_ID","user_schede_rifiuti");
#
if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) { 
	$FEDIT->FGE_SetValue("ID_CER",$ID,"user_schede_rifiuti");
	}
$FEDIT->FGE_LookUpCFG("ID_CER","user_schede_rifiuti",true);
$FEDIT->FGE_UseTables("lov_cer");
$FEDIT->FGE_SetSelectFields(array("ID_CER","COD_CER"),"lov_cer");
if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) { 
	$FEDIT->FGE_SetFilter("ID_CER",$ID,"lov_cer");
	}
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();
#
if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea scheda rifiuto");	
} else {

	if($SOGER->UserData['core_impiantiREG_IND']==0){ 
		$Carico		= "UserNuovoMovCaricoF";
		$Scarico	= "UserNuovoMovScaricoF";
		$Scheda		= "UserNuovaSchedaSistriF";
		}
	else{
		$Carico		= "UserNuovoMovCarico";
		$Scarico	= "UserNuovoMovScarico";
		$Scheda		= "UserNuovaSchedaSistri";
		}

	# firefox
	$css.="<style type=\"text/css\">\n";
	$css.="div#CreaCaricoFromRif{\n";
	$css.="position: absolute; top:1323px; left: 220px; z-index: 10;";
	$css.="}\n";
	$css.="div#CreaScaricoFromRif{\n";
	$css.="position: absolute; top:1323px; left: 322px; z-index: 10;";
	$css.="}\n";
	$css.="div#CreaSchedaFromRif{\n";
	$css.="position: absolute; top:1323px; left: 455px; z-index: 10;";
	$css.="}\n";
	$css.="</style>\n";

	# ie
	$css.="<!--[if IE]>";
	$css.="<style type=\"text/css\">\n";
	$css.="div#CreaCaricoFromRif{\n";
	$css.="position:relative;margin-left:30px;margin-top:-43px;z-index: 10;";
	$css.="}\n";
	$css.="div#CreaScaricoFromRif{\n";
	$css.="position:absolute;margin-left:30px;margin-top:-43px;z-index: 10;";
	$css.="}\n";
	$css.="div#CreaSchedaFromRif{\n";
	$css.="position:absolute;margin-left:37px;margin-top:-43px;z-index: 10;";
	$css.="}\n";
	$css.="</style>\n";
	$css.="<![endif]-->";

	echo $css;

	# crea carico

	$div_carico = "<div id=\"CreaCaricoFromRif\">\n";
	$div_carico.= "<input type=\"button\" onblur=\"this.style.background='#034373';this.style.color='white';\" onfocus=\"this.style.background='yellow';this.style.color='#034373';\" onclick=\"if(".$approved."=='1' && (".$ClassificazioneHP."==1 || ".$pericoloso."==0 || !Regolamento1357Attivo() )){location.replace('__scripts/status.php?area=".$Carico."&amp;ID_RIF=".$_GET["filter"]."');}else{alert('Impossibile movimentare un rifiuto non approvato o non riclassificato secondo il Regolamento UE 1357/2014');}\" value=\"crea carico\" class=\"FGEbutton\" id=\"\">\n";
	$div_carico.= "</div>\n";

	# crea scarico
	$div_scarico = "<div id=\"CreaScaricoFromRif\">\n";
	$div_scarico.= "<input type=\"button\" onblur=\"this.style.background='#034373';this.style.color='white';\" onfocus=\"this.style.background='yellow';this.style.color='#034373';\" onclick=\"if(".$approved."=='1' && (".$ClassificazioneHP."==1 || ".$pericoloso."==0 || !Regolamento1357Attivo() )){location.replace('__scripts/status.php?area=".$Scarico."&amp;ID_RIF=".$_GET["filter"]."');}else{alert('Impossibile movimentare un rifiuto non approvato o non riclassificato secondo il Regolamento UE 1357/2014');}\" value=\"crea scarico / fir\" class=\"FGEbutton\" id=\"\">\n";
	$div_scarico.= "</div>\n";

	if($pericoloso==0)
		$style = " style=\"display:none;\" ";
	else 
		$style = " style=\"display:block;\" ";
	# crea scheda
	$div_scheda = "<div id=\"CreaSchedaFromRif\" ".$style.">\n";
	$div_scheda.= "<input type=\"button\" onblur=\"this.style.background='#034373';this.style.color='white';\" onfocus=\"this.style.background='yellow';this.style.color='#034373';\" onclick=\"if(".$approved."=='1' && (".$ClassificazioneHP."==1 || ".$pericoloso."==0 || !Regolamento1357Attivo() )){location.replace('__scripts/status.php?area=".$Scheda."&amp;ID_RIF=".$_GET["filter"]."');}else{alert('Impossibile movimentare un rifiuto non approvato o non riclassificato secondo il Regolamento UE 1357/2014');}\" value=\"crea scheda sistri\" class=\"FGEbutton\" id=\"\">\n";
	$div_scheda.= "</div>\n";

	echo $div_carico;
	echo $div_scarico;
	echo $div_scheda;

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva scheda rifiuto");
}
require_once("__includes/COMMON_sleepForgEdit.php");
?>
