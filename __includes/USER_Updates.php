<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
#
$FEDIT->FGE_UseTables(array("core_dbversion"));
$FEDIT->FGE_SetSelectFields(array("version","installDate","description"),"core_dbversion");
$FEDIT->FGE_DescribeFields();
#
echo($FEDIT->FGE_DataGrid($SOGER->AppDescriptiveLocation,"__scripts/FGE_DataGridEdit.php","-----"));
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>
