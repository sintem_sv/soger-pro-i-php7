<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;


$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_schede_rifiuti_pittogrammi","user_schede_rifiuti");
$FEDIT->FGE_SetFormFields(array("ID_RIF","ID_IMP","HP_CLASSES","GHS01","GHS02","GHS03","GHS04","GHS05","GHS06","GHS07","GHS08","GHS09","M"),"user_schede_rifiuti_pittogrammi");

if(!isset($_GET["pri"]) && !isset($_GET["filter"])) {

	$sql = "SELECT ID_SF, pericoloso, HP1, HP2, HP3, HP4, HP5, HP6, HP7, HP8, HP9, HP10, HP11, HP12, HP13, HP14, HP15, CONCAT( IF( HP1 = 1 , 'HP1 ' , '' ) , IF( HP2 = 1 , 'HP2 ' , '' ) , IF( HP3 = 1 , 'HP3 ' , '' ), IF( HP4 = 1 , 'HP4 ' , '' ) , IF( HP5 = 1 , 'HP5 ' , '' ) , IF( HP6 = 1 , 'HP6 ' , '' ) , IF( HP7 = 1 , 'HP7 ' , '' ) , IF( HP8 = 1 , 'HP8 ' , '' ) , IF( HP9 = 1 , 'HP9 ' , '' ) , IF( HP10 = 1 , 'HP10 ' , '' ) , IF( HP11 = 1 , 'HP11 ' , '' ) , IF( HP12 = 1 , 'HP12 ' , '' ) , IF( HP13 = 1 , 'HP13 ' , '' ) , IF( HP14 = 1 , 'HP14 ' , '' ) , IF( HP15 = 1 , 'HP15 ' , '' ) ) AS HP_CLASSES FROM user_schede_rifiuti WHERE ID_RIF='".$_GET['ID_RIF']."';";
	$FEDIT->SdbRead($sql,"DbRecordSetInfoRif",true,false);

	$FEDIT->FGE_DescribeFields();

	$FEDIT->FGE_LookUpDefault("ID_IMP","user_schede_rifiuti_pittogrammi");

	#
	$FEDIT->FGE_LookUpCFG("ID_RIF","user_schede_rifiuti_pittogrammi");
	$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("ID_RIF","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
	$ProfiloFields = "user_schede_rifiuti";
	include("SOGER_FiltriProfilo.php");
	$FEDIT->FGE_SetOrder("lov_cer:COD_CER");
	$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#

	if($FEDIT->DbRecordSetInfoRif[0]["HP1"]=="1")
		$FEDIT->FGE_SetValue("GHS01","1","user_schede_rifiuti_pittogrammi");
	else
		$FEDIT->FGE_SetValue("GHS01","0","user_schede_rifiuti_pittogrammi");

	if($FEDIT->DbRecordSetInfoRif[0]["HP2"]=="1")
		$FEDIT->FGE_SetValue("GHS03","1","user_schede_rifiuti_pittogrammi");
	else
		$FEDIT->FGE_SetValue("GHS03","0","user_schede_rifiuti_pittogrammi");

	if($FEDIT->DbRecordSetInfoRif[0]["HP3"]=="1")
		$FEDIT->FGE_SetValue("GHS02","1","user_schede_rifiuti_pittogrammi");
	else
		$FEDIT->FGE_SetValue("GHS02","0","user_schede_rifiuti_pittogrammi");

	if($FEDIT->DbRecordSetInfoRif[0]["HP9"]=="1")
		$FEDIT->FGE_SetValue("M","1","user_schede_rifiuti_pittogrammi");
	else
		$FEDIT->FGE_SetValue("M","0","user_schede_rifiuti_pittogrammi");

	if($FEDIT->DbRecordSetInfoRif[0]["HP14"]=="1")
		$FEDIT->FGE_SetValue("GHS09","1","user_schede_rifiuti_pittogrammi");
	else
		$FEDIT->FGE_SetValue("GHS09","0","user_schede_rifiuti_pittogrammi");

	$FEDIT->FGE_SetValue("GHS04","0","user_schede_rifiuti_pittogrammi");
	$FEDIT->FGE_SetValue("GHS05","0","user_schede_rifiuti_pittogrammi");
	$FEDIT->FGE_SetValue("GHS06","0","user_schede_rifiuti_pittogrammi");
	$FEDIT->FGE_SetValue("GHS07","0","user_schede_rifiuti_pittogrammi");
	$FEDIT->FGE_SetValue("GHS08","0","user_schede_rifiuti_pittogrammi");

	$FEDIT->FGE_SetValue("ID_RIF",$_GET['ID_RIF'],"user_schede_rifiuti_pittogrammi");
	$FEDIT->FGE_SetValue("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti_pittogrammi");
        
    $FEDIT->FGE_SetValue("HP_CLASSES",$FEDIT->DbRecordSetInfoRif[0]["HP_CLASSES"],"user_schede_rifiuti_pittogrammi");
	
	$FEDIT->FGE_HideFields(array("ID_IMP"),"user_schede_rifiuti_pittogrammi");
	$FEDIT->FGE_DisableFields(array("ID_RIF", "HP_CLASSES"),"user_schede_rifiuti_pittogrammi");

	$FEDIT->FGE_SetTitle("ID_RIF","Scheda rifiuto","user_schede_rifiuti_pittogrammi");
	$FEDIT->FGE_SetTitle("GHS01","Pittogrammi di pericolo","user_schede_rifiuti_pittogrammi");

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"salva segnaletica");

} else {

	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_IMP","user_schede_rifiuti_pittogrammi");

	#
	$FEDIT->FGE_LookUpCFG("ID_RIF","user_schede_rifiuti_pittogrammi");
	$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("ID_RIF","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
	$ProfiloFields = "user_schede_rifiuti";
	include("SOGER_FiltriProfilo.php");
	$FEDIT->FGE_SetOrder("lov_cer:COD_CER");
	$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#


	$FEDIT->FGE_HideFields(array("ID_IMP"),"user_schede_rifiuti_pittogrammi");
	$FEDIT->FGE_DisableFields(array("ID_RIF", "HP_CLASSES"),"user_schede_rifiuti_pittogrammi");

	$FEDIT->FGE_SetTitle("ID_RIF","Scheda rifiuto","user_schede_rifiuti_pittogrammi");
	$FEDIT->FGE_SetTitle("GHS01","Pittogrammi di pericolo","user_schede_rifiuti_pittogrammi");
        
    echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva segnaletica");

	}


	# firefox
	$css.="<style type=\"text/css\">\n";
	$css.="div#LegendaPittogrammi{\n";
	$css.="position: absolute; top:419px; left: 220px; z-index: 10;";
	$css.="}\n";
	$css.="</style>\n";

	# ie
	$css.="<!--[if IE]>";
	$css.="<style type=\"text/css\">\n";
	$css.="div#LegendaPittogrammi{\n";
	$css.="position:relative;margin-left:30px;margin-top:-455px;z-index: 10;";
	$css.="}\n";
	$css.="</style>\n";
	$css.="<![endif]-->";

	echo $css;

	# HIDDEN DIALOG BOX - LEGENDA
	$tmp.="<div id='Popup_LegendaPittogrammi' title='Pittogrammi GHS' style='display:none;'>";
	$tmp.="<img src=\"__scripts/etichette/GHS/legenda.gif\" />";
	$tmp.="</div>";	
	echo $tmp;

	# Legenda
	echo "<div id=\"LegendaPittogrammi\">\n";
	echo "<input id=\"ShowLegenda\" type=\"button\" onblur=\"this.style.background='#034373';this.style.color='white';\" onfocus=\"this.style.background='yellow';this.style.color='#034373';\" onclick=\"\" value=\"Visualizza legenda\" class=\"FGEbutton\" id=\"\">\n";
	echo "</div>\n";



require_once("__includes/COMMON_sleepForgEdit.php");
?>
