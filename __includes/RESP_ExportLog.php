<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
require_once("__scripts/STATS_funct.php");

#
global $SOGER;

if(isset($_POST['database'])){
	$CSVbuf = AddCSVcolumn("TITOLO");
	$CSVbuf .= AddCSVRow();
	$CSVbuf .= AddCSVRow();

	$FEDIT->DbServerData["db"] = $_POST['database'];
	$sql = "SELECT DATE_FORMAT( TMS, 'Il giorno %d/%m/%Y alle %T' ) AS QUANDO , core_logs.table AS TABELLA, pri_key AS ID, core_users.usr AS USERNAME, sqlType AS OPERAZIONE, `sqlCommand` AS LOG_SQL ";
	$sql.= "FROM core_logs ";
	$sql.= "JOIN core_users ON core_users.ID_USR = core_logs.ID_USR ";
	$sql.= "WHERE core_logs.ID_IMP = '".$SOGER->UserData['core_usersID_IMP']."' ";
	$sql.= "AND core_logs.ID_USR IN (".implode(", ",$_POST['users']).") ";
	$sql.= "AND core_logs.table IN ('".implode("', '",$_POST['tables'])."') ";
	if($_POST['data_da']!=''){
		$data_da_array = explode("/", $_POST['data_da']);
		$data_da = $data_da_array[2]."-".$data_da_array[1]."-".$data_da_array[0];
		$sql.= "AND TMS>='".$data_da."' ";
		}
	if($_POST['data_a']!=''){
		$data_a_array = explode("/", $_POST['data_a']);
		$data_a = $data_a_array[2]."-".$data_a_array[1]."-".$data_a_array[0];
		$sql.= "AND TMS<='".$data_a."' ";
		}
	$FEDIT->SDBRead($sql,"DbRecordsetLOG");
	$FEDIT->DbServerData["db"] = 'soger'.date('Y');

	$CSVbuf.= AddCSVcolumn("QUANDO");
	$CSVbuf.= AddCSVcolumn("USERNAME");
	$CSVbuf.= AddCSVcolumn("OPERAZIONE");
	$CSVbuf.= AddCSVcolumn("TABELLA");
	$CSVbuf.= AddCSVcolumn("ID");
	$CSVbuf.= AddCSVcolumn("LOG_SQL");
	$CSVbuf.= AddCSVRow();


	for($l=0;$l<count($FEDIT->DbRecordsetLOG);$l++){
		$CSVbuf.= AddCSVcolumn($FEDIT->DbRecordsetLOG[$l]['QUANDO']);
		$CSVbuf.= AddCSVcolumn($FEDIT->DbRecordsetLOG[$l]['USERNAME']);
		$CSVbuf.= AddCSVcolumn($FEDIT->DbRecordsetLOG[$l]['OPERAZIONE']);
		$CSVbuf.= AddCSVcolumn($FEDIT->DbRecordsetLOG[$l]['TABELLA']);
		$CSVbuf.= AddCSVcolumn($FEDIT->DbRecordsetLOG[$l]['ID']);
		$LOG	= str_replace(array("\n", "\t", "\r"), '', $FEDIT->DbRecordsetLOG[$l]['LOG_SQL']);
		$CSVbuf.= AddCSVcolumn($LOG);
		$CSVbuf.= AddCSVRow();
		}

	//var_dump($CSVbuf);

	CSVOut($CSVbuf,"SO.Ge.R. Pro LOG");		
	}

echo "<script type=\"text/javascript\">";
echo "function checkForm(form) {\n";
echo "// regular expression to match required date format\n";
echo "re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;\n";
echo "if( (form.data_da.value != '' && !form.data_da.value.match(re)) || (form.data_a.value != '' && !form.data_a.value.match(re)) ) {\n";
echo "alert(\"Il formato delle date non e' valido (usare gg/mm/aaaa)!\");\n";
echo "return false;\n";
echo "}\n";
echo "var users_checkboxs=document.getElementsByName(\"users[]\");\n";
echo "var users_selected=false;\n";
echo "var tables_checkboxs=document.getElementsByName(\"tables[]\");\n";
echo "var tables_selected=false;\n";
echo "for(var i=0,l=users_checkboxs.length;i<l;i++){\n";
echo "if(users_checkboxs[i].checked){\n";
echo "users_selected=true;\n";
echo "}\n";
echo "}\n";
echo "for(var i=0,l=tables_checkboxs.length;i<l;i++){\n";
echo "if(tables_checkboxs[i].checked){\n";
echo "tables_selected=true;\n";
echo "}\n";
echo "}\n";
echo "if(!users_selected || !tables_selected){\n";
echo "alert(\"Almeno un utente e una tabella devono essere selezionati!\");\n";
echo "return false;\n";
echo "}\n";
echo "form.submit();\n";
echo "}\n";
echo "function check_all(field,campi) {\n";
echo "for (i = 0; i < field.elements.length; i++){\n";
echo "if(field.elements[i].type==\"checkbox\"){\n";
echo "if(field.elements[i].name.indexOf(campi)!=-1){\n";
echo "field.elements[i].checked = !field.elements[i].checked;\n";
echo "}";
echo "}";
echo "}";
echo "}";

echo "</script>\n";


echo "<div class=\"FGEDataGridTitle\"><div>$SOGER->AppDescriptiveLocation</div></div>";
if(count($SOGER->AppNavDIVs)>0) {
	$tmp .= "<div id=\"NavDiv\">\n";
	$tmp .= "<!-- NAV DIVs BEGIN -->\n[NAV_STUCTURE]\n<!-- NAV DIVs END -->\n";
	$SOGER->DIVmake_Navigation();
	$tmp .= "</div>\n";
	echo $tmp;
	}

## DATABASE
$sql = "SHOW databases";
$FEDIT->SDBRead($sql,"DbRecordsetDB");

## UTENTI
$sql = "SELECT ID_USR, usr, nome, cognome FROM core_users WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND sadmin=0 AND sresponsabile=0 AND usr<>'sintem' ORDER BY cognome ASC;";
$FEDIT->SDBRead($sql,"DbRecordsetUSR");

## TABELLE
$sql = "SELECT table_name, table_comment FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = 'soger".date("Y")."' AND table_name LIKE 'user_%' AND table_comment IS NOT NULL AND table_comment<>'' ORDER BY table_comment ASC;";
$FEDIT->SDBRead($sql,"DbRecordsetTAB");
$DbRecordsetTAB = $FEDIT->DbRecordsetTAB;
array_push($DbRecordsetTAB, array('table_name' => 'login-logout', 'table_comment' => '<b>Accessi al Sistema</b>'));

$colonne	= 4;
$colspan	= $colonne-1;
$tabelle	= count($DbRecordsetTAB);
$righe		= ceil($tabelle/$colonne);
$tabella	= 0;

echo "<div><form name='FormLOG' action='".$_SERVER['REQUEST_URI']."' method='POST'>";

echo "<table class=\"ExportLog\" cellpadding='2' cellspacing='0'>\n";
echo "<tr>";
	echo "<th>Database</th>";
	echo "<td colspan=\"".$colspan."\"><select name=\"database\">";
	foreach($FEDIT->DbRecordsetDB as $k=>$Db) {
		if(strpos($Db["Database"],"soger2")!==false) {
			echo "<option value=\"" . $Db["Database"] . "\">" . $Db["Database"] . "</option>";	
		}
	}
	echo "</td>";
echo "</tr>";
echo "<tr>";
	echo "<th>Data inizio</th>";
	echo "<td style=\"text-align:center;\"><input style=\"text-align:right;\" type=\"text\" id=\"data_da\" name=\"data_da\" value=\"\" /></td>";
	echo "<th>Data fine</th>";
	echo "<td style=\"text-align:center;\"><input style=\"text-align:right;\" type=\"text\" id=\"data_a\" name=\"data_a\" value=\"\" /></td>";
echo "</tr>";
echo "<tr>";
	echo "<th>Utenti (<a style=\"color:#fff;\" href=\"javascript:check_all(document.FormLOG,'users');\">inverti selezione</a>)</th>";
	echo "<td colspan=\"".$colspan."\">";
	foreach($FEDIT->DbRecordsetUSR as $k=>$Usr)
		echo "<input type=\"checkbox\" name=\"users[]\" value=\"".$Usr['ID_USR']."\"> ".$Usr['cognome']." ".$Usr['nome']." <i>(username: ".$Usr['usr'].")</i><br />";
	echo "</td>";
echo "</tr>";

echo "<tr><th colspan=\"".$colonne."\">Tabelle (<a style=\"color:#fff;\" href=\"javascript:check_all(document.FormLOG,'tables');\">inverti selezione</a>)</th></tr>";

for($r=0;$r<$righe;$r++){
	echo "<tr>";
	for($c=0;$c<$colonne;$c++){
		if($tabella<$tabelle)
			echo "<td><input type=\"checkbox\" name=\"tables[]\" value=\"".$DbRecordsetTAB[$tabella]['table_name']."\" > ".$DbRecordsetTAB[$tabella]['table_comment']."</td>";

		else
			echo "<td>&nbsp;</td>";
		$tabella++;
		}
	echo "</tr>";
	}

echo "<tr><th colspan=\"".$colonne."\" style=\"text-align:center;border-bottom:none;\"><input type=\"button\" value=\"Esporta LOG\" name=\"Esporta_LOG\" id=\"Esporta_LOG\" onClick=\"javascript:checkForm(FormLOG);\"></th></tr>";

echo "</table>";

echo "</form></div>";

require_once("__includes/COMMON_sleepForgEdit.php");
?>