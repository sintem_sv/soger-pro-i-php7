<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_schede_sicurezza","user_schede_rifiuti","lov_cer");
//$FEDIT->FGE_SetSelectFields(array("data_compilazione","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_sicurezza");
$FEDIT->FGE_SetSelectFields(array("data_compilazione","produttore","trasportatore","destinatario","intermediario"),"user_schede_sicurezza");
$FEDIT->FGE_SetSelectFields(array("ID_IMP","descrizione"),"user_schede_rifiuti");
$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
$FEDIT->FGE_DescribeFields();
#
$ProfiloFields = "user_schede_sicurezza";
include("SOGER_FiltriProfilo.php");
$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
#
$FEDIT->FGE_HideFields(array("ID_IMP"),"user_schede_rifiuti");
//$FEDIT->FGE_HideFields(array("produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_sicurezza");
$FEDIT->FGE_HideFields(array("produttore","trasportatore","destinatario","intermediario"),"user_schede_sicurezza");
#
echo $FEDIT->SG_SchedeSicGrid($SOGER->AppDescriptiveLocation);			
require_once("__includes/COMMON_sleepForgEdit.php");
?>
<div class="FGE4Col">
<input type="button" name="nuovo" class="FGEbutton" value="crea nuova procedura di sicurezza" onclick="javascript: document.location='__scripts/new_scheda_sicurezza.php'"/>
</div>
