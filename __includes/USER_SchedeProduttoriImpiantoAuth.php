<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_autorizzazioni_pro");
$FEDIT->FGE_SetFormFields(array("ID_RIF","ID_UIMP","ID_AUT","num_aut","PREV_num_aut","rilascio","scadenza","note","inherit_edits"),"user_autorizzazioni_pro");

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {
	#
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_AUT","user_autorizzazioni_pro");
	#
	$FEDIT->FGE_LookUpCFG("ID_UIMP","user_autorizzazioni_pro");
	$FEDIT->FGE_UseTables("user_impianti_produttori");
	$FEDIT->FGE_SetSelectFields(array("description","ID_AZP"),"user_impianti_produttori");
	$FEDIT->FGE_SetFilter("ID_AZP",$_SESSION["FGE_IDs"]["user_aziende_produttori"],"user_impianti_produttori");
	$FEDIT->FGE_HideFields("ID_AZP","user_impianti_produttori");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#
	$FEDIT->FGE_LookUpCFG("ID_RIF","user_autorizzazioni_pro");
	$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
	//$FEDIT->FGE_SetSelectFields(array("cod_pro","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("cod_pro","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
	$ProfiloFields = "user_schede_rifiuti";
	include("SOGER_FiltriProfilo.php");
	//$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_SetOrder("lov_cer:COD_CER");
	$FEDIT->FGE_LookUpDone();
	#
	$FEDIT->FGE_HideFields(array("inherit_edits","PREV_num_aut"),"user_autorizzazioni_pro");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea autorizzazione");
} else {
	if(isset($_GET["Involved"])) {
		$FEDIT->FGE_DisableFields(array("ID_RIF","ID_UIMP"),"user_autorizzazioni_pro");	
	}
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_AUT","user_autorizzazioni_pro");
	#
	$FEDIT->FGE_LookUpCFG("ID_UIMP","user_autorizzazioni_pro");
	$FEDIT->FGE_UseTables("user_impianti_produttori");
	$FEDIT->FGE_SetSelectFields(array("description","ID_AZP"),"user_impianti_produttori");
	$FEDIT->FGE_SetFilter("ID_AZP",$_SESSION["FGE_IDs"]["user_aziende_produttori"],"user_impianti_produttori");
	$FEDIT->FGE_HideFields("ID_AZP","user_impianti_produttori");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#
	$FEDIT->FGE_LookUpCFG("ID_RIF","user_autorizzazioni_pro");
	$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
	//$FEDIT->FGE_SetSelectFields(array("cod_pro","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("cod_pro","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
	$ProfiloFields = "user_schede_rifiuti";
	include("SOGER_FiltriProfilo.php");
	$ProfiloFields = "user_schede_rifiuti";
	include("SOGER_FiltriProfilo.php");
	//$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_rifiuti");
	$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_SetOrder("lov_cer:COD_CER");
	$FEDIT->FGE_LookUpDone();
	#
	$FEDIT->FGE_HideFields(array("inherit_edits","PREV_num_aut"),"user_autorizzazioni_pro");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva autorizzazione");
}
require_once("__includes/COMMON_sleepForgEdit.php");
?>
