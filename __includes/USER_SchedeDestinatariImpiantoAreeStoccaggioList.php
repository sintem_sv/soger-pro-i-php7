<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEditDG.php");
#
global $SOGER;
$FEDG->FGE_FlushTableInfo();
$FEDG->FGE_UseTables(array("user_aree_stoccaggio_dest","user_impianti_destinatari","user_aziende_destinatari"));
$FEDG->FGE_SetSelectFields(array("description"),"user_impianti_destinatari");
$FEDG->FGE_SetSelectFields(array("description", "max_stock"),"user_aree_stoccaggio_dest");
$FEDG->FGE_DescribeFields();

#
$FEDG->FGE_SetFilter("ID_AZD",$_SESSION["FGE_IDs"]["user_aziende_destinatari"],"user_aziende_destinatari");

echo($FEDG->FGE_DataGrid("Aree di stoccaggio","__scripts/FGE_DataGridEdit.php","EDC--",true,"FEDG",$_SESSION['SearchingFilter']));
#
require_once("__includes/COMMON_sleepForgEditDG.php");
?>
<div class="FGE4Col">
<input type="button" name="nuovo" class="FGEbutton" value="crea nuova area di stoccaggio" onclick="javascript: document.location='<?php echo $_SERVER["PHP_SELF"]?>'"/>
</div>

