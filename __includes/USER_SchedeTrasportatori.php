<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_aziende_trasportatori");
$FEDIT->FGE_SetFormFields(array("ID_IMP","description","piva","codfisc","indirizzo","ID_COM","IN_ITALIA","esenzione_contributo","contributo","approved"),"user_aziende_trasportatori");
//$FEDIT->FGE_SetFormFields(array("idSIS_regCrono"),"user_aziende_trasportatori");
$FEDIT->FGE_SetFormFields(array("NumAlboAutotraspProprio","NumAlboAutotrasp"),"user_aziende_trasportatori");
$FEDIT->FGE_HideFields(array("IN_ITALIA"),"user_aziende_trasportatori");	
//$FEDIT->FGE_HideFields(array("idSIS_regCrono"),"user_aziende_trasportatori");

$FEDIT->FGE_SetTitle("description","Dati societari","user_aziende_trasportatori");
$FEDIT->FGE_SetTitle("indirizzo","Sede legale","user_aziende_trasportatori");
$FEDIT->FGE_SetTitle("esenzione_contributo","Contributo annuale Albo Nazionale Gestori Ambientali","user_aziende_trasportatori");
$FEDIT->FGE_SetTitle("NumAlboAutotraspProprio","Iscrizione Albo Autotrasportatori","user_aziende_trasportatori");
$FEDIT->FGE_SetBreak("piva","user_aziende_trasportatori");

$ProfiloFields = "user_aziende_trasportatori";
include("SOGER_CampiProfilo.php");

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {

	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookupDefault("ID_COM","user_aziende_trasportatori",true);
	$FEDIT->FGE_SetValue("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_aziende_trasportatori");
	//$FEDIT->FGE_SetValue("idSIS_regCrono",$SOGER->UserData['idSIS_regCrono'],"user_aziende_trasportatori");
	$FEDIT->FGE_HideFields(array("ID_IMP","produttore","trasportatore","destinatario","intermediario","approved"),"user_aziende_trasportatori");
	if(dateDiff("d",date("m/d/Y"),"06/30/".date("Y"))<0) {
		$data = (date("Y") + 1) . "-6-30";
	} else {
		$data = date("Y") . "-06-30";	
	}
	$FEDIT->FGE_SetValue("contributo",$data,"user_aziende_trasportatori");
	$FEDIT->FGE_SetValue("approved","1","user_aziende_trasportatori");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea trasportatore");
	
} else {
	//if(isset($_GET["Involved"])) {
	//	$FEDIT->FGE_DisableFields(array("description","piva","codfisc"),"user_aziende_trasportatori");	
	//}	
	$_SESSION["FGE_IDs"]["user_aziende_trasportatori"] = $_GET["filter"];
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookupDefault("ID_COM","user_aziende_trasportatori",true);
	$FEDIT->FGE_HideFields(array("ID_IMP","produttore","trasportatore","destinatario","intermediario","approved"),"user_aziende_trasportatori");	
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva trasportatore");
}

	#
require_once("__includes/COMMON_sleepForgEdit.php");
?>
