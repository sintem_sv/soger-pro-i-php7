<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_contratti_intermediari","user_schede_rifiuti");
$FEDIT->FGE_SetFormFields(array("description","ID_AZI","ID_RIF","DT_INIZIO","DT_SCADENZA"),"user_contratti_intermediari");

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {

	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_SetValue("description","","user_contratti_intermediari");
	//$FEDIT->FGE_SetValue("DT_INIZIO",date("d-m-Y"),"user_contratti_intermediari");
	$FEDIT->FGE_SetValue("ID_AZI",$_SESSION['ID_AZI_CONTRACT'],"user_contratti_intermediari");
	$FEDIT->FGE_HideFields(array("ID_AZI"),"user_contratti_intermediari");
	
	#
	$FEDIT->FGE_LookUpCFG("ID_RIF","user_contratti_intermediari");
	$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");	//$FEDIT->FGE_SetSelectFields(array("descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
	$ProfiloFields = "user_schede_rifiuti";
	include("SOGER_FiltriProfilo.php");
	$FEDIT->FGE_SetOrder("lov_cer:COD_CER");
	//$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","approved"),"user_schede_rifiuti");
	#


	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea contratto intermediario");

} else {

	$FEDIT->FGE_DescribeFields();
	
	$FEDIT->FGE_HideFields(array("ID_AZI"),"user_contratti_intermediari");
	$FEDIT->FGE_LookUpCFG("ID_RIF","user_contratti_intermediari");
	$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");	//$FEDIT->FGE_SetSelectFields(array("descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
	$ProfiloFields = "user_schede_rifiuti";
	include("SOGER_FiltriProfilo.php");
	$FEDIT->FGE_SetOrder("lov_cer:COD_CER");
	//$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","approved"),"user_schede_rifiuti");
	
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica contratto intermediario");
}
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>