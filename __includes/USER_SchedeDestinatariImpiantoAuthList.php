<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEditDG.php");
#
global $SOGER;
$FEDG->FGE_FlushTableInfo();
$FEDG->FGE_UseTables(array("user_autorizzazioni_dest","user_impianti_destinatari","user_aziende_destinatari","user_schede_rifiuti"));
$FEDG->FGE_SetSelectFields(array("description"),"user_impianti_destinatari");
$FEDG->FGE_SetSelectFields(array("ID_RIF","approved"),"user_schede_rifiuti");
$FEDG->FGE_HideFields(array("approved"),"user_schede_rifiuti");
$FEDG->FGE_SetSelectFields(array("ID_RIF","ID_AUT","num_aut","rilascio","scadenza","ID_TRAT","ID_OP_RS","ID_OP_RS2","ID_ORIGINE_DATI","approved"),"user_autorizzazioni_dest");
$FEDG->FGE_HideFields("ID_ORIGINE_DATI","user_autorizzazioni_dest");
$FEDG->FGE_DescribeFields();
$FEDG->FGE_SetOrder("user_autorizzazioni_dest:ID_RIF");
#
$FEDG->FGE_LookUpDefault("ID_AUT","user_autorizzazioni_dest");
$FEDG->FGE_LookUpDefault("ID_TRAT","user_autorizzazioni_dest");
$FEDG->FGE_LookUpDefault("ID_OP_RS","user_autorizzazioni_dest");
$FEDG->FGE_LookUpDefault("ID_OP_RS2","user_autorizzazioni_dest");
#
$FEDG->FGE_LookUpCFG("ID_RIF","user_autorizzazioni_dest");
$FEDG->FGE_UseTables("lov_cer","user_schede_rifiuti");
$FEDG->FGE_SetSelectFields(array("cod_pro","descrizione","ID_CER","ID_IMP"),"user_schede_rifiuti");
$FEDG->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
$FEDG->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
$FEDG->FGE_HideFields(array("ID_CER","ID_IMP"),"user_schede_rifiuti");
$FEDG->FGE_LookUpDescribe();
$FEDG->FGE_LookUpDone();
#
$FEDG->FGE_SetFilter("ID_AZD",$_SESSION["FGE_IDs"]["user_aziende_destinatari"],"user_aziende_destinatari");
$FEDG->FGE_HideFields(array("approved"),"user_autorizzazioni_dest");

echo($FEDG->FGE_DataGrid("Numeri Albo in Archivio","__scripts/FGE_DataGridEdit.php","EDC-T",true,"FEDG",$_SESSION['SearchingFilter']));
#
require_once("__includes/COMMON_sleepForgEditDG.php");
?>
<div class="FGE4Col">
<input type="button" name="nuovo" class="FGEbutton" value="crea nuova autorizzazione" onclick="javascript: document.location='<?php echo $_SERVER["PHP_SELF"]?>'"/>
</div>

