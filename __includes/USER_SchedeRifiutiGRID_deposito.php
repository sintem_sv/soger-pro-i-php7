<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
#
$FEDIT->FGE_UseTables("user_schede_rifiuti");
//$FEDIT->FGE_SetSelectFields(array("ID_CER","cod_pro","descrizione","ID_SF","ID_FONTE_RIF","pericoloso","trasportatore","destinatario","produttore","idSIS_regCrono","approved"),"user_schede_rifiuti");
$FEDIT->FGE_SetSelectFields(array("ID_CER","cod_pro","descrizione","ID_SF","ID_FONTE_RIF","pericoloso","trasportatore","destinatario","produttore","approved"),"user_schede_rifiuti");
$FEDIT->FGE_DescribeFields();
$FEDIT->FGE_SetOrder("user_schede_rifiuti:ID_CER");

$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
$ProfiloFields = "user_schede_rifiuti";
include("SOGER_FiltriProfilo.php");
#
//$FEDIT->FGE_HideFields(array("trasportatore","destinatario","produttore","idSIS_regCrono","approved","ID_FONTE_RIF"),"user_schede_rifiuti");
$FEDIT->FGE_HideFields(array("trasportatore","destinatario","produttore","approved","ID_FONTE_RIF"),"user_schede_rifiuti");
$FEDIT->FGE_LookUpDefault("ID_SF","user_schede_rifiuti");
#
$FEDIT->FGE_LookUpCFG("ID_CER","user_schede_rifiuti");
$FEDIT->FGE_UseTables("lov_cer");
$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();
#
#echo($FEDIT->SG_RifiutiDataGrid($SOGER->AppDescriptiveLocation,"__scripts/FGE_DataGridEdit.php","EDCP-"));
//echo($FEDIT->FGE_DataGrid($SOGER->AppDescriptiveLocation,"__scripts/FGE_DataGridEdit.php","EDCP-",false));

//if($SOGER->UserData["core_usersG1"]=="0")
	echo($FEDIT->FGE_DataGrid($SOGER->AppDescriptiveLocation,"__scripts/FGE_DataGridEdit.php","-----",false));
//else
//	echo($FEDIT->FGE_DataGrid($SOGER->AppDescriptiveLocation,"__scripts/FGE_DataGridEdit.php","EDCPT",false));


#
require_once("__includes/COMMON_sleepForgEdit.php");
?>