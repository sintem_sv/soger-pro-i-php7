<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_contratti_budget");
$FEDIT->FGE_SetFormFields(array("ID_RIF","ID_IMP","euro_anno","euro_gen","euro_feb","euro_mar","euro_apr","euro_mag","euro_giu","euro_lug","euro_ago","euro_set","euro_ott","euro_nov","euro_dic"),"user_contratti_budget");

if(!isset($_GET["pri"]) && !isset($_GET["filter"])) {

	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_SetValue("euro_anno",0,"user_contratti_budget");
	$FEDIT->FGE_SetValue("euro_gen",0,"user_contratti_budget");
	$FEDIT->FGE_SetValue("euro_feb",0,"user_contratti_budget");
	$FEDIT->FGE_SetValue("euro_mar",0,"user_contratti_budget");
	$FEDIT->FGE_SetValue("euro_apr",0,"user_contratti_budget");
	$FEDIT->FGE_SetValue("euro_mag",0,"user_contratti_budget");
	$FEDIT->FGE_SetValue("euro_giu",0,"user_contratti_budget");
	$FEDIT->FGE_SetValue("euro_lug",0,"user_contratti_budget");
	$FEDIT->FGE_SetValue("euro_ago",0,"user_contratti_budget");
	$FEDIT->FGE_SetValue("euro_set",0,"user_contratti_budget");
	$FEDIT->FGE_SetValue("euro_ott",0,"user_contratti_budget");
	$FEDIT->FGE_SetValue("euro_nov",0,"user_contratti_budget");	
	$FEDIT->FGE_SetValue("euro_dic",0,"user_contratti_budget");
	$FEDIT->FGE_SetValue("ID_RIF",$_GET['ID_RIF'],"user_contratti_budget");

	$FEDIT->FGE_SetValue("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_contratti_budget");
	$FEDIT->FGE_HideFields(array("ID_RIF","ID_IMP"),"user_contratti_budget");
	

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea budget rifiuto");

} else {

	$FEDIT->FGE_DescribeFields();

	$FEDIT->FGE_SetValue("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_contratti_budget");
	$FEDIT->FGE_HideFields(array("ID_RIF","ID_IMP"),"user_contratti_budget");
	$sql="SELECT ID_RIF FROM user_contratti_budget WHERE ID_BUDG=".$_GET['filter']." AND ID_IMP='".$SOGER->UserData["core_impiantiID_IMP"]."';";
	$FEDIT->SDBRead($sql,"DbRecordSetTmp",true,false);

	$FEDIT->FGE_SetValue("ID_RIF",$FEDIT->DbRecordSetTmp[0]["ID_RIF"],"user_contratti_budget");
	
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica budget rifiuto");
}
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>