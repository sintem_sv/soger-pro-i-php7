<?php

function PTD($stringIN) {
	switch($stringIN) {
		case "P":
		return "produttore='1'";
		break;
		case "D":
		return "destinatario='1'";
		break;
		case "T":
		return "trasportatore='1'";
		break;
		case "I":
		return "intermediario='1'";
		break;
		}
	}



function SqlPTD($stringIN) {
//{{{
	switch($stringIN) {
		case "P":
		return "'1','0','0','0'";
		break;
		case "D":
		return "'0','0','1','0'";
		break;
		case "T":
		return "'0','1','0','0'";
		break;
		case "I":
		return "'0','0','0','1'";
		break;
	} //}}}
}

function slashes($value) {
//{{{
	if(is_null($value)) {
		return "null";
	}
	if(get_magic_quotes_gpc()==0) {
		return  "'" . addslashes($value) . "'";
	} else {
		return"'" . $value . "'";
	} //}}}
}
function AZcopy(&$DBobj,$soggetto,&$SourceINFO,&$DestINFO) {
//{{{
	switch($soggetto) {
		case "produttori":
		$table = "user_aziende_produttori";
		$pri = "ID_AZP";
		if(isset($SourceINFO["produttori"]["AZids"])) {
			$IDs = implode(",",$SourceINFO["produttori"]["AZids"]);
		} else {
			$IDs = null;
		}
		break;
		case "destinatari":
		$table = "user_aziende_destinatari";
		$pri = "ID_AZD";
		if(isset($SourceINFO["destinatari"]["AZids"])) {
			$IDs = implode(",",$SourceINFO["destinatari"]["AZids"]);
		} else {
			$IDs = null;
		}
		break;
		case "trasportatori":
		$table = "user_aziende_trasportatori";
		$pri = "ID_AZT";
		if(isset($SourceINFO["trasportatori"]["AZids"])) {
			$IDs = implode(",",$SourceINFO["trasportatori"]["AZids"]);
		} else {
			$IDs = null;
		}
		break;
		case "intermediari":
		$table = "user_aziende_intermediari";
		$pri = "ID_AZI";
		if(isset($SourceINFO["intermediari"]["AZids"])) {
			$IDs = implode(",",$SourceINFO["intermediari"]["AZids"]);
		} else {
			$IDs = null;
		}
		break;
	}
	if(is_null($IDs)) {
	#	echo "uscito";
		return array();
	}
	$DBobj->SDBRead("SELECT * FROM $table WHERE $pri IN($IDs)","DbRecordSet");
	$IMP_ID = array();

	foreach($DBobj->DbRecordSet as $k=>$dati) {
			#
			#	MAIN RECORD (AZIENDE)
			#
			switch($soggetto) {
				case "produttori":
				$tmp = "INSERT INTO $table VALUES (null,'" . $DestINFO["impID"]  . "'," . SqlPTD($DestINFO["impPROFILE"]);
				$tmp .= "," . slashes($dati["idSIS_regCrono"]) . "," . slashes($dati["idSIS"]) . "," . slashes($dati["versione"]) . "," . slashes($dati["SIS_iscritto"]);
				$tmp .= "," . slashes($dati["description"]) . "," . slashes($dati["indirizzo"]) . "," . slashes($dati["latitude"]) . "," . slashes($dati["longitude"]) . "," . $dati["ID_COM"] . "," . $dati["IN_ITALIA"] . "," . slashes($dati["piva"]) . "," . slashes($dati["codfisc"]) . ", " . slashes($dati["IscrittoCONAI"]) . ", " . slashes($dati["approved"]).", 0)";
				break;
				case "trasportatori":
				$tmp = "INSERT INTO $table VALUES (null,'" . $DestINFO["impID"]  . "'," . SqlPTD($DestINFO["impPROFILE"]);
				$tmp .= "," . slashes($dati["idSIS_regCrono"]) . "," . slashes($dati["idSIS"]) . "," . slashes($dati["versione"]) . "," . slashes($dati["SIS_iscritto"]);
				$tmp .= "," . slashes($dati["description"]) . "," . slashes($dati["indirizzo"]) . "," . slashes($dati["latitude"]) . "," . slashes($dati["longitude"]) . "," . $dati["ID_COM"] . "," . $dati["IN_ITALIA"] . "," . slashes($dati["piva"]) . "," . slashes($dati["codfisc"]) . "," . slashes($dati["NumAlboAutotrasp"]) . "," . slashes($dati["NumAlboAutotraspProprio"]) . "," . slashes($dati["esenzione_contributo"]) . "," . slashes($dati["contributo"]) . "," . slashes($dati["approved"]).", 0)";
				break;
				case "destinatari":
				$tmp = "INSERT INTO $table VALUES (null,'" . $DestINFO["impID"]  . "'," . SqlPTD($DestINFO["impPROFILE"]);
				$tmp .= "," . slashes($dati["idSIS_regCrono"]) . "," . slashes($dati["idSIS"]) . "," . slashes($dati["versione"]) . "," . slashes($dati["SIS_iscritto"]);
				$tmp .= "," . slashes($dati["description"]) . "," . slashes($dati["indirizzo"]) . "," . slashes($dati["latitude"]) . "," . slashes($dati["longitude"]) . "," . $dati["ID_COM"] . "," . $dati["IN_ITALIA"] . "," . slashes($dati["piva"]) . "," . slashes($dati["codfisc"]) . "," . slashes($dati["esenzione_contributo"]) . "," . slashes($dati["contributo"]) . ", " . slashes($dati["approved"]) . ", 0)";
				break;
				case "intermediari":
				$tmp = "INSERT INTO $table VALUES (null,'" . $DestINFO["impID"]  . "'," . SqlPTD($DestINFO["impPROFILE"]);
				$tmp .= "," . slashes($dati["idSIS_regCrono"]) . "," . slashes($dati["idSIS"]) . "," . slashes($dati["versione"]) . "," . slashes($dati["SIS_iscritto"]);
				$tmp .= "," . slashes($dati["description"]) . "," . slashes($dati["indirizzo"]) . "," . slashes($dati["latitude"]) . "," . slashes($dati["longitude"]) . "," . $dati["ID_COM"] . "," . $dati["IN_ITALIA"] . "," . slashes($dati["piva"]) . "," . slashes($dati["codfisc"]) . "," . slashes($dati["esenzione_contributo"]) . "," . slashes($dati["contributo"]) . ", " . slashes($dati["commerciante"]) . "," . slashes($dati["esclusoFIR"]) . ", " . slashes($dati["approved"]) . ", 0)";
				break;
			}
			//die($tmp);
			$DBobj->SDBWrite(utf8_encode($tmp),true,false);
			$AZ_ID = $DBobj->DbLastID;
			#
			#	SUB RECORDS (IMPIANTI)
			#
			switch($soggetto) {
				case "produttori":
                                    $azID = "ID_AZP";
                                    $impID = "ID_UIMP";
                                    $TabellaImpianti = "user_impianti_produttori";
                                    break;
				case "destinatari":
                                    $azID = "ID_AZD";
                                    $impID = "ID_UIMD";
                                    $TabellaImpianti = "user_impianti_destinatari";
                                    break;
				case "trasportatori":
                                    $azID = "ID_AZT";
                                    $impID = "ID_UIMT";
                                    $TabellaImpianti = "user_impianti_trasportatori";
                                    break;
				case "intermediari":
                                    $azID = "ID_AZI";
                                    $impID = "ID_UIMI";
                                    $TabellaImpianti = "user_impianti_intermediari";
                                    break;
			}
			if(!is_null($TabellaImpianti)) {
				$DBobj->SDBRead("SELECT * FROM $TabellaImpianti WHERE $pri=" . $dati[$pri],"AuxRecordSet");
				if($DBobj->DbRecsNum>0) {
					foreach($DBobj->AuxRecordSet as $k=>$v) {
						$sql = "INSERT INTO ".$TabellaImpianti;
                                                $sql.= " (".$impID.", idSIS, versione, ".$azID.", description, latitude, longitude, ID_COM, IN_ITALIA";
                                                if($TabellaImpianti=='user_impianti_produttori') $sql .= ", FuoriUnitaLocale";
                                                $sql.= ", ONU_62, COD_IMPORT, approved)";
                                                $sql.= " VALUES(null,".slashes($v["idSIS"]).", ".slashes($v["versione"]).", '$AZ_ID'," . slashes($v["description"]) . "," . slashes($v["latitude"]) . "," . slashes($v["longitude"]) . "," . $v["ID_COM"] . "," . $v["IN_ITALIA"];
						if($TabellaImpianti=='user_impianti_produttori') $sql .= "," .$v["FuoriUnitaLocale"];
						$sql .= ", ". slashes($v["ONU_62"]);
						$sql .= ", 0, ".$v["approved"]." )";
						//print_r(var_dump($sql));
						$DBobj->SDBWrite(utf8_encode($sql),true,false);
						$IMP_ID[] = $v[$impID] . "|" . $DBobj->DbLastID;
					}
				}
//                                else {
//					return array();
//				}
			}

	}
	return $IMP_ID; //}}}
}

function AUTHcopy(&$DBobj,$soggetto,&$IDinfo) {
//{{{
	if(count($IDinfo)==0) {
	#	echo "uscito";
		return array();
	}
	#echo "continuo $soggetto";
	#return;
	$ImpIDs = array();	# formato: $ImpIDs[source] = dest
	foreach($IDinfo as $k=>$valuesPair) {
		$Ids = explode ("|",$valuesPair);
		$ImpIDs[$Ids[0]] = $Ids[1];
	}
	switch($soggetto) {
                case "produttori":
		$TabellaAutorizzazioni = "user_autorizzazioni_pro";
		$ImpRef = "ID_UIMP";
		break;
		case "destinatari":
		$TabellaAutorizzazioni = "user_autorizzazioni_dest";
		$ImpRef = "ID_UIMD";
		break;
		case "trasportatori":
		$TabellaAutorizzazioni = "user_autorizzazioni_trasp";
		$ImpRef = "ID_UIMT";
		break;
		case "intermediari":
		$TabellaAutorizzazioni = "user_autorizzazioni_interm";
		$ImpRef = "ID_UIMI";
		break;
	}
	$sqls = array();
	$DBobj->SDbRead("SELECT * FROM $TabellaAutorizzazioni WHERE $ImpRef IN(" . implode(",",array_keys($ImpIDs)) . ")","DbRecordSet");
	if($soggetto=="trasportatori") {
		$idAutorizzazioni = array();
	}
	if($DBobj->DbRecsNum==0) {
		return array();
	}
	foreach($DBobj->DbRecordSet as $K=>$V) {
		switch($soggetto) {
		case "produttori":
			$sql[] = "INSERT INTO $TabellaAutorizzazioni VALUES(null," . $ImpIDs[$V[$ImpRef]] . "," . $V["ID_AUT"] . "," . slashes($V["num_aut"]) . "," . slashes($V["PREV_num_aut"]) . "," . slashes($V["rilascio"]) . "," . slashes($V["scadenza"]) . ",99999," .$V["ID_ORIGINE_DATI"]. "," .slashes($V["note"]).", 0, 0, " . $V["approved"] . ")";
		break;
		case "trasportatori":
			$sql[] = "INSERT INTO $TabellaAutorizzazioni VALUES(null," . $ImpIDs[$V[$ImpRef]] . "," . $V["ID_AUT"] . "," . slashes($V["num_aut"]) . "," . slashes($V["PREV_num_aut"]) . "," . slashes($V["rilascio"]) . "," . slashes($V["scadenza"]) . ",99999," .$V["ID_ORIGINE_DATI"]. "," .slashes($V["note"]).", 0, 0, " . $V["approved"] . ")";
			$idAutorizzazioni[] = $V["ID_AUTHT"];
		break;
		case "intermediari":
			$sql[] = "INSERT INTO $TabellaAutorizzazioni VALUES(null," . $ImpIDs[$V[$ImpRef]] . "," . $V["ID_AUT"] . "," . slashes($V["num_aut"]) . "," . slashes($V["PREV_num_aut"]) . "," . slashes($V["rilascio"]) . "," . slashes($V["scadenza"]) . ",99999," .$V["ID_ORIGINE_DATI"]. "," .slashes($V["note"]).", 0, 0, " . $V["approved"] . ")";
		break;
		case "destinatari":
			$sql[] = "INSERT INTO $TabellaAutorizzazioni VALUES(null," . $ImpIDs[$V[$ImpRef]] . "," . $V["ID_AUT"] . "," . slashes($V["num_aut"]) . "," . slashes($V["PREV_num_aut"]) . "," . slashes($V["rilascio"]) . "," . slashes($V["scadenza"]) . ",99999," . $V["ID_TRAT"] . "," . $V["ID_OP_RS"] . "," .$V["ID_OP_RS2"]. "," .$V["ID_ORIGINE_DATI"]. "," .slashes($V["note"]).", 0, 0, " . $V["approved"] . ")";
		break;
		}
	}
	$c=0;
	foreach($sql as $k=>$v) {
		$DBobj->SDbWrite(utf8_encode($v),true,false);
		if($soggetto=="trasportatori") {
			$idAutorizzazioni[$c] .= "|" . $DBobj->DbLastID;
		}
		$c++;
	}
	if($soggetto=="trasportatori") {
		return $idAutorizzazioni;
	}
	//}}}
}
function AUTISTIcopy(&$DBobj,&$IDinfo) {
//{{{
	if(count($IDinfo)==0) {
		return;
	}
	$ImpIDs = array();	# formato: $ImpIDs[source] = dest
	foreach($IDinfo as $k=>$valuesPair) {
		$Ids = explode ("|",$valuesPair);
		$ImpIDs[$Ids[0]] = $Ids[1];
	}
	$sqls = array();
	$DBobj->SDbRead("SELECT * FROM user_autisti WHERE ID_UIMT IN(" . implode(",",array_keys($ImpIDs)) . ")","DbRecordSet");
	if($DBobj->DbRecsNum>0) {
		foreach($DBobj->DbRecordSet as $K=>$V) {
			$sql[] = "INSERT INTO user_autisti VALUES(null," . slashes($V["nome"]) . "," . slashes($V["description"]) . "," . slashes($V["patente"]) . ","  . slashes($V["rilascio"]) . ","  . slashes($V["scadenza"]) ."," .  $ImpIDs[$V["ID_UIMT"]] . "," . $V["adr"] . ", 0)";
		}
		foreach($sql as $k=>$v) {
			$DBobj->SDbWrite(utf8_encode($v),true,false);;
		}
	}//}}}
}
function RIMORCHIcopy(&$DBobj,&$IDinfo) {
//{{{
	if(count($IDinfo)==0) {
		return;
	}
	$AutIDs = array();	# formato: $ImpIDs[source] = dest
	foreach($IDinfo as $k=>$valuesPair) {
		$Ids = explode ("|",$valuesPair);
		$AutIDs[$Ids[0]] = $Ids[1];
	}
	$DBobj->SDbRead("SELECT * FROM user_rimorchi WHERE ID_AUTHT IN(" . implode(",",array_keys($AutIDs)) . ")","DbRecordSet");
	if($DBobj->DbRecsNum>0) {
		$sqls = array();
		foreach($DBobj->DbRecordSet as $K=>$V) {
			$sql[] = "INSERT INTO user_rimorchi VALUES(null," . slashes($V["description"]) . "," . slashes($V["PREV_description"]) . "," . $V["adr"] . "," . $AutIDs[$V["ID_AUTHT"]] . "," . $V["ID_ORIGINE_DATI"] . "," . $V["ID_LIM"] . "," . $V["ID_MZ_RMK"] . ", 0, 0)";
		}
		foreach($sql as $k=>$v) {
			$DBobj->SDbWrite(utf8_encode($v),true,false);
		}
	} //}}}
}
function AUTOMEZZIcopy(&$DBobj,&$IDinfo) {
//{{{
	if(count($IDinfo)==0) {
		return;
	}
	$AutIDs = array();	# formato: $ImpIDs[source] = dest
	foreach($IDinfo as $k=>$valuesPair) {
		$Ids = explode ("|",$valuesPair);
		$AutIDs[$Ids[0]] = $Ids[1];
	}
	$DBobj->SDbRead("SELECT * FROM user_automezzi WHERE ID_AUTHT IN(" . implode(",",array_keys($AutIDs)) . ")","DbRecordSet");
	if($DBobj->DbRecsNum>0) {
		$sqls = array();
		foreach($DBobj->DbRecordSet as $K=>$V) {
			$sql[] = "INSERT INTO user_automezzi VALUES(null," . slashes($V["description"]) . "," . slashes($V["PREV_description"]) . "," . $V["adr"] . "," . $V["BlackBox"] . "," . $AutIDs[$V["ID_AUTHT"]] . "," . $V["ID_ORIGINE_DATI"] . "," . $V["ID_LIM"] . "," . $V["ID_MZ_TRA"] . ", 0, 0)";
		}
		foreach($sql as $k=>$v) {
			$DBobj->SDbWrite(utf8_encode($v),true,false);;
		}
	} //}}}
}
?>
