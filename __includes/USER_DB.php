<script type="text/javascript">
function ChangeDB(Fref) {
	if(confirm('Caricare il database selezionato?\n\nAttenzione: il database verra\' caricato solo per la sessione di lavoro corrente\n\n')) {
		Fref.submit();	
	}
}
function ExportDB(Fref) {
	if(confirm('Esportare il database?')) {
		document.DbBK.submit();	
	}
}
</script>
<?php
require_once("__classes/ForgEdit2.class");
require_once("__classes/ForgEdit.RegExp");
require_once("__classes/DbLink.class");
require_once("__libs/SQLFunct.php");

require("__includes/COMMON_wakeForgEdit.php");
global $SOGER;

$sql = "SHOW databases LIKE 'soger20%'";
$FEDIT->SDBRead($sql,"DbRecordset");
?> 

<div class="FGEDataGridTitle"><div><?php echo $SOGER->AppDescriptiveLocation; ?></div></div><br />
<?php
if($SOGER->AppLocation!="login" && $SOGER->AppLocation!="login_documents" && count($SOGER->AppNavDIVs)>0) {
	$tmp .= "<div id=\"NavDiv\">\n";
	$tmp .= "<!-- NAV DIVs BEGIN -->\n[NAV_STUCTURE]\n<!-- NAV DIVs END -->\n";
	$SOGER->DIVmake_Navigation();
	$tmp .= "</div>\n";
	echo $tmp;
	}
?>


<form name="DbSelect" method="post" action="__scripts/DB_Change.php">
<fieldset class="FGEfieldset">
    <div class="FGEFormTitle FGE4Col">Database in uso</div>
<select tabindex="50" name="database">
<?php

foreach($FEDIT->DbRecordset as $k=>$Db) {
    $YEAR = substr($Db["Database (soger20%)"], -4);
    if($YEAR<=date('Y')){
        $selected = $Db["Database (soger20%)"]==$FEDIT->DbServerData["db"]? "SELECTED":"";
        echo "<option value=\"" . $Db["Database (soger20%)"] . "\" $selected>" . $Db["Database (soger20%)"] . "</option>";	
        }
    }
?>
</select>

<input type="button" class="FGEbutton" tabindex="53" value="cambia database" onclick="javascript:ChangeDB(this.form);"/>
</fieldset>
</form>
