<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();

$FEDIT->FGE_UseTables("user_contratti_dest_cond");
$FEDIT->FGE_SetFormFields(array("ID_CNT_D", "description","euro","ID_UM_CNT","APPLY_W_INT","ID_UIMD"),"user_contratti_dest_cond");
$FEDIT->FGE_SetFilter("ID_CNT_D",$_SESSION['ID_CONTRACT'],"user_contratti_dest_cond");

$SQL = "SELECT ID_AZD FROM user_contratti_destinatari WHERE ID_CNT_D=".$_SESSION['ID_CONTRACT'].";";
$FEDIT->SDBRead($SQL,"DbRecordSetContrattoD",true,false);
$ID_AZD = $FEDIT->DbRecordSetContrattoD[0]['ID_AZD'];

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {

	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_SetValue("description","","user_contratti_dest_cond");
	$FEDIT->FGE_SetValue("euro",0,"user_contratti_dest_cond");
	$FEDIT->FGE_SetValue("ID_CNT_D",$_SESSION['ID_CONTRACT'],"user_contratti_dest_cond");
	$FEDIT->FGE_HideFields("ID_CNT_D","user_contratti_dest_cond");
	
	#
	$FEDIT->FGE_LookUpDefault("ID_UM_CNT","user_contratti_dest_cond");
	#
        
        #
	$FEDIT->FGE_LookUpCFG("ID_UIMD","user_contratti_dest_cond");
	$FEDIT->FGE_UseTables("user_impianti_destinatari");
	$FEDIT->FGE_SetSelectFields(array("description","ID_AZD"),"user_impianti_destinatari");
	$FEDIT->FGE_SetFilter("ID_AZD",$ID_AZD,"user_impianti_destinatari");
	$FEDIT->FGE_HideFields("ID_AZD","user_impianti_destinatari");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea condizione contrattuale");
$js ="<script type=\"text/javascript\">\n";
$js.= <<<JS
document.getElementById('user_contratti_dest_cond:APPLY_W_INTB').checked=true;
document.getElementById('user_contratti_dest_cond:APPLY_W_INTA').checked=false;
JS;
$js.="</script>\n";
echo $js;

} else {

	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_UM_CNT","user_contratti_dest_cond");
        
        #
	$FEDIT->FGE_LookUpCFG("ID_UIMD","user_contratti_dest_cond");
	$FEDIT->FGE_UseTables("user_impianti_destinatari");
	$FEDIT->FGE_SetSelectFields(array("description","ID_AZD"),"user_impianti_destinatari");
	$FEDIT->FGE_SetFilter("ID_AZD",$ID_AZD,"user_impianti_destinatari");
	$FEDIT->FGE_HideFields("ID_AZD","user_impianti_destinatari");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#
        
        $FEDIT->FGE_SetValue("ID_CNT_D",$_SESSION['ID_CONTRACT'],"user_contratti_dest_cond");
	$FEDIT->FGE_HideFields("ID_CNT_D","user_contratti_dest_cond");

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica condizione contrattuale");
}
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>