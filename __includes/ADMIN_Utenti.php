<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;


$js ="<script type='text/javascript'>";
$js.= <<<JS

var DisabledFunctions = new Array('O7', 'D1', 'D2');

function ResetPermessi(prefix, from, to){

	for(var p=from; p<=to; p++){
		var Permesso = prefix + p;
		if(DisabledFunctions.indexOf(Permesso)==-1)
			document.getElementById(Permesso).checked=false;
		}
	}

function InversionePermessi(prefix, from, to){
	for(var p=from; p<=to; p++){
		var Permesso = prefix + p;
		if(DisabledFunctions.indexOf(Permesso)==-1){
			if(document.getElementById(Permesso).checked){
				document.getElementById(Permesso).checked=false;
				}
			else{
				document.getElementById(Permesso).checked=true;
				}
			}
		}
	}

function check_PermessiUtente(action){

	var canSave			= true;
	var error			= "ATTENZIONE, impossibile completare l'operazione:\\n\\n";
	var RegExp_mail		= /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	var isProduttore	= document.PermessiUtente['produttore'][1].checked;
	var isTrasportatore	= document.PermessiUtente['trasportatore'][1].checked;
	var isDestinatario	= document.PermessiUtente['destinatario'][1].checked;
	var isIntermediario	= document.PermessiUtente['intermediario'][1].checked;
	
	var email			= document.PermessiUtente['email'].value;
	
	var usr				= document.PermessiUtente['usr'].value;
	var pwd				= document.PermessiUtente['pwd'].value;
	var nome			= document.PermessiUtente['nome'].value;
	var cognome			= document.PermessiUtente['cognome'].value;
	var obbligatori		= {"usr":usr, "pwd":pwd, "nome":nome, "cognome":cognome};

	if(!isProduttore && !isTrasportatore && !isDestinatario && !isIntermediario){
		canSave			= false;
		error			+="- e' necessario indicare almeno un profilo d'uso (produttore, trasportatore, destinatario o intermediario)\\n";
		}

	if(!RegExp_mail.test(email)){
		canSave			= false;
		error			+="- e' necessario indicare un indirizzo email valido\\n";
		}
	
	for(var index in obbligatori) {
		if(obbligatori[index]==''){
			canSave			= false;
			error			+="- il campo "+index+" e' obbligatorio\\n";
			}
		}


	if(!canSave)
		alert(error);
	else{
		//document.forms["PermessiUtente"].submit();
		switch(action){
			case 'create':
				document.forms["PermessiUtente"].submit();
				break;
			case 'duplicate':
				document.PermessiUtente['ID_USR'].value = 0;
				document.PermessiUtente['usr'].value += ' COPIA';
				document.forms["PermessiUtente"].submit();
				break;
			}

		}

	}

JS;
$js.="</script>";
echo $js;




#############################################
#
#	Sono responsabile o sono amministratore?
#
#############################################

if($SOGER->UserData['core_userssresponsabile']==1){
	$ID_IMPDisabled='disabled';
	$ID_IMP=$SOGER->UserData['core_usersID_IMP'];
	}
else{
	$ID_IMPDisabled='';
	}







if(isset($_POST['usr'])){
	if($_POST['ID_USR']>0){
		if(isset($_POST['ID_IMP_HIDDEN'])) $ID_IMP=addslashes($_POST['ID_IMP_HIDDEN']); else $ID_IMP=addslashes($_POST['ID_IMP']);
		// UPDATE RECORD ON DB
		$SQL ="UPDATE core_users SET ";
		$SQL.="usr='".addslashes($_POST['usr'])."', ";
		$SQL.="pwd='".addslashes($_POST['pwd'])."', ";
		$SQL.="description='".addslashes($_POST['description'])."', ";
		$SQL.="ID_IMP='".$ID_IMP."', ";
		$SQL.="nome='".addslashes($_POST['nome'])."', ";
		$SQL.="cognome='".addslashes($_POST['cognome'])."', ";
		$SQL.="email='".addslashes($_POST['email'])."', ";
		$SQL.="telefono='".addslashes($_POST['telefono'])."', ";
		$SQL.="produttore='".$_POST['produttore']."', ";
		$SQL.="trasportatore='".$_POST['trasportatore']."', ";
		$SQL.="destinatario='".$_POST['destinatario']."', ";
		$SQL.="intermediario='".$_POST['intermediario']."', ";
		// operatore
		if(isset($_POST['O1'])) $SQL.="O1=1, "; else $SQL.="O1=0, ";
		if(isset($_POST['O2'])) $SQL.="O2=1, "; else $SQL.="O2=0, ";
		if(isset($_POST['O3'])) $SQL.="O3=1, "; else $SQL.="O3=0, ";
		if(isset($_POST['O4'])) $SQL.="O4=1, "; else $SQL.="O4=0, ";
		if(isset($_POST['O5'])) $SQL.="O5=1, "; else $SQL.="O5=0, ";
		if(isset($_POST['O6'])) $SQL.="O6=1, "; else $SQL.="O6=0, ";
		if(isset($_POST['O7'])) $SQL.="O7=1, "; else $SQL.="O7=0, ";
		if(isset($_POST['O8'])) $SQL.="O8=1, "; else $SQL.="O8=0, ";
		if(isset($_POST['O9'])) $SQL.="O9=1, "; else $SQL.="O9=0, ";
		if(isset($_POST['O10'])) $SQL.="O10=1, "; else $SQL.="O10=0, ";
		if(isset($_POST['O11'])) $SQL.="O11=1, "; else $SQL.="O11=0, ";
		if(isset($_POST['O12'])) $SQL.="O12=1, "; else $SQL.="O12=0, ";
		// gestore
		if(isset($_POST['G1'])) $SQL.="G1=1, "; else $SQL.="G1=0, ";
		if(isset($_POST['G2'])) $SQL.="G2=1, "; else $SQL.="G2=0, ";
		if(isset($_POST['G3'])) $SQL.="G3=1, "; else $SQL.="G3=0, ";
		if(isset($_POST['G4'])) $SQL.="G4=1, "; else $SQL.="G4=0, ";
		if(isset($_POST['G5'])) $SQL.="G5=1, "; else $SQL.="G5=0, ";
		if(isset($_POST['G6'])) $SQL.="G6=1, "; else $SQL.="G6=0, ";
		if(isset($_POST['G7'])) $SQL.="G7=1, "; else $SQL.="G7=0, ";
		if(isset($_POST['G8'])) $SQL.="G8=1, "; else $SQL.="G8=0, ";
		if(isset($_POST['G9'])) $SQL.="G9=1, "; else $SQL.="G9=0, ";
		if(isset($_POST['G10'])) $SQL.="G10=1, "; else $SQL.="G10=0, ";
		if(isset($_POST['G11'])) $SQL.="G11=1, "; else $SQL.="G11=0, ";
		if(isset($_POST['G12'])) $SQL.="G12=1, "; else $SQL.="G12=0, ";
		if(isset($_POST['G13'])) $SQL.="G13=1, "; else $SQL.="G13=0, ";
		if(isset($_POST['G14'])) $SQL.="G14=1, "; else $SQL.="G14=0, ";
		// delegato
		if(isset($_POST['D1'])) $SQL.="D1=1, "; else $SQL.="D1=0, ";
		if(isset($_POST['D2'])) $SQL.="D2=1, "; else $SQL.="D2=0, ";
		if(isset($_POST['D3'])) $SQL.="D3=1, "; else $SQL.="D3=0, ";
		if(isset($_POST['D4'])) $SQL.="D4=1, "; else $SQL.="D4=0, ";
		if(isset($_POST['D5'])) $SQL.="D5=1, "; else $SQL.="D5=0, ";
		if(isset($_POST['D6'])) $SQL.="D6=1, "; else $SQL.="D6=0, ";
		if(isset($_POST['D7'])) $SQL.="D7=1, "; else $SQL.="D7=0, ";
		// responsabile
		if(isset($_POST['R1'])) $SQL.="R1=1, "; else $SQL.="R1=0, ";
		if(isset($_POST['R2'])) $SQL.="R2=1, "; else $SQL.="R2=0, ";
		if(isset($_POST['R3'])) $SQL.="R3=1, "; else $SQL.="R3=0, ";
		if(isset($_POST['R4'])) $SQL.="R4=1, "; else $SQL.="R4=0, ";
		if(isset($_POST['R5'])) $SQL.="R5=1, "; else $SQL.="R5=0, ";
		if(isset($_POST['R6'])) $SQL.="R6=1, "; else $SQL.="R6=0, ";
		if(isset($_POST['R7'])) $SQL.="R7=1 "; else $SQL.="R7=0 ";

		$SQL.="WHERE ID_USR=".$_POST['ID_USR'];

		//var_dump($SQL);

		// DB WRITE
		$FEDIT->SDBWrite($SQL,true,false);

		// URL FOR REDIRECTION
		$REDIR = $_SERVER['REQUEST_URI'];
		
		}
	else{
		if(isset($_POST['ID_IMP_HIDDEN'])) $ID_IMP=addslashes($_POST['ID_IMP_HIDDEN']); else $ID_IMP=addslashes($_POST['ID_IMP']);
		// INSERT RECORD ON DB
		$SQL ="INSERT INTO core_users (";
		$SQL.="`ID_IMP`, `usr`, `pwd`, `description`, `nome`, `cognome`, `email`, `telefono`, `produttore`, `trasportatore`, `destinatario`, `intermediario`, ";
		$SQL.="`O1`, `O2`, `O3`, `O4`, `O5`, `O6`, `O7`, `O8`, `O9`, `O10`, `O11`, `O12`, ";
		$SQL.="`G1`, `G2`, `G3`, `G4`, `G5`, `G6`, `G7`, `G8`, `G9`, `G10`, `G11`, `G12`, `G13`, `G14`, ";
		$SQL.="`D1`, `D2`, `D3`, `D4`, `D5`, `D6`, `D7`, ";
		$SQL.="`R1`, `R2`, `R3`, `R4`, `R5`, `R6`, `R7` ";
		$SQL.=") VALUES (";
		$SQL.="'".$ID_IMP."', '".addslashes($_POST['usr'])."', '".addslashes($_POST['pwd'])."', '".addslashes($_POST['description'])."', ";
		$SQL.="'".addslashes($_POST['nome'])."', '".addslashes($_POST['cognome'])."', '".addslashes($_POST['email'])."', '".addslashes($_POST['telefono'])."', ";
		$SQL.="'".$_POST['produttore']."', '".$_POST['trasportatore']."', '".$_POST['destinatario']."', '".$_POST['intermediario']."', ";
		if(isset($_POST['O1'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['O2'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['O3'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['O4'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['O5'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['O6'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['O7'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['O8'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['O9'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['O10'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['O11'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['O12'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['G1'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['G2'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['G3'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['G4'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['G5'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['G6'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['G7'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['G8'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['G9'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['G10'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['G11'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['G12'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['G13'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['G14'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['D1'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['D2'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['D3'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['D4'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['D5'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['D6'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['D7'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['R1'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['R2'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['R3'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['R4'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['R5'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['R6'])) $SQL.="'1', "; else $SQL.="'0', ";
		if(isset($_POST['R7'])) $SQL.="'1' "; else $SQL.="'0' ";
		$SQL.=");";
	
		// DB WRITE
		$FEDIT->SDBWrite($SQL,true,false);
		$ID_USR=str_pad((string)$FEDIT->DbLastID, 7, "0", STR_PAD_LEFT);

		// CREATE USER DIRECTORY
		if(!is_dir("__documents/__reg/".$ID_IMP."/".$ID_USR))
			mkdir("__documents/__reg/".$ID_IMP."/".$ID_USR, 0777);
		
		if(!is_dir("__documents/__adr/".$ID_IMP."/".$ID_USR)) 
			mkdir("__documents/__adr/".$ID_IMP."/".$ID_USR, 0777);

		if(!is_dir("__documents/__alarm/".$ID_IMP."/".$ID_USR)) 
			mkdir("__documents/__alarm/".$ID_IMP."/".$ID_USR, 0777);
		
		// URL FOR REDIRECTION
		$hash = MakeUrlHash('core_users','ID_USR',$ID_USR);
		$REDIR = "?table=core_users&pri=ID_USR&filter=" . $ID_USR . "&hash=" . $hash . "&ID_SV=";
		$REDIR.= "&UserIsResponsabile=0";
		$REDIR.= "&FGE_action=edit";
		//die(var_dump($REDIR));
		}

	// FEEDBACK
	$SOGER->SetFeedback("Utente salvato","2");
	
	// REDIRECTION
	header("location: ".$REDIR);
	}



echo "<div class=\"FGEDataGridTitle\" style=\"margin-bottom:65px;\"><div>$SOGER->AppDescriptiveLocation</div></div>";
if($SOGER->AppLocation!="login" && $SOGER->AppLocation!="login_documents" && count($SOGER->AppNavDIVs)>0) {
	$tmp .= "<div id=\"NavDiv\">\n";
	$tmp .= "<!-- NAV DIVs BEGIN -->\n[NAV_STUCTURE]\n<!-- NAV DIVs END -->\n";
	$SOGER->DIVmake_Navigation();
	$tmp .= "</div>\n";
	echo $tmp;
	}


if(isset($_GET['filter'])){
	$SQL ="SELECT ID_USR, ID_IMP, usr, pwd, description, nome, cognome, email, telefono, produttore, trasportatore, destinatario, intermediario, ";
	$SQL.="O1, O2, O3, O4, O5, O6, O7, O8, O9, O10, O11, O12, ";
	$SQL.="G1, G2, G3, G4, G5, G6, G7, G8, G9, G10, G11, G12, G13, G14, ";
	$SQL.="D1, D2, D3, D4, D5, D6, D7, ";
	$SQL.="R1, R2, R3, R4, R5, R6, R7 ";
	$SQL.="FROM core_users WHERE ID_USR=".$_GET['filter'];
	$FEDIT->SDBRead($SQL,"DbRecordSet",true,false);

	//var_dump($FEDIT->DbRecordSet);
	
	// INPUT TYPE HIDDEN
	$ID_USR=$FEDIT->DbRecordSet[0]['ID_USR'];

	// INPUT TYPE TEXT
	$ID_IMP=$FEDIT->DbRecordSet[0]['ID_IMP']; // ->ovverride previous assignation

	// INPUT TYPE TEXT
	$usr=$FEDIT->DbRecordSet[0]['usr'];
	$pwd=$FEDIT->DbRecordSet[0]['pwd'];
	$description=$FEDIT->DbRecordSet[0]['description'];
	$nome=$FEDIT->DbRecordSet[0]['nome'];
	$cognome=$FEDIT->DbRecordSet[0]['cognome'];
	$email=$FEDIT->DbRecordSet[0]['email'];
	$telefono=$FEDIT->DbRecordSet[0]['telefono'];

	// INPUT TYPE RADIO
	$produttore=$FEDIT->DbRecordSet[0]['produttore'];
	if($produttore=='0'){ $checkedProd0 = 'checked'; $checkedProd1 = ''; } else { $checkedProd0 = ''; $checkedProd1 = 'checked'; }
	$trasportatore=$FEDIT->DbRecordSet[0]['trasportatore'];
	if($trasportatore=='0'){ $checkedTrasp0 = 'checked'; $checkedTrasp1 = ''; } else { $checkedTrasp0 = ''; $checkedTrasp1 = 'checked'; }
	$destinatario=$FEDIT->DbRecordSet[0]['destinatario'];
	if($destinatario=='0'){ $checkedDest0 = 'checked'; $checkedDest1 = ''; } else { $checkedDest0 = ''; $checkedDest1 = 'checked'; }
	$intermediario=$FEDIT->DbRecordSet[0]['intermediario'];
	if($intermediario=='0'){ $checkedInt0 = 'checked'; $checkedInt1 = ''; } else { $checkedInt0 = ''; $checkedInt1 = 'checked'; }

	// INPUT TYPE CHECKBOX	
	$O1=$FEDIT->DbRecordSet[0]['O1']; if($O1==0) $checkedO1=''; else $checkedO1='checked';
	$O2=$FEDIT->DbRecordSet[0]['O2']; if($O2==0) $checkedO2=''; else $checkedO2='checked';
	$O3=$FEDIT->DbRecordSet[0]['O3']; if($O3==0) $checkedO3=''; else $checkedO3='checked';
	$O4=$FEDIT->DbRecordSet[0]['O4']; if($O4==0) $checkedO4=''; else $checkedO4='checked';
	$O5=$FEDIT->DbRecordSet[0]['O5']; if($O5==0) $checkedO5=''; else $checkedO5='checked';
	$O6=$FEDIT->DbRecordSet[0]['O6']; if($O6==0) $checkedO6=''; else $checkedO6='checked';
	$O7=$FEDIT->DbRecordSet[0]['O7']; if($O7==0) $checkedO7=''; else $checkedO7='checked';
	$O8=$FEDIT->DbRecordSet[0]['O8']; if($O8==0) $checkedO8=''; else $checkedO8='checked';
	$O9=$FEDIT->DbRecordSet[0]['O9']; if($O9==0) $checkedO9=''; else $checkedO9='checked';
	$O10=$FEDIT->DbRecordSet[0]['O10']; if($O10==0) $checkedO10=''; else $checkedO10='checked';
	$O11=$FEDIT->DbRecordSet[0]['O11']; if($O11==0) $checkedO11=''; else $checkedO11='checked';
	$O12=$FEDIT->DbRecordSet[0]['O12']; if($O12==0) $checkedO12=''; else $checkedO12='checked';
	$G1=$FEDIT->DbRecordSet[0]['G1']; if($G1==0) $checkedG1=''; else $checkedG1='checked';
	$G2=$FEDIT->DbRecordSet[0]['G2']; if($G2==0) $checkedG2=''; else $checkedG2='checked';
	$G3=$FEDIT->DbRecordSet[0]['G3']; if($G3==0) $checkedG3=''; else $checkedG3='checked';
	$G4=$FEDIT->DbRecordSet[0]['G4']; if($G4==0) $checkedG4=''; else $checkedG4='checked';
	$G5=$FEDIT->DbRecordSet[0]['G5']; if($G5==0) $checkedG5=''; else $checkedG5='checked';
	$G6=$FEDIT->DbRecordSet[0]['G6']; if($G6==0) $checkedG6=''; else $checkedG6='checked';
	$G7=$FEDIT->DbRecordSet[0]['G7']; if($G7==0) $checkedG7=''; else $checkedG7='checked';
	$G8=$FEDIT->DbRecordSet[0]['G8']; if($G8==0) $checkedG8=''; else $checkedG8='checked';
	$G9=$FEDIT->DbRecordSet[0]['G9']; if($G9==0) $checkedG9=''; else $checkedG9='checked';
	$G10=$FEDIT->DbRecordSet[0]['G10']; if($G10==0) $checkedG10=''; else $checkedG10='checked';
	$G11=$FEDIT->DbRecordSet[0]['G11']; if($G11==0) $checkedG11=''; else $checkedG11='checked';
	$G12=$FEDIT->DbRecordSet[0]['G12']; if($G12==0) $checkedG12=''; else $checkedG12='checked';
	$G13=$FEDIT->DbRecordSet[0]['G13']; if($G13==0) $checkedG13=''; else $checkedG13='checked';
	$G14=$FEDIT->DbRecordSet[0]['G14']; if($G14==0) $checkedG14=''; else $checkedG14='checked';
	$D1=$FEDIT->DbRecordSet[0]['D1']; if($D1==0) $checkedD1=''; else $checkedD1='checked';
	$D2=$FEDIT->DbRecordSet[0]['D2']; if($D2==0) $checkedD2=''; else $checkedD2='checked';
	$D3=$FEDIT->DbRecordSet[0]['D3']; if($D3==0) $checkedD3=''; else $checkedD3='checked';
	$D4=$FEDIT->DbRecordSet[0]['D4']; if($D4==0) $checkedD4=''; else $checkedD4='checked';
	$D5=$FEDIT->DbRecordSet[0]['D5']; if($D5==0) $checkedD5=''; else $checkedD5='checked';
	$D6=$FEDIT->DbRecordSet[0]['D6']; if($D6==0) $checkedD6=''; else $checkedD6='checked';
	$D7=$FEDIT->DbRecordSet[0]['D7']; if($D7==0) $checkedD7=''; else $checkedD7='checked';
	$R1=$FEDIT->DbRecordSet[0]['R1']; if($R1==0) $checkedR1=''; else $checkedR1='checked';
	$R2=$FEDIT->DbRecordSet[0]['R2']; if($R2==0) $checkedR2=''; else $checkedR2='checked';
	$R3=$FEDIT->DbRecordSet[0]['R3']; if($R3==0) $checkedR3=''; else $checkedR3='checked';
	$R4=$FEDIT->DbRecordSet[0]['R4']; if($R4==0) $checkedR4=''; else $checkedR4='checked';
	$R5=$FEDIT->DbRecordSet[0]['R5']; if($R5==0) $checkedR5=''; else $checkedR5='checked';
	$R6=$FEDIT->DbRecordSet[0]['R6']; if($R6==0) $checkedR6=''; else $checkedR6='checked';
	$R7=$FEDIT->DbRecordSet[0]['R7']; if($R7==0) $checkedR7=''; else $checkedR7='checked';
	}
else{	
	
	// INPUT TYPE HIDDEN
	$ID_USR=0;
	
	// INPUT TYPE TEXT
	//$ID_IMP='################################';

	// INPUT TYPE TEXT
	$usr		="";
	$pwd		="";
	$description="";
	$nome		="";
	$cognome	="";
	$email		="";
	$telefono	="";

	// INPUT TYPE RADIO
	$checkedProd0 = 'checked'; $checkedProd1 = '';
	$checkedTrasp0 = 'checked'; $checkedTrasp1 = '';
	$checkedDest0 = 'checked'; $checkedDest1 = '';
	$checkedInt0 = 'checked'; $checkedInt1 = '';

	// INPUT TYPE CHECKBOX
	$checkedO1='';
	$checkedO2='';
	$checkedO3='';
	$checkedO4='';
	$checkedO5='';
	$checkedO6='';
	$checkedO7='';
	$checkedO8='';
	$checkedO9='';
	$checkedO10='';
	$checkedO11='';	
	$checkedO12='';
	$checkedG1='';
	$checkedG2='';
	$checkedG3='';
	$checkedG4='';
	$checkedG5='';
	$checkedG6='';
	$checkedG7='';
	$checkedG8='';
	$checkedG9='';
	$checkedG10='';
	$checkedG11='';
	$checkedG12='';
	$checkedG13='';	
	$checkedG14='';
	$checkedD1='';
	$checkedD2='';
	$checkedD3='';
	$checkedD4='';
	$checkedD5='';
	$checkedD6='';
	$checkedD7='';
	$checkedR1='';
	$checkedR2='';
	$checkedR3='';
	$checkedR4='';
	$checkedR5='';
	$checkedR6='';
	$checkedR7='';
	}

echo "<div><form name='PermessiUtente' action='".$_SERVER['REQUEST_URI']."' method='POST'>";


echo "<table class=\"DatiAnagrafici\" cellpadding='2' cellspacing='0'>\n";
	echo "<tr>\n";
		echo "<th>Impianto:</th>\n";
		//echo "<td><input type='text' name='ID_IMP' id='ID_IMP' value='".$SOGER->UserData["core_usersID_IMP"]."' disabled /></td>\n";
		echo "<td>\n";
			echo "<select style='width: 150px;' name='ID_IMP' id='ID_IMP' ".$ID_IMPDisabled." >\n";
			$SQL="SELECT ID_IMP, description FROM core_impianti ORDER BY description ASC";
			$FEDIT->SDBRead($SQL,"DbRecordSetIMP",true,false);
			for($i=0;$i<count($FEDIT->DbRecordSetIMP);$i++){
				if($ID_IMP==$FEDIT->DbRecordSetIMP[$i]['ID_IMP']) $Selected='selected'; else $Selected='';
				echo "<option value='".$FEDIT->DbRecordSetIMP[$i]['ID_IMP']."' ".$Selected." >".$FEDIT->DbRecordSetIMP[$i]['description']."</option>\n";
				}
			echo "</select>\n";
		echo "</td>\n";
		echo "<th>Username:</th>\n";
		echo "<td><input type='text' name='usr' id='usr' value='".$usr."' /></td>\n";
		echo "<th>Password:</th>\n";
		echo "<td><input type='password' name='pwd' id='pwd' value='".$pwd."' /></td>\n";
		echo "<th>Descrizione:</th>\n";
		echo "<td><input type='text' name='description' id='description' value='".$description."' /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<th>Nome:</th>\n";
		echo "<td><input type='text' name='nome' id='nome' value='".$nome."' /></td>\n";
		echo "<th>Cognome:</th>\n";
		echo "<td><input type='text' name='cognome' id='cognome' value='".$cognome."' /></td>\n";
		echo "<th>Email:</th>\n";
		echo "<td><input type='text' name='email' id='email' value='".$email."' /></td>\n";
		echo "<th>Telefono:</th>\n";
		echo "<td><input type='text' name='telefono' id='telefono' value='".$telefono."' /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<th>Produttore:</th>\n";
		echo "<td><input type='radio' name='produttore' value='0' ".$checkedProd0." />no<input type='radio' name='produttore' value='1' ".$checkedProd1." />s�</td>\n";
		echo "<th>Trasportatore:</th>\n";
		echo "<td><input type='radio' name='trasportatore' value='0' ".$checkedTrasp0." />no<input type='radio' name='trasportatore' value='1' ".$checkedTrasp1." />s�</td>\n";
		echo "<th>Destinatario:</th>\n";
		echo "<td><input type='radio' name='destinatario' value='0' ".$checkedDest0." />no<input type='radio' name='destinatario' value='1' ".$checkedDest1." />s�</td>\n";
		echo "<th>Intermediario:</th>\n";
		echo "<td><input type='radio' name='intermediario' value='0' ".$checkedInt0." />no<input type='radio' name='intermediario' value='1' ".$checkedInt1." />s�</td>\n";
	echo "</tr>\n";
echo "</table>";

echo "<table class=\"TabellaPermessiUtente\" cellpadding='2' cellspacing='0'>\n";
	echo "<tr>\n";
		echo "<th rowspan='12' class='O'>\n";
			echo "<div>\n";
				echo "<div class='IconaRuolo'><img src='__css/operatore.gif' alt='Operatore ambientale' title='Operatore ambientale' /></div>\n";
				echo "<div class='DescrizioneRuolo'>Operatore ambientale</div>\n";
				echo "<div class='PulsantiRuolo'>\n";
					echo "<input class='PermessiButton' type='button' value='Inverti selezione' name='inversione' onClick='InversionePermessi(\"O\", 1, 12);' />\n";
					echo "<input class='PermessiButton' type='button' value='Reset' name='reset' onClick='ResetPermessi(\"O\", 1, 12);' />\n";
				echo "</div>\n";
			echo "</div>\n";
		echo "</th>\n";
		echo "<td class='PermessoCode'>O1</td>\n";
		echo "<td class='PermessoDescription'>Anagrafiche: accesso per modifica</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='O1' id='O1' ".$checkedO1." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>O2</td>\n";
		echo "<td class='PermessoDescription'>Anagrafiche: accesso per stampa</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='O2' id='O2' ".$checkedO2." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>O3</td>\n";
		echo "<td class='PermessoDescription'>Stampa Lista Fornitori</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='O3' id='O3' ".$checkedO3." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>O4</td>\n";
		echo "<td class='PermessoDescription'>Schede rifiuti: crea richieste di carico, scarico e conferimento</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='O4' id='O4' ".$checkedO4." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>O5</td>\n";
		echo "<td class='PermessoDescription'>Movimentazione: creazione Carichi e Scarichi, e modifica</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='O5' id='O5' ".$checkedO5." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>O6</td>\n";
		echo "<td class='PermessoDescription'>Movimentazione Registro Fanghi</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='O6' id='O6' ".$checkedO6." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode' style=\"background-color:#FFD8E1\">O7</td>\n";
		echo "<td class='PermessoDescription' style=\"background-color:#FFD8E1\">Genera e Stampa Documento di Accompagnamento</td>\n";
		echo "<td class='checkbox' style=\"background-color:#FFD8E1\"><input disabled type='checkbox' name='O7' id='O7' ".$checkedO7." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>O8</td>\n";
		echo "<td class='PermessoDescription'>Statistiche: accesso generazione in formato PDF</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='O8' id='O8' ".$checkedO8." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>O9</td>\n";
		echo "<td class='PermessoDescription'>Configurazione: accesso al pannello Database</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='O9' id='O9' ".$checkedO9." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>O10</td>\n";
		echo "<td class='PermessoDescription'>Configurazione: accesso al pannello Dati Utente</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='O10' id='O10' ".$checkedO10." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>O11</td>\n";
		echo "<td class='PermessoDescription'>Configurazione: accesso al pannello Stampe</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='O11' id='O11' ".$checkedO11." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>O12</td>\n";
		echo "<td class='PermessoDescription'>Movimentazione: modifica dei movimenti creati da qualsiasi utente dello stabilimento</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='O12' id='O12' ".$checkedO12." /></td>\n";
	echo "</tr>\n";
echo "</table>";


echo "<table class=\"TabellaPermessiUtente\" cellpadding='2' cellspacing='0'>\n";
	echo "<tr>\n";
		echo "<th rowspan='14' class='G'>\n";
			echo "<div>\n";
				echo "<div class='IconaRuolo'><img src='__css/gestore.gif' alt='Gestore ambientale' title='Gestore ambientale' /></div>\n";
				echo "<div class='DescrizioneRuolo'>Gestore ambientale</div>\n";
				echo "<div class='PulsantiRuolo'>\n";
					echo "<input class='PermessiButton' type='button' value='Inverti selezione' name='inversione' onClick='InversionePermessi(\"G\", 1, 14);' />\n";
					echo "<input class='PermessiButton' type='button' value='Reset' name='reset' onClick='ResetPermessi(\"G\", 1, 14);' />\n";
				echo "</div>\n";
			echo "</div>\n";
		echo "</th>\n";
		echo "<td class='PermessoCode'>G1</td>\n";
		echo "<td class='PermessoDescription'>Anagrafiche: abilitazione o esclusione fornitore / scheda rifiuto</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='G1' id='G1' ".$checkedG1." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>G2</td>\n";
		echo "<td class='PermessoDescription'>Geolocalizza Anagrafiche Fornitori</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='G2' id='G2' ".$checkedG2." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>G3</td>\n";
		echo "<td class='PermessoDescription'>Gestione economica - Contratti</td>\n";
		if($SOGER->UserData['core_impiantiMODULO_ECO']=="0") $disabledG3='disabled'; else $disabledG3='';
		echo "<td class='checkbox'><input type='checkbox' name='G3' id='G3' ".$checkedG3." ".$disabledG3." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>G4</td>\n";
		echo "<td class='PermessoDescription'>Compila segnaletica di sicurezza</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='G4' id='G4' ".$checkedG4." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>G5</td>\n";
		echo "<td class='PermessoDescription'>Schede rifiuti: genera le etichette di pericolo</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='G5' id='G5' ".$checkedG5." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>G6</td>\n";
		echo "<td class='PermessoDescription'>Schede rifiuti: crea le procedure di sicurezza</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='G6' id='G6' ".$checkedG6." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>G7</td>\n";
		echo "<td class='PermessoDescription'>Carica e Scarica Analisi, Caratterizzazioni e Procedure di Sicurezza della Gestione rifiuti</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='G7' id='G7' ".$checkedG7." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>G8</td>\n";
		echo "<td class='PermessoDescription'>Organizza Deposito rifiuti</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='G8' id='G8' ".$checkedG8." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>G9</td>\n";
		echo "<td class='PermessoDescription'>Crea e Controlla Percorsi dei Trasportatori</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='G9' id='G9' ".$checkedG9." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>G10</td>\n";
		echo "<td class='PermessoDescription'>Statistiche: generazione in formato .csv ( Excel )</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='G10' id='G10' ".$checkedG10." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>G11</td>\n";
		echo "<td class='PermessoDescription'>Calcola Indici di Gestione</td>\n";
		if($SOGER->UserData['core_impiantiMODULO_WMS']=="0") $disabledG11='disabled'; else $disabledG11='';
		echo "<td class='checkbox'><input type='checkbox' name='G11' id='G11' ".$checkedG11." ".$disabledG11." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>G12</td>\n";
		echo "<td class='PermessoDescription'>Controllo Deposito Temporaneo</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='G12' id='G12' ".$checkedG12." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>G13</td>\n";
		echo "<td class='PermessoDescription'>Schede rifiuti: crea le schede di caratterizzazione</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='G13' id='G13' ".$checkedG13." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>G14</td>\n";
		echo "<td class='PermessoDescription'>Anagrafiche: visualizza fornitore / scheda rifiuto non abilitato</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='G14' id='G14' ".$checkedG14." /></td>\n";
	echo "</tr>\n";
echo "</table>";





echo "<table class=\"TabellaPermessiUtente\" cellpadding='2' cellspacing='0'>\n";
	echo "<tr>\n";
		echo "<th rowspan='7' class='D'>\n";
			echo "<div>\n";
				echo "<div class='IconaRuolo'><img src='__css/delegato.gif' alt='Delegato' title='Delegato' /></div>\n";
				echo "<div class='DescrizioneRuolo'>Delegato</div>\n";
				echo "<div class='PulsantiRuolo'>\n";
					echo "<input class='PermessiButton' type='button' value='Inverti selezione' name='inversione' onClick='InversionePermessi(\"D\", 3, 7);' />\n";
					echo "<input class='PermessiButton' type='button' value='Reset' name='reset' onClick='ResetPermessi(\"D\", 1, 7);' />\n";
				echo "</div>\n";
			echo "</div>\n";
		echo "</th>\n";
		echo "<td class='PermessoCode' style=\"background-color:#FFD8E1\">D1</td>\n";
		echo "<td class='PermessoDescription' style=\"background-color:#FFD8E1\">Movimentazione: fiscalizzazione dei movimenti <i>(funzione rimossa dal programma)</i></td>\n";
		echo "<td class='checkbox' style=\"background-color:#FFD8E1\"><input disabled type='checkbox' name='D1' id='D1' ".$checkedD1." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode' style=\"background-color:#FFD8E1\">D2</td>\n";
		echo "<td class='PermessoDescription' style=\"background-color:#FFD8E1\">Annulla ultima fiscalizzazione <i>(funzione rimossa dal programma)</i></td>\n";
		echo "<td class='checkbox' style=\"background-color:#FFD8E1\"><input disabled type='checkbox' name='D2' id='D2' ".$checkedD2." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>D3</td>\n";
		echo "<td class='PermessoDescription'>Stampa del formulario</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='D3' id='D3' ".$checkedD3." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>D4</td>\n";
		echo "<td class='PermessoDescription'>Stampa del registro Carico/Scarico fiscale</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='D4' id='D4' ".$checkedD4." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>D5</td>\n";
		echo "<td class='PermessoDescription'>Stampa Registro Fanghi</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='D5' id='D5' ".$checkedD5." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>D6</td>\n";
		echo "<td class='PermessoDescription'>Configurazione: accesso al pannello Registrazione</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='D6' id='D6' ".$checkedD6." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>D7</td>\n";
		echo "<td class='PermessoDescription'>MUD: esportazione dati</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='D7' id='D7' ".$checkedD7." /></td>\n";
	echo "</tr>\n";
echo "</table>";







echo "<table class=\"TabellaPermessiUtente\" cellpadding='2' cellspacing='0'>\n";
	echo "<tr>\n";
		echo "<th rowspan='7' class='R'>\n";
			echo "<div>\n";
				echo "<div class='IconaRuolo'><img src='__css/responsabile.gif' alt='Responsabile ambientale' title='Responsabile ambientale' /></div>\n";
				echo "<div class='DescrizioneRuolo'>Responsabile Ambientale</div>\n";
				echo "<div class='PulsantiRuolo'>\n";
					echo "<input class='PermessiButton' type='button' value='Inverti selezione' name='inversione' onClick='InversionePermessi(\"R\", 1, 7);' />\n";
					echo "<input class='PermessiButton' type='button' value='Reset' name='reset' onClick='ResetPermessi(\"R\", 1, 7);' />\n";
				echo "</div>\n";
			echo "</div>\n";
		echo "</th>\n";
		echo "<td class='PermessoCode'>R1</td>\n";
		echo "<td class='PermessoDescription'>Crea le Classi Ambientali dei rifiuti</td>\n";
		if($SOGER->UserData['core_impiantiMODULO_WMS']=="0") $disabledR1='disabled'; else $disabledR1='';
		echo "<td class='checkbox'><input type='checkbox' name='R1' id='R1' ".$checkedR1." ".$disabledR1." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>R2</td>\n";
		echo "<td class='PermessoDescription'>Statistiche: accesso al Controllo di Movimentazione</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='R2' id='R2' ".$checkedR2." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>R3</td>\n";
		echo "<td class='PermessoDescription'>Registro Adempimenti [ISO14001/EMAS]</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='R3' id='R3' ".$checkedR3." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>R4</td>\n";
		echo "<td class='PermessoDescription'>Configura Non Conformit� [ISO14001/EMAS]</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='R4' id='R4' ".$checkedR4." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>R5</td>\n";
		echo "<td class='PermessoDescription'>Configurazione: accesso al pannello Controlli</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='R5' id='R5' ".$checkedR5." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>R6</td>\n";
		echo "<td class='PermessoDescription'>Configurazione: accesso al pannello Allarmi</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='R6' id='R6' ".$checkedR6." /></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
		echo "<td class='PermessoCode'>R7</td>\n";
		echo "<td class='PermessoDescription'>Configurazione: accesso al pannello Costo viaggi</td>\n";
		echo "<td class='checkbox'><input type='checkbox' name='R7' id='R7' ".$checkedR7." /></td>\n";
	echo "</tr>\n";
echo "</table>";





echo "<table class=\"TabellaPermessiUtente\" cellpadding='2' cellspacing='0'>\n";
	echo "<tr>\n";
		echo "<td class='SalvaUtente'>\n";
		echo "<input type='hidden' name='ID_USR' value='".$ID_USR."' />\n";
		if($ID_IMPDisabled=='disabled')
			echo "<input type='hidden' name='ID_IMP_HIDDEN' value='".$ID_IMP."' />\n";
		echo "<input class='PermessiButton' type='button' name='SubmitForm' value='SALVA UTENTE' onClick='javascript:check_PermessiUtente(\"create\");' />\n";
		if(isset($_GET['pri']))
			echo "<input class='PermessiButton' type='button' name='SubmitForm_Duplicate' value='DUPLICA UTENTE' onClick='javascript:check_PermessiUtente(\"duplicate\");' />\n";
		echo "</td>\n";
	echo "</tr>\n";
echo "</table>";


echo "</form></div>";


require_once("__includes/COMMON_sleepForgEdit.php");
?>