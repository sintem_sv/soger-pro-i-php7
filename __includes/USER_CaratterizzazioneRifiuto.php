<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;

$ID_RIF=$_GET['filter'];

#
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_schede_rifiuti");

$FEDIT->FGE_SetFormFields(array("ID_CER", "ID_SF", "ID_UMIS", "CAR_AnalisiNecessaria", "CAR_DataAnalisi", "CAR_DataScadenzaAnalisi", "CAR_NumDocumento", "CAR_Laboratorio", "CAR_Responsabile" ), "user_schede_rifiuti");
$FEDIT->FGE_SetFormFields(array("ID_RIFPROD", "ID_RIFPROD_F", "ID_RIFTYPE", "ID_FONTE_RIF", "ID_OR_RIF", "CAR_Ciclo", "CAR_MatPrime", "CAR_prep_pericolosi", "et_comp_per", "ID_DEST_RIF", "CAR_Note"),"user_schede_rifiuti");

$ProfiloFields = "user_schede_rifiuti";
include("SOGER_CampiProfilo.php");

$FEDIT->FGE_DisableFields(array("ID_CER","descrizione","ID_SF","ID_UMIS"),"user_schede_rifiuti");

$FEDIT->FGE_DescribeFields();
$FEDIT->FGE_SetValue("CAR_LastUpd", date("d/m/Y"), "user_schede_rifiuti");

#

$FEDIT->FGE_HideFields(array("produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");

#
$FEDIT->FGE_SetTitle("ID_CER","Rifiuto","user_schede_rifiuti");
$FEDIT->FGE_SetTitle("CAR_AnalisiNecessaria","Analisi","user_schede_rifiuti");
$FEDIT->FGE_SetTitle("ID_RIFPROD","Provenienza e composizione del rifiuto","user_schede_rifiuti");
$FEDIT->FGE_SetTitle("ID_DEST_RIF","Destinazione del rifiuto","user_schede_rifiuti");
$FEDIT->FGE_SetTitle("CAR_Note","Note","user_schede_rifiuti");
$FEDIT->FGE_SetBreak("CAR_NumDocumento","user_schede_rifiuti");

#

$FEDIT->FGE_LookUpCFG("ID_CER","user_schede_rifiuti");
$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
//$FEDIT->FGE_SetSelectFields(array("descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_rifiuti");
$FEDIT->FGE_SetSelectFields(array("descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
$FEDIT->FGE_SetFilter("ID_RIF",$ID_RIF,"user_schede_rifiuti");
$ProfiloFields = "user_schede_rifiuti";
include("SOGER_FiltriProfilo.php");
$FEDIT->FGE_SetOrder("lov_cer:COD_CER");
//$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_rifiuti");
$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();

$FEDIT->FGE_LookUpDefault("ID_SF","user_schede_rifiuti");
$FEDIT->FGE_LookUpDefault("ID_UMIS","user_schede_rifiuti");
$FEDIT->FGE_LookUpDefault("ID_RIFPROD","user_schede_rifiuti");
$FEDIT->FGE_LookUpDefault("ID_RIFPROD_F","user_schede_rifiuti");
$FEDIT->FGE_LookUpDefault("ID_RIFTYPE","user_schede_rifiuti");
$FEDIT->FGE_LookUpDefault("ID_FONTE_RIF","user_schede_rifiuti");
$FEDIT->FGE_LookUpDefault("ID_OR_RIF","user_schede_rifiuti");
$FEDIT->FGE_LookUpDefault("ID_DEST_RIF","user_schede_rifiuti");
$FEDIT->FGE_LookUpDone();
#


$js ="<script type=\"text/javascript\">\n";
$js.= <<< ENDJS
function show_hide(id){
	var obj=document.getElementById(id);
	if(obj.style.display=='none'){
		obj.style.display='block';
		obj.style.zindex=10;
		}
	else{
		obj.style.display='none';
		obj.style.zindex=-1;
	}
}

/*
estensioni=new Array("jpg", "jpeg", "JPG", "JPEG");
posizione_punto=path.lastIndexOf(".");
lunghezza_stringa=path.length;
estensione=path.substring(posizione_punto+1,lunghezza_stringa);
*/

				
ENDJS;
$js.="</script>\n";

echo $js;

#css firefox
$topFF=540;
//if($SOGER->UserData['core_usersFANGHI']==1) $topFF+=85;
$topIE=582;
//if($SOGER->UserData['core_usersFANGHI']==1) $topIE+=85;
$css="<style type=\"text/css\">\n";
$css.="div#btn_print_car{\n";
$css.="position: absolute; top: ".$topFF."px; left: 690px; z-index: 10;";
$css.="}\n";
$css.="div#btn_allega_img{\n";
$css.="position: absolute; top: ".$topFF."px; left: 548px; z-index: 10;";
$css.="}\n";
$css.="div#UPL{\n";
$css.="position:absolute;margin-left:300px;margin-top:200px;z-index: 10; height: 225px;  padding: 3px; border: 2px #5C7D99 SOLID; background-color: #FFFFFF;";
$css.="}\n";
$css.="</style>\n";

#css ie
$css.="<!--[if IE]>";
$css.="<style type=\"text/css\">\n";
$css.="div#btn_print_car{\n";
$css.="position: absolute; top: ".$topIE."px; left: 610px; z-index: 10;";
$css.="}\n";
$css.="div#btn_allega_img{\n";
$css.="position: absolute; top: ".$topIE."px; left: 430px; z-index: 10;";
$css.="}\n";
$css.="div#UPL{\n";
$css.="position:absolute;margin-left:-600px;margin-top:200px;z-index: 10; height: 225px;  padding: 3px; border: 2px #5C7D99 SOLID; background-color: #FFFFFF;";
$css.="}\n";
$css.="</style>\n";
$css.="<![endif]-->";

/*
$css.="<style type=\"text/css\">\n";
$css.="div#UPL{\n";
$css.="position:absolute;margin-left:300px;margin-top:200px;z-index: 10; height: 225px;  padding: 3px; border: 2px #5C7D99 SOLID; background-color: #FFFFFF;";
$css.="}\n";
$css.="</style>\n";
*/

echo $css;

$divUpload ="<div id=\"UPL\" style=\"display:none;\">\n";

	$divUpload.="<form name=\"UploadImmagine\" method=\"post\" action=\"__mov_snipplets/UploadImg.php\" style=\"width:400px;\" enctype=\"multipart/form-data\">\n";
		$divUpload.="<fieldset class=\"FGEfieldset\" style=\"width:80%;margin-left:30px;\">\n";
		$divUpload.="<legend class=\"FGElegend\">CARICA UN' IMMAGINE</legend>\n";
			
			$divUpload.="<div class=\"FGE1Col\" style=\"margin-left:30px;margin-top:30px;\">\n";
				$divUpload.="<label for=\"file\" class=\"FGEmandatory\">Percorso</label><br/>\n";
				$divUpload.="<input type=\"file\" tabindex=\"1000\" class=\"FGEinput\" id=\"user_autisti:nome\" name=\"immagine\" size=\"30\" maxlength=\"30\" value=\"\" onfocus=\"this.style.background='yellow';\" onblur=\"this.style.background='white';\"/>\n";
			$divUpload.="</div>\n";
			
			$divUpload.="<div class=\"FGESubmitRow\" style=\"margin-left:30px;margin-top:30px;\">\n";

				$divUpload.="<input type=\"submit\" class=\"FGEbutton\" value=\"carica\" onclick=\"javascript:show_hide('UPL');\" onfocus=\"this.style.background='yellow';this.style.color='#034373';\" onblur=\"this.style.background='#034373';this.style.color='white';\"/>\n";

				$divUpload.="<input type=\"button\" class=\"FGEbutton\" value=\"annulla\" onclick=\"javascript:show_hide('UPL');\" onfocus=\"this.style.background='yellow';this.style.color='#034373';\" onblur=\"this.style.background='#034373';this.style.color='white';\"/>\n";

				$divUpload.="<input type=\"hidden\" name=\"ID_RIF\" value=\"".$_SESSION["IDRifiutoCaratt"]."\" />";
			$divUpload.="</div>\n";
		$divUpload.="</fieldset>\n";
	$divUpload.="</form>\n";


$divUpload.="</div>\n";

//echo $divUpload;



$btn ="<div id=\"btn_print_car\">\n";
$btn.="<input type=\"button\" class=\"FGEbutton\" name=\"\" value=\"Stampa scheda di caratterizzazione\" onclick=\"javascript:SaveAndShow(".$_SESSION["IDRifiutoCaratt"].");\" />\n";
$btn.="</div>\n";
$btn.="<div id=\"btn_allega_img\">\n";
$btn.="<input type=\"button\" class=\"FGEbutton\" name=\"\" value=\"Allega un' immagine\" onclick=\"javascript:show_hide('UPL');\"/>\n";
$btn.="</div>\n";	

//echo $btn;


echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"Salva");
require_once("__includes/COMMON_sleepForgEdit.php");
?>