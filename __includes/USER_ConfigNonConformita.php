<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;

if(count($_POST)>1){
	//die(var_dump($_POST));
	$post=array_keys($_POST);
	for($p=0;$p<count($post);$p++){
		if($post[$p]!="salva"){
			$nc_config ="UPDATE core_impianti_nc ";
			$nc_config.="SET ID_NCL='".$_POST[$post[$p]]."' ";
			$nc_config.="WHERE ID_NC=".$post[$p]." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
			$FEDIT->SDBWrite($nc_config,true,false);
			//print_r($nc_config."<hr>");
			$SOGER->SetFeedback("Configurazione salvata correttamente","2");
			}
		}	
	}

echo "<div class=\"FGEDataGridTitle\"><div>Gestione procedure � Configurazione non conformit�</div></div>";

if($SOGER->AppLocation!="login" && $SOGER->AppLocation!="login_documents" && count($SOGER->AppNavDIVs)>0) {
	$tmp .= "<div id=\"NavDiv\">\n";
	$tmp .= "<!-- NAV DIVs BEGIN -->\n[NAV_STUCTURE]\n<!-- NAV DIVs END -->\n";
	$SOGER->DIVmake_Navigation();
	$tmp .= "</div>\n";
	echo $tmp;
	}




$sql="SELECT * FROM core_impianti_nc WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1;";
$FEDIT->SDBRead($sql,"DbRecordset");

# � la prima volta che accedo
if($FEDIT->DbRecsNum==0){
	$sql="SELECT ID_NC, default_ID_NCL FROM lov_nc";
	$FEDIT->SDBRead($sql,"DbRecordSet");
	for($i=0;$i<count($FEDIT->DbRecordSet);$i++){
		$setup ="INSERT INTO core_impianti_nc VALUES ('";
		$setup.=$SOGER->UserData['core_usersID_IMP']."', ".$FEDIT->DbRecordSet[$i]['ID_NC'].", ";
		$setup.=$FEDIT->DbRecordSet[$i]['default_ID_NCL'].", ";
		switch($SOGER->UserData['workmode']){
			case "produttore":
				$setup.="1,0,0,0";
				break;
			case "trasportatore":
				$setup.="0,1,0,0";
				break;
			case "destinatario":
				$setup.="0,0,1,0";
				break;
			case "intermediario":
				$setup.="0,0,0,1";
				breeak;
			}
		$setup.=" );";
		$FEDIT->SDBWrite($setup,true,false);
		}
	}

# layout

$SQL="SELECT DISTINCT condizione FROM lov_nc ORDER BY ID_NC";
$FEDIT->SdbRead($SQL, "DbRecordSetConds");

echo "<form style=\"padding-top:50px;\" name='ConfigNonConformita' method='post' action='".$_SERVER['PHP_SELF']."'>\n";

for($c=0;$c<count($FEDIT->DbRecordSetConds);$c++){
	echo "<table class='config_nc_table' cellspacing='0' cellpadding='2'>\n";

		## header
		echo "<tr>\n";
		echo "<th colspan='5'>\n";
		echo $FEDIT->DbRecordSetConds[$c]['condizione'];
		echo "</th>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "<th>Condizione</th>\n";
		echo "<th>Tipo di documento</th>\n";
		echo "<th>Non conformit�</th>\n";
		echo "<th>Osservazione</th>\n";
		echo "<th>Suggerimento</th>\n";
		echo "</tr>\n";


		# rows
		$SQL="SELECT * FROM lov_nc WHERE condizione='".addslashes($FEDIT->DbRecordSetConds[$c]['condizione'])."'";
		$FEDIT->SdbRead($SQL, "DbRecordSet");
		for($r=0;$r<count($FEDIT->DbRecordSet);$r++){
			
			$frase ="Rifiuto ";
			
			if($FEDIT->DbRecordSet[$r]['pericoloso']=='1')
				$frase.="pericoloso ";
			else
				$frase.="non pericoloso ";

			if($FEDIT->DbRecordSet[$r]['miscela_reazione_lega']=='1')
				$frase.="( miscela, da reazione o lega ).";
			else
				$frase.="( non � miscela, da reazione o lega ).";

			echo "<tr>\n";
				echo "<td width='500'>".$frase."</td>\n";
				echo "<td width='200'>".$FEDIT->DbRecordSet[$r]['documento']."</td>\n";
				$SQL="SELECT ID_NCL FROM core_impianti_nc WHERE ID_NC=".$FEDIT->DbRecordSet[$r]['ID_NC']." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
				$FEDIT->SdbRead($SQL, "RadioValue");
				switch($FEDIT->RadioValue[0]['ID_NCL']){
					case '1':
						$checked1="checked";
						$checked2="";
						$checked3="";
						break;
					case '2':
						$checked1="";
						$checked2="checked";
						$checked3="";
						break;
					case '3':
						$checked1="";
						$checked2="";
						$checked3="checked";
						break;
					}
				echo "<td width='110' align='center'><input type='radio' name='".$FEDIT->DbRecordSet[$r]['ID_NC']."' value='1' ".$checked1."></td>\n";
				echo "<td width='70' align='center'><input type='radio' name='".$FEDIT->DbRecordSet[$r]['ID_NC']."' value='2' ".$checked2."></td>\n";
				echo "<td width='70' align='center'><input type='radio' name='".$FEDIT->DbRecordSet[$r]['ID_NC']."' value='3' ".$checked3."></td>\n";
			echo "</tr>\n";
			}

	echo "</table>";
	}

echo "<input style=\"margin:20px;\" class=\"FGEbutton\" type=\"submit\" name=\"salva\" value=\"salva\" />";

echo "</form>";

#
require_once("__includes/COMMON_sleepForgEdit.php");
?>