<?php
session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__libs/SQLFunct.php");
require_once("../__libs/SogerECO_funct.php");
require_once("COMMON_wakeForgEdit.php");
require_once("COMMON_wakeSoger.php");



if($_GET['NMOVto']!='scheduled'){
	$NMOVto	= $_GET['NMOVto'];
	}
else{
	$sql ="SELECT MAX(NMOV) AS MAXNMOV FROM user_movimenti WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND FISCALIZZATO=0 ";
	$sql.="AND NMOV<>9999999 ";
	include("SOGER_DirectProfilo.php");
	$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
	if(isset($FEDIT->DbRecordSet))
		$NMOVto=$FEDIT->DbRecordSet[0]['MAXNMOV'];
	else
		$NMOVto=0;
	}


##########################################
#
# Aggiorno FKEdisponibilita prima della fiscalizzazione solo per i registri con meno di 100 movimenti
#
##########################################

$workmode	= $SOGER->UserData['workmode'];

if($workmode=='produttore' AND $NMOVto<=100){
	$ID_IMP		= $SOGER->UserData['core_usersID_IMP'];
	$TableName	= "user_movimenti";
	require("../__scripts/MovimentiDispoMOV.php");
	## aggiorno riferimenti carico
	$LegamiSISTRI	= false;
	require("../__scripts/MovimentiRifMovCarico.php");
	}

##########################################






$TableSpec = "user_movimenti.";

$sql ="SELECT * FROM user_movimenti WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND FISCALIZZATO=0 ";

# fiscalizza solo movimenti dello stesso rifiuto
if($SOGER->UserData['core_usersmov_fiscalize_rif']==1 && isset($_GET['ID_RIF']))
	$sql.=" AND ID_RIF='".$_GET['ID_RIF']."' ";

#
if($NMOVto==9999999)
	$sql.=" AND (NMOV<".$NMOVto." OR (NMOV=".$NMOVto." AND ID_MOV<=".$_GET['ID_MOV'].")) ";
else
	$sql.=" AND NMOV<=".$NMOVto." ";

include("SOGER_DirectProfilo.php");
$sql.=" ORDER BY NMOV ASC";

//echo $sql;

$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

$MovFisc=array();





// Funzione che ordina per DTMOV. Quando vi sono due movimenti con stessa DTMOV, li ordina per ID_MOV
function OrdinaDT($a, $b){
	
	if($a['DTMOV']=='0000-00-00') //-> movimenti NMOV 9999999
		$a['DTMOV']=date('Y')."-12-31"; //-> solo per ordinamento, no override
	if($b['DTMOV']=='0000-00-00') //-> movimenti NMOV 9999999
		$b['DTMOV']=date('Y')."-12-31"; //-> solo per ordinamento, no override

	$aq = $a['DTMOV'];
	$bq = $b['DTMOV'];


	if (strtotime($aq) == strtotime($bq) ){
		//return ($a['ID_MOV'] < $b['ID_MOV']) ? -1 : 1;
		return ($a['NMOV'] < $b['NMOV']) ? -1 : 1;
		}
	else{
		return (strtotime($aq) < strtotime($bq)) ? -1 : 1;
		}
	}



// Funzione che a parit� di DTMOV ordina l' array anteponendo i carichi agli scarichi. Quando vi sono due movimenti dello stesso tipo, li ordina per ID_MOV
function OrdinaCS($a, $b){
	
	if($a['DTMOV']=='0000-00-00') //-> movimenti NMOV 9999999
		$a['DTMOV']=date('Y')."-12-31"; //-> solo per ordinamento, no override
	if($b['DTMOV']=='0000-00-00') //-> movimenti NMOV 9999999
		$b['DTMOV']=date('Y')."-12-31"; //-> solo per ordinamento, no override

	$aq = $a['DTMOV'];
	$bq = $b['DTMOV'];

	if (strtotime($aq) == strtotime($bq) ){
		 # due mov di tipo diverso
		if($a['TIPO']!=$b['TIPO']){
			if( $a['TIPO']=='C' AND $b['TIPO']=='S' ){
				return 0;
				}
			if( $a['TIPO']=='S' AND $b['TIPO']=='C' ){
				return 1;
				}
			}
		# due mov dello stesso tipo
		else{
			//if($a['ID_MOV'] > $b['ID_MOV'])
			if($a['NMOV'] > $b['NMOV'])
				return 1;
			else
				return 0;
			}
		}

	return (strtotime($aq) < strtotime($bq)) ? -1 : 1;
	}

//print_r($MovFisc);
if($FEDIT->DbRecsNum>0){
	if($SOGER->UserData['core_usersmov_fiscalize_order']==1){
		usort($FEDIT->DbRecordSet, "OrdinaCS");
		}
	else{
		usort($FEDIT->DbRecordSet, "OrdinaDT");
		}
	}
//print_r($MovFisc);








for($m=0;$m<count($FEDIT->DbRecordSet);$m++){

	// primo record estratto
	if($m==0){
		$MovFisc[0]=$FEDIT->DbRecordSet[0];
		//$MovFisc[0]["FKErifIND"]="|".$FEDIT->DbRecordSet[0]["NMOV"]."|";
		$MovFisc[0]["FKErifIND"]="|".$FEDIT->DbRecordSet[0]["ID_MOV"]."|";
		$RecBuffer =$FEDIT->DbRecordSet[0];
		}
	else{
		if($FEDIT->DbRecordSet[$m]["TIPO"]=="S"){
			$MovFisc[] = $FEDIT->DbRecordSet[$m];
			//$MovFisc[count($MovFisc)-1]["FKErifIND"]="|".$FEDIT->DbRecordSet[$m]["NMOV"]."|";
			$MovFisc[count($MovFisc)-1]["FKErifIND"]="|".$FEDIT->DbRecordSet[$m]["ID_MOV"]."|";
			$RecBuffer = $FEDIT->DbRecordSet[$m];
			}
		else{ // � un carico, devo capire se posso fare un merge con un carico precedente o nuova riga
			
			if($SOGER->UserData['workmode']=="produttore"){
				## il merge col carico precedente lo posso fare esclusivamente se l' ultimo movimento di quel rifiuto in MovFisc � un carico
				$MovTypeBuffer='S';
				for($r=0;$r<count($MovFisc);$r++){
					if($FEDIT->DbRecordSet[$m]["ID_RIF"]==$MovFisc[$r]["ID_RIF"]){
						$MovTypeBuffer=$MovFisc[$r]["TIPO"];
						$indexToMerge=$r;
						}
					}
				
				$Carico1=explode("-", $MovFisc[$indexToMerge]["DTMOV"]);
				$Carico1=$Carico1[1]."/".$Carico1[2]."/".$Carico1[0];

				$Carico2=explode("-", $FEDIT->DbRecordSet[$m]["DTMOV"]);
				$Carico2=$Carico2[1]."/".$Carico2[2]."/".$Carico2[0];

				$GiorniTraCarichi=dateDiff("d", $Carico1, $Carico2);
				
				// aggiungo controllo su intervallo temporale, non superiore a 12 giorni ( 					
				if($MovTypeBuffer=='C' && $GiorniTraCarichi<=12 && $SOGER->UserData['core_usersmov_fiscalize_merge']==1){ 
					$MovFisc[$indexToMerge]["quantita"]			+= $FEDIT->DbRecordSet[$m]["quantita"];
					$MovFisc[$indexToMerge]["qta_hidden"]		+= $FEDIT->DbRecordSet[$m]["qta_hidden"];
					$MovFisc[$indexToMerge]["pesoN"]			+= $FEDIT->DbRecordSet[$m]["pesoN"];
					//$MovFisc[$indexToMerge]["FKErifIND"]		.= "|".$FEDIT->DbRecordSet[$m]["NMOV"]."|";
					$MovFisc[$indexToMerge]["FKErifIND"]		.= "|".$FEDIT->DbRecordSet[$m]["ID_MOV"]."|";
					unset($indexToMerge);
					}
				else{
					$MovFisc[]=$FEDIT->DbRecordSet[$m];
					//$MovFisc[count($MovFisc)-1]["FKErifIND"]="|".$FEDIT->DbRecordSet[$m]["NMOV"]."|";
					$MovFisc[count($MovFisc)-1]["FKErifIND"]="|".$FEDIT->DbRecordSet[$m]["ID_MOV"]."|";
					}
				}
			else{
				// non sono produttore!
				$MovFisc[]=$FEDIT->DbRecordSet[$m];
				//$MovFisc[count($MovFisc)-1]["FKErifIND"]="|".$FEDIT->DbRecordSet[$m]["NMOV"]."|";
				$MovFisc[count($MovFisc)-1]["FKErifIND"]="|".$FEDIT->DbRecordSet[$m]["ID_MOV"]."|";
				}
			}
		}		
	}


$Response="";


for($r=0;$r<count($MovFisc);$r++){
	

	//$MovFisc[$r]["ID_MOV"]		="";
	$MovFisc[$r]["NMOV"]			="";
	$MovFisc[$r]["FKElastCarico"]	="";
	$MovFisc[$r]["FKErifCarico"]	="";

	foreach($MovFisc[$r] as $k => $v){
		if($k=="ID_RIF"){
			$SQL ="SELECT descrizione, COD_CER FROM user_schede_rifiuti ";
			$SQL.="JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
			$SQL.="WHERE ID_RIF='".$MovFisc[$r]["ID_RIF"]."'";
			$FEDIT->SDBRead($SQL,"DbRecordSet",true,false);
			$RIF_desc=$FEDIT->DbRecordSet[0]['COD_CER']." ".$FEDIT->DbRecordSet[0]['descrizione'];
			}
		$Response.="[K]".$k."[V]".$v."[;]";
		if($k=="approved") $Response.="[K]RIF_desc[V]".$RIF_desc."[;][END]";
		}
	}


# fiscalizzo tutti i movimenti o solo quelli di un ID_RIF?
if(count($MovFisc)>0){
	if($SOGER->UserData['core_usersmov_fiscalize_rif']==1 && isset($_GET['ID_RIF']))
		$Response.=$_GET['ID_RIF']."|";
	else
		$Response.="garbage|";
	}

$sql = "SELECT MAX(NMOV) AS NMOV, MAX(DTMOV) AS DTMOV FROM user_movimenti_fiscalizzati WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND NMOV<>9999999";
$TableSpec = "user_movimenti_fiscalizzati.";
require("SOGER_DirectProfilo.php");
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

if($FEDIT->DbRecsNum>0) {
	$NMOV=$FEDIT->DbRecordSet[0]["NMOV"];
	$DTMOV=$FEDIT->DbRecordSet[0]["DTMOV"];
	$NMOV++;
	}
else{
	$NMOV=1;
	$DTMOV=date('Y')."-01-01";
	}

$_SESSION["NMOV2FISCAL"]	= $NMOV;
$_SESSION["NMOV2FISCALzz"]	= $NMOVto;

if(count($MovFisc)>0){
	# data massima presente su registro fiscale
	$Response.=$DTMOV."|";
	}


//print_r($MovFisc);die();
$Response=utf8_encode($Response);
echo $Response;

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>