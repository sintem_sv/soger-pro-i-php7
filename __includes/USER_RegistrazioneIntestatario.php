<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("core_impianti");
$FEDIT->FGE_SetFormFields(array("ID_IMP","description","codfisc","indirizzo","ID_COM","IN_ITALIA","telefono","fax","ente", "ID_ATECO_07", "TotAddettiUL", "REA", "LR_nome", "LR_cognome", "MUD_PS_DEST"),"core_impianti");
$FEDIT->FGE_DescribeFields();	
$FEDIT->FGE_Disablefields("ID_IMP","core_impianti");

$FEDIT->FGE_LookUpDefault("ID_COM","core_impianti",true);

$FEDIT->FGE_LookUpCFG("ID_ATECO_07","core_impianti",true);
$FEDIT->FGE_UseTables("lov_ateco2007");
$FEDIT->FGE_SetSelectFields(array("ATECO_07"),"lov_ateco2007");
$FEDIT->FGE_SetOrder("lov_ateco2007:ATECO_07");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();

$FEDIT->FGE_Hidefields("IN_ITALIA","core_impianti");
$FEDIT->FGE_SetTitle("ID_IMP","Intestatario Registro","core_impianti");
$FEDIT->FGE_SetTitle("ID_ATECO_07","MUD","core_impianti");

echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva");
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>
