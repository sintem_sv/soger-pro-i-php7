<?php
$javascript ="<script type='text/javascript'>\n";
$javascript.= <<< JAVASCRIPT

function showMapInterface(canCreate, gotViaggi, ID_MZ_TRA, DA_INDIRIZZO, DA_COMUNE, DA_PROVINCIA, DA_CAP, DA_NAZIONE, A_INDIRIZZO, A_COMUNE, A_PROVINCIA, A_CAP, A_NAZIONE, PRI, FILTER, ANNO, MATRIX){
	document.getElementById('hidden_ID_MZ_TRA').value=ID_MZ_TRA;
	
	document.getElementById('hidden_DA_INDIRIZZO').value=DA_INDIRIZZO;
	document.getElementById('hidden_DA_COMUNE').value=DA_COMUNE;
	document.getElementById('hidden_DA_PROVINCIA').value=DA_PROVINCIA;
	document.getElementById('hidden_DA_CAP').value=DA_CAP;
	document.getElementById('hidden_DA_NAZIONE').value=DA_NAZIONE;

	document.getElementById('hidden_A_INDIRIZZO').value=A_INDIRIZZO;
	document.getElementById('hidden_A_COMUNE').value=A_COMUNE;
	document.getElementById('hidden_A_PROVINCIA').value=A_PROVINCIA;
	document.getElementById('hidden_A_CAP').value=A_CAP;
	document.getElementById('hidden_A_NAZIONE').value=A_NAZIONE;

	document.getElementById('hidden_PRI').value=PRI;
	document.getElementById('hidden_FILTER').value=FILTER;
	document.getElementById('hidden_gotViaggi').value=gotViaggi;
	document.getElementById('hidden_ANNO').value=ANNO;
	document.getElementById('hidden_MATRIX').value=MATRIX;

	if(!gotViaggi) document.MapConfig["action"][1].disabled=true;

	var obj=document.getElementById('MapInterface');
		if(obj.style.display=='none'){
			obj.style.display='block';
			obj.style.zindex=10;
			}
		else{
			obj.style.display='none';
			obj.style.zindex=-1;
			}
		}

function Go2Map(){
	
	var ID_MZ_TRA=document.MapConfig["hidden_ID_MZ_TRA"].value;
	
	var DA_INDIRIZZO=document.MapConfig["hidden_DA_INDIRIZZO"].value;
	var DA_COMUNE=document.MapConfig["hidden_DA_COMUNE"].value;
	var DA_PROVINCIA=document.MapConfig["hidden_DA_PROVINCIA"].value;
	var DA_CAP=document.MapConfig["hidden_DA_CAP"].value;
	var DA_NAZIONE=document.MapConfig["hidden_DA_NAZIONE"].value;

	var A_INDIRIZZO=document.MapConfig["hidden_A_INDIRIZZO"].value;
	var A_COMUNE=document.MapConfig["hidden_A_COMUNE"].value;
	var A_PROVINCIA=document.MapConfig["hidden_A_PROVINCIA"].value;
	var A_CAP=document.MapConfig["hidden_A_CAP"].value;
	var A_NAZIONE=document.MapConfig["hidden_A_NAZIONE"].value;

	var ID_VIAGGIO=document.MapConfig["hidden_ID_VIAGGIO"].value;
	var action=document.MapConfig["hidden_action"].value;
	var PRI=document.MapConfig["hidden_PRI"].value;
	var FILTER=document.MapConfig["hidden_FILTER"].value;
	var ANNO=document.MapConfig["hidden_ANNO"].value;
	var MATRIX=document.MapConfig["hidden_MATRIX"].value;

	var MAPpars="ID_MZ_TRA=" + ID_MZ_TRA;
	MAPpars+="&da_indirizzo=" + DA_INDIRIZZO + "&da_comune=" + DA_COMUNE + "&da_provincia=" + DA_PROVINCIA + "&da_cap=" + DA_CAP + "&da_nazione=" + DA_NAZIONE;
	MAPpars+="&a_indirizzo=" + A_INDIRIZZO + "&a_comune=" + A_COMUNE + "&a_provincia=" + A_PROVINCIA + "&a_cap=" + A_CAP + "&a_nazione=" + A_NAZIONE;
	MAPpars+="&ID_VIAGGIO=" + ID_VIAGGIO + "&action=" + action + "&" + PRI + "=" + FILTER + "&anno="+ ANNO + "&matrix=" + MATRIX;
	document.getElementById('MapInterface').style.display='none';
	location.replace('__scripts/statusCUSTOM_MovScaricoMap.php?'+MAPpars);
	}

JAVASCRIPT;
$javascript.="</script>";
echo $javascript;


		$css ="<style type=\"text/css\">\n";

		$css.="div#MapInterface{";
		$css.="position: absolute;";
		$css.="margin-left:-600px;";
		$css.="margin-top:130px;";
		$css.="z-index:10;";
		$css.="height:260px;";
		$css.="padding:3px;";
		$css.="background-color:#ffffff;";
		$css.="border:2px solid black;";
		$css.="}\n";

		$css.="div#ListaViaggi{";
		$css.="padding-left:60px;";
		$css.="padding-top:10px;";
		$css.="}\n";

		$css.="</style>\n";
		
		echo $css;
		
		$MapInterface ="<div id=\"MapInterface\" style=\"display:none;\">\n";
		$MapInterface.="<form id=\"MapConfig\" name=\"MapConfig\" style=\"width:400px;\" action=\"\" method=\"post\">\n";
		$MapInterface.="<fieldset class=\"FGEfieldset\" style=\"width:80%;margin-left:30px;\">\n";
		$MapInterface.="<legend class=\"FGElegend\">VISUALIZZA PERCORSO</legend>\n";
			
			$MapInterface.="<h3>Il movimento selezionato non fa parte di nessun viaggio, come si desidera procedere?</h3>";
			$MapInterface.="<input type=\"radio\" name=\"action\" value=\"create\" checked /> crea un nuovo viaggio<br />\n";
			$MapInterface.="<input type=\"radio\" name=\"action\" value=\"addTappa\" /> aggiungi ad altro viaggio:<br />\n";

			$MapInterface.="<div id=\"ListaViaggi\">\n";

			if($gotViaggi=='true'){
				$MapInterface.="elenco viaggi";
				}
			else
				$MapInterface.="nessun viaggio compatibile rilevato";

			$MapInterface.="</div>\n";

		$MapInterface.="<div class=\"FGESubmitRow\">";
		$MapInterface.="<input type=\"button\" onblur=\"this.style.background='#034373';this.style.color='white';\" onfocus=\"this.style.background='yellow';this.style.color='#034373';\" onclick=\"javascript:Go2Map(document.getElementById('hidden_ID_MZ_TRA').value, document.getElementById('hidden_DA_INDIRIZZO').value, document.getElementById('hidden_DA_COMUNE').value, document.getElementById('hidden_DA_PROVINCIA').value, document.getElementById('hidden_DA_CAP').value, document.getElementById('hidden_DA_NAZIONE').value, document.getElementById('hidden_A_INDIRIZZO').value, document.getElementById('hidden_A_COMUNE').value, document.getElementById('hidden_A_PROVINCIA').value, document.getElementById('hidden_A_CAP').value, document.getElementById('hidden_A_NAZIONE').value, document.getElementById('hidden_PRI').value, document.getElementById('hidden_FILTER').value );\" value=\"conferma\" class=\"FGEbutton\">";
		$MapInterface.="<input type=\"button\" onblur=\"this.style.background='#034373';this.style.color='white';\" onfocus=\"this.style.background='yellow';this.style.color='#034373';\" onclick=\"javascript:showMapInterface(true, document.getElementById('hidden_gotViaggi').value);\" value=\"annulla\" class=\"FGEbutton\" style=\"margin-left:8px;\">";
		$MapInterface.="</div>";

		$MapInterface.="</fieldset>\n";
		$MapInterface.="<input type=\"hidden\" id=\"hidden_action\" name=\"hidden_action\" value=\"create\" />\n";
		$MapInterface.="<input type=\"hidden\" id=\"hidden_ID_VIAGGIO\" name=\"hidden_ID_VIAGGIO\" value=\"0\" />\n";

		$MapInterface.="<input type=\"hidden\" id=\"hidden_ID_MZ_TRA\" name=\"hidden_ID_MZ_TRA\" value=\"0\" />\n";
		
		$MapInterface.="<input type=\"hidden\" id=\"hidden_DA_INDIRIZZO\" name=\"hidden_DA_INDIRIZZO\" value=\"0\" />\n";
		$MapInterface.="<input type=\"hidden\" id=\"hidden_DA_COMUNE\" name=\"hidden_DA_COMUNE\" value=\"0\" />\n";
		$MapInterface.="<input type=\"hidden\" id=\"hidden_DA_PROVINCIA\" name=\"hidden_DA_PROVINCIA\" value=\"0\" />\n";
		$MapInterface.="<input type=\"hidden\" id=\"hidden_DA_CAP\" name=\"hidden_DA_CAP\" value=\"0\" />\n";
		$MapInterface.="<input type=\"hidden\" id=\"hidden_DA_NAZIONE\" name=\"hidden_DA_NAZIONE\" value=\"0\" />\n";
		
		$MapInterface.="<input type=\"hidden\" id=\"hidden_A_INDIRIZZO\" name=\"hidden_A_INDIRIZZO\" value=\"0\" />\n";
		$MapInterface.="<input type=\"hidden\" id=\"hidden_A_COMUNE\" name=\"hidden_A_COMUNE\" value=\"0\" />\n";
		$MapInterface.="<input type=\"hidden\" id=\"hidden_A_PROVINCIA\" name=\"hidden_A_PROVINCIA\" value=\"0\" />\n";
		$MapInterface.="<input type=\"hidden\" id=\"hidden_A_CAP\" name=\"hidden_A_CAP\" value=\"0\" />\n";
		$MapInterface.="<input type=\"hidden\" id=\"hidden_A_NAZIONE\" name=\"hidden_A_NAZIONE\" value=\"0\" />\n";
		
		$MapInterface.="<input type=\"hidden\" id=\"hidden_PRI\" name=\"hidden_PRI\" value=\"0\" />\n";
		$MapInterface.="<input type=\"hidden\" id=\"hidden_FILTER\" name=\"hidden_FILTER\" value=\"0\" />\n";

		$MapInterface.="<input type=\"hidden\" id=\"hidden_gotViaggi\" name=\"hidden_gotViaggi\" value=\"false\" />\n";
		$MapInterface.="<input type=\"hidden\" id=\"hidden_ANNO\" name=\"hidden_ANNO\" value=\"0\" />\n";
		$MapInterface.="<input type=\"hidden\" id=\"hidden_MATRIX\" name=\"hidden_MATRIX\" value=\"0\" />\n";
		$MapInterface.="</form>\n";
		$MapInterface.="</div>\n";
	
		echo $MapInterface;

?>