<?php
	require_once("__includes/COMMON_ForgEditClassFiles.php");
	require_once("__includes/COMMON_wakeForgEdit.php");
	#
	global $SOGER;
	$TableName="user_movimenti_fiscalizzati";
	if($SOGER->UserData['workmode']=='produttore'){
		$LegamiSISTRI	= false;
		require("__scripts/MovimentiRifMovCarico.php");
		}
	require_once("__includes/COMMON_sleepForgEdit.php");
	require("__includes/USER_RegistroFiscaleSiNo.inc");

?>

<form name="StampaRegistro" method="get" action="" style="width: 90%">
<fieldset class="FGEfieldset">
<legend class="FGElegend" style="font-size:x-small;">Stampa Registro</legend>
<label for="da" class="">Da movimento n�</label> <input type="text" value="1" id="da" name="da" size="4" class="FGEinput" tabindex="50"/>
<label for="a" class="">a movimento n�</label> <input type="text" value="" id="a" name="a" size="4" class="FGEinput" tabindex="51"/><br/>
<!-- <label for="cartaBianca" class="">Stampa su carta bianca</label><input onchange="alert('Stampa fiscale non abilitata');this.checked=true;" type="checkbox" value="Cbianca" name="cartaBianca" id="cartaBianca" class="FGEinput" tabindex="52" checked /><br/> -->
<label for="cartaBianca" class="">Stampa su carta bianca</label><input type="checkbox" value="Cbianca" name="cartaBianca" id="cartaBianca" class="FGEinput" tabindex="52" checked /><br/>
<label for="Npagina" class="">Stampa numeri di pagina</label><input type="checkbox" value="StampaNpag" name="Npagina" id="Npagina" class="FGEinput" tabindex="53" checked /><br/>
<label for="RegVidimato" class="">Registro vidimato</label>
	<input type="radio" value="1" name="Vidimato" id="Vidimato1" class="FGEinput" tabindex="54" />S�
	<input type="radio" value="0" name="Vidimato" id="Vidimato0" class="FGEinput" tabindex="55" style="margin-left:10px;" checked />No<br />

<label for="paginaIniz" class="">Numero di pagina iniziale</label> <input type="text" size="4" value="1" name="paginaIniz" id="paginaIniz" class="FGEinput" tabindex="56"/>

<br/><br/>
<input type="button" class="FGEbutton" tabindex="55" value="stampa registro" onclick="javascript:RegistroPrintShowInterface();"/>

</fieldset>
</form>

<?php if($SOGER->UserData['core_usersFANGHI']==1){ ?>
	<form name="StampaRegistroFanghi" method="get" action="" style="width: 90%">
	<fieldset class="FGEfieldset">
	<legend class="FGElegend" style="font-size:x-small;">Stampa Registro Fanghi</legend>
	
	<label for="da" class="">Da movimento n�</label> <input type="text" value="1" id="da" name="da" size="4" class="FGEinput" tabindex="50"/>

	<label for="a" class="">a movimento n�</label> <input type="text" value="" id="a" name="a" size="4" class="FGEinput" tabindex="51"/><br/>

	<label for="paginaIniz" class="">Numero di pagina iniziale</label> <input type="text" size="4" value="1" name="paginaIniz" id="paginaIniz" class="FGEinput" tabindex="56"/>

	<br/><br/>
	<input type="button" class="FGEbutton" tabindex="55" value="stampa registro fanghi" onclick="javascript:RegistroFanghiPrintShowInterface();"/>

	</fieldset>
	</form>
<?php } ?>


<form name="StampaRegistroPerVidimazione" method="get" action="" style="width: 90%">
<fieldset class="FGEfieldset">
<legend class="FGElegend" style="font-size:x-small;">Numerazione Registro per Vidimazione</legend>

<label for="paginaTot" class="">Numero di pagine totali</label> <input type="text" size="4" value="1" name="paginaTot" id="paginaTot" class="FGEinput" tabindex="70"/>

<br />
<label for="paginaIniz" class="">Numero di pagina iniziale</label> <input type="text" size="4" value="1" name="paginaIniz" id="paginaIniz" class="FGEinput" tabindex="71"/>

<br />
<label for="RagSoc" class="">Stampa ragione sociale</label> <input type="checkbox" value="1" name="RagSoc" id="RagSoc" class="FGEinput" tabindex="72"/>

<br />
<label for="AnnoRegistro" class="">Anno di riferimento</label> <input type="text" size="4" value="<?php echo date('Y'); ?>" name="AnnoRegistro" id="AnnoRegistro" class="FGEinput" tabindex="73"/>

<br/><br/>
<input type="button" class="FGEbutton" tabindex="73" value="stampa numerazione" onclick="javascript:RegistroVidimazione(this.form);"/>
</fieldset>
</form>