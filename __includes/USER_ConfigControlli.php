<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;

$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("core_users");
$FEDIT->FGE_SetFormFields(array("ID_USR","ID_IMP"),"core_users");

$FEDIT->FGE_SetFormFields(array("AUT_SCAD_LOCK","FDA_LOCK","CONTO_TERZI_LOCK","CONTO_TERZI_CHECK","CHECK_DESTINATARIO","CHECK_TRASPORTATORE", "CHECK_MOV_ORDER","CHECK_DOUBLE_FIR", "ID_GDEP","GGedit_{$SOGER->UserData['workmode']}","RESTORE_NMOV", "FRM_pericolosi"),"core_users");

if($SOGER->UserData["workmode"]=="produttore") $FEDIT->FGE_SetFormFields(array("FRM_LAYOUT_SCARICO_INTERNO"),"core_users");
if($SOGER->UserData["workmode"]=="destinatario") $FEDIT->FGE_SetFormFields(array("FRM_LAYOUT_DEST"),"core_users");

//$FEDIT->FGE_SetFormFields(array("ID_ST_DET","ID_COMM","ID_CAR"),"core_users");

if($SOGER->UserData["workmode"]=="produttore" && $SOGER->UserData['core_impiantiREG_IND']=="1"){
	$FEDIT->FGE_SetFormFields(array("ID_DAY","ID_FISC_PROD","D2","mov_fiscalize_merge","mov_fiscalize_order"),"core_users");
	}
if($SOGER->UserData["workmode"]=="destinatario" && $SOGER->UserData['core_impiantiREG_IND']=="1"){
	$FEDIT->FGE_SetFormFields(array("ID_DAY","ID_FISC_DEST","D2","mov_fiscalize_merge","mov_fiscalize_order"),"core_users");
	}
if($SOGER->UserData["workmode"]=="trasportatore" && $SOGER->UserData['core_impiantiREG_IND']=="1"){
	$FEDIT->FGE_SetFormFields(array("ID_DAY","ID_FISC_TRASP","D2","mov_fiscalize_merge","mov_fiscalize_order"),"core_users");
	}
if($SOGER->UserData["workmode"]=="intermediario" && $SOGER->UserData['core_impiantiREG_IND']=="1"){
	$FEDIT->FGE_SetFormFields(array("ID_DAY","ID_FISC_INTERM","D2","mov_fiscalize_merge","mov_fiscalize_order"),"core_users");
	}

$FEDIT->FGE_SetFormFields(array("PRNT_HP4_HP8"),"core_users");


if($SOGER->UserData["core_usersR5"]=="0") {
	$FEDIT->FGE_DisableFields(array("AUT_SCAD_LOCK","FDA_LOCK","CONTO_TERZI_LOCK","CONTO_TERZI_CHECK","CHECK_DESTINATARIO","CHECK_TRASPORTATORE","CHECK_MOV_ORDER","CHECK_DOUBLE_FIR","ID_DAY","ID_GDEP","GGedit_{$SOGER->UserData['workmode']}","RESTORE_NMOV","FRM_pericolosi","D2","mov_fiscalize_merge", "mov_fiscalize_order","PRNT_HP4_HP8"),"core_users");
	//$FEDIT->FGE_DisableFields(array("ID_ST_DET","ID_COMM","ID_CAR"),"core_users");
	if($SOGER->UserData["workmode"]=="produttore"){
		$FEDIT->FGE_DisableFields("ID_FISC_PROD","core_users");
		}
	if($SOGER->UserData["workmode"]=="destinatario"){
		$FEDIT->FGE_DisableFields("ID_FISC_DEST","core_users");
		}
	if($SOGER->UserData["workmode"]=="trasportatore"){
		$FEDIT->FGE_DisableFields("ID_FISC_TRASP","core_users");
		}
	if($SOGER->UserData["workmode"]=="intermediario"){
		$FEDIT->FGE_DisableFields("ID_FISC_INTERM","core_users");
		}
	}


#
$FEDIT->FGE_DescribeFields();

//$FEDIT->FGE_LookUpDefault("ID_ST_DET","core_users");
//$FEDIT->FGE_LookUpDefault("ID_COMM","core_users");
//$FEDIT->FGE_LookUpDefault("ID_CAR","core_users");
$FEDIT->FGE_LookUpDefault("ID_IMP","core_users");
if($SOGER->UserData["workmode"]=="produttore" && $SOGER->UserData['core_impiantiREG_IND']=="1"){
	$FEDIT->FGE_LookUpDefault("ID_FISC_PROD","core_users");
	$FEDIT->FGE_LookUpDefault("ID_DAY","core_users");
	}
if($SOGER->UserData["workmode"]=="destinatario" && $SOGER->UserData['core_impiantiREG_IND']=="1"){
	$FEDIT->FGE_LookUpDefault("ID_FISC_DEST","core_users");
	$FEDIT->FGE_LookUpDefault("ID_DAY","core_users");
	}
if($SOGER->UserData["workmode"]=="trasportatore" && $SOGER->UserData['core_impiantiREG_IND']=="1"){
	$FEDIT->FGE_LookUpDefault("ID_FISC_TRASP","core_users");
	$FEDIT->FGE_LookUpDefault("ID_DAY","core_users");
	}
if($SOGER->UserData["workmode"]=="intermediario" && $SOGER->UserData['core_impiantiREG_IND']=="1"){
	$FEDIT->FGE_LookUpDefault("ID_FISC_INTERM","core_users");
	$FEDIT->FGE_LookUpDefault("ID_DAY","core_users");
	}
$FEDIT->FGE_LookUpDefault("ID_GDEP","core_users");

#

$FEDIT->FGE_HideFields(array("ID_IMP"),"core_users");
if($SOGER->UserData['core_impiantiMODULO_FDA']==0)
    $FEDIT->FGE_HideFields(array("FDA_LOCK"),"core_users");

$FEDIT->FGE_SetTitle("AUT_SCAD_LOCK","Controlli e blocchi di sicurezza","core_users");
//$FEDIT->FGE_SetTitle("ID_ST_DET","Scheda di trasporto","core_users");
$FEDIT->FGE_SetTitle("ID_DAY","Fiscalizzazione","core_users");


echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"Salva");

#
require_once("__includes/COMMON_sleepForgEdit.php");
?>