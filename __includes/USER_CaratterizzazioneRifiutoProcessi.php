<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;

if(isset($_POST['saveMeProc'])){
	$SQL="DELETE FROM user_schede_rifiuti_processi WHERE ID_RIF='".$_SESSION["IDRifiutoCaratt"]."';";
	$FEDIT->SDBWrite($SQL,true,false);
	$processi=$_POST['processi'];
	for($p=0;$p<count($processi);$p++){
		$SQL="INSERT INTO user_schede_rifiuti_processi VALUES (".$_SESSION["IDRifiutoCaratt"].", ".$processi[$p].");";
		$FEDIT->SDBWrite($SQL,true,false);
		}
	$SOGER->SetFeedback("Lista processi aggiornata","2");
	}

$SQL="SELECT * FROM lov_processi ORDER BY description ASC";
$FEDIT->SDBRead($SQL,"DbRecordSet");

$SQL_SEL="SELECT ID_PROC FROM user_schede_rifiuti_processi WHERE ID_RIF='".$_SESSION["IDRifiutoCaratt"]."'";
$FEDIT->SDBRead($SQL_SEL,"DbRecordSetSel");
$processi=Array();
for($s=0; $s<count($FEDIT->DbRecordSetSel); $s++){
	array_push($processi,$FEDIT->DbRecordSetSel[$s]['ID_PROC']);
	}

?>

<div class="FGEDataGridTitle"><div>ANAGRAFICHE � SCHEDE RIFIUTI PRODOTTI � CARATTERIZZAZIONE RIFIUTO</div></div><br>
<?php
if($SOGER->AppLocation!="login" && $SOGER->AppLocation!="login_documents" && count($SOGER->AppNavDIVs)>0) {
	$tmp .= "<div id=\"NavDiv\">\n";
	$tmp .= "<!-- NAV DIVs BEGIN -->\n[NAV_STUCTURE]\n<!-- NAV DIVs END -->\n";
	$SOGER->DIVmake_Navigation();
	$tmp .= "</div>\n";
	echo $tmp;
	}
?>

	<form name="FGEForm_1" id="FGEForm_1" method="post">

		<input type="hidden" name="saveMeProc" value="1" />

		<fieldset class="FGEfieldset">

			<div class="FGEFormTitle FGE4Col">Processi</div>

				<div id="FGED_user_schede_rifiuti:Processi" class="FGE4Col">
					
					<table class="FGEDataGridTable">
						<tbody>
							<tr>
								<th style="width:22px;" id="checkProc1">&nbsp;</th>
								<th class="FGEDataGridTableHeader" id="processi1">Processi</th>
								<th style="width:22px;" id="spacer">&nbsp;</th>
								<th style="width:22px;" id="checkProc2">&nbsp;</th>
								<th class="FGEDataGridTableHeader" id="processi2">Processi</th>
							</tr>
<?php
for($p=0;$p<count($FEDIT->DbRecordSet);$p+=2){
?>

							<tr>
								<td class="FGEDataGridTableCell" headers="checkProc">
									<input name="processi[]" onfocus="this.style.background='yellow';" onblur="this.style.background='white';" tabindex="50" value="<?php echo $FEDIT->DbRecordSet[$p]['ID_PROC']; ?>" type="checkbox" <?php if(in_array($FEDIT->DbRecordSet[$p]['ID_PROC'], $processi)) echo "checked"; ?>>
								</td>
								<td style="text-align:left;padding-left:10px;" class="FGEDataGridTableCell" headers="pericoloso">
									<?php echo $FEDIT->DbRecordSet[$p]['description']; ?>
								</td>
								
								<td>&nbsp;</td>

								<?php if(isset($FEDIT->DbRecordSet[$p+1]['ID_PROC'])){ ?>
								<td class="FGEDataGridTableCell" headers="checkProc">
									<input name="processi[]" onfocus="this.style.background='yellow';" onblur="this.style.background='white';" tabindex="50" value="<?php echo $FEDIT->DbRecordSet[$p+1]['ID_PROC']; ?>" type="checkbox" <?php if(in_array($FEDIT->DbRecordSet[$p+1]['ID_PROC'], $processi)) echo "checked"; ?>>
								</td>
								<td style="text-align:left;padding-left:10px;" class="FGEDataGridTableCell" headers="pericoloso">
									<?php echo $FEDIT->DbRecordSet[$p+1]['description']; ?>
								</td>
								<?php } else { ?>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<?php } ?>
							</tr>

<?php } ?>

						</tbody>
					</table>



				</div>

				<div class="FGESubmitRow">
					<input style="background: none repeat scroll 0% 0% rgb(3, 67, 115); color: white;" id="SubmitButton" class="FGEbutton" tabindex="67" value="Salva" onfocus="this.style.background='yellow';this.style.color='#034373';" onblur="this.style.background='#034373';this.style.color='white';" type="submit">
					
				</div>

		</fieldset>

	</form>

<?php
require_once("__includes/COMMON_sleepForgEdit.php");
?>