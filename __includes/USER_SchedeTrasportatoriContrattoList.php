<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
//require_once("__includes/COMMON_wakeForgEdit.php");
require("__includes/COMMON_wakeForgEditDG.php");
#
global $SOGER;
$FEDG->FGE_FlushTableInfo();
#

if(isset($_GET['filter']) && $_GET['pri']=="ID_AZT") $_SESSION['ID_AZT_CONTRACT']=$_GET['filter'];

$FEDG->FGE_UseTables("user_contratti_trasportatori","user_schede_rifiuti");

$FEDG->FGE_SetSelectFields(array("ID_CER","descrizione","approved"),"user_schede_rifiuti");
$FEDG->FGE_SetSelectFields(array("ID_AZT","description","DT_INIZIO","DT_SCADENZA"),"user_contratti_trasportatori");
$FEDG->FGE_SetFilter("approved","1","user_schede_rifiuti");
$FEDG->FGE_SetFilter("ID_AZT",$_SESSION['ID_AZT_CONTRACT'],"user_contratti_trasportatori");
$FEDG->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");


$FEDG->FGE_DescribeFields();
#
$FEDG->FGE_HideFields(array("approved"),"user_schede_rifiuti");
$FEDG->FGE_HideFields(array("ID_AZT"),"user_contratti_trasportatori");
#
$FEDG->FGE_LookUpCFG("ID_CER","user_schede_rifiuti");
$FEDG->FGE_UseTables("lov_cer");
$FEDG->FGE_SetSelectFields(array("COD_CER"),"lov_cer");

$FEDG->FGE_LookUpDescribe();
$FEDG->FGE_LookUpDone();

echo($FEDG->FGE_DataGrid($SOGER->AppDescriptiveLocation,"__scripts/FGE_DataGridEdit.php","EDC--",false,"FEDG",$_SESSION['SearchingFilter']));


#
//require_once("__includes/COMMON_sleepForgEdit.php");
require("__includes/COMMON_sleepForgEditDG.php");
?>
<div class="FGE4Col">
<input type="button" name="nuovo" class="FGEbutton" value="crea nuovo contratto" onclick="javascript: document.location='__scripts/newContrattoTrasportatore.php'"/>
</div>
