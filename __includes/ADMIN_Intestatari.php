<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("core_intestatari");
$FEDIT->FGE_SetFormFields(array("ID_INT","description","indirizzo","ID_COM","IN_ITALIA","telefono","fax","email","piva","codfisc"),"core_intestatari");
#
if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {

	$FEDIT->FGE_DescribeFields();	
	$FEDIT->FGE_LookUpDefault("ID_COM","core_intestatari");
	$FEDIT->FGE_HideFields(array("IN_ITALIA"),"core_intestatari");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea intestatario");

} else {
	
	$FEDIT->FGE_DescribeFields();	
	$FEDIT->FGE_LookUpDefault("ID_COM","core_intestatari");
	$FEDIT->FGE_DisableFields("ID_INT","core_intestatari");
	$FEDIT->FGE_HideFields(array("IN_ITALIA"),"core_intestatari");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica intestatario");}
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>
