<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_autorizzazioni_interm");
$FEDIT->FGE_SetFormFields(array("ID_UIMI","ID_RIF","ID_AUT","num_aut","PREV_num_aut","rilascio","scadenza","ID_ORIGINE_DATI","note","inherit_edits"),"user_autorizzazioni_interm");

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {
	#
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_AUT","user_autorizzazioni_interm");
	$FEDIT->FGE_LookUpDefault("ID_ORIGINE_DATI","user_autorizzazioni_interm");
	#
	$FEDIT->FGE_LookUpCFG("ID_RIF","user_autorizzazioni_interm");
	$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
	//$FEDIT->FGE_SetSelectFields(array("cod_pro","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("cod_pro","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
	$ProfiloFields = "user_schede_rifiuti";
	include("SOGER_FiltriProfilo.php");
	$FEDIT->FGE_SetOrder("lov_cer:COD_CER");
	//$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_rifiuti");
	$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#
	$FEDIT->FGE_LookUpCFG("ID_UIMI","user_autorizzazioni_interm");
	$FEDIT->FGE_UseTables("user_impianti_intermediari");
	$FEDIT->FGE_SetSelectFields(array("description","ID_AZI"),"user_impianti_intermediari");
	$FEDIT->FGE_SetFilter("ID_AZI",$_SESSION["FGE_IDs"]["user_aziende_intermediari"],"user_impianti_intermediari");
	$FEDIT->FGE_HideFields("ID_AZI","user_impianti_intermediari");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#

	$FEDIT->FGE_HideFields(array("inherit_edits","PREV_num_aut"),"user_autorizzazioni_interm");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea autorizzazione");
	
} else {
	if(isset($_GET["Involved"])) {
		$FEDIT->FGE_DisableFields(array("ID_RIF","ID_UIMI"),"user_autorizzazioni_interm");	
	}
	#
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_AUT","user_autorizzazioni_interm");
	$FEDIT->FGE_LookUpDefault("ID_ORIGINE_DATI","user_autorizzazioni_interm");
	#
	$FEDIT->FGE_LookUpCFG("ID_RIF","user_autorizzazioni_interm");
	$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
	//$FEDIT->FGE_SetSelectFields(array("cod_pro","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("cod_pro","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
	$ProfiloFields = "user_schede_rifiuti";
	include("SOGER_FiltriProfilo.php");
	$FEDIT->FGE_SetOrder("lov_cer:COD_CER");
	//$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#
	$FEDIT->FGE_LookUpCFG("ID_UIMI","user_autorizzazioni_interm");
	$FEDIT->FGE_UseTables("user_impianti_intermediari");
	$FEDIT->FGE_SetSelectFields(array("description","ID_AZI"),"user_impianti_intermediari");
	$FEDIT->FGE_SetFilter("ID_AZI",$_SESSION["FGE_IDs"]["user_aziende_intermediari"],"user_impianti_intermediari");
	$FEDIT->FGE_HideFields("ID_AZI","user_impianti_intermediari");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#

	$FEDIT->FGE_HideFields(array("inherit_edits","PREV_num_aut"),"user_autorizzazioni_interm");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva autorizzazione");

		$js ="<script type=\"text/javascript\">\n";
		$js.= <<< ENDJS
		function show_hide(id){
			var obj=document.getElementById(id);
			if(obj.style.display=='none'){
				obj.style.display='block';
				obj.style.zindex=10;
				}
			else{
				obj.style.display='none';
				obj.style.zindex=-1;
				}
			}
ENDJS;
		$js.="</script>\n";
		echo $js;

		$css ="<style type=\"text/css\">\n";
		$css.="div#btn_associa_1{\n";
		$css.="position: absolute; top: 347px; left: 212px; z-index: 10;";
		$css.="}\n";
		$css.="div#btn_associa_2{\n";
		$css.="position: absolute; top: 347px; left: 357px; z-index: 10;";
		$css.="}\n";
		$css.="div#btn_associa_3{\n";
		$css.="position: absolute; top: 347px; left: 530px; z-index: 10;";
		$css.="}\n";
		$css.="</style>\n";
		
		#css ie
		$css.="<!--[if IE]>";
		$css.="<style type=\"text/css\">\n";
		$css.="div#btn_associa_1{\n";
		$css.="position: absolute; top: 382px; left: 276px; z-index: 10;";
		$css.="}\n";
		$css.="div#btn_associa_2{\n";
		$css.="position: absolute; top: 382px; left: 458px; z-index: 10;";
		$css.="}\n";
		$css.="div#btn_associa_3{\n";
		$css.="position: absolute; top: 382px; left: 682px; z-index: 10;";
		$css.="}\n";
		$css.="</style>\n";
		$css.="<![endif]-->";

		echo $css;
		
		$divAut ="<div>\n";
			
			$divAut.="<div id=\"btn_associa_1\">\n";
			
				$divAut.="<input type=\"button\" class=\"FGEbutton\" name=\"\" value=\"Associa a tutti i rifiuti\" onclick=\"if(window.confirm('Confermi di voler associare l\' autorizzazione a tutti i rifiuti in archivio?')){location.replace('__scripts/SOGER_AssocAuth.php?RIF=all&table=".$_GET['table']."&pri=".$_GET['pri']."&filter=".$_GET['filter']."');}\" />\n";

			$divAut.="</div>\n";

			$divAut.="<div id=\"btn_associa_2\">\n";
			
				$divAut.="<input type=\"button\" class=\"FGEbutton\" name=\"\" value=\"Associa ai rifiuti pericolosi\" onclick=\"if(window.confirm('Confermi di voler associare l\' autorizzazione a tutti i rifiuti pericolosi in archivio?')){location.replace('__scripts/SOGER_AssocAuth.php?RIF=p&table=".$_GET['table']."&pri=".$_GET['pri']."&filter=".$_GET['filter']."');}\" />\n";

			$divAut.="</div>\n";

			$divAut.="<div id=\"btn_associa_3\">\n";
			
				$divAut.="<input type=\"button\" class=\"FGEbutton\" name=\"\" value=\"Associa ai rifiuti non pericolosi\" onclick=\"if(window.confirm('Confermi di voler associare l\' autorizzazione a tutti i rifiuti non pericolosi in archivio?')){location.replace('__scripts/SOGER_AssocAuth.php?RIF=np&table=".$_GET['table']."&pri=".$_GET['pri']."&filter=".$_GET['filter']."');}\" />\n";

			$divAut.="</div>\n";

	
		echo $divAut;


}
require_once("__includes/COMMON_sleepForgEdit.php");
?>
