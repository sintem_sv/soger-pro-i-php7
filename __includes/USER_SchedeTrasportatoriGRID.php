<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
include("__includes/USER_SchedeTrasportatoriFlushSessionVars.php");
$FEDIT->FGE_FlushTableInfo();
#
$FEDIT->FGE_UseTables("user_aziende_trasportatori");
$FEDIT->FGE_SetSelectFields(array("description","indirizzo","ID_COM","piva","codfisc","produttore","trasportatore","destinatario","intermediario","approved"),"user_aziende_trasportatori");
$FEDIT->FGE_SetSelectFields(array("latitude","longitude"),"user_aziende_trasportatori");

$FEDIT->FGE_DescribeFields();

$FEDIT->FGE_SetOrder("user_aziende_trasportatori:description");

$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_aziende_trasportatori");

$ProfiloFields = "user_aziende_trasportatori";
include("SOGER_FiltriProfilo.php");

$FEDIT->FGE_HideFields(array("produttore","trasportatore","destinatario","intermediario","approved","latitude","longitude"),"user_aziende_trasportatori");
#
$FEDIT->FGE_LookUpDefault("ID_COM","user_aziende_trasportatori");
#

$Privileges ="ED-PT";
//if($SOGER->UserData["core_usersO1"]=="1" && $SOGER->UserData['core_impiantiMODULO_SIS']==1) $Privileges.="Y"; else $Privileges.="-";

echo($FEDIT->FGE_DataGrid($SOGER->AppDescriptiveLocation,"__scripts/FGE_DataGridEdit.php",$Privileges, true, "FEDIT", $_SESSION['SearchingFilter']));

#
require_once("__includes/COMMON_sleepForgEdit.php");
?>
<div class="FGE4Col">
<input type="button" name="nuovo" class="FGEbutton" value="crea nuovo trasportatore" onclick="javascript: document.location='__scripts/new_trasportatore.php'"/>
</div>







<!-- HIDDEN DIALOG BOX -->

<div id='jqpopup_LatLong' title='Posizione sulla mappa'>

	<div id='LatLong_address'>
		<input type='text' name='address' id='address' value='' />
		<input type='button' name='address_go' id='address_go' value='Centra mappa' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' onClick='CenterMap()' />
	</div>

	<div id='LatLong_map' style='width:430px;height:320px;'>
	</div>

</div>