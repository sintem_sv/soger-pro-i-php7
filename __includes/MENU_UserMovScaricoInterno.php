<?php 
global $SOGER;

$newRecordImg = 'BT_userMovNuovoScarico.gif';
$newRecordTxt = 'Nuovo scarico';
if(stripos($this->AppDescriptiveLocation, 'interno') !== false){
	$newRecordImg = 'BT_userMovNuovoScaricoInterno.gif';
	$newRecordTxt = 'Nuovo scarico interno';
	}
elseif(stripos($this->AppDescriptiveLocation, 'scarico') !== false){
	$newRecordImg = 'BT_userMovNuovoScarico.gif';
	$newRecordTxt = 'Nuovo scarico';
	}
elseif(stripos($this->AppDescriptiveLocation, 'formulario') !== false){
	$newRecordImg = 'BT_userMovNuovoFormulario.gif';
	$newRecordTxt = 'Nuovo formulario';
	}
elseif(stripos($this->AppDescriptiveLocation, 'scheda') !== false){
	$newRecordImg = 'BT_userMovNuovaSchedaSistri.gif';
	$newRecordTxt = 'Nuova Scheda Sistri';
	}

$TableName		= "user_movimenti_fiscalizzati";
$AppBackStatus	= "UserNuovoMovScaricoInternoF";
?>

<a href="#" title="Generale"><img src="__css/BT_userMovCaricoGen_anti90-ON.gif" class="NoBorder" alt="Generale" /></a>

<?php if($SOGER->UserData['core_usersFRM_LAYOUT_SCARICO_INTERNO']==1){ ?>
	<a href="__scripts/status.php?area=UserNuovoMovScaricoInternoF" title="<?php echo $newRecordTxt; ?>"><img src="__css/<?php echo $newRecordImg; ?>" class="NoBorder" alt="<?php echo $newRecordTxt; ?>" /></a>
<?php } ?>