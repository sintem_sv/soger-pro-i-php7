<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEditDG.php");
#
global $SOGER;
$FEDG->FGE_FlushTableInfo();
$FEDG->FGE_UseTables("user_impianti_destinatari");
$FEDG->FGE_SetSelectFields(array("idSIS","description","ID_COM","telefono","fax","approved"),"user_impianti_destinatari");
$FEDG->FGE_SetSelectFields(array("latitude","longitude"),"user_impianti_destinatari");

#
$FEDG->FGE_DescribeFields();
$FEDG->FGE_SetOrder("user_impianti_destinatari:description");

$FEDG->FGE_LookUpDefault("ID_COM","user_impianti_destinatari");
$FEDG->FGE_SetFilter("ID_AZD",$_SESSION["FGE_IDs"]["user_aziende_destinatari"],"user_impianti_destinatari");
#
$FEDG->FGE_HideFields(array("idSIS","approved","latitude","longitude"),"user_impianti_destinatari");

// E = Edit
// D = Delete
// C = Clone
// P = Print
// T = Thumb (approve/disapprove)
// Y = sYnc (SISTRI)

$Privileges ="ED--T";
if($SOGER->UserData["core_usersO1"]=="1" && $SOGER->UserData['core_impiantiMODULO_SIS']==1) $Privileges.="Y"; else $Privileges.="-";

echo($FEDG->FGE_DataGrid("Impianti in Archivio","__scripts/FGE_DataGridEdit.php",$Privileges,true,"FEDG",$_SESSION['SearchingFilter']));

#
require_once("__includes/COMMON_sleepForgEditDG.php");
?>
<div class="FGE4Col">
<input type="button" name="nuovo" value="crea nuovo impianto" class="FGEbutton" onclick="javascript: document.location='<?php echo $_SERVER["PHP_SELF"]?>'"/>
</div>




<!-- HIDDEN DIALOG BOX -->

<div id="jqpopup_SyncroImpianto" title="Selezione unit� locale SISTRI">

	<p>
		Seleziona dal men� a tendina sottostante l'unit� locale SISTRI corrispondente all'impianto caricato in So.Ge.R. PRO:
	</p>

			
		<table id="TABLE_MatchSedi" cellspacing="0">

			<tr>
				<th>Impianto So.Ge.R. PRO</th>

				<td id="impianto_pro" colspan="2"> ..IMPIANTO.. </td>

			</tr>
			
			<tr>
				<th>Unit� locale SISTRI</th>

				<td>

					<select name="Select_UnitaLocale" id="Select_UnitaLocale">
						<option value='0'>Seleziona unit� locale</option>
					</select>

					<span id="AjaxStatus_UnitaLocale"></span>
				
				</td>

			</tr>


		</table>

		<div class="ui-widget">
			<div id="DIV_AjaxCall" class="ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
				<p id="P_AjaxCall">
					<span id="SPAN_AjaxCall" style="float: left; margin-right: .3em;"></span>
					<span id="SPAN_AjaxCallTxt"></span>
				</p>
			</div>
		</div>

	</p>

</div>








<!-- HIDDEN DIALOG BOX -->

<div id='jqpopup_LatLong' title='Posizione sulla mappa'>

	<div id='LatLong_address'>
		<input type='text' name='address' id='address' value='' />
		<input type='button' name='address_go' id='address_go' value='Centra mappa' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' onClick='CenterMap()' />
	</div>

	<div id='LatLong_map' style='width:430px;height:320px;'>
	</div>

</div>