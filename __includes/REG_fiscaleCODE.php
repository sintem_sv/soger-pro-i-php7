<?php
	$Xcorr = $SOGER->UserData["core_usersREG_PRNT_X"];
	$Ycorr = $SOGER->UserData["core_usersREG_PRNT_Y"];
	//if($Xcorr>4)	$Xcorr=4;
	if($Ycorr>12)	$Ycorr=12;

	// aggiorno riferimenti di carico
	$TableName = "user_movimenti_fiscalizzati";
    $LegamiSISTRI = false;
	if($SOGER->UserData['workmode']=='produttore')
            require("../__scripts/MovimentiRifMovCarico.php");

	#
	$sql  = "SELECT";
	$sql .= " user_movimenti_fiscalizzati.ID_MOV_F,user_movimenti_fiscalizzati.PerRiclassificazione,user_movimenti_fiscalizzati.TIPO,user_movimenti_fiscalizzati.NMOV,user_movimenti_fiscalizzati.DTMOV, user_movimenti_fiscalizzati.idSIS_scheda, user_movimenti_fiscalizzati.idSIS, user_movimenti_fiscalizzati.numero_scheda, user_movimenti_fiscalizzati.dataSchedaSistri_invio";
	$sql .= ",user_movimenti_fiscalizzati.NFORM,user_movimenti_fiscalizzati.DTFORM,user_movimenti_fiscalizzati.N_ANNEX_VII,user_movimenti_fiscalizzati.lotto,user_movimenti_fiscalizzati.FKErifCarico";
	$sql .= ",user_movimenti_fiscalizzati.quantita,user_movimenti_fiscalizzati.NOTER,user_movimenti_fiscalizzati.VER_DESTINO,user_movimenti_fiscalizzati.PS_DESTINO, user_movimenti_fiscalizzati.LINK_DestProd";
	$sql .= ",user_schede_rifiuti.descrizione,user_schede_rifiuti.NOTER AS NOTE_RIFIUTO,user_schede_rifiuti.RifDaManutenzione,lov_cer.COD_CER,lov_cer.PERICOLOSO as pericoloso, peso_spec";
	$sql .= ",lov_stato_fisico.description AS STFdes";
	$sql .= ",H1,H2,H3A,H3B,H4,H5,H6,H7,H8,H9,H10,H11,H12,H13,H14,H15";
	$sql .= ",HP1,HP2,HP3,HP4,HP5,HP6,HP7,HP8,HP9,HP10,HP11,HP12,HP13,HP14,HP15";
	$sql .= ",ANN_scheda, ANN_scheda_causale, ANN_registrazione, ANN_registrazione_causale";
	$sql .= ",lov_misure.ID_UMIS AS Umisura";
	$sql .= ",user_aziende_produttori.description AS PRdes,user_impianti_produttori.description AS IMdes";
	$sql .= ",COMpro.description AS COMdes,COMpro.shdes_prov AS COMproPR, COMpro.des_prov AS COMproDesProv, COMpro.IN_ITALIA AS COMproItaliano";
	$sql .= ",lov_operazioni_rs.description AS OPRSdes";
	$sql .= ",lov_cer.description AS NomeCER";
	$sql .= ",user_aziende_intermediari.description AS INTdes,user_aziende_intermediari.commerciante AS INTcommerciante,user_aziende_intermediari.esclusoFIR AS esclusoFIR,user_aziende_intermediari.indirizzo AS INTind";
	$sql .= ",user_aziende_intermediari.codfisc AS INTcfisc, user_aziende_intermediari.ID_COM AS INTidcom";
	$sql .= ",user_autorizzazioni_interm.num_aut AS INTaut,user_movimenti_fiscalizzati.FKErilascioI AS INTrilascio";
	$sql .= ",COMint.description AS COMintdes,COMint.shdes_prov AS COMintPR, COMint.CAP AS INTCAP, COMint.des_prov AS COMintDesProv, COMint.IN_ITALIA AS COMintItaliano";
	$sql .= " FROM user_movimenti_fiscalizzati"; 
	$sql .= " JOIN user_schede_rifiuti on user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF";
	$sql .= " JOIN lov_cer on user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
	$sql .= " JOIN lov_stato_fisico ON user_schede_rifiuti.ID_SF=lov_stato_fisico.ID_SF";
	$sql .= " JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS";
	$sql .= " LEFT JOIN user_aziende_produttori ON user_aziende_produttori.ID_AZP=user_movimenti_fiscalizzati.ID_AZP";
	$sql .= " LEFT JOIN user_impianti_produttori ON user_movimenti_fiscalizzati.ID_UIMP=user_impianti_produttori.ID_UIMP"; 
	$sql .= " LEFT JOIN lov_comuni_istat AS COMpro ON user_impianti_produttori.ID_COM=COMpro.ID_COM";
	$sql .= " LEFT JOIN lov_operazioni_rs ON user_movimenti_fiscalizzati.ID_OP_RS=lov_operazioni_rs.ID_OP_RS";
	$sql .= " LEFT JOIN user_aziende_intermediari ON user_movimenti_fiscalizzati.ID_AZI=user_aziende_intermediari.ID_AZI";
	$sql .= " LEFT JOIN user_impianti_intermediari ON user_impianti_intermediari.ID_UIMI=user_movimenti_fiscalizzati.ID_UIMI"; 
	$sql .= " LEFT JOIN user_autorizzazioni_interm ON user_movimenti_fiscalizzati.ID_AUTHI=user_autorizzazioni_interm.ID_AUTHI";
	$sql .= " LEFT JOIN lov_comuni_istat AS COMint ON user_aziende_intermediari.ID_COM=COMint.ID_COM";
	$sql .= " WHERE (NMOV>='" . $_GET["da"] . "' AND NMOV<='" . $_GET["a"] . "') AND NMOV<>9999999";
	$TableSpec = "user_movimenti_fiscalizzati.";
	include("../__includes/SOGER_DirectProfilo.php");
	#movimenti sono SEMPRE approved=1
	$sql .= " AND user_movimenti_fiscalizzati.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
	$sql .= " ORDER BY NMOV ASC";
	$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

	#
	if(isset($_GET["cartaBianca"])) {
		# stampa registro (carta bianca)
		$DcName = "Stampa_Registro";
		$x = 2 + $Xcorr;
		$y = 12 + $Ycorr;
		$GridsCount = 0;
		$PageCount = 1;
	
		if ($_GET["PaginaIniz"]=="1") {
			$PageNr = 1;
			$CustomPageIniz = false;
		} else {
			$CustomPageIniz = true;
			$PageNr = $_GET["PaginaIniz"]; 
		}
		$TotalPages = ceil($FEDIT->DbRecsNum/3);
		# crea sfondi (griglia dati)
                
        # creo variabile contatore Juan
        $contatore = $FEDIT->DbRecsNum;
		for($c=0;$c<$contatore;$c++) {
			SfondoRegistro($x,$y,$FEDIT->FGE_PdfBuffer);
			if(!isset($BufAnno)) {
				$BufAnno = date("Y",strtotime($FEDIT->DbRecordSet[$c]["DTMOV"]));
				}

			# MODULO_LDP
			$LDP = '';
			
			if($SOGER->UserData['core_impiantiMODULO_LDP']=='1' AND $FEDIT->DbRecordSet[$c]["TIPO"]=="C" AND $SOGER->UserData['workmode']=="produttore" AND $FEDIT->DbRecordSet[$c]['LINK_DestProd']!='' AND !is_null($FEDIT->DbRecordSet[$c]['LINK_DestProd'])){
				
				$LDP.= "FIR/rifiuti dal cui tratt. deriva il carico: mov ";
				$FIR = explode(',', $FEDIT->DbRecordSet[$c]['LINK_DestProd']);
				for($f=0;$f<count($FIR);$f++){
					$SQL = "SELECT IF(DTFORM IS NULL, 'n.d.', DTFORM) AS DTFORM, NFORM, NMOV, lov_cer.COD_CER, user_schede_rifiuti.descrizione ";
					$SQL.= "FROM user_movimenti_fiscalizzati ";
					$SQL.= "JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_movimenti_fiscalizzati.ID_RIF ";
					$SQL.= "JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
					$SQL.= "WHERE ID_MOV_F=".$FIR[$f].";";
					$FEDIT->SDBRead($SQL,"DbRecordSetLDP",true,false);
					$LDP.= $FEDIT->DbRecordSetLDP[0]['NMOV'];
					$LDP.= ", ";
					}
				$LDP = substr($LDP, 0, strlen($LDP)-2);
				$LDP.= " (mov. del registro destinatario)";
				}

			// set FISCALE=1 to stop editing and delete
			if($_GET['Vidimato']=='1'){
				$sql = "UPDATE user_movimenti_fiscalizzati SET FISCALE=1 WHERE ID_MOV_F=".$FEDIT->DbRecordSet[$c]['ID_MOV_F'].";";
				$FEDIT->SDBWrite($sql,true,false);
				}

			DatiRegistro($FEDIT->DbRecordSet[$c],$x,$y,$FEDIT->FGE_PdfBuffer, $LDP);
			$y +=84.6;
			$GridsCount++;
			
			if(isset($_GET["Npagina"]) && $PageCount==$TotalPages) {			//ultima pagina del registro
					$FEDIT->FGE_PdfBuffer->SetXY(190,271);
					if($_GET['Vidimato']==0)
						$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$BufAnno."/".$PageNr,0,"L");
					else{
						$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$PageNr,0,"L");
						}
			}
			if($GridsCount==3 && $PageCount<$TotalPages) {						//pagine intermedie del registro
				# numeri di pagina
				if(isset($_GET["Npagina"])) {
					$FEDIT->FGE_PdfBuffer->SetXY(190,271);
					if($_GET['Vidimato']==0)
						$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$BufAnno."/".$PageNr,0,"L");
					else{
						$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$PageNr,0,"L");
						}
					}
				$FEDIT->FGE_PdfBuffer->AddPage();
				$GridsCount = 0;
				$PageCount++;
				$PageNr++;
				$x = 2 + $Xcorr;
				$y = 12 + $Ycorr;
			}
		}
		
	} else {
		# stampa registro (prefincato)
		
			$DcName = "Stampa_Registro_Prefincato";
			$x = 2 + $Xcorr;
			$y = 16 + $Ycorr;
			$GridsCount = 0;
			$PageCount = 1;
		
			if ($_GET["PaginaIniz"]=="1") {
				$PageNr = 1;
				$CustomPageIniz = false;
			} else {
				$CustomPageIniz = true;
				$PageNr = $_GET["PaginaIniz"]; 
			}
			$TotalPages = ceil($FEDIT->DbRecsNum/3);
			# crea sfondi (griglia dati)
            $contatore = $FEDIT->DbRecsNum; # creo variabile contatore Juan
			for($c=0;$c<$contatore;$c++) {
				if(!isset($BufAnno)) {
					$BufAnno = date("Y",strtotime($FEDIT->DbRecordSet[$c]["DTMOV"]));
					}

				# MODULO_LDP
				$LDP = '';
				
				if($SOGER->UserData['core_impiantiMODULO_LDP']=='1' AND $FEDIT->DbRecordSet[$c]["TIPO"]=="C" AND $SOGER->UserData['workmode']=="produttore" AND $FEDIT->DbRecordSet[$c]['LINK_DestProd']!=''){
					$LDP.= "FIR/rifiuti dal cui tratt. deriva il carico: ";
					$FIR = explode(',', $FEDIT->DbRecordSet[$c]['LINK_DestProd']);
					
					for($f=0;$f<count($FIR);$f++){
						$SQL = "SELECT IF(DTFORM IS NULL, 'n.d.', DTFORM) AS DTFORM, NFORM, NMOV, lov_cer.COD_CER, user_schede_rifiuti.descrizione ";
						$SQL.= "FROM user_movimenti_fiscalizzati ";
						$SQL.= "JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_movimenti_fiscalizzati.ID_RIF ";
						$SQL.= "JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
						$SQL.= "WHERE ID_MOV_F=".$FIR[$f].";";
						$FEDIT->SDBRead($SQL,"DbRecordSetLDP",true,false);
						$LDP.= $FEDIT->DbRecordSetLDP[0]['NMOV'];
						$LDP.= ", ";
						}
					$LDP = substr($LDP, 0, strlen($LDP)-2);
					}
			
				// set FISCALE=1 to stop editing and delete
				if($_GET['Vidimato']=='1'){
					$sql = "UPDATE user_movimenti_fiscalizzati SET FISCALE=1 WHERE ID_MOV_F=".$FEDIT->DbRecordSet[$c]['ID_MOV_F'].";";
					$FEDIT->SDBWrite($sql,true,false);
					}

				DatiRegistro($FEDIT->DbRecordSet[$c],$x,$y,$FEDIT->FGE_PdfBuffer, $LDP);
				$y +=84.6;
				$GridsCount++;
		
				if(isset($_GET["Npagina"]) && $PageCount==$TotalPages) {			//ultima pagina del registro
					$FEDIT->FGE_PdfBuffer->SetFont('Arial','',5);
					$FEDIT->FGE_PdfBuffer->SetXY(190,271);
					if($_GET['Vidimato']==0)
						$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$BufAnno."/".$PageNr,0,"L");
					else{
						$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$PageNr,0,"L");
						}
					$FEDIT->FGE_PdfBuffer->SetFont('Arial','',7);
					}								

				if($GridsCount==3 && $PageCount<$TotalPages) {
					# numeri di pagina
					if(isset($_GET["Npagina"])) {
						$FEDIT->FGE_PdfBuffer->SetFont('Arial','',5);
						$FEDIT->FGE_PdfBuffer->SetXY(190,271);
						if($_GET['Vidimato']==0)
							$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$BufAnno."/".$PageNr,0,"L");
						else{
							$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$PageNr,0,"L");
							}
						$FEDIT->FGE_PdfBuffer->SetFont('Arial','',7);
						}
					$FEDIT->FGE_PdfBuffer->AddPage();
					$GridsCount = 0;
					$PageCount++;
					$PageNr++;
					$x = 2 + $Xcorr;
					$y = 16 + $Ycorr;
				}
			}
	}


?>