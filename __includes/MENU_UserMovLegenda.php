<script type="text/javascript">
    $(document).ready(function(){
        $( "form" ).first().css( "padding-top", "173px" ); 
    });
</script>

<?php
global $SOGER;
// autorizzazioni scadute/assenti
if($SOGER->UserData['core_usersAUT_SCAD_LOCK']==0)
    $img1 = "<img src='__css/licensegray.png' alt='Blocco per autorizzazioni scadute/assenti non attivo' title='Blocco per autorizzazioni scadute/assenti non attivo' />";
else
    $img1 = "<img src='__css/license.png' alt='Blocco per autorizzazioni scadute/assenti attivo' title='Blocco per autorizzazioni scadute/assenti attivo' />";

// abilitazione conto terzi assente
if($SOGER->UserData['core_usersCONTO_TERZI_LOCK']==0)
    $img2 = "<img src='__css/flag_greengray.png' alt='Blocco per abilitazione conto terzi assente non attivo' title='Blocco per abilitazione conto terzi assente non attivo' />";
else
    $img2 = "<img src='__css/flag_green.png' alt='Blocco per abilitazione conto terzi assente attivo' title='Blocco per abilitazione conto terzi assente attivo' />";

// fir senza destinatario
if($SOGER->UserData['core_usersCHECK_DESTINATARIO']==0)
    $img3 = "<img src='__css/recyclegray.png' alt='Blocco per formulari senza destinatario non attivo' title='Blocco per formulari senza destinatario non attivo' />";
else
    $img3 = "<img src='__css/recycle.png' alt='Blocco per formulari senza destinatario attivo' title='Blocco per formulari senza destinatario attivo' />";

// fir senza trasportatore
if($SOGER->UserData['core_usersCHECK_TRASPORTATORE']==0)
    $img4 = "<img src='__css/lorrygray.png' alt='Blocco per formulari senza trasportatore non attivo' title='Blocco per formulari senza trasportatore non attivo' />";
else
    $img4 = "<img src='__css/lorry.png' alt='Blocco per formulari senza trasportatore attivo' title='Blocco per formulari senza trasportatore attivo' />";

// nuovo fir: mostra rif. pericolosi
if($SOGER->UserData['core_usersFRM_pericolosi']==0)
    $img5 = "<img src='__css/imageRifgray.gif' alt='Non mostra rifiuti pericolosi nella creazione di un nuovo formulario' title='Non mostra rifiuti pericolosi nella creazione di un nuovo formulario' />";
else
    $img5 = "<img src='__css/imageRif1.gif' alt='Mostra rifiuti pericolosi nella creazione di un nuovo formulario' title='Mostra rifiuti pericolosi nella creazione di un nuovo formulario' />";

// blocco movimentazione se targhe non valide (FDA)
if($SOGER->UserData['core_usersFDA_LOCK']==1 && $SOGER->UserData['core_impiantiMODULO_FDA']==1)
    $img6 = "<img src='__css/fda.png' alt='Blocco per mancata validazione targhe attivo' title='Blocco per mancata validazione targhe attivo' />";
else
    $img6 = "<img src='__css/fdagray.png' alt='Blocco per mancata validazione targhe non attivo' title='Blocco per mancata validazione targhe non attivo' />";

?>

<div id="MovLegenda">

    <div class="FGEFormTitle FGE4Col">
        <div class="MovLegenda_title">&nbsp;&nbsp;</div>
        Allarmi e blocchi attivi
    </div>
    
    <table id="MovLegenda_icon">
        <tr>
            <td style="width:40%;">Impedisci movimenti con autorizzazioni scadute/assenti</td>
            <td style="width:10%;"><?php echo $img1; ?></td>
            <td style="width:40%;">Impedisci movimenti con abilitazione conto terzi assente</td>
            <td style="width:10%;"><?php echo $img2; ?></td>
        </tr>
        <tr>
            <td>Impedisci FIR senza destinatario</td>
            <td><?php echo $img3; ?></td>
            <td>Impedisci FIR senza trasportatore</td>
            <td><?php echo $img4; ?></td>
        </tr>
        <tr>
            <td>"Nuovo formulario": mostra rifiuti pericolosi</td>
            <td><?php echo $img5; ?></td>
            <td>Blocco per mancata validazione targhe</td>
            <td><?php echo $img6; ?></td>
        </tr>
    </table>

</div>