<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;

include("USER_MovFlushSessionVars.php");
require("__includes/USER_FormularioFiscaleSiNo.inc");
#
$FEDIT->FGE_FlushTableInfo();
#
$FEDIT->FGE_UseTables("user_movimenti_fiscalizzati","user_schede_rifiuti","lov_cer");
$FEDIT->FGE_SetSelectFields(array("FISCALIZZATO","NFORM","DTFORM","ID_RIF","pesoN","quantita","FKEumis", "PS_DESTINO","FISCALE","StampaAnnua","ID_IMP","produttore","destinatario","trasportatore"),"user_movimenti_fiscalizzati");
$FEDIT->FGE_SetSelectFields(array("NMOV","TIPO","FANGHI","DTMOV","VER_DESTINO","DT_FORM","ID_OP_RS","ID_AUTST","ID_AUTO","ID_UIMP"),"user_movimenti_fiscalizzati");
$FEDIT->FGE_SetSelectFields(array("descrizione","peso_spec"),"user_schede_rifiuti");
$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
$FEDIT->FGE_DescribeFields();
$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_movimenti_fiscalizzati");
$FEDIT->FGE_SetFilter("FANGHI",1,"user_movimenti_fiscalizzati");
$ProfiloFields = "user_movimenti_fiscalizzati";

include("SOGER_FiltriProfilo.php");
#
$FEDIT->FGE_HideFields(array("FISCALE","FANGHI","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_movimenti_fiscalizzati");
#
echo($FEDIT->SG_MovimentiDataGrid($SOGER->AppDescriptiveLocation, "user_movimenti_fiscalizzati", "fanghi"));
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>
