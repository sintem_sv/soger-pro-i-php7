<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;

if(count($_POST)>1){

	// per ora so che sono solo due automezzi con id 2 e 7
	$SQL="DELETE FROM user_viaggi_impianti WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."';";
	$FEDIT->SDBWrite($SQL,true,false);
	
	//autocarro - id 2
	$SQL ="INSERT INTO `user_viaggi_impianti` ";
	$SQL.="(`ID_VI` , `ID_MZ_TRA` , `ID_IMP` , `costo_gasolio` , `costo_autostrada` , `costo_rc` , `costo_bra` ,`costo_conducente` , `costo_usura` , `costo_generale` , `costo_correttivo` ) ";
	$SQL.="VALUES ( NULL , '2', '".$SOGER->UserData['core_usersID_IMP']."', '".$_POST['2-costo_gasolio']."', '".$_POST['2-costo_autostrada']."', '".$_POST['2-costo_rc']."', '".$_POST['2-costo_bra']."', '".$_POST['2-costo_conducente']."', '".$_POST['2-costo_usura']."', '".$_POST['2-costo_generale']."', '".$_POST['2-costo_correttivo']."');";
	$FEDIT->SDBWrite($SQL,true,false);
	
	//furgone - id 7
	$SQL ="INSERT INTO `user_viaggi_impianti` ";
	$SQL.="(`ID_VI` , `ID_MZ_TRA` , `ID_IMP` , `costo_gasolio` , `costo_autostrada` , `costo_rc` , `costo_bra` ,`costo_conducente` , `costo_usura` , `costo_generale` , `costo_correttivo`) ";
	$SQL.="VALUES ( NULL , '7', '".$SOGER->UserData['core_usersID_IMP']."', '".$_POST['7-costo_gasolio']."', '".$_POST['7-costo_autostrada']."', '".$_POST['7-costo_rc']."', '".$_POST['7-costo_bra']."', '".$_POST['7-costo_conducente']."', '".$_POST['7-costo_usura']."', '".$_POST['7-costo_generale']."', '".$_POST['7-costo_correttivo']."');";
	$FEDIT->SDBWrite($SQL,true,false);
	}

?>

<div class="FGEDataGridTitle"><div><?php echo $SOGER->AppDescriptiveLocation; ?></div></div><br />
<?php
if($SOGER->AppLocation!="login" && $SOGER->AppLocation!="login_documents" && count($SOGER->AppNavDIVs)>0) {
	$tmp .= "<div id=\"NavDiv\">\n";
	$tmp .= "<!-- NAV DIVs BEGIN -->\n[NAV_STUCTURE]\n<!-- NAV DIVs END -->\n";
	$SOGER->DIVmake_Navigation();
	$tmp .= "</div>\n";
	echo $tmp;
	}
?>


	<form name="FGEForm_1" id="FGEForm_1" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">

		<fieldset class="FGEfieldset">

			<div class="FGEFormTitle FGE4Col">Costi viaggio (euro per chilometro)</div>

				<div id="FGED_user_schede_rifiuti:Macchinari" class="FGE4Col">

				<?php
				$SQL="SELECT ID_MZ_TRA FROM lov_mezzi_trasporto;";
				$FEDIT->SDBRead($SQL,"MezziTrasporto",true,false);
				$CostiViaggio=array();
				for($m=0;$m<count($FEDIT->MezziTrasporto);$m++){
					$SQL="SELECT * FROM user_viaggi_impianti WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ID_MZ_TRA=".$FEDIT->MezziTrasporto[$m]['ID_MZ_TRA'].";";
					$FEDIT->SDBRead($SQL,"Costi",true,false);
					if($FEDIT->DbRecsNum==0) {
						$SQL="SELECT * FROM lov_costi_trasposto WHERE ID_MZ_TRA=".$FEDIT->MezziTrasporto[$m]['ID_MZ_TRA'].";";
						$FEDIT->SDBRead($SQL,"Costi",true,false);
						}
					$CostiViaggio[$FEDIT->MezziTrasporto[$m]['ID_MZ_TRA']] = $FEDIT->Costi[0];
					$CostiViaggio[$FEDIT->MezziTrasporto[$m]['ID_MZ_TRA']]['costo_totale'] = 
						$CostiViaggio[$FEDIT->MezziTrasporto[$m]['ID_MZ_TRA']]['costo_gasolio'] + 
						$CostiViaggio[$FEDIT->MezziTrasporto[$m]['ID_MZ_TRA']]['costo_autostrada'] + 
						$CostiViaggio[$FEDIT->MezziTrasporto[$m]['ID_MZ_TRA']]['costo_rc'] + 
						$CostiViaggio[$FEDIT->MezziTrasporto[$m]['ID_MZ_TRA']]['costo_bra'] + 
						$CostiViaggio[$FEDIT->MezziTrasporto[$m]['ID_MZ_TRA']]['costo_conducente'] + 
						$CostiViaggio[$FEDIT->MezziTrasporto[$m]['ID_MZ_TRA']]['costo_usura'] + 
						$CostiViaggio[$FEDIT->MezziTrasporto[$m]['ID_MZ_TRA']]['costo_generale'];
					}
				?>
					<table id="CostiViaggio">
						<tr>
							<th style="background-color:#8FA2B3;border-color:#8FA2B3">&nbsp;</th>
							<th>Gasolio</th>
							<th>Autostrada</th>
							<th>Assicurazione RCA</th>
							<th>Bollo, revisione, etc.</th>
							<th>Salario conducente</th>
							<th>Manutenzione</th>
							<th>Spese generali</th>
							<th>Totale</th>
						</tr>
						<tr>
							<th>Furgone</th>
							<td><input <?php if($SOGER->UserData["core_usersR7"]=="0") echo " disabled "; ?> onKeyUp="checkNumber(this);updateTotale('7');" type="text" size="5" name="7-costo_gasolio" id="7-costo_gasolio" value="<?php echo $CostiViaggio[7]['costo_gasolio']; ?>" /></td>
							<td><input <?php if($SOGER->UserData["core_usersR7"]=="0") echo " disabled "; ?> onKeyUp="checkNumber(this);updateTotale('7');" type="text" size="5" name="7-costo_autostrada" id="7-costo_autostrada" value="<?php echo $CostiViaggio[7]['costo_autostrada']; ?>" /></td>
							<td><input <?php if($SOGER->UserData["core_usersR7"]=="0") echo " disabled "; ?> onKeyUp="checkNumber(this);updateTotale('7');" type="text" size="5" name="7-costo_rc" id="7-costo_rc" value="<?php echo $CostiViaggio[7]['costo_rc']; ?>" /></td>
							<td><input <?php if($SOGER->UserData["core_usersR7"]=="0") echo " disabled "; ?> onKeyUp="checkNumber(this);updateTotale('7');" type="text" size="5" name="7-costo_bra" id="7-costo_bra" value="<?php echo $CostiViaggio[7]['costo_bra']; ?>" /></td>
							<td><input <?php if($SOGER->UserData["core_usersR7"]=="0") echo " disabled "; ?> onKeyUp="checkNumber(this);updateTotale('7');" type="text" size="5" name="7-costo_conducente" id="7-costo_conducente" value="<?php echo $CostiViaggio[7]['costo_conducente']; ?>" /></td>
							<td><input <?php if($SOGER->UserData["core_usersR7"]=="0") echo " disabled "; ?> onKeyUp="checkNumber(this);updateTotale('7');" type="text" size="5" name="7-costo_usura" id="7-costo_usura" value="<?php echo $CostiViaggio[7]['costo_usura']; ?>" /></td>
							<td><input <?php if($SOGER->UserData["core_usersR7"]=="0") echo " disabled "; ?> onKeyUp="checkNumber(this);updateTotale('7');" type="text" size="5" name="7-costo_generale" id="7-costo_generale" value="<?php echo $CostiViaggio[7]['costo_generale']; ?>" /></td>
							<td><input <?php if($SOGER->UserData["core_usersR7"]=="0") echo " disabled "; ?> onKeyUp="checkNumber(this);updateTotale('7');" type="text" size="5" name="7-costo_totale" id="7-costo_totale" value="<?php echo $CostiViaggio[7]['costo_totale']; ?>" disabled /></td>
						</tr>
						<tr>
							<th>Mezzo pesante</th>
							<td><input <?php if($SOGER->UserData["core_usersR7"]=="0") echo " disabled "; ?> onKeyUp="checkNumber(this);updateTotale('2');" type="text" size="5" name="2-costo_gasolio" id="2-costo_gasolio" value="<?php echo $CostiViaggio[2]['costo_gasolio']; ?>" /></td>
							<td><input <?php if($SOGER->UserData["core_usersR7"]=="0") echo " disabled "; ?> onKeyUp="checkNumber(this);updateTotale('2');" type="text" size="5" name="2-costo_autostrada" id="2-costo_autostrada" value="<?php echo $CostiViaggio[2]['costo_autostrada']; ?>" /></td>
							<td><input <?php if($SOGER->UserData["core_usersR7"]=="0") echo " disabled "; ?> onKeyUp="checkNumber(this);updateTotale('2');" type="text" size="5" name="2-costo_rc" id="2-costo_rc" value="<?php echo $CostiViaggio[2]['costo_rc']; ?>" /></td>
							<td><input <?php if($SOGER->UserData["core_usersR7"]=="0") echo " disabled "; ?> onKeyUp="checkNumber(this);updateTotale('2');" type="text" size="5" name="2-costo_bra" id="2-costo_bra" value="<?php echo $CostiViaggio[2]['costo_bra']; ?>" /></td>
							<td><input <?php if($SOGER->UserData["core_usersR7"]=="0") echo " disabled "; ?> onKeyUp="checkNumber(this);updateTotale('2');" type="text" size="5" name="2-costo_conducente" id="2-costo_conducente" value="<?php echo $CostiViaggio[2]['costo_conducente']; ?>" /></td>
							<td><input <?php if($SOGER->UserData["core_usersR7"]=="0") echo " disabled "; ?> onKeyUp="checkNumber(this);updateTotale('2');" type="text" size="5" name="2-costo_usura" id="2-costo_usura" value="<?php echo $CostiViaggio[2]['costo_usura']; ?>" /></td>
							<td><input <?php if($SOGER->UserData["core_usersR7"]=="0") echo " disabled "; ?> onKeyUp="checkNumber(this);updateTotale('2');" type="text" size="5" name="2-costo_generale" id="2-costo_generale" value="<?php echo $CostiViaggio[2]['costo_generale']; ?>" /></td>
							<td><input <?php if($SOGER->UserData["core_usersR7"]=="0") echo " disabled "; ?> onKeyUp="checkNumber(this);updateTotale('2');" type="text" size="5" name="2-costo_totale" id="2-costo_totale" value="<?php echo $CostiViaggio[2]['costo_totale']; ?>" disabled /></td>
						</tr>
					</table>
				</div>





			<div class="FGEFormTitle FGE4Col">Correttivo costo viaggio (euro per viaggio)</div>

				<div id="div_CostiViaggio_Correttivo" class="FGE4Col">

					<table id="CostiViaggio_Correttivo">
						<tr>
							<th style="background-color:#8FA2B3;border-color:#8FA2B3">&nbsp;</th>
							<th>Correttivo</th>
						</tr>
						<tr>
							<th>Furgone</th>
							<td><input <?php if($SOGER->UserData["core_usersR7"]=="0") echo " disabled "; ?> onKeyUp="checkNumber(this);" type="text" size="5" name="7-costo_correttivo" id="7-costo_correttivo" value="<?php echo $CostiViaggio[7]['costo_correttivo']; ?>" /></td>
						</tr>
						<tr>
							<th>Mezzo pesante</th>
							<td><input <?php if($SOGER->UserData["core_usersR7"]=="0") echo " disabled "; ?> onKeyUp="checkNumber(this);" type="text" size="5" name="2-costo_correttivo" id="2-costo_correttivo" value="<?php echo $CostiViaggio[2]['costo_correttivo']; ?>" /></td>
						</tr>
					</table>
				</div>








				<div class="FGESubmitRow">
					<input style="background: none repeat scroll 0% 0% rgb(3, 67, 115); color: white;" id="SubmitButton" class="FGEbutton" tabindex="67" value="Salva" onfocus="this.style.background='yellow';this.style.color='#034373';" onblur="this.style.background='#034373';this.style.color='white';" type="submit">
					
				</div>

		</fieldset>

	</form>


<?php
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>
