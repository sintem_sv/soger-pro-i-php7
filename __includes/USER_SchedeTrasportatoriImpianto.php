<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_impianti_trasportatori");
$FEDIT->FGE_SetFormFields(array("ID_AZT","description","ID_COM","IN_ITALIA","website","referente","telefono","cellulare","fax","email","ONU_62"),"user_impianti_trasportatori");
$FEDIT->FGE_HideFields(array("IN_ITALIA"),"user_impianti_trasportatori");

$FEDIT->FGE_SetTitle("description","Impianto","user_impianti_trasportatori");
$FEDIT->FGE_SetTitle("website","Recapiti","user_impianti_trasportatori");
$FEDIT->FGE_SetTitle("ONU_62","Responsabile rifiuti in ADR, classe 6.2","user_impianti_trasportatori");

$FEDIT->FGE_SetBreak("telefono","user_impianti_trasportatori");

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {
	
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_SetValue("ID_AZT",$_SESSION["FGE_IDs"]["user_aziende_trasportatori"],"user_impianti_trasportatori");
	$FEDIT->FGE_LookupDefault("ID_COM","user_impianti_trasportatori",true);
	$FEDIT->FGE_HideFields("ID_AZT","user_impianti_trasportatori");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea impianto");
	
} else {
	
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookupDefault("ID_COM","user_impianti_trasportatori",true);
	$FEDIT->FGE_HideFields("ID_AZT","user_impianti_trasportatori");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva impianto");
}
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>
