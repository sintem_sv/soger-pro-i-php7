<?php
global $SOGER;
if(!isset($TableSpec)) {
	$TableSpec = "";
}

if(!isset($_GET["pro"]) && !isset($_GET["dest"]) && !isset($_GET["tra"])) {
	if($SOGER->UserData["workmode"]=="produttore") {
		$sql2 .= " AND ${TableSpec}produttore='1'";
	} else {
		$sql2 .= " AND ${TableSpec}produttore='0'";
	}
	if($SOGER->UserData["workmode"]=="trasportatore") {
		$sql2 .= " AND ${TableSpec}trasportatore='1'";
	} else {
		$sql2 .= " AND ${TableSpec}trasportatore='0'";
	}
	if($SOGER->UserData["workmode"]=="destinatario") {
		$sql2 .= " AND ${TableSpec}destinatario='1'";
	} else {
		$sql2 .= " AND ${TableSpec}destinatario='0'";
	}	
} else {
	if($_GET["pro"]=="true") {
		$sql2 .= " AND ${TableSpec}produttore='1'";
	} else {
		$sql2 .= " AND ${TableSpec}produttore='0'";
	}
	if($_GET["tra"]=="true") {
		$sql2 .= " AND ${TableSpec}trasportatore='1'";
	} else {
		$sql2 .= " AND ${TableSpec}trasportatore='0'";
	}
	if($_GET["dest"]=="true") {
		$sql2 .= " AND ${TableSpec}destinatario='1'";
	} else {
		$sql2 .= " AND ${TableSpec}destinatario='0'";
	}		
}

#
$notFilter=array();
$notFilter[]="UserNuovoProduttore";
$notFilter[]="UserNuovoProduttoreImpianto";
$notFilter[]="UserNuovoProduttoreAutorizzazioni";
$notFilter[]="UserNuovoContrattoProduttore";
$notFilter[]="UserProduttoreContratti";
$notFilter[]="UserProduttoreContrattoCondizioni";
$notFilter[]="UserCondizioneContrattoProduttore";

$notFilter[]="UserNuovoDestinatario";
$notFilter[]="UserNuovoDestinatarioImpianto";
$notFilter[]="UserNuovoDestinatarioAutorizzazioni";
$notFilter[]="UserDestinatarioContratti";
$notFilter[]="UserNuovoContrattoDestinatario";
$notFilter[]="UserDestinatarioContrattoCondizioni";
$notFilter[]="UserCondizioneContrattoDestinatario";

$notFilter[]="UserNuovoIntermediario";
$notFilter[]="UserNuovoIntermediarioAutorizzazioni";
$notFilter[]="UserNuovoContrattoIntermediario";
$notFilter[]="UserIntermediarioContratti";
$notFilter[]="UserIntermediarioContrattoCondizioni";
$notFilter[]="UserCondizioneContrattoIntermediario";

$notFilter[]="UserNuovoTrasportatore";
$notFilter[]="UserNuovoTrasportatoreImpianto";
$notFilter[]="UserNuovoTrasportatoreAutorizzazioni";
$notFilter[]="UserNuovoTrasportatoreRimorchi";
$notFilter[]="UserNuovoTrasportatoreAutisti";
$notFilter[]="UserNuovoTrasportatoreAutomezzi";
$notFilter[]="UserTrasportatoreContratti";
$notFilter[]="UserNuovoContrattoTrasportatore";
$notFilter[]="UserTrasportatoreContrattoCondizioni";
$notFilter[]="UserCondizioneContrattoTrasportatore";

if(!in_array($SOGER->AppLocation,$notFilter))
	$sql2 .= " AND ${TableSpec}approved='1'";
#
?>
