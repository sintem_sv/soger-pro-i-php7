<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;

echo "<div class=\"FGEDataGridTitle\" style=\"margin-bottom:65px;\"><div>Controllo di gestione � Indici di gestione</div></div>";

$sql="SELECT * FROM lov_sdg_index;";
$FEDIT->SDBRead($sql,"DbRecordSet");

$js.= <<<JS
<script type="text/javascript">

$(document).ready(function(){

	$(".COL_KM").hide();
	$("#select_sdg").change(function(){
		var I = $("#select_sdg").val(); // 1=IS; 2=IT; 3=IK
		switch(I){
			case '1':
			case '2':
				$(".COL_KGD").show();
				$(".COL_KM").hide();
				break;
			case '3':
				$(".COL_KGD").hide();
				$(".COL_KM").show();
				break;
			}
		});

	$("#select_mese").change(function(){
		$("#select_trimestre").val($("#select_trimestre option:first").val());
		$("#select_anno").val($("#select_anno option:first").val());
		});

	$("#select_trimestre").change(function(){
		$("#select_mese").val($("#select_mese option:first").val());
		$("#select_anno").val($("#select_anno option:first").val());
		});

	$("#select_anno").change(function(){
		$("#select_trimestre").val($("#select_trimestre option:first").val());
		$("#select_mese").val($("#select_mese option:first").val());
		});
	
	//loading while ajax is getting info
	$('#showIndex').ajaxStart(function() {
		$('.INDEX_MIO, .INDEX_RIF, .KGD, .KM, .SCOST').html('<img src="__css/fb_loading.gif" />');
		});

	$('#showIndex').ajaxStop(function() {
		$('#canDownload').val(1);
		});
	
	$("#downloadPDF").click(function(){
		var TITLE = $('#select_sdg option:selected').text();
		$("#FileName").val(TITLE);
		if($('#canDownload').val()==1){
			$('#DownloadType').val('PDF');
			$("#INDICI2PDF").submit();
			}
		else
			alert("Attenzione, impossibile procedere con il download del file, procedere prima con il calcolo.");
		});

	$("#downloadCSV").click(function(){
		var TITLE = $('#select_sdg option:selected').text();
		$("#FileName").val(TITLE);
		if($('#canDownload').val()==1){
			$('#DownloadType').val('CSV');
			$("#INDICI2PDF").submit();
			}
		else
			alert("Attenzione, impossibile procedere con il download del file, procedere prima con il calcolo.");
		});

	$("#showIndex").click(function(){
		if( $("#select_mese").val()=='' && $("#select_trimestre").val()=='' && $("#select_anno").val()=='')
			alert("Attenzione, selezionare il periodo di riferimento");
		else{
			var MyRIF	= new Array();
			var MyCER	= new Array();
			var MyCERCODE	= new Array();

			//populate array
			for(r=0;r<$("#rifiutiCounter").val();r++){
				MyCER[r]	=$("#ID_CER_"+r).val();
				MyCERCODE[r]=$("#ID_CERCODE_"+r).val();
				MyRIF[r]	=$("#ID_RIF_"+r).val();
				}
			
			//get data and update view
			$.post('__sdg/getIndex.php', {CERCODE:MyCERCODE, CER:MyCER, RIF:MyRIF, INDICE:$("#select_sdg").val(), mese:$("#select_mese").val(), trimestre:$("#select_trimestre").val(), anno:$("#select_anno").val()}, function(phpResponse){
				//alert(dump(phpResponse));
				for(r=0;r<$("#rifiutiCounter").val();r++){

					// MIO INDICE
					$("#IM_"+r).html(phpResponse[r]['IM']);
					$("#IH_IM_"+r).val(phpResponse[r]['IM']);

					// INDICE NAZIONALE
					$("#IR_"+r).html(phpResponse[r]['IR']);
					$("#IH_IR_"+r).val(phpResponse[r]['IR']);

					// SCARTO
					scarto=phpResponse[r]['SC'];
					if(scarto!='n.d.'){
						$("#SC_"+r).html(scarto+" %");
						$("#IH_SC_"+r).val(scarto+" %");
						}
					else{
						$("#SC_"+r).html(scarto);
						$("#IH_SC_"+r).val(scarto);
						}

					if(parseFloat(scarto)<0){
						$("#SC_"+r).css('color', '#FF0000');
						}
					if(parseFloat(scarto)>0){
						$("#SC_"+r).css('color', '#037214');
						}
					
					// KG A DESTINO
					kg=phpResponse[r]['KG'];
					$("#KG_"+r).html(kg+" kg");
					$("#IH_KG_"+r).val(kg+" kg");
					
					// KM MEDI
					km=phpResponse[r]['KM'];
					$("#KM_"+r).html(km+" km");
					$("#IH_KM_"+r).val(km+" km");
					}				
				},"json");
			}
		});//click
			
	});

</script>
JS;

echo $js;

$tableConfig ="<table id=\"TableConfigIndex\" cellpadding='0' cellspacing='1'>\n<form name='INDICI2PDF' id='INDICI2PDF' method='POST' action='__sdg/DownloadIndici.php'>\n";

$tableConfig.="<thead><tr>";
	$tableConfig.="<th colspan=\"4\">CONFIGURA VISUALIZZAZIONE</th>\n";
$tableConfig.="</tr>\n</thead><tbody>\n";

	$tableConfig.="<tr><th>Seleziona l'indice di gestione</th>";
		
		$tableConfig.="<td colspan=\"3\">";
			$tableConfig.="<select name='sdg' id='select_sdg'>";
			for($i=0;$i<count($FEDIT->DbRecordSet);$i++){
				$tableConfig.="<option value='".$FEDIT->DbRecordSet[$i]['id']."'>".$FEDIT->DbRecordSet[$i]['INDEX']." - ".$FEDIT->DbRecordSet[$i]['description']."</option>";
				}
			$tableConfig.="</select>";
		$tableConfig.="</td></tr>";

	$tableConfig.="<tr><th>Indica il periodo di riferimento</th>";

		## MESE
		$tableConfig.="<td>";
			$tableConfig.="<select name='mese' id='select_mese'>";
				$tableConfig.="<option value=''>-- mese --</option>";
				$tableConfig.="<option value='January'>Gennaio ".substr($_SESSION["DbInUse"], -4)."</option>";
				$tableConfig.="<option value='February'>Febbraio ".substr($_SESSION["DbInUse"], -4)."</option>";
				$tableConfig.="<option value='March'>Marzo ".substr($_SESSION["DbInUse"], -4)."</option>";
				$tableConfig.="<option value='April'>Aprile ".substr($_SESSION["DbInUse"], -4)."</option>";
				$tableConfig.="<option value='May'>Maggio ".substr($_SESSION["DbInUse"], -4)."</option>";
				$tableConfig.="<option value='June'>Giugno ".substr($_SESSION["DbInUse"], -4)."</option>";
				$tableConfig.="<option value='July'>Luglio ".substr($_SESSION["DbInUse"], -4)."</option>";
				$tableConfig.="<option value='August'>Agosto ".substr($_SESSION["DbInUse"], -4)."</option>";
				$tableConfig.="<option value='September'>Settembre ".substr($_SESSION["DbInUse"], -4)."</option>";
				$tableConfig.="<option value='October'>Ottobre ".substr($_SESSION["DbInUse"], -4)."</option>";
				$tableConfig.="<option value='November'>Novembre ".substr($_SESSION["DbInUse"], -4)."</option>";
				$tableConfig.="<option value='December'>Dicembre ".substr($_SESSION["DbInUse"], -4)."</option>";
			$tableConfig.="</select>";
		$tableConfig.="</td>";

		## TRIMESTRE
		$tableConfig.="<td>";
			$tableConfig.="<select name='trimestre' id='select_trimestre'>";
				$tableConfig.="<option value=''>-- trimestre --</option>";
				$tableConfig.="<option value='trim_I'>Trimestre 1: gennaio, febbraio, marzo ".substr($_SESSION["DbInUse"], -4)."</option>";
				$tableConfig.="<option value='trim_II'>Trimestre 2: aprlie, maggio, giugno ".substr($_SESSION["DbInUse"], -4)."</option>";
				$tableConfig.="<option value='trim_III'>Trimestre 3: luglio, agosto, settembre ".substr($_SESSION["DbInUse"], -4)."</option>";
				$tableConfig.="<option value='trim_IV'>Trimestre 4: ottobre, novembre, dicembre ".substr($_SESSION["DbInUse"], -4)."</option>";
			$tableConfig.="</select>";
		$tableConfig.="</td>";

		## ANNO 
		$tableConfig.="<td>";
			$tableConfig.="<select name='anno' id='select_anno'>";
				$tableConfig.="<option value=''>-- anno --</option>";
				$tableConfig.="<option value='".substr($_SESSION["DbInUse"], -4)."'>Anno ".substr($_SESSION["DbInUse"], -4)."</option>";
			$tableConfig.="</select>";
		$tableConfig.="</td></tr>";


		$tableConfig.="<td colspan=\"4\">";
			## SUBMIT 
			$tableConfig.="<input class='IndexButton' type='button' id='showIndex' value='CALCOLA INDICE' />";
			## PDF 
			$tableConfig.="<input class='IndexButton' type='button' id='downloadPDF' value='DOWNLOAD PDF' />";
			## CSV
			$tableConfig.="<input class='IndexButton' type='button' id='downloadCSV' value='DOWNLOAD CSV' />";
		$tableConfig.="</td></tr>";
	
	$tableConfig.="</tbody></table>";




$sql="SELECT ID_RIF, lov_cer.COD_CER AS CER, lov_cer.ID_CER AS ID_CER, descrizione, ID_RIF FROM user_schede_rifiuti JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1 ORDER BY COD_CER;";
$FEDIT->SDBRead($sql,"DbRecordset");

$table="<table id=\"TableIndex\" cellspacing='1' cellpadding='0'>";

	$table.="<thead><tr>";
		$table.="<th>CODICE CER</th>\n";
		$table.="<th>DESCRIZIONE</th>\n";
		$table.="<th>INDICE</th>\n";
		$table.="<th>INDICE NAZ.</th>\n";
		$table.="<th>SCARTO %</th>\n";
		$table.="<th class=\"COL_KGD\">KG (a destino)</th>\n";
		$table.="<th class=\"COL_KM\">KM MEDI</th>\n";
	$table.="</tr>\n</thead><tbody>\n";

for($r=0;$r<count($FEDIT->DbRecordset);$r++){
	$table.="<tr class=\"ncRow\">";
		$table.="<td class=\"CER\">".$FEDIT->DbRecordset[$r]['CER'];
			$table.="<input type='hidden' name='ID_CER_".$r."' id='ID_CER_".$r."' value='".$FEDIT->DbRecordset[$r]['ID_CER']."' />";
			$table.="<input type='hidden' name='ID_CERCODE_".$r."' id='ID_CERCODE_".$r."' value='".$FEDIT->DbRecordset[$r]['CER']."' />";
			$table.="<input type='hidden' name='ID_RIF_".$r."' id='ID_RIF_".$r."' value='".$FEDIT->DbRecordset[$r]['ID_RIF']."' />";
		$table.="</td>\n";
		$table.="<td class=\"DESC\" name=\"DESC_".$r."\" id=\"DESC_".$r."\">".$FEDIT->DbRecordset[$r]['descrizione']."</td><input type='hidden' name='IH_DESC_".$r."' id='IH_DESC_".$r."' value='".$FEDIT->DbRecordset[$r]['descrizione']."' />\n";
		$table.="<td class=\"INDEX_MIO\" name='IM_".$r."' id='IM_".$r."'></td><input type='hidden' name='IH_IM_".$r."' id='IH_IM_".$r."' value='' />\n";
		$table.="<td class=\"INDEX_RIF\" name='IR_".$r."' id='IR_".$r."'></td><input type='hidden' name='IH_IR_".$r."' id='IH_IR_".$r."' value='' />\n";
		$table.="<td class=\"SCOST\" name='SC_".$r."' id='SC_".$r."'></td><input type='hidden' name='IH_SC_".$r."' id='IH_SC_".$r."' value='' />\n";
		$table.="<td class=\"COL_KGD KGD\" name='KG_".$r."' id='KG_".$r."'></td><input type='hidden' name='IH_KG_".$r."' id='IH_KG_".$r."' value='' />\n";
		$table.="<td class=\"COL_KM KM\" name='KM_".$r."' id='KM_".$r."'></td><input type='hidden' name='IH_KM_".$r."' id='IH_KM_".$r."' value='' />\n";
	$table.="</tr>\n";
	}

$table.="</tbody></table>";

## rifiutiCounter
$table.="<input type='hidden' name='rifiutiCounter' id='rifiutiCounter' value='".count($FEDIT->DbRecordset)."' />\n";
$table.="<input type='hidden' name='canDownload' id='canDownload' value='0' />\n";
$table.="<input type='hidden' name='DownloadType' id='DownloadType' value='' />\n";
$table.="<input type='hidden' name='FileName' id='FileName' value='' />\n";
$table.="</form>";

echo $tableConfig;
echo $title;
echo $table;

require_once("__includes/COMMON_sleepForgEdit.php");
?>