<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_impianti_produttori");
$FEDIT->FGE_SetFormFields(array("ID_AZP","description","ID_COM","IN_ITALIA","FuoriUnitaLocale","NFirstMOV","website","referente","telefono","cellulare","fax","email","ONU_62"),"user_impianti_produttori");
$FEDIT->FGE_HideFields(array("IN_ITALIA"),"user_impianti_produttori");

$FEDIT->FGE_SetTitle("description","Impianto","user_impianti_produttori");
$FEDIT->FGE_SetTitle("website","Recapiti","user_impianti_produttori");
$FEDIT->FGE_SetTitle("ONU_62","Responsabile rifiuti in ADR, classe 6.2","user_impianti_produttori");

$FEDIT->FGE_SetBreak("telefono","user_impianti_produttori");

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_SetValue("ID_AZP",$_SESSION["FGE_IDs"]["user_aziende_produttori"],"user_impianti_produttori");
	$FEDIT->FGE_SetValue("NFirstMOV","1","user_impianti_produttori");
	$FEDIT->FGE_SetValue("FuoriUnitaLocale","0","user_impianti_produttori");
	$FEDIT->FGE_LookUpDefault("ID_COM","user_impianti_produttori",true);
	$FEDIT->FGE_HideFields(array("ID_AZP"),"user_impianti_produttori");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea impianto");	
} else {

	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_COM","user_impianti_produttori",true);
	$FEDIT->FGE_HideFields(array("ID_AZP"),"user_impianti_produttori");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva impianto");
}
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>