<?php

require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;


?>


<div id="FGEmultiblock"> 
 
<div class="FGEDataGridTitle"><div>DOCUMENTI ALLEGATI</div></div>
<?php
if($SOGER->AppLocation!="login" && $SOGER->AppLocation!="login_documents" && count($SOGER->AppNavDIVs)>0) {
	$tmp .= "<div id=\"NavDiv\">\n";
	$tmp .= "<!-- NAV DIVs BEGIN -->\n[NAV_STUCTURE]\n<!-- NAV DIVs END -->\n";
	$SOGER->DIVmake_Navigation();
	$tmp .= "</div>\n";
	echo $tmp;
	}
?>


<form name="FGEForm_1" id="FGEForm_1" method="post" action="__mov_snipplets/UploadAllegato.php" enctype="multipart/form-data"> 
<fieldset  class="FGEfieldset"> 
 
<div id="FGED_doc_allegato" class="FGE1Col"> 
<label for="doc_allegato" class="FGElabel">Seleziona il file da allegare</label><br/>
<input type="file" class="FGEinput_file" id="doc_allegato" name="doc_allegato" tabindex="50" size="15" maxlength="15" value="" onfocus="this.style.background='yellow';" onblur="this.style.background='white';"/> 
</div>

<div class="FGENewLine">&nbsp;</div> 
<div class="FGENewLine">&nbsp;</div> 

<div id="FGED_doc:allegato_submit" class="FGE1Col"> 
<input type="submit" id="SubmitButton" class="FGEbutton" tabindex="53" value="carica il file" onfocus="this.style.background='yellow';this.style.color='#034373';" onblur="this.style.background='#034373';this.style.color='white';"/>
</div> 

<input type="hidden" name="soggetto" value="<?php echo $_GET['table']; ?>" />
<input type="hidden" name="id" value="<?php echo $_GET['filter']; ?>"/>
 
</fieldset></form> 
 
</div>


<div id="FGEmultiblock"> 
 
<div class="FGEDataGridTitle"><div>ALLEGATI</div></div><br />

<?php

$arrayfiles=array();
$directory='__upload/allegati/intermediario/';

if(file_exists($directory)){	
	
	$handle=opendir($directory);
	while(false !== ($file = readdir($handle))){	
		if(is_file($directory.$file)){
			# controllo codice fiscale
			//print_r(substr($file, 1, strpos($file, "]")-1));
			if(substr($file, 1, strpos($file, "]")-1)==$_GET['filter'])
				array_push($arrayfiles, $file);			
			}	
		}	
	$handle = closedir($handle);
	}

if(count($arrayfiles)>0){

	$url = $_SERVER["QUERY_STRING"]; 

	$replace="[".$_GET['filter']."]";

	$table ="<table class=\"FGEDataGridTable\"><tbody>";
		$table.="<tr>";
			$table.="<th class=\"FGEDataGridTableHeader\" id=\"Documento\">Documento</th>";
			$table.="<th></th>";
		$table.="</tr>";

	for($f=0;$f<count($arrayfiles);$f++){

		$table.="<tr>";
			$table.="<td class=\"Tcell\" style=\"text-align:left; padding-left:10px;\" headers=\"Documento\">".utf8_decode(str_replace($replace, "", $arrayfiles[$f]))."</td>";
			$table.="<td class=\"Tcell\"><a href=\"".$directory.utf8_decode($arrayfiles[$f])."\" target=\"_blank\" title=\"scarica il file\"><img src=\"__css/disk.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-left: 1px; margin-top: 1px; margin-right: 2px; alt=\"scarica il file\" /></a>";
			$table.="<a href=\"javascript:if(window.confirm('Sei sicuro di voler eliminare il file?')){location.replace('__mov_snipplets/DeleteAllegato.php?$url&amp;directory=".$directory."&amp;file=".addslashes(utf8_decode($arrayfiles[$f]))."')};\" title=\"elimina il file\"><img src=\"__css/FGE_delete.png\" class=\"NoBorder\" style=\"margin-bottom: 1px; margin-left: 1px; margin-top: 1px; margin-right: 2px; alt=\"elimina il file\" /></a></td>";
		
	}

	$table.="</tbody></table>";

	echo $table;

}

?>
 
</div>




<?php
require_once("__includes/COMMON_sleepForgEdit.php");
?>