<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");

global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("core_users");

$FEDIT->FGE_SetFormFields(array("ID_IMP","description","nome","cognome","email","usr","pwd","sresponsabile"),"core_users");

$FEDIT->FGE_SetTitle("ID_IMP","Stabilimento","core_users");
$FEDIT->FGE_SetTitle("nome","Credenziali di accesso","core_users");

$FEDIT->FGE_SetBreak("usr","core_users");

$FEDIT->FGE_HideFields(array("sresponsabile","description"),"core_users");

$FEDIT->FGE_DescribeFields();

$FEDIT->FGE_LookUpDefault("ID_IMP","core_users");

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {
	$FEDIT->FGE_SetValue("sresponsabile",1,"core_users");
	$FEDIT->FGE_SetValue("description","Responsabile di sito","core_users");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea responsabile");
	} 
else {
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica responsabile");
	}

require_once("__includes/COMMON_sleepForgEdit.php");
?>