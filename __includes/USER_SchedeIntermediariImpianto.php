<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_impianti_intermediari");
$FEDIT->FGE_SetFormFields(array("ID_AZI","description","ID_COM","IN_ITALIA","website","referente","telefono","cellulare","fax","email","printRecapitoInFir","ONU_62"),"user_impianti_intermediari");
$FEDIT->FGE_HideFields(array("IN_ITALIA"),"user_impianti_intermediari");

$FEDIT->FGE_SetTitle("description","Impianto","user_impianti_intermediari");
$FEDIT->FGE_SetTitle("website","Recapiti","user_impianti_intermediari");
$FEDIT->FGE_SetTitle("ONU_62","Responsabile rifiuti in ADR, classe 6.2","user_impianti_intermediari");

$FEDIT->FGE_SetBreak("telefono","user_impianti_intermediari");

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {
	
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_SetValue("ID_AZI",$_SESSION["FGE_IDs"]["user_aziende_intermediari"],"user_impianti_intermediari");
	$FEDIT->FGE_LookUpDefault("ID_COM","user_impianti_intermediari",true);
	$FEDIT->FGE_HideFields("ID_AZI","user_impianti_intermediari");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea impianto");
	
} else {
	
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_COM","user_impianti_intermediari",true);
	$FEDIT->FGE_HideFields("ID_AZI","user_impianti_intermediari");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva impianto");
}
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>