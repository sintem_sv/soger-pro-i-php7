<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("lov_comuni_istat");
$FEDIT->FGE_SetFormFields(array("description","CAP","cod_comISTAT","cod_prov","des_prov","shdes_prov","cod_reg","des_reg","IN_ITALIA","nazione","codice_catastale"),"lov_comuni_istat");
$FEDIT->FGE_DescribeFields();

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {
	$FEDIT->FGE_SetValue("IN_ITALIA",1,"lov_comuni_istat");
	$FEDIT->FGE_SetValue("nazione","ITALIA","lov_comuni_istat");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea comune");	
} else {
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva comune");	
}
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>
