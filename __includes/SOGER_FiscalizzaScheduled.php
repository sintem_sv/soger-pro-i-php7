<?php

require_once("__classes/ForgEdit2.class");
require_once("__classes/ForgEdit.RegExp");
require_once("__classes/DbLink.class");
require_once("__libs/SQLFunct.php");
require("__includes/COMMON_wakeForgEdit.php");
require("__includes/COMMON_ForgEditClassFiles.php");

global $SOGER;


$FISCAL_1 = <<< ENDED

<div id="FISCAL" style="display:none">

	<div id="title">Fiscalizzazione dei movimenti - step 1 di 2</div>

	<div id="txt1" style="margin-left:20px;"><br /><br /><b>Prospetto dei movimenti che verranno inseriti nel registro fiscale:</b></div>
	
	<form action="javascript:completeAHAH.likeSubmit('__includes/SOGER_FiscalizzaWriteScheduled.php', 'POST', 'FRM_fiscal', 'table_prospetto');" id="FRM_fiscal" name="FRM_fiscal" method="POST">
	
	<div id="table_prospetto">

		<table cellpadding="0" cellspacing="0" id="prospetto_movimenti">

			<tbody id="tablebody"></tbody>

		</table>

	</div>

	<div id="pulsanti">
		<input class="FiscalButton" type="button" name="invia_movimenti" value="Scrivi registro fiscale" onClick="javascript:validate();" />
		<input class="FiscalButton" type="reset"  name="annulla_edits"   value="Annulla le modifiche" />
		<input class="FiscalButton" type="button" name="chiudi_popup"    value="Chiedi in seguito" onClick="javascript:obj=document.getElementById('FISCAL');obj.style.display='none';clearTable('tablebody');" />
	</div>

	</form>


	<!-- <div id="txt2">Attendere prego</div> -->


</div>

ENDED;

echo $FISCAL_1;

$javascript ="<script type='text/javascript'>\n";
$javascript.= <<< JAVASCRIPT

var obj=document.getElementById('FISCAL');
obj.style.display='none';
//setTimeout(hideMyDiv,10000);

function validate(){
	var counter = document.getElementById('counter').value;
	var canSubmit = 0;
	var reason = 0; // 1=num non corretto // 2=qta troppo basse

	// controllo formato dati inseriti
	for(i=0; i<counter; i++){

		fieldValue=document.getElementById('CSTMquantita_'+i).value;

		if(fieldValue=='')
			document.getElementById('CSTMquantita_'+i).value = 0;

		if(fieldValue.charAt(fieldValue.length - 1)=='.'){
			document.getElementById('CSTMquantita_'+i).value = fieldValue.substring(0, (fieldValue.length) - 1);
			}

		}
		
		if(counter>0){
			var DATA_FISCtmp = document.getElementById('DTMOV_FISC').value.split('-');
			var DATA_FISC	 = DATA_FISCtmp[1]+"/"+DATA_FISCtmp[2]+"/"+DATA_FISCtmp[0];
			var DATA_FISCprint	 = DATA_FISCtmp[2]+"/"+DATA_FISCtmp[1]+"/"+DATA_FISCtmp[0];

			var DATA_INDtmp  = document.getElementById('DTMOV_0').value.split('-');
			var DATA_IND	 = DATA_INDtmp[1]+"/"+DATA_INDtmp[2]+"/"+DATA_INDtmp[0];
			var DATA_INDprint	 = DATA_INDtmp[2]+"/"+DATA_INDtmp[1]+"/"+DATA_INDtmp[0];

			var DF = new Date(DATA_FISC);
			var DI = new Date(DATA_IND);  

			// date nel formato mese/giorno/anno
			var date_diff = (DI.getTime() - DF.getTime())/86400000;

			if(date_diff<0 && DATA_INDprint!='00/00/0000' ){
				reason=3;
				canSubmit++;
				}
			}

	if(canSubmit==0){
		// cerco eventuali scarichi da fiscalizzare
		for(i=0; i<counter; i++){
			if(document.getElementById('TIPO_'+i).value=='S'){
				var scarico = document.getElementById('quantita_'+i).value;
				var carico = 0;
				var gotGiacenza=false;
				for(k=0; k<i; k++){
					if(document.getElementById('ID_RIF_'+k).value==document.getElementById('ID_RIF_'+i).value){
						if(document.getElementById('TIPO_'+k).value=='C'){
							carico = (carico * 1) + (document.getElementById('CSTMquantita_'+k).value * 1);
							if(!gotGiacenza){
								var giacenza = document.getElementById('FKEdisponibilita_'+k).value;
								gotGiacenza=true;
								}
							var CtoEdit=k;
							}
						else
							carico = (carico * 1) - (document.getElementById('quantita_'+k).value * 1);
						}
					}
				// PROBLEMA: la giacenza � calcolata sempre in kg, devo convertirla nell'unit� di misura del rifiuto!
				//alert(document.getElementById('FKEumis_'+arrayS[i]).value);
				switch(document.getElementById('FKEumis_'+arrayS[i]).value){
					case 'Litri':
						ps = (document.getElementById('FKEpesospecifico_'+arrayS[i]).value * 1);
						giacenza = giacenza / ps;
						break
					case 'Mc.':
						ps = (document.getElementById('FKEpesospecifico_'+arrayS[i]).value * 1);
						giacenza = giacenza / ps / 1000;
						break;
					}

				var disponibilita=(giacenza * 1) + (carico * 1);
				var difference=scarico-disponibilita;
				
				delete giacenze[document.getElementById('ID_RIF_'+arrayS[i]).value];

				if(difference>0){
					//window.alert("ID_RIF: "+ document.getElementById('ID_RIF_'+i).value +", giacenza: "+giacenza+", carico: "+carico+", scarico: "+scarico);
					reason=2;
					canSubmit++;
					document.getElementById('CSTMquantita_'+CtoEdit).style.background='red';
					document.getElementById('CSTMquantita_'+CtoEdit).value=(document.getElementById('CSTMquantita_'+CtoEdit).value *1 ) + (difference * 1);
					}
				}
			}
		}

	if(canSubmit==0){
		document.FRM_fiscal.submit();
		adaptTxt();
		}
	else{
		switch(reason){
			case 1:
				window.alert("Attenzione, verifica il formato dei dati immessi.");
				break;
			case 2:
				window.alert("Attenzione, i quantitativi sono stati abbassati oltre il consentito. So.Ge.R. Pro setter� ora il quantitativo minimo che � necessario caricare per il rifiuto. Verificare quanto immesso e procedere nuovamente al salvataggio.");
				break;
			case 3:
				window.alert("Attenzione, sul registro fiscale sono presenti movimenti con data "+DATA_FISCprint+", successiva a quella di uno dei movimenti che si sta cercando di fiscalizzare ("+DATA_INDprint+"). Impossibile completare l'operazione.");
				break;
			}
		}

	}

function adaptTxt(){

	// titolo
	titolo=document.getElementById('title');
	titolo.innerHTML="Fiscalizzazione dei movimenti - step 2 di 2";

	// messaggio
	msg=document.getElementById('txt1');
	msg.innerHTML="<br /><br /><b>Esito delle operazioni di scrittura sul database:</b>";

	// pulsanti
	btn=document.getElementById('pulsanti');
	btn.innerHTML="<input type=\"button\" name=\"CloseAndReload\" value=\"Chiudi finestra\" class=\"FiscalButton\" onClick=\"window.location.reload();\" />";

	}

function str_between(string, prefix, suffix) {
	s = string;
	var i = s.indexOf(prefix);	
	if (i >= 0) {
		s = s.substring(i + prefix.length);
		}
	else {
		return '';
		}
	if (suffix) {
		i = s.indexOf(suffix);
		if (i >= 0) {
			s = s.substring(0, i);
			}
		else {
			return '';
			}
		}
	return s;
	}

function clearTable(tbodyId){
	document.getElementById(tbodyId).innerHTML='';
	}


function showFiscal(NMOVto, ID_MOV){
	var LKUP = new ajaxObject('__includes/SOGER_PreparaFiscalizzazione.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		// [K]Chiave[V]Valore[;][K]Chiave[V]Valore[;][END]
		movimenti=responseTxt.split("[END]");
		//window.alert(responseTxt);
		var extraData = movimenti[movimenti.length-1].split("|");
				
		if(document.createElement && document.getElementById && document.getElementsByTagName) {
				
				
			//var oTbody=document.createElement("TBODY");
			//oTbody.setAttribute('id', 'tablebody');
			var oTrMain=document.createElement("TR");
			var oTh1=document.createElement("TH");
			var oTh2=document.createElement("TH");
			var oTh3=document.createElement("TH");
			var oTh4=document.createElement("TH");
			var oTh5=document.createElement("TH");
			
			oTh1.setAttribute('width', '25');
			oTh2.setAttribute('width', '75');
			oTh3.setAttribute('width', '240');
			oTh4.setAttribute('width', '140');
			//oTh4.setAttribute('colspan', '2');
			oTh4.setAttribute('width', '60');
			oTh5.setAttribute('width', '80');

			oTh1.innerHTML="Tipo";
			oTh2.innerHTML="Data";
			oTh3.innerHTML="Rifiuto";
			oTh4.innerHTML="Quantit�";
			//oTh4.innerHTML="QTA1";
			oTh5.innerHTML="";

			//oTbody.appendChild(oTrMain);		
			oTrMain.appendChild(oTh1);
			oTrMain.appendChild(oTh2);
			oTrMain.appendChild(oTh3);
			oTrMain.appendChild(oTh4);
			oTrMain.appendChild(oTh5);

			document.getElementById('tablebody').appendChild(oTrMain);
			//document.getElementById('prospetto_movimenti').appendChild(oTbody);

				
			for(m=0;m<movimenti.length-1;m++){ // tolgo l' ultimo indice, quello che va dall' ultimo [END] in poi..
				// crea elementi

				var oTr=document.createElement("TR");
				
				var oTd1=document.createElement("TD");
				var oTd2=document.createElement("TD");
				var oTd3=document.createElement("TD");
				var oTd4=document.createElement("TD");
				var oTd5=document.createElement("TD");

				oTd1.setAttribute('align', 'center');
				oTd2.setAttribute('align', 'center');
				oTd3.setAttribute('style', 'padding-left:3px');
				oTd4.setAttribute('align', 'right');
				oTd5.setAttribute('align', 'right');
				oTd4.setAttribute('width', '70');
				oTd5.setAttribute('width', '70');

				DATA = str_between(movimenti[m], "[K]DTMOV[V]", "[;]"); 
				if(DATA!='0000-00-00'){
					DATA = DATA.split("-");
					DATA = DATA[2] + "/" + DATA[1] + "/" + DATA[0];
					}
				else
					DATA = 'n.d.';

				TIPO = str_between(movimenti[m], "[K]TIPO[V]", "[;]");
				TIPO_S_SCHEDA = str_between(movimenti[m], "[K]TIPO_S_SCHEDA[V]", "[;]");
				SenzaTrasporto = str_between(movimenti[m], "[K]SenzaTrasporto[V]", "[;]");
				Transfrontaliero = str_between(movimenti[m], "[K]Transfrontaliero[V]", "[;]");
				FISCALE = str_between(movimenti[m], "[K]FISCALE[V]", "[;]");
				FISCALIZZATO = 1;
				DTMOV = str_between(movimenti[m], "[K]DTMOV[V]", "[;]");
				ID_IMP = str_between(movimenti[m], "[K]ID_IMP[V]", "[;]");
				ID_RIF = str_between(movimenti[m], "[K]ID_RIF[V]", "[;]");
				FANGHI = str_between(movimenti[m], "[K]FANGHI[V]", "[;]");
				RIF  = str_between(movimenti[m], "[K]RIF_desc[V]", "[;]"); 
				quantita  = str_between(movimenti[m], "[K]quantita[V]", "[;]");
				//quantita_residua  = str_between(movimenti[m], "[K]quantita_residua[V]", "[;]");
				qta_hidden  = str_between(movimenti[m], "[K]qta_hidden[V]", "[;]");
				pesoL  = str_between(movimenti[m], "[K]pesoL[V]", "[;]");
				pesoN  = str_between(movimenti[m], "[K]pesoN[V]", "[;]");
				tara   = str_between(movimenti[m], "[K]tara[V]", "[;]");
				Km   = str_between(movimenti[m], "[K]Km[V]", "[;]");
				FKEdisponibilita   = str_between(movimenti[m], "[K]FKEdisponibilita[V]", "[;]");
				FKEgiacINI   = str_between(movimenti[m], "[K]FKEgiacINI[V]", "[;]");
				FKElastCarico   = str_between(movimenti[m], "[K]FKElastCarico[V]", "[;]");
				FKErifCarico   = str_between(movimenti[m], "[K]FKErifCarico[V]", "[;]");
				FKErifIND = str_between(movimenti[m], "[K]FKErifIND[V]", "[;]");
				NPER   = str_between(movimenti[m], "[K]NPER[V]", "[;]");
				FKEumis = str_between(movimenti[m], "[K]FKEumis[V]", "[;]"); 
				FKESF = str_between(movimenti[m], "[K]FKESF[V]", "[;]"); 
				FKEpesospecifico = str_between(movimenti[m], "[K]FKEpesospecifico[V]", "[;]"); 
				NFORM = str_between(movimenti[m], "[K]NFORM[V]", "[;]"); 
				DTFORM = str_between(movimenti[m], "[K]DTFORM[V]", "[;]"); 
				FANGHI_BOLLA = str_between(movimenti[m], "[K]FANGHI_BOLLA[V]", "[;]"); 
				ID_COMM = str_between(movimenti[m], "[K]ID_COMM[V]", "[;]"); 
				ID_CAR = str_between(movimenti[m], "[K]ID_CAR[V]", "[;]"); 
				// produttore
				ID_AZP = str_between(movimenti[m], "[K]ID_AZP[V]", "[;]"); 
				FKEcfiscP = str_between(movimenti[m], "[K]FKEcfiscP[V]", "[;]"); 
				ID_UIMP = str_between(movimenti[m], "[K]ID_UIMP[V]", "[;]"); 
				ID_AUTH = str_between(movimenti[m], "[K]ID_AUTH[V]", "[;]"); 
				FKErilascioP = str_between(movimenti[m], "[K]FKErilascioP[V]", "[;]"); 
				FKEscadenzaP = str_between(movimenti[m], "[K]FKEscadenzaP[V]", "[;]"); 
				// destinatario
				ID_AZD = str_between(movimenti[m], "[K]ID_AZD[V]", "[;]"); 
				FKEcfiscD = str_between(movimenti[m], "[K]FKEcfiscD[V]", "[;]"); 
				ID_UIMD = str_between(movimenti[m], "[K]ID_UIMD[V]", "[;]"); 
				ID_AUTHD = str_between(movimenti[m], "[K]ID_AUTHD[V]", "[;]"); 
				FKErilascioD = str_between(movimenti[m], "[K]FKErilascioD[V]", "[;]"); 
				FKEscadenzaD = str_between(movimenti[m], "[K]FKEscadenzaD[V]", "[;]"); 
				ID_OP_RS = str_between(movimenti[m], "[K]ID_OP_RS[V]", "[;]"); 
				ID_TRAT = str_between(movimenti[m], "[K]ID_TRAT[V]", "[;]"); 
				// trasportatore
				ID_AZT = str_between(movimenti[m], "[K]ID_AZT[V]", "[;]"); 
				FKEcfiscT = str_between(movimenti[m], "[K]FKEcfiscT[V]", "[;]"); 
				ID_UIMT = str_between(movimenti[m], "[K]ID_UIMT[V]", "[;]"); 
				ID_AUTHT = str_between(movimenti[m], "[K]ID_AUTHT[V]", "[;]"); 
				FKErilascioT = str_between(movimenti[m], "[K]FKErilascioT[V]", "[;]"); 
				FKEscadenzaT = str_between(movimenti[m], "[K]FKEscadenzaT[V]", "[;]"); 
				NumAlboAutotrasp = str_between(movimenti[m], "[K]NumAlboAutotrasp[V]", "[;]"); 
				NumAlboAutotraspProprio = str_between(movimenti[m], "[K]NumAlboAutotraspProprio[V]", "[;]");
				adr = str_between(movimenti[m], "[K]adr[V]", "[;]");
				COLLI = str_between(movimenti[m], "[K]COLLI[V]", "[;]"); 
				ID_TIPO_IMBALLAGGIO_ = str_between(movimenti[m], "[K]ID_TIPO_IMBALLAGGIO[V]", "[;]"); 
				ALTRO_TIPO_IMBALLAGGIO_ = str_between(movimenti[m], "[K]ALTRO_TIPO_IMBALLAGGIO[V]", "[;]"); 
				ID_AUTO = str_between(movimenti[m], "[K]ID_AUTO[V]", "[;]");
				ID_AUTST = str_between(movimenti[m], "[K]ID_AUTST[V]", "[;]");
				ID_VIAGGIO = str_between(movimenti[m], "[K]ID_VIAGGIO[V]", "[;]");
				dt_in_trasp = str_between(movimenti[m], "[K]dt_in_trasp[V]", "[;]");
				hr_in_trasp = str_between(movimenti[m], "[K]hr_in_trasp[V]", "[;]");
				ID_RMK = str_between(movimenti[m], "[K]ID_RMK[V]", "[;]");
				FKEadrRMK = str_between(movimenti[m], "[K]FKEadrRMK[V]", "[;]"); 
				FKEmzRMK = str_between(movimenti[m], "[K]FKEmzRMK[V]", "[;]"); 
				FKEadrAUT = str_between(movimenti[m], "[K]FKEadrAUT[V]", "[;]"); 
				FKEmzAUT = str_between(movimenti[m], "[K]FKEmzAUT[V]", "[;]"); 
				// intermediario
				ID_AZI = str_between(movimenti[m], "[K]ID_AZI[V]", "[;]");
				ID_UIMI = str_between(movimenti[m], "[K]ID_UIMI[V]", "[;]");
				ID_AUTHI = str_between(movimenti[m], "[K]ID_AUTHI[V]", "[;]");
				FKEcfiscI = str_between(movimenti[m], "[K]FKEcfiscI[V]", "[;]");
				FKEscadenzaI = str_between(movimenti[m], "[K]FKEscadenzaI[V]", "[;]");
				FKErilascioI = str_between(movimenti[m], "[K]FKErilascioI[V]", "[;]");
				// workmode
				produttore = str_between(movimenti[m], "[K]produttore[V]", "[;]"); 
				trasportatore = str_between(movimenti[m], "[K]trasportatore[V]", "[;]"); 
				destinatario = str_between(movimenti[m], "[K]destinatario[V]", "[;]"); 
				intermediario = str_between(movimenti[m], "[K]intermediario[V]", "[;]");
				// formulario
				NOTEF = str_between(movimenti[m], "[K]NOTEF[V]", "[;]");
				NOTER = str_between(movimenti[m], "[K]NOTER[V]", "[;]");
				AVV_FORM = str_between(movimenti[m], "[K]AVV_FORM[V]", "[;]");
				AVV_SMALT = str_between(movimenti[m], "[K]AVV_SMALT[V]", "[;]");
				DT_FORM = str_between(movimenti[m], "[K]DT_FORM[V]", "[;]");
				DT_SMALT = str_between(movimenti[m], "[K]DT_SMALT[V]", "[;]");
				VER_DESTINO = str_between(movimenti[m], "[K]VER_DESTINO[V]", "[;]");
				PS_DESTINO = str_between(movimenti[m], "[K]PS_DESTINO[V]", "[;]");
				collettame = str_between(movimenti[m], "[K]collettame[V]", "[;]");
				percorso = str_between(movimenti[m], "[K]percorso[V]", "[;]");
				QL = str_between(movimenti[m], "[K]QL[V]", "[;]");
				StampaAnnua = str_between(movimenti[m], "[K]StampaAnnua[V]", "[;]");
				approved = str_between(movimenti[m], "[K]approved[V]", "[;]");

				// SISTRI
				//SIS_OK = str_between(movimenti[m], "[K]SIS_OK[V]", "[;]");
				//SIS_OK_SCHEDA = str_between(movimenti[m], "[K]SIS_OK_SCHEDA[V]", "[;]");
				//idSIS_regCrono = str_between(movimenti[m], "[K]idSIS_regCrono[V]", "[;]");
			

				oTd1.innerHTML=TIPO;
				oTd2.innerHTML=DATA;
				oTd3.innerHTML=RIF;
				oTd4.innerHTML=quantita + " " + FKEumis;
				

				var oField=document.createElement("INPUT");
				// setta attributi
				oField.setAttribute("type","text");
				oField.setAttribute("name","CSTMquantita_"+m);
				oField.setAttribute("id","CSTMquantita_"+m);
				oField.setAttribute("size","4");
				oField.setAttribute("value",quantita);
				oField.setAttribute("onkeyup", "checkNumber(this);");

				if(produttore==1){
					if(TIPO=="S") oField.setAttribute("disabled",true);
					}

				if(destinatario==1){
					if(TIPO=="C") oField.setAttribute("disabled",true);
					}
				
				var oFieldH=document.createElement("INPUT");
				// setta attributi
				oFieldH.setAttribute("type","hidden");
				oFieldH.setAttribute("name","quantita_"+m);
				oFieldH.setAttribute("id","quantita_"+m);
				oFieldH.setAttribute("value",quantita);

				// appendi al relativo padre
				oTd5.appendChild(oField);
				oTd5.appendChild(oFieldH);
				oTd5.innerHTML+=" ";
				oTd5.innerHTML+=FKEumis;
				oTr.appendChild(oTd1);
				oTr.appendChild(oTd2);
				oTr.appendChild(oTd3);
				oTr.appendChild(oTd4);
				oTr.appendChild(oTd5);
				//oTbody.appendChild(oTr);
		
				//document.getElementById('prospetto_movimenti').getElementsByTagName('TBODY')[0].appendChild(oTr);
				document.getElementById('tablebody').appendChild(oTr);

				var hidden1=document.createElement("INPUT");
				hidden1.setAttribute("value",TIPO);
				hidden1.setAttribute("name","TIPO_"+m);
				hidden1.setAttribute("id","TIPO_"+m);
				hidden1.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden1);

				var hidden2=document.createElement("INPUT");
				hidden2.setAttribute("value",FISCALE);
				hidden2.setAttribute("name","FISCALE_"+m);
				hidden2.setAttribute("id","FISCALE_"+m);
				hidden2.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden2);

				var hidden3=document.createElement("INPUT");
				hidden3.setAttribute("value",FISCALIZZATO);
				hidden3.setAttribute("name","FISCALIZZATO_"+m);
				hidden3.setAttribute("id","FISCALIZZATO_"+m);
				hidden3.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden3);

				var hidden4=document.createElement("INPUT");
				hidden4.setAttribute("value",DTMOV);
				hidden4.setAttribute("name","DTMOV_"+m);
				hidden4.setAttribute("id","DTMOV_"+m);
				hidden4.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden4);

				var hidden5=document.createElement("INPUT");
				hidden5.setAttribute("value",ID_RIF);
				hidden5.setAttribute("name","ID_RIF_"+m);
				hidden5.setAttribute("id","ID_RIF_"+m);
				hidden5.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden5);

				var hidden6=document.createElement("INPUT");
				hidden6.setAttribute("value",FANGHI);
				hidden6.setAttribute("name","FANGHI_"+m);
				hidden6.setAttribute("id","FANGHI_"+m);
				hidden6.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden6);

				var hidden7=document.createElement("INPUT");
				hidden7.setAttribute("value",qta_hidden);
				hidden7.setAttribute("name","qta_hidden_"+m);
				hidden7.setAttribute("id","qta_hidden_"+m);
				hidden7.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden7);

				var hidden8=document.createElement("INPUT");
				hidden8.setAttribute("value",tara);
				hidden8.setAttribute("name","tara_"+m);
				hidden8.setAttribute("id","tara_"+m);
				hidden8.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden8);

				var hidden9=document.createElement("INPUT");
				hidden9.setAttribute("value",pesoL);
				hidden9.setAttribute("name","pesoL_"+m);
				hidden9.setAttribute("id","pesoL_"+m);
				hidden9.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden9);

				var hidden10=document.createElement("INPUT");
				hidden10.setAttribute("value",pesoN);
				hidden10.setAttribute("name","pesoN_"+m);
				hidden10.setAttribute("id","pesoN_"+m);
				hidden10.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden10);

				var hidden11=document.createElement("INPUT");
				hidden11.setAttribute("value",Km);
				hidden11.setAttribute("name","Km_"+m);
				hidden11.setAttribute("id","Km_"+m);
				hidden11.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden11);

				var hidden12=document.createElement("INPUT");
				hidden12.setAttribute("value",FKEdisponibilita);
				hidden12.setAttribute("name","FKEdisponibilita_"+m);
				hidden12.setAttribute("id","FKEdisponibilita_"+m);
				hidden12.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden12);

				var hidden13=document.createElement("INPUT");
				hidden13.setAttribute("value",FKEgiacINI);
				hidden13.setAttribute("name","FKEgiacINI_"+m);
				hidden13.setAttribute("id","FKEgiacINI_"+m);
				hidden13.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden13);

				var hidden14=document.createElement("INPUT");
				hidden14.setAttribute("value",FKElastCarico);
				hidden14.setAttribute("name","FKElastCarico_"+m);
				hidden14.setAttribute("id","FKElastCarico_"+m);
				hidden14.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden14);

				var hidden15=document.createElement("INPUT");
				hidden15.setAttribute("value",FKErifCarico);
				hidden15.setAttribute("name","FKErifCarico_"+m);
				hidden15.setAttribute("id","FKErifCarico_"+m);
				hidden15.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden15);

				var hidden16=document.createElement("INPUT");
				hidden16.setAttribute("value",NPER);
				hidden16.setAttribute("name","NPER_"+m);
				hidden16.setAttribute("id","NPER_"+m);
				hidden16.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden16);

				var hidden17=document.createElement("INPUT");
				hidden17.setAttribute("value",FKEumis);
				hidden17.setAttribute("name","FKEumis_"+m);
				hidden17.setAttribute("id","FKEumis_"+m);
				hidden17.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden17);

				var hidden18=document.createElement("INPUT");
				hidden18.setAttribute("value",FKESF);
				hidden18.setAttribute("name","FKESF_"+m);
				hidden18.setAttribute("id","FKESF_"+m);
				hidden18.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden18);

				var hidden19=document.createElement("INPUT");
				hidden19.setAttribute("value",FKEpesospecifico);
				hidden19.setAttribute("name","FKEpesospecifico_"+m);
				hidden19.setAttribute("id","FKEpesospecifico_"+m);
				hidden19.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden19);

				var hidden20=document.createElement("INPUT");
				hidden20.setAttribute("value",NFORM);
				hidden20.setAttribute("name","NFORM_"+m);
				hidden20.setAttribute("id","NFORM_"+m);
				hidden20.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden20);

				var hidden21=document.createElement("INPUT");
				hidden21.setAttribute("value",DTFORM);
				hidden21.setAttribute("name","DTFORM_"+m);
				hidden21.setAttribute("id","DTFORM_"+m);
				hidden21.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden21);

				var hidden22=document.createElement("INPUT");
				hidden22.setAttribute("value",FANGHI_BOLLA);
				hidden22.setAttribute("name","FANGHI_BOLLA_"+m);
				hidden22.setAttribute("id","FANGHI_BOLLA_"+m);
				hidden22.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden22);

				var hidden23=document.createElement("INPUT");
				hidden23.setAttribute("value",ID_COMM);
				hidden23.setAttribute("name","ID_COMM_"+m);
				hidden23.setAttribute("id","ID_COMM_"+m);
				hidden23.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden23);

				var hidden24=document.createElement("INPUT");
				hidden24.setAttribute("value",ID_CAR);
				hidden24.setAttribute("name","ID_CAR_"+m);
				hidden24.setAttribute("id","ID_CAR_"+m);
				hidden24.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden24);

				var hidden25=document.createElement("INPUT");
				hidden25.setAttribute("value",ID_AZD);
				hidden25.setAttribute("name","ID_AZD_"+m);
				hidden25.setAttribute("id","ID_AZD_"+m);
				hidden25.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden25);

				var hidden26=document.createElement("INPUT");
				hidden26.setAttribute("value",FKEcfiscD);
				hidden26.setAttribute("name","FKEcfiscD_"+m);
				hidden26.setAttribute("id","FKEcfiscD_"+m);
				hidden26.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden26);

				var hidden27=document.createElement("INPUT");
				hidden27.setAttribute("value",ID_UIMD);
				hidden27.setAttribute("name","ID_UIMD_"+m);
				hidden27.setAttribute("id","ID_UIMD_"+m);
				hidden27.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden27);

				var hidden28=document.createElement("INPUT");
				hidden28.setAttribute("value",ID_AUTHD);
				hidden28.setAttribute("name","ID_AUTHD_"+m);
				hidden28.setAttribute("id","ID_AUTHD_"+m);
				hidden28.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden28);

				var hidden29=document.createElement("INPUT");
				hidden29.setAttribute("value",FKErilascioD);
				hidden29.setAttribute("name","FKErilascioD_"+m);
				hidden29.setAttribute("id","FKErilascioD_"+m);
				hidden29.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden29);

				var hidden30=document.createElement("INPUT");
				hidden30.setAttribute("value",FKEscadenzaD);
				hidden30.setAttribute("name","FKEscadenzaD_"+m);
				hidden30.setAttribute("id","FKEscadenzaD_"+m);
				hidden30.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden30);

				var hidden31=document.createElement("INPUT");
				hidden31.setAttribute("value",ID_AZT);
				hidden31.setAttribute("name","ID_AZT_"+m);
				hidden31.setAttribute("id","ID_AZT_"+m);
				hidden31.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden31);

				var hidden32=document.createElement("INPUT");
				hidden32.setAttribute("value",FKEcfiscT);
				hidden32.setAttribute("name","FKEcfiscT_"+m);
				hidden32.setAttribute("id","FKEcfiscT_"+m);
				hidden32.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden32);

				var hidden33=document.createElement("INPUT");
				hidden33.setAttribute("value",ID_UIMT);
				hidden33.setAttribute("name","ID_UIMT_"+m);
				hidden33.setAttribute("id","ID_UIMT_"+m);
				hidden33.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden33);

				var hidden34=document.createElement("INPUT");
				hidden34.setAttribute("value",ID_AUTHT);
				hidden34.setAttribute("name","ID_AUTHT_"+m);
				hidden34.setAttribute("id","ID_AUTHT_"+m);
				hidden34.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden34);

				var hidden35=document.createElement("INPUT");
				hidden35.setAttribute("value",FKErilascioT);
				hidden35.setAttribute("name","FKErilascioT_"+m);
				hidden35.setAttribute("id","FKErilascioT_"+m);
				hidden35.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden35);

				var hidden36=document.createElement("INPUT");
				hidden36.setAttribute("value",FKEscadenzaT);
				hidden36.setAttribute("name","FKEscadenzaT_"+m);
				hidden36.setAttribute("id","FKEscadenzaT_"+m);
				hidden36.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden36);

				var hidden37=document.createElement("INPUT");
				hidden37.setAttribute("value",NumAlboAutotrasp);
				hidden37.setAttribute("name","NumAlboAutotrasp_"+m);
				hidden37.setAttribute("id","NumAlboAutotrasp_"+m);
				hidden37.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden37);

				var hidden38=document.createElement("INPUT");
				hidden38.setAttribute("value",NumAlboAutotraspProprio);
				hidden38.setAttribute("name","NumAlboAutotraspProprio_"+m);
				hidden38.setAttribute("id","NumAlboAutotraspProprio_"+m);
				hidden38.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden38);

				var hidden39=document.createElement("INPUT");
				hidden39.setAttribute("value",produttore);
				hidden39.setAttribute("name","produttore_"+m);
				hidden39.setAttribute("id","produttore_"+m);
				hidden39.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden39);

				var hidden40=document.createElement("INPUT");
				hidden40.setAttribute("value",trasportatore);
				hidden40.setAttribute("name","trasportatore_"+m);
				hidden40.setAttribute("id","trasportatore_"+m);
				hidden40.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden40);

				var hidden41=document.createElement("INPUT");
				hidden41.setAttribute("value",destinatario);
				hidden41.setAttribute("name","destinatario_"+m);
				hidden41.setAttribute("id","destinatario_"+m);
				hidden41.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden41);

				var hidden42=document.createElement("INPUT");
				hidden42.setAttribute("value",intermediario);
				hidden42.setAttribute("name","intermediario_"+m);
				hidden42.setAttribute("id","intermediario_"+m);
				hidden42.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden42);

				var hidden43=document.createElement("INPUT");
				hidden43.setAttribute("value",ID_OP_RS);
				hidden43.setAttribute("name","ID_OP_RS_"+m);
				hidden43.setAttribute("id","ID_OP_RS_"+m);
				hidden43.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden43);

				var hidden44=document.createElement("INPUT");
				hidden44.setAttribute("value",ID_TRAT);
				hidden44.setAttribute("name","ID_TRAT_"+m);
				hidden44.setAttribute("id","ID_TRAT_"+m);
				hidden44.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden44);

				var hidden45=document.createElement("INPUT");
				hidden45.setAttribute("value",adr);
				hidden45.setAttribute("name","adr_"+m);
				hidden45.setAttribute("id","adr_"+m);
				hidden45.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden45);

				var hidden46=document.createElement("INPUT");
				hidden46.setAttribute("value",COLLI);
				hidden46.setAttribute("name","COLLI_"+m);
				hidden46.setAttribute("id","COLLI_"+m);
				hidden46.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden46);

				var hidden47=document.createElement("INPUT");
				hidden47.setAttribute("value",ID_AUTO);
				hidden47.setAttribute("name","ID_AUTO_"+m);
				hidden47.setAttribute("id","ID_AUTO_"+m);
				hidden47.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden47);

				var hidden48=document.createElement("INPUT");
				hidden48.setAttribute("value",ID_AUTST);
				hidden48.setAttribute("name","ID_AUTST_"+m);
				hidden48.setAttribute("id","ID_AUTST_"+m);
				hidden48.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden48);

				var hidden49=document.createElement("INPUT");
				hidden49.setAttribute("value",dt_in_trasp);
				hidden49.setAttribute("name","dt_in_trasp_"+m);
				hidden49.setAttribute("id","dt_in_trasp_"+m);
				hidden49.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden49);

				var hidden50=document.createElement("INPUT");
				hidden50.setAttribute("value",hr_in_trasp);
				hidden50.setAttribute("name","hr_in_trasp_"+m);
				hidden50.setAttribute("id","hr_in_trasp_"+m);
				hidden50.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden50);

				var hidden51=document.createElement("INPUT");
				hidden51.setAttribute("value",ID_RMK);
				hidden51.setAttribute("name","ID_RMK_"+m);
				hidden51.setAttribute("id","ID_RMK_"+m);
				hidden51.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden51);

				var hidden52=document.createElement("INPUT");
				hidden52.setAttribute("value",ID_AZI);
				hidden52.setAttribute("name","ID_AZI_"+m);
				hidden52.setAttribute("id","ID_AZI_"+m);
				hidden52.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden52);

				var hidden53=document.createElement("INPUT");
				hidden53.setAttribute("value",FKEadrRMK);
				hidden53.setAttribute("name","FKEadrRMK_"+m);
				hidden53.setAttribute("id","FKEadrRMK_"+m);
				hidden53.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden53);

				var hidden54=document.createElement("INPUT");
				hidden54.setAttribute("value",FKEmzRMK);
				hidden54.setAttribute("name","FKEmzRMK_"+m);
				hidden54.setAttribute("id","FKEmzRMK_"+m);
				hidden54.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden54);

				var hidden55=document.createElement("INPUT");
				hidden55.setAttribute("value",FKEadrAUT);
				hidden55.setAttribute("name","FKEadrAUT_"+m);
				hidden55.setAttribute("id","FKEadrAUT_"+m);
				hidden55.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden55);

				var hidden56=document.createElement("INPUT");
				hidden56.setAttribute("value",FKEmzAUT);
				hidden56.setAttribute("name","FKEmzAUT_"+m);
				hidden56.setAttribute("id","FKEmzAUT_"+m);
				hidden56.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden56);

				var hidden57=document.createElement("INPUT");
				hidden57.setAttribute("value",NOTEF);
				hidden57.setAttribute("name","NOTEF_"+m);
				hidden57.setAttribute("id","NOTEF_"+m);
				hidden57.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden57);

				var hidden58=document.createElement("INPUT");
				hidden58.setAttribute("value",NOTER);
				hidden58.setAttribute("name","NOTER_"+m);
				hidden58.setAttribute("id","NOTER_"+m);
				hidden58.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden58);

				var hidden59=document.createElement("INPUT");
				hidden59.setAttribute("value",AVV_FORM);
				hidden59.setAttribute("name","AVV_FORM_"+m);
				hidden59.setAttribute("id","AVV_FORM_"+m);
				hidden59.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden59);

				var hidden60=document.createElement("INPUT");
				hidden60.setAttribute("value",AVV_SMALT);
				hidden60.setAttribute("name","AVV_SMALT_"+m);
				hidden60.setAttribute("id","AVV_SMALT_"+m);
				hidden60.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden60);

				var hidden61=document.createElement("INPUT");
				hidden61.setAttribute("value",DT_FORM);
				hidden61.setAttribute("name","DT_FORM_"+m);
				hidden61.setAttribute("id","DT_FORM_"+m);
				hidden61.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden61);

				var hidden62=document.createElement("INPUT");
				hidden62.setAttribute("value",DT_SMALT);
				hidden62.setAttribute("name","DT_SMALT_"+m);
				hidden62.setAttribute("id","DT_SMALT_"+m);
				hidden62.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden62);

				var hidden63=document.createElement("INPUT");
				hidden63.setAttribute("value",VER_DESTINO);
				hidden63.setAttribute("name","VER_DESTINO_"+m);
				hidden63.setAttribute("id","VER_DESTINO_"+m);
				hidden63.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden63);

				var hidden64=document.createElement("INPUT");
				hidden64.setAttribute("value",PS_DESTINO);
				hidden64.setAttribute("name","PS_DESTINO_"+m);
				hidden64.setAttribute("id","PS_DESTINO_"+m);
				hidden64.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden64);

				var hidden65=document.createElement("INPUT");
				hidden65.setAttribute("value",collettame);
				hidden65.setAttribute("name","collettame_"+m);
				hidden65.setAttribute("id","collettame_"+m);
				hidden65.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden65);

				var hidden66=document.createElement("INPUT");
				hidden66.setAttribute("value",percorso);
				hidden66.setAttribute("name","percorso_"+m);
				hidden66.setAttribute("id","percorso_"+m);
				hidden66.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden66);

				var hidden67=document.createElement("INPUT");
				hidden67.setAttribute("value",QL);
				hidden67.setAttribute("name","QL_"+m);
				hidden67.setAttribute("id","QL_"+m);
				hidden67.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden67);

				var hidden68=document.createElement("INPUT");
				hidden68.setAttribute("value",StampaAnnua);
				hidden68.setAttribute("name","StampaAnnua_"+m);
				hidden68.setAttribute("id","StampaAnnua_"+m);
				hidden68.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden68);

				var hidden69=document.createElement("INPUT");
				hidden69.setAttribute("value",approved);
				hidden69.setAttribute("name","approved_"+m);
				hidden69.setAttribute("id","approved_"+m);
				hidden69.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden69);

				var hidden70=document.createElement("INPUT");
				hidden70.setAttribute("value",ID_AZP);
				hidden70.setAttribute("name","ID_AZP_"+m);
				hidden70.setAttribute("id","ID_AZP_"+m);
				hidden70.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden70);

				var hidden71=document.createElement("INPUT");
				hidden71.setAttribute("value",FKEcfiscP);
				hidden71.setAttribute("name","FKEcfiscP_"+m);
				hidden71.setAttribute("id","FKEcfiscP_"+m);
				hidden71.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden71);

				var hidden72=document.createElement("INPUT");
				hidden72.setAttribute("value",ID_UIMP);
				hidden72.setAttribute("name","ID_UIMP_"+m);
				hidden72.setAttribute("id","ID_UIMP_"+m);
				hidden72.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden72);

				var hidden73=document.createElement("INPUT");
				hidden73.setAttribute("value",ID_AUTH);
				hidden73.setAttribute("name","ID_AUTH_"+m);
				hidden73.setAttribute("id","ID_AUTH_"+m);
				hidden73.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden73);

				var hidden74=document.createElement("INPUT");
				hidden74.setAttribute("value",FKErilascioP);
				hidden74.setAttribute("name","FKErilascioP_"+m);
				hidden74.setAttribute("id","FKErilascioP_"+m);
				hidden74.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden74);

				var hidden75=document.createElement("INPUT");
				hidden75.setAttribute("value",FKEscadenzaP);
				hidden75.setAttribute("name","FKEscadenzaP_"+m);
				hidden75.setAttribute("id","FKEscadenzaP_"+m);
				hidden75.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden75);

				var hidden76=document.createElement("INPUT");
				hidden76.setAttribute("value",ID_IMP);
				hidden76.setAttribute("name","ID_IMP_"+m);
				hidden76.setAttribute("id","ID_IMP_"+m);
				hidden76.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden76);

				var hidden77=document.createElement("INPUT");
				hidden77.setAttribute("value",FKErifIND);
				hidden77.setAttribute("name","FKErifIND_"+m);
				hidden77.setAttribute("id","FKErifIND_"+m);
				hidden77.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden77);

				var hidden78=document.createElement("INPUT");
				hidden78.setAttribute("value",ID_UIMI);
				hidden78.setAttribute("name","ID_UIMI_"+m);
				hidden78.setAttribute("id","ID_UIMI_"+m);
				hidden78.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden78);

				var hidden79=document.createElement("INPUT");
				hidden79.setAttribute("value",ID_AUTHI);
				hidden79.setAttribute("name","ID_AUTHI_"+m);
				hidden79.setAttribute("id","ID_AUTHI_"+m);
				hidden79.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden79);

				var hidden80=document.createElement("INPUT");
				hidden80.setAttribute("value",FKEscadenzaI);
				hidden80.setAttribute("name","FKEscadenzaI_"+m);
				hidden80.setAttribute("id","FKEscadenzaI_"+m);
				hidden80.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden80);

				var hidden81=document.createElement("INPUT");
				hidden81.setAttribute("value",FKErilascioI);
				hidden81.setAttribute("name","FKErilascioI_"+m);
				hidden81.setAttribute("id","FKErilascioI_"+m);
				hidden81.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden81);

				var hidden82=document.createElement("INPUT");
				hidden82.setAttribute("value",FKEcfiscI);
				hidden82.setAttribute("name","FKEcfiscI_"+m);
				hidden82.setAttribute("id","FKEcfiscI_"+m);
				hidden82.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden82);

				var hidden83=document.createElement("INPUT");
				hidden83.setAttribute("value",ID_VIAGGIO);
				hidden83.setAttribute("name","ID_VIAGGIO_"+m);
				hidden83.setAttribute("id","ID_VIAGGIO_"+m);
				hidden83.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden83);
/*				
				var hidden84=createNamedElement("INPUT", "idSIS_regCrono_"+m);
				hidden84.setAttribute("value",idSIS_regCrono);
				hidden84.setAttribute("name","idSIS_regCrono_"+m);
				hidden84.setAttribute("id","idSIS_regCrono_"+m);
				hidden84.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden84);

				var hidden85=createNamedElement("INPUT", "SIS_OK_"+m);
				hidden85.setAttribute("value",SIS_OK);
				hidden85.setAttribute("name","SIS_OK_"+m);
				hidden85.setAttribute("id","SIS_OK_"+m);
				hidden85.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden85);

				var hidden86=createNamedElement("INPUT", "SIS_OK_SCHEDA_"+m);
				hidden86.setAttribute("value",SIS_OK_SCHEDA);
				hidden86.setAttribute("name","SIS_OK_SCHEDA_"+m);
				hidden86.setAttribute("id","SIS_OK_SCHEDA_"+m);
				hidden86.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden86);

				var hidden87=createNamedElement("INPUT", "quantita_residua_"+m);
				hidden87.setAttribute("value",quantita_residua);
				hidden87.setAttribute("name","quantita_residua_"+m);
				hidden87.setAttribute("id","quantita_residua_"+m);
				hidden87.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden87);
*/

				var hidden88=createNamedElement("INPUT", "ID_TIPO_IMBALLAGGIO_"+m);
				hidden88.setAttribute("value",ID_TIPO_IMBALLAGGIO);
				hidden88.setAttribute("name","ID_TIPO_IMBALLAGGIO_"+m);
				hidden88.setAttribute("id","ID_TIPO_IMBALLAGGIO_"+m);
				hidden88.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden88);

				var hidden89=createNamedElement("INPUT", "ALTRO_TIPO_IMBALLAGGIO_"+m);
				hidden89.setAttribute("value",ALTRO_TIPO_IMBALLAGGIO);
				hidden89.setAttribute("name","ALTRO_TIPO_IMBALLAGGIO_"+m);
				hidden89.setAttribute("id","ALTRO_TIPO_IMBALLAGGIO_"+m);
				hidden89.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden89);

				var hidden90=createNamedElement("INPUT", "TIPO_S_SCHEDA_"+m);
				hidden90.setAttribute("value",TIPO_S_SCHEDA);
				hidden90.setAttribute("name","TIPO_S_SCHEDA_"+m);
				hidden90.setAttribute("id","TIPO_S_SCHEDA_"+m);
				hidden90.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden90);

				var hidden91=createNamedElement("INPUT", "SenzaTrasporto_"+m);
				hidden91.setAttribute("value",SenzaTrasporto);
				hidden91.setAttribute("name","SenzaTrasporto_"+m);
				hidden91.setAttribute("id","SenzaTrasporto_"+m);
				hidden91.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden91);

				var hidden92=createNamedElement("INPUT", "Transfrontaliero_"+m);
				hidden92.setAttribute("value",Transfrontaliero);
				hidden92.setAttribute("name","Transfrontaliero_"+m);
				hidden92.setAttribute("id","Transfrontaliero_"+m);
				hidden92.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden92);

				}

			var hiddenCounter=document.createElement("INPUT");
			hiddenCounter.setAttribute("value",m);
			hiddenCounter.setAttribute("name","counter");
			hiddenCounter.setAttribute("id","counter");
			hiddenCounter.setAttribute("type","hidden");
			document.getElementById('FRM_fiscal').appendChild(hiddenCounter);

			var hiddenDTMOV_FISC=createNamedElement("INPUT", "DTMOV_FISC");
			hiddenDTMOV_FISC.setAttribute("value",extraData[1]);
			hiddenDTMOV_FISC.setAttribute("id","DTMOV_FISC");
			hiddenDTMOV_FISC.setAttribute("type","hidden");
			document.getElementById('FRM_fiscal').appendChild(hiddenDTMOV_FISC);

			}


		var obj=document.getElementById('FISCAL');
		obj.style.display='block';
		}
	Parameters = "NMOVto=" + NMOVto + "&ID_MOV=" + ID_MOV;
	LKUP.update(Parameters);
	}

JAVASCRIPT;
$javascript.="</script>";
echo $javascript;

$tmp .= "<script type=\"text/javascript\">\n";
$tmp .= "showFiscal('scheduled','scheduled');";
$tmp .= "\n</script>";

echo $tmp;


require("__includes/COMMON_sleepForgEdit.php");
?>