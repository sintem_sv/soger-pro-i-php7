<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
#

$FEDIT->FGE_UseTables("user_contratti_budget","user_schede_rifiuti");

$FEDIT->FGE_SetSelectFields(array("ID_CER","descrizione","approved"),"user_schede_rifiuti");
$FEDIT->FGE_SetSelectFields(array("euro_anno","ID_IMP"),"user_contratti_budget");
$FEDIT->FGE_SetFilter("approved","1","user_schede_rifiuti");
$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_contratti_budget");


$FEDIT->FGE_DescribeFields();
#
$FEDIT->FGE_HideFields(array("approved"),"user_schede_rifiuti");
$FEDIT->FGE_HideFields(array("ID_IMP"),"user_contratti_budget");
#
$FEDIT->FGE_LookUpCFG("ID_CER","user_schede_rifiuti");
$FEDIT->FGE_UseTables("lov_cer");
$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");

$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();

echo($FEDIT->FGE_DataGrid($SOGER->AppDescriptiveLocation,"__scripts/FGE_DataGridEdit.php","ED---",false));

#
require_once("__includes/COMMON_sleepForgEdit.php");
?>
<div class="FGE4Col">
<input type="button" name="nuovo" class="FGEbutton" value="assegna budget a rifiuto" onclick="javascript: document.location='__scripts/newBudgetRifiuto.php'"/>
</div>
