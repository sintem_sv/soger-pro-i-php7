<?php

require_once("__classes/ForgEdit2.class");
require_once("__classes/ForgEdit.RegExp");
require_once("__classes/DbLink.class");
require_once("SOGER_FiscalizzaScheduledJS.php");
require_once("__libs/SQLFunct.php");
require("__includes/COMMON_wakeForgEdit.php");
global $SOGER;


$LOADING ="<div id=\"LOADING\">\n";

if($SOGER->UserData['core_impiantiREG_IND']==1)
	$title = "Verifica carichi automatici e fiscalizzazione";
else
	$title = "Verifica carichi automatici";

$LOADING.="<div id=\"title\">".$title."</div>\n";

$LOADING.="<div id=\"\">\n";
		$LOADING.="<div id=\"txt1\"><b>Controllo esecuzione carichi automatici:</b></div>\n";
		$LOADING.="<div id=\"loading_carichi\" class=\"loadgif\"></div>\n";
$LOADING.="</div>\n";

if($SOGER->UserData['core_impiantiREG_IND']==1){
	$LOADING.="<div id=\"\">\n";
		$LOADING.="<div id=\"txt1\"><b>Controllo esecuzione fiscalizzazione automatica:</b></div>\n";
		$LOADING.="<div id=\"loading_fiscal\" class=\"loadgif\"></div>\n";
	$LOADING.="</div>\n";
	}

$LOADING.="<div id=\"pulsanti\" style=\"padding-top:15px;\">\n";
		$LOADING.="<input disabled style=\"color:gray;border:gray;background-color:#C9C6B8;\" class=\"FiscalButton\" type=\"button\" name=\"chiudi_popup\" id=\"chiudi_popup\" value=\"Chiudi\" onClick=\"javascript:obj=document.getElementById('LOADING');obj.style.display='none';\" />\n";
$LOADING.="</div>\n";

$LOADING.="</div>\n";
	
if($SOGER->UserData['core_impiantiREG_IND']==1){
	
	if($SOGER->UserData['core_usersID_DAY']!=0)	$devoFiscal = '1'; else	$devoFiscal = '0';

	$LOADING.="<form action=\"javascript:completeAHAH.likeSubmit('__includes/SOGER_FiscalizzaWriteScheduled.php', 'POST', 'FRM_fiscal', 'loading_fiscal');\" id=\"FRM_fiscal\" name=\"FRM_fiscal\" method=\"POST\">\n";
		$LOADING.="<table cellpadding=\"0\" cellspacing=\"0\" id=\"prospetto_movimenti\" style=\"display:none;\">\n";
			$LOADING.="<tbody id=\"tablebody\"></tbody>\n";
		$LOADING.="</table>\n";
		$LOADING.="<input type=\"hidden\" name=\"devoFiscal\" id=\"devoFiscal\" value=\"".$devoFiscal."\" />\n";
		$LOADING.="<input type=\"hidden\" name=\"FiscalTo\" id=\"FiscalTo\" value=\"\" />\n";
	$LOADING.="</form>\n";
	}

echo $LOADING;

$javascript ="<script type='text/javascript'>\n";
$javascript.= <<< JAVASCRIPT

// inserisco carichi automatici
completeAHAH.ahah('__includes/SOGER_AutoCarichiWrite.php', 'loading_carichi', '', 'GET', '');

// le ultime righe di SOGER_AutoCarichiWrite.php avviano controllo fiscalizzazione
// fiscalizzo tutti i movimenti fino 'a ieri'


JAVASCRIPT;
$javascript.="</script>";
echo $javascript;

?>