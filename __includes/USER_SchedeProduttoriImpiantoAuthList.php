<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEditDG.php");
#
global $SOGER;
$FEDG->FGE_FlushTableInfo();
$FEDG->FGE_UseTables(array("user_autorizzazioni_pro","user_impianti_produttori","user_aziende_produttori"));
$FEDG->FGE_SetSelectFields(array("description"),"user_impianti_produttori");
$FEDG->FGE_SetSelectFields(array("ID_RIF","ID_AUT","num_aut","rilascio","scadenza","approved"),"user_autorizzazioni_pro");
#
$FEDG->FGE_DescribeFields();
#
$FEDG->FGE_LookUpCFG("ID_RIF","user_autorizzazioni_pro");
$FEDG->FGE_UseTables("lov_cer","user_schede_rifiuti");
$FEDG->FGE_SetSelectFields(array("cod_pro","descrizione","ID_CER","ID_IMP"),"user_schede_rifiuti");
$FEDG->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
$FEDG->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
$FEDG->FGE_HideFields(array("ID_CER","ID_IMP"),"user_schede_rifiuti");
$FEDG->FGE_LookUpDescribe();
$FEDG->FGE_LookUpDone();
#
$FEDG->FGE_LookUpDefault("ID_AUT","user_autorizzazioni_pro");
#
$FEDG->FGE_SetFilter("ID_AZP",$_SESSION["FGE_IDs"]["user_aziende_produttori"],"user_aziende_produttori");
$FEDG->FGE_HideFields(array("approved"),"user_autorizzazioni_pro");
#
echo($FEDG->FGE_DataGrid("Numeri Albo in Archivio","__scripts/FGE_DataGridEdit.php","EDC-T",true,"FEDG",$_SESSION['SearchingFilter']));
require_once("__includes/COMMON_sleepForgEditDG.php");
?>
<div class="FGE4Col">
<input type="button" name="nuovo" class="FGEbutton" value="crea nuova autorizzazione" onclick="javascript: document.location='<?php echo $_SERVER["PHP_SELF"]?>'"/>
</div>
