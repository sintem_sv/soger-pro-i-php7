<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_autorizzazioni_dest");
$FEDIT->FGE_SetFormFields(array("ID_UIMD","ID_RIF","ID_AUT","num_aut","PREV_num_aut","rilascio","scadenza","ID_TRAT","ID_ORIGINE_DATI","ID_OP_RS","ID_OP_RS2","note","inherit_edits"),"user_autorizzazioni_dest");

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {
	#
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_TRAT","user_autorizzazioni_dest");
	$FEDIT->FGE_LookUpDefault("ID_AUT","user_autorizzazioni_dest");
	$FEDIT->FGE_LookUpDefault("ID_ORIGINE_DATI","user_autorizzazioni_dest");
	#
	$FEDIT->FGE_LookUpCFG("ID_RIF","user_autorizzazioni_dest");
	$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("cod_pro","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
	$ProfiloFields = "user_schede_rifiuti";
	include("SOGER_FiltriProfilo.php");
	$FEDIT->FGE_SetOrder("lov_cer:COD_CER");
	$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#
	$FEDIT->FGE_LookUpCFG("ID_UIMD","user_autorizzazioni_dest");
	$FEDIT->FGE_UseTables("user_impianti_destinatari");
	$FEDIT->FGE_SetSelectFields(array("description","ID_AZD"),"user_impianti_destinatari");
	$FEDIT->FGE_SetFilter("ID_AZD",$_SESSION["FGE_IDs"]["user_aziende_destinatari"],"user_impianti_destinatari");
	$FEDIT->FGE_HideFields("ID_AZD","user_impianti_destinatari");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#
	$FEDIT->FGE_LookUpCFG("ID_OP_RS","user_autorizzazioni_dest");
	$FEDIT->FGE_UseTables("lov_operazioni_rs");
	$FEDIT->FGE_SetSelectFields(array("description","longdes"),"lov_operazioni_rs");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#
	$FEDIT->FGE_LookUpCFG("ID_OP_RS2","user_autorizzazioni_dest");
	$FEDIT->FGE_UseTables("lov_operazioni_rs_sec");
	$FEDIT->FGE_SetSelectFields(array("description","longdes"),"lov_operazioni_rs_sec");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#

	$FEDIT->FGE_HideFields(array("inherit_edits","PREV_num_aut"),"user_autorizzazioni_dest");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea autorizzazione");
	
} else {
	if(isset($_GET["Involved"])) {
		$FEDIT->FGE_DisableFields(array("ID_RIF","ID_UIMD"),"user_autorizzazioni_dest");	
	}
	#
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_TRAT","user_autorizzazioni_dest");
	$FEDIT->FGE_LookUpDefault("ID_AUT","user_autorizzazioni_dest");
	$FEDIT->FGE_LookUpDefault("ID_ORIGINE_DATI","user_autorizzazioni_dest");
	#
	$FEDIT->FGE_LookUpCFG("ID_RIF","user_autorizzazioni_dest");
	$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("cod_pro","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
	$ProfiloFields = "user_schede_rifiuti";
	include("SOGER_FiltriProfilo.php");
	$FEDIT->FGE_SetOrder("lov_cer:COD_CER");
	$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#
	$FEDIT->FGE_LookUpCFG("ID_UIMD","user_autorizzazioni_dest");
	$FEDIT->FGE_UseTables("user_impianti_destinatari");
	$FEDIT->FGE_SetSelectFields(array("description","ID_AZD"),"user_impianti_destinatari");
	$FEDIT->FGE_SetFilter("ID_AZD",$_SESSION["FGE_IDs"]["user_aziende_destinatari"],"user_impianti_destinatari");
	$FEDIT->FGE_HideFields("ID_AZD","user_impianti_destinatari");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#
	$FEDIT->FGE_LookUpCFG("ID_OP_RS","user_autorizzazioni_dest");
	$FEDIT->FGE_UseTables("lov_operazioni_rs");
	$FEDIT->FGE_SetSelectFields(array("description","longdes"),"lov_operazioni_rs");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
        #
	$FEDIT->FGE_LookUpCFG("ID_OP_RS2","user_autorizzazioni_dest");
	$FEDIT->FGE_UseTables("lov_operazioni_rs_sec");
	$FEDIT->FGE_SetSelectFields(array("description","longdes"),"lov_operazioni_rs_sec");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
        
	$FEDIT->FGE_HideFields(array("inherit_edits","PREV_num_aut"),"user_autorizzazioni_dest");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva autorizzazione");
}
require_once("__includes/COMMON_sleepForgEdit.php");
?>
