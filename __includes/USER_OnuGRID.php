<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
#
$FEDIT->FGE_UseTables(array("lov_num_onu"));
$FEDIT->FGE_SetSelectFields(array("description","des","eti","imb"),"lov_num_onu");
#
$FEDIT->FGE_DescribeFields();
#
?>
<?php
echo($FEDIT->FGE_DataGrid($SOGER->AppDescriptiveLocation,"__scripts/FGE_DataGridEdit.php","-----",true,"FEDIT",$_SESSION['SearchingFilter']));
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>
