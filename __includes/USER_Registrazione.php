<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("core_users");
$FEDIT->FGE_SetFormFields(array("nome", "cognome", "email", "telefono"),"core_users");
$FEDIT->FGE_SetFormFields(array("usr", "pwd", "nuovapwd", "nuovapwd_confirm"),"core_users");
if($SOGER->UserData['core_impiantiMODULO_SIS']==1)
	$FEDIT->FGE_SetFormFields(array("SIS_identity","SIS_Nome_Delegato","SIS_Cognome_Delegato"),"core_users");
$FEDIT->FGE_DisableFields(array("usr","pwd"),"core_users");	
$FEDIT->FGE_DescribeFields();	

$FEDIT->FGE_SetTitle("nome","Dettagli utente","core_users");
$FEDIT->FGE_SetTitle("usr","Credenziali di accesso","core_users");
if($SOGER->UserData['core_impiantiMODULO_SIS']==1)
	$FEDIT->FGE_SetTitle("SIS_identity","Identity SISTRI","core_users");

#css firefox
$css="<style type=\"text/css\">\n";
$css.="div#btn_dll{\n";
$css.="position: absolute; top: 416px; left: 150px; z-index: 10;";
$css.="}\n";
$css.="</style>\n";

#css ie
$css.="<!--[if IE]>";
$css.="<style type=\"text/css\">\n";
$css.="div#btn_dll{\n";
$css.="position: absolute; top: 453px; left: 150px; z-index: 10;";
$css.="}\n";
$css.="</style>\n";
$css.="<![endif]-->";

echo $css;

$btn ="<div id=\"btn_dll\">\n";
$btn.="<input type=\"button\" class=\"FGEbutton\" name=\"\" value=\"Download DLL per firma\" onclick=\"javascript:location.replace('__SIS/signature/userlib_install.zip');\" />\n";
$btn.="</div>\n";	

if($SOGER->UserData['core_impiantiMODULO_SIS']==1)
	echo $btn;


echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva");
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>
