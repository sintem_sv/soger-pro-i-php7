<?php
global $SOGER;
if(!isset($TableSpec)) {
	$TableSpec = "";
}

if(!isset($_GET["pro"]) && !isset($_GET["dest"]) && !isset($_GET["tra"]) && !isset($_GET["int"])) {
	if($SOGER->UserData["workmode"]=="produttore") {
		$sql .= " AND ${TableSpec}produttore='1'";
	} else {
		$sql .= " AND ${TableSpec}produttore='0'";
	}
	if($SOGER->UserData["workmode"]=="trasportatore") {
		$sql .= " AND ${TableSpec}trasportatore='1'";
	} else {
		$sql .= " AND ${TableSpec}trasportatore='0'";
	}
	if($SOGER->UserData["workmode"]=="destinatario") {
		$sql .= " AND ${TableSpec}destinatario='1'";
	} else {
		$sql .= " AND ${TableSpec}destinatario='0'";
	}	
	if($SOGER->UserData["workmode"]=="intermediario") {
		$sql .= " AND ${TableSpec}intermediario='1'";
	} else {
		$sql .= " AND ${TableSpec}intermediario='0'";
	}	
} else {
	if($_GET["pro"]=="true") {
		$sql .= " AND ${TableSpec}produttore='1'";
	} else {
		$sql .= " AND ${TableSpec}produttore='0'";
	}
	if($_GET["tra"]=="true") {
		$sql .= " AND ${TableSpec}trasportatore='1'";
	} else {
		$sql .= " AND ${TableSpec}trasportatore='0'";
	}
	if($_GET["dest"]=="true") {
		$sql .= " AND ${TableSpec}destinatario='1'";
	} else {
		$sql .= " AND ${TableSpec}destinatario='0'";
	}		
	if($_GET["int"]=="true") {
		$sql .= " AND ${TableSpec}intermediario='1'";
	} else {
		$sql .= " AND ${TableSpec}intermediario='0'";
	}		
}

#
$notFilter=array();
$notFilter[]="UserNuovoProduttore";
$notFilter[]="UserNuovoProduttoreImpianto";
$notFilter[]="UserNuovoProduttoreAutorizzazioni";
$notFilter[]="UserNuovoContrattoProduttore";
$notFilter[]="UserProduttoreContratti";
$notFilter[]="UserProduttoreContrattoCondizioni";
$notFilter[]="UserCondizioneContrattoProduttore";

$notFilter[]="UserNuovoDestinatario";
$notFilter[]="UserNuovoDestinatarioImpianto";
$notFilter[]="UserNuovoDestinatarioAutorizzazioni";
$notFilter[]="UserDestinatarioContratti";
$notFilter[]="UserNuovoContrattoDestinatario";
$notFilter[]="UserDestinatarioContrattoCondizioni";
$notFilter[]="UserCondizioneContrattoDestinatario";

$notFilter[]="UserNuovoIntermediario";
$notFilter[]="UserNuovoIntermediarioImpianto";
$notFilter[]="UserNuovoIntermediarioAutorizzazioni";
$notFilter[]="UserNuovoContrattoIntermediario";
$notFilter[]="UserIntermediarioContratti";
$notFilter[]="UserIntermediarioContrattoCondizioni";
$notFilter[]="UserCondizioneContrattoIntermediario";

$notFilter[]="UserNuovoTrasportatore";
$notFilter[]="UserNuovoTrasportatoreImpianto";
$notFilter[]="UserNuovoTrasportatoreAutorizzazioni";
$notFilter[]="UserNuovoTrasportatoreRimorchi";
$notFilter[]="UserNuovoTrasportatoreAutisti";
$notFilter[]="UserNuovoTrasportatoreAutomezzi";
$notFilter[]="UserTrasportatoreContratti";
$notFilter[]="UserNuovoContrattoTrasportatore";
$notFilter[]="UserTrasportatoreContrattoCondizioni";
$notFilter[]="UserCondizioneContrattoTrasportatore";

# nelle statistiche non filtriamo per approved, si vede tutto...
$notFilter[]="UserStatsCfg";
$notFilter[]="UserStatsCfgAn";
$notFilter[]="UserStatsCfgEco";
$notFilter[]="UserStatsCfgGiac";
$notFilter[]="UserStatsCfgGl";
$notFilter[]="UserStatsCfgSpec";
$notFilter[]="UserStatsCfgCP";

if(!in_array($SOGER->AppLocation,$notFilter))
	$sql .= " AND ${TableSpec}approved='1'";
#
?>
