<?php

require_once("__classes/ForgEdit2.class");
require_once("__classes/ForgEdit.RegExp");
require_once("__classes/DbLink.class");
require_once("__libs/SQLFunct.php");
require("__includes/COMMON_wakeForgEdit.php");
require("__includes/COMMON_ForgEditClassFiles.php");

global $SOGER;



$javascript ="<script type='text/javascript'>\n";
$javascript.= <<< JAVASCRIPT

function in_array (needle, haystack, argStrict) {
    // Checks if the given value exists in the array  
    // 
    // version: 1006.1915
    // discuss at: http://phpjs.org/functions/in_array    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: vlado houba
    // +   input by: Billy
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: in_array('van', ['Kevin', 'van', 'Zonneveld']);    // *     returns 1: true
    // *     example 2: in_array('vlado', {0: 'Kevin', vlado: 'van', 1: 'Zonneveld'});
    // *     returns 2: false
    // *     example 3: in_array(1, ['1', '2', '3']);
    // *     returns 3: true    // *     example 3: in_array(1, ['1', '2', '3'], false);
    // *     returns 3: true
    // *     example 4: in_array(1, ['1', '2', '3'], true);
    // *     returns 4: false
    var key = '', strict = !!argStrict; 
    if (strict) {
        for (key in haystack) {
            if (key === needle) {
                return true;            
				}
        }
    } else {
        for (key in haystack) {
            if (key == needle) {                
				return true;
            }
        }
    }
     return false;
}

function validate(){
	var counter = document.getElementById('counter').value;
	var canSubmit = 0;
	var reason = 0; // 1=num non corretto // 2=qta troppo basse //3=date incoerenti ind-fisc

	// controllo formato dati inseriti
	for(i=0; i<counter; i++){

		fieldValue=document.getElementById('CSTMquantita_'+i).value;

		if(fieldValue=='')
			document.getElementById('CSTMquantita_'+i).value = 0;

		if(fieldValue.charAt(fieldValue.length - 1)=='.'){
			document.getElementById('CSTMquantita_'+i).value = fieldValue.substring(0, (fieldValue.length) - 1);
			}

		}

	if(counter>0){
		var DATA_FISCtmp = document.getElementById('DTMOV_FISC').value.split('-');
		var DATA_FISC	 = DATA_FISCtmp[1]+"/"+DATA_FISCtmp[2]+"/"+DATA_FISCtmp[0];
		var DATA_FISCprint	 = DATA_FISCtmp[2]+"/"+DATA_FISCtmp[1]+"/"+DATA_FISCtmp[0];

		var DATA_INDtmp  = document.getElementById('DTMOV_0').value.split('-');
		var DATA_IND	 = DATA_INDtmp[1]+"/"+DATA_INDtmp[2]+"/"+DATA_INDtmp[0];
		var DATA_INDprint	 = DATA_INDtmp[2]+"/"+DATA_INDtmp[1]+"/"+DATA_INDtmp[0];

		var DF = new Date(DATA_FISC);
		var DI = new Date(DATA_IND);  

		// date nel formato mese/giorno/anno
		var date_diff = (DI.getTime() - DF.getTime())/86400000;

		if(date_diff<0 && DATA_INDprint!='00/00/0000' ){
			reason=3;
			canSubmit++;
			}
		}

	if(canSubmit==0){
		
		var arrayS		= new Array();
		var arrayC		= new Array();
		var giacenze	= new Array();
		var counterCC	= 0;
		for(i=0; i<counter; i++){
			if(document.getElementById('TIPO_'+i).value=='S'){
				arrayS[counterCC]=i;
				counterCC++;
				}
			}

		// array singolo rifiuto
		var arrayRIF = new Array();
		for(i=0; i<counter; i++){
			if(in_array(document.getElementById('ID_RIF_'+i).value, arrayRIF, false)){
				arrayRIF[document.getElementById('ID_RIF_'+i).value]+=i+",";
				}
			else{
				arrayRIF[document.getElementById('ID_RIF_'+i).value]=i+",";
				}
			}

		
		// cerco eventuali scarichi da fiscalizzare
		for(i=0; i<arrayS.length; i++){
			//window.alert("ciclo gli scarichi..."+i+" di "+arrayS.length);
			var scarico = document.getElementById('quantita_'+arrayS[i]).value;
			var carico = 0;
			var gotCarico=false;				/**/
			var arrayTMP = arrayRIF[document.getElementById('ID_RIF_'+arrayS[i]).value].split(",");
			for(k=0; k<(arrayTMP.length)-1; k++){
				if(arrayTMP[k]<arrayS[i]){
					if(document.getElementById('TIPO_'+arrayTMP[k]).value=='C'){
						if(giacenze[document.getElementById('ID_RIF_'+arrayS[i]).value] === undefined) {
							var ID_SCARICO	= document.getElementById('ID_MOV_'+arrayS[i]).value;
							var ID_CARICO	= document.getElementById('ID_MOV_'+arrayTMP[k]).value;
							if(ID_SCARICO<ID_CARICO)
								giacenze[document.getElementById('ID_RIF_'+arrayS[i]).value] = document.getElementById('FKEdisponibilita_'+arrayS[i]).value;
							else
								giacenze[document.getElementById('ID_RIF_'+arrayS[i]).value] = document.getElementById('FKEdisponibilita_'+arrayTMP[k]).value;
							}
						gotCarico=true;
						carico = (carico * 1) + (document.getElementById('CSTMquantita_'+arrayTMP[k]).value * 1);
						var CtoEdit=arrayTMP[k];
						}
					else{
						if(giacenze[document.getElementById('ID_RIF_'+arrayS[i]).value] === undefined)
							giacenze[document.getElementById('ID_RIF_'+arrayS[i]).value] = document.getElementById('FKEdisponibilita_'+arrayS[i]).value;
						carico = (carico * 1) - (document.getElementById('quantita_'+arrayTMP[k]).value * 1);
						}
					}
				}
			if(!gotCarico){ 
				carico=0;
				if(giacenze[document.getElementById('ID_RIF_'+arrayS[i]).value] === undefined)
					giacenze[document.getElementById('ID_RIF_'+arrayS[i]).value]=document.getElementById('FKEdisponibilita_'+arrayS[i]).value;
				}

			// PROBLEMA: la giacenza � calcolata sempre in kg, devo convertirla nell'unit� di misura del rifiuto!
			//alert(document.getElementById('FKEumis_'+arrayS[i]).value);
			switch(document.getElementById('FKEumis_'+arrayS[i]).value){
				case 'Litri':
					ps = (document.getElementById('FKEpesospecifico_'+arrayS[i]).value * 1);
					giacenze[document.getElementById('ID_RIF_'+arrayS[i]).value] = giacenze[document.getElementById('ID_RIF_'+arrayS[i]).value] / ps;
					break
				case 'Mc.':
					ps = (document.getElementById('FKEpesospecifico_'+arrayS[i]).value * 1);
					giacenze[document.getElementById('ID_RIF_'+arrayS[i]).value] = giacenze[document.getElementById('ID_RIF_'+arrayS[i]).value] / ps / 1000;
					break;
				}

			var disponibilita=(giacenze[document.getElementById('ID_RIF_'+arrayS[i]).value] * 1) + (carico * 1);
			disponibilita=disponibilita.toFixed(2);
			var difference=scarico-disponibilita;
			//window.alert("Scarico: "+scarico+" | Disponibilita: "+disponibilita+" ( G: "+giacenze[document.getElementById('ID_RIF_'+arrayS[i]).value]+" + C: "+carico+" )");

			delete giacenze[document.getElementById('ID_RIF_'+arrayS[i]).value];

			if(difference>0){
				reason=2;
				canSubmit++;
				}
			}
		}

	if(canSubmit==0){
		document.FRM_fiscal.submit();
		//adaptTxt();
		}
	else{
		
		// feedback
		var statusDiv = document.getElementById('loading_fiscal');
		statusDiv.innerHTML="<br />Impossibile completare la fiscalizzazione automatica<br />";
		statusDiv.setAttribute('class', '"finished"');

		// enable closure
		document.getElementById('chiudi_popup').style.color='white';
		document.getElementById('chiudi_popup').style.backgroundColor='#034373';
		document.getElementById('chiudi_popup').style.borderColor='yellow';
		document.getElementById('chiudi_popup').disabled=false;


		switch(reason){
			case 0:
				window.alert("Fiscalizzazione interrotta per debug.");
				break;
			case 1:
				window.alert("Attenzione, verifica il formato dei dati immessi.");
				break;
			case 2:
				window.alert("Attenzione, i quantitativi sono stati abbassati oltre il consentito. So.Ge.R. Pro setter� ora il quantitativo minimo che � necessario caricare per il rifiuto. Verificare quanto immesso e procedere nuovamente al salvataggio.");
				break;
			case 3:
				window.alert("Attenzione, sul registro fiscale sono presenti movimenti con data "+DATA_FISCprint+", successiva a quella di uno dei movimenti che si sta cercando di fiscalizzare ("+DATA_INDprint+"). Impossibile completare l'operazione.");
				break;
			}
		}

	}
function str_between(string, prefix, suffix) {
	s = string;
	var i = s.indexOf(prefix);	
	if (i >= 0) {
		s = s.substring(i + prefix.length);
		}
	else {
		return '';
		}
	if (suffix) {
		i = s.indexOf(suffix);
		if (i >= 0) {
			s = s.substring(0, i);
			}
		else {
			return '';
			}
		}
	return s;
	}

function clearTable(tbodyId){
	//document.getElementById(tbodyId).innerHTML='';
	var cell = document.getElementById(tbodyId);
	if ( cell.hasChildNodes() ){
		while ( cell.childNodes.length >= 1 ){
			cell.removeChild( cell.firstChild );       
			} 
		}
	}

function createNamedElement(type, name) {
   var element = null;
   // Try the IE way; this fails on standards-compliant browsers
   try {
      element = document.createElement('<'+type+' name="'+name+'">');
   } catch (e) {
   }
   if (!element || element.nodeName != type.toUpperCase()) {
      // Non-IE browser; use canonical method to create named element
      element = document.createElement(type);
      element.name = name;
   }
   return element;
}


function showFiscal(NMOVto, ID_MOV){
	var LKUP = new ajaxObject('__includes/SOGER_PreparaFiscalizzazione.php',FGE_DummyAjaxCallback);
	LKUP.callback = function (responseTxt, responseStat) {
		//[K]Chiave[V]Valore[;][K]Chiave[V]Valore[;][END]
		movimenti=responseTxt.split("[END]");
		//window.alert(responseTxt);
		var extraData = movimenti[movimenti.length-1].split("|");
				
		if(document.createElement && document.getElementById && document.getElementsByTagName) {
				
				
			//var oTbody=document.createElement("TBODY");
			//oTbody.setAttribute('id', 'tablebody');
			var oTrMain=document.createElement("TR");
			var oTh1=document.createElement("TH");
			var oTh2=document.createElement("TH");
			var oTh3=document.createElement("TH");
			var oTh4=document.createElement("TH");
			var oTh5=document.createElement("TH");
			
			oTh1.setAttribute('width', '"25"');
			oTh2.setAttribute('width', '"75"');
			oTh3.setAttribute('width', '"240"');
			oTh4.setAttribute('width', '"60"');
			oTh5.setAttribute('width', '"80"');

			oTh1.innerHTML="Tipo";
			oTh2.innerHTML="Data";
			oTh3.innerHTML="Rifiuto";
			oTh4.innerHTML="Quantit�";
			oTh5.innerHTML="";

			oTrMain.appendChild(oTh1);
			oTrMain.appendChild(oTh2);
			oTrMain.appendChild(oTh3);
			oTrMain.appendChild(oTh4);
			oTrMain.appendChild(oTh5);

			document.getElementById('tablebody').appendChild(oTrMain);
				
			for(m=0;m<movimenti.length-1;m++){ // tolgo l' ultimo indice, quello che va dall' ultimo [END] in poi..
				// crea elementi

				var oTr=document.createElement("TR");
				
				var oTd1=document.createElement("TD");
				var oTd2=document.createElement("TD");
				var oTd3=document.createElement("TD");
				var oTd4=document.createElement("TD");
				var oTd5=document.createElement("TD");

				oTd1.setAttribute('align', '"center"');
				oTd2.setAttribute('align', '"center"');
				oTd3.setAttribute('style', '"padding-left:3px"');
				oTd4.setAttribute('align', '"right"');
				oTd5.setAttribute('align', '"right"');
				oTd4.setAttribute('width', '"70"');
				oTd5.setAttribute('width', '"70"');

				DATA = str_between(movimenti[m], "[K]DTMOV[V]", "[;]"); 
				if(DATA!='0000-00-00'){
					DATA = DATA.split("-");
					DATA = DATA[2] + "/" + DATA[1] + "/" + DATA[0];
					}
				else
					DATA = 'n.d.';

				TIPO = str_between(movimenti[m], "[K]TIPO[V]", "[;]");
				TIPO_S_SCHEDA = str_between(movimenti[m], "[K]TIPO_S_SCHEDA[V]", "[;]");
				SenzaTrasporto = str_between(movimenti[m], "[K]SenzaTrasporto[V]", "[;]");
				Transfrontaliero = str_between(movimenti[m], "[K]Transfrontaliero[V]", "[;]");
				FISCALE = str_between(movimenti[m], "[K]FISCALE[V]", "[;]");
				FISCALIZZATO = 1;
				ID_MOV = str_between(movimenti[m], "[K]ID_MOV[V]", "[;]");
				DTMOV = str_between(movimenti[m], "[K]DTMOV[V]", "[;]");
				ID_IMP = str_between(movimenti[m], "[K]ID_IMP[V]", "[;]");
				ID_RIF = str_between(movimenti[m], "[K]ID_RIF[V]", "[;]");
				FANGHI = str_between(movimenti[m], "[K]FANGHI[V]", "[;]");
				RIF  = str_between(movimenti[m], "[K]RIF_desc[V]", "[;]"); 
				CAR_DataAnalisi  = str_between(movimenti[m], "[K]CAR_DataAnalisi[V]", "[;]"); 
				CAR_NumDocumento  = str_between(movimenti[m], "[K]CAR_NumDocumento[V]", "[;]"); 
				CAR_Laboratorio  = str_between(movimenti[m], "[K]CAR_Laboratorio[V]", "[;]"); 
				quantita  = str_between(movimenti[m], "[K]quantita[V]", "[;]");
				//quantita_residua  = str_between(movimenti[m], "[K]quantita_residua[V]", "[;]");
				qta_hidden  = str_between(movimenti[m], "[K]qta_hidden[V]", "[;]");
				pesoL  = str_between(movimenti[m], "[K]pesoL[V]", "[;]");
				pesoN  = str_between(movimenti[m], "[K]pesoN[V]", "[;]");
				tara   = str_between(movimenti[m], "[K]tara[V]", "[;]");
				Km   = str_between(movimenti[m], "[K]Km[V]", "[;]");
				FKEdisponibilita   = str_between(movimenti[m], "[K]FKEdisponibilita[V]", "[;]");
				FKEgiacINI   = str_between(movimenti[m], "[K]FKEgiacINI[V]", "[;]");
				FKElastCarico   = str_between(movimenti[m], "[K]FKElastCarico[V]", "[;]");
				FKErifCarico   = str_between(movimenti[m], "[K]FKErifCarico[V]", "[;]");
				FKErifIND = str_between(movimenti[m], "[K]FKErifIND[V]", "[;]");
				NPER   = str_between(movimenti[m], "[K]NPER[V]", "[;]");
				FKEumis = str_between(movimenti[m], "[K]FKEumis[V]", "[;]"); 
				FKESF = str_between(movimenti[m], "[K]FKESF[V]", "[;]"); 
				FKEpesospecifico = str_between(movimenti[m], "[K]FKEpesospecifico[V]", "[;]"); 
				NFORM = str_between(movimenti[m], "[K]NFORM[V]", "[;]"); 
				DTFORM = str_between(movimenti[m], "[K]DTFORM[V]", "[;]"); 
				lotto = str_between(movimenti[m], "[K]lotto[V]", "[;]"); 
				FANGHI_BOLLA = str_between(movimenti[m], "[K]FANGHI_BOLLA[V]", "[;]"); 
				ID_COMM = str_between(movimenti[m], "[K]ID_COMM[V]", "[;]"); 
				ID_CAR = str_between(movimenti[m], "[K]ID_CAR[V]", "[;]"); 
				// produttore
				ID_AZP = str_between(movimenti[m], "[K]ID_AZP[V]", "[;]"); 
				FKEcfiscP = str_between(movimenti[m], "[K]FKEcfiscP[V]", "[;]"); 
				ID_UIMP = str_between(movimenti[m], "[K]ID_UIMP[V]", "[;]"); 
				ID_AUTH = str_between(movimenti[m], "[K]ID_AUTH[V]", "[;]"); 
				FKErilascioP = str_between(movimenti[m], "[K]FKErilascioP[V]", "[;]"); 
				FKEscadenzaP = str_between(movimenti[m], "[K]FKEscadenzaP[V]", "[;]"); 
				// destinatario
				ID_AZD = str_between(movimenti[m], "[K]ID_AZD[V]", "[;]"); 
				FKEcfiscD = str_between(movimenti[m], "[K]FKEcfiscD[V]", "[;]"); 
				ID_UIMD = str_between(movimenti[m], "[K]ID_UIMD[V]", "[;]"); 
				ID_AUTHD = str_between(movimenti[m], "[K]ID_AUTHD[V]", "[;]"); 
				FKErilascioD = str_between(movimenti[m], "[K]FKErilascioD[V]", "[;]"); 
				FKEscadenzaD = str_between(movimenti[m], "[K]FKEscadenzaD[V]", "[;]"); 
				ID_OP_RS = str_between(movimenti[m], "[K]ID_OP_RS[V]", "[;]"); 
				ID_TRAT = str_between(movimenti[m], "[K]ID_TRAT[V]", "[;]"); 
				// trasportatore
				ID_AZT = str_between(movimenti[m], "[K]ID_AZT[V]", "[;]"); 
				FKEcfiscT = str_between(movimenti[m], "[K]FKEcfiscT[V]", "[;]"); 
				ID_UIMT = str_between(movimenti[m], "[K]ID_UIMT[V]", "[;]"); 
				ID_AUTHT = str_between(movimenti[m], "[K]ID_AUTHT[V]", "[;]"); 
				FKErilascioT = str_between(movimenti[m], "[K]FKErilascioT[V]", "[;]"); 
				FKEscadenzaT = str_between(movimenti[m], "[K]FKEscadenzaT[V]", "[;]"); 
				NumAlboAutotrasp = str_between(movimenti[m], "[K]NumAlboAutotrasp[V]", "[;]"); 
				NumAlboAutotraspProprio = str_between(movimenti[m], "[K]NumAlboAutotraspProprio[V]", "[;]");
				adr = str_between(movimenti[m], "[K]adr[V]", "[;]");
				ID_ONU = str_between(movimenti[m], "[K]ID_ONU[V]", "[;]"); 
				COLLI = str_between(movimenti[m], "[K]COLLI[V]", "[;]"); 
				ID_TIPO_IMBALLAGGIO = str_between(movimenti[m], "[K]ID_TIPO_IMBALLAGGIO[V]", "[;]"); 
				ALTRO_TIPO_IMBALLAGGIO = str_between(movimenti[m], "[K]ALTRO_TIPO_IMBALLAGGIO[V]", "[;]"); 
				ID_AUTO = str_between(movimenti[m], "[K]ID_AUTO[V]", "[;]");
				ID_AUTST = str_between(movimenti[m], "[K]ID_AUTST[V]", "[;]");
				ID_VIAGGIO = str_between(movimenti[m], "[K]ID_VIAGGIO[V]", "[;]");
				dt_in_trasp = str_between(movimenti[m], "[K]dt_in_trasp[V]", "[;]");
				hr_in_trasp = str_between(movimenti[m], "[K]hr_in_trasp[V]", "[;]");
				//dt_in_trasp_prev = str_between(movimenti[m], "[K]dt_in_trasp_prev[V]", "[;]");
				//hr_in_trasp_prev = str_between(movimenti[m], "[K]hr_in_trasp_prev[V]", "[;]");
				//dt_fn_trasp = str_between(movimenti[m], "[K]dt_fn_trasp[V]", "[;]");
				//hr_fn_trasp = str_between(movimenti[m], "[K]hr_fn_trasp[V]", "[;]");
				//dt_fn_trasp_prev = str_between(movimenti[m], "[K]dt_fn_trasp_prev[V]", "[;]");
				//hr_fn_trasp_prev = str_between(movimenti[m], "[K]hr_fn_trasp_prev[V]", "[;]");
				ID_RMK = str_between(movimenti[m], "[K]ID_RMK[V]", "[;]");
				FKEadrRMK = str_between(movimenti[m], "[K]FKEadrRMK[V]", "[;]"); 
				FKEmzRMK = str_between(movimenti[m], "[K]FKEmzRMK[V]", "[;]"); 
				FKEadrAUT = str_between(movimenti[m], "[K]FKEadrAUT[V]", "[;]"); 
				FKEmzAUT = str_between(movimenti[m], "[K]FKEmzAUT[V]", "[;]"); 
				// intermediario
				ID_AZI = str_between(movimenti[m], "[K]ID_AZI[V]", "[;]");
				ID_UIMI = str_between(movimenti[m], "[K]ID_UIMI[V]", "[;]");
				ID_AUTHI = str_between(movimenti[m], "[K]ID_AUTHI[V]", "[;]");
				FKEcfiscI = str_between(movimenti[m], "[K]FKEcfiscI[V]", "[;]");
				FKEscadenzaI = str_between(movimenti[m], "[K]FKEscadenzaI[V]", "[;]");
				FKErilascioI = str_between(movimenti[m], "[K]FKErilascioI[V]", "[;]");
				// workmode
				produttore = str_between(movimenti[m], "[K]produttore[V]", "[;]"); 
				trasportatore = str_between(movimenti[m], "[K]trasportatore[V]", "[;]"); 
				destinatario = str_between(movimenti[m], "[K]destinatario[V]", "[;]"); 
				intermediario = str_between(movimenti[m], "[K]intermediario[V]", "[;]");
				// formulario
				NOTEF = str_between(movimenti[m], "[K]NOTEF[V]", "[;]");
				NOTER = str_between(movimenti[m], "[K]NOTER[V]", "[;]");
				AVV_FORM = str_between(movimenti[m], "[K]AVV_FORM[V]", "[;]");
				AVV_SMALT = str_between(movimenti[m], "[K]AVV_SMALT[V]", "[;]");
				DT_FORM = str_between(movimenti[m], "[K]DT_FORM[V]", "[;]");
				DT_SMALT = str_between(movimenti[m], "[K]DT_SMALT[V]", "[;]");
				VER_DESTINO = str_between(movimenti[m], "[K]VER_DESTINO[V]", "[;]");
				PS_DESTINO = str_between(movimenti[m], "[K]PS_DESTINO[V]", "[;]");
				collettame = str_between(movimenti[m], "[K]collettame[V]", "[;]");
				percorso = str_between(movimenti[m], "[K]percorso[V]", "[;]");
				QL = str_between(movimenti[m], "[K]QL[V]", "[;]");
				StampaAnnua = str_between(movimenti[m], "[K]StampaAnnua[V]", "[;]");
				USER_nome = str_between(movimenti[m], "[K]USER_nome[V]", "[;]");
				USER_cognome = str_between(movimenti[m], "[K]USER_cognome[V]", "[;]");
				USER_telefono = str_between(movimenti[m], "[K]USER_telefono[V]", "[;]");
				USER_email = str_between(movimenti[m], "[K]USER_email[V]", "[;]");
				//SIS_identity = str_between(movimenti[m], "[K]SIS_identity[V]", "[;]");
				//SIS_Nome_Delegato = str_between(movimenti[m], "[K]SIS_Nome_Delegato[V]", "[;]");
				//SIS_Cognome_Delegato = str_between(movimenti[m], "[K]SIS_Cognome_Delegato[V]", "[;]");
				approved = str_between(movimenti[m], "[K]approved[V]", "[;]");
			
				// SISTRI
				//SIS_OK = str_between(movimenti[m], "[K]SIS_OK[V]", "[;]");
				//SIS_OK_SCHEDA = str_between(movimenti[m], "[K]SIS_OK_SCHEDA[V]", "[;]");
				//idSIS_regCrono = str_between(movimenti[m], "[K]idSIS_regCrono[V]", "[;]");
				
				// ALLEGATO VII
				N_ANNEX_VII = str_between(movimenti[m], "[K]N_ANNEX_VII[V]", "[;]"); 
				ID_ORG = str_between(movimenti[m], "[K]ID_ORG[V]", "[;]"); 
				REFERENTE_ORG = str_between(movimenti[m], "[K]REFERENTE_ORG[V]", "[;]"); 
				REFERENTE_DEST = str_between(movimenti[m], "[K]REFERENTE_DEST[V]", "[;]"); 
				REFERENTE_TRASP = str_between(movimenti[m], "[K]REFERENTE_TRASP[V]", "[;]");
				REFERENTE_GEN = str_between(movimenti[m], "[K]REFERENTE_GEN[V]", "[;]");
				ID_BASILEA = str_between(movimenti[m], "[K]ID_BASILEA[V]", "[;]"); 
				ID_OCSE = str_between(movimenti[m], "[K]ID_OCSE[V]", "[;]"); 
				ID_ALL_IIIA = str_between(movimenti[m], "[K]ID_ALL_IIIA[V]", "[;]");
				ID_ALL_IIIB = str_between(movimenti[m], "[K]ID_ALL_IIIB[V]", "[;]");


				oTd1.innerHTML=TIPO;
				oTd2.innerHTML=DATA;
				oTd3.innerHTML=RIF;
				oTd4.innerHTML=quantita + " " + FKEumis;
				

				var oField=createNamedElement("INPUT", "CSTMquantita_"+m);
				// setta attributi
				oField.setAttribute("type","text");
				oField.setAttribute("id","CSTMquantita_"+m);
				oField.setAttribute("size","4");
				oField.setAttribute("value",quantita);
				oField.setAttribute("onkeyup", "checkNumber(this);");
				
				if(produttore==1){
					if(TIPO=="S") oField.setAttribute("disabled",true);
					}

				if(destinatario==1){
					if(TIPO=="C") oField.setAttribute("disabled",true);
					}
				
				//var oFieldH=document.createElement("INPUT");
				var oFieldH=createNamedElement("INPUT", "quantita_"+m);
				// setta attributi
				oFieldH.setAttribute("type","hidden");
				//oFieldH.setAttribute("name","quantita_"+m);
				oFieldH.setAttribute("id","quantita_"+m);
				oFieldH.setAttribute("value",quantita);

				// appendi al relativo padre
				oTd5.appendChild(oField);
				oTd5.appendChild(oFieldH);
				oTd5.innerHTML+=" ";
				oTd5.innerHTML+=FKEumis;
				oTr.appendChild(oTd1);
				oTr.appendChild(oTd2);
				oTr.appendChild(oTd3);
				oTr.appendChild(oTd4);
				oTr.appendChild(oTd5);
				//oTbody.appendChild(oTr);
		
				//document.getElementById('prospetto_movimenti').getElementsByTagName('TBODY')[0].appendChild(oTr);
				document.getElementById('tablebody').appendChild(oTr);

				//var hidden1=document.createElement("INPUT");
				var hidden1=createNamedElement("INPUT", "TIPO_"+m);
				hidden1.setAttribute("value",TIPO);
				//hidden1.setAttribute("name","TIPO_"+m);
				hidden1.setAttribute("id","TIPO_"+m);
				hidden1.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden1);

				//var hidden2=document.createElement("INPUT");
				var hidden2=createNamedElement("INPUT", "FISCALE_"+m);
				hidden2.setAttribute("value",FISCALE);
				//hidden2.setAttribute("name","FISCALE_"+m);
				hidden2.setAttribute("id","FISCALE_"+m);
				hidden2.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden2);

				//var hidden3=document.createElement("INPUT");
				var hidden3=createNamedElement("INPUT", "FISCALIZZATO_"+m);
				hidden3.setAttribute("value",FISCALIZZATO);
				//hidden3.setAttribute("name","FISCALIZZATO_"+m);
				hidden3.setAttribute("id","FISCALIZZATO_"+m);
				hidden3.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden3);

				//var hidden4=document.createElement("INPUT");
				var hidden4=createNamedElement("INPUT", "DTMOV_"+m);
				hidden4.setAttribute("value",DTMOV);
				//hidden4.setAttribute("name","DTMOV_"+m);
				hidden4.setAttribute("id","DTMOV_"+m);
				hidden4.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden4);

				//var hidden5=document.createElement("INPUT");
				var hidden5=createNamedElement("INPUT", "ID_RIF_"+m);
				hidden5.setAttribute("value",ID_RIF);
				//hidden5.setAttribute("name","ID_RIF_"+m);
				hidden5.setAttribute("id","ID_RIF_"+m);
				hidden5.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden5);

				//var hidden6=document.createElement("INPUT");
				var hidden6=createNamedElement("INPUT", "FANGHI_"+m);
				hidden6.setAttribute("value",FANGHI);
				//hidden6.setAttribute("name","FANGHI_"+m);
				hidden6.setAttribute("id","FANGHI_"+m);
				hidden6.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden6);

				//var hidden7=document.createElement("INPUT");
				var hidden7=createNamedElement("INPUT", "qta_hidden_"+m);
				hidden7.setAttribute("value",qta_hidden);
				//hidden7.setAttribute("name","qta_hidden_"+m);
				hidden7.setAttribute("id","qta_hidden_"+m);
				hidden7.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden7);

				//var hidden8=document.createElement("INPUT");
				var hidden8=createNamedElement("INPUT", "tara_"+m);
				hidden8.setAttribute("value",tara);
				//hidden8.setAttribute("name","tara_"+m);
				hidden8.setAttribute("id","tara_"+m);
				hidden8.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden8);

				//var hidden9=document.createElement("INPUT");
				var hidden9=createNamedElement("INPUT", "pesoL_"+m);
				hidden9.setAttribute("value",pesoL);
				//hidden9.setAttribute("name","pesoL_"+m);
				hidden9.setAttribute("id","pesoL_"+m);
				hidden9.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden9);

				//var hidden10=document.createElement("INPUT");
				var hidden10=createNamedElement("INPUT", "pesoN_"+m);
				hidden10.setAttribute("value",pesoN);
				//hidden10.setAttribute("name","pesoN_"+m);
				hidden10.setAttribute("id","pesoN_"+m);
				hidden10.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden10);

				//var hidden11=document.createElement("INPUT");
				var hidden11=createNamedElement("INPUT", "Km_"+m);
				hidden11.setAttribute("value",Km);
				//hidden11.setAttribute("name","Km_"+m);
				hidden11.setAttribute("id","Km_"+m);
				hidden11.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden11);

				//var hidden12=document.createElement("INPUT");
				var hidden12=createNamedElement("INPUT", "FKEdisponibilita_"+m);
				hidden12.setAttribute("value",FKEdisponibilita);
				//hidden12.setAttribute("name","FKEdisponibilita_"+m);
				hidden12.setAttribute("id","FKEdisponibilita_"+m);
				hidden12.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden12);

				//var hidden13=document.createElement("INPUT");
				var hidden13=createNamedElement("INPUT", "FKEgiacINI_"+m);
				hidden13.setAttribute("value",FKEgiacINI);
				//hidden13.setAttribute("name","FKEgiacINI_"+m);
				hidden13.setAttribute("id","FKEgiacINI_"+m);
				hidden13.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden13);

				//var hidden14=document.createElement("INPUT");
				var hidden14=createNamedElement("INPUT", "FKElastCarico_"+m);
				hidden14.setAttribute("value",FKElastCarico);
				//hidden14.setAttribute("name","FKElastCarico_"+m);
				hidden14.setAttribute("id","FKElastCarico_"+m);
				hidden14.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden14);

				//var hidden15=document.createElement("INPUT");
				var hidden15=createNamedElement("INPUT", "FKErifCarico_"+m);
				hidden15.setAttribute("value",FKErifCarico);
				//hidden15.setAttribute("name","FKErifCarico_"+m);
				hidden15.setAttribute("id","FKErifCarico_"+m);
				hidden15.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden15);

				//var hidden16=document.createElement("INPUT");
				var hidden16=createNamedElement("INPUT", "NPER_"+m);
				hidden16.setAttribute("value",NPER);
				//hidden16.setAttribute("name","NPER_"+m);
				hidden16.setAttribute("id","NPER_"+m);
				hidden16.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden16);

				//var hidden17=document.createElement("INPUT");
				var hidden17=createNamedElement("INPUT", "FKEumis_"+m);
				hidden17.setAttribute("value",FKEumis);
				//hidden17.setAttribute("name","FKEumis_"+m);
				hidden17.setAttribute("id","FKEumis_"+m);
				hidden17.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden17);

				//var hidden18=document.createElement("INPUT");
				var hidden18=createNamedElement("INPUT", "FKESF_"+m);
				hidden18.setAttribute("value",FKESF);
				//hidden18.setAttribute("name","FKESF_"+m);
				hidden18.setAttribute("id","FKESF_"+m);
				hidden18.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden18);

				//var hidden19=document.createElement("INPUT");
				var hidden19=createNamedElement("INPUT", "FKEpesospecifico_"+m);
				hidden19.setAttribute("value",FKEpesospecifico);
				//hidden19.setAttribute("name","FKEpesospecifico_"+m);
				hidden19.setAttribute("id","FKEpesospecifico_"+m);
				hidden19.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden19);

				//var hidden20=document.createElement("INPUT");
				var hidden20=createNamedElement("INPUT", "NFORM_"+m);
				hidden20.setAttribute("value",NFORM);
				//hidden20.setAttribute("name","NFORM_"+m);
				hidden20.setAttribute("id","NFORM_"+m);
				hidden20.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden20);

				//var hidden21=document.createElement("INPUT");
				var hidden21=createNamedElement("INPUT", "DTFORM_"+m);
				hidden21.setAttribute("value",DTFORM);
				//hidden21.setAttribute("name","DTFORM_"+m);
				hidden21.setAttribute("id","DTFORM_"+m);
				hidden21.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden21);

				//var hidden22=document.createElement("INPUT");
				var hidden22=createNamedElement("INPUT", "FANGHI_BOLLA_"+m);
				hidden22.setAttribute("value",FANGHI_BOLLA);
				//hidden22.setAttribute("name","FANGHI_BOLLA_"+m);
				hidden22.setAttribute("id","FANGHI_BOLLA_"+m);
				hidden22.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden22);

				//var hidden23=document.createElement("INPUT");
				var hidden23=createNamedElement("INPUT", "ID_COMM_"+m);
				hidden23.setAttribute("value",ID_COMM);
				//hidden23.setAttribute("name","ID_COMM_"+m);
				hidden23.setAttribute("id","ID_COMM_"+m);
				hidden23.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden23);

				//var hidden24=document.createElement("INPUT");
				var hidden24=createNamedElement("INPUT", "ID_CAR_"+m);
				hidden24.setAttribute("value",ID_CAR);
				//hidden24.setAttribute("name","ID_CAR_"+m);
				hidden24.setAttribute("id","ID_CAR_"+m);
				hidden24.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden24);

				//var hidden25=document.createElement("INPUT");
				var hidden25=createNamedElement("INPUT", "ID_AZD_"+m);
				hidden25.setAttribute("value",ID_AZD);
				//hidden25.setAttribute("name","ID_AZD_"+m);
				hidden25.setAttribute("id","ID_AZD_"+m);
				hidden25.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden25);

				//var hidden26=document.createElement("INPUT");
				var hidden26=createNamedElement("INPUT", "FKEcfiscD_"+m);
				hidden26.setAttribute("value",FKEcfiscD);
				//hidden26.setAttribute("name","FKEcfiscD_"+m);
				hidden26.setAttribute("id","FKEcfiscD_"+m);
				hidden26.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden26);

				//var hidden27=document.createElement("INPUT");
				var hidden27=createNamedElement("INPUT", "ID_UIMD_"+m);
				hidden27.setAttribute("value",ID_UIMD);
				//hidden27.setAttribute("name","ID_UIMD_"+m);
				hidden27.setAttribute("id","ID_UIMD_"+m);
				hidden27.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden27);

				//var hidden28=document.createElement("INPUT");
				var hidden28=createNamedElement("INPUT", "ID_AUTHD_"+m);
				hidden28.setAttribute("value",ID_AUTHD);
				//hidden28.setAttribute("name","ID_AUTHD_"+m);
				hidden28.setAttribute("id","ID_AUTHD_"+m);
				hidden28.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden28);

				//var hidden29=document.createElement("INPUT");
				var hidden29=createNamedElement("INPUT", "FKErilascioD_"+m);
				hidden29.setAttribute("value",FKErilascioD);
				//hidden29.setAttribute("name","FKErilascioD_"+m);
				hidden29.setAttribute("id","FKErilascioD_"+m);
				hidden29.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden29);

				//var hidden30=document.createElement("INPUT");
				var hidden30=createNamedElement("INPUT", "FKEscadenzaD_"+m);
				hidden30.setAttribute("value",FKEscadenzaD);
				//hidden30.setAttribute("name","FKEscadenzaD_"+m);
				hidden30.setAttribute("id","FKEscadenzaD_"+m);
				hidden30.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden30);

				//var hidden31=document.createElement("INPUT");
				var hidden31=createNamedElement("INPUT", "ID_AZT_"+m);
				hidden31.setAttribute("value",ID_AZT);
				//hidden31.setAttribute("name","ID_AZT_"+m);
				hidden31.setAttribute("id","ID_AZT_"+m);
				hidden31.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden31);

				//var hidden32=document.createElement("INPUT");
				var hidden32=createNamedElement("INPUT", "FKEcfiscT_"+m);
				hidden32.setAttribute("value",FKEcfiscT);
				//hidden32.setAttribute("name","FKEcfiscT_"+m);
				hidden32.setAttribute("id","FKEcfiscT_"+m);
				hidden32.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden32);

				//var hidden33=document.createElement("INPUT");
				var hidden33=createNamedElement("INPUT", "ID_UIMT_"+m);
				hidden33.setAttribute("value",ID_UIMT);
				//hidden33.setAttribute("name","ID_UIMT_"+m);
				hidden33.setAttribute("id","ID_UIMT_"+m);
				hidden33.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden33);

				//var hidden34=document.createElement("INPUT");
				var hidden34=createNamedElement("INPUT", "ID_AUTHT_"+m);
				hidden34.setAttribute("value",ID_AUTHT);
				//hidden34.setAttribute("name","ID_AUTHT_"+m);
				hidden34.setAttribute("id","ID_AUTHT_"+m);
				hidden34.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden34);

				//var hidden35=document.createElement("INPUT");
				var hidden35=createNamedElement("INPUT", "FKErilascioT_"+m);
				hidden35.setAttribute("value",FKErilascioT);
				//hidden35.setAttribute("name","FKErilascioT_"+m);
				hidden35.setAttribute("id","FKErilascioT_"+m);
				hidden35.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden35);

				//var hidden36=document.createElement("INPUT");
				var hidden36=createNamedElement("INPUT", "FKEscadenzaT_"+m);
				hidden36.setAttribute("value",FKEscadenzaT);
				//hidden36.setAttribute("name","FKEscadenzaT_"+m);
				hidden36.setAttribute("id","FKEscadenzaT_"+m);
				hidden36.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden36);

				//var hidden37=document.createElement("INPUT");
				var hidden37=createNamedElement("INPUT", "NumAlboAutotrasp_"+m);
				hidden37.setAttribute("value",NumAlboAutotrasp);
				//hidden37.setAttribute("name","NumAlboAutotrasp_"+m);
				hidden37.setAttribute("id","NumAlboAutotrasp_"+m);
				hidden37.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden37);

				//var hidden38=document.createElement("INPUT");.
				var hidden38=createNamedElement("INPUT", "NumAlboAutotraspProprio_"+m);
				hidden38.setAttribute("value",NumAlboAutotraspProprio);
				//hidden38.setAttribute("name","NumAlboAutotraspProprio_"+m);
				hidden38.setAttribute("id","NumAlboAutotraspProprio_"+m);
				hidden38.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden38);

				//var hidden39=document.createElement("INPUT");
				var hidden39=createNamedElement("INPUT", "produttore_"+m);
				hidden39.setAttribute("value",produttore);
				//hidden39.setAttribute("name","produttore_"+m);
				hidden39.setAttribute("id","produttore_"+m);
				hidden39.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden39);

				//var hidden40=document.createElement("INPUT");
				var hidden40=createNamedElement("INPUT", "trasportatore_"+m);
				hidden40.setAttribute("value",trasportatore);
				//hidden40.setAttribute("name","trasportatore_"+m);
				hidden40.setAttribute("id","trasportatore_"+m);
				hidden40.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden40);

				//var hidden41=document.createElement("INPUT");
				var hidden41=createNamedElement("INPUT", "destinatario_"+m);
				hidden41.setAttribute("value",destinatario);
				//hidden41.setAttribute("name","destinatario_"+m);
				hidden41.setAttribute("id","destinatario_"+m);
				hidden41.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden41);

				//var hidden42=document.createElement("INPUT");
				var hidden42=createNamedElement("INPUT", "intermediario_"+m);
				hidden42.setAttribute("value",intermediario);
				//hidden42.setAttribute("name","intermediario_"+m);
				hidden42.setAttribute("id","intermediario_"+m);
				hidden42.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden42);

				//var hidden43=document.createElement("INPUT");
				var hidden43=createNamedElement("INPUT", "ID_OP_RS_"+m);
				hidden43.setAttribute("value",ID_OP_RS);
				//hidden43.setAttribute("name","ID_OP_RS_"+m);
				hidden43.setAttribute("id","ID_OP_RS_"+m);
				hidden43.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden43);

				//var hidden44=document.createElement("INPUT");
				var hidden44=createNamedElement("INPUT", "ID_TRAT_"+m);
				hidden44.setAttribute("value",ID_TRAT);
				//hidden44.setAttribute("name","ID_TRAT_"+m);
				hidden44.setAttribute("id","ID_TRAT_"+m);
				hidden44.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden44);

				//var hidden45=document.createElement("INPUT");
				var hidden45=createNamedElement("INPUT", "adr_"+m);
				hidden45.setAttribute("value",adr);
				//hidden45.setAttribute("name","adr_"+m);
				hidden45.setAttribute("id","adr_"+m);
				hidden45.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden45);

				//var hidden46=document.createElement("INPUT");
				var hidden46=createNamedElement("INPUT", "COLLI_"+m);
				hidden46.setAttribute("value",COLLI);
				//hidden46.setAttribute("name","COLLI_"+m);
				hidden46.setAttribute("id","COLLI_"+m);
				hidden46.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden46);

				//var hidden47=document.createElement("INPUT");
				var hidden47=createNamedElement("INPUT", "ID_AUTO_"+m);
				hidden47.setAttribute("value",ID_AUTO);
				//hidden47.setAttribute("name","ID_AUTO_"+m);
				hidden47.setAttribute("id","ID_AUTO_"+m);
				hidden47.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden47);

				//var hidden48=document.createElement("INPUT");
				var hidden48=createNamedElement("INPUT", "ID_AUTST_"+m);
				hidden48.setAttribute("value",ID_AUTST);
				//hidden48.setAttribute("name","ID_AUTST_"+m);
				hidden48.setAttribute("id","ID_AUTST_"+m);
				hidden48.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden48);

				//var hidden49=document.createElement("INPUT");
				var hidden49=createNamedElement("INPUT", "dt_in_trasp_"+m);
				hidden49.setAttribute("value",dt_in_trasp);
				//hidden49.setAttribute("name","dt_in_trasp_"+m);
				hidden49.setAttribute("id","dt_in_trasp_"+m);
				hidden49.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden49);

				//var hidden50=document.createElement("INPUT");
				var hidden50=createNamedElement("INPUT", "hr_in_trasp_"+m);
				hidden50.setAttribute("value",hr_in_trasp);
				//hidden50.setAttribute("name","hr_in_trasp_"+m);
				hidden50.setAttribute("id","hr_in_trasp_"+m);
				hidden50.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden50);

				//var hidden51=document.createElement("INPUT");
				var hidden51=createNamedElement("INPUT", "ID_RMK_"+m);
				hidden51.setAttribute("value",ID_RMK);
				//hidden51.setAttribute("name","ID_RMK_"+m);
				hidden51.setAttribute("id","ID_RMK_"+m);
				hidden51.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden51);

				//var hidden52=document.createElement("INPUT");
				var hidden52=createNamedElement("INPUT", "ID_AZI_"+m);
				hidden52.setAttribute("value",ID_AZI);
				//hidden52.setAttribute("name","ID_AZI_"+m);
				hidden52.setAttribute("id","ID_AZI_"+m);
				hidden52.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden52);

				//var hidden53=document.createElement("INPUT");
				var hidden53=createNamedElement("INPUT", "FKEadrRMK_"+m);
				hidden53.setAttribute("value",FKEadrRMK);
				//hidden53.setAttribute("name","FKEadrRMK_"+m);
				hidden53.setAttribute("id","FKEadrRMK_"+m);
				hidden53.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden53);

				//var hidden54=document.createElement("INPUT");
				var hidden54=createNamedElement("INPUT", "FKEmzRMK_"+m);
				hidden54.setAttribute("value",FKEmzRMK);
				//hidden54.setAttribute("name","FKEmzRMK_"+m);
				hidden54.setAttribute("id","FKEmzRMK_"+m);
				hidden54.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden54);

				//var hidden55=document.createElement("INPUT");
				var hidden55=createNamedElement("INPUT", "FKEadrAUT_"+m);
				hidden55.setAttribute("value",FKEadrAUT);
				//hidden55.setAttribute("name","FKEadrAUT_"+m);
				hidden55.setAttribute("id","FKEadrAUT_"+m);
				hidden55.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden55);

				//var hidden56=document.createElement("INPUT");
				var hidden56=createNamedElement("INPUT", "FKEmzAUT_"+m);
				hidden56.setAttribute("value",FKEmzAUT);
				//hidden56.setAttribute("name","FKEmzAUT_"+m);
				hidden56.setAttribute("id","FKEmzAUT_"+m);
				hidden56.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden56);

				//var hidden57=document.createElement("INPUT");
				var hidden57=createNamedElement("INPUT", "NOTEF_"+m);
				hidden57.setAttribute("value",NOTEF);
				//hidden57.setAttribute("name","NOTEF_"+m);
				hidden57.setAttribute("id","NOTEF_"+m);
				hidden57.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden57);

				//var hidden58=document.createElement("INPUT");
				var hidden58=createNamedElement("INPUT", "NOTER_"+m);
				hidden58.setAttribute("value",NOTER);
				//hidden58.setAttribute("name","NOTER_"+m);
				hidden58.setAttribute("id","NOTER_"+m);
				hidden58.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden58);

				//var hidden59=document.createElement("INPUT");
				var hidden59=createNamedElement("INPUT", "AVV_FORM_"+m);
				hidden59.setAttribute("value",AVV_FORM);
				//hidden59.setAttribute("name","AVV_FORM_"+m);
				hidden59.setAttribute("id","AVV_FORM_"+m);
				hidden59.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden59);

				//var hidden60=document.createElement("INPUT");
				var hidden60=createNamedElement("INPUT", "AVV_SMALT_"+m);
				hidden60.setAttribute("value",AVV_SMALT);
				//hidden60.setAttribute("name","AVV_SMALT_"+m);
				hidden60.setAttribute("id","AVV_SMALT_"+m);
				hidden60.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden60);

				//var hidden61=document.createElement("INPUT");
				var hidden61=createNamedElement("INPUT", "DT_FORM_"+m);
				hidden61.setAttribute("value",DT_FORM);
				//hidden61.setAttribute("name","DT_FORM_"+m);
				hidden61.setAttribute("id","DT_FORM_"+m);
				hidden61.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden61);

				//var hidden62=document.createElement("INPUT");
				var hidden62=createNamedElement("INPUT", "DT_SMALT_"+m);
				hidden62.setAttribute("value",DT_SMALT);
				//hidden62.setAttribute("name","DT_SMALT_"+m);
				hidden62.setAttribute("id","DT_SMALT_"+m);
				hidden62.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden62);

				//var hidden63=document.createElement("INPUT");
				var hidden63=createNamedElement("INPUT", "VER_DESTINO_"+m);
				hidden63.setAttribute("value",VER_DESTINO);
				//hidden63.setAttribute("name","VER_DESTINO_"+m);
				hidden63.setAttribute("id","VER_DESTINO_"+m);
				hidden63.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden63);

				//var hidden64=document.createElement("INPUT");
				var hidden64=createNamedElement("INPUT", "PS_DESTINO_"+m);
				hidden64.setAttribute("value",PS_DESTINO);
				//hidden64.setAttribute("name","PS_DESTINO_"+m);
				hidden64.setAttribute("id","PS_DESTINO_"+m);
				hidden64.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden64);

				//var hidden65=document.createElement("INPUT");
				var hidden65=createNamedElement("INPUT", "collettame_"+m);
				hidden65.setAttribute("value",collettame);
				//hidden65.setAttribute("name","collettame_"+m);
				hidden65.setAttribute("id","collettame_"+m);
				hidden65.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden65);

				//var hidden66=document.createElement("INPUT");
				var hidden66=createNamedElement("INPUT", "percorso_"+m);
				hidden66.setAttribute("value",percorso);
				//hidden66.setAttribute("name","percorso_"+m);
				hidden66.setAttribute("id","percorso_"+m);
				hidden66.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden66);

				//var hidden67=document.createElement("INPUT");
				var hidden67=createNamedElement("INPUT", "QL_"+m);
				hidden67.setAttribute("value",QL);
				//hidden67.setAttribute("name","QL_"+m);
				hidden67.setAttribute("id","QL_"+m);
				hidden67.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden67);

				//var hidden68=document.createElement("INPUT");
				var hidden68=createNamedElement("INPUT", "StampaAnnua_"+m);
				hidden68.setAttribute("value",StampaAnnua);
				//hidden68.setAttribute("name","StampaAnnua_"+m);
				hidden68.setAttribute("id","StampaAnnua_"+m);
				hidden68.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden68);

				//var hidden69=document.createElement("INPUT");
				var hidden69=createNamedElement("INPUT", "approved_"+m);
				hidden69.setAttribute("value",approved);
				//hidden69.setAttribute("name","approved_"+m);
				hidden69.setAttribute("id","approved_"+m);
				hidden69.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden69);

				//var hidden70=document.createElement("INPUT");
				var hidden70=createNamedElement("INPUT", "ID_AZP_"+m);
				hidden70.setAttribute("value",ID_AZP);
				//hidden70.setAttribute("name","ID_AZP_"+m);
				hidden70.setAttribute("id","ID_AZP_"+m);
				hidden70.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden70);

				//var hidden71=document.createElement("INPUT");
				var hidden71=createNamedElement("INPUT", "FKEcfiscP_"+m);
				hidden71.setAttribute("value",FKEcfiscP);
				//hidden71.setAttribute("name","FKEcfiscP_"+m);
				hidden71.setAttribute("id","FKEcfiscP_"+m);
				hidden71.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden71);

				//var hidden72=document.createElement("INPUT");
				var hidden72=createNamedElement("INPUT", "ID_UIMP_"+m);
				hidden72.setAttribute("value",ID_UIMP);
				//hidden72.setAttribute("name","ID_UIMP_"+m);
				hidden72.setAttribute("id","ID_UIMP_"+m);
				hidden72.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden72);

				//var hidden73=document.createElement("INPUT");
				var hidden73=createNamedElement("INPUT", "ID_AUTH_"+m);
				hidden73.setAttribute("value",ID_AUTH);
				//hidden73.setAttribute("name","ID_AUTH_"+m);
				hidden73.setAttribute("id","ID_AUTH_"+m);
				hidden73.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden73);

				//var hidden74=document.createElement("INPUT");
				var hidden74=createNamedElement("INPUT", "FKErilascioP_"+m);
				hidden74.setAttribute("value",FKErilascioP);
				//hidden74.setAttribute("name","FKErilascioP_"+m);
				hidden74.setAttribute("id","FKErilascioP_"+m);
				hidden74.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden74);

				//var hidden75=document.createElement("INPUT");
				var hidden75=createNamedElement("INPUT", "FKEscadenzaP_"+m);
				hidden75.setAttribute("value",FKEscadenzaP);
				//hidden75.setAttribute("name","FKEscadenzaP_"+m);
				hidden75.setAttribute("id","FKEscadenzaP_"+m);
				hidden75.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden75);

				//var hidden76=document.createElement("INPUT");
				var hidden76=createNamedElement("INPUT", "ID_IMP_"+m);
				hidden76.setAttribute("value",ID_IMP);
				//hidden76.setAttribute("name","ID_IMP_"+m);
				hidden76.setAttribute("id","ID_IMP_"+m);
				hidden76.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden76);

				//var hidden77=document.createElement("INPUT");
				var hidden77=createNamedElement("INPUT", "FKErifIND_"+m);
				hidden77.setAttribute("value",FKErifIND);
				//hidden77.setAttribute("name","FKErifIND_"+m);
				hidden77.setAttribute("id","FKErifIND_"+m);
				hidden77.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden77);

				//var hidden78=document.createElement("INPUT");
				var hidden78=createNamedElement("INPUT", "ID_ONU_"+m);
				hidden78.setAttribute("value",ID_ONU);
				//hidden78.setAttribute("name","ID_ONU_"+m);
				hidden78.setAttribute("id","ID_ONU_"+m);
				hidden78.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden78);
/*
				//var hidden79=document.createElement("INPUT");
				var hidden79=createNamedElement("INPUT", "dt_in_trasp_prev_"+m);
				hidden79.setAttribute("value",dt_in_trasp_prev);
				//hidden79.setAttribute("name","dt_in_trasp_prev_"+m);
				hidden79.setAttribute("id","dt_in_trasp_prev_"+m);
				hidden79.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden79);

				//var hidden80=document.createElement("INPUT");
				var hidden80=createNamedElement("INPUT", "dt_fn_trasp_"+m);
				hidden80.setAttribute("value",dt_fn_trasp);
				//hidden80.setAttribute("name","dt_fn_trasp_"+m);
				hidden80.setAttribute("id","dt_fn_trasp_"+m);
				hidden80.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden80);

				//var hidden81=document.createElement("INPUT");
				var hidden81=createNamedElement("INPUT", "dt_fn_trasp_prev_"+m);
				hidden81.setAttribute("value",dt_fn_trasp_prev);
				//hidden81.setAttribute("name","dt_fn_trasp_prev_"+m);
				hidden81.setAttribute("id","dt_fn_trasp_prev_"+m);
				hidden81.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden81);

				//var hidden82=document.createElement("INPUT");
				var hidden82=createNamedElement("INPUT", "hr_in_trasp_prev_"+m);
				hidden82.setAttribute("value",hr_in_trasp_prev);
				//hidden82.setAttribute("name","hr_in_trasp_prev_"+m);
				hidden82.setAttribute("id","hr_in_trasp_prev_"+m);
				hidden82.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden82);

				//var hidden83=document.createElement("INPUT");
				var hidden83=createNamedElement("INPUT", "hr_fn_trasp_"+m);
				hidden83.setAttribute("value",hr_fn_trasp);
				//hidden83.setAttribute("name","hr_fn_trasp_"+m);
				hidden83.setAttribute("id","hr_fn_trasp_"+m);
				hidden83.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden83);

				//var hidden84=document.createElement("INPUT");
				var hidden84=createNamedElement("INPUT", "hr_fn_trasp_prev_"+m);
				hidden84.setAttribute("value",hr_fn_trasp_prev);
				//hidden84.setAttribute("name","hr_fn_trasp_prev_"+m);
				hidden84.setAttribute("id","hr_fn_trasp_prev_"+m);
				hidden84.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden84);
*/
				//var hidden85=document.createElement("INPUT");
				var hidden85=createNamedElement("INPUT", "USER_nome_"+m);
				hidden85.setAttribute("value",USER_nome);
				//hidden85.setAttribute("name","USER_nome_"+m);
				hidden85.setAttribute("id","USER_nome_"+m);
				hidden85.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden85);

				//var hidden86=document.createElement("INPUT");
				var hidden86=createNamedElement("INPUT", "USER_cognome_"+m);
				hidden86.setAttribute("value",USER_cognome);
				//hidden86.setAttribute("name","USER_cognome_"+m);
				hidden86.setAttribute("id","USER_cognome_"+m);
				hidden86.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden86);

				//var hidden87=document.createElement("INPUT");
				var hidden87=createNamedElement("INPUT", "CAR_DataAnalisi_"+m);
				hidden87.setAttribute("value",CAR_DataAnalisi);
				//hidden87.setAttribute("name","CAR_DataAnalisi_"+m);
				hidden87.setAttribute("id","CAR_DataAnalisi_"+m);
				hidden87.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden87);

				//var hidden88=document.createElement("INPUT");
				var hidden88=createNamedElement("INPUT", "CAR_NumDocumento_"+m);
				hidden88.setAttribute("value",CAR_NumDocumento);
				//hidden88.setAttribute("name","CAR_NumDocumento_"+m);
				hidden88.setAttribute("id","CAR_NumDocumento_"+m);
				hidden88.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden88);

				//var hidden89=document.createElement("INPUT");
				var hidden89=createNamedElement("INPUT", "CAR_Laboratorio_"+m);
				hidden89.setAttribute("value",CAR_Laboratorio);
				//hidden89.setAttribute("name","CAR_Laboratorio_"+m);
				hidden89.setAttribute("id","CAR_Laboratorio_"+m);
				hidden89.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden89);

				//var hidden90=document.createElement("INPUT");
				var hidden90=createNamedElement("INPUT", "ID_UIMI_"+m);
				hidden90.setAttribute("value",ID_UIMI);
				//hidden90.setAttribute("name","ID_UIMI_"+m);
				hidden90.setAttribute("id","ID_UIMI_"+m);
				hidden90.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden90);

				//var hidden91=document.createElement("INPUT");
				var hidden91=createNamedElement("INPUT", "ID_AUTHI_"+m);
				hidden91.setAttribute("value",ID_AUTHI);
				//hidden91.setAttribute("name","ID_AUTHI_"+m);
				hidden91.setAttribute("id","ID_AUTHI_"+m);
				hidden91.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden91);

				//var hidden92=document.createElement("INPUT");
				var hidden92=createNamedElement("INPUT", "FKErilascioI_"+m);
				hidden92.setAttribute("value",FKErilascioI);
				//hidden92.setAttribute("name","FKErilascioI_"+m);
				hidden92.setAttribute("id","FKErilascioI_"+m);
				hidden92.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden92);

				//var hidden93=document.createElement("INPUT");
				var hidden93=createNamedElement("INPUT", "FKEscadenzaI_"+m);
				hidden93.setAttribute("value",FKEscadenzaI);
				//hidden93.setAttribute("name","FKEscadenzaI_"+m);
				hidden93.setAttribute("id","FKEscadenzaI_"+m);
				hidden93.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden93);

				//var hidden94=document.createElement("INPUT");
				var hidden94=createNamedElement("INPUT", "FKEcfiscI_"+m);
				hidden94.setAttribute("value",FKEcfiscI);
				//hidden94.setAttribute("name","FKEcfiscI_"+m);
				hidden94.setAttribute("id","FKEcfiscI_"+m);
				hidden94.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden94);

				//var hidden95=document.createElement("INPUT");
				var hidden95=createNamedElement("INPUT", "ID_VIAGGIO_"+m);
				hidden95.setAttribute("value",ID_VIAGGIO);
				//hidden95.setAttribute("name","ID_VIAGGIO_"+m);
				hidden95.setAttribute("id","ID_VIAGGIO"+m);
				hidden95.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden95);

/*
				//var hidden96=document.createElement("INPUT");
				var hidden96=createNamedElement("INPUT", "SIS_identity_"+m);
				hidden96.setAttribute("value",SIS_identity);
				//hidden96.setAttribute("name","SIS_identity"+m);
				hidden96.setAttribute("id","SIS_identity_"+m);
				hidden96.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden96);

				//var hidden97=document.createElement("INPUT");
				var hidden97=createNamedElement("INPUT", "SIS_Nome_Delegato_"+m);
				hidden97.setAttribute("value",SIS_Nome_Delegato);
				//hidden97.setAttribute("name","SIS_Nome_Delegato_"+m);
				hidden97.setAttribute("id","SIS_Nome_Delegato_"+m);
				hidden97.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden97);

				//var hidden98=document.createElement("INPUT");
				var hidden98=createNamedElement("INPUT", "SIS_Cognome_Delegato_"+m);
				hidden98.setAttribute("value",SIS_Cognome_Delegato);
				//hidden98.setAttribute("name","SIS_Cognome_Delegato_"+m);
				hidden98.setAttribute("id","SIS_Cognome_Delegato_"+m);
				hidden98.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden98);
				
				
				//var hidden99=document.createElement("INPUT");
				var hidden99=createNamedElement("INPUT", "idSIS_regCrono_"+m);
				hidden99.setAttribute("value",idSIS_regCrono);
				//hidden99.setAttribute("name","idSIS_regCrono_"+m);
				hidden99.setAttribute("id","idSIS_regCrono_"+m);
				hidden99.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden99);

				//var hidden100=document.createElement("INPUT");
				var hidden100=createNamedElement("INPUT", "SIS_OK_"+m);
				hidden100.setAttribute("value",SIS_OK);
				//hidden100.setAttribute("name","SIS_OK_"+m);
				hidden100.setAttribute("id","SIS_OK_"+m);
				hidden100.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden100);

				//var hidden101=document.createElement("INPUT");
				var hidden101=createNamedElement("INPUT", "SIS_OK_SCHEDA_"+m);
				hidden101.setAttribute("value",SIS_OK_SCHEDA);
				//hidden101.setAttribute("name","SIS_OK_SCHEDA_"+m);
				hidden101.setAttribute("id","SIS_OK_SCHEDA_"+m);
				hidden101.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden101);

				var hidden102=createNamedElement("INPUT", "quantita_residua_"+m);
				hidden102.setAttribute("value",quantita_residua);
				hidden102.setAttribute("name","quantita_residua_"+m);
				hidden102.setAttribute("id","quantita_residua_"+m);
				hidden102.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden102);
*/

				var hidden103=createNamedElement("INPUT", "ID_TIPO_IMBALLAGGIO_"+m);
				hidden103.setAttribute("value",ID_TIPO_IMBALLAGGIO);
				hidden103.setAttribute("name","ID_TIPO_IMBALLAGGIO_"+m);
				hidden103.setAttribute("id","ID_TIPO_IMBALLAGGIO_"+m);
				hidden103.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden103);

				var hidden104=createNamedElement("INPUT", "ALTRO_TIPO_IMBALLAGGIO_"+m);
				hidden104.setAttribute("value",ALTRO_TIPO_IMBALLAGGIO);
				hidden104.setAttribute("name","ALTRO_TIPO_IMBALLAGGIO_"+m);
				hidden104.setAttribute("id","ALTRO_TIPO_IMBALLAGGIO_"+m);
				hidden104.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden104);

				var hidden105=createNamedElement("INPUT", "USER_telefono_"+m);
				hidden105.setAttribute("value",USER_telefono);
				hidden105.setAttribute("name","USER_telefono_"+m);
				hidden105.setAttribute("id","USER_telefono_"+m);
				hidden105.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden105);

				var hidden106=createNamedElement("INPUT", "USER_email_"+m);
				hidden106.setAttribute("value",USER_email);
				hidden106.setAttribute("name","USER_email_"+m);
				hidden106.setAttribute("id","USER_email_"+m);
				hidden106.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden106);

				var hidden107=createNamedElement("INPUT", "ID_MOV_"+m);
				hidden107.setAttribute("value",ID_MOV);
				hidden107.setAttribute("name","ID_MOV_"+m);
				hidden107.setAttribute("id","ID_MOV_"+m);
				hidden107.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden107);

				var hidden108=createNamedElement("INPUT", "N_ANNEX_VII_"+m);
				hidden108.setAttribute("value",N_ANNEX_VII);
				hidden108.setAttribute("name","N_ANNEX_VII_"+m);
				hidden108.setAttribute("id","N_ANNEX_VII_"+m);
				hidden108.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden108);

				var hidden109=createNamedElement("INPUT", "lotto_"+m);
				hidden109.setAttribute("value",lotto);
				hidden109.setAttribute("name","lotto_"+m);
				hidden109.setAttribute("id","lotto_"+m);
				hidden109.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden109);

				// ALLEGATO VII

				var hidden110=createNamedElement("INPUT", "ID_ORG_"+m);
				hidden110.setAttribute("value",ID_ORG);
				hidden110.setAttribute("name","ID_ORG_"+m);
				hidden110.setAttribute("id","ID_ORG_"+m);
				hidden110.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden110);

				var hidden111=createNamedElement("INPUT", "REFERENTE_ORG_"+m);
				hidden111.setAttribute("value",REFERENTE_ORG);
				hidden111.setAttribute("name","REFERENTE_ORG_"+m);
				hidden111.setAttribute("id","REFERENTE_ORG_"+m);
				hidden111.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden111);

				var hidden112=createNamedElement("INPUT", "REFERENTE_DEST_"+m);
				hidden112.setAttribute("value",REFERENTE_DEST);
				hidden112.setAttribute("name","REFERENTE_DEST_"+m);
				hidden112.setAttribute("id","REFERENTE_DEST_"+m);
				hidden112.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden112);

				var hidden113=createNamedElement("INPUT", "REFERENTE_TRASP_"+m);
				hidden113.setAttribute("value",REFERENTE_TRASP);
				hidden113.setAttribute("name","REFERENTE_TRASP_"+m);
				hidden113.setAttribute("id","REFERENTE_TRASP_"+m);
				hidden113.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden113);

				var hidden114=createNamedElement("INPUT", "REFERENTE_GEN_"+m);
				hidden114.setAttribute("value",REFERENTE_GEN);
				hidden114.setAttribute("name","REFERENTE_GEN_"+m);
				hidden114.setAttribute("id","REFERENTE_GEN_"+m);
				hidden114.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden114);

				var hidden115=createNamedElement("INPUT", "ID_BASILEA_"+m);
				hidden115.setAttribute("value",ID_BASILEA);
				hidden115.setAttribute("name","ID_BASILEA_"+m);
				hidden115.setAttribute("id","ID_BASILEA_"+m);
				hidden115.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden115);

				var hidden116=createNamedElement("INPUT", "ID_OCSE_"+m);
				hidden116.setAttribute("value",ID_OCSE);
				hidden116.setAttribute("name","ID_OCSE_"+m);
				hidden116.setAttribute("id","ID_OCSE_"+m);
				hidden116.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden116);

				var hidden117=createNamedElement("INPUT", "ID_ALL_IIIA_"+m);
				hidden117.setAttribute("value",ID_ALL_IIIA);
				hidden117.setAttribute("name","ID_ALL_IIIA_"+m);
				hidden117.setAttribute("id","ID_ALL_IIIA_"+m);
				hidden117.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden117);

				var hidden118=createNamedElement("INPUT", "ID_ALL_IIIB_"+m);
				hidden118.setAttribute("value",ID_ALL_IIIB);
				hidden118.setAttribute("name","ID_ALL_IIIB_"+m);
				hidden118.setAttribute("id","ID_ALL_IIIB_"+m);
				hidden118.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden118);

				var hidden119=createNamedElement("INPUT", "TIPO_S_SCHEDA_"+m);
				hidden119.setAttribute("value",TIPO_S_SCHEDA);
				hidden119.setAttribute("name","TIPO_S_SCHEDA_"+m);
				hidden119.setAttribute("id","TIPO_S_SCHEDA_"+m);
				hidden119.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden119);

				var hidden120=createNamedElement("INPUT", "SenzaTrasporto_"+m);
				hidden120.setAttribute("value",SenzaTrasporto);
				hidden120.setAttribute("name","SenzaTrasporto_"+m);
				hidden120.setAttribute("id","SenzaTrasporto_"+m);
				hidden120.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden120);

				var hidden121=createNamedElement("INPUT", "Transfrontaliero_"+m);
				hidden121.setAttribute("value",Transfrontaliero);
				hidden121.setAttribute("name","Transfrontaliero_"+m);
				hidden121.setAttribute("id","Transfrontaliero_"+m);
				hidden121.setAttribute("type","hidden");
				document.getElementById('FRM_fiscal').appendChild(hidden121);


				}

			//var hiddenCounter=document.createElement("INPUT");
			var hiddenCounter=createNamedElement("INPUT", "counter");
			hiddenCounter.setAttribute("value",m);
			//hiddenCounter.setAttribute("name","counter");
			hiddenCounter.setAttribute("id","counter");
			hiddenCounter.setAttribute("type","hidden");
			document.getElementById('FRM_fiscal').appendChild(hiddenCounter);

			var hiddenDTMOV_FISC=createNamedElement("INPUT", "DTMOV_FISC");
			hiddenDTMOV_FISC.setAttribute("value",extraData[1]);
			hiddenDTMOV_FISC.setAttribute("id","DTMOV_FISC");
			hiddenDTMOV_FISC.setAttribute("type","hidden");
			document.getElementById('FRM_fiscal').appendChild(hiddenDTMOV_FISC);

			// ho finito di generare il mio form, procedo con la validazione
			validate();

			}


		//var obj=document.getElementById('FISCAL');
		//obj.style.display='block';
		}
	Parameters = "NMOVto=" + NMOVto + "&ID_MOV=" + ID_MOV;
	LKUP.update(Parameters);
	}

JAVASCRIPT;
$javascript.="</script>";
echo $javascript;


require("__includes/COMMON_sleepForgEdit.php");
?>