<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
#
$FEDIT->FGE_UseTables("user_schede_rifiuti");
//$FEDIT->FGE_SetSelectFields(array("ID_CER","cod_pro","descrizione","ID_SF","ID_FONTE_RIF","pericoloso","trasportatore","destinatario","produttore","idSIS_regCrono","approved","PREV_NextS"),"user_schede_rifiuti");
$FEDIT->FGE_SetSelectFields(array("ID_CER","cod_pro","descrizione","ID_SF","ID_FONTE_RIF","pericoloso","trasportatore","destinatario","produttore","approved","PREV_NextS"),"user_schede_rifiuti");
$FEDIT->FGE_DescribeFields();
$FEDIT->FGE_SetOrder("user_schede_rifiuti:ID_CER");

$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
$ProfiloFields = "user_schede_rifiuti";
include("SOGER_FiltriProfilo.php");
#
//$FEDIT->FGE_HideFields(array("trasportatore","destinatario","produttore","idSIS_regCrono","approved","ID_FONTE_RIF"),"user_schede_rifiuti");
$FEDIT->FGE_HideFields(array("trasportatore","destinatario","produttore","approved","ID_FONTE_RIF"),"user_schede_rifiuti");
$FEDIT->FGE_LookUpDefault("ID_SF","user_schede_rifiuti");
#
$FEDIT->FGE_LookUpCFG("ID_CER","user_schede_rifiuti");
$FEDIT->FGE_UseTables("lov_cer");
$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();
#

echo($FEDIT->FGE_DataGrid($SOGER->AppDescriptiveLocation,"__scripts/FGE_DataGridEdit.php","-----",false));


#
require_once("__includes/COMMON_sleepForgEdit.php");
?>

<div class="FGE4Col">
	
	<input type="button" name="updateNextS" class="FGEbutton" value="aggiorna previsione prossimi conferimenti" onclick="javascript: document.location='__includes/SOGER_CheckNextS.php'"/>

	<input type="button" name="disableAutoCarichi" class="FGEbutton" value="disabilita tutti i carichi automatici" onclick="javascript: document.location='__includes/SOGER_DisableAutoCarichi.php'" style="margin-left:10px;" />

	<input type="button" name="enableAutoCarichi" class="FGEbutton" value="riabilita tutti i carichi automatici" onclick="javascript: document.location='__includes/SOGER_EnableAutoCarichi.php'" style="margin-left:10px;" />

</div>
