<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_aree_stoccaggio_dest");
$FEDIT->FGE_SetFormFields(array("ID_UIMD","description","max_stock"),"user_aree_stoccaggio_dest");

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {
	#
	$FEDIT->FGE_DescribeFields();
	#
	$FEDIT->FGE_LookUpCFG("ID_UIMD","user_aree_stoccaggio_dest");
	$FEDIT->FGE_UseTables("user_impianti_destinatari");
	$FEDIT->FGE_SetSelectFields(array("description","ID_AZD"),"user_impianti_destinatari");
	$FEDIT->FGE_SetFilter("ID_AZD",$_SESSION["FGE_IDs"]["user_aziende_destinatari"],"user_impianti_destinatari");
	$FEDIT->FGE_HideFields("ID_AZD","user_impianti_destinatari");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea area stoccaggio");
	
} else {

	#
	$FEDIT->FGE_DescribeFields();
	#
	$FEDIT->FGE_LookUpCFG("ID_UIMD","user_aree_stoccaggio_dest");
	$FEDIT->FGE_UseTables("user_impianti_destinatari");
	$FEDIT->FGE_SetSelectFields(array("description","ID_AZD"),"user_impianti_destinatari");
	$FEDIT->FGE_SetFilter("ID_AZD",$_SESSION["FGE_IDs"]["user_aziende_destinatari"],"user_impianti_destinatari");
	$FEDIT->FGE_HideFields("ID_AZD","user_impianti_destinatari");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva area stoccaggio");
}
require_once("__includes/COMMON_sleepForgEdit.php");
?>
