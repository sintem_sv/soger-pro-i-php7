<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;

include("USER_MovFlushSessionVars.php");
require("__includes/USER_FormularioFiscaleSiNo.inc");
#
$FEDIT->FGE_FlushTableInfo();
#
$FEDIT->FGE_UseTables("user_movimenti_fiscalizzati","user_schede_rifiuti","lov_cer","lov_stato_fisico");
$FEDIT->FGE_SetSelectFields(array("idSIS","idSIS_scheda","FISCALIZZATO","ID_BAT","SIS_OK","SIS_OK_SCHEDA","statoRegistrazioniCrono","statoSchedaSistri","numero_scheda","NFORM","DTFORM","ID_RIF","pesoN","quantita","FKEumis", "PS_DESTINO","FISCALE","StampaAnnua","ID_IMP","produttore","intermediario","destinatario","trasportatore","idSIS_regCrono", "SenzaTrasporto", "TIPO_S_INTERNO", "ID_USR"),"user_movimenti_fiscalizzati");
$FEDIT->FGE_SetSelectFields(array("PerRiclassificazione","NMOV","TIPO","DTMOV","VER_DESTINO","DT_FORM","ID_OP_RS","ID_AUTST","ID_AUTO","ID_UIMP"),"user_movimenti_fiscalizzati");
$FEDIT->FGE_SetSelectFields(array("ID_AZP","ID_AZT","ID_AZD"),"user_movimenti_fiscalizzati");
$FEDIT->FGE_SetSelectFields(array("LINK_DestProd","LINK_TrattamentoTerminato"),"user_movimenti_fiscalizzati");
$FEDIT->FGE_SetSelectFields(array("descrizione","cod_pro","peso_spec"),"user_schede_rifiuti");
$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
$FEDIT->FGE_SetSelectFields(array("description"),"lov_stato_fisico");
$FEDIT->FGE_DescribeFields();
$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_movimenti_fiscalizzati");
$ProfiloFields = "user_movimenti_fiscalizzati";
include("SOGER_FiltriProfilo.php");
#
$FEDIT->FGE_HideFields(array("FISCALE","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS","idSIS_scheda","idSIS_regCrono", "SenzaTrasporto", "TIPO_S_INTERNO", "ID_USR"),"user_movimenti_fiscalizzati");
#
echo($FEDIT->SG_MovimentiFiscaliDataGrid($SOGER->AppDescriptiveLocation, "user_movimenti_fiscalizzati", "fiscale"));
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>