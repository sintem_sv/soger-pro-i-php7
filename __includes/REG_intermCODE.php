<?php

					$Xcorr =  $SOGER->UserData["core_usersREG_PRNT_X"];
					$Ycorr = $SOGER->UserData["core_usersREG_PRNT_Y"];
					//if($Xcorr>4)	$Xcorr=4;
					if($Ycorr>12)	$Ycorr=12;

					//echo "TIME: ".time()."<br/>";
					#
					$sql = "SELECT";
					$sql .= " user_movimenti_fiscalizzati.TIPO,user_movimenti_fiscalizzati.NMOV,user_movimenti_fiscalizzati.DTMOV";
					$sql .= ",user_movimenti_fiscalizzati.NFORM,user_movimenti_fiscalizzati.DTFORM";
					$sql .= ",user_movimenti_fiscalizzati.quantita,user_movimenti_fiscalizzati.PS_DESTINO,user_movimenti_fiscalizzati.NOTER";
					$sql .= ",user_schede_rifiuti.descrizione,user_schede_rifiuti.NOTER AS NOTE_RIFIUTO,lov_cer.COD_CER,lov_cer.PERICOLOSO as pericoloso, peso_spec";
					$sql .= ",lov_stato_fisico.description AS STFdes";
					$sql .= ",HP1,HP2,HP3,HP4,HP5,HP6,HP7,HP8,HP9,HP10,HP11,HP12,HP13,HP14,HP15";
					$sql .= ",ANN_scheda, ANN_scheda_causale, ANN_registrazione, ANN_registrazione_causale";
					$sql .= ",lov_misure.ID_UMIS AS Umisura";
					$sql .= ",user_aziende_produttori.description AS PRdes,user_aziende_produttori.codfisc AS PRCF,user_impianti_produttori.description AS IMPRdes";
					$sql .= ",user_aziende_destinatari.description AS DEdes,user_aziende_destinatari.codfisc AS DECF,user_impianti_destinatari.description AS IMDEdes";
					$sql .= ",user_aziende_trasportatori.description AS TRdes,user_aziende_trasportatori.codfisc AS TRCF,user_impianti_trasportatori.description AS IMTRdes";
					$sql .= ",COMpro.description AS COMPRdes,COMpro.shdes_prov AS COMPRPR";
					$sql .= ",COMdes.description AS COMDEdes,COMdes.shdes_prov AS COMDEPR";
					$sql .= ",COMtra.description AS COMTRdes,COMtra.shdes_prov AS COMTRPR";
					$sql .= ",lov_operazioni_rs.description AS OPRSdes";
					$sql .= ",lov_cer.description AS NomeCER";
					$sql .= " FROM user_movimenti_fiscalizzati"; 
					$sql .= " JOIN user_schede_rifiuti on user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF";
					$sql .= " JOIN lov_cer on user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
					$sql .= " JOIN lov_stato_fisico ON user_schede_rifiuti.ID_SF=lov_stato_fisico.ID_SF";
					$sql .= " JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS";
					
					$sql .= " JOIN user_aziende_produttori ON user_movimenti_fiscalizzati.ID_AZP=user_aziende_produttori.ID_AZP";
					$sql .= " JOIN user_impianti_produttori ON user_impianti_produttori.ID_UIMP=user_movimenti_fiscalizzati.ID_UIMP"; 
					$sql .= " JOIN lov_comuni_istat AS COMpro ON user_impianti_produttori.ID_COM=COMpro.ID_COM";
					
					$sql .= " JOIN user_aziende_destinatari ON user_movimenti_fiscalizzati.ID_AZD=user_aziende_destinatari.ID_AZD";
					$sql .= " JOIN user_impianti_destinatari ON user_impianti_destinatari.ID_UIMD=user_movimenti_fiscalizzati.ID_UIMD"; 
					$sql .= " JOIN lov_comuni_istat AS COMdes ON user_impianti_destinatari.ID_COM=COMdes.ID_COM";
					
					$sql .= " JOIN user_aziende_trasportatori ON user_movimenti_fiscalizzati.ID_AZT=user_aziende_trasportatori.ID_AZT";
					$sql .= " JOIN user_impianti_trasportatori ON user_impianti_trasportatori.ID_UIMT=user_movimenti_fiscalizzati.ID_UIMT"; 
					$sql .= " JOIN lov_comuni_istat AS COMtra ON user_impianti_trasportatori.ID_COM=COMtra.ID_COM";
					
					$sql .= " LEFT JOIN lov_operazioni_rs ON user_movimenti_fiscalizzati.ID_OP_RS=lov_operazioni_rs.ID_OP_RS";

					//$sql .= " WHERE (NMOV>='" . $_GET["da"]*2 . "' AND NMOV<='" . $_GET["a"]*2 . "') ";
					//$sql .= " AND TIPO='I' ";

					//$sql .= " WHERE (NMOV>='" . $_GET["da"] . "' AND NMOV<='" . $_GET["a"] . "') ";
					$sql .= " WHERE user_movimenti_fiscalizzati.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "' AND NMOV<>9999999 ";

					$sql .= " AND (NMOV>='" . $_GET["da"] . "' AND NMOV<='" . $_GET["a"] . "') AND NMOV<>9999999";

						
					$TableSpec = "user_movimenti_fiscalizzati.";
					include("../__includes/SOGER_DirectProfilo.php");
					#movimenti sono SEMPRE approved=1
					$sql .= " ORDER BY NMOV ASC";



					$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
					
					//echo "TIME: ".time()."<br/>";die();

					//print_r($FEDIT->DbRecordSet);

					# in caso di stampa annua...
					/*
					if($_GET['Fiscale']==1){
						for($c=0;$c<$FEDIT->DbRecsNum;$c++) {
							$sql = "UPDATE user_movimenti_fiscalizzati SET StampaAnnua = '1' WHERE NMOV = '".$FEDIT->DbRecordSet[$c]["NMOV"]."' ";
							$sql.= "AND user_movimenti_fiscalizzati.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
							$sql.= "AND user_movimenti_fiscalizzati.intermediario=1 ";
							$FEDIT->SDBWrite($sql,true,false);
						}
					}
					*/

					#
					if(isset($_GET["cartaBianca"])) {

						# stampa registro (carta bianca)
						$DcName = "Stampa_Registro";
						$x = 2 + $Xcorr;
						$y = 12 + $Ycorr;
						$GridsCount = 0;
						$PageCount = 1;
					
						if ($_GET["PaginaIniz"]=="1") {
							$PageNr = 1;
							$CustomPageIniz = false;
							} 
						else {
							$CustomPageIniz = true;
							$PageNr = $_GET["PaginaIniz"]; 
							}

						$TotalPages = ceil($FEDIT->DbRecsNum/3);						
						
						# crea sfondi (griglia dati)
						for($c=0;$c<$FEDIT->DbRecsNum;$c++) {

							//echo "PAGINA: ".$PageCount."<br />";
							//$PageCount++;
							
							
							SfondoRegistroInt($x,$y,$FEDIT->FGE_PdfBuffer);
							if(!isset($BufAnno)) {
								$BufAnno = date("Y",strtotime($FEDIT->DbRecordSet[$c]["DTMOV"]));
								}
							$ContaNMOV=$c+1;
							
							
							
							DatiRegistroInt($FEDIT->DbRecordSet[$c],$x,$y,$FEDIT->FGE_PdfBuffer,$ContaNMOV);
							
							$y +=84.6;
							$GridsCount++;
							
							if(isset($_GET["Npagina"]) && $PageCount==$TotalPages) {			//ultima pagina del registro
								$FEDIT->FGE_PdfBuffer->SetXY(190,271);
								if($_GET['Vidimato']==0)
									$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$BufAnno."/".$PageNr,0,"L");
								else{
									$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$PageNr,0,"L");
									}
								}

							if($GridsCount==3 && $PageCount<$TotalPages) {						//pagine intermedie del registro
								# numeri di pagina
								if(isset($_GET["Npagina"])) {
									$FEDIT->FGE_PdfBuffer->SetXY(190,271);
									if($_GET['Vidimato']==0)
										$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$BufAnno."/".$PageNr,0,"L");
									else{
										$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$PageNr,0,"L");
										}
									}
								$FEDIT->FGE_PdfBuffer->AddPage();
								$GridsCount = 0;
								$PageCount++;
								$PageNr++;
								$x = 2 + $Xcorr;
								$y = 12 + $Ycorr;
								}


							}						
						//die();
						} 
					else {
						# stampa registro (prefincato)
						
							$DcName = "Stampa_Registro_Prefincato";
							$x = 2 + $Xcorr;
							$y = 16 + $Ycorr;
							$GridsCount = 0;
							$PageCount = 1;
						
							if ($_GET["PaginaIniz"]=="1") {
								$PageNr = 1;
								$CustomPageIniz = false;
							} else {
								$CustomPageIniz = true;
								$PageNr = $_GET["PaginaIniz"]; 
							}
							$TotalPages = ceil($FEDIT->DbRecsNum/3);
							# crea sfondi (griglia dati)
							for($c=0;$c<$FEDIT->DbRecsNum;$c++) {
								if(!isset($BufAnno)) {
									$BufAnno = date("Y",strtotime($FEDIT->DbRecordSet[$c]["DTMOV"]));
									}
								$ContaNMOV=$c+1;
								DatiRegistroInt($FEDIT->DbRecordSet[$c],$x,$y,$FEDIT->FGE_PdfBuffer,$ContaNMOV);
								$y +=84.6;
								$GridsCount++;
						
								if(isset($_GET["Npagina"]) && $PageCount==$TotalPages) {			//ultima pagina del registro
									$FEDIT->FGE_PdfBuffer->SetFont('Arial','',5);
									$FEDIT->FGE_PdfBuffer->SetXY(190,271);
									if($_GET['Vidimato']==0)
										$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$BufAnno."/".$PageNr,0,"L");
									else{
										$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$PageNr,0,"L");
										}
									$FEDIT->FGE_PdfBuffer->SetFont('Arial','',7);
									}								

								if($GridsCount==3 && $PageCount<$TotalPages) {
									# numeri di pagina
									if(isset($_GET["Npagina"])) {
										$FEDIT->FGE_PdfBuffer->SetFont('Arial','',5);
										$FEDIT->FGE_PdfBuffer->SetXY(190,271);
										if($_GET['Vidimato']==0)
											$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$BufAnno."/".$PageNr,0,"L");
										else{
											$FEDIT->FGE_PdfBuffer->MultiCell(25,5,$PageNr,0,"L");
											}
										$FEDIT->FGE_PdfBuffer->SetFont('Arial','',7);
										}
									$FEDIT->FGE_PdfBuffer->AddPage();
									$GridsCount = 0;
									$PageCount++;
									$PageNr++;
									$x = 2 + $Xcorr;
									$y = 16 + $Ycorr;
								}
							}
					}


?>