<?PHP
//require("USER_SchedeTrasportatoriIDcheck.php");
//print_r($this->AppLocation);
switch($this->AppLocation){
	case "UserNuovoTrasportatore":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-ON.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_numalbo-OFF.gif";
		$BT_contratti		= "BT_contratti_tr-OFF.gif";
		$BT_automezzi		= "BT_automezzi-OFF.gif";
		$BT_autisti			= "BT_autisti-OFF.gif";
		$BT_rimorchi		= "BT_rimorchi-OFF.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";		
		break;
	case "UserNuovoTrasportatoreImpianto":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-ON.gif";
		$BT_autorizzazioni	= "BT_numalbo-OFF.gif";
		$BT_contratti		= "BT_contratti_tr-OFF.gif";
		$BT_automezzi		= "BT_automezzi-OFF.gif";
		$BT_autisti			= "BT_autisti-OFF.gif";
		$BT_rimorchi		= "BT_rimorchi-OFF.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";		
		break;
	case "UserNuovoTrasportatoreAutorizzazioni":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_numalbo-ON.gif";
		$BT_contratti		= "BT_contratti_tr-OFF.gif";
		$BT_automezzi		= "BT_automezzi-OFF.gif";
		$BT_autisti			= "BT_autisti-OFF.gif";
		$BT_rimorchi		= "BT_rimorchi-OFF.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";		
		break;
	case "UserNuovoContrattoTrasportatore";
	case "UserTrasportatoreContratti";
	case "UserTrasportatoreContrattoCondizioni";
	case "UserCondizioneContrattoTrasportatore":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_numalbo-OFF.gif";
		$BT_contratti		= "BT_contratti_tr-ON.gif";
		$BT_automezzi		= "BT_automezzi-OFF.gif";
		$BT_autisti			= "BT_autisti-OFF.gif";
		$BT_rimorchi		= "BT_rimorchi-OFF.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";		
		break;
	case "UserNuovoTrasportatoreAutomezzi":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_numalbo-OFF.gif";
		$BT_contratti		= "BT_contratti_tr-OFF.gif";
		$BT_automezzi		= "BT_automezzi-ON.gif";
		$BT_autisti			= "BT_autisti-OFF.gif";
		$BT_rimorchi		= "BT_rimorchi-OFF.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";		
		break;
	case "UserNuovoTrasportatoreRimorchi":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_numalbo-OFF.gif";
		$BT_contratti		= "BT_contratti_tr-OFF.gif";
		$BT_automezzi		= "BT_automezzi-OFF.gif";
		$BT_autisti			= "BT_autisti-OFF.gif";
		$BT_rimorchi		= "BT_rimorchi-ON.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";		
		break;
	case "UserNuovoTrasportatoreAutisti";
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_numalbo-OFF.gif";
		$BT_contratti		= "BT_contratti_tr-OFF.gif";
		$BT_automezzi		= "BT_automezzi-OFF.gif";
		$BT_autisti			= "BT_autisti-ON.gif";
		$BT_rimorchi		= "BT_rimorchi-OFF.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";		
		break;
	case "UserNuovoTrasportatoreAllegati";
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_numalbo-OFF.gif";
		$BT_contratti		= "BT_contratti_tr-OFF.gif";
		$BT_automezzi		= "BT_automezzi-OFF.gif";
		$BT_autisti			= "BT_autisti-OFF.gif";
		$BT_rimorchi		= "BT_rimorchi-OFF.gif";
		$BT_allegati		= "BT_allegati-ON.gif";		
		break;
	}


if(!isset($_SESSION["FGE_IDs"]["user_aziende_trasportatori"])) {
?>
<a href="#" title="Dati Intestatario"><img src="__css/<?php echo $BT_ragione; ?>" class="NoBorder" alt="Dati Intestatario" /></a>
<a href="javascript:alert('Inserire%20prima%20la%20Ragione%20Sociale')" title="Dati Impianto"><img src="__css/<?php echo $BT_impianti; ?>" class="NoBorder" alt="Dati Impianto"/></a>
<a href="javascript:alert('Inserire%20prima%20i%20dati%20Impianto')" title="Numeri Albo"><img src="__css/<?php echo $BT_autorizzazioni; ?>" class="NoBorder" alt="Numeri Albo"/></a>
<a href="javascript:alert('Inserire%20prima%20i%20dati%20Impianto')" title="Automezzi"><img src="__css/<?php echo $BT_automezzi; ?>" class="NoBorder" alt="Automezzi"/></a>
<a href="javascript:alert('Inserire%20prima%20i%20dati%20Impianto')" title="Autisti"><img src="__css/<?php echo $BT_autisti; ?>" class="NoBorder" alt="Autisti"/></a>
<a href="javascript:alert('Inserire%20prima%20i%20dati%20Impianto')" title="Rimorchi"><img src="__css/<?php echo $BT_rimorchi; ?>" class="NoBorder" alt="Rimorchi"/></a>
<?php if($this->UserData["core_impiantiMODULO_ECO"]=="1" AND $this->UserData["core_usersG3"]=="1"){ ?>
<a href="javascript:alert('Inserire%20prima%20i%20dati%20Autorizzazioni')" title="Contratti"><img src="__css/<?php echo $BT_contratti; ?>" class="NoBorder" alt="Contratti"/></a>
<?php } ?>
<a href="javascript:alert('Inserire%20prima%20la%20Ragione%20Sociale')" title="Allegati"><img src="__css/<?php echo $BT_allegati; ?>" class="NoBorder" alt="Allegati"/></a>

<?php
} else {
	$urlRS = "__scripts/SOGER_standardMenu.php?area=UserNuovoTrasportatore&amp;table=user_aziende_trasportatori&amp;pri=ID_AZT&amp;filter=" . @$_SESSION["FGE_IDs"]["user_aziende_trasportatori"] . "&hash=" . MakeUrlHash("user_aziende_trasportatori","ID_AZT",@$_SESSION["FGE_IDs"]["user_aziende_trasportatori"]);
		
	$urlALL = "__scripts/SOGER_standardMenu.php?area=UserNuovoTrasportatoreAllegati&amp;table=user_aziende_trasportatori&amp;pri=ID_AZT&amp;filter=" . @$_SESSION["FGE_IDs"]["user_aziende_trasportatori"] . "&hash=" . MakeUrlHash("user_aziende_trasportatori","ID_AZT",@$_SESSION["FGE_IDs"]["user_aziende_trasportatori"]);

	$sql = "SELECT MAX(ID_UIMT) AS ID_UIMT FROM user_impianti_trasportatori WHERE ID_AZT=".@$_SESSION["FGE_IDs"]["user_aziende_trasportatori"].";";
	$F = new DBLink();
	$F->DbConn($_SESSION["DbInUse"]);
	$F->SDbRead($sql,"DbRecordSet",true,false);
	$ID_UIMT = $F->DbRecordSet[0]["ID_UIMT"];
	if(!is_null($ID_UIMT)) {
		
		$urlIMP = "__scripts/SOGER_standardMenu.php?area=UserNuovoTrasportatoreImpianto&amp;table=user_impianti_trasportatori&amp;pri=ID_UIMT&amp;filter=" . $F->DbRecordSet[0]["ID_UIMT"] . "&hash=" . MakeUrlHash("user_impianti_trasportatori","ID_UIMT",$ID_UIMT);
		

		$sql = "SELECT MAX(ID_AUTHT) AS ID_AUTHT FROM user_autorizzazioni_trasp WHERE ID_UIMT=".$ID_UIMT.";";
		$F = new DBLink();
		$F->DbConn($_SESSION["DbInUse"]);
		$F->SDbRead($sql,"DbRecordSet",true,false);
		$ID_AUTHT = $F->DbRecordSet[0]["ID_AUTHT"];		
		if(!is_null($F->DbRecordSet[0]["ID_AUTHT"])) {

			$urlAUTH = "__scripts/SOGER_standardMenu.php?area=UserNuovoTrasportatoreAutorizzazioni&amp;table=user_autorizzazioni_trasp&amp;pri=ID_AUTHT&amp;filter=" . $F->DbRecordSet[0]["ID_AUTHT"] . "&hash=" . MakeUrlHash("user_autorizzazioni_trasp","ID_AUTHT",$ID_AUTHT);

			$sql = "SELECT MAX(ID_CNT_T) AS ID_CNT_T FROM user_contratti_trasportatori WHERE ID_AZT=".@$_SESSION["FGE_IDs"]["user_aziende_trasportatori"].";";
			$F = new DBLink();
			$F->DbConn($_SESSION["DbInUse"]);
			$F->SDbRead($sql,"DbRecordSet",true,false);
			$ID_CNT_T = $F->DbRecordSet[0]["ID_CNT_T"];	
			if(!is_null($ID_CNT_T)) {
				$urlECO = "__scripts/SOGER_standardMenu.php?area=UserTrasportatoreContratti&amp;table=user_contratti_trasportatori&amp;pri=ID_AZT&amp;filter=" . $_SESSION["FGE_IDs"]["user_aziende_trasportatori"]."&amp;hash=" . MakeUrlHash("user_contratti_trasportatori","ID_AZT",$_SESSION["FGE_IDs"]["user_aziende_trasportatori"]);
				}
			else{
				$_SESSION['ID_AZT_CONTRACT']=$_SESSION["FGE_IDs"]["user_aziende_trasportatori"];
				$urlECO = "__scripts/SOGER_standardMenu.php?area=UserNuovoContrattoTrasportatore";
				}
		} else {
			$urlAUTH = "__scripts/status.php?area=UserNuovoTrasportatoreAutorizzazioni";
			$urlECO = "javascript:alert('Inserire%20prima%20i%20dati%20Autorizzazioni')";
		}

		$urlAUTST = "__scripts/status.php?area=UserNuovoTrasportatoreAutisti";

		$urlAUTM = "__scripts/status.php?area=UserNuovoTrasportatoreAutomezzi";

		$urlRMK = "__scripts/status.php?area=UserNuovoTrasportatoreRimorchi";
		
	} else {
		$urlIMP = "__scripts/status.php?area=UserNuovoTrasportatoreImpianto";
		$urlAUTH = "javascript:alert('Inserire%20prima%20i%20dati%20Impianto')";
		$urlAUTST = "javascript:alert('Inserire%20prima%20i%20dati%20Impianto')";
		$urlAUTM = "javascript:alert('Inserire%20prima%20i%20dati%20Impianto')";
		$urlRMK = "javascript:alert('Inserire%20prima%20i%20dati%20Impianto')";
		//$urlECO = "javascript:alert('Inserire%20prima%20i%20dati%20Impianto')";
	}
?>
<a href="<?php echo $urlRS ?>" title="Dati Intestatario"><img src="__css/<?php echo $BT_ragione; ?>" class="NoBorder" alt="Dati Intestatario" /></a>
<a href="<?php echo $urlIMP ?>" title="Dati Impianto"><img src="__css/<?php echo $BT_impianti; ?>" class="NoBorder" alt="Dati Impianto"/></a>
<a href="<?php echo $urlAUTH ?>" title="Numeri Albo"><img src="__css/<?php echo $BT_autorizzazioni; ?>" class="NoBorder" alt="Numeri Albo"/></a>
<a href="<?php echo $urlAUTM ?>" title="Automezzi"><img src="__css/<?php echo $BT_automezzi; ?>" class="NoBorder" alt="Automezzi"/></a>
<a href="<?php echo $urlAUTST ?>" title="Autisti"><img src="__css/<?php echo $BT_autisti; ?>" class="NoBorder" alt="Autisti"/></a>
<a href="<?php echo $urlRMK ?>" title="Rimorchi"><img src="__css/<?php echo $BT_rimorchi; ?>" class="NoBorder" alt="Rimorchi"/></a>
<?php if($this->UserData["core_impiantiMODULO_ECO"]=="1" AND $this->UserData["core_usersG3"]=="1"){ ?>
<a href="<?php echo $urlECO ?>" title="Contratti"><img src="__css/<?php echo $BT_contratti; ?>" class="NoBorder" alt="Contratti"/></a>
<?php } ?>
<a href="<?php echo $urlALL ?>" title="Allegati"><img src="__css/<?php echo $BT_allegati; ?>" class="NoBorder" alt="Allegati"/></a>

<?php
}
?>