<?php

global $SOGER;


###########################################################
#
# WORKMODE
#
###########################################################

if($SOGER->UserData["workmode"]=="produttore") {
        $FEDIT->FGE_SetFilter("produttore","1",$ProfiloFields);
} else {
        $FEDIT->FGE_SetFilter("produttore","0",$ProfiloFields);
        }
if($SOGER->UserData["workmode"]=="trasportatore") {
        $FEDIT->FGE_SetFilter("trasportatore","1",$ProfiloFields);
} else {
        $FEDIT->FGE_SetFilter("trasportatore","0",$ProfiloFields);
        }
if($SOGER->UserData["workmode"]=="destinatario") {
        $FEDIT->FGE_SetFilter("destinatario","1",$ProfiloFields);
} else {
        $FEDIT->FGE_SetFilter("destinatario","0",$ProfiloFields);
        }
if($SOGER->UserData["workmode"]=="intermediario") {
        $FEDIT->FGE_SetFilter("intermediario","1",$ProfiloFields);
} else {
        $FEDIT->FGE_SetFilter("intermediario","0",$ProfiloFields);
        }


###########################################################
#
# APPROVED = 1
#
###########################################################

$NotFilterStatus = array();

if($SOGER->UserData["core_usersG1"]=="0"){

    // Se G1=0 il filtro non lo applico sempre?...
    array_push($NotFilterStatus, "UserNuovoMovScaricoDettaglio", "UserNuovoMovCaricoDettaglio", "UserNuovoMovScaricoDettaglioF", "UserNuovoMovCaricoDettaglioF");

    // Se G14=1 applico meno filtri, lascio che possano vedere le anagrafiche ma che non possano utilizzarle
    if($SOGER->UserData["core_usersG14"]=="1")
        array_push($NotFilterStatus, "UserSchedeRifiuti", "UserSchedeProduttori", "UserSchedeDestinatari", "UserSchedeIntermediari", "UserSchedeTrasportatori");

}
else{
    array_push($NotFilterStatus, "UserNuovaSchedaSic", "UserSchedeDestinatari", "UserSchedeRifiuti", "UserSchedaRifiutoCaratt", "UserSchedeIntermediari", "UserSchedeProduttori", "UserSchedeTrasportatori", "UserNuovoMovScaricoDettaglio", "UserNuovoMovCaricoDettaglio", "UserNuovoMovScaricoDettaglioF", "UserNuovoMovCaricoDettaglioF", "UserRifiutoSegnaletica");
}

if(isset($_GET) AND (strpos($_GET['table'], 'user_autorizzazioni') !== false))
    array_push($NotFilterStatus, $SOGER->AppLocation);

array_push($NotFilterStatus, "UserRifiutoPittogrammi");

if( !in_array($SOGER->AppLocation, $NotFilterStatus) && !isset($_SESSION["MovimentoEdit_aPosteriori"]) )
    $FEDIT->FGE_SetFilter("approved","1",$ProfiloFields);

?>