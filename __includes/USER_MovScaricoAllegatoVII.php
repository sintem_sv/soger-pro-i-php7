<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
global $SOGER;

$TableName=$_GET['table'];
if($TableName=="user_movimenti") $PRI="ID_MOV"; else $PRI="ID_MOV_F";

$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables($TableName);

$FEDIT->FGE_SetFormFields(array("N_ANNEX_VII", "ID_ORG", "REFERENTE_ORG", "REFERENTE_GEN", "REFERENTE_DEST", "REFERENTE_TRASP"),$TableName);
$FEDIT->FGE_SetFormFields(array("ID_BASILEA", "ID_OCSE", "ID_ALL_IIIA", "ID_ALL_IIIB"),$TableName);
$FEDIT->FGE_SetBreak("N_ANNEX_VII",$TableName);
#
$FEDIT->FGE_DescribeFields();
#
$FEDIT->FGE_LookUpDefault("ID_ORG",$TableName);
	
$FEDIT->FGE_LookUpCFG("ID_BASILEA",$TableName,true,false);
$FEDIT->FGE_UseTables(array("lov_annex_vii_basilea"));
$FEDIT->FGE_SetSelectFields(array("code", "description"),"lov_annex_vii_basilea");
$FEDIT->FGE_HideFields(array("description"),"lov_annex_vii_basilea");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();
	
$FEDIT->FGE_LookUpCFG("ID_OCSE",$TableName,true,false);
$FEDIT->FGE_UseTables(array("lov_annex_vii_ocse"));
$FEDIT->FGE_SetSelectFields(array("code", "description"),"lov_annex_vii_ocse");
$FEDIT->FGE_HideFields(array("description"),"lov_annex_vii_ocse");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();
	
$FEDIT->FGE_LookUpCFG("ID_ALL_IIIA",$TableName,true,false);
$FEDIT->FGE_UseTables(array("lov_annex_vii_iiia"));
$FEDIT->FGE_SetSelectFields(array("code", "description"),"lov_annex_vii_iiia");
$FEDIT->FGE_HideFields(array("description"),"lov_annex_vii_iiia");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();
	
$FEDIT->FGE_LookUpCFG("ID_ALL_IIIB",$TableName,true,false);
$FEDIT->FGE_UseTables(array("lov_annex_vii_iiib"));
$FEDIT->FGE_SetSelectFields(array("code", "description"),"lov_annex_vii_iiib");
$FEDIT->FGE_HideFields(array("description"),"lov_annex_vii_iiib");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();

$FEDIT->FGE_SetTitle("N_ANNEX_VII","Allegato VII",$TableName);
$FEDIT->FGE_SetTitle("ID_ORG","Persona che organizza la spedizione / Generatore dei rifiuti",$TableName);
$FEDIT->FGE_SetTitle("REFERENTE_DEST","Importatore/Destinatario",$TableName);
$FEDIT->FGE_SetTitle("REFERENTE_TRASP","Primo vettore",$TableName);
$FEDIT->FGE_SetTitle("ID_BASILEA","Identificazione dei rifiuti",$TableName);

$FEDIT->FGE_DisableFields(array("REFERENTE_GEN"),$TableName);


if($_SESSION['Fiscalizzato']=="1" && $SOGER->UserData['core_impiantiREG_IND']=="1"){
	$FEDIT->FGE_DisableFields(array("ID_ORG", "REFERENTE_ORG", "REFERENTE_GEN", "REFERENTE_DEST", "REFERENTE_TRASP","ID_BASILEA", "ID_OCSE", "ID_ALL_IIIA", "ID_ALL_IIIB"),$TableName);
	}

# verifico se devo bloccare movimento perch� aperto da altri
if($SOGER->UserData['core_usersO12']=='0'){

	$sql = "SELECT ID_USR FROM ".$_GET['table']." WHERE ".$_GET['pri']."=".$_GET['filter'].";";
	$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

	if($FEDIT->DbRecordSet[0]['ID_USR']!='0000000' AND ($SOGER->UserData['core_usersID_USR']!=$FEDIT->DbRecordSet[0]['ID_USR'])){
		echo "<div id=\"Errors\" class=\"ErrType1\" onclick=\"javascript:document.getElementById('Errors').style.zIndex=-1\">";
		echo "L'utente non ha i permessi necessari per modificare i movimenti inseriti da altri utenti [cod. O12]";
		echo "</div>";
		$FEDIT->FGE_DisableFields(array("N_ANNEX_VII", "ID_ORG", "REFERENTE_ORG", "REFERENTE_GEN", "REFERENTE_DEST", "REFERENTE_TRASP"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_BASILEA", "ID_OCSE", "ID_ALL_IIIA", "ID_ALL_IIIB"),$TableName);
		}
	}



echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"Salva Allegato VII");


# firefox
$css.="<style type=\"text/css\">\n";
$css.="div#btn_print_annex_vii{\n";
$css.="position: absolute; top: 765px; left: 155px; z-index: 10;";
$css.="}\n";
$css.="</style>\n";

# ie
$css.="<!--[if IE]>";
$css.="<style type=\"text/css\">\n";
$css.="div#btn_print_annex_vii{\n";
$css.="position: absolute; top: 821px; left: 170px; z-index: 10;";
$css.="}\n";
$css.="</style>\n";
$css.="<![endif]-->";

$html ="<div class=\"FGESubmitRow\" id=\"btn_print_annex_vii\">\n";

$html.="<input type=\"button\" class=\"FGEbutton\" value=\"Stampa Allegato VII\" onclick=\"CheckAnnexVIIAndPrint();\" onfocus=\"this.style.background='yellow';this.style.color='#034373';\" onblur=\"this.style.background='#034373';this.style.color='white';\"/>\n";

$html.="</div>\n";

echo $css;
echo $html;

if(isset($_GET['PrintAnnexVII'])){
	$js = "<script type='text/javascript'>";

	$js.= "$(window).load(function(){";
	$js.= "var url = '__scripts/AllegatoVII.php?table=".$_GET['table']."&filter=".$_GET['filter']."&pri=".$_GET['pri']."';";
	$js.= "location.replace(url);";
	$js.= "})";

	$js.= "</script>";
	echo $js;
	}

require_once("__includes/COMMON_sleepForgEdit.php");

?>