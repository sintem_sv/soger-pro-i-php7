<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;

echo "<div class=\"FGEDataGridTitle\" style=\"margin-bottom:65px;\"><div>Controllo di gestione � Classi di efficienza</div></div>";

$js.= <<<JS
<script type="text/javascript">

$(document).ready(function(){

	$("#select_mese").change(function(){
		$("#select_trimestre").val($("#select_trimestre option:first").val());
		$("#select_anno").val($("#select_anno option:first").val());
		});

	$("#select_trimestre").change(function(){
		$("#select_mese").val($("#select_mese option:first").val());
		$("#select_anno").val($("#select_anno option:first").val());
		});

	$("#select_anno").change(function(){
		$("#select_trimestre").val($("#select_trimestre option:first").val());
		$("#select_mese").val($("#select_mese option:first").val());
		});
	
	//loading while ajax is getting info
	$('#showCea').ajaxStart(function() {
		$('.FSS, .FST, .FSK, .FSM, .CEA, .KGD').html('<img src="__css/fb_loading.gif" />');
		});

	$('#showCea').ajaxStop(function() {
		$('#canDownload').val(1);
		});

	$("#downloadPDF").click(function(){
		if($('#canDownload').val()==1){
			$('#DownloadType').val('PDF');
			$("#CEA2PDF").submit();
			}
		else
			alert("Attenzione, impossibile procedere con il download del file, procedere prima con il calcolo.");
		});

	$("#downloadCSV").click(function(){
		if($('#canDownload').val()==1){
			$('#DownloadType').val('CSV');
			$("#CEA2PDF").submit();
			}
		else
			alert("Attenzione, impossibile procedere con il download del file, procedere prima con il calcolo.");
		});

	$("#showCea").click(function(){
		if($("#select_trimestre").val()=='' && $("#select_anno").val()=='')
			alert("Attenzione, selezionare il periodo di riferimento");
		else{
			var MyRIF	= new Array();
			var MyCER	= new Array();
			var MyCERCODE	= new Array();

			//populate array
			for(r=0;r<$("#rifiutiCounter").val();r++){
				MyCER[r]	=$("#ID_CER_"+r).val();
				MyCERCODE[r]=$("#ID_CERCODE_"+r).val();
				MyRIF[r]	=$("#ID_RIF_"+r).val();
				}
			
			//get data and update view
			$.post('__sdg/getCea.php', {CERCODE:MyCERCODE, CER:MyCER, RIF:MyRIF, trimestre:$("#select_trimestre").val(), anno:$("#select_anno").val()}, function(phpResponse){
				//alert(dump(phpResponse));
				for(r=0;r<$("#rifiutiCounter").val();r++){

					// FSS
					FSS=phpResponse[r]['FSS'];
					if(FSS!='n.d.'){
						$("#FSS_"+r).html(FSS+" %");
						$("#IH_FSS_"+r).val(FSS+" %");
						}
					else{
						$("#FSS_"+r).html(FSS);
						$("#IH_FSS_"+r).val(FSS);
						}
					if(parseFloat(FSS)<0){
						$("#FSS_"+r).css('color', '#FF0000');
						}
					if(parseFloat(FSS)>0){
						$("#FSS_"+r).css('color', '#037214');
						}

					// FST
					FST=phpResponse[r]['FST'];
					if(FST!='n.d.'){
						$("#FST_"+r).html(FST+" %");
						$("#IH_FST_"+r).val(FST+" %");
						}
					else{
						$("#FST_"+r).html(FST);
						$("#IH_FST_"+r).val(FST);
						}
					if(parseFloat(FST)<0){
						$("#FST_"+r).css('color', '#FF0000');
						}
					if(parseFloat(FST)>0){
						$("#FST_"+r).css('color', '#037214');
						}

					// FSK
					FSK=phpResponse[r]['FSK'];
					if(FSK!='n.d.'){
						$("#FSK_"+r).html(FSK+" %");
						$("#IH_FSK_"+r).val(FSK+" %");
						}
					else{
						$("#FSK_"+r).html(FSK);
						$("#IH_FSK_"+r).val(FSK);
						}
					if(parseFloat(FSK)<0){
						$("#FSK_"+r).css('color', '#FF0000');
						}
					if(parseFloat(FSK)>0){
						$("#FSK_"+r).css('color', '#037214');
						}

					// FSM
					FSM=phpResponse[r]['FSM'];
					if(FSM!='n.d.'){
						$("#FSM_"+r).html(FSM+" %");
						$("#IH_FSM_"+r).val(FSM+" %");
						}
					else{
						$("#FSM_"+r).html(FSM);
						$("#IH_FSM_"+r).val(FSM);
						}
					if(parseFloat(FSM)<0){
						$("#FSM_"+r).css('color', '#FF0000');
						}
					if(parseFloat(FSM)>0){
						$("#FSM_"+r).css('color', '#037214');
						}

					// CEA
					$("#CEA_"+r).html(phpResponse[r]['CEA']);
					$("#IH_CEA_"+r).val(phpResponse[r]['CEA']);

					// KG A DESTINO
					//kg=phpResponse[r]['KG'];
					//$("#KG_"+r).html(kg+" kg");
					//$("#IH_KG_"+r).val(kg+" kg");
					}				
				},"json");
			}
		});//click
			
	});

</script>
JS;

echo $js;

$tableConfig ="<table id=\"TableConfigIndex\" cellpadding='0' cellspacing='1'>\n<form name='CEA2PDF' id='CEA2PDF' method='POST' action='__sdg/DownloadCEA.php'>";

$tableConfig.="<thead><tr>";
	$tableConfig.="<th colspan=\"3\">CONFIGURA VISUALIZZAZIONE</th>\n";
$tableConfig.="</tr>\n</thead><tbody>\n";

	$tableConfig.="<tr><th>Indica il periodo di riferimento</th>";

		## TRIMESTRE
		$tableConfig.="<td>";
			$tableConfig.="<select name='trimestre' id='select_trimestre'>";
				$tableConfig.="<option value=''>-- trimestre --</option>";
				$tableConfig.="<option value='trim_I'>Trimestre 1: gennaio, febbraio, marzo ".substr($_SESSION["DbInUse"], -4)."</option>";
				$tableConfig.="<option value='trim_II'>Trimestre 2: aprlie, maggio, giugno ".substr($_SESSION["DbInUse"], -4)."</option>";
				$tableConfig.="<option value='trim_III'>Trimestre 3: luglio, agosto, settembre ".substr($_SESSION["DbInUse"], -4)."</option>";
				$tableConfig.="<option value='trim_IV'>Trimestre 4: ottobre, novembre, dicembre ".substr($_SESSION["DbInUse"], -4)."</option>";
			$tableConfig.="</select>";
		$tableConfig.="</td>";

		## ANNO 
		$tableConfig.="<td>";
			$tableConfig.="<select name='anno' id='select_anno'>";
				$tableConfig.="<option value=''>-- anno --</option>";
				$tableConfig.="<option value='".substr($_SESSION["DbInUse"], -4)."'>Anno ".substr($_SESSION["DbInUse"], -4)."</option>";
			$tableConfig.="</select>";
		$tableConfig.="</td></tr>";


		$tableConfig.="<td colspan=\"3\">";
			## SUBMIT 
			$tableConfig.="<input class='IndexButton' type='button' id='showCea' value='CALCOLA CEA' />";
			## PDF 
			$tableConfig.="<input class='IndexButton' type='button' id='downloadPDF' value='DOWNLOAD PDF' />";
			## CSV
			$tableConfig.="<input class='IndexButton' type='button' id='downloadCSV' value='DOWNLOAD CSV' />";
		$tableConfig.="</td></tr>";
	
	$tableConfig.="</tbody></table>";




$sql="SELECT ID_RIF, lov_cer.COD_CER AS CER, lov_cer.ID_CER AS ID_CER, descrizione, ID_RIF FROM user_schede_rifiuti JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1 ORDER BY COD_CER;";
$FEDIT->SDBRead($sql,"DbRecordset");

$table.="<table id=\"TableIndex\" cellspacing='1' cellpadding='0'>";

	$table.="<thead><tr>";
		$table.="<th>CODICE CER</th>\n";
		$table.="<th>DESCRIZIONE</th>\n";
		$table.="<th>FSS</th>\n";
		$table.="<th>FST</th>\n";
		$table.="<th>FSK</th>\n";
		$table.="<th>FSM</th>\n";
		$table.="<th>CEA</th>\n";
		//$table.="<th>KG (a destino)</th>\n";
	$table.="</tr>\n</thead><tbody>\n";

for($r=0;$r<count($FEDIT->DbRecordset);$r++){
	$table.="<tr class=\"ncRow\">";
		$table.="<td class=\"CER\">".$FEDIT->DbRecordset[$r]['CER'];
			$table.="<input type='hidden' name='ID_CER_".$r."' id='ID_CER_".$r."' value='".$FEDIT->DbRecordset[$r]['ID_CER']."' />";
			$table.="<input type='hidden' name='ID_CERCODE_".$r."' id='ID_CERCODE_".$r."' value='".$FEDIT->DbRecordset[$r]['CER']."' />";
			$table.="<input type='hidden' name='ID_RIF_".$r."' id='ID_RIF_".$r."' value='".$FEDIT->DbRecordset[$r]['ID_RIF']."' />";
		$table.="</td>\n";
		$table.="<td class=\"DESC\" id=\"DESC_".$r."\">".$FEDIT->DbRecordset[$r]['descrizione']."<input type='hidden' name='IH_DESC_".$r."' id='IH_DESC_".$r."' value='".$FEDIT->DbRecordset[$r]['descrizione']."' /></td>\n";
		$table.="<td class=\"FSS\" id='FSS_".$r."'></td><input type='hidden' name='IH_FSS_".$r."' id='IH_FSS_".$r."' value='' />\n";
		$table.="<td class=\"FST\" id='FST_".$r."'></td><input type='hidden' name='IH_FST_".$r."' id='IH_FST_".$r."' value='' />\n";
		$table.="<td class=\"FSK\" id='FSK_".$r."'></td><input type='hidden' name='IH_FSK_".$r."' id='IH_FSK_".$r."' value='' />\n";
		$table.="<td class=\"FSM\" id='FSM_".$r."'></td><input type='hidden' name='IH_FSM_".$r."' id='IH_FSM_".$r."' value='' />\n";
		$table.="<td class=\"CEA\" id='CEA_".$r."'></td><input type='hidden' name='IH_CEA_".$r."' id='IH_CEA_".$r."' value='' />\n";
		//$table.="<td class=\"KGD\" id='KG_".$r."'></td><input type='hidden' name='IH_KG_".$r."' id='IH_KG_".$r."' value='' />\n";
	$table.="</tr>\n";
	}

$table.="</tbody></table>";

## rifiutiCounter
$table.="<input type='hidden' name='rifiutiCounter' id='rifiutiCounter' value='".count($FEDIT->DbRecordset)."' />\n";
$table.="<input type='hidden' name='canDownload' id='canDownload' value='0' />\n";
$table.="<input type='hidden' name='DownloadType' id='DownloadType' value='' />\n";
$table.="<input type='hidden' name='FileName' id='FileName' value='Classi di efficienza' />\n";
$table.="</form>";

echo $tableConfig;
echo $title;
echo $table;

require_once("__includes/COMMON_sleepForgEdit.php");
?>