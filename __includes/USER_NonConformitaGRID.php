<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;


$sql="SELECT * FROM core_impianti_nc WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1;";
$FEDIT->SDBRead($sql,"DbRecordset");

# � la prima volta che accedo
if($FEDIT->DbRecsNum==0){
	$sql="SELECT ID_NC, default_ID_NCL FROM lov_nc";
	$FEDIT->SDBRead($sql,"DbRecordSet");
	for($i=0;$i<count($FEDIT->DbRecordSet);$i++){
		$setup ="INSERT INTO core_impianti_nc VALUES ('";
		$setup.=$SOGER->UserData['core_usersID_IMP']."', ".$FEDIT->DbRecordSet[$i]['ID_NC'].", ";
		$setup.=$FEDIT->DbRecordSet[$i]['default_ID_NCL'].", ";
		switch($SOGER->UserData['workmode']){
			case "produttore":
				$setup.="1,0,0,0";
				break;
			case "trasportatore":
				$setup.="0,1,0,0";
				break;
			case "destinatario":
				$setup.="0,0,1,0";
				break;
			case "intermediario":
				$setup.="0,0,0,1";
				breeak;
			}
		$setup.=" );";
		$FEDIT->SDBWrite($setup,true,false);
		}
	}



echo "<div class=\"FGEDataGridTitle\" style=\"margin-bottom:65px;\"><div>Gestione procedure � Non conformit�</div></div>";
if($SOGER->AppLocation!="login" && $SOGER->AppLocation!="login_documents" && count($SOGER->AppNavDIVs)>0) {
	$tmp .= "<div id=\"NavDiv\">\n";
	$tmp .= "<!-- NAV DIVs BEGIN -->\n[NAV_STUCTURE]\n<!-- NAV DIVs END -->\n";
	$SOGER->DIVmake_Navigation();
	$tmp .= "</div>\n";
	echo $tmp;
	}


# Elenco dei rifiuti
$sql ="SELECT user_schede_rifiuti.ID_RIF, user_schede_rifiuti.ID_FONTE_RIF, descrizione, user_schede_rifiuti.pericoloso, et_approved, et_approved_start, et_approved_end, user_schede_rifiuti.ID_RIFTYPE, ";
$sql.="CAR_Analisi_approved, CAR_Analisi_approved_start, CAR_Analisi_approved_end, CAR_AnalisiNecessaria, CAR_NumDocumento, lov_cer.COD_CER, lov_stato_fisico.description as stato_fisico, ID_RIFPROD_F,";
$sql.="lov_tipo_rifiuto.description as tipo_rifiuto, sk_approved, sk_approved_start, sk_approved_end, CAR_prep_pericolosi, et_comp_per, CAR_approved, CAR_approved_start, CAR_approved_end, user_schede_rifiuti_deposito.eseguo_carico_automatico, user_schede_rifiuti_deposito.ID_CONT, user_schede_sicurezza.ID_SK ";
$sql.="FROM user_schede_rifiuti ";
$sql.="JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
$sql.="JOIN lov_stato_fisico ON user_schede_rifiuti.ID_SF=lov_stato_fisico.ID_SF ";
$sql.="LEFT JOIN user_schede_rifiuti_deposito ON user_schede_rifiuti.ID_RIF=user_schede_rifiuti_deposito.ID_RIF ";
$sql.="LEFT JOIN user_schede_sicurezza ON user_schede_rifiuti.ID_RIF=user_schede_sicurezza.ID_RIF ";
$sql.="LEFT JOIN lov_tipo_rifiuto ON user_schede_rifiuti.ID_RIFTYPE=lov_tipo_rifiuto.ID_RIFTYPE ";
$sql.="WHERE user_schede_rifiuti.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND user_schede_rifiuti.".$SOGER->UserData['workmode']."=1 ";
$sql.="AND user_schede_rifiuti.approved=1 ORDER BY COD_CER ASC, descrizione ASC ";
$FEDIT->SDBRead($sql,"DbRecordset");

//die(var_dump($FEDIT->DbRecordset));

for($r=0;$r<count($FEDIT->DbRecordset);$r++){
	$gotAlert=false;
	$alertRif ="<div class=\"nc rifRow\" id=\"RIF_".$FEDIT->DbRecordset[$r]['ID_RIF']."\">";
		$alertRif.="<div class=\"Col CER\">".$FEDIT->DbRecordset[$r]['COD_CER']."</div>\n";
		$alertRif.="<div class=\"Col DESC\">".$FEDIT->DbRecordset[$r]['descrizione']."</div>\n";
		$alertRif.="<div class=\"Col SF\">".$FEDIT->DbRecordset[$r]['stato_fisico']."</div>\n";
	$alertRif.="</div><br />\n";
	
	## ETICHETTA	
		## DOCUMENTO NON APPROVATO		
		$idRifType=array(2,4,5);
		$esito ="Rifiuto ";
		if($FEDIT->DbRecordset[$r]['pericoloso']==0){	
			$esito.="non pericoloso ";
			if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( non � miscela, da reazione o lega ) ";
				$ID_NC=31;
				}
			if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( miscela, da reazione o lega ) ";
				$ID_NC=32;
				}			
			}
		if($FEDIT->DbRecordset[$r]['pericoloso']==1){
			$esito.="pericoloso ";
			if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( non � miscela, da reazione o lega ) ";
				$ID_NC=29;
				}
			if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( miscela, da reazione o lega ) ";
				$ID_NC=30;
				}			
			}

		if($FEDIT->DbRecordset[$r]['et_approved']==0){
			$esito.="- l' etichetta non risulta approvata";
			$sql ="SELECT description, icon FROM lov_nc_level ";
			$sql.="JOIN core_impianti_nc ON core_impianti_nc.ID_NCL=lov_nc_level.ID_NCL ";
			$sql.="WHERE ID_NC=".$ID_NC." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
			$FEDIT->SdbRead($sql,"NCLevel");
			$level	= $FEDIT->NCLevel[0]['description'];
			$icon	= $FEDIT->NCLevel[0]['icon'];
			$gotAlert=true;
			if($FEDIT->DbRecordset[$r]['pericoloso']==1) $ico="etichetta21.gif"; else $ico="etichetta20.gif";
			$alertRif.="<div class=\"nc ncRow\">";
			$alertRif.="<div class=\"Col Emt\"><img src=\"__css/".$ico."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
			$alertRif.="<div class=\"Col Ico\"><img src=\"__css/".$icon."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
			$alertRif.="<div class=\"Col Type\">".$level.": </div>";
			$alertRif.="<div class=\"Col Text\">".$esito."</div>";
			$alertRif.="</div><br />\n";
			}

		## APPROVAZIONE SCADUTA
		$idRifType=array(2,4,5);
		$esito ="Rifiuto ";
		if($FEDIT->DbRecordset[$r]['pericoloso']==0){	
			$esito.="non pericoloso ";
			if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( non � miscela, da reazione o lega ) ";
				$ID_NC=47;
				}
			if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( miscela, da reazione o lega ) ";
				$ID_NC=48;
				}			
			}
		if($FEDIT->DbRecordset[$r]['pericoloso']==1){
			$esito.="pericoloso ";
			if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( non � miscela, da reazione o lega ) ";
				$ID_NC=45;
				}
			if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( miscela, da reazione o lega ) ";
				$ID_NC=46;
				}			
			}

		if($FEDIT->DbRecordset[$r]['et_approved']==1){
			list($y,$m,$d)=explode("-",$FEDIT->DbRecordset[$r]['et_approved_start']);
			$scadenza=$FEDIT->DbRecordset[$r]['et_approved_end'];
			if(date('Y-m-d')>$scadenza){
				$esito.="- l'approvazione dell' etichetta � scaduta";
				$sql ="SELECT description, icon FROM lov_nc_level ";
				$sql.="JOIN core_impianti_nc ON core_impianti_nc.ID_NCL=lov_nc_level.ID_NCL ";
				$sql.="WHERE ID_NC=".$ID_NC." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
				$FEDIT->SdbRead($sql,"NCLevel");
				$level	= $FEDIT->NCLevel[0]['description'];
				$icon	= $FEDIT->NCLevel[0]['icon'];
				$gotAlert=true;
				if($FEDIT->DbRecordset[$r]['pericoloso']==1) $ico="etichetta21.gif"; else $ico="etichetta20.gif";
				$alertRif.="<div class=\"nc ncRow\">";
				$alertRif.="<div class=\"Col Emt\"><img src=\"__css/".$ico."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Ico\"><img src=\"__css/".$icon."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Type\">".$level.": </div>";
				$alertRif.="<div class=\"Col Text\">".$esito."</div>";
				$alertRif.="</div><br />\n";				
				}
			}

			








	## ANALISI ( se CAR_AnalisiNecessaria==1 )
		
		## DOCUMENTO NON APPROVATO		
		$idRifType=array(2,4,5);
		$esito ="Rifiuto ";
		if($FEDIT->DbRecordset[$r]['pericoloso']==0){	
			$esito.="non pericoloso ";
			if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( non � miscela, da reazione o lega ) ";
				$ID_NC=39;
				}
			if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( miscela, da reazione o lega ) ";
				$ID_NC=40;
				}			
			}
		if($FEDIT->DbRecordset[$r]['pericoloso']==1){
			$esito.="pericoloso ";
			if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( non � miscela, da reazione o lega ) ";
				$ID_NC=37;
				}
			if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( miscela, da reazione o lega ) ";
				$ID_NC=38;
				}			
			}

		if($FEDIT->DbRecordset[$r]['CAR_Analisi_approved']==0 AND file_exists('__upload/analisi/'.$FEDIT->DbRecordset[$r]["ID_RIF"].'.pdf')){
			$esito.="- l' analisi di laboratorio non risulta approvata";
			$sql ="SELECT description, icon FROM lov_nc_level ";
			$sql.="JOIN core_impianti_nc ON core_impianti_nc.ID_NCL=lov_nc_level.ID_NCL ";
			$sql.="WHERE ID_NC=".$ID_NC." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
			$FEDIT->SdbRead($sql,"NCLevel");
			$level	= $FEDIT->NCLevel[0]['description'];
			$icon	= $FEDIT->NCLevel[0]['icon'];
			$gotAlert=true;
			$alertRif.="<div class=\"nc ncRow\">";
			$alertRif.="<div class=\"Col Emt\"><img src=\"__css/analisi.png\" alt=\"".$level."\" title=\"".$level."\" /></div>";
			$alertRif.="<div class=\"Col Ico\"><img src=\"__css/".$icon."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
			$alertRif.="<div class=\"Col Type\">".$level.": </div>";
			$alertRif.="<div class=\"Col Text\">".$esito."</div>";
			$alertRif.="</div><br />\n";
			}

		## APPROVAZIONE SCADUTA
		$idRifType=array(2,4,5);
		$esito ="Rifiuto ";
		if($FEDIT->DbRecordset[$r]['pericoloso']==0){	
			$esito.="non pericoloso ";
			if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( non � miscela, da reazione o lega ) ";
				$ID_NC=55;
				}
			if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( miscela, da reazione o lega ) ";
				$ID_NC=56;
				}			
			}
		if($FEDIT->DbRecordset[$r]['pericoloso']==1){
			$esito.="pericoloso ";
			if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( non � miscela, da reazione o lega ) ";
				$ID_NC=53;
				}
			if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( miscela, da reazione o lega ) ";
				$ID_NC=54;
				}			
			}

		if($FEDIT->DbRecordset[$r]['CAR_Analisi_approved']==1 AND file_exists('__upload/analisi/'.$FEDIT->DbRecordset[$r]["ID_RIF"].'.pdf')){
			list($y,$m,$d)=explode("-",$FEDIT->DbRecordset[$r]['CAR_Analisi_approved_start']);
			$scadenza=$FEDIT->DbRecordset[$r]['CAR_Analisi_approved_end'];
			if(date('Y-m-d')>$scadenza){
				$esito.="- l'approvazione dell' analisi di laboratorio � scaduta";
				$sql ="SELECT description, icon FROM lov_nc_level ";
				$sql.="JOIN core_impianti_nc ON core_impianti_nc.ID_NCL=lov_nc_level.ID_NCL ";
				$sql.="WHERE ID_NC=".$ID_NC." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
				$FEDIT->SdbRead($sql,"NCLevel");
				$level	= $FEDIT->NCLevel[0]['description'];
				$icon	= $FEDIT->NCLevel[0]['icon'];
				$gotAlert=true;
				$alertRif.="<div class=\"nc ncRow\">";
				$alertRif.="<div class=\"Col Emt\"><img src=\"__css/analisi.png\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Ico\"><img src=\"__css/".$icon."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Type\">".$level.": </div>";
				$alertRif.="<div class=\"Col Text\">".$esito."</div>";
				$alertRif.="</div><br />\n";				
				}
			}

		## DOCUMENTO NON CARICATO SU SERVER
		$idRifType=array(2,4,5);
		$esito ="Rifiuto ";
		if($FEDIT->DbRecordset[$r]['pericoloso']==0){	
			$esito.="non pericoloso ";
			if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( non � miscela, da reazione o lega ) ";
				$ID_NC=23;
				}
			if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( miscela, da reazione o lega ) ";
				$ID_NC=24;
				}			
			}
		if($FEDIT->DbRecordset[$r]['pericoloso']==1){
			$esito.="pericoloso ";
			if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( non � miscela, da reazione o lega ) ";
				$ID_NC=21;
				}
			if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
				$esito.="( miscela, da reazione o lega ) ";
				$ID_NC=22;
				}			
			}

		if(!file_exists('__upload/analisi/'.$FEDIT->DbRecordset[$r]["ID_RIF"].'.pdf')){
			$esito.="- l' analisi del rifiuto non � caricata su server";
			$sql ="SELECT description, icon FROM lov_nc_level ";
			$sql.="JOIN core_impianti_nc ON core_impianti_nc.ID_NCL=lov_nc_level.ID_NCL ";
			$sql.="WHERE ID_NC=".$ID_NC." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
			$FEDIT->SdbRead($sql,"NCLevel");
			$level	= $FEDIT->NCLevel[0]['description'];
			$icon	= $FEDIT->NCLevel[0]['icon'];
			$gotAlert=true;
			$alertRif.="<div class=\"nc ncRow\">";
			$alertRif.="<div class=\"Col Emt\"><img src=\"__css/analisi.png\" alt=\"".$level."\" title=\"".$level."\" /></div>";
			$alertRif.="<div class=\"Col Ico\"><img src=\"__css/".$icon."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
			$alertRif.="<div class=\"Col Type\">".$level.": </div>";
			$alertRif.="<div class=\"Col Text\">".$esito."</div>";
			$alertRif.="</div><br />\n";					
			}		

				






		## PROCEDURE DI SICUREZZA
	
			## DOCUMENTO NON APPROVATO		
			$idRifType=array(2,4,5);
			$esito ="Rifiuto ";
			if($FEDIT->DbRecordset[$r]['pericoloso']==0){	
				$esito.="non pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=35;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=36;
					}			
				}
			if($FEDIT->DbRecordset[$r]['pericoloso']==1){
				$esito.="pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=33;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=34;
					}			
				}

			if($FEDIT->DbRecordset[$r]['sk_approved']==0){
				$esito.="- la procedura di sicurezza non risulta approvata";
				$sql ="SELECT description, icon FROM lov_nc_level ";
				$sql.="JOIN core_impianti_nc ON core_impianti_nc.ID_NCL=lov_nc_level.ID_NCL ";
				$sql.="WHERE ID_NC=".$ID_NC." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
				$FEDIT->SdbRead($sql,"NCLevel");
				$level	= $FEDIT->NCLevel[0]['description'];
				$icon	= $FEDIT->NCLevel[0]['icon'];
				$gotAlert=true;
				$alertRif.="<div class=\"nc ncRow\">";
				$alertRif.="<div class=\"Col Emt\"><img src=\"__css/elmetto.png\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Ico\"><img src=\"__css/".$icon."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Type\">".$level.": </div>";
				$alertRif.="<div class=\"Col Text\">".$esito."</div>";
				$alertRif.="</div><br />\n";
				}

		## APPROVAZIONE SCADUTA
			$idRifType=array(2,4,5);
			$esito ="Rifiuto ";
			if($FEDIT->DbRecordset[$r]['pericoloso']==0){	
				$esito.="non pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=51;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=52;
					}			
				}
			if($FEDIT->DbRecordset[$r]['pericoloso']==1){
				$esito.="pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=49;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=50;
					}			
				}

			if($FEDIT->DbRecordset[$r]['sk_approved']==1){
				list($y,$m,$d)=explode("-",$FEDIT->DbRecordset[$r]['sk_approved_start']);
				$scadenza=$FEDIT->DbRecordset[$r]['sk_approved_end'];
				if(date('Y-m-d')>$scadenza){
					$esito.="- l'approvazione della procedura di sicurezza � scaduta";
					$sql ="SELECT description, icon FROM lov_nc_level ";
					$sql.="JOIN core_impianti_nc ON core_impianti_nc.ID_NCL=lov_nc_level.ID_NCL ";
					$sql.="WHERE ID_NC=".$ID_NC." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
					$FEDIT->SdbRead($sql,"NCLevel");
					$level	= $FEDIT->NCLevel[0]['description'];
					$icon	= $FEDIT->NCLevel[0]['icon'];
					$gotAlert=true;
					$alertRif.="<div class=\"nc ncRow\">";
					$alertRif.="<div class=\"Col Emt\"><img src=\"__css/elmetto.png\" alt=\"".$level."\" title=\"".$level."\" /></div>";
					$alertRif.="<div class=\"Col Ico\"><img src=\"__css/".$icon."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
					$alertRif.="<div class=\"Col Type\">".$level.": </div>";
					$alertRif.="<div class=\"Col Text\">".$esito."</div>";
					$alertRif.="</div><br />\n";				
					}
				}


			## DOCUMENTO NON CARICATO SU SERVER
			$idRifType=array(2,4,5);
			$esito ="Rifiuto ";
			if($FEDIT->DbRecordset[$r]['pericoloso']==0){	
				$esito.="non pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=19;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=20;
					}			
				}
			if($FEDIT->DbRecordset[$r]['pericoloso']==1){
				$esito.="pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=17;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=18;
					}			
				}

			if( !file_exists('__upload/sicurezza/'.$FEDIT->DbRecordset[$r]["ID_RIF"].'.pdf') and !file_exists('__upload/sicurezza/'.$FEDIT->DbRecordset[$r]["ID_RIF"].'.rtf') and !file_exists('__upload/sicurezza/'.$FEDIT->DbRecordset[$r]["ID_RIF"].'.doc') ){
				$esito.="- la procedura di sicurezza non � caricata su server";
				$sql ="SELECT description, icon FROM lov_nc_level ";
				$sql.="JOIN core_impianti_nc ON core_impianti_nc.ID_NCL=lov_nc_level.ID_NCL ";
				$sql.="WHERE ID_NC=".$ID_NC." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
				$FEDIT->SdbRead($sql,"NCLevel");
				$level	= $FEDIT->NCLevel[0]['description'];
				$icon	= $FEDIT->NCLevel[0]['icon'];
				$gotAlert=true;
				$alertRif.="<div class=\"nc ncRow\">";
				$alertRif.="<div class=\"Col Emt\"><img src=\"__css/elmetto.png\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Ico\"><img src=\"__css/".$icon."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Type\">".$level.": </div>";
				$alertRif.="<div class=\"Col Text\">".$esito."</div>";
				$alertRif.="</div><br />\n";					
				}		

			## DOCUMENTO NON REALIZZATO
			$idRifType=array(2,4,5);
			$esito ="Rifiuto ";
			if($FEDIT->DbRecordset[$r]['pericoloso']==0){	
				$esito.="non pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=11;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=12;
					}			
				}
			if($FEDIT->DbRecordset[$r]['pericoloso']==1){
				$esito.="pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=9;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=10;
					}			
				}

			if(is_null($FEDIT->DbRecordset[$r]['ID_SK'])){
				$esito.="- la procedura di sicurezza non � stata creata";
				$sql ="SELECT description, icon FROM lov_nc_level ";
				$sql.="JOIN core_impianti_nc ON core_impianti_nc.ID_NCL=lov_nc_level.ID_NCL ";
				$sql.="WHERE ID_NC=".$ID_NC." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
				$FEDIT->SdbRead($sql,"NCLevel");
				$level	= $FEDIT->NCLevel[0]['description'];
				$icon	= $FEDIT->NCLevel[0]['icon'];
				$gotAlert=true;
				$alertRif.="<div class=\"nc ncRow\">";
				$alertRif.="<div class=\"Col Emt\"><img src=\"__css/elmetto.png\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Ico\"><img src=\"__css/".$icon."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Type\">".$level.": </div>";
				$alertRif.="<div class=\"Col Text\">".$esito."</div>";
				$alertRif.="</div><br />\n";
				}



				
		## CARATTERIZZAZIONI

			## DOCUMENTO NON APPROVATO		
			$idRifType=array(2,4,5);
			$esito ="Rifiuto ";
			if($FEDIT->DbRecordset[$r]['pericoloso']==0){	
				$esito.="non pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=27;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=28;
					}			
				}
			if($FEDIT->DbRecordset[$r]['pericoloso']==1){
				$esito.="pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=25;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=26;
					}			
				}

			if($FEDIT->DbRecordset[$r]['CAR_approved']==0){
				$esito.="- la scheda di caratterizzazione non risulta approvata";
				$sql ="SELECT description, icon FROM lov_nc_level ";
				$sql.="JOIN core_impianti_nc ON core_impianti_nc.ID_NCL=lov_nc_level.ID_NCL ";
				$sql.="WHERE ID_NC=".$ID_NC." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
				$FEDIT->SdbRead($sql,"NCLevel");
				$level	= $FEDIT->NCLevel[0]['description'];
				$icon	= $FEDIT->NCLevel[0]['icon'];
				$gotAlert=true;
				$alertRif.="<div class=\"nc ncRow\">";
				$alertRif.="<div class=\"Col Emt\"><img src=\"__css/camera.png\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Ico\"><img src=\"__css/".$icon."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Type\">".$level.": </div>";
				$alertRif.="<div class=\"Col Text\">".$esito."</div>";
				$alertRif.="</div><br />\n";
				}

		## APPROVAZIONE SCADUTA
			$idRifType=array(2,4,5);
			$esito ="Rifiuto ";
			if($FEDIT->DbRecordset[$r]['pericoloso']==0){	
				$esito.="non pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=43;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=44;
					}
				}
			if($FEDIT->DbRecordset[$r]['pericoloso']==1){
				$esito.="pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=41;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=42;
					}
				}

			if($FEDIT->DbRecordset[$r]['CAR_approved']==1){
				list($y,$m,$d)=explode("-",$FEDIT->DbRecordset[$r]['CAR_approved_start']);
				$scadenza=$FEDIT->DbRecordset[$r]['CAR_approved_end'];
				if(date('Y-m-d')>$scadenza){
					$esito.="- l'approvazione della scheda di caratterizzazione � scaduta";
					$sql ="SELECT description, icon FROM lov_nc_level ";
					$sql.="JOIN core_impianti_nc ON core_impianti_nc.ID_NCL=lov_nc_level.ID_NCL ";
					$sql.="WHERE ID_NC=".$ID_NC." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
					$FEDIT->SdbRead($sql,"NCLevel");
					$level	= $FEDIT->NCLevel[0]['description'];
					$icon	= $FEDIT->NCLevel[0]['icon'];
					$gotAlert=true;
					$alertRif.="<div class=\"nc ncRow\">";
					$alertRif.="<div class=\"Col Emt\"><img src=\"__css/camera.png\" alt=\"".$level."\" title=\"".$level."\" /></div>";
					$alertRif.="<div class=\"Col Ico\"><img src=\"__css/".$icon."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
					$alertRif.="<div class=\"Col Type\">".$level.": </div>";
					$alertRif.="<div class=\"Col Text\">".$esito."</div>";
					$alertRif.="</div><br />\n";				
					}
				}


			## DOCUMENTO NON CARICATO SU SERVER
			$idRifType=array(2,4,5);
			$esito ="Rifiuto ";
			if($FEDIT->DbRecordset[$r]['pericoloso']==0){	
				$esito.="non pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=15;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=16;
					}			
				}
			if($FEDIT->DbRecordset[$r]['pericoloso']==1){
				$esito.="pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=13;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=14;
					}			
				}

			if( !file_exists('__upload/caratterizzazioni/'.$FEDIT->DbRecordset[$r]["ID_RIF"].'.pdf') and !file_exists('__upload/caratterizzazioni/'.$FEDIT->DbRecordset[$r]["ID_RIF"].'.rtf') and !file_exists('__upload/caratterizzazioni/'.$FEDIT->DbRecordset[$r]["ID_RIF"].'.doc') ){
				$esito.="- la scheda di caratterizzazione non � caricata su server";
				$sql ="SELECT description, icon FROM lov_nc_level ";
				$sql.="JOIN core_impianti_nc ON core_impianti_nc.ID_NCL=lov_nc_level.ID_NCL ";
				$sql.="WHERE ID_NC=".$ID_NC." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
				$FEDIT->SdbRead($sql,"NCLevel");
				$level	= $FEDIT->NCLevel[0]['description'];
				$icon	= $FEDIT->NCLevel[0]['icon'];
				$gotAlert=true;
				$alertRif.="<div class=\"nc ncRow\">";
				$alertRif.="<div class=\"Col Emt\"><img src=\"__css/camera.png\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Ico\"><img src=\"__css/".$icon."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Type\">".$level.": </div>";
				$alertRif.="<div class=\"Col Text\">".$esito."</div>";
				$alertRif.="</div><br />\n";					
				}		

			## DOCUMENTO NON REALIZZATO
			$idRifType=array(2,4,5);
			$esito ="Rifiuto ";
			if($FEDIT->DbRecordset[$r]['pericoloso']==0){	
				$esito.="non pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=3;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=4;
					}			
				}
			if($FEDIT->DbRecordset[$r]['pericoloso']==1){
				$esito.="pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=1;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=2;
					}			
				}

			if($FEDIT->DbRecordset[$r]['ID_FONTE_RIF']==0){
				$esito.="- la scheda di caratterizzazione non � stata creata";
				$sql ="SELECT description, icon FROM lov_nc_level ";
				$sql.="JOIN core_impianti_nc ON core_impianti_nc.ID_NCL=lov_nc_level.ID_NCL ";
				$sql.="WHERE ID_NC=".$ID_NC." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
				$FEDIT->SdbRead($sql,"NCLevel");
				$level	= $FEDIT->NCLevel[0]['description'];
				$icon	= $FEDIT->NCLevel[0]['icon'];
				$gotAlert=true;
				$alertRif.="<div class=\"nc ncRow\">";
				$alertRif.="<div class=\"Col Emt\"><img src=\"__css/camera.png\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Ico\"><img src=\"__css/".$icon."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Type\">".$level.": </div>";
				$alertRif.="<div class=\"Col Text\">".$esito."</div>";
				$alertRif.="</div><br />\n";
				}


			## hai indicato che si usano sostanze pericolose 1907/2006 o 1272/2008 ma non le hai definite
			
			$idRifType=array(2,4,5);
			$esito ="Rifiuto ";
			if($FEDIT->DbRecordset[$r]['pericoloso']==0){	
				$esito.="non pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=59;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=60;
					}			
				}
			if($FEDIT->DbRecordset[$r]['pericoloso']==1){
				$esito.="pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=57;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=58;
					}			
				}
				
			if($FEDIT->DbRecordset[$r]['CAR_prep_pericolosi']==1 AND (trim($FEDIT->DbRecordset[$r]['et_comp_per'])==trim('nessuna sostanza pericolosa') || trim($FEDIT->DbRecordset[$r]['et_comp_per'])=='') ){
				$esito.="- nel ciclo produttivo si utilizzano <b>sostanze classificate pericolosi ai sensi del Reg. 1907/2006 o 1272/2008</b>, ma tali sostanze non sono state indicate";
				$sql ="SELECT description, icon FROM lov_nc_level ";
				$sql.="JOIN core_impianti_nc ON core_impianti_nc.ID_NCL=lov_nc_level.ID_NCL ";
				$sql.="WHERE ID_NC=".$ID_NC." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
				$FEDIT->SdbRead($sql,"NCLevel");
				$level	= $FEDIT->NCLevel[0]['description'];
				$icon	= $FEDIT->NCLevel[0]['icon'];
				$gotAlert=true;
				$alertRif.="<div class=\"nc ncRow\">";
				$alertRif.="<div class=\"Col Emt\"><img src=\"__css/camera.png\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Ico\"><img src=\"__css/".$icon."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Type\">".$level.": </div>";
				$alertRif.="<div class=\"Col Text\">".$esito."</div>";
				$alertRif.="</div><br />\n";
				}






			## produzione regolare ma non sono impostati carichi automatici

			$idRifType=array(2,4,5);
			$esito ="Rifiuto ";
			if($FEDIT->DbRecordset[$r]['pericoloso']==0){	
				$esito.="non pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=63;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=64;
					}			
				}
			if($FEDIT->DbRecordset[$r]['pericoloso']==1){
				$esito.="pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=61;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=62;
					}			
				}
				
			if($FEDIT->DbRecordset[$r]['ID_RIFPROD_F']==1 AND $FEDIT->DbRecordset[$r]['eseguo_carico_automatico']==0){
				$esito.="- il rifiuto � prodotto con frequenza regolare ma non sono definiti i carichi automatici";
				$sql ="SELECT description, icon FROM lov_nc_level ";
				$sql.="JOIN core_impianti_nc ON core_impianti_nc.ID_NCL=lov_nc_level.ID_NCL ";
				$sql.="WHERE ID_NC=".$ID_NC." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
				$FEDIT->SdbRead($sql,"NCLevel");
				$level	= $FEDIT->NCLevel[0]['description'];
				$icon	= $FEDIT->NCLevel[0]['icon'];
				$gotAlert=true;
				$alertRif.="<div class=\"nc ncRow\">";
				$alertRif.="<div class=\"Col Emt\"><img src=\"__css/camera.png\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Ico\"><img src=\"__css/".$icon."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Type\">".$level.": </div>";
				$alertRif.="<div class=\"Col Text\">".$esito."</div>";
				$alertRif.="</div><br />\n";
				}
			

			## non hai indicato il contenitore in gestione deposito

			$idRifType=array(2,4,5);
			$esito ="Rifiuto ";
			if($FEDIT->DbRecordset[$r]['pericoloso']==0){	
				$esito.="non pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=67;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=68;
					}			
				}
			if($FEDIT->DbRecordset[$r]['pericoloso']==1){
				$esito.="pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=65;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=66;
					}			
				}

			if( is_null($FEDIT->DbRecordset[$r]['ID_CONT']) | $FEDIT->DbRecordset[$r]['ID_CONT']==0 ){
				$esito.="- il rifiuto � privo dell' indicazione del contenitore nella \"Gestione deposito\"";
				$sql ="SELECT description, icon FROM lov_nc_level ";
				$sql.="JOIN core_impianti_nc ON core_impianti_nc.ID_NCL=lov_nc_level.ID_NCL ";
				$sql.="WHERE ID_NC=".$ID_NC." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
				$FEDIT->SdbRead($sql,"NCLevel");
				$level	= $FEDIT->NCLevel[0]['description'];
				$icon	= $FEDIT->NCLevel[0]['icon'];
				$gotAlert=true;
				$alertRif.="<div class=\"nc ncRow\">";
				$alertRif.="<div class=\"Col Emt\"><img src=\"__css/camera.png\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Ico\"><img src=\"__css/".$icon."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Type\">".$level.": </div>";
				$alertRif.="<div class=\"Col Text\">".$esito."</div>";
				$alertRif.="</div><br />\n";
				}

			
			## non hai indicato il numero di referto analitico ( e l'analisi � necessaria )

			$idRifType=array(2,4,5);
			$esito ="Rifiuto ";
			if($FEDIT->DbRecordset[$r]['pericoloso']==0){	
				$esito.="non pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=71;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=72;
					}			
				}
			if($FEDIT->DbRecordset[$r]['pericoloso']==1){
				$esito.="pericoloso ";
				if(!in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( non � miscela, da reazione o lega ) ";
					$ID_NC=69;
					}
				if(in_array($FEDIT->DbRecordset[$r]['ID_RIFTYPE'], $idRifType)){
					$esito.="( miscela, da reazione o lega ) ";
					$ID_NC=70;
					}			
				}

			if( (is_null($FEDIT->DbRecordset[$r]['CAR_NumDocumento']) | $FEDIT->DbRecordset[$r]['CAR_NumDocumento']=='') AND $FEDIT->DbRecordset[$r]['CAR_AnalisiNecessaria']==1 ){
				$esito.="- il rifiuto � privo dell' indicazione del numero di \"Referto analitico\"";
				$sql ="SELECT description, icon FROM lov_nc_level ";
				$sql.="JOIN core_impianti_nc ON core_impianti_nc.ID_NCL=lov_nc_level.ID_NCL ";
				$sql.="WHERE ID_NC=".$ID_NC." AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1";
				$FEDIT->SdbRead($sql,"NCLevel");
				$level	= $FEDIT->NCLevel[0]['description'];
				$icon	= $FEDIT->NCLevel[0]['icon'];
				$gotAlert=true;
				$alertRif.="<div class=\"nc ncRow\">";
				$alertRif.="<div class=\"Col Emt\"><img src=\"__css/camera.png\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Ico\"><img src=\"__css/".$icon."\" alt=\"".$level."\" title=\"".$level."\" /></div>";
				$alertRif.="<div class=\"Col Type\">".$level.": </div>";
				$alertRif.="<div class=\"Col Text\">".$esito."</div>";
				$alertRif.="</div><br />\n";
				}
		
						
	if($gotAlert)
		echo $alertRif;	
	}

#
require_once("__includes/COMMON_sleepForgEdit.php");
?>