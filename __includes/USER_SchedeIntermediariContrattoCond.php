<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();

$FEDIT->FGE_UseTables("user_contratti_int_cond");
$FEDIT->FGE_SetFormFields(array("ID_CNT_I", "description","euro","ID_UM_CNT","ID_UIMD"),"user_contratti_int_cond");
$FEDIT->FGE_SetFilter("ID_CNT_I",$_SESSION['ID_CONTRACT'],"user_contratti_int_cond");


if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {

	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_SetValue("description","","user_contratti_int_cond");
	$FEDIT->FGE_SetValue("euro",0,"user_contratti_int_cond");
	$FEDIT->FGE_SetValue("ID_CNT_I",$_SESSION['ID_CONTRACT'],"user_contratti_int_cond");
	$FEDIT->FGE_HideFields("ID_CNT_I","user_contratti_int_cond");
	
	#
	$FEDIT->FGE_LookUpDefault("ID_UM_CNT","user_contratti_int_cond");
	#
	$FEDIT->FGE_LookUpCFG("ID_UIMD","user_contratti_int_cond");
	$FEDIT->FGE_UseTables("user_aziende_destinatari", "user_impianti_destinatari", "lov_comuni_istat");
	$FEDIT->FGE_SetSelectFields(array("description","ID_AZD"),"user_aziende_destinatari");
	$FEDIT->FGE_SetSelectFields(array("description","ID_AZD"),"user_impianti_destinatari");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData['core_usersID_IMP'],"user_aziende_destinatari");
	$FEDIT->FGE_SetFilter($SOGER->UserData['workmode'],1,"user_aziende_destinatari");
	$FEDIT->FGE_HideFields("ID_AZD","user_impianti_destinatari");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea condizione contrattuale");

} else {

	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_UM_CNT","user_contratti_int_cond");
	#
	$FEDIT->FGE_LookUpCFG("ID_UIMD","user_contratti_int_cond");
	$FEDIT->FGE_UseTables("user_aziende_destinatari", "user_impianti_destinatari", "lov_comuni_istat");
	$FEDIT->FGE_SetSelectFields(array("description","ID_AZD"),"user_aziende_destinatari");
	$FEDIT->FGE_SetSelectFields(array("description","ID_AZD"),"user_impianti_destinatari");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData['core_usersID_IMP'],"user_aziende_destinatari");
	$FEDIT->FGE_SetFilter($SOGER->UserData['workmode'],1,"user_aziende_destinatari");
	$FEDIT->FGE_HideFields("ID_AZD","user_impianti_destinatari");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#
	$FEDIT->FGE_SetValue("ID_CNT_I",$_SESSION['ID_CONTRACT'],"user_contratti_int_cond");
	$FEDIT->FGE_HideFields("ID_CNT_I","user_contratti_int_cond");

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica condizione contrattuale");
}
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>