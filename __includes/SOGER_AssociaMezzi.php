<?php
require_once("__includes/COMMON_ForgEditClassFiles.php");
require("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();

if($_GET["table"]=="user_automezzi") {
	$nome = "l'automezzo";
	$FEDIT->FGE_UseTables(array("user_automezzi","user_autorizzazioni_trasp","user_aziende_trasportatori","lov_cer","user_schede_rifiuti","user_impianti_trasportatori"));
	$FEDIT->FGE_SetSelectFields(array("num_aut"),"user_autorizzazioni_trasp");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetSelectFields(array("descrizione"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("description"),"user_impianti_trasportatori");
	$FEDIT->FGE_SetSelectFields(array("ID_AUTO","description","adr","ID_MZ_TRA"),"user_automezzi");
	#
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_SetFilter("ID_AUTO",$_GET["filter"],"user_automezzi");
	$FEDIT->FGE_LookUpDefault("ID_MZ_TRA","user_automezzi");
	#
	echo($FEDIT->FGE_DataGrid("Automezzo","__scripts/FGE_DataGridEdit.php","-----",false));
} else {
	$nome = "il rimorchio";
	$FEDIT->FGE_UseTables(array("user_rimorchi","user_autorizzazioni_trasp","user_aziende_trasportatori","lov_cer","user_schede_rifiuti","user_impianti_trasportatori"));
	$FEDIT->FGE_SetSelectFields(array("num_aut"),"user_autorizzazioni_trasp");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetSelectFields(array("descrizione"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("description"),"user_impianti_trasportatori");
	$FEDIT->FGE_SetSelectFields(array("ID_RMK","description","adr","ID_MZ_RMK"),"user_rimorchi");
	#
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_SetFilter("ID_RMK",$_GET["filter"],"user_rimorchi");
	$FEDIT->FGE_LookUpDefault("ID_MZ_RMK","user_rimorchi");
	#
	echo($FEDIT->FGE_DataGrid("Associa $nome...","__scripts/FGE_DataGridEdit.php","-----",false));	
}
#
#	CREA LISTA ESCLUSIONI
#
if($_GET["table"]=="user_automezzi") {
	$sql2 = " SELECT * FROM user_automezzi JOIN user_autorizzazioni_trasp ON user_autorizzazioni_trasp.ID_AUTHT=user_automezzi.ID_AUTHT WHERE user_automezzi.description='" . urldecode($_GET["TGN"]) . "'";
	$sql2 .= " AND user_autorizzazioni_trasp.ID_UIMT=" . $_GET["ID_IMP"];
} else {
	$sql2 = " SELECT * FROM user_rimorchi JOIN user_autorizzazioni_trasp ON user_autorizzazioni_trasp.ID_AUTHT=user_rimorchi.ID_AUTHT WHERE user_rimorchi.description='" . urldecode($_GET["TGN"]) . "'";
	$sql2 .= " AND user_autorizzazioni_trasp.ID_UIMT=" . $_GET["ID_IMP"];
}
$FEDIT->SDBRead($sql2,"DbRecordSet");

$Ex = array();
foreach($FEDIT->DbRecordSet as $k=>$values) {
	$Ex[] = $values["ID_AUTHT"];
}
require_once("__includes/COMMON_sleepForgEdit.php");
#
#	CARICA LISTA AUTORIZZAZIONI / CER
#
require("__includes/COMMON_wakeForgEditDG.php");

$SQL = " SELECT user_autorizzazioni_trasp.ID_AUTHT,num_aut,rilascio,COD_CER,cod_pro,user_schede_rifiuti.descrizione AS CERdes, user_schede_rifiuti.ID_RIF AS RIF_REF FROM (user_autorizzazioni_trasp";
$SQL .= "  JOIN user_schede_rifiuti ON user_autorizzazioni_trasp.ID_RIF=user_schede_rifiuti.ID_RIF ";
$SQL .= "  JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER) ";
$SQL .= " WHERE ";
//$SQL .= "user_schede_rifiuti.ID_RIF<>'" . $_GET["ID_RIF"] . "' AND ";
$SQL .= "user_autorizzazioni_trasp.ID_UIMT='" . $_GET["ID_IMP"] . "'";

# col group by si impone che per un rifiuto ci sia una sola autorizzazione..
//$SQL .= " group by user_schede_rifiuti.ID_RIF";

$SQL .= " ORDER BY lov_cer.COD_CER, user_schede_rifiuti.descrizione ";

$FEDG->SDbRead($SQL,"DbRecordSet");

	$tmp = "<form name=\"mezziAssoc\" action=\"__scripts/SOGER_AssocMezzi.php\" method=\"post\"><div class=\"FGEDataGridTitle\"><div>...ai Numeri Albo / CER</div></div>";
	$tmp .= "\n<div class=\"FGEDataGridPaging\" id=\"FGE_DG_2\">";
	$tmp .= "<table class=\"FGEDataGridTable\">";
	$tmp .= "<tr>";
	#
	$tmp .= "<th class=\"FGEDataGridTableHeader\">&nbsp;</th>";
	$tmp .= "<th class=\"FGEDataGridTableHeader\">codice cer</th>";
	$tmp .= "<th class=\"FGEDataGridTableHeader\">codice</th>";
	$tmp .= "<th class=\"FGEDataGridTableHeader\">descrizione</th>";
	$tmp .= "<th class=\"FGEDataGridTableHeader\">numero albo</th>";
	$tmp .= "<th class=\"FGEDataGridTableHeader\">rilascio</th>";
	$tmp .= "</tr>";
	
	if(isset($FEDG->DbRecordSet)) {
		foreach($FEDG->DbRecordSet as $k=>$data) {
			if(array_search($data["ID_AUTHT"],$Ex)===false) {
			$tmp .= "<tr>";	
			$tmp .= "<td class=\"FGEDataGridTableCell\"><input type=\"checkbox\" CHECKED name=\"Auts[]\" value=\"" . $data["ID_AUTHT"] . "\" /></td>";
			$tmp .= "<td class=\"FGEDataGridTableCell\">" . $data["COD_CER"] . "</td>";
			$tmp .= "<td class=\"FGEDataGridTableCell\">" . $data["cod_pro"] . "</td>";
			$tmp .= "<td class=\"FGEDataGridTableCell\">" . $data["CERdes"] . "</td>";
			$tmp .= "<td class=\"FGEDataGridTableCell\">" . $data["num_aut"] . "</td>";
			$tmp .= "<td class=\"FGEDataGridTableCell\">".  date("d/m/Y",strtotime($data["rilascio"])) . "</td>";
			$tmp .= "</tr>";		
			}
		}
		$tmp .= "<tr>";	
		$tmp .= "<td class=\"FGEDataGridTableCell\" colspan=\"6\" style=\"text-align: left;\">";
		$tmp .= "<input type=\"button\" class=\"FGEbutton\" onclick=\"if(confirm('Associare il mezzo ai cer selezionati?')) {document.mezziAssoc.submit();}\" value=\"associa ai cer selezionati\"/></td>";
		$tmp .= "</tr>";
	} else {
		$tmp .= "<tr>";	
		$tmp .= "<td class=\"FGEDataGridTableCell\" colspan=\"5\"><br/><b>nessun cer/autorizzazione associabile</b><br/>&nbsp;</td>";
		$tmp .= "</tr>";		
	}
	
	$tmp .= "</table>";
	$tmp .= "<input name=\"source\" type=\"hidden\" value=\"" . $_GET["filter"] . "\"/>";
	$tmp .= "<input name=\"key\" type=\"hidden\" value=\"" . $_GET["pri"] . "\"/>";
	$tmp .= "<input name=\"table\" type=\"hidden\" value=\"" . $_GET["table"] . "\"/>";
	$tmp .= "</div></form>";
	
	echo $tmp;
require_once("__includes/COMMON_sleepForgEditDG.php");
?>
