<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_aziende_produttori");
$FEDIT->FGE_SetFormFields(array("ID_IMP","description","piva","codfisc"),"user_aziende_produttori");
if($SOGER->UserData['workmode']=='destinatario'){
	$FEDIT->FGE_SetFormFields("IscrittoCONAI","user_aziende_produttori");	
	}
$FEDIT->FGE_SetFormFields(array("indirizzo","ID_COM","IN_ITALIA","approved"),"user_aziende_produttori");

$FEDIT->FGE_HideFields(array("IN_ITALIA"),"user_aziende_produttori");	

$FEDIT->FGE_SetTitle("description","Dati societari","user_aziende_produttori");
$FEDIT->FGE_SetTitle("indirizzo","Sede legale","user_aziende_produttori");
$FEDIT->FGE_SetBreak("piva","user_aziende_produttori");

$ProfiloFields = "user_aziende_produttori";
include("SOGER_CampiProfilo.php");

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {
	#
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_COM","user_aziende_produttori",true);
	$FEDIT->FGE_SetValue("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_aziende_produttori");
        $FEDIT->FGE_SetValue("IscrittoCONAI","0","user_aziende_produttori");
	//$FEDIT->FGE_SetValue("idSIS_regCrono",$SOGER->UserData['idSIS_regCrono'],"user_aziende_produttori");
	$FEDIT->FGE_SetValue("approved","1","user_aziende_produttori");
	$FEDIT->FGE_HideFields(array("ID_IMP","produttore","trasportatore","destinatario","intermediario","approved"),"user_aziende_produttori");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea produttore");
} else {
	$_SESSION["FGE_IDs"]["user_aziende_produttori"] = $_GET["filter"];
	#
	//if(isset($_GET["Involved"])) {
	//	$FEDIT->FGE_DisableFields(array("description","piva","codfisc"),"user_aziende_produttori");
	//}	
	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_COM","user_aziende_produttori",true);
	$FEDIT->FGE_HideFields(array("ID_IMP","produttore","trasportatore","destinatario","intermediario","approved"),"user_aziende_produttori");	
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva produttore");
}
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>