<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
global $SOGER;

$TableName="user_movimenti_fiscalizzati";

require("USER_Movimenti_DettaglioNMOV.php");

$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_movimenti_fiscalizzati");
$FEDIT->FGE_SetFormFields(array("NMOV","DTMOV","FKErifCarico"),"user_movimenti_fiscalizzati");

$FEDIT->FGE_SetFormFields(array("ID_AUTO","FKEadrAUT","FKEmzAUT","ID_AUTST","dt_in_trasp","hr_in_trasp","COLLI"),"user_movimenti_fiscalizzati");
$FEDIT->FGE_SetFormFields(array("ID_RMK","FKEadrRMK","FKEmzRMK"),"user_movimenti_fiscalizzati");
$FEDIT->FGE_SetFormFields(array("ID_AZI","percorso","NOTEF","NOTER"),"user_movimenti_fiscalizzati");
#
$FEDIT->FGE_SetFormFields("ID_UIMT","user_movimenti_fiscalizzati");
$FEDIT->FGE_HideFields(array("NMOV","FKErifCarico","ID_UIMT"),"user_movimenti_fiscalizzati");

$FEDIT->FGE_HideFields(array("TIPO","quantita","qta_hidden","tara","pesoL","pesoN","FKEumis","FKESF","FKEpesospecifico","NFORM","DTFORM","adr","QL","ID_IMP_SPECCHIO","ID_OP_RS"),"user_movimenti_fiscalizzati");


#
$FEDIT->FGE_DescribeFields();
#



$autoTRADEST = true;
require("__mov_snipplets/LKUP_Automezzi.php");
require("__mov_snipplets/LKUP_Rimorchi.php");
require("__mov_snipplets/LKUP_Autisti.php");


#
$FEDIT->FGE_LookUpCFG("ID_AZI","user_movimenti_fiscalizzati",false,true);	
$FEDIT->FGE_UseTables(array("user_aziende_intermediari"));
//$FEDIT->FGE_SetSelectFields(array("ID_AZI","ID_IMP","description","produttore","destinatario","trasportatore","idSIS_regCrono","approved"),"user_aziende_intermediari");
$FEDIT->FGE_SetSelectFields(array("ID_AZI","ID_IMP","description","produttore","destinatario","trasportatore","approved"),"user_aziende_intermediari");
$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_aziende_intermediari");
$ProfiloFields = "user_aziende_intermediari";
include("SOGER_FiltriProfilo.php");
$FEDIT->FGE_GroupBy("ID_AZI","user_aziende_intermediari");
//$FEDIT->FGE_HideFields(array("ID_AZI","ID_IMP","produttore","destinatario","trasportatore","idSIS_regCrono","approved"),"user_aziende_intermediari");
$FEDIT->FGE_HideFields(array("ID_AZI","ID_IMP","produttore","destinatario","trasportatore","approved"),"user_aziende_intermediari");
$FEDIT->FGE_LookUpDescribe();
$FEDIT->FGE_LookUpDone();
#

$rosetteA = '<div id="rosetteA" class="rosette">&nbsp;&nbsp;</div>';
$rosetteR = '<div id="rosetteR" class="rosette">&nbsp;&nbsp;</div>';

$FEDIT->FGE_SetTitle("ID_AUTO",$rosetteA."Automezzo","user_movimenti_fiscalizzati");
$FEDIT->FGE_SetTitle("ID_RMK",$rosetteR."Rimorchio","user_movimenti_fiscalizzati");
$FEDIT->FGE_SetTitle("ID_AZI","Intermediario","user_movimenti_fiscalizzati");
#
$FEDIT->FGE_DisableFields(array("NMOV","DTMOV","FKEadrAUT","FKEmzAUT","FKEadrRMK","FKEmzRMK","FKErifCarico","adr"),"user_movimenti_fiscalizzati");

if(isset($_SESSION['StampaAnnua']) && $_SESSION['StampaAnnua']==1){
	$FEDIT->FGE_DisableFields(array("ID_AZI","NOTER"),"user_movimenti_fiscalizzati");
	}

#

echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva dettaglio");
	$RIF_REF = $FEDIT->DbRecordSet[0]["ID_RIF"];
	$ID_RMK = $FEDIT->DbRecordSet[0]["ID_RMK"];
	$ID_AUTO = $FEDIT->DbRecordSet[0]["ID_AUTO"];
	$DTMOV_IN_TRASP = ($FEDIT->DbRecordSet[0]["DTMOV"]=='0000-00-00' || is_null($FEDIT->DbRecordSet[0]["DTMOV"]))? null:$FEDIT->DbRecordSet[0]["DTMOV"];
	$TableName="user_movimenti_fiscalizzati";
	$LegamiSISTRI	= false;
	require("__scripts/MovimentiRifMovCarico.php");

#

if($SOGER->UserData["core_usersAutistaOTF"]=="1" && $IsAdr=="0"){
		$js ="<script type=\"text/javascript\">\n";
		$js.= <<< ENDJS
		function show_hide(id){
			if(document.CreaAutista.HiddenTrasportatore.value<1){
				window.alert("Attenzione, nella scheda Generale non � stato selezionato il trasportatore");
				}
			else{
				var obj=document.getElementById(id);
				if(obj.style.display=='none'){
					obj.style.display='block';
					obj.style.zindex=10;
					}
				else{
					obj.style.display='none';
					obj.style.zindex=-1;
				}
			}
		}
		function creaAutista(){
			if(document.CreaAutista.nome.value=="" || document.CreaAutista.description.value==""){
				window.alert("Attenzione, tutti i campi sono obbligatori");
				}
			else{
				//inserisce record autista
				AjaxAutista("user_movimenti_fiscalizzati");
				}
			}
		
				
ENDJS;
		$js.="</script>\n";
		
		$css ="<style type=\"text/css\">\n";
		$css.="div#btn_nuovo_autista{\n";
		$css.="position: absolute; top: 139px; left: 770px; z-index: 10;";
		$css.="}\n";
		$css.="div#autista{\n";
		$css.="position:absolute;margin-left:-550px;z-index: 10; height: 225px;  padding: 3px; border: 2px #5C7D99 SOLID; background-color: #FFFFFF;";
		$css.="}\n";
		$css.="</style>\n";
		
		echo $css;
		echo $js;
		
		$divAut ="<div id=\"btn_nuovo_autista\">\n";
			$divAut.="<input type=\"button\" class=\"FGEbutton\" name=\"\" value=\"Nuovo autista\" onclick=\"javascript:show_hide('autista');\" />\n";
		
			$divAut.="<div id=\"autista\" style=\"display:none;\">\n";
				$divAut.="<form name=\"CreaAutista\" method=\"post\" action=\"\" style=\"width:400px;\">\n";
					$divAut.="<fieldset class=\"FGEfieldset\" style=\"width:80%;margin-left:30px;\">\n";
					$divAut.="<legend class=\"FGElegend\">NUOVO AUTISTA</legend>\n";
						$divAut.="<div class=\"FGE1Col\" style=\"margin-left:30px;\">\n";
							$divAut.="<label for=\"nome\" class=\"FGEmandatory\">nome</label><br/>\n";
							$divAut.="<input type=\"text\" tabindex=\"1000\" class=\"FGEinput\" id=\"user_autisti:nome\" name=\"nome\" size=\"30\" maxlength=\"30\" value=\"\" onfocus=\"this.style.background='yellow';\"  onblur=\"this.style.background='white';\"/>\n";
						$divAut.="</div>\n";
						
						$divAut.="<div class=\"FGE1Col\" style=\"clear:both;margin-left:30px;\">\n";
							$divAut.="<label for=\"description\" class=\"FGEmandatory\">cognome</label><br/>\n";
							$divAut.="<input type=\"text\" tabindex=\"1001\" class=\"FGEinput\" id=\"user_autisti:description\" name=\"description\" size=\"30\" maxlength=\"30\" value=\"\" onfocus=\"this.style.background='yellow';\"  onblur=\"this.style.background='white';\"/>\n";
						$divAut.="</div>\n";
					
						$divAut.="<div class=\"FGESubmitRow\">\n";
							$divAut.="<input type=\"button\" class=\"FGEbutton\" value=\"crea autista\" onclick=\"creaAutista();\" onfocus=\"this.style.background='yellow';this.style.color='#034373';\" onblur=\"this.style.background='#034373';this.style.color='white';\"/>\n";
							$divAut.="<input type=\"button\" class=\"FGEbutton\" value=\"annulla\" onclick=\"javascript:show_hide('autista');\" onfocus=\"this.style.background='yellow';this.style.color='#034373';\" onblur=\"this.style.background='#034373';this.style.color='white';\"/>\n";
						$divAut.="</div>\n";
					$divAut.="</fieldset>\n";
					$divAut.="<input type=\"hidden\" name=\"HiddenTrasportatore\" value=\"".$ID_UIMT."\" />\n";
				$divAut.="</form>\n";
			$divAut.="</div>\n";
			
		$divAut.="</div>\n";	
	
		echo $divAut;
	}

require_once("__includes/COMMON_sleepForgEdit.php");


if(!isset($AutomezzoRosette)){
$sql="SELECT ID_ORIGINE_DATI FROM user_automezzi WHERE ID_AUTO='".$ID_AUTO."';";
$FEDIT->SdbRead($sql,"DbRecordSetID_AUTO",true,false);
$AutomezzoRosette="'".$FEDIT->DbRecordSetID_AUTO[0]["ID_ORIGINE_DATI"]."'";
$js ="<script type=\"text/javascript\">\n";
$js.= <<< ENDJS
CUSTOM_ROSETTE('rosetteA',$AutomezzoRosette);
ENDJS;
$js.= "</script>";
echo $js;
}

if(!isset($RimorchioRosette)){
$sql="SELECT ID_ORIGINE_DATI FROM user_rimorchi WHERE ID_RMK='".$ID_RMK."';";
$FEDIT->SdbRead($sql,"DbRecordSetID_RMK",true,false);
$RimorchioRosette="'".$FEDIT->DbRecordSetID_RMK[0]["ID_ORIGINE_DATI"]."'";
$js ="<script type=\"text/javascript\">\n";
$js.= <<< ENDJS
CUSTOM_ROSETTE('rosetteR',$RimorchioRosette);
ENDJS;
$js.= "</script>";
echo $js;
}


if(isset($Automezzo) && !$multipleAutomezzi) {

$js ="<script type=\"text/javascript\">\n";
$js.= <<< ENDJS
CUSTOM_ROSETTE('rosetteA',$AutomezzoRosette);
ENDJS;
$js.= "</script>";
echo $js;

?>
<script type="text/javascript">
	// automezzo / adr (se disponibili)
	//CUSTOM_ROSETTE('rosetteA',$AutomezzoRosette);
	document.FGEForm_1["user_movimenti_fiscalizzati:FKEadrAUT"].value = '<?php echo $AdrAutomezzo; ?>';
	document.FGEForm_1["user_movimenti_fiscalizzati:FKEmzAUT"].value = '<?php echo $Automezzo; ?>';
</script>
<?php	
}
?>

<script type="text/javascript">
	// ora e data inizio trasporto automatica
	<?php if($SOGER->UserData['core_usersFRM_SET_DATE']=='1'){ ?>
		if(document.FGEForm_1["<?php echo $TableName; ?>:dt_in_trasp"].value=="")
			document.FGEForm_1["<?php echo $TableName; ?>:dt_in_trasp"].value = '<?php echo $DTMOV_IN_TRASP? date("d/m/Y", strtotime($DTMOV_IN_TRASP)):date("d/m/Y"); ?>';
	<?php } ?>

	<?php if($SOGER->UserData['core_usersFRM_SET_HR']=='1'){ ?>
		if(document.FGEForm_1["<?php echo $TableName; ?>:hr_in_trasp"].value=="")
			document.FGEForm_1["<?php echo $TableName; ?>:hr_in_trasp"].value = '<?php echo date("H:i"); ?>';
	<?php } ?>
</script>
