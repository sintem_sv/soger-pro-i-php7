<?php
if(isset($_GET['filter'])){
	$ID_RIF=$_GET['filter'];
	$hash=MakeUrlHash("user_schede_rifiuti","ID_RIF",$ID_RIF);
}
else{
	$hash="";
	$ID_RIF=0;
	}
?>

<script type="text/javascript">
function SaveAndShow(IDRifiutoCaratt){
	document.FGEForm_1["redirection"].value = "Caratterizzazione.php?filter="+IDRifiutoCaratt;
	FGEForm_1_SendCheck(FGEForm_1);
	}

function GoCaratt() {
	if(confirm('Salvare i dati e passare alla caratterizzazione?')) {
		document.forms[0]["redirection"].value = "statusCUSTOM_SchedaRifiutoCaratt.php";
		FGEForm_1_SendCheck(document.forms[0]);
	}
}
function GoGeneral() {
	if(confirm('Salvare i dati e passare alla schermata generale? \nAnnulla per procedere senza salvare i dati.')) {
		document.FGEForm_1["redirection"].value = "statusCUSTOM_SchedaRifiutoGen.php";
		FGEForm_1_SendCheck(FGEForm_1);
	}
	else{
		location.replace("__scripts/FGE_DataGridEdit.php?table=user_schede_rifiuti&pri=ID_RIF&filter=<?php echo $ID_RIF ?>&hash=<?php echo $hash; ?>&FGE_action=edit");
		}
}
</script>

<!--
<a href="<?php if($this->AppLocation=="UserSchedaRifiutoCaratt") echo "javascript:GoGeneral();"; else echo "#"; ?>" title="Schede Rifiuti Prodotti"><img src="__css/BT_userSchedaRifiutoGeneral.gif" class="NoBorder" alt="Schede Rifiuti Prodotti" /></a><br/>

<a href="<?php if($this->AppLocation=="UserNuovoRifiuto2") echo "javascript:GoCaratt();"; else echo "#"; ?>" title="Crea Richiesta di Carico"><img src="__css/BT_userSchedaRifiutoCaratt.gif" class="NoBorder" alt="Crea Richiesta di Carico" /></a><br/>
-->