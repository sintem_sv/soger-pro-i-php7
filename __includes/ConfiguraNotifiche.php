<?php
require_once("__includes/COMMON_ForgEditClassFiles.php");
require("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;

if(count($_POST)>1){
	$SQL="DELETE FROM user_promemoria WHERE ID_USR=".$SOGER->UserData['core_usersID_USR'].";";
	$FEDIT->SDBWrite($SQL,true,false);

	if($_POST['MailNotificaRegistroCS']!='' AND !is_null($_POST['MailNotificaRegistroCS'])){
		$SQL="INSERT INTO user_promemoria (`ID_USR`,`ID_IMP`,`id_prom` ,`id_fprom` ,`mail`) VALUES (".$SOGER->UserData['core_usersID_USR'].", '".$SOGER->UserData['core_impiantiID_IMP']."', 1, ".$_POST['FrequenzaNotificheCS'].", '".$_POST['MailNotificaRegistroCS']."');";
		$FEDIT->SDBWrite($SQL,true,false);
		}

	if($_POST['MailNotificaRegistroADR']!='' AND !is_null($_POST['MailNotificaRegistroADR'])){
		$SQL="INSERT INTO user_promemoria (`ID_USR`,`ID_IMP`,`id_prom` ,`id_fprom` ,`mail`) VALUES (".$SOGER->UserData['core_usersID_USR'].", '".$SOGER->UserData['core_impiantiID_IMP']."', 2, ".$_POST['FrequenzaNotificheADR'].", '".$_POST['MailNotificaRegistroADR']."');";
		$FEDIT->SDBWrite($SQL,true,false);
		}

	if($_POST['MailNotificaAllarmi']!='' AND !is_null($_POST['MailNotificaAllarmi'])){
		$SQL="INSERT INTO user_promemoria (`ID_USR`,`ID_IMP`,`id_prom` ,`id_fprom` ,`mail`) VALUES (".$SOGER->UserData['core_usersID_USR'].", '".$SOGER->UserData['core_impiantiID_IMP']."', 3, ".$_POST['FrequenzaNotificheAllarmi'].", '".$_POST['MailNotificaAllarmi']."');";
		$FEDIT->SDBWrite($SQL,true,false);
		}

	if($_POST['MailNotificaConformitaLeg']!='' AND !is_null($_POST['MailNotificaConformitaLeg'])){
		$SQL="INSERT INTO user_promemoria (`ID_USR`,`ID_IMP`,`id_prom` ,`id_fprom` ,`mail`) VALUES (".$SOGER->UserData['core_usersID_USR'].", '".$SOGER->UserData['core_impiantiID_IMP']."', 4, ".$_POST['FrequenzaNotificheConformitaLeg'].", '".$_POST['MailNotificaConformitaLeg']."');";
		$FEDIT->SDBWrite($SQL,true,false);
		}

	}



// Promemoria download registro C/S
$SQL="SELECT * FROM user_promemoria WHERE ID_USR=".$SOGER->UserData['core_usersID_USR']." AND ID_PROM=1;";
$FEDIT->SdbRead($SQL,"UserPromemoriaRegistroCS");
if($FEDIT->DbRecsNum>0)	$UserPromemoriaRegistroCS	= $FEDIT->UserPromemoriaRegistroCS; else $UserPromemoriaRegistroCS = NULL;

// Promemoria download registro ADR
$SQL="SELECT * FROM user_promemoria WHERE ID_USR=".$SOGER->UserData['core_usersID_USR']." AND ID_PROM=2;";
$FEDIT->SdbRead($SQL,"UserPromemoriaRegistroADR");
if($FEDIT->DbRecsNum>0)	$UserPromemoriaRegistroADR	= $FEDIT->UserPromemoriaRegistroADR; else $UserPromemoriaRegistroADR = NULL;

// Promemoria download allarmi
$SQL="SELECT * FROM user_promemoria WHERE ID_USR=".$SOGER->UserData['core_usersID_USR']." AND ID_PROM=3;";
$FEDIT->SdbRead($SQL,"UserPromemoriaAllarmi");
if($FEDIT->DbRecsNum>0)	$UserPromemoriaAllarmi		= $FEDIT->UserPromemoriaAllarmi; else $UserPromemoriaAllarmi = NULL;

// Promemoria download allarmi documento controllo conformitą leg.
$SQL="SELECT * FROM user_promemoria WHERE ID_USR=".$SOGER->UserData['core_usersID_USR']." AND ID_PROM=4;";
$FEDIT->SdbRead($SQL,"UserPromemoriaConformitaLeg");
if($FEDIT->DbRecsNum>0)	$UserPromemoriaConformitaLeg		= $FEDIT->UserPromemoriaConformitaLeg; else $UserPromemoriaConformitaLeg = NULL;

?>

<div class="divName" style="margin-bottom:20px"><span>CONFIGURA NOTIFICHE</span></div>

<form style="width:100%;" id="ConfiguraNotifiche" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">


		<div>
		<table style="width:100%;">
			<tr>
				<th colspan="2" style="background-color:#034373;color:#fff">Generazione automatica registro C/S</th>
			</tr>
			<tr>
				<td class="FGEDataGridTableCellLeft">Indirizzo e-mail per notifica</td>
				<td class="FGEDataGridTableCellRight"><input type="text" value="<?php if(!is_null($UserPromemoriaRegistroCS[0]['mail'])) echo $UserPromemoriaRegistroCS[0]['mail']; ?>" name="MailNotificaRegistroCS" id="MailNotificaRegistroCS" class="FGEinput" tabindex="71"/></td>
			</tr>
			<tr>
				<td class="FGEDataGridTableCellLeft">Frequenza di invio della notifica</td>
				<td class="FGEDataGridTableCellRight">
					<select name="FrequenzaNotificheCS">
					<?php 
					$SQL="SELECT * FROM lov_frequenza_promemoria WHERE id_fprom<2;";
					$FEDIT->SdbRead($SQL,"FrequenzaNotificheCS");
					for($f=0;$f<count($FEDIT->FrequenzaNotificheCS);$f++){
						echo "<option value='".$FEDIT->FrequenzaNotificheCS[$f]['id_fprom']."' ";
						if ($UserPromemoriaRegistroCS[0]['id_fprom']==$FEDIT->FrequenzaNotificheCS[$f]['id_fprom']) echo " selected ";
						echo ">".$FEDIT->FrequenzaNotificheCS[$f]['description']."</option>";
						}
					?>
					</select>
				</td>
			</tr>
		</table>
		
		<table style="width:100%;margin-top:20px">
			
			<tr>
				<th colspan="2" style="background-color:#034373;color:#fff">Promemoria download registro ADR</th>
			</tr>
			<tr>
				<td class="FGEDataGridTableCellLeft">Indirizzo e-mail per notifica</td>
				<td class="FGEDataGridTableCellRight"><input type="text" value="<?php if(!is_null($UserPromemoriaRegistroADR[0]['mail'])) echo $UserPromemoriaRegistroADR[0]['mail']; ?>" name="MailNotificaRegistroADR" id="MailNotificaRegistroADR" class="FGEinput" tabindex="74"/></td>
			</tr>
			<tr>
				<td class="FGEDataGridTableCellLeft">Frequenza di invio della notifica</td>
				<td class="FGEDataGridTableCellRight">
					<select name="FrequenzaNotificheADR">
					<?php 
					$SQL="SELECT * FROM lov_frequenza_promemoria WHERE id_fprom>1;";
					$FEDIT->SdbRead($SQL,"FrequenzaNotificheADR");
					for($f=0;$f<count($FEDIT->FrequenzaNotificheADR);$f++){
						echo "<option value='".$FEDIT->FrequenzaNotificheADR[$f]['id_fprom']."' ";
						if ($UserPromemoriaRegistroADR[0]['id_fprom']==$FEDIT->FrequenzaNotificheADR[$f]['id_fprom']) echo " selected ";
						echo ">".$FEDIT->FrequenzaNotificheADR[$f]['description']."</option>";
						}
					?>
					</select>
				</td>
			</tr>

		</table>
		
		<table style="width:100%;margin-top:20px">

			<tr>
				<th colspan="2" style="background-color:#034373;color:#fff">Promemoria download lista allarmi</th>
			</tr>
			<tr>
				<td class="FGEDataGridTableCellLeft">Indirizzo e-mail per notifica</td>
				<td class="FGEDataGridTableCellRight"><input type="text" value="<?php if(!is_null($UserPromemoriaAllarmi[0]['mail'])) echo $UserPromemoriaAllarmi[0]['mail']; ?>" name="MailNotificaAllarmi" id="MailNotificaAllarmi" class="FGEinput" tabindex="77"/></td>
			</tr>
			<tr>
				<td class="FGEDataGridTableCellLeft">Frequenza di invio della notifica</td>
				<td class="FGEDataGridTableCellRight">
					<select name="FrequenzaNotificheAllarmi">
					<?php 
					$SQL="SELECT * FROM lov_frequenza_promemoria;";
					$FEDIT->SdbRead($SQL,"FrequenzaNotificheAllarmi");
					for($f=0;$f<count($FEDIT->FrequenzaNotificheAllarmi);$f++){
						echo "<option value='".$FEDIT->FrequenzaNotificheAllarmi[$f]['id_fprom']."' ";
						if ($UserPromemoriaAllarmi[0]['id_fprom']==$FEDIT->FrequenzaNotificheAllarmi[$f]['id_fprom']) echo " selected ";
						echo ">".$FEDIT->FrequenzaNotificheAllarmi[$f]['description']."</option>";
						}
					?>
					</select>
				</td>
			</tr>

		</table>

	</div>






<input style="margin-top:25px;" type="submit" class="FGEbutton" tabindex="80" value="Salva configurazione" />

</form>

<?php
require_once("__includes/COMMON_sleepForgEditDG.php");
?>