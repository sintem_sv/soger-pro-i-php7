<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require("__includes/COMMON_wakeForgEdit.php");
require("USER_SchedeProduttoriIDcheck.php");
$FEDIT->SDbRead($sql,"DbRecordSet",true,false);

//print_r($FEDIT->DbRecordSet[0]);

if($FEDIT->DbRecsNum==0) {	
	$urlRS   = "#";
	$urlIMP  = "javascript:alert('Inserire%20prima%20la%20ragione%20sociale')";
        $urlAUTH = "javascript:alert('Inserire%20prima%20i%20dati%20Impianto')";
	$urlECO  = "javascript:alert('Inserire%20prima%20i%20dati%20Impianto')";
	$urlALL  = "javascript:alert('Inserire%20prima%20la%20ragione%20sociale')";

} else {
	$urlRS   = "__scripts/SOGER_standardMenu.php?area=UserNuovoProduttore&amp;table=user_aziende_produttori&amp;pri=ID_AZP&amp;filter=";
	$urlRS  .= $FEDIT->DbRecordSet[0]["ID_AZP"] . "&hash=" . MakeUrlHash("user_aziende_produttori","ID_AZP",$FEDIT->DbRecordSet[0]["ID_AZP"]);

	$urlALL   = "__scripts/SOGER_standardMenu.php?area=UserNuovoProduttoreAllegati&amp;table=user_aziende_produttori&amp;pri=ID_AZP&amp;filter=";
	$urlALL  .= $FEDIT->DbRecordSet[0]["ID_AZP"] . "&hash=" . MakeUrlHash("user_aziende_produttori","ID_AZP",$FEDIT->DbRecordSet[0]["ID_AZP"]);


	if(!is_null($FEDIT->DbRecordSet[0]["ID_UIMP"])) {
		$urlIMP = "__scripts/SOGER_standardMenu.php?area=UserNuovoProduttoreImpianto&amp;table=user_impianti_produttori&amp;pri=ID_UIMP&amp;filter=";
		$urlIMP .= $FEDIT->DbRecordSet[0]["ID_UIMP"] . "&hash=" . MakeUrlHash("user_impianti_produttori","ID_UIMP",$FEDIT->DbRecordSet[0]["ID_UIMP"]); 
                if(!is_null($FEDIT->DbRecordSet[0]["ID_AUTH"])) {
			$urlAUTH = "__scripts/SOGER_standardMenu.php?area=UserNuovoProduttoreAutorizzazioni&amp;table=user_autorizzazioni_pro&amp;pri=ID_AUTH&amp;filter=" . $FEDIT->DbRecordSet[0]["ID_AUTH"] . "&hash=" . MakeUrlHash("user_autorizzazioni_pro","ID_AUTH",$FEDIT->DbRecordSet[0]["ID_AUTH"]); 
		} else {
			$urlAUTH = "__scripts/status.php?area=UserNuovoProduttoreAutorizzazioni";
		}
		if(!is_null($FEDIT->DbRecordSet[0]["ID_CNT_P"])) {
			$urlECO = "__scripts/SOGER_standardMenu.php?area=UserProduttoreContratti&amp;table=user_contratti_produttori&amp;pri=ID_AZP&amp;filter=" . $FEDIT->DbRecordSet[0]["ID_AZP"]."&amp;hash=" . MakeUrlHash("user_contratti_produttori","ID_AZP",$FEDIT->DbRecordSet[0]["ID_AZP"]);
			}
		else{
			$_SESSION['ID_AZP_CONTRACT']=$_SESSION["FGE_IDs"]["user_aziende_produttori"];
			$urlECO = "__scripts/SOGER_standardMenu.php?area=UserNuovoContrattoProduttore";
			}
	} else {
                $urlAUTH = "javascript:alert('Inserire%20prima%20i%20dati%20Impianto')";
		$urlECO  = "javascript:alert('Inserire%20prima%20i%20dati%20Impianto')";
		$urlIMP = "__scripts/status.php?area=UserNuovoProduttoreImpianto";
	}
}
?>


<?php
switch($SOGER->AppLocation){
	case "UserNuovoProduttore":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-ON.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_userSchedeProduttoriAUT-OFF.gif";
		$BT_contratti		= "BT_contratti-OFF.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";
		break;
	case "UserNuovoProduttoreImpianto":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-ON.gif";
		$BT_autorizzazioni	= "BT_userSchedeProduttoriAUT-OFF.gif";
		$BT_contratti		= "BT_contratti-OFF.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";
		break;
	case "UserNuovoProduttoreAutorizzazioni":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_userSchedeProduttoriAUT-ON.gif";
		$BT_contratti		= "BT_contratti-OFF.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";
		break;
	case "UserNuovoContrattoProduttore":
	case "UserProduttoreContratti":
	case "UserProduttoreContrattoCondizioni":
	case "UserCondizioneContrattoProduttore":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_userSchedeProduttoriAUT-OFF.gif";
		$BT_contratti		= "BT_contratti-ON.gif";
		$BT_allegati		= "BT_allegati-OFF.gif";
		break;
	case "UserNuovoProduttoreAllegati":
		$BT_ragione			= "BT_userSchedeProduttoriINTEST-OFF.gif";
		$BT_impianti		= "BT_userSchedeProduttoriIMPI-OFF.gif";
		$BT_autorizzazioni	= "BT_userSchedeProduttoriAUT-OFF.gif";
		$BT_contratti		= "BT_contratti-OFF.gif";
		$BT_allegati		= "BT_allegati-ON.gif";
		break;
	}
?>


<a href="<?php echo $urlRS ?>" title="Ragione Sociale"><img src="__css/<?php echo $BT_ragione; ?>" class="NoBorder" alt="Ragione Sociale" /></a>
<a href="<?php echo $urlIMP ?>" title="Dati Impianto"><img src="__css/<?php echo $BT_impianti; ?>" class="NoBorder" alt="Dati Impianto"/></a>
<a href="<?php echo $urlAUTH ?>" title="Autorizzazioni"><img src="__css/<?php echo $BT_autorizzazioni; ?>" class="NoBorder" alt="Autorizzazioni"/></a>
<?php if($this->UserData["core_impiantiMODULO_ECO"]=="1" AND $this->UserData["core_usersG3"]=="1"){ ?>
<a href="<?php echo $urlECO ?>" title="Contratti"><img src="__css/<?php echo $BT_contratti; ?>" class="NoBorder" alt="Contratti"/></a>
<?php } ?>

<a href="<?php echo $urlALL ?>" title="Allegati"><img src="__css/<?php echo $BT_allegati; ?>" class="NoBorder" alt="Allegati"/></a>
