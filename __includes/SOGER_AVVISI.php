<?php

#
# 	PARAMETRI CONTROLLI + includes
#
$gotAlarms_gestione = false;
$gotAlarms_altri = false;
require_once("__classes/ForgEdit2.class");
require_once("__classes/ForgEdit.RegExp");
require_once("__classes/DbLink.class");
require_once("__libs/SQLFunct.php");
require("__includes/COMMON_wakeForgEdit.php");
global $SOGER;

#
#   Extra Js code
#

$js = <<< JS
    <script type="text/javascript">
        $(document).ready(function(){
        
            $( "#SogerAvvisiGestione, #SogerAvvisiAltri" ).click(function(){
                $( "#SogerAvvisiGestioneContainer" ).slideToggle();
                $( "#SogerAvvisiAltriContainer" ).slideToggle();
                });
        
            $( "#SogerAvvisiDownload" ).click(function(){
                window.location.href = '__scripts/ListaControlloAllarmi.php';
                return false;
                });
        });
    </script>
JS;
echo $js;

#
# 	VERIFICA / GENERAZIONE COSTI FISSI X SOGER ECO
#
if($SOGER->UserData["core_usersG3"] == "1") {
    require("__libs/SogerECO_funct.php");
    $resultArray = array();
    $sql = sql_estraiCostiFissi($SOGER->UserData["core_usersID_IMP"],"prod");
    $FEDIT->SDbRead($sql,"DbRecordSet",true,false);
    if($FEDIT->DbRecsNum>0) {
        $resultArray[] = costiFissiSqlVerifica($FEDIT,"prod");
    }
    $sql = sql_estraiCostiFissi($SOGER->UserData["core_usersID_IMP"],"dest");
    $FEDIT->SDbRead($sql,"DbRecordSet",true,false);
    if($FEDIT->DbRecsNum>0) {
        $resultArray[] =  costiFissiSqlVerifica($FEDIT,"dest");
    }
    $sql = sql_estraiCostiFissi($SOGER->UserData["core_usersID_IMP"],"inter");
    $FEDIT->SDbRead($sql,"DbRecordSet",true,false);
    if($FEDIT->DbRecsNum>0) {
        $resultArray[] =  costiFissiSqlVerifica($FEDIT,"inter");
    }
    $sql = sql_estraiCostiFissi($SOGER->UserData["core_usersID_IMP"],"trasp");
    $FEDIT->SDbRead($sql,"DbRecordSet",true,false);
    if($FEDIT->DbRecsNum>0) {
        $resultArray[] = costiFissiSqlVerifica($FEDIT,"trasp");
    }
    CostiFissiDoTheDamnThing($resultArray,$FEDIT,$SOGER->UserData["core_usersID_IMP"]);
}		

$impianto = $SOGER->UserData["core_impiantiID_IMP"];
$CkRitornoFormulario = $SOGER->UserData["core_usersck_RIT_FORM"];
$intervalloForm = $SOGER->UserData["core_usersck_RIT_FORMgg"];
$CkAutorizzazioni = $SOGER->UserData["core_usersck_AUT"];
$intervallo = $SOGER->UserData["core_usersck_AUTgg"];
$CkAnalisi = $SOGER->UserData["core_usersck_ANALISI"];
$intervalloAnalisi = $SOGER->UserData["core_usersck_ANALISIgg"];
$CkMOV_SISTRI = $SOGER->UserData["core_usersck_MOV_SISTRI"];
$intervalloMOV_SISTRI = $SOGER->UserData["core_usersck_MOV_SISTRIgg"];
$CkContributi = $SOGER->UserData["core_usersck_CNTR"];
$CkDepositoTemporaneo = $SOGER->UserData["core_usersck_DEP_TEMP"]; 
$CkDepositoTemporaneoM3 = $SOGER->UserData["core_usersq_limite"];
$CkDepositoTemporaneoM3pericolosi = $SOGER->UserData["core_usersq_limite_p"]; 
$CkDepositoTemporaneoM3totali = $SOGER->UserData["core_usersq_limite_t"];

# GIORNI MODIFICA MOVIMENTI
$GGedit_produttore                      = $SOGER->UserData["core_usersGGedit_produttore"]; 
$GGedit_trasportatore                   = $SOGER->UserData["core_usersGGedit_trasportatore"]; 
$GGedit_destinatario                    = $SOGER->UserData["core_usersGGedit_destinatario"]; 
$GGedit_intermediario                   = $SOGER->UserData["core_usersGGedit_intermediario"];

//print_r(var_dump($SOGER->UserData));


############################ ALLARMI DI GESTIONE ############################

echo '<div class="divName" id="SogerAvvisiGestione"><span>ALLARMI DI GESTIONE</span></div>';

echo '<div id="SogerAvvisiGestioneContainer">';


#
#	GIORNI PER LA MODIFICA DEI MOVIMENTI
#
$parameter = ${"GGedit_".$SOGER->UserData['workmode']};
if($SOGER->UserData['workmode']=="produttore") $limit=14; else $limit=2;
if($parameter>$limit){
    $gotAlarms_gestione=true;
    echo "<div class=\"SogerAvvisiTitle\">Giorni per la modifica dei movimenti</div>";
    $avviso = "Attenzione, i giorni di modificabilit� della movimentazione indicati in configurazione superano le disposizioni dell'articolo 190 del D.Lgs. 152/2006 sulla corretta tenuta del registro di carico e scarico rifiuti.";
    echo "<div class=\"SogerAvvisiItem\">$avviso</div>";
}


#
#	MEZZI SENZA BLACK BOX
#
if($SOGER->UserData['workmode']=='produttore' AND $SOGER->UserData['core_impiantiMODULO_SIS']=='1'){
    $sql ="SELECT DISTINCT user_aziende_trasportatori.ID_AZT, user_aziende_trasportatori.description as trasportatore, user_automezzi.description AS targa FROM `user_automezzi` JOIN user_autorizzazioni_trasp ON user_autorizzazioni_trasp.ID_AUTHT=user_automezzi.ID_AUTHT JOIN user_impianti_trasportatori ON user_impianti_trasportatori.ID_UIMT=user_autorizzazioni_trasp.ID_UIMT JOIN user_aziende_trasportatori ON user_aziende_trasportatori.ID_AZT=user_impianti_trasportatori.ID_AZT JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_autorizzazioni_trasp.ID_RIF WHERE BlackBox=0 AND user_autorizzazioni_trasp.approved=1 AND user_impianti_trasportatori.approved=1 AND user_aziende_trasportatori.approved=1 AND user_aziende_trasportatori.produttore=1 AND user_aziende_trasportatori.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND pericoloso=1 ORDER BY trasportatore, targa;";
    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);

    if($FEDIT->DbRecsNum>0) {
        $gotAlarms_altri=true;
        echo "<div class=\"SogerAvvisiTitle\">Automezzi senza Black Box SISTRI</div>";
	$AZT_buffer	= 0;
	$avvisi		= array();
	$ContaAvvisi    = 0;
	for($d=0;$d<count($FEDIT->DbRecordSet);$d++){
            if($FEDIT->DbRecordSet[$d]['ID_AZT']!=$AZT_buffer){
                $avviso ="Attenzione, i seguenti automezzi del trasportatore ".$FEDIT->DbRecordSet[$d]['trasportatore']." risultano sprovvisti della Black Box SISTRI: ";
                $ContaAvvisi++;
                }
            $AZT_buffer	= $FEDIT->DbRecordSet[$d]['ID_AZT'];
            $avviso.=$FEDIT->DbRecordSet[$d]['targa'];
            $NextRecord = $d+1;
            if($NextRecord<count($FEDIT->DbRecordSet) AND $AZT_buffer==$FEDIT->DbRecordSet[$NextRecord]['ID_AZT'])
                $avviso.=", ";
            $avvisi[$ContaAvvisi] = $avviso;
        }
	for($a=1; $a<=count($avvisi); $a++){
            echo "<div class=\"SogerAvvisiItem\">$avvisi[$a]";
            echo "</div>";
        }
    }
}


#
#	SCADENZA ANALISI
#
if($CkAnalisi=='1') {
    $sql  = "SELECT lov_cer.COD_CER, descrizione, CAR_NumDocumento, CAR_DataAnalisi, CAR_DataScadenzaAnalisi ";
    $sql .= " FROM user_schede_rifiuti JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
    $sql .= " WHERE (CAR_DataScadenzaAnalisi< ADDDATE(CURDATE(),$intervalloAnalisi) OR CAR_DataScadenzaAnalisi IS NULL OR CAR_DataScadenzaAnalisi='0000-00-00') ";
    $sql .= " AND ID_IMP='" . $impianto . "'";
    $sql .= " AND approved=1 ";
    $sql .= " AND CAR_DataAnalisi<>'0000-00-00' AND CAR_DataAnalisi IS NOT NULL ";
    $TableSpec = "user_schede_rifiuti.";
    require("SOGER_DirectProfilo.php");	
    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);
	
    if($FEDIT->DbRecsNum>0) {
        $gotAlarms_gestione=true;
        echo "<div class=\"SogerAvvisiTitle\">Analisi di laboratorio</div>";
        foreach($FEDIT->DbRecordSet as $k=>$dati) {
            $avviso = "CER " . $dati["COD_CER"] . " - " . $dati["descrizione"] . ":"; 
            $DataDocumento_array = explode("-", $dati["CAR_DataAnalisi"]);
            $DataDocumento = $DataDocumento_array[2]."/".$DataDocumento_array[1]."/".$DataDocumento_array[0];
            if($dati["CAR_DataScadenzaAnalisi"]=="0000-00-00" OR is_null($dati["CAR_DataScadenzaAnalisi"]))
                $avviso.= " la data di fine validit� dell'analisi ".$dati["CAR_NumDocumento"]." del ".$DataDocumento." � mancante.";
            else{
                #
                # necessit� di cambiare l'allarme a seconda che l'analisi sia scaduta o in scadenza!
                #	
                $oggi=mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                $scade=explode('-', $dati['CAR_DataScadenzaAnalisi']);
                $scadenza=mktime(0, 0, 0, $scade[1], $scade[2], $scade[0]);
                $differenza=($scadenza - $oggi)/(60*60*24);
                if($differenza>=0)
                    $avviso.=" l'analisi ".$dati["CAR_NumDocumento"]." del ".$DataDocumento." � prossima al termine della sua validit�.";
                else
                    $avviso.=" l'analisi ".$dati["CAR_NumDocumento"]." del ".$DataDocumento." ha superato il termine della sua validit� ed � necessario ripeterla.";
                }
            echo "<div class=\"SogerAvvisiItem\">$avviso";
            echo "</div>";	
            $c++;
        }
    }	
}


#
#	ORGANIZZAZIONE DEPOSITO
#
$sql ="SELECT lov_cer.COD_CER, user_schede_rifiuti.FKEdisponibilita AS giacenza, lov_contenitori.portata, lov_contenitori.m_cubi, user_schede_rifiuti_deposito.NumCont, user_schede_rifiuti_deposito.AVVISO_SINGOLO, user_schede_rifiuti_deposito.MaxStock, lov_cer.cod_cer, user_schede_rifiuti.descrizione, user_schede_rifiuti.ID_UMIS ";
$sql.="FROM user_schede_rifiuti ";
$sql.="JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
$sql.="RIGHT JOIN user_schede_rifiuti_deposito ON user_schede_rifiuti.ID_RIF=user_schede_rifiuti_deposito.ID_RIF ";
$sql.="LEFT JOIN lov_contenitori ON user_schede_rifiuti_deposito.ID_CONT=lov_contenitori.ID_CONT ";
$sql.="WHERE user_schede_rifiuti.ID_IMP='".$impianto."' AND user_schede_rifiuti.FKEdisponibilita>0 ";
$TableSpec = "user_schede_rifiuti.";
require("SOGER_DirectProfilo.php");
unset($TableSpec);
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

$PrintTitle=false;
for($s=0;$s<count($FEDIT->DbRecordSet);$s++){
    $avviso="";
    if($FEDIT->DbRecordSet[$s]['MaxStock']>0){
        if($FEDIT->DbRecordSet[$s]['AVVISO_SINGOLO']=='1')
            $MAX=$FEDIT->DbRecordSet[$s]['MaxStock'];
        else
            $MAX=$FEDIT->DbRecordSet[$s]['MaxStock'] * $FEDIT->DbRecordSet[$s]['NumCont'];
    }
    else{
        if($FEDIT->DbRecordSet[$s]['AVVISO_SINGOLO']=='1'){
            switch($FEDIT->DbRecordSet[$s]['ID_UMIS']){
                case 1: //kg
                    $MAX=$FEDIT->DbRecordSet[$s]['portata'];
                    break;
                case 2: //litri
                    $MAX=$FEDIT->DbRecordSet[$s]['m_cubi'] * 1000;
                    break;
                case 3: //m cubi
                    $MAX=$FEDIT->DbRecordSet[$s]['m_cubi'];
                    break;
            }		
        }
        else{
            switch($FEDIT->DbRecordSet[$s]['ID_UMIS']){
                case 1: //kg
                    $MAX=$FEDIT->DbRecordSet[$s]['NumCont'] * $FEDIT->DbRecordSet[$s]['portata'];
                    break;
                case 2: //litri
                    $MAX=$FEDIT->DbRecordSet[$s]['NumCont'] * $FEDIT->DbRecordSet[$s]['m_cubi'] * 1000;
                    break;
                case 3: //m cubi
                    $MAX=$FEDIT->DbRecordSet[$s]['NumCont'] * $FEDIT->DbRecordSet[$s]['m_cubi'];
                    break;
            }		
        }
    }
    if($FEDIT->DbRecordSet[$s]['giacenza']>=$MAX && $MAX>0 ){
        $gotAlarms_gestione=true;
        if(!$PrintTitle){
            echo "<div class=\"SogerAvvisiTitle\">Gestione Deposito</div>";
            $PrintTitle=true;
        }
	$avviso.=$FEDIT->DbRecordSet[$s]['COD_CER']." ".$FEDIT->DbRecordSet[$s]['descrizione'].": attenzione, contenitore pieno.<br />";
        echo "<div class=\"SogerAvvisiItem\">$avviso";
        echo "</div>";
    }
}


#
#	AVVISO DEPOSITO TEMPORANEO (RIF SINGOLO)
#
if($CkDepositoTemporaneo=="1" && $SOGER->UserData["workmode"]=="produttore") {
    if($SOGER->UserData['core_impiantiREG_IND']==1) $TableName="user_movimenti"; else $TableName="user_movimenti_fiscalizzati";
    $sql = "SELECT giac_ini, 0 AS isGiacIniAuto, user_schede_rifiuti.originalID_RIF AS ID_RIF,lov_cer.COD_CER,lov_cer.pericoloso, NMOV,DTMOV,TIPO,quantita,lov_misure.ID_UMIS,lov_misure.description AS misura,peso_spec,descrizione,q_limite,t_limite ";
    $sql .= " FROM user_schede_rifiuti ";
    $sql .= " LEFT JOIN ".$TableName." ON user_schede_rifiuti.ID_RIF=".$TableName.".ID_RIF AND NMOV<>9999999 AND ".$TableName.".PerRiclassificazione=0";
    $sql .= " JOIN lov_misure ON user_schede_rifiuti.ID_UMIS=lov_misure.ID_UMIS";
    $sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
    $sql .= " WHERE (q_limite<>'0' OR t_limite<>0)";
    $sql .= " AND user_schede_rifiuti.produttore='1'";
    $sql .= " AND user_schede_rifiuti.ID_IMP='" . $impianto . "'";
    $sql .= " AND ".$TableName.".ID_IMP='" . $impianto . "'";
    //$sql .= " AND user_schede_rifiuti.FKEdisponibilita>0";

    $sql2 = "SELECT giac_ini, 1 AS isGiacIniAuto,user_schede_rifiuti.originalID_RIF AS ID_RIF,lov_cer.COD_CER,lov_cer.pericoloso, NMOV,DTMOV,TIPO,quantita,lov_misure.ID_UMIS,lov_misure.description AS misura,peso_spec,descrizione,q_limite,t_limite ";
    $sql2 .= " FROM user_schede_rifiuti ";
    $sql2 .= " JOIN user_movimenti_giacenze_iniziali ON user_schede_rifiuti.ID_RIF=user_movimenti_giacenze_iniziali.ID_RIF";
    $sql2 .= " JOIN lov_misure ON user_schede_rifiuti.ID_UMIS=lov_misure.ID_UMIS";
    $sql2 .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
    $sql2 .= " WHERE (q_limite<>'0' OR t_limite<>0)";
    $sql2 .= " AND user_schede_rifiuti.produttore='1'";
    $sql2 .= " AND user_schede_rifiuti.ID_IMP='" . $impianto . "'";
    $sql2 .= " AND user_movimenti_giacenze_iniziali.ID_IMP='" . $impianto . "'";
    //$sql2 .= " AND user_schede_rifiuti.FKEdisponibilita>0";

    $sqlUnion = "(".$sql.") UNION (".$sql2.")";
    $sqlUnion.= " ORDER BY ID_RIF ASC, isGiacIniAuto DESC, NMOV ASC, DTMOV ASC";

    $FEDIT->SDBRead($sqlUnion,"DbRecordSet",true,false);
    //print_r($FEDIT->DbRecordSet);
    $DetTmp = array();
    if($FEDIT->DbRecsNum>0) {
        $RifBuffer = "";
        foreach($FEDIT->DbRecordSet as $k=>$dati) {
            if($dati["ID_RIF"]!=$RifBuffer) {
                $RifBuffer = $dati["ID_RIF"];

                # MODALITA CONTROLLO (TEMPO / PESO)
                if($dati["q_limite"]!=0 && !is_null($dati["q_limite"])) {
                    if($dati["ID_UMIS"]=="1" | $dati["peso_spec"]!="0")
                        $mode = "Q";
                    else
                        $mode = "U"; //U=undefined: se non si tratta di Kg e manca peso specifico x convertire
                }
                else
                    $mode = "T";

                # setup (giac ini, modo verifica)
                # set giac_ini solo se � giacenza "manuale"
                $dati["giac_ini"] = ($dati['isGiacAuto']==0? $dati["giac_ini"]:0);
                if($dati["q_limite"]!=0) {
                    if($dati["ID_UMIS"]=="1" | $dati["peso_spec"]!="0") # m3 | presenza valore peso specifico
                        $subTotQ[$RifBuffer] = array($dati["q_limite"],toM3($dati["giac_ini"],$dati["ID_UMIS"],$dati["peso_spec"]),$dati["COD_CER"]." - ".$dati["descrizione"], $dati["pericoloso"]);
                    else
                        $subTotQ[$RifBuffer] = array($dati["q_limite"],0,$dati["COD_CER"]." - ".$dati["descrizione"], $dati["pericoloso"]);
                    }
                else {
                    $subTotT = $dati["giac_ini"];
                    $subTotT_C[$dati["ID_RIF"]] = $dati["giac_ini"];
                }
            }

            if($mode=="Q") {
                if($dati["TIPO"]=="C")
                    $subTotQ[$dati["ID_RIF"]][1] +=  toM3($dati["quantita"],$dati["ID_UMIS"],$dati["peso_spec"]);
                else
                    $subTotQ[$dati["ID_RIF"]][1] -= toM3($dati["quantita"],$dati["ID_UMIS"],$dati["peso_spec"]);
            }

            if($dati["TIPO"]=="C" && $mode!="T"){
                $subTotT -= $dati["quantita"];
                if($subTotT==0) {
                    if(isset($tmpCarichi[$dati["ID_RIF"]])) {
                        unset($tmpCarichi[$dati["ID_RIF"]]);
                    }
                }
            }

        }

        $TotC = 0;
        $TotS = 0;
        $RIFbuf = "";
        $counter = 0;
        $Ypos = 33;
        $NPfound = false;
        $Pfound = false;
        $rifiuti = array();
        $CarichiRifiuto = array();

        for($i=0;$i<count($FEDIT->DbRecordSet);$i++){
            $indice=count($rifiuti);
            if($FEDIT->DbRecordSet[$i]['ID_RIF']!=$RIFbuf) {
                $rifiuti[$indice]['totC']=($FEDIT->DbRecordSet[$i]['isGiacIniAuto']==0? $FEDIT->DbRecordSet[$i]['giac_ini']:0);
                $rifiuti[$indice]['COD_CER']=$FEDIT->DbRecordSet[$i]['COD_CER'];
                $rifiuti[$indice]['pericoloso']=$FEDIT->DbRecordSet[$i]['pericoloso'];
                $rifiuti[$indice]['RifID']=$FEDIT->DbRecordSet[$i]['ID_RIF'];
                $rifiuti[$indice]['giac_ini']=($FEDIT->DbRecordSet[$i]['isGiacIniAuto']==0? $FEDIT->DbRecordSet[$i]['giac_ini']:0);
                $rifiuti[$indice]['descrizione']=$FEDIT->DbRecordSet[$i]['descrizione'];
                $rifiuti[$indice]['UM']=$FEDIT->DbRecordSet[$i]['misura'];
                $rifiuti[$indice]['IDUM']=$FEDIT->DbRecordSet[$i]['ID_UMIS'];
                $rifiuti[$indice]['PSPEC']=$FEDIT->DbRecordSet[$i]['peso_spec'];
                $rifiuti[$indice]['t_limite']=$FEDIT->DbRecordSet[$i]['t_limite'];
                if($FEDIT->DbRecordSet[$i]['TIPO']=="C"){
                    $rifiuti[$indice]['carico'][0]['qta']=$FEDIT->DbRecordSet[$i]['quantita'];
                    $rifiuti[$indice]['carico'][0]['num']=$FEDIT->DbRecordSet[$i]['NMOV'];
                    $rifiuti[$indice]['carico'][0]['data']=$FEDIT->DbRecordSet[$i]['DTMOV'];
                    $rifiuti[$indice]['totC']+=$FEDIT->DbRecordSet[$i]['quantita'];
                }
                if($FEDIT->DbRecordSet[$i]['TIPO']=="S")
                    @$rifiuti[$indice]['totS']=$FEDIT->DbRecordSet[$i]['quantita'];
            }
            else{
                # somme
                $countCarichi=count($rifiuti[$indice-1]['carico']);
                if($FEDIT->DbRecordSet[$i]['TIPO']=="C"){
                    $rifiuti[$indice-1]['carico'][$countCarichi]['qta']=$FEDIT->DbRecordSet[$i]['quantita'];
                    $rifiuti[$indice-1]['carico'][$countCarichi]['num']=$FEDIT->DbRecordSet[$i]['NMOV'];
                    $rifiuti[$indice-1]['carico'][$countCarichi]['data']=$FEDIT->DbRecordSet[$i]['DTMOV'];
                    $rifiuti[$indice-1]['totC']+=$FEDIT->DbRecordSet[$i]['quantita'];
                }
                if($FEDIT->DbRecordSet[$i]['TIPO']=="S")
                    @$rifiuti[$indice-1]['totS']+=$FEDIT->DbRecordSet[$i]['quantita'];
            }
            $RIFbuf=$FEDIT->DbRecordSet[$i]['ID_RIF'];
        }
        //if($SOGER->UserData['core_usersusr']=='sintem')
        //print_r($rifiuti);	

        for($r=0;$r<count($rifiuti);$r++){
			/* rifiuto riclassificato */
			$sql = "select ID_RIF, COD_CER, descrizione FROM user_schede_rifiuti 
			JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER 
			where originalID_RIF=".$rifiuti[$r]['RifID']." AND ID_RIF>originalID_RIF
			AND produttore=1 AND ID_IMP='".$impianto."' ORDER BY ID_RIF LIMIT 0, 1;";
			$FEDIT->SDbRead($sql,"DbRecordSet",true,false);
			if($FEDIT->DbRecordSet[0]){
				$rifiuti[$r]['RifID']=$FEDIT->DbRecordSet[0]['ID_RIF'];
				$rifiuti[$r]['COD_CER']=$FEDIT->DbRecordSet[0]['COD_CER'];
				$rifiuti[$r]['descrizione']=$FEDIT->DbRecordSet[0]['descrizione'];
			}

            $string=checkCarichi($rifiuti[$r], $CarichiRifiuto);
            $result=explode("|",$string);
            $tmpCarichi[$result[5]] = array($result[1],$result[2],$result[4],$result[3],$result[0],$result[5],$result[6]);
        }

        //print_R($subTotQ);

        # VERIFICA TEMPO
        if(isset($tmpCarichi) && @count($tmpCarichi)>0) {
            foreach($tmpCarichi as $k=>$dt) {
                $showPreAlarm = ( strtotime($dt[0]) + ($dt[2]*24*60*60) - ($SOGER->UserData["core_usersck_DEP_TEMPgg"]*24*60*60) );
                $showAlarm = ( strtotime($dt[0]) + ($dt[2]*24*60*60) );
                if( time() >= $showAlarm ){
                    $differenza = dateDiff("d",date('m-d-Y',$showPreAlarm),date("m/d/Y"));
                    $DetTmp[$dt[3]] = array($dt[3],$subTotT,$dt[2],"giorni",$dt[4]."/".date('Y', strtotime($dt[0])),"T1");
                }
                else{
                    if( time() >= $showPreAlarm ){
                        $differenza=abs(strtotime(date('Y-m-d',$showAlarm)) - strtotime(date('Y-m-d')))/(86400); 
                        $DetTmp[$dt[3]] = array($dt[3],$subTotT,$dt[2],"giorni",$dt[4]."/".date('Y', strtotime($dt[0])),"T2",$differenza, $dt[5], $dt[6]);
                    }
                }
            }	
        }

        # VERIFICA PESO
        if(isset($subTotQ) && @count($subTotQ)>0) {
            foreach($subTotQ as $k=>$dt) {
                if($dt[1]>$dt[0]) {
                    $DetTmp[$k] = array($dt[2],$dt[1],$dt[0],$dt[4],"m3","Q");	
                }
            }
        }

    }

    if(count($DetTmp)>0) {
        $avvisoDetTmp = array();
        foreach($DetTmp as $k=>$avv) {
            if($k!=''){
                switch($avv[5]){
                    case "Q":
                        if($SOGER->UserData['core_usersID_GDEP']==2)
                            $avvisoDetTmp[] = "Il rifiuto \"" . $avv[0] . "\" ha superato la giacenza di " . $avv[2] . " " . $avv[3] . " (giacenza attuale: " . $avv[1] . " m3)";
                        break;

                    case "T1":
                        if($SOGER->UserData['core_usersID_GDEP']==1){
                            if($avv[4]==0) $carico="giacenza iniziale"; else $carico="carico numero ".$avv[4];
                            $avvisoDetTmp[] = "Il rifiuto \"" . $avv[0] . "\" ha superato il periodo massimo di deposito " . $avv[2] . " " . $avv[3] . " ( ".$carico." ) ";
                        }
                        break;

                    case "T2":
                        if($SOGER->UserData['core_usersID_GDEP']==1){
                            if($SOGER->UserData['core_impiantiREG_IND']==0){
                                $linkNewS		= "UserNuovoMovScaricoF";
                                $linkNewScheda	= "UserNuovaSchedaSistriF";
                            }
                            else{
                                $linkNewS		= "UserNuovoMovScarico";
                                $linkNewScheda	= "UserNuovaSchedaSistri";
                            }
                            if($avv[4]==0) $carico="giacenza iniziale"; else $carico="carico numero ".$avv[4];
                            $avviso = "Restano ".number_format($avv[6],0,",","")." giorni per effettuare uno scarico del rifiuto \"" . $avv[0] . "\", prima che questo superi il periodo massimo di deposito di " . $avv[2] . " " . $avv[3] ." ( ".$carico." ) ";
                            if(!isset($_GET["PlainTxt"])){
                                $avviso.="<a href=\"__scripts/status.php?area=".$linkNewS."&ID_RIF=".$avv[7]."\" class=\"Avvisi\">crea scarico / fir</a>";
                                if($avv[8]==1){
                                    $avviso.=" oppure <a href=\"__scripts/status.php?area=".$linkNewScheda."&ID_RIF=".$avv[7]."\" class=\"Avvisi\">crea scheda sistri</a>";
                                }
                            }
                            $avvisoDetTmp[] = $avviso;
                        }
                        break;
                }
            }
        }

        if(count($avvisoDetTmp)>0) {
            $gotAlarms_gestione=true;
            echo "<div class=\"SogerAvvisiTitle\">Deposito Temporaneo - Rifiuto</div>";
            for($a=0;$a<count($avvisoDetTmp);$a++){
                echo "<div class=\"SogerAvvisiItem\">".$avvisoDetTmp[$a];
                echo "</div>";
            }
        }
    }
}


#
#	AVVISO DEPOSITO TEMPORANEO (SOMMA RIFIUTI NON PERICOLOSI)
#
if($CkDepositoTemporaneo=="1" && $SOGER->UserData["workmode"]=="produttore" && $CkDepositoTemporaneoM3>0 && $SOGER->UserData['core_usersID_GDEP']==2) {
    if($SOGER->UserData['core_impiantiREG_IND']==1) $TableName="user_movimenti"; else $TableName="user_movimenti_fiscalizzati";
    $sql = "SELECT giac_ini,user_schede_rifiuti.originalID_RIF AS ID_RIF, lov_cer.COD_CER,TIPO,quantita,lov_misure.ID_UMIS,lov_misure.description AS misura,peso_spec,descrizione";
    $sql .= " FROM user_schede_rifiuti ";
    $sql .= " LEFT JOIN ".$TableName." ON user_schede_rifiuti.ID_RIF=".$TableName.".ID_RIF AND NMOV<>9999999 AND PerRiclassificazione=0";
    $sql .= " JOIN lov_misure ON user_schede_rifiuti.ID_UMIS=lov_misure.ID_UMIS";
    $sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
    $sql .= " WHERE user_schede_rifiuti.pericoloso='0' AND user_schede_rifiuti.produttore='1' AND peso_spec<>'0'";
    $sql .= " AND user_schede_rifiuti.ID_IMP='" . $impianto . "' ORDER BY lov_cer.COD_CER ASC,NMOV ASC";
    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);
    if($FEDIT->DbRecsNum>0) {
        $CerBuffer = "";
        $DetTmp = array();
        $SubTot = array();
        foreach($FEDIT->DbRecordSet as $k=>$dati) {
            if($dati["COD_CER"]!=$CerBuffer) {
                $SubTot[$dati["COD_CER"]] = toM3($dati["giac_ini"],$dati["ID_UMIS"],$dati["peso_spec"]);
                $CerBuffer = $dati["COD_CER"];
            }
            if($dati["TIPO"]=="C") {
                $SubTot[$dati["COD_CER"]] +=  toM3($dati["quantita"],$dati["ID_UMIS"],$dati["peso_spec"]);
            } 
            else {
                $SubTot[$dati["COD_CER"]] -=  toM3($dati["quantita"],$dati["ID_UMIS"],$dati["peso_spec"]);
            }
        }
        foreach($SubTot as $cer=>$q) {
            if($q>$CkDepositoTemporaneoM3) {
                $DetTmp[$cer] = array($q,$CkDepositoTemporaneoM3,"m3");	
            }
        }
        if(count($DetTmp)>0) {
            $gotAlarms_gestione=true;
            echo "<div class=\"SogerAvvisiTitle\">Deposito Temporaneo - CER (m3 non pericolosi)</div>";
            foreach($DetTmp as $k=>$avv) {
                if($k!=''){
                    $avviso = "I rifiuti CER $k hanno superato la giacenza di $CkDepositoTemporaneoM3 m3 (giacenza attuale: " .  $avv[0] . " m3)";
                    echo "<div class=\"SogerAvvisiItem\">$avviso";
                    echo "</div>";
                }
            }
        }
    }
}


#
#	AVVISO DEPOSITO TEMPORANEO (SOMMA RIFIUTI PERICOLOSI)
#
if($CkDepositoTemporaneo=="1" && $SOGER->UserData["workmode"]=="produttore" && $CkDepositoTemporaneoM3pericolosi>0 && $SOGER->UserData['core_usersID_GDEP']==2) {
    if($SOGER->UserData['core_impiantiREG_IND']==1) $TableName="user_movimenti"; else $TableName="user_movimenti_fiscalizzati";	
    $sql = "SELECT giac_ini,user_schede_rifiuti.originalID_RIF AS ID_RIF, lov_cer.COD_CER,TIPO,quantita,lov_misure.ID_UMIS,lov_misure.description AS misura,peso_spec,descrizione";
    $sql .= " FROM user_schede_rifiuti ";
    $sql .= " LEFT JOIN ".$TableName." ON user_schede_rifiuti.ID_RIF=".$TableName.".ID_RIF AND NMOV<>9999999 AND PerRiclassificazione=0";
    $sql .= " JOIN lov_misure ON user_schede_rifiuti.ID_UMIS=lov_misure.ID_UMIS";
    $sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
    $sql .= " WHERE user_schede_rifiuti.pericoloso='1' AND user_schede_rifiuti.produttore='1' AND peso_spec<>'0'";
    $sql .= " AND user_schede_rifiuti.ID_IMP='" . $impianto . "' ORDER BY lov_cer.COD_CER ASC,NMOV ASC";
    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);
    if($FEDIT->DbRecsNum>0) {
        $CerBuffer = "";
        $DetTmp = array();
        $SubTot = array();
        foreach($FEDIT->DbRecordSet as $k=>$dati) {
            if($dati["COD_CER"]!=$CerBuffer) {
                $SubTot[$dati["COD_CER"]] = toM3($dati["giac_ini"],$dati["ID_UMIS"],$dati["peso_spec"]);
                $CerBuffer = $dati["COD_CER"];
            }
            if($dati["TIPO"]=="C") {
                $SubTot[$dati["COD_CER"]] +=  toM3($dati["quantita"],$dati["ID_UMIS"],$dati["peso_spec"]);
            } 
            else {
                $SubTot[$dati["COD_CER"]] -=  toM3($dati["quantita"],$dati["ID_UMIS"],$dati["peso_spec"]);
            }
        }
        foreach($SubTot as $cer=>$q) {
            if($q>$CkDepositoTemporaneoM3pericolosi) {
                $DetTmp[$cer] = array($q,$CkDepositoTemporaneoM3pericolosi,"m3");	
            }				
        }

        if(count($DetTmp)>0) {
            $gotAlarms_gestione=true;
            echo "<div class=\"SogerAvvisiTitle\">Deposito Temporaneo - CER (m3 pericolosi)</div>";
            foreach($DetTmp as $k=>$avv) {
                if($k!=''){
                    $avviso = "I rifiuti CER $k hanno superato la giacenza di $CkDepositoTemporaneoM3pericolosi m3 (giacenza attuale: " .  $avv[0] . " m3)";
                    echo "<div class=\"SogerAvvisiItem\">$avviso";
                    echo "</div>";
                }
            }
        }
    }
}


#
#	AVVISO DEPOSITO TEMPORANEO (SOMMA TUTTI I RIFIUTI)
#
if($CkDepositoTemporaneo=="1" && $SOGER->UserData["workmode"]=="produttore" && $CkDepositoTemporaneoM3totali>0 && $SOGER->UserData['core_usersID_GDEP']==2) {
    if($SOGER->UserData['core_impiantiREG_IND']==1) $TableName="user_movimenti"; else $TableName="user_movimenti_fiscalizzati";	
    $sql = "SELECT giac_ini,user_schede_rifiuti.originalID_RIF AS ID_RIF, lov_cer.COD_CER,TIPO,quantita,lov_misure.ID_UMIS,lov_misure.description AS misura,peso_spec,descrizione";
    $sql .= " FROM user_schede_rifiuti ";
    $sql .= " LEFT JOIN ".$TableName." ON user_schede_rifiuti.ID_RIF=".$TableName.".ID_RIF AND NMOV<>9999999 AND PerRiclassificazione=0";
    $sql .= " JOIN lov_misure ON user_schede_rifiuti.ID_UMIS=lov_misure.ID_UMIS";
    $sql .= " JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER";
    $sql .= " WHERE user_schede_rifiuti.produttore='1' AND peso_spec<>'0'";
    $sql .= " AND user_schede_rifiuti.ID_IMP='" . $impianto . "' ORDER BY lov_cer.COD_CER ASC,NMOV ASC";
    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);
    if($FEDIT->DbRecsNum>0) {
        $CerBuffer = "";
        $DetTmp = array();
        $SubTot = array();
        foreach($FEDIT->DbRecordSet as $k=>$dati) {
            if($dati["COD_CER"]!=$CerBuffer) {
                $SubTot[$dati["COD_CER"]] = toM3($dati["giac_ini"],$dati["ID_UMIS"],$dati["peso_spec"]);
                $CerBuffer = $dati["COD_CER"];
            }
            if($dati["TIPO"]=="C") {
                $SubTot[$dati["COD_CER"]] +=  toM3($dati["quantita"],$dati["ID_UMIS"],$dati["peso_spec"]);
            } 
            else {
                $SubTot[$dati["COD_CER"]] -=  toM3($dati["quantita"],$dati["ID_UMIS"],$dati["peso_spec"]);
            }
        }
        foreach($SubTot as $cer=>$q) {
            if($q>$CkDepositoTemporaneoM3totali) {
                $DetTmp[$cer] = array($q,$CkDepositoTemporaneoM3totali,"m3");	
            }				
        }
	if(count($DetTmp)>0) {
            $gotAlarms_gestione=true;
            echo "<div class=\"SogerAvvisiTitle\">Deposito Temporaneo - CER (m3 totali)</div>";
            foreach($DetTmp as $k=>$avv) {
                if($k!=''){
                    $avviso = "I rifiuti CER $k hanno superato la giacenza di $CkDepositoTemporaneoM3totali m3 (giacenza attuale: " .  $avv[0] . " m3)";
                    echo "<div class=\"SogerAvvisiItem\">$avviso";
                    echo "</div>";
                }
            }
        }
    }
}


#
#	AVVISI DI CARICO DALL' ULTIMO CARICO
#
if($SOGER->UserData['core_impiantiREG_IND']==1) $TableName="user_movimenti"; else $TableName="user_movimenti_fiscalizzati";	
$sql = "SELECT NMOV,COD_CER,descrizione,user_schede_rifiuti.ID_RIF,C_S_ID,avv_giorni,avv_quantita,user_schede_rifiuti.ID_IMP,DTMOV,TIPO,lov_misure.description FROM user_schede_rifiuti ";
$sql .= "JOIN ".$TableName." ON user_schede_rifiuti.ID_RIF=".$TableName.".ID_RIF JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS ";
$sql .= "JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
$sql .= "WHERE user_schede_rifiuti.ID_IMP='" . $impianto . "'";
$sql .=" AND ".$TableName.".TIPO = 'C' AND NMOV<>9999999 AND PerRiclassificazione=0 ";
$TableSpec = "user_schede_rifiuti.";
include("SOGER_DirectProfilo.php");
unset($TableSpec);
$sql .= " AND C_S_ID<>'1' ORDER BY NMOV ASC";
//print_r($sql);
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
if($FEDIT->DbRecsNum>0) {
    $AvvisiC = array();
    foreach($FEDIT->DbRecordSet as $k=>$v) {
        if($v["C_S_ID"]=="2") {
            # avv dall'ultimo carico
            $differenza = dateDiff("d",date("m/d/Y",strtotime($v["DTMOV"])),date("m/d/Y"));
            if($differenza>=$v["avv_giorni"]) {
                $AvvisiC[$v["ID_RIF"]] = array($v["description"],$v["descrizione"],$v["avv_quantita"],$v["DTMOV"],$v["COD_CER"],$v["ID_RIF"]);
            }
            elseif ($differenza<$v["avv_giorni"]) {
                unset($AvvisiC[$v["ID_RIF"]]);
            }
        }
    }
//print_r($AvvisiC);	
}


#
#	AVVISI DI CARICO DALL' ULTIMO SCARICO
#
if($SOGER->UserData['core_impiantiREG_IND']==1) $TableName="user_movimenti"; else $TableName="user_movimenti_fiscalizzati";	
$sql = "SELECT NMOV,COD_CER,descrizione,user_schede_rifiuti.ID_RIF,C_S_ID,avv_giorni,avv_quantita,user_schede_rifiuti.ID_IMP,DTMOV,TIPO,lov_misure.description FROM user_schede_rifiuti ";
$sql .= "JOIN ".$TableName." ON user_schede_rifiuti.ID_RIF=".$TableName.".ID_RIF JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS ";
$sql .= "JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
$sql .= "WHERE user_schede_rifiuti.ID_IMP='" . $impianto . "' ";
$sql .= "AND ".$TableName.".TIPO = 'S' AND NMOV<>9999999 AND PerRiclassificazione=0 ";
$TableSpec = "user_schede_rifiuti.";
include("SOGER_DirectProfilo.php");
unset($TableSpec);
$sql .= " AND C_S_ID<>'1' ORDER BY NMOV ASC";
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
if($FEDIT->DbRecsNum>0) {
    $AvvisiS = array();
    foreach($FEDIT->DbRecordSet as $k=>$v) {
        if($v["C_S_ID"]=="3") {
            # avv dall'ultimo scarico
            $differenza = dateDiff("d",date("m/d/Y",strtotime($v["DTMOV"])),date("m/d/Y"));
            if($differenza>=$v["avv_giorni"]) {
                $AvvisiS[$v["ID_RIF"]] = array($v["description"],$v["descrizione"],$v["avv_quantita"],$v["DTMOV"],$v["COD_CER"],$v["ID_RIF"] );
            } 
            elseif ($differenza<$v["avv_giorni"]) {
                unset($AvvisiS[$v["ID_RIF"]]);
            }
        }
    }
//print_r($AvvisiS);	
}


#
#	AVVISI RITORNO QUARTA COPIA FORMULARIO
#
if($CkRitornoFormulario=="1" AND $SOGER->UserData["workmode"]=="produttore") {
    $sql = "SELECT ID_MOV_F,NMOV,NFORM,DTFORM,DT_FORM,dt_in_trasp FROM user_movimenti_fiscalizzati ";
    $sql .= "WHERE user_movimenti_fiscalizzati.ID_IMP='" . $impianto . "'";
    $TableSpec = "user_movimenti_fiscalizzati.";
    include("SOGER_DirectProfilo.php");
    $sql .= " AND ( ";
    $sql .= " (user_movimenti_fiscalizzati.DTFORM<>'0000-00-00' AND user_movimenti_fiscalizzati.DTFORM IS NOT NULL) ";
    $sql .= " OR ";
    $sql .= " (user_movimenti_fiscalizzati.dt_in_trasp<>'0000-00-00' AND user_movimenti_fiscalizzati.dt_in_trasp IS NOT NULL) ";
    $sql .= " ) ";
    $sql .= " AND IF ((user_movimenti_fiscalizzati.dt_in_trasp <>'0000-00-00' AND user_movimenti_fiscalizzati.dt_in_trasp IS NOT NULL), dt_in_trasp, DTFORM) <= (SUBDATE(CURDATE(),INTERVAL '$intervalloForm' DAY)) ";
    $sql .= " AND TIPO='S' ";
    $sql .= " AND (DT_FORM='0000-00-00' OR ISNULL(DT_FORM)) ";
    $sql .= " AND SenzaTrasporto = 0 ";
    $sql .= " AND NMOV<>9999999 ";
    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);
    unset($TableSpec);
    if($FEDIT->DbRecsNum>0) {
        $gotAlarms_gestione=true;
        echo "<div class=\"SogerAvvisiTitle\">Ritorno quarta copia Formulario</div>";
	foreach($FEDIT->DbRecordSet as $k=>$dati) {
            $DT_RIFERIMENTO = ($dati['dt_in_trasp']!='0000-00-00' AND !is_null($dati['dt_in_trasp']))? $dati['dt_in_trasp']:$dati['DTFORM'];
            $DTFORM		= explode('-', $DT_RIFERIMENTO);
            $ts_oggi	= mktime(0, 0, 0, date('m'), date('d'), date('Y'));
            $ts_DTFORM	= mktime(0, 0, 0, $DTFORM[1], $DTFORM[2], $DTFORM[0]);
            $seconds	= $ts_oggi - $ts_DTFORM;
            $GiorniTrascorsi = abs(intval($seconds/86400)); 
            if($GiorniTrascorsi<=90)
                $avviso = "La quarta copia del formulario " . $dati["NFORM"] . " del movimento num. " . $dati["NMOV"] . ", emesso ".$GiorniTrascorsi." giorni fa, non � ancora rientrata.";
            else
                $avviso = "La quarta copia del formulario " . $dati["NFORM"] . " del movimento num. " . $dati["NMOV"] . ", emesso ".$GiorniTrascorsi." giorni fa, non � rientrata nei 90 giorni previsti.";
            echo "<div class=\"SogerAvvisiItem\">$avviso</div>";
        }
    }
}


#
#	AVVISI DI CARICO DALL' ULTIMO MOVIMENTO
#
if($SOGER->UserData['core_impiantiREG_IND']==1) $TableName="user_movimenti"; else $TableName="user_movimenti_fiscalizzati";	
$sql = "SELECT NMOV,COD_CER,descrizione,user_schede_rifiuti.ID_RIF,C_S_ID,avv_giorni,avv_quantita,user_schede_rifiuti.ID_IMP,DTMOV,TIPO,lov_misure.description FROM user_schede_rifiuti ";
$sql .= "JOIN ".$TableName." ON user_schede_rifiuti.ID_RIF=".$TableName.".ID_RIF JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS ";
$sql .= "JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
$sql .= "WHERE user_schede_rifiuti.ID_IMP='" . $impianto . "' AND NMOV<>9999999 AND PerRiclassificazione=0 ";
$TableSpec = "user_schede_rifiuti.";
include("SOGER_DirectProfilo.php");
unset($TableSpec);
$sql .= " AND C_S_ID<>'1' ORDER BY NMOV ASC";
//print_r($sql);
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
if($FEDIT->DbRecsNum>0) {
    $AvvisiM = array();
    foreach($FEDIT->DbRecordSet as $k=>$v) {
        if($v["C_S_ID"]=="4") {
            # avv dall'ultimo movimento
            $differenza = dateDiff("d",date("m/d/Y",strtotime($v["DTMOV"])),date("m/d/Y"));
            if($differenza>=$v["avv_giorni"]) {
                $AvvisiM[$v["ID_RIF"]] = array($v["description"],$v["descrizione"],$v["avv_quantita"],$v["DTMOV"],$v["COD_CER"],$v["ID_RIF"]);
            }
            elseif ($differenza<$v["avv_giorni"]) {
                unset($AvvisiM[$v["ID_RIF"]]);
            }
        }
    }
//print_r($AvvisiM);	
}
		
if(isset($AvvisiS) | isset($AvvisiC) | isset($AvvisiM)) {
    $gotAlarms_gestione=true;
    //print_r(count($AvvisiC));
    if(count(@$AvvisiS)>0 | count(@$AvvisiC)>0 | count(@$AvvisiM)>0) {
        if(!isset($_GET["PlainTxt"])) {
            echo "<div class=\"SogerAvvisiTitle\">Avvisi di Carico</div>";
        } 
        else {
            $PlainTxtBuf .= "AVVISI DI CARICO\r\n";
        }
    }
    if(isset($AvvisiC)){
        foreach(@$AvvisiC as $k=>$v) {
            $avviso = "Caricare "  . $v[2] . " " . $v[0] . " del rifiuto \"" . $v[4] . " - " . $v[1] . "\""; 
            echo "<div class=\"SogerAvvisiItem\">$avviso ";
            if($SOGER->UserData['core_impiantiREG_IND']==0) 
                $linkNewC="UserNuovoMovCaricoF";
            else
                $linkNewC="UserNuovoMovCarico"; 
            echo " <a href=\"__scripts/status.php?area=".$linkNewC."&ID_RIF=".$v[5]."&MovimentoRIF_quantita=".$v[2]."\" class=\"Avvisi\">crea carico</a>";
            echo "</div>";
        }
    }
		
    if(isset($AvvisiS)){
        foreach(@$AvvisiS as $k=>$v) {
            $avviso = "Caricare "  . $v[2] . " " . $v[0] . " del rifiuto \"" . $v[4] . " - " . $v[1] . "\""; 
            echo "<div class=\"SogerAvvisiItem\">$avviso ";
            if($SOGER->UserData['core_impiantiREG_IND']==0) 
                $linkNewC="UserNuovoMovCaricoF";
            else
                $linkNewC="UserNuovoMovCarico"; 
            echo " <a href=\"__scripts/status.php?area=".$linkNewC."&ID_RIF=".$v[5]."&MovimentoRIF_quantita=".$v[2]."\" class=\"Avvisi\">crea carico</a>";
            echo "</div>";
        }
    }

    if(isset($AvvisiM)){
        foreach(@$AvvisiM as $k=>$v) {
            $avviso = "Caricare "  . $v[2] . " " . $v[0] . " del rifiuto \"" . $v[4] . " - " . $v[1] . "\""; 
            echo "<div class=\"SogerAvvisiItem\">$avviso ";
            if($SOGER->UserData['core_impiantiREG_IND']==0) 
                $linkNewC="UserNuovoMovCaricoF";
            else
                $linkNewC="UserNuovoMovCarico"; 
            echo " <a href=\"__scripts/status.php?area=".$linkNewC."&ID_RIF=".$v[5]."&MovimentoRIF_quantita=".$v[2]."\" class=\"Avvisi\">crea carico</a>";
            echo "</div>";
        }
    }
	
}

#
#	autorizzazioni DESTINATARI e TRASPORTATORI e INTERMEDIARI e PRODUTTORI
#
if($CkAutorizzazioni=='1') {
    #
    #   autorizzazioni intermediari
    #
    $sql  = "SELECT ADDDATE(CURDATE(),$intervallo) AS dateSpan,user_impianti_intermediari.description AS IMdes,user_aziende_intermediari.description,user_aziende_intermediari.ID_AZI";
    $sql .= ",user_schede_rifiuti.descrizione AS CERdes,COD_CER";
    $sql .= ",ID_AUTHI,user_autorizzazioni_interm.num_aut,user_autorizzazioni_interm.scadenza,user_autorizzazioni_interm.rilascio";
    $sql .= " FROM user_aziende_intermediari JOIN user_impianti_intermediari ON user_impianti_intermediari.ID_AZI=user_aziende_intermediari.ID_AZI";
    $sql .= " JOIN user_autorizzazioni_interm ON user_autorizzazioni_interm.ID_UIMI=user_impianti_intermediari.ID_UIMI";
    $sql .= " JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_autorizzazioni_interm.ID_RIF";
    $sql .= " JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER";
    $sql .= " WHERE (user_autorizzazioni_interm.scadenza< ADDDATE(CURDATE(),$intervallo) OR user_autorizzazioni_interm.scadenza IS NULL OR user_autorizzazioni_interm.scadenza='0000-00-00') ";
    $sql .= " AND user_autorizzazioni_interm.approved=1 ";
    $sql .= " AND user_aziende_intermediari.ID_IMP='" . $impianto . "'";
    $sql .= " AND user_autorizzazioni_interm.ID_AUT='1'"; // --> 0= domanda iscrizione; 1= autorizzazione;
    $sql .= " AND user_schede_rifiuti.approved=1 ";
    $TableSpec = "user_aziende_intermediari.";
    require("SOGER_DirectProfilo.php");

    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);
    if($FEDIT->DbRecsNum>0) {
        $gotAlarms_gestione=true;
        echo "<div class=\"SogerAvvisiTitle\">Autorizzazioni Intermediari</div>";
	foreach($FEDIT->DbRecordSet as $k=>$dati) {
            if($dati["scadenza"]=="0000-00-00") {
                $avviso = " Scadenza autorizzazione mancante: ";	
                }
            else {
                #
                # necessit� di cambiare l'allarme a seconda che l'autorizzazione sia scaduta o in scadenza!
                #	
                $oggi=mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                $scade=explode('-', $dati['scadenza']);
                $scadenza=mktime(0, 0, 0, $scade[1], $scade[2], $scade[0]);
                $differenza=($scadenza - $oggi)/(60*60*24);
                if($differenza>=0){
                    $avviso=" Autorizzazione in scadenza";
                    #$avviso.=" ( il ".date("d/m/Y",strtotime($dati["scadenza"]))." )";
                    $avviso.=": ";
                }
                else
                    $avviso=" Autorizzazione scaduta: ";
            }
            $avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
            $avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"]; 
            $avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
            echo "<div class=\"SogerAvvisiItem\">$avviso";
            echo "</div>";	
        }
    }	
    #
    #	autorizzazioni intermediari - domande di iscrizione
    #
    $sql  = "SELECT ADDDATE(CURDATE(),$intervallo) AS dateSpan,user_impianti_intermediari.description AS IMdes,user_aziende_intermediari.description,user_aziende_intermediari.ID_AZI";
    $sql .= ",user_schede_rifiuti.descrizione AS CERdes,COD_CER";
    $sql .= ",ID_AUTHI,user_autorizzazioni_interm.num_aut,user_autorizzazioni_interm.scadenza,user_autorizzazioni_interm.rilascio";
    $sql .= " FROM user_aziende_intermediari JOIN user_impianti_intermediari ON user_impianti_intermediari.ID_AZI=user_aziende_intermediari.ID_AZI";
    $sql .= " JOIN user_autorizzazioni_interm ON user_autorizzazioni_interm.ID_UIMI=user_impianti_intermediari.ID_UIMI";
    $sql .= " JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_autorizzazioni_interm.ID_RIF";
    $sql .= " JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER";
    $sql .= " WHERE (user_autorizzazioni_interm.scadenza< ADDDATE(CURDATE(),$intervallo) OR user_autorizzazioni_interm.scadenza IS NULL OR user_autorizzazioni_interm.scadenza='0000-00-00') ";
    $sql .= " AND user_autorizzazioni_interm.approved=1 ";
    $sql .= " AND user_aziende_intermediari.ID_IMP='" . $impianto . "'";
    $sql .= " AND user_autorizzazioni_interm.ID_AUT='0'"; // --> 0= domanda iscrizione; 1= autorizzazione;
    $sql .= " AND user_schede_rifiuti.approved=1 ";
    $TableSpec = "user_aziende_intermediari.";
    require("SOGER_DirectProfilo.php");

    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);
    if($FEDIT->DbRecsNum>0) {
        $gotAlarms_gestione=true;
        echo "<div class=\"SogerAvvisiTitle\">Domanda di iscrizione Intermediari</div>";
	foreach($FEDIT->DbRecordSet as $k=>$dati) {
            $oggi		= mktime(0, 0, 0, date('m'), date('d'), date('Y'));
            $rilascio	= explode('-', $dati['rilascio']);
            $rilascio	= mktime(0, 0, 0, $rilascio[1], $rilascio[2], $rilascio[0]);
            $differenza_rilascio=($rilascio - $oggi)/(60*60*24);
            if($dati["scadenza"]=="0000-00-00"){
                if($differenza_rilascio<=60)
                    $avviso = " Sono trascorsi almeno 60 giorni dalla data di presentazione della domanda di iscrizione: ";	
                else
                    $avviso = " Scadenza domanda di iscrizione mancante: ";	
            }
            else {
                #
                # necessit� di cambiare l'allarme a seconda che l'autorizzazione sia scaduta o in scadenza!
                #	
                $scade=explode('-', $dati['scadenza']);
                $scadenza=mktime(0, 0, 0, $scade[1], $scade[2], $scade[0]);
                $differenza=($scadenza - $oggi)/(60*60*24);
                if($differenza>=0)
                    $avviso=" Domanda di iscrizione in scadenza: ";
                else
                    $avviso=" Domanda di iscrizione scaduta: ";
            }
            $avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
            $avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"]; 
            $avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
            echo "<div class=\"SogerAvvisiItem\">$avviso";
            echo "</div>";
	}
    }
    #
    #	autorizzazioni destinatari	
    #
    $sql = "SELECT ADDDATE(CURDATE(),$intervallo) AS dateSpan,user_impianti_destinatari.description AS IMdes,user_aziende_destinatari.description,user_aziende_destinatari.ID_AZD";
    $sql .= ",user_schede_rifiuti.descrizione AS CERdes,COD_CER";
    $sql .= ",ID_AUTHD,user_autorizzazioni_dest.num_aut,user_autorizzazioni_dest.scadenza,user_autorizzazioni_dest.rilascio";
    $sql .= " FROM user_aziende_destinatari JOIN user_impianti_destinatari ON user_impianti_destinatari.ID_AZD=user_aziende_destinatari.ID_AZD";
    $sql .= " JOIN user_autorizzazioni_dest ON user_autorizzazioni_dest.ID_UIMD=user_impianti_destinatari.ID_UIMD";
    $sql .= " JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_autorizzazioni_dest.ID_RIF";
    $sql .= " JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER";
    $sql .= " WHERE (user_autorizzazioni_dest.scadenza< ADDDATE(CURDATE(),$intervallo) OR user_autorizzazioni_dest.scadenza IS NULL OR user_autorizzazioni_dest.scadenza='0000-00-00') ";
    $sql .= " AND user_autorizzazioni_dest.approved=1 ";
    $sql .= " AND user_aziende_destinatari.ID_IMP='" . $impianto . "'";
    $sql .= " AND user_schede_rifiuti.approved=1 ";
    $TableSpec = "user_aziende_destinatari.";
    require("SOGER_DirectProfilo.php");

    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);
    if($FEDIT->DbRecsNum>0) {
        $gotAlarms_gestione=true;
        echo "<div class=\"SogerAvvisiTitle\">Autorizzazioni Destinatari</div>";
	foreach($FEDIT->DbRecordSet as $k=>$dati) {
            if($dati["scadenza"]=="0000-00-00") {
                $avviso = " Scadenza Autorizzazione mancante: ";	
            }
            else {
                #
                # necessit� di cambiare l'allarme a seconda che l'autorizzazione sia scaduta o in scadenza!
                #	
                $oggi=mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                $scade=explode('-', $dati['scadenza']);
                $scadenza=mktime(0, 0, 0, $scade[1], $scade[2], $scade[0]);
                $differenza=($scadenza - $oggi)/(60*60*24);
                if($differenza>=0){
                    $avviso=" Autorizzazione in scadenza";
                    #$avviso.=" ( il ".date("d/m/Y",strtotime($dati["scadenza"]))." )";
                    $avviso.=": ";
                }
                else
                    $avviso=" Autorizzazione scaduta: ";
            }
            $avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
            $avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"]; 
            $avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
            echo "<div class=\"SogerAvvisiItem\">$avviso";
            echo "</div>";	
        }
    }	
    #
    #	autorizzazioni trasportatori	
    #
    $sql = "SELECT ADDDATE(CURDATE(),$intervallo) AS dateSpan,user_impianti_trasportatori.description AS IMdes,user_aziende_trasportatori.description,user_aziende_trasportatori.ID_AZT";
    $sql .= ",user_schede_rifiuti.descrizione AS CERdes,COD_CER";
    $sql .= ",ID_AUTHT,user_autorizzazioni_trasp.num_aut,user_autorizzazioni_trasp.scadenza,user_autorizzazioni_trasp.rilascio";
    $sql .= " FROM user_aziende_trasportatori JOIN user_impianti_trasportatori ON user_impianti_trasportatori.ID_AZT=user_aziende_trasportatori.ID_AZT";
    $sql .= " JOIN user_autorizzazioni_trasp ON user_autorizzazioni_trasp.ID_UIMT=user_impianti_trasportatori.ID_UIMT";
    $sql .= " JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_autorizzazioni_trasp.ID_RIF";
    $sql .= " JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER";
    $sql .= " WHERE (user_autorizzazioni_trasp.scadenza< ADDDATE(CURDATE(),$intervallo) OR user_autorizzazioni_trasp.scadenza IS NULL OR user_autorizzazioni_trasp.scadenza='0000-00-00') ";
    $sql .= " AND user_autorizzazioni_trasp.approved=1 ";
    $sql .= " AND user_aziende_trasportatori.ID_IMP='" . $impianto . "'";
    $sql .= " AND user_schede_rifiuti.approved=1 ";
    $TableSpec = "user_aziende_trasportatori.";
    require("SOGER_DirectProfilo.php");
    unset($TableSpec);

    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);
    if($FEDIT->DbRecsNum>0) {
        $gotAlarms_gestione=true;
        echo "<div class=\"SogerAvvisiTitle\">Autorizzazioni Trasportatori</div>";
        foreach($FEDIT->DbRecordSet as $k=>$dati) {
            if($dati["scadenza"]=="0000-00-00") {
                $avviso = " Scadenza Numero Albo mancante: ";	
            }
            else {
                #
                # necessit� di cambiare l'allarme a seconda che l'autorizzazione sia scaduta o in scadenza!
                #	
                $oggi=mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                $scade=explode('-', $dati['scadenza']);
                $scadenza=mktime(0, 0, 0, $scade[1], $scade[2], $scade[0]);
                $differenza=($scadenza - $oggi)/(60*60*24);
                if($differenza>=0){
                    $avviso=" Numero albo in scadenza";
                    #$avviso.=" ( il ".date("d/m/Y",strtotime($dati["scadenza"]))." )";
                    $avviso.=": ";
                    }
                else
                    $avviso=" Numero albo scaduto: ";
            }
            $avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
            $avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"]; 
            $avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
            echo "<div class=\"SogerAvvisiItem\">$avviso";
            echo "</div>";
        }
    }
    #
    #	autorizzazioni produttori	
    #
    $sql = "SELECT ADDDATE(CURDATE(),$intervallo) AS dateSpan,user_impianti_produttori.description AS IMdes,user_aziende_produttori.description,user_aziende_produttori.ID_AZP";
    $sql .= ",user_schede_rifiuti.descrizione AS CERdes,COD_CER";
    $sql .= ",ID_AUTH,user_autorizzazioni_pro.num_aut,user_autorizzazioni_pro.scadenza,user_autorizzazioni_pro.rilascio";
    $sql .= " FROM user_aziende_produttori JOIN user_impianti_produttori ON user_impianti_produttori.ID_AZP=user_aziende_produttori.ID_AZP";
    $sql .= " JOIN user_autorizzazioni_pro ON user_autorizzazioni_pro.ID_UIMP=user_impianti_produttori.ID_UIMP";
    $sql .= " JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_autorizzazioni_pro.ID_RIF";
    $sql .= " JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER";
    $sql .= " WHERE (user_autorizzazioni_pro.scadenza< ADDDATE(CURDATE(),$intervallo) OR user_autorizzazioni_pro.scadenza IS NULL OR user_autorizzazioni_pro.scadenza='0000-00-00') ";
    $sql .= " AND user_autorizzazioni_pro.approved=1 ";
    $sql .= " AND user_aziende_produttori.ID_IMP='" . $impianto . "'";
    $sql .= " AND user_schede_rifiuti.approved=1 ";
    $TableSpec = "user_aziende_produttori.";
    require("SOGER_DirectProfilo.php");
    unset($TableSpec);
    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);
    if($FEDIT->DbRecsNum>0) {
        $gotAlarms_gestione=true;
        echo "<div class=\"SogerAvvisiTitle\">Autorizzazioni Produttori</div>";
        foreach($FEDIT->DbRecordSet as $k=>$dati) {
            if($dati["scadenza"]=="0000-00-00") {
                $avviso = " Scadenza autorizzazioni: ";	
            }
            else {
                #
                # necessit� di cambiare l'allarme a seconda che l'autorizzazione sia scaduta o in scadenza!
                #	
                $oggi=mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                $scade=explode('-', $dati['scadenza']);
                $scadenza=mktime(0, 0, 0, $scade[1], $scade[2], $scade[0]);
                $differenza=($scadenza - $oggi)/(60*60*24);
                if($differenza>=0){
                    $avviso=" Autorizzazione in scadenza";
                    #$avviso.=" ( il ".date("d/m/Y",strtotime($dati["scadenza"]))." )";
                    $avviso.=": ";
                    }
                else
                    $avviso=" Autorizzazione scaduta: ";
            }
            $avviso .= $dati["num_aut"] . " del " . date("d/m/Y",strtotime($dati["rilascio"]));
            $avviso .= " - CER " . $dati["COD_CER"] . " - " . $dati["CERdes"]; 
            $avviso .= " (" . $dati["description"] . ", impianto di ". $dati["IMdes"] . ") ";
            echo "<div class=\"SogerAvvisiItem\">$avviso";
            echo "</div>";
        }
    }
}


#
#	PATENTI ADR
#
$sql = "SELECT user_autisti.ID_AUTST,nome,user_autisti.description AS cognome,rilascio,scadenza,user_aziende_trasportatori.description as AZdes,user_impianti_trasportatori.description AS IMPdes";
$sql .= " FROM user_autisti ";
$sql .= " JOIN user_impianti_trasportatori ON user_impianti_trasportatori.ID_UIMT=user_autisti.ID_UIMT";
$sql .= " JOIN user_aziende_trasportatori ON user_aziende_trasportatori.ID_AZT=user_impianti_trasportatori.ID_AZT";
$sql .= " WHERE (scadenza<'" . date("Y-m-d") . "' OR scadenza='0000-00-00') AND (patente<>'' AND NOT ISNULL(patente)) ";
$sql .= " AND adr='1'";	
$sql .= " AND ID_IMP='" . $impianto . "'";
$TableSpec="user_aziende_trasportatori.";
require("SOGER_DirectProfilo.php");
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
if($FEDIT->DbRecsNum>0) {
    $gotAlarms_altri=true;
    echo "<div class=\"SogerAvvisiTitle\">Patenti ADR</div>";
    $RecordsToUpdate = array();
    foreach($FEDIT->DbRecordSet as $k=>$dati) {
        $RecordsToUpdate[] = $dati["ID_AUTST"];
        $avviso = " La Patente ADR di ";
        $avviso .= $dati["cognome"] . " " . $dati["nome"];
        $avviso .= " (" . $dati["AZdes"] . ", impianto di " . $dati["IMPdes"] . ")";
        if($dati["scadenza"]=="0000-00-00") 
            $avviso .= " non ha indicazione di scadenza";	
        else 
            $avviso .= " � scaduta";
        echo "<div class=\"SogerAvvisiItem\">$avviso";
        echo "</div>";
    }
}


if(!$gotAlarms_gestione)
    echo "<div class=\"SogerAvvisiItem\" style=\"width: 50%; margin:5px;\">Nessun allarme</div>";

echo "</div>";

############################ ALLARMI DI MOVIMENTAZIONE ############################

echo '<div class="divName" id="SogerAvvisiAltri"><span>ALLARMI DI MOVIMENTAZIONE</span></div>';

echo '<div id="SogerAvvisiAltriContainer" style="display:none;">';


#
#	Schede rifiuto senza nuova classificazione
#
$sql ="SELECT descrizione, lov_cer.COD_CER ";
$sql.="FROM user_schede_rifiuti JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
$sql.="WHERE ".$SOGER->UserData['workmode']."='1' AND ID_IMP='".$impianto."' ";
$sql.="AND ClassificazioneHP=0 AND approved=1 ORDER BY COD_CER";
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

if($FEDIT->DbRecsNum>0) {
    $gotAlarms_altri=true;
    echo "<div class=\"SogerAvvisiTitle\">Rifiuti da classificare secondo Reg. UE 1357/2014</div>";                        
    for($d=0;$d<count($FEDIT->DbRecordSet);$d++){            
        $avviso ="Il rifiuto ".$FEDIT->DbRecordSet[$d]['COD_CER']." - ".$FEDIT->DbRecordSet[$d]['descrizione']." deve essere riclassificato come disposto dal Regolamento UE 1357/2014.<br />";
        echo "<div class=\"SogerAvvisiItem\">$avviso";
        echo "</div>";
    }
}


#
#	Schede rifiuto in ADR (n.a.s., no 2.1.3.5.5) senza indicazione componenti pericolosi
#
$sql ="SELECT descrizione, lov_cer.COD_CER, lov_num_onu.nas, lov_num_onu.disposizioni_speciali ";
$sql.="FROM user_schede_rifiuti JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
$sql.="JOIN lov_num_onu ON lov_num_onu.ID_ONU=user_schede_rifiuti.ID_ONU ";
$sql.="WHERE ".$SOGER->UserData['workmode']."='1' AND ID_IMP='".$impianto."' ";
$sql.="AND adr=1 AND nas=1 AND (disposizioni_speciali LIKE '%274%' OR disposizioni_speciali LIKE '%318%') AND ADR_2_1_3_5_5=0 AND trim(ONU_per)='' AND approved=1 ORDER BY COD_CER";
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

if($FEDIT->DbRecsNum>0) {
    $gotAlarms_altri=true;
    echo "<div class=\"SogerAvvisiTitle\">Rifiuti in ADR</div>";
    for($d=0;$d<count($FEDIT->DbRecordSet);$d++){
        $avviso ="Il rifiuto ".$FEDIT->DbRecordSet[$d]['COD_CER']." - ".$FEDIT->DbRecordSet[$d]['descrizione']." � sottoposto alla normativa ADR e gli � stato attribuito un numero ONU che richiede obbligatoriamente l'indicazione dei componenti pericolosi.<br />";
        echo "<div class=\"SogerAvvisiItem\">$avviso";
        echo "</div>";
    }
}


#
#	Formulari senza numero di formulario
#
if($SOGER->UserData['workmode']=='produttore'){
    $sql ="SELECT NMOV FROM user_movimenti_fiscalizzati ";
    $sql.="WHERE ".$SOGER->UserData['workmode']."='1' AND ID_IMP='".$impianto."' ";
    $sql.="AND PerRiclassificazione=0 AND NMOV<>9999999 AND TIPO='S' AND SenzaTrasporto=0 AND TIPO_S_INTERNO=0 AND (TRIM(NFORM)='' OR NFORM IS NULL) AND (TRIM(N_ANNEX_VII)='' OR N_ANNEX_VII IS NULL) ";
    $sql.="ORDER BY NMOV ASC ";
    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);

    if($FEDIT->DbRecsNum>0) {
        $gotAlarms_altri=true;
        echo "<div class=\"SogerAvvisiTitle\">Movimenti senza numero di formulario</div>";
        for($d=0;$d<count($FEDIT->DbRecordSet);$d++){
            $avviso ="Il movimento numero ".$FEDIT->DbRecordSet[$d]['NMOV']." fa riferimento ell'emissione di un formulario per il quale non � stato indicato il numero.<br />";
            echo "<div class=\"SogerAvvisiItem\">$avviso";
            echo "</div>";
        }
    }
}


#
#	MOVIMENTI DOPPI - fiscale
#
$sql ="SELECT NMOV, ID_IMP, COUNT(ID_MOV_F) AS quanti FROM user_movimenti_fiscalizzati WHERE ".$SOGER->UserData['workmode']."='1' AND ID_IMP='".$impianto."' AND NMOV<>9999999 GROUP BY NMOV, ID_IMP HAVING quanti > 1";
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

if($FEDIT->DbRecsNum>0) {
    $gotAlarms_altri=true;
    echo "<div class=\"SogerAvvisiTitle\">Elenco Movimenti Fiscali</div>";
    for($d=0;$d<count($FEDIT->DbRecordSet);$d++){
	$avviso ="Attenzione, movimento numero ".$FEDIT->DbRecordSet[$d]['NMOV']." duplicato su registro fiscale. Contattare l' assistenza prima di stampare il registro.<br />";
        echo "<div class=\"SogerAvvisiItem\">$avviso";
        echo "</div>";
    }
}


#
#	MOVIMENTI DOPPI - industriale
#
$sql ="SELECT NMOV, ID_IMP, COUNT(ID_MOV) AS quanti FROM user_movimenti WHERE ".$SOGER->UserData['workmode']."='1' AND ID_IMP='".$impianto."' AND NMOV<>9999999 GROUP BY NMOV, ID_IMP HAVING quanti > 1";
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

if($FEDIT->DbRecsNum>0) {
    $gotAlarms_altri=true;
    echo "<div class=\"SogerAvvisiTitle\">Elenco Movimenti Industriali</div>";
    for($d=0;$d<count($FEDIT->DbRecordSet);$d++){
	$avviso ="Attenzione, movimento numero ".$FEDIT->DbRecordSet[$d]['NMOV']." duplicato su registro industriale. Contattare l' assistenza prima di stampare il registro.<br />";
        echo "<div class=\"SogerAvvisiItem\">$avviso";
        echo "</div>";
    } 
}


#
#	FIRMA REGISTRAZIONI CRONOLOGICHE SISTRI
#
if($SOGER->UserData['workmode']=='produttore' AND $SOGER->UserData['core_impiantiMODULO_SIS']=='1' AND $CkMOV_SISTRI=='1'){
    $sql  = "SELECT NMOV, DTMOV, dataRegistrazioniCrono_invio, statoRegistrazioniCrono ";
    $sql .= "FROM user_movimenti_fiscalizzati JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_movimenti_fiscalizzati.ID_RIF ";
    $sql .= "WHERE (statoRegistrazioniCrono='NON FIRMATA' OR statoRegistrazioniCrono IS NULL) ";
    $sql .= "AND user_movimenti_fiscalizzati.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' ";
    $sql .= "AND user_movimenti_fiscalizzati.produttore=1 AND pericoloso=1 AND DTMOV>'2014-02-28' ";
    $INTERVAL = 10 - $intervalloMOV_SISTRI;
    $sql .= "AND (CURDATE()>=DATE_ADD(DTMOV, INTERVAL ".$INTERVAL." DAY)) ";
    $sql .= "AND NMOV<>9999999 ";
    $sql .= "AND (PerRiclassificazione=0 OR TIPO='C') ";
    $sql .= "ORDER BY DTMOV, dataRegistrazioniCrono_invio, NMOV";

    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);
    if($FEDIT->DbRecsNum>0) {
        $gotAlarms_altri=true;
	echo "<div class=\"SogerAvvisiTitle\">Firma registrazioni cronologiche SISTRI</div>";
        foreach($FEDIT->DbRecordSet as $k=>$dati) {
            $oggi   = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
            if(is_null($dati['statoRegistrazioniCrono'])){
                $operazione		= "trasmettere e firmare";
                $dataMovimento	= explode('-', $dati['DTMOV']);
                $giornoInvio	= mktime(0, 0, 0, $dataMovimento[1], $dataMovimento[2], $dataMovimento[0]);
            }
            else{
                $operazione		= "firmare";
                $dataRegistrazioniCrono_invio	= explode('-', $dati['dataRegistrazioniCrono_invio']);
                $giornoInvio	= mktime(0, 0, 0, $dataRegistrazioniCrono_invio[1], $dataRegistrazioniCrono_invio[2], $dataRegistrazioniCrono_invio[0]);
            }
            $differenza	= 10 - ($oggi - $giornoInvio)/(60*60*24);
            if($differenza>=0){
                if($differenza<=$intervalloMOV_SISTRI)
                    $avviso =" Attenzione, restano ".round(abs($differenza))." giorni per ".$operazione." la registrazione cronologica relativa al movimento numero ".$dati['NMOV'].".";
            }
            else{
                $avviso =" Attenzione, da ".round(abs($differenza))." giorni � superato il termine entro il quale ".$operazione." la registrazione cronologica relativa al movimento numero ".$dati['NMOV'].".";
            }
            echo "<div class=\"SogerAvvisiItem\">$avviso";
            echo "</div>";
        }
    }
}

#
#	FIRMA SCHEDE SISTRI
#
if($SOGER->UserData['workmode']=='produttore' AND $SOGER->UserData['core_impiantiMODULO_SIS']=='1' AND $CkMOV_SISTRI=='1'){
    $sql  = "SELECT NMOV, DTMOV, dataSchedaSistri_invio, statoSchedaSistri ";
    $sql .= "FROM user_movimenti_fiscalizzati JOIN user_schede_rifiuti ON user_schede_rifiuti.ID_RIF=user_movimenti_fiscalizzati.ID_RIF ";
    $sql .= "WHERE (statoSchedaSistri='NON FIRMATA' OR statoSchedaSistri IS NULL) ";
    $sql .= "AND user_movimenti_fiscalizzati.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND TIPO='S' ";
    $sql .= "AND user_movimenti_fiscalizzati.produttore=1 AND pericoloso=1 AND DTMOV>'2014-02-28' ";
    $INTERVAL = 10 - $intervalloMOV_SISTRI;
    $sql .= "AND (CURDATE()>=DATE_ADD(DTMOV, INTERVAL ".$INTERVAL." DAY)) ";
    $sql .= "AND PerRiclassificazione=0 ";
    $sql .= "ORDER BY DTMOV, dataSchedaSistri_invio, NMOV";

    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);
    if($FEDIT->DbRecsNum>0) {
        $gotAlarms_altri=true;
        echo "<div class=\"SogerAvvisiTitle\">Firma schede SISTRI</div>";
        foreach($FEDIT->DbRecordSet as $k=>$dati) {
            $oggi = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
            if(is_null($dati['statoSchedaSistri'])){
                $operazione = "trasmettere e firmare";
                $dataMovimento = explode('-', $dati['DTMOV']);
		$giornoInvio = mktime(0, 0, 0, $dataMovimento[1], $dataMovimento[2], $dataMovimento[0]);
            }
            else{
                $operazione = "firmare";
                $dataSchedaSistri_invio	= explode('-', $dati['dataSchedaSistri_invio']);
                $giornoInvio = mktime(0, 0, 0, $dataSchedaSistri_invio[1], $dataSchedaSistri_invio[2], $dataSchedaSistri_invio[0]);
            }
            $differenza	= 10 - ($oggi - $giornoInvio)/(60*60*24);
            if($differenza>=0){
                if($differenza<=$intervalloMOV_SISTRI)
                    $avviso.=" Attenzione, restano ".round(abs($differenza))." giorni per ".$operazione." la scheda SISTRI relativa al movimento numero ".$dati['NMOV'].".";
            }
            else{
                $avviso.=" Attenzione, da ".round(abs($differenza))." giorni � superato il termine entro il quale ".$operazione." la scheda SISTRI relativa al movimento numero ".$dati['NMOV'].".";
                }
            echo "<div class=\"SogerAvvisiItem\">$avviso";
            echo "</div>";
        }
    }
}


#
#	SCADENZA APPROVAZIONE CARATTERIZZAZIONI
#
$sql ="SELECT lov_cer.COD_CER, ID_RIF, descrizione, CAR_approved_start, CAR_approved_end ";
$sql.="FROM user_schede_rifiuti ";
$sql.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
$sql.="WHERE ID_IMP='".$impianto."' ";
$sql.="AND CAR_approved_end<NOW() ";
$TableSpec = "user_schede_rifiuti.";
require("SOGER_DirectProfilo.php");
unset($TableSpec);

$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
if($FEDIT->DbRecsNum>0) {
    $gotAlarms_altri=true;
    echo "<div class=\"SogerAvvisiTitle\">Approvazione caratterizzazioni</div>";
    foreach($FEDIT->DbRecordSet as $k=>$dati) {
        $ScadenzaApprovazione_array = explode("-",$dati['CAR_approved_end']);
        $ScadenzaApprovazione = (int)$ScadenzaApprovazione_array[2]."/".(int)$ScadenzaApprovazione_array[1]."/".(int)$ScadenzaApprovazione_array[0];
        $avviso = "L'approvazione della caratterizzazione per il rifiuto ".$dati['COD_CER']." ".$dati['descrizione']." � scaduta il ".$ScadenzaApprovazione;
        echo "<div class=\"SogerAvvisiItem\">$avviso";
        echo "</div>";
    }
}


#
#	SCADENZA APPROVAZIONE CLASSIFICAZIONI
#
$sql ="SELECT lov_cer.COD_CER, ID_RIF, descrizione, CLASS_approved_start, CLASS_approved_end ";
$sql.="FROM user_schede_rifiuti ";
$sql.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
$sql.="WHERE ID_IMP='".$impianto."' ";
$sql.="AND CLASS_approved_end<NOW() ";
$TableSpec = "user_schede_rifiuti.";
require("SOGER_DirectProfilo.php");
unset($TableSpec);

$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
if($FEDIT->DbRecsNum>0) {
    $gotAlarms_altri=true;
    echo "<div class=\"SogerAvvisiTitle\">Approvazione classificazioni</div>";
    foreach($FEDIT->DbRecordSet as $k=>$dati) {
        $ScadenzaApprovazione_array = explode("-",$dati['CLASS_approved_end']);
        $ScadenzaApprovazione = (int)$ScadenzaApprovazione_array[2]."/".(int)$ScadenzaApprovazione_array[1]."/".(int)$ScadenzaApprovazione_array[0];
        $avviso = "L'approvazione della classificazione per il rifiuto ".$dati['COD_CER']." ".$dati['descrizione']." � scaduta il ".$ScadenzaApprovazione;
        echo "<div class=\"SogerAvvisiItem\">$avviso";
        echo "</div>";
    }
}


#
#	SCADENZA APPROVAZIONE ETICHETTE
#
$sql ="SELECT lov_cer.COD_CER, ID_RIF, descrizione, et_approved_start, et_approved_end ";
$sql.="FROM user_schede_rifiuti ";
$sql.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
$sql.="WHERE ID_IMP='".$impianto."' ";
$sql.="AND et_approved_end<NOW() ";
$TableSpec = "user_schede_rifiuti.";
require("SOGER_DirectProfilo.php");
unset($TableSpec);

$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
if($FEDIT->DbRecsNum>0) {
    $gotAlarms_altri=true;
    echo "<div class=\"SogerAvvisiTitle\">Approvazione etichette</div>";
    foreach($FEDIT->DbRecordSet as $k=>$dati) {
        $ScadenzaApprovazione_array = explode("-",$dati['et_approved_end']);
        $ScadenzaApprovazione = (int)$ScadenzaApprovazione_array[2]."/".(int)$ScadenzaApprovazione_array[1]."/".(int)$ScadenzaApprovazione_array[0];
        $avviso = "L'approvazione dell'etichetta per il rifiuto ".$dati['COD_CER']." ".$dati['descrizione']." � scaduta il ".$ScadenzaApprovazione;
        //$sql = "UPDATE user_schede_rifiuti SET et_approved=0 WHERE ID_RIF=".$dati['ID_RIF'];
        //$FEDIT->SDBWrite($sql,"DbRecordSetUpd",false,false);
        echo "<div class=\"SogerAvvisiItem\">$avviso";
        echo "</div>";
    }
}


#
#	SCADENZA APPROVAZIONE PROCEDURE DI SICUREZZA
#
$sql ="SELECT lov_cer.COD_CER, ID_RIF, descrizione, sk_approved_start, sk_approved_end ";
$sql.="FROM user_schede_rifiuti ";
$sql.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
$sql.="WHERE ID_IMP='".$impianto."' ";
$sql.="AND sk_approved_end<NOW() ";
$TableSpec = "user_schede_rifiuti.";
require("SOGER_DirectProfilo.php");
unset($TableSpec);

$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
if($FEDIT->DbRecsNum>0) {
    $gotAlarms_altri=true;
    echo "<div class=\"SogerAvvisiTitle\">Approvazione procedure di sicurezza</div>";
    foreach($FEDIT->DbRecordSet as $k=>$dati) {
        $ScadenzaApprovazione_array = explode("-",$dati['sk_approved_end']);
        $ScadenzaApprovazione = (int)$ScadenzaApprovazione_array[2]."/".(int)$ScadenzaApprovazione_array[1]."/".(int)$ScadenzaApprovazione_array[0];
        $avviso = "L'approvazione della procedura di sicurezza per il rifiuto ".$dati['COD_CER']." ".$dati['descrizione']." � scaduta il ".$ScadenzaApprovazione;
        echo "<div class=\"SogerAvvisiItem\">$avviso";
        echo "</div>";
    }
}




#
#	SCADENZA APPROVAZIONE ANALISI
#
$sql ="SELECT lov_cer.COD_CER, ID_RIF, descrizione, CAR_Analisi_approved_start, CAR_Analisi_approved_end ";
$sql.="FROM user_schede_rifiuti ";
$sql.="JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
$sql.="WHERE ID_IMP='".$impianto."' ";
$sql.="AND CAR_Analisi_approved_end<NOW() ";
$TableSpec = "user_schede_rifiuti.";
require("SOGER_DirectProfilo.php");
unset($TableSpec);

$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
if($FEDIT->DbRecsNum>0) {
    $gotAlarms_altri=true;
    echo "<div class=\"SogerAvvisiTitle\">Approvazione analisi</div>";
    foreach($FEDIT->DbRecordSet as $k=>$dati) {
        $ScadenzaApprovazione_array = explode("-",$dati['CAR_Analisi_approved_end']);
        $ScadenzaApprovazione = (int)$ScadenzaApprovazione_array[2]."/".(int)$ScadenzaApprovazione_array[1]."/".(int)$ScadenzaApprovazione_array[0];
        $avviso = "L'approvazione dell'analisi per il rifiuto ".$dati['COD_CER']." ".$dati['descrizione']." � scaduta il ".$ScadenzaApprovazione;
        echo "<div class=\"SogerAvvisiItem\">$avviso";
        echo "</div>";
    }
}


#
#	CONTRIBUTI ANNUALI
#
if($CkContributi=='1') {
    #
    #	trasportatori 
    #
    $sql = "SELECT description,ID_AZT,contributo";
    $sql .= " FROM user_aziende_trasportatori ";
    $sql .= " WHERE contributo<'" . date("Y-m-d") . "' AND contributo<>'0000-00-00'";
    $sql .= " AND esenzione_contributo=0 ";
    $sql .= " AND ID_IMP='" . $impianto . "'";
    $TableSpec='user_aziende_trasportatori.';
    require("SOGER_DirectProfilo.php");
    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);
    if($FEDIT->DbRecsNum>0) {
        $gotAlarms_altri=true;
        echo "<div class=\"SogerAvvisiTitle\">Contributi Trasportatori</div>";
        foreach($FEDIT->DbRecordSet as $k=>$dati) {
            $avviso = "Il contributo di " . $dati["description"] . " � scaduto ";			
            echo "<div class=\"SogerAvvisiItem\">$avviso";
            echo "</div>";
        }
    }
    #
    #	destinatari 
    #
    $sql = "SELECT description,ID_AZD,contributo";
    $sql .= " FROM user_aziende_destinatari ";
    $sql .= " WHERE contributo<'" . date("Y-m-d") . "' AND contributo<>'0000-00-00'";
    $sql .= " AND esenzione_contributo=0 ";
    $sql .= " AND ID_IMP='" . $impianto . "'";
    $TableSpec='user_aziende_destinatari.';
    require("SOGER_DirectProfilo.php");
    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);
    if($FEDIT->DbRecsNum>0) {
        $gotAlarms_altri=true;
        echo "<div class=\"SogerAvvisiTitle\">Contributi Destinatari</div>";
        foreach($FEDIT->DbRecordSet as $k=>$dati) {
            $avviso = "Il contributo di " . $dati["description"] . " � scaduto ";
            echo "<div class=\"SogerAvvisiItem\">$avviso";
            echo "</div>";
        }	
    }
    #
    #	intermediari 
    #
    $sql = "SELECT description,ID_AZI,contributo";
    $sql .= " FROM user_aziende_intermediari ";
    $sql .= " WHERE contributo<'" . date("Y-m-d") . "' AND contributo<>'0000-00-00'";
    $sql .= " AND esenzione_contributo=0 ";
    $sql .= " AND ID_IMP='" . $impianto . "'";
    $TableSpec='user_aziende_intermediari.';
    require("SOGER_DirectProfilo.php");
    $FEDIT->SDBRead($sql,"DbRecordSet",true,false);
    if($FEDIT->DbRecsNum>0) {
        $gotAlarms_altri=true;
        echo "<div class=\"SogerAvvisiTitle\">Contributi Intermediari</div>";
        foreach($FEDIT->DbRecordSet as $k=>$dati) {
            $avviso = "Il contributo di " . $dati["description"] . " � scaduto ";
            echo "<div class=\"SogerAvvisiItem\">$avviso";
            echo "</div>";
        }
    }

}

if(!$gotAlarms_altri)
    echo "<div class=\"SogerAvvisiItem\" style=\"width: 50%; margin:5px;\">Nessun allarme</div>";

echo "</div>";

echo '<div class="divName" id="SogerAvvisiDownload"><span>SCARICA GLI ALLARMI</span></div>';

//echo "<div class=\"divName\">\n";
//
//echo "<div><a style=\"text-decoration:none;color:#ffffff;\" href=\"__scripts/ListaControlloAllarmi.php\">\n";
//echo "<img style=\"margin-top:4px;margin-left:5px;\" class=\"NoBorder\" src=\"__css/disk.png\" alt=\"Scarica gli allarmi\" title=\"Scarica gli allarmi\" />\n";
//echo "</a></div>\n";
//
//echo "<div style=\"margin-top:4px;margin-left:5px;\"><a style=\"text-decoration:none;font-weight:bold;color:#ffffff;\" href=\"__scripts/ListaControlloAllarmi.php\">\n";
//echo "Scarica gli allarmi\n";
//echo "</a></div>\n";
//
//echo "</div>";

function CheckCarichi($rif, $CarichiRifiuto){

    if(isset($rif['totS'])) $TotS=$rif['totS']; else $TotS=0;
    $giacOK=false;
    if(($rif['totC']+$rif['giac_ini'])>$rif['totS']){

        # 0 - giacenza iniziale
        if($rif['giac_ini']>0 && !$giacOK){
            $CarichiRifiuto[$rif['RifID']][0]['qta']=$rif['giac_ini'];
            $CarichiRifiuto[$rif['RifID']][0]['num']=0;
            $CarichiRifiuto[$rif['RifID']][0]['data']=date("Y")."-01-01";
            $giacOK=true;
        }
        # 1 - array carichi rifiuto
        if(isset($rif['carico'])){
            for($k=0;$k<count($rif['carico']);$k++){
                if(isset($CarichiRifiuto[$rif['RifID']])) $index=count($CarichiRifiuto[$rif['RifID']]); else $index=0;
                $CarichiRifiuto[$rif['RifID']][$index]['qta']=$rif['carico'][$k]['qta'];
                $CarichiRifiuto[$rif['RifID']][$index]['num']=$rif['carico'][$k]['num'];
                $CarichiRifiuto[$rif['RifID']][$index]['data']=$rif['carico'][$k]['data'];
            }
        }
	
        # 2 - sottraggo qta da scaricato fino a quando scaricato resta positivo
        for($i=0;$i<count($CarichiRifiuto[$rif['RifID']]);$i++){
            //print_r("TotS=".var_dump($TotS)." tolgo ".var_dump($CarichiRifiuto[$rif['RifID']][$i]['qta'])."<br />");
            if($TotS>=0){
                $TotS=round($TotS, 2)-round($CarichiRifiuto[$rif['RifID']][$i]['qta'], 2);
                //print_r("TotS=".var_dump($TotS)."<br />");
                if($TotS<0)
                    $lastCarico=$CarichiRifiuto[$rif['RifID']][$i]['num'];
                $lastCaricoData=$CarichiRifiuto[$rif['RifID']][$i]['data'];
            }
        }

        if(isset($lastCarico) && !is_null($lastCarico)){
            return(@$lastCarico."|".@$lastCaricoData."|".abs(@$TotS)."|".$rif["COD_CER"] . " - " . $rif["descrizione"]."|".$rif["t_limite"]."|".$rif["RifID"]."|".$rif["pericoloso"]);
            //print_r("RIF ".$rif['RifID'].", devo scaricare dal carico: ".$lastCarico. " Kg ".abs($TotS)."<br/>\n\r");
        }
    }
}

function toM3($in,$Um,$PesoSpec) {
    if($in=="0")
        return 0;
    switch($Um) {
        case "3": # m3
            return $in;
            break;
        case "2": # Litri->M3
            $tmp = $in*$PesoSpec;
            return round(($tmp/$PesoSpec/1000),2);
            break;
        case "1": # Kg->M3
            if($PesoSpec==0) $PesoSpec=1;
            return round(($in/$PesoSpec/1000),2);
            break;
    }
}
?>