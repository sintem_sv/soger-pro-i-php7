<form name="EtichetteInterface" id="EtichetteInterface" action="">
<input type="hidden" name="ID_RIF" value="" />
<div id="SOGER_EtichetteInterface" style="display:none;padding:2px;">

	<h3>Configura la tua etichetta</h3>

	<table>

		<tr>
			<td style="">Scegli quali pittogrammi utilizzare:</td>
			<td style="text-align:right;">
				<select id="REGOLAMENTO">
				<!--<option value="0">Direttiva 67/548/CEE</option>-->
				<option value="1" selected>Regolamento CE 1272/2008</option>
				</select>
			</td>
		</tr>

		<tr>
			<td style="">Scegli la modalit&aacute; di stampa:</td>
			<td style="text-align:right;">
				<select id="COLORI">
				<option value="0">Stampa in bianco e nero</option>
				<option value="1" selected>Stampa a colori</option>
				</select>
			</td>
		</tr>


	</table>

	<!--

	<div id="SOGER_EtichetteColoriNO" style="color: white; width: 298px; height:100%; text-align:center; border: 1px SOLID black; background-color: black; float: left;">
		<br/><input checked type="radio" name="COLORI" value="0"/> <b>Stampa in bianco e nero</b><br/>&nbsp;
	</div>
	<div id="SOGER_EtichetteColoriSI" style="color: white; width: 298px; height:100%; text-align:center; border: 1px SOLID black; background-color: #ec7d24;">
		<br/><input type="radio" name="COLORI" value="1"/> <b>Stampa a colori</b><br/>&nbsp;
	</div>

	-->
	<div id="SOGER_EtichetteColoriGO" style="text-align:center; width: 100%;margin-top:25px;margin-bottom:25px;">
		<input type="button" value="procedi" style="margin-top: 2px;" onclick="javascript:EtichettePrintGo();"/>
		&nbsp;<input type="button" value="annulla" style="margin-top: 2px;" onclick="javascript:EtichettePrintAbort();"/>
	</div>
</div>
</form>