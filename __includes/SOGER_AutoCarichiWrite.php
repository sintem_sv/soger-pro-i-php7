<?php
session_start();
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__classes/Core.class");
require_once("../__classes/Logic.class");
require_once("../__classes/Visual.class");
require_once("../__libs/SQLFunct.php");
require_once("../__libs/SogerECO_funct.php");
require_once("COMMON_wakeForgEdit.php");
require_once("COMMON_wakeSoger.php");


global $FEDIT;
global $SOGER;

## non so perch� cavolo debba ridefinire il database...
$FEDIT->DbServerData["db"]=$_SESSION["DbInUse"];

## devo caricare su registro industriale o fiscale?
if($SOGER->UserData['core_impiantiREG_IND']=='1'){
	$TableName="user_movimenti";
	$IDMov		= "ID_MOV";
	}
else{
	$TableName="user_movimenti_fiscalizzati";
	$IDMov		= "ID_MOV_F";
	}

## leggo ID_RIF per aggiornamento disponibilit�
$SQL = "SELECT ID_RIF, last_check FROM user_schede_rifiuti_deposito WHERE ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."';";
$FEDIT->SdbRead($SQL,"DbRecordSetD",true,false);


$AUTO_CAR=array();
$K=0;


if(substr($FEDIT->DbServerData["db"],-4)==date('Y')){

	for($c=0;$c<count($FEDIT->DbRecordSetD); $c++){
		
		if(date('Y-m-d') != $FEDIT->DbRecordSetD[$c]['last_check']){

			#
			#	AGGIORNA DISPONIBILITA RIFIUTO
			#
			$IMP_FILTER=true;
			$DISPO_PRINT_OUT = false;
			$IDRIF = $FEDIT->DbRecordSetD[$c]['ID_RIF'];
			$DTMOV		= date("Y-m-d");
			$EXCLUDE_9999999	= true;
			require("../__scripts/MovimentiDispoRIF.php");
			if($Disponibilita<0) $Disponibilita=0;
			$SQL="UPDATE user_schede_rifiuti SET FKEdisponibilita='$Disponibilita' WHERE ID_RIF='".$IDRIF."'";
			$FEDIT->SDBWrite($SQL,true,false);

			#
			#	SELEZIONO INFO PER CARICO AUTOMATICO
			#
			$SQL = "SELECT user_schede_rifiuti_deposito.ID_DEP, user_schede_rifiuti_deposito.ID_RIF, eseguo_carico_automatico, carico_automatico, intervallo, ID_ICA, MaxStock, NumCont, lov_contenitori.portata, lov_contenitori.m_cubi, ";
			$SQL.= "user_schede_rifiuti.originalID_RIF, user_schede_rifiuti.FANGHI_reg, user_schede_rifiuti.FKEdisponibilita as disponibilita, ";
			$SQL.= "user_schede_rifiuti.produttore as produttore, user_schede_rifiuti.peso_spec, last_carico, ";
			$SQL.= "lov_stato_fisico.description as SF, lov_misure.description as UM ";
			$SQL.= "FROM user_schede_rifiuti_deposito ";
			$SQL.= "LEFT JOIN lov_contenitori ON user_schede_rifiuti_deposito.ID_CONT=lov_contenitori.ID_CONT ";
			$SQL.= "JOIN user_schede_rifiuti ON user_schede_rifiuti_deposito.ID_RIF=user_schede_rifiuti.ID_RIF ";
			$SQL.= "JOIN lov_stato_fisico ON user_schede_rifiuti.ID_SF=lov_stato_fisico.ID_SF ";
			$SQL.= "JOIN lov_misure ON user_schede_rifiuti.ID_UMIS=lov_misure.ID_UMIS ";
			$SQL.= "WHERE user_schede_rifiuti_deposito.ID_RIF='".$IDRIF."' ";
			$SQL.= "AND user_schede_rifiuti.approved=1 ";
			$FEDIT->SdbRead($SQL,"DbRecordSetDeposito",true,false);
			$AUTO_RIF = $FEDIT->DbRecordSetDeposito;

			#
			#	VERIFICO SE C'E' SPAZIO NEI CONTENITORI
			#
			$DisponibilitaKg	= $AUTO_RIF[0]['disponibilita'];
			if($AUTO_RIF[0]['MaxStock']>0){
				// quantitativo massimo stoccabile in kg
				$MaxStockKg = $AUTO_RIF[0]['MaxStock'] * $AUTO_RIF[0]['NumCont'];
				}
			else{
				// uso capacit� standard (in kg) dei contenitori
				$MaxStockKg = $AUTO_RIF[0]['portata'] * $AUTO_RIF[0]['NumCont'];
				}
			if($MaxStockKg>$DisponibilitaKg) $SpazioContenitore = true; else $SpazioContenitore = false;

			#
			#	CONTROLLI GIORNI DALL'ULTIMO CARICO
			#
			// Giorni trascorsi dall'ultimo carico
			$DataO	= mktime(0, 0, 0, date('m'), date('d'), date('Y'));
			$DataC  = explode('-', $AUTO_RIF[0]['last_carico']);
			$DataC  = mktime(0, 0, 0, (int)$DataC[1], (int)$DataC[2], (int)$DataC[0]);
			$differenza=ceil(($DataO - $DataC)/(60*60*24));				// quanti gg sono passati

			// Intervallo in giorni tra carichi automatici
			if($AUTO_RIF[0]['ID_ICA']==1)
				$intervallo=$AUTO_RIF[0]['intervallo'];				// quanti gg devono passare
			else
				$intervallo=$AUTO_RIF[0]['intervallo'] * 7;			// quanti gg devono passare

			if($differenza>=$intervallo) $TempoTrascorso = true; else $TempoTrascorso = false;			

			// Quante volte � passato il tempo necessario all'esecuzione del nuovo carico?
			// echo $AUTO_RIF[0]['ID_DEP'].": ".$differenza. " / " .$intervallo."<hr>";

			if($differenza>0 AND $intervallo>0){
				
				//echo var_dump($AUTO_RIF)."<hr>";
				$ripetizione = $differenza / $intervallo;
				$ripetizione = floor($ripetizione);

				if( ($SpazioContenitore && $TempoTrascorso) OR $AUTO_RIF[0]['eseguo_carico_automatico']==0){
					
					#
					#	PREPARO ARRAY CARICHI AUTOMATICI
					#

					for($r=1;$r<=$ripetizione;$r++){
						
						$KgCaricabili = $MaxStockKg-$DisponibilitaKg;
						switch($AUTO_RIF[0]['UM']){
							case 'Kg.':
								$KgCaricare	= $AUTO_RIF[0]['carico_automatico'];
								break;
							case 'Litri':
								$KgCaricare	= $AUTO_RIF[0]['carico_automatico'] * $AUTO_RIF[0]['peso_spec'];
								break;
							case 'Mc.':
								$KgCaricare	= $AUTO_RIF[0]['carico_automatico'] * $AUTO_RIF[0]['peso_spec'] * 1000;
								break;
							}
						if($KgCaricare > $KgCaricabili) $KgCaricare=$KgCaricabili;

						if($KgCaricare>0 OR $AUTO_RIF[0]['eseguo_carico_automatico']==0){

							# ['pesoN']
							$AUTO_CAR[$K]['pesoN']	= $KgCaricare;
							
							# ['quantita']
							switch($AUTO_RIF[0]['UM']){
								case 'Kg.':
									$QuantitaConvertita	= $KgCaricare;
									break;
								case 'Litri':
									$QuantitaConvertita	= $KgCaricare / $AUTO_RIF[0]['peso_spec'];
									break;
								case 'Mc.':
									$QuantitaConvertita	= $KgCaricare / $AUTO_RIF[0]['peso_spec'] / 1000;
									break;
								}
							$AUTO_CAR[$K]['quantita']	= $QuantitaConvertita;
							
							# ['data']
							if(isset($DataCarico))
								$DataCaricoPrecedente = $DataCarico;
							else
								$DataCaricoPrecedente = date('Y-m-d',$DataC);
							if($AUTO_RIF[0]['ID_ICA']==1) $ripet=$r; else $ripet=$r * 7;
							$DataCarico = $DataC + ($ripet * 24 * 60 * 60 * $AUTO_RIF[0]['intervallo']);
							$DataCarico = date('Y-m-d', $DataCarico);
							$AUTO_CAR[$K]['data']	= $DataCarico;

							# ['altri']
							$AUTO_CAR[$K]['ID_RIF']						= $AUTO_RIF[0]['ID_RIF'];
							$AUTO_CAR[$K]['originalID_RIF']				= $AUTO_RIF[0]['originalID_RIF'];
							$AUTO_CAR[$K]['UM']							= $AUTO_RIF[0]['UM'];
							$AUTO_CAR[$K]['SF']							= $AUTO_RIF[0]['SF'];
							$AUTO_CAR[$K]['peso_spec']					= $AUTO_RIF[0]['peso_spec'];
							$AUTO_CAR[$K]['FANGHI_REG']					= $AUTO_RIF[0]['FANGHI_reg'];
							$AUTO_CAR[$K]['eseguo_carico_automatico']	= $AUTO_RIF[0]['eseguo_carico_automatico'];
							//$AUTO_CAR[$K]['idSIS_regCrono']				= $AUTO_RIF[0]['idSIS_regCrono'];
							}
						$DisponibilitaKg+=$KgCaricare;
						$K++;
						}
					}
				}
			}
		}
	}


#
#	ORDINO PER DATA L'ARRAY DEI CARICHI AUTOMATICI
#
function ordina($a, $b){

 $aq = $a['data'];
 $bq = $b['data'];

 if (strtotime($a['data']) == strtotime($b['data']) ){
	return 0;
	}
 return (strtotime($aq) < strtotime($bq)) ? -1 : 1;

}

//print_r($AUTO_CAR);
usort($AUTO_CAR, "ordina");
//print_r($AUTO_CAR);

#
#	ESCLUDO GIORNI FESTIVI
#
// Pasqua
$pasqua = date("Y-m-d", easter_date(date('Y')));
// Pasquetta
list ($anno,$mese,$giorno) = explode("-",$pasqua);
$pasquetta = mktime (0,0,0,date($mese),date($giorno)+1,date($anno));
$pasquetta = date('Y-m-d', $pasquetta);
//Giorni festivi
$giorniFestivi = array("01-01",
			   "01-06",
			   "04-25",
			   "05-01",
			   "06-02",
			   "08-15",
			   "11-01",
			   "12-08",
			   "12-25",
			   "12-26",
			   );
// aggiungo pasqua e pasquetta all' array dei giorni festivi
$pasqua=explode("-", $pasqua);
$giorniFestivi[]=$pasqua[1]."-".$pasqua[2];
$pasquetta=explode("-", $pasquetta);
$giorniFestivi[]=$pasquetta[1]."-".$pasquetta[2];

function isFestivo($carico,$giorniFestivi){
	$weekday=date('w', strtotime($carico)); //0-sabato; 6-domenica;
	$mese_giorno = date('m-d',strtotime($carico));
	if($weekday==0 | $weekday==6 | in_array($mese_giorno,$giorniFestivi)) $isFestivo=true; else $isFestivo=false;			
	return $isFestivo;
	}

for($c=0;$c<count($AUTO_CAR);$c++){
	$CheckFestivo=true;
	while($CheckFestivo){
		$CheckFestivo=isFestivo($AUTO_CAR[$c]['data'],$giorniFestivi);
		if($CheckFestivo){
			for($c2=$c;$c2<count($AUTO_CAR);$c2++){
				if($AUTO_CAR[$c2]['ID_RIF']==$AUTO_CAR[$c]['ID_RIF']){
					## devo aumentare la data di tutti i carichi successivi dello stesso rif
					$old_data = explode("-",$AUTO_CAR[$c2]['data']);
					$new_data = mktime (0,0,0,$old_data[1],$old_data[2]+1,$old_data[0]);
					$AUTO_CAR[$c2]['data'] = date('Y-m-d', $new_data);
					}
				}
			}
		}
	}

usort($AUTO_CAR, "ordina");

//var_dump($AUTO_CAR);

#
#	Elimino i carichi con data precedente all' ultimo movimento industriale inserito
#	dovrebbe gi� partire dalla data corrente in avanti, ma continuano a verificarsi carichi retrodatati nel momento in cui si libera spazio nel contenitore
#
// DATA ULTIMO MOVIMENTO INDUSTRIALE
$SQL = "SELECT MAX(DTMOV) AS MAXDT FROM user_movimenti_fiscalizzati WHERE ".$SOGER->UserData['workmode']."=1 AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."';";
$FEDIT->SdbRead($SQL,"MaxDataInd",true,false);
$MaxDataInd = $FEDIT->MaxDataInd[0]['MAXDT'];

$AUTO_CAR_TMP_PRE= array();
for($d=0;$d<count($AUTO_CAR);$d++){
	list($annoCarico,$meseCarico,$giornoCarico) = explode("-",$AUTO_CAR[$d]['data']);
	if(mktime(0,0,0,$meseCarico,$giornoCarico,$annoCarico) >= mktime(0,0,0,date("m",strtotime($MaxDataInd)),date("d",strtotime($MaxDataInd)),date("Y",strtotime($MaxDataInd))) ){
		$AUTO_CAR_TMP_PRE[]=$AUTO_CAR[$d];
		}
	}
$AUTO_CAR = $AUTO_CAR_TMP_PRE;

//var_dump($AUTO_CAR);



#
#	Elimino i carichi che dopo le correzioni avrebbero data superiore a quella odierna
#
$AUTO_CAR_TMP_POST= array();
for($d=0;$d<count($AUTO_CAR);$d++){
	list($annoCarico,$meseCarico,$giornoCarico) = explode("-",$AUTO_CAR[$d]['data']);
	if(mktime(0,0,0,$meseCarico,$giornoCarico,$annoCarico) <= mktime(0,0,0,date('m'),date('d'),date('Y')) ){
		$AUTO_CAR_TMP_POST[]=$AUTO_CAR[$d];
		}
	}
$AUTO_CAR = $AUTO_CAR_TMP_POST;

//var_dump($AUTO_CAR);
	
#
# RICAVO IL NUMERO DELL' ULTIMO MOVIMENTO PRESENTE SUL REGISTRO, L' ID_AZP E L' ID_UIMP
#
$READNUM ="SELECT MAX(NMOV) as num, ID_AZP, ID_UIMP, FKEcfiscP FROM ".$TableName." WHERE ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."' AND produttore='1' AND NMOV<>9999999 GROUP BY ID_AZP;";
$FEDIT->SdbRead($READNUM,"NMOV",true,false);

if($FEDIT->DbRecsNum>0){
	$NMOV		= $FEDIT->NMOV[0]['num'];
	$ID_AZP		= $FEDIT->NMOV[0]['ID_AZP'];
	$ID_UIMP	= $FEDIT->NMOV[0]['ID_UIMP'];
	$COD_FISC	= $FEDIT->NMOV[0]['FKEcfiscP'];
	}
else{
	$NMOV		= 0;

	$SQL_AZP="SELECT ID_AZP, codfisc FROM user_aziende_produttori WHERE ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."' AND approved=1 AND produttore=1";
	$FEDIT->SdbRead($SQL_AZP,"AZP",true,false);
	$ID_AZP		= $FEDIT->AZP[0]['ID_AZP'];
	$COD_FISC	= $FEDIT->AZP[0]['codfisc'];
	if(isset($FEDIT->AZP)){
		$SQL_UIMP="SELECT ID_UIMP FROM user_impianti_produttori WHERE ID_AZP=".$ID_AZP;
		$FEDIT->SdbRead($SQL_UIMP,"UIMP",true,false);
		$ID_UIMP	= $FEDIT->UIMP[0]['ID_UIMP'];
		}
	}


#
#	SCRIVO CARICHI NEL REGISTRO
#
for($c=0;$c<count($AUTO_CAR);$c++){
	
	if($AUTO_CAR[$c]['eseguo_carico_automatico']==1){

		## DISPONIBILITA' DEL RIFIUTO PRIMA DEL CARICO AUTOMATICO
		$SQLDISPO_PRE="SELECT FKEdisponibilita FROM user_schede_rifiuti WHERE ID_RIF='".$AUTO_CAR[$c]['ID_RIF']."'";
		$FEDIT->SDBRead($SQLDISPO_PRE,"Dispo",true,false);

		$Identifiers	= $SOGER->UserData['core_usersID_USR'].$SOGER->UserData['core_usersID_IMP'].$SOGER->UserData['workmode'];
		$UniqueString	= md5(uniqid($Identifiers, true));


		$NMOV++;
		$CARICHI = "INSERT INTO ".$TableName." ";
		$CARICHI.= "(TIPO,tara,pesoL,NMOV,DTMOV,ID_IMP,ID_RIF,originalID_RIF,FANGHI,quantita,pesoN,FKEdisponibilita,FKEumis,FKESF,FKEpesospecifico,ID_AZP,ID_UIMP,FKEcfiscP,";
		//$CARICHI.= "produttore,trasportatore,destinatario,intermediario,idSIS_regCrono)";
		$CARICHI.= "produttore,trasportatore,destinatario,intermediario,ID_USR,UniqueString)";
		$CARICHI.= "VALUES ";
		$CARICHI.= "('C','0','0','".$NMOV."','".$AUTO_CAR[$c]['data']."','".$SOGER->UserData["core_usersID_IMP"]."','".$AUTO_CAR[$c]['ID_RIF']."','".$AUTO_CAR[$c]['originalID_RIF']."', "; 
		$CARICHI.= "'".$AUTO_CAR[$c]['FANGHI_REG']."', '".$AUTO_CAR[$c]['quantita']."', '".$AUTO_CAR[$c]['pesoN']."', '".$FEDIT->Dispo[0]['FKEdisponibilita']."', '".$AUTO_CAR[$c]['UM']."', ";
		//$CARICHI.= "'".$AUTO_CAR[$c]['SF']."', '".$AUTO_CAR[$c]['peso_spec']."','".$ID_AZP."','".$ID_UIMP."', '".$COD_FISC."', '1', '0', '0', '0','".$AUTO_CAR[$c]['idSIS_regCrono']."');";
		$CARICHI.= "'".$AUTO_CAR[$c]['SF']."', '".$AUTO_CAR[$c]['peso_spec']."','".$ID_AZP."','".$ID_UIMP."', '".$COD_FISC."', '1', '0', '0', '0', '".$SOGER->UserData['core_usersID_USR']."', '".$UniqueString."');";

		//echo "Inserisco il carico automatico<br />".$CARICHI."<hr>";
		$FEDIT->SDBWrite($CARICHI,true,false);
		$MovID		= $FEDIT->DbLastID;

		## AGGIORNO LA DISPONIBILITA' DEL RIFIUTO
		$SQLDISPO = "UPDATE user_schede_rifiuti SET FKEdisponibilita = FKEdisponibilita + ".$AUTO_CAR[$c]['pesoN']." WHERE ID_RIF='".$AUTO_CAR[$c]['ID_RIF']."'";
		//echo "Aggiorno la disponibilita' del rifiuto id ".$AUTO_CAR[$c]['ID_RIF']."<hr />";
		$FEDIT->SDBWrite($SQLDISPO,true,false);

		## AGGIORNO COMPUTO ECONOMICO ( solo se sono su reg. fisc. )
		if($TableI[0] == "user_movimenti_fiscalizzati" && $SOGER->UserData["core_impiantiMODULO_ECO"]=="1"){
			require("../__libs/SogerECO_funct.php");
			$updCosti	= false;
			$impianto = $SOGER->UserData["core_impiantiID_IMP"];
			$resultArray = array();
			$DTMOV = $AUTO_CAR[$c]['data'];
			require("../__libs/SogerECO_GeneraCosti.php");
			}
		}

	## AGGIORNO DATA ULTIMO CARICO RIFIUTO
	$LAST_C = "UPDATE user_schede_rifiuti_deposito SET last_carico='".$AUTO_CAR[$c]['data']."' WHERE ID_RIF='".$AUTO_CAR[$c]['ID_RIF']."';";
	//echo "Aggiorno la data di ultimo carico del rifiuto id ".$AUTO_CAR[$c]['ID_RIF']." (".$AUTO_CAR[$c]['data'].")<hr>";
	$FEDIT->SDBWrite($LAST_C,true,false);
	}

//die(var_dump($AUTO_CAR));

#
#	AGGIORNO DATA LAST CHECK
#
$SQL="UPDATE user_schede_rifiuti_deposito SET last_check='".date('Y-m-d')."' WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."';";
//echo "Aggiorno la data di ultimo controllo esecuzione carichi automatici (".date('Y-m-d').")<hr>";
$FEDIT->SDBWrite($SQL,true,false);



if(count($AUTO_CAR)>0){
	$result.="<div id=\"loading_carichi\" class=\"finished\"><br />So.Ge.R. Pro ha ultimato la scrittura dei carichi automatici.";
	}
else{
	$result.="<div id=\"loading_carichi\" class=\"finished\"><br />So.Ge.R. Pro non ha rilevato carichi automatici";
	}


#
# FISCALIZZAZIONE AUTOMATICA
#

if($SOGER->UserData['core_impiantiREG_IND']==1){

	$devoFiscal=false;

	#oggi
	list($anno,$mese,$giorno) = explode('-',date('Y-m-d'));
	$data1 = mktime( 0, 0, 0, $mese, $giorno, $anno);

	switch($SOGER->UserData['workmode']){

		case 'produttore':
			$LastFisc='Plast_fisc_sched';
			$FiscIntervalTable='lov_fiscalizzazione_prod';
			$FiscInterval='ID_FISC_PROD';
			break;
		case 'destinatario':
			$LastFisc='Dlast_fisc_sched';
			$FiscIntervalTable='lov_fiscalizzazione_dest';
			$FiscInterval='ID_FISC_DEST';
			break;
		case 'trasportatore':
			$LastFisc='Tlast_fisc_sched';
			$FiscIntervalTable='lov_fiscalizzazione_trasp';
			$FiscInterval='ID_FISC_TRASP';
			break;
		case 'intermediario':
			$LastFisc='Ilast_fisc_sched';
			$FiscIntervalTable='lov_fiscalizzazione_interm';
			$FiscInterval='ID_FISC_INTERM';
			break;

		}

		#data ultima fiscalizzazione programmata
		$sql="SELECT ".$LastFisc." FROM core_impianti WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."'";
		$FEDIT->SdbRead($sql,"DbRecordSetLastScheduled",true,false);
		if(is_null($FEDIT->DbRecordSetLastScheduled[0][$LastFisc]))
			$FEDIT->DbRecordSetLastScheduled[0][$LastFisc]='9999-12-31';
		list($anno,$mese,$giorno) = explode('-',$FEDIT->DbRecordSetLastScheduled[0][$LastFisc]);
		$data2 = mktime( 0, 0, 0, $mese, $giorno, $anno);

		#gg passati da ultima fiscalizzazione
		$differenza=($data1-$data2)/60/60/24;

		#giorno di fiscalizzazione
		$FiscalDay = $SOGER->UserData['core_usersID_DAY'];

		#fiscalizzo ogni..
		$sql="SELECT gg FROM ".$FiscIntervalTable." WHERE ".$FiscInterval."='".$SOGER->UserData['core_users'.$FiscInterval]."';";
		$FEDIT->SdbRead($sql,"DbRecordSetFiscInterval",true,false);
		$FiscInterval = $FEDIT->DbRecordSetFiscInterval[0]['gg'];

	#fiscalizzo movimenti fino al giorno della settimana prestabilito prima di oggi ( il venerd� prima di oggi ) e se questo � successivo a $LastFisc
	for($d=0;$d<=6;$d++){
		$day = date('Y-m-d',strtotime(date('Y-m-d')." -".$d." days"));
		list($anno,$mese,$giorno) = explode('-',$day);
		if(date('w',mktime(0,0,0,$mese,$giorno,$anno)) == $FiscalDay){
			$LastChosenDay=$day;
			}
		//print_r($LastChosenDay);
		}

	$previsione = $data2 + ($FiscInterval * 60 * 60 * 24);
	$FiscalTo = date('Y-m-d', $previsione);
	$_SESSION['DataProxFisc']=$FiscalTo;

	if(strtotime($FiscalTo)>strtotime($FEDIT->DbRecordSetLastScheduled[0][$LastFisc]) && strtotime($FiscalTo)<strtotime(date('Y-m-d'))){
		//print_r($FiscalTo." - ".$FEDIT->DbRecordSetLastScheduled[0][$LastFisc]);
		$devoFiscal=true;
		$FiscalTo = $LastChosenDay;
		}

	if($SOGER->UserData['core_usersID_DAY']==0) $devoFiscal=false;

	if($devoFiscal){
		$sql="SELECT MAX(NMOV) as lastIndustrial, ID_MOV FROM user_movimenti WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."='1' AND DTMOV<='".$FiscalTo."';";
		$FEDIT->SdbRead($sql,"DbRecordSetLastInd",true,false);
		if(isset($FEDIT->DbRecordSetLastInd) AND !is_null($FEDIT->DbRecordSetLastInd[0]['lastIndustrial'])){
			$lastIndustrial=$FEDIT->DbRecordSetLastInd[0]['lastIndustrial'];
			$ID_MOV = $FEDIT->DbRecordSetLastInd[0]['ID_MOV'];
			}
		else{
			$lastIndustrial=0;
			$ID_MOV = 0;
			}
		$javascript ="<script type='text/javascript'>\n";
		$javascript.="document.getElementById('FiscalTo').value='".$FiscalTo."';\n";
		$javascript.="showFiscal(".$lastIndustrial.", '".$ID_MOV."');\n";
		$javascript.="</script>\n";
		}
	else{
		$javascript ="<script type='text/javascript'>\n";
		$javascript.="document.getElementById('devoFiscal').value=0;\n";
		$javascript.="document.FRM_fiscal.submit();\n";
				
		// abilito bottone chiusura popup
		$javascript.="document.getElementById('chiudi_popup').style.color='white';\n";
		$javascript.="document.getElementById('chiudi_popup').style.color='white';\n";
		$javascript.="document.getElementById('chiudi_popup').style.backgroundColor='#034373';\n";
		$javascript.="document.getElementById('chiudi_popup').style.borderColor='yellow';\n";
		$javascript.="document.getElementById('chiudi_popup').disabled=false;\n";

		$javascript.="</script>\n";

		}
	
	}

else{
	
	// non ho il registro industriale
	$javascript ="<script type='text/javascript'>\n";
			
	// abilito bottone chiusura popup
	$javascript.="document.getElementById('chiudi_popup').style.color='white';\n";
	$javascript.="document.getElementById('chiudi_popup').style.backgroundColor='#034373';\n";
	$javascript.="document.getElementById('chiudi_popup').style.borderColor='yellow';\n";
	$javascript.="document.getElementById('chiudi_popup').disabled=false;\n";

	$javascript.="</script>\n";

	}

$result.=$javascript;

$result.="</div>";

echo $result;



require_once("COMMON_sleepForgEdit.php");
require_once("COMMON_sleepSoger.php");
?>