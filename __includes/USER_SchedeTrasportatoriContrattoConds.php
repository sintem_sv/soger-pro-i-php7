<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEditDG.php");
#
global $SOGER;
$FEDG->FGE_FlushTableInfo();
#

$FEDG->FGE_UseTables("user_contratti_trasp_cond","lov_contratti_um");
$FEDG->FGE_SetSelectFields(array("motodo_calcolo"),"lov_contratti_um");
$FEDG->FGE_SetSelectFields(array("ID_CNT_T","description","euro"),"user_contratti_trasp_cond");
$FEDG->FGE_SetFilter("ID_CNT_T",$_SESSION['ID_CONTRACT'],"user_contratti_trasp_cond");
$FEDG->FGE_HideFields("ID_CNT_T","user_contratti_trasp_cond");

$FEDG->FGE_DescribeFields();
#

echo($FEDG->FGE_DataGrid($SOGER->AppDescriptiveLocation,"__scripts/FGE_DataGridEdit.php","EDC--",false,"FEDG",$_SESSION['SearchingFilter']));

#
require_once("__includes/COMMON_sleepForgEditDG.php");

?>
<div class="FGE4Col">
<input type="button" name="nuovo" class="FGEbutton" value="nuova condizione contrattuale" onclick="javascript: document.location='__scripts/newCondizioneContrattoTrasportatore.php'"/>
</div>
