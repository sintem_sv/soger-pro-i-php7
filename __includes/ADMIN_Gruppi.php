<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("core_gruppi");
$FEDIT->FGE_SetFormFields(array("ID_GRP","ID_INT","description","indirizzo","ID_COM","IN_ITALIA","telefono","fax","email","piva","codfisc","contributo_expire"),"core_gruppi");

if(!isset($_GET["table"]) &&!isset($_GET["pri"]) && !isset($_GET["filter"])) {
	
		$FEDIT->FGE_DescribeFields();
		$FEDIT->FGE_LookUpDefault("ID_COM","core_gruppi");
		$FEDIT->FGE_LookUpDefault("ID_INT","core_gruppi");
		$FEDIT->FGE_HideFields(array("IN_ITALIA"),"core_gruppi");
		echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea gruppo");

} else {

		$FEDIT->FGE_DescribeFields();
		$FEDIT->FGE_LookUpDefault("ID_COM","core_gruppi");
		$FEDIT->FGE_LookUpDefault("ID_INT","core_gruppi");
		$FEDIT->FGE_DisableFields("ID_GRP","core_gruppi");
		$FEDIT->FGE_HideFields(array("IN_ITALIA"),"core_gruppi");
		echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica gruppo");	
	
}
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>
