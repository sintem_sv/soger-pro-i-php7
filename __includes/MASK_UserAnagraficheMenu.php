<?php global $SOGER; ?>
	
	<div class="divName"><span>GESTIONE ANAGRAFICHE</span></div>

	<a href="__scripts/status.php?area=UserGestioneDeposito" title="Gestione deposito">
	<img src="__css/SMN_GestioneDeposito.gif" alt="Gestione deposito" class="NoBorder"/></a>

	<a href="__scripts/status.php?area=UserSchedeRifiuti" title="Schede Rifiuti Prodotti">
	<img src="__css/SMN_SchedeRifiutiProdotti.gif" alt="Schede Rifiuti Prodotti" class="NoBorder"/></a>

	<a href="__scripts/status.php?area=UserSchedeProduttori" title="Schede Produttori">
	<img src="__css/SMN_SchedeProduttori.gif" alt="Schede Produttori" class="NoBorder"/></a>

	<a href="__scripts/status.php?area=UserSchedeDestinatari" title="Schede Destinatari">
	<img src="__css/SMN_SchedeDestinatari.gif" alt="Schede Destinatari" class="NoBorder"/></a>

	<a href="__scripts/status.php?area=UserSchedeIntermediari" title="Schede Intermediari">
	<img src="__css/SMN_SchedeIntermediari.gif" alt="Schede Intermediari" class="NoBorder"/></a>

	<a href="__scripts/status.php?area=UserSchedeTrasportatori" title="Schede Trasportatori">
	<img src="__css/SMN_SchedeTrasportatori.gif" alt="Schede Trasportatori" class="NoBorder"/></a>

<?php if($SOGER->UserData['core_impiantiMODULO_SIS']==1 && $SOGER->UserData["core_usersO1"]=="1"){ ?>
	<a href="#" id="AllineamentoSISTRI" title="Allineamento SISTRI">
	<img src="__css/SMN_AllineamentoSISTRI.gif" alt="Allineamento SISTRI" class="NoBorder"/></a>
<?php } ?>




<!-- HIDDEN DIALOG BOX -->

<div id='jqpopup_AllineamentoSISTRI_STEP1' title='Allineamento delle anagrafiche con SISTRI'>

	<p>
	L'operazione richiesta sincronizzer� le anagrafiche delle sedi legali dei vostri fornitori con quelle presenti nel database SISTRI.<br /><br />

	La sincronizzazione dei dati di impianto dovr� essere eseguita manulmente.<br /><br />

	Alla fine della procedura verr� generato un report che vi invitiamo ad osservare con cura.<br /><br />

	Tale documento conterr� l'esito dell'operazione di sincronizzazione delle anagrafiche.<br /><br />

	Confermi l'operazione?
	</p>

</div>


<div id='jqpopup_AllineamentoSISTRI_STEP2' title='Allineamento delle anagrafiche con SISTRI'>

	<p>E' in corso la sincronizzazione delle anagrafiche con il database SISTRI, l'operazione potrebbe richiedere qualche secondo.</p>
	
	<table id="TABLE_Allineamento_SISTRI">
		<tr>
			<td>Anagrafiche produttori:</th><td style="align:right;" id="TD_user_aziende_produttori"><img src='__css/progress_bar.gif' /></td>
		</tr>
		<tr>
			<td>Anagrafiche trasportatori:</th><td style="align:right;" id="TD_user_aziende_trasportatori"><img src='__css/progress_bar.gif' /></td>
		</tr>
		<tr>
			<td>Anagrafiche destinatari:</th><td style="align:right;" id="TD_user_aziende_destinatari"><img src='__css/progress_bar.gif' /></td>
		</tr>
		<tr>
			<td>Anagrafiche intermediari:</th><td style="align:right;" id="TD_user_aziende_intermediari"><img src='__css/progress_bar.gif' /></td>
		</tr>
	</table>

	<p id="SincroLog"></p>

</div>