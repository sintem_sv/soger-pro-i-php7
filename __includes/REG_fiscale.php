<div class="divName"><span>GESTIONE REGISTRI</span></div>

<?php
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$TableName="user_movimenti_fiscalizzati";
//$LegamiSISTRI	= false;
//require("__scripts/MovimentiRifMovCarico.php");
require_once("__includes/COMMON_sleepForgEdit.php");
require("__includes/USER_RegistroFiscaleSiNo.inc");

/*
 * Dal 1.2.2018 la stampa del registro su modulo continuo bianco � disabilitata.
 * La possibilit� resta attiva solo per STS e VINAVIL Ravenna.
 * Appena avranno vidimato A4, la possibilit� verr� rimossa anche per loro.
 */

$disabled = in_array($SOGER->UserData['core_usersID_IMP'],array("001ALEIM"))? "":"disabled";
//$disabled = "disabled";

/******************************************************************************/


?>

<form name="StampaRegistro" id="StampaRegistro" method="get" action="" style="margin-left:5px;margin-right:10px;">

<?php
$parameter = $SOGER->UserData["core_usersGGedit_".$SOGER->UserData['workmode']];
if($SOGER->UserData['workmode']=="produttore") $limit=14; else $limit=2;
if($parameter>$limit){
?>
<fieldset class="FGEfieldset" style="border-color:red;background-color:#fff;">
    <legend class="FGElegend" style="font-size:x-small;color:red;">ATTENZIONE!</legend>
    <p>
        Attenzione, i giorni di modificabilit� della movimentazione indicati in
        configurazione superano le disposizioni dell'articolo 190 del D.Lgs.
        152/2006 sulla corretta tenuta del registro di carico e scarico rifiuti.
    </p>
</fieldset>
<?php } ?>

<fieldset class="FGEfieldset">
	<legend class="FGElegend" style="font-size:x-small;">Stampa Registro Fiscale</legend>

<table style="width:100%;">


<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="da" class="">Da movimento n�</label></td>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><input type="text" value="1" id="da" name="da" size="4" class="FGEinput" tabindex="50"/></td>
</tr>

<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="a" class="">A movimento n�</label></td>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><input type="text" value="" id="a" name="a" size="4" class="FGEinput" tabindex="51"/></td>
</tr>

<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="cartaBianca" class="">Stampa su carta bianca</label></td>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><input type="checkbox" value="Cbianca" name="cartaBianca" id="cartaBianca" class="FGEinput" tabindex="52" checked <?= $disabled ?> /></td>
</tr>

<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="Npagina" class="">Stampa numeri di pagina</label></td>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><input type="checkbox" value="StampaNpag" name="Npagina" id="Npagina" class="FGEinput" tabindex="53" checked /></td>
</tr>

<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="RegVidimato" class="">Registro vidimato</label></td>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><input type="radio" value="1" name="Vidimato" id="Vidimato1" class="FGEinput" tabindex="54" />S�
	<input type="radio" value="0" name="Vidimato" id="Vidimato0" class="FGEinput" tabindex="55" style="margin-left:10px;" checked />No</td>
</tr>

<tr>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><label for="paginaIniz" class="">Numero di pagina iniziale</label></td>
	<td class="FGEDataGridTableCellLeft" style="padding-left:5px;"><input type="text" size="4" value="1" name="paginaIniz" id="paginaIniz" class="FGEinput" tabindex="56"/></td>
</tr>

</table>


<br/><br/>

<input type="button" class="FGEbutton" tabindex="55" value="stampa registro" onclick="javascript:RegistroPrintGo();"/>

</fieldset>
</form>