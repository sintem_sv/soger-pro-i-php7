<?php

require_once("__classes/ForgEdit2.class");
require_once("__classes/ForgEdit.RegExp");
require_once("__classes/DbLink.class");
require_once("__libs/SQLFunct.php");
require("__includes/COMMON_wakeForgEdit.php");
require("__includes/COMMON_ForgEditClassFiles.php");

global $SOGER;



$FISCAL_1 = <<< ENDED

<div id="FISCAL" style="display:none">

	<div id="title">Fiscalizzazione dei movimenti - step 1 di 2</div>

	<div id="txt1" style="margin-left:20px;"><br /><br /><b>Prospetto dei movimenti che verranno inseriti nel registro fiscale:</b></div>
	
	<form action="javascript:completeAHAH.likeSubmit('__includes/SOGER_FiscalizzaWrite.php', 'POST', 'FRM_fiscal', 'table_prospetto');" id="FRM_fiscal" name="FRM_fiscal" method="POST">
	
	<div id="table_prospetto">

		<table cellpadding="0" cellspacing="0" id="prospetto_movimenti">

			<tbody id="tablebody"></tbody>

		</table>

	</div>

	<div id="pulsanti" style="padding-top:15px;">
		<input class="FiscalButton" type="button" name="invia_movimenti" value="Scrivi registro fiscale" onClick="javascript:validate();" />
		<input class="FiscalButton" type="reset"  name="annulla_edits"   value="Annulla le modifiche" />
		<input class="FiscalButton" type="button" name="chiudi_popup"    value="Chiedi in seguito" onClick="javascript:obj=document.getElementById('FISCAL');obj.style.display='none';clearTable('tablebody');" />
	</div>

	</form>


	<!-- <div id="txt2">Attendere prego</div> -->


</div>

ENDED;

echo $FISCAL_1;


require("__includes/COMMON_sleepForgEdit.php");
?>