<?php 

global $SOGER; 

?>

<div class="divName"><span>MOVIMENTI</span></div>

	<?php if($SOGER->UserData['workmode'] != 'intermediario' ){ ?>
		<a href="__scripts/status.php?area=UserNuovoMovCaricoF" title="Movimento di Carico"><img src="__css/SMN_MovimentiCarico.gif" alt="Nuovo movimento di Carico" class="NoBorder"/></a>

		<a href="__scripts/status.php?area=UserNuovoMovScaricoF" title="Movimento di Scarico"><img src="__css/SMN_MovimentiScarico.gif" alt="Nuovo movimento di Scarico" class="NoBorder"/></a>
	<?php } else { ?>
		<a href="__scripts/status.php?area=UserNuovoMovCaricoF" title="Nuovo Movimento"><img src="__css/SMN_Movimento.gif" alt="Nuovo movimento" class="NoBorder"/></a>
	<?php } ?>

	<?php if($SOGER->UserData['core_usersFRM_LAYOUT_SCARICO_INTERNO']==1){ ?>
		<a href="__scripts/status.php?area=UserNuovoMovScaricoInternoF" title="Movimento di Scarico Interno"><img src="__css/SMN_MovimentiScaricoInterno.gif" alt="Nuovo movimento di Scarico Interno" class="NoBorder"/></a>
	<?php } ?>

	
	<a href="__scripts/status.php?area=UserVediMovimentiF" title="Registro C/S fiscali"><img src="__css/SMN_VediMovimentiInd.gif" alt="Registro C/S fiscali" class="NoBorder"/></a>

	<?php if($SOGER->UserData['workmode'] != 'intermediario' ){ ?>
		<a href="__scripts/status.php?area=UserVediMovimentiFiscali" title="Elenco FIR"><img src="__css/SMN_VediMovimentiFisc.gif" alt="Elenco FIR" class="NoBorder"/></a>
	<?php } ?>

	<?php if($SOGER->UserData['workmode'] != 'intermediario' ){ ?>
		<a href="__scripts/status.php?area=UserVediMovimentiIVcopie" title="Gestione delle IV copie"><img src="__css/SMN_VediMovimentiIVcopie.gif" alt="Gestione delle IV copie" class="NoBorder"/></a>
	<?php } ?>

	<?php if($SOGER->UserData['core_usersFANGHI']==1){ ?>
		<a href="__scripts/status.php?area=UserVediMovimentiFanghi" title="Vedi Movimenti Fanghi"><img src="__css/SMN_VediMovimentiFanghi.gif" alt="Vedi Movimenti Fanghi" class="NoBorder"/></a>
	<?php } ?>