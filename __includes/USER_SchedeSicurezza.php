<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_schede_sicurezza");
$FEDIT->FGE_SetFormFields(array("ID_RIF","data_compilazione","note","ID_SFSK","odore","colore","pH","lastLab","ID_IMP"),"user_schede_sicurezza");
$FEDIT->FGE_SetFormFields(array("ID_RSK","produttore","trasportatore","destinatario","intermediario"),"user_schede_sicurezza");
$FEDIT->FGE_SetFormFields(array("carrello","pedana","cisterna","manuale","prescrizioni_mov"),"user_schede_sicurezza");
$FEDIT->FGE_SetFormFields(array("cr_carrello","cr_pianale","cr_cassone","cr_ragno","cr_cisterna","cr_manuale"),"user_schede_sicurezza");
$FEDIT->FGE_SetFormFields(array("RIUTILZ","cnt_fustim","cnt_fustip","cnt_container","cnt_contpl","cnt_sacchi","cnt_cisterna","cnt_bigbag","noacq","origine","stock","responsabile"),"user_schede_sicurezza");
$ProfiloFields = "user_schede_sicurezza";
include("SOGER_CampiProfilo.php");

$FEDIT->FGE_SetTitle("ID_RIF","Denominazione del Rifiuto","user_schede_sicurezza");
$FEDIT->FGE_SetTitle("ID_SFSK","Caratteristiche del Rifiuto","user_schede_sicurezza");
$FEDIT->FGE_SetTitle("ID_RSK","Protezione Individuale","user_schede_sicurezza");
$FEDIT->FGE_SetTitle("carrello","Mezzi ammessi per la movimentazione e l'immagazzinamento","user_schede_sicurezza");
$FEDIT->FGE_SetTitle("cr_carrello","Mezzi ammessi per il carico dei veicoli","user_schede_sicurezza");
$FEDIT->FGE_SetTitle("RIUTILZ","Contenitori previsti ed ammessi","user_schede_sicurezza");
$FEDIT->FGE_SetTitle("noacq","Provvedimenti in caso di dispersione o incendio","user_schede_sicurezza");
$FEDIT->FGE_SetTitle("origine","Altri dati","user_schede_sicurezza");

if(!isset($_GET["pri"]) && !isset($_GET["filter"])) { 
	
	$FEDIT->FGE_DescribeFields();	
	#
	$FEDIT->FGE_LookUpDefault("ID_SFSK","user_schede_sicurezza");
	$FEDIT->FGE_LookUpDefault("ID_RSK","user_schede_sicurezza");
	#
	$FEDIT->FGE_LookUpCFG("ID_RIF","user_schede_sicurezza",false,true);
	$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
	//$FEDIT->FGE_SetSelectFields(array("descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
	$FEDIT->FGE_SetFilter("ID_RIF",$_GET["ID_RIF"],"user_schede_rifiuti");
	$ProfiloFields = "user_schede_rifiuti";
	include("SOGER_FiltriProfilo.php");
	//$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","approved"),"user_schede_rifiuti");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();	
	#
	$data =  date("Y-m-d");
	$FEDIT->FGE_SetValue("data_compilazione",$data,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("lastLab",$data,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("carrello",0,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("pedana",0,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("cisterna",0,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("manuale",0,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("cr_carrello",0,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("cr_pianale",0,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("cr_cassone",0,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("cr_ragno",0,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("cr_cisterna",0,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("cr_manuale",0,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("RIUTILZ",0,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("cnt_fustim",0,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("cnt_fustip",0,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("cnt_container",0,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("cnt_contpl",0,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("cnt_sacchi",0,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("cnt_cisterna",0,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("cnt_bigbag",0,"user_schede_sicurezza");
	$FEDIT->FGE_SetValue("noacq",0,"user_schede_sicurezza");
	//$FEDIT->FGE_HideFields(array("ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_sicurezza");
	$FEDIT->FGE_HideFields(array("ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_sicurezza");
	$FEDIT->FGE_DisableFields("ID_RIF","user_schede_sicurezza");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"crea procedura di sicurezza");	

} else {
	
	$FEDIT->FGE_DescribeFields();	
	$FEDIT->FGE_LookUpDefault("ID_RSK","user_schede_sicurezza");
	$FEDIT->FGE_LookUpDefault("ID_SFSK","user_schede_sicurezza");
	#
	$FEDIT->FGE_LookUpCFG("ID_RIF","user_schede_sicurezza");
	$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
	//$FEDIT->FGE_SetSelectFields(array("descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
	$ProfiloFields = "user_schede_rifiuti";
	include("SOGER_FiltriProfilo.php");
	//$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_rifiuti");
	$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();	
	#
	$FEDIT->FGE_HideFields(array("ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_sicurezza");
	$FEDIT->FGE_DisableFields("ID_RIF","user_schede_sicurezza");
	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva procedura di sicurezza");	
}


require_once("__includes/COMMON_sleepForgEdit.php");
?>
