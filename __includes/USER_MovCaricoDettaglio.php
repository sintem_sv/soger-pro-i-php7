<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
global $SOGER;

$TableName=$_GET['table'];

require("USER_Movimenti_DettaglioNMOV.php");
	
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables($TableName);
$FEDIT->FGE_SetFormFields(array("NMOV","DTMOV"),$TableName);

if($this->UserData["workmode"]!="produttore"){
	$FEDIT->FGE_SetFormFields(array("ID_AUTO","FKEadrAUT","FKEmzAUT"),$TableName);
	$FEDIT->FGE_SetFormFields(array("ID_RMK","FKEadrRMK","FKEmzRMK"),$TableName);
	$FEDIT->FGE_SetFormFields(array("NOTER"),$TableName);
	}
else
	$FEDIT->FGE_SetFormFields(array("NOTER"),$TableName);

#
$FEDIT->FGE_DescribeFields();
#
$autoTRADEST = false;
require("__mov_snipplets/LKUP_Automezzi.php");
require("__mov_snipplets/LKUP_Rimorchi.php");


$FEDIT->FGE_DisableFields(array("NMOV","DTMOV"),$TableName);
$FEDIT->FGE_HideFields("NMOV",$TableName);

if($this->UserData["workmode"]=="produttore"){
	if(isset($_SESSION['StampaAnnua']) && $_SESSION['StampaAnnua']==1){
		$FEDIT->FGE_DisableFields(array("NOTER"),$TableName);
		}
	}
else{
	#
	$FEDIT->FGE_SetTitle("ID_AUTO","Automezzo",$TableName);
	$FEDIT->FGE_SetTitle("ID_RMK","Rimorchio",$TableName);
	#
	$FEDIT->FGE_LookUpCFG("ID_AUTHT",$TableName);	
	$FEDIT->FGE_UseTables("user_autorizzazioni_trasp");
	$FEDIT->FGE_SetSelectFields(array("ID_RIF","ID_UIMT","num_aut"),"user_autorizzazioni_trasp");
	$FEDIT->FGE_HideFields(array("ID_UIMT","ID_RIF"),"user_autorizzazioni_trasp");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone(); 

	$FEDIT->FGE_DisableFields(array("NMOV","DTMOV","FKEadrAUT","FKEmzAUT","FKEadrRMK","FKEmzRMK"),$TableName);

	# verifico se devo bloccare movimento perch� aperto da altri
	if($SOGER->UserData['core_usersO12']=='0'){
		
		$sql = "SELECT ID_USR FROM ".$_GET['table']." WHERE ".$_GET['pri']."=".$_GET['filter'].";";
		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
		
		if($FEDIT->DbRecordSet[0]['ID_USR']!='0000000' AND ($SOGER->UserData['core_usersID_USR']!=$FEDIT->DbRecordSet[0]['ID_USR'])){
			echo "<div id=\"Errors\" class=\"ErrType1\" onclick=\"javascript:document.getElementById('Errors').style.zIndex=-1\">";
			echo "L'utente non ha i permessi necessari per modificare i movimenti inseriti da altri utenti [cod. O12]";
			echo "</div>";
			$FEDIT->FGE_DisableFields(array("dt_in_trasp","hr_in_trasp","percorso"),$TableName);
			$FEDIT->FGE_DisableFields(array("ID_AUTO","ID_AUTST"),$TableName);
			$FEDIT->FGE_DisableFields(array("ID_RMK"),$TableName);
			$FEDIT->FGE_DisableFields(array("ID_COMM","ID_CAR"),$TableName);
			}
		}


	}

echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva dettaglio");
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>
