<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
global $SOGER;

$TableName=$_GET['table'];
if($TableName=="user_movimenti") $PRI="ID_MOV"; else $PRI="ID_MOV_F";

require("USER_Movimenti_DettaglioNMOV.php");

$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables($TableName);

$FEDIT->FGE_SetFormFields(array("NMOV","FKErifCarico"),$TableName);
$FEDIT->FGE_SetFormFields(array("dt_in_trasp","hr_in_trasp","percorso"),$TableName);
$FEDIT->FGE_SetFormFields(array("ID_AUTO","FKEadrAUT","FKEmzAUT","ID_AUTST"),$TableName);
$FEDIT->FGE_SetFormFields(array("ID_RMK","FKEadrRMK","FKEmzRMK"),$TableName);
//$FEDIT->FGE_SetFormFields(array("ID_RIF","FKESF"),$TableName);
//$FEDIT->FGE_SetFormFields(array("CAR_NumDocumento","CAR_Laboratorio","CAR_DataAnalisi", "prescrizioni_mov"),$TableName);
//$FEDIT->FGE_SetBreak("CAR_NumDocumento",$TableName);
$FEDIT->FGE_SetFormFields(array("FKEcfiscT","FKEcfiscD"),$TableName);

# FDA
$FEDIT->FGE_SetFormFields(array("FDAchecked","FDAinvalid"),$TableName);
$FEDIT->FGE_HideFields(array("FDAchecked","FDAinvalid"),$TableName);

#
$FEDIT->FGE_SetFormFields(array("ID_UIMT","ID_AUTHT"),$TableName);
$FEDIT->FGE_HideFields(array("NMOV","ID_UIMT","ID_AUTHT"),$TableName);
$FEDIT->FGE_HideFields(array("FKEcfiscT","FKEcfiscD"),$TableName);
#
$FEDIT->FGE_DescribeFields();
#




$autoTRADEST = true;
require("__mov_snipplets/LKUP_Automezzi.php");
require("__mov_snipplets/LKUP_Rimorchi.php");
require("__mov_snipplets/LKUP_Autisti.php");
require("__mov_snipplets/LKUP_OpRS.php");

require("__mov_snipplets/LKUP_Rifiuto.php");
$FEDIT->FGE_LookUpDefault("ID_ONU",$TableName);

$rosetteA = '<div id="rosetteA" class="rosette">&nbsp;&nbsp;</div>';
$rosetteR = '<div id="rosetteR" class="rosette">&nbsp;&nbsp;</div>';

$FEDIT->FGE_SetTitle("dt_in_trasp","Dati del viaggio",$TableName);
$FEDIT->FGE_SetTitle("ID_RIF","Dati SISTRI: confezionamento e prescrizioni",$TableName);
$FEDIT->FGE_SetTitle("ID_AUTO",$rosetteA."Automezzo",$TableName);
$FEDIT->FGE_SetTitle("ID_RMK",$rosetteR."Rimorchio",$TableName);

#
$FEDIT->FGE_DisableFields(array("NMOV","DTMOV","FKEadrAUT","FKEmzAUT","FKEadrRMK","FKEmzRMK","FKErifCarico"),$TableName);
//$FEDIT->FGE_DisableFields(array("ID_RIF", "FKESF"),$TableName);
//$FEDIT->FGE_DisableFields(array("CAR_NumDocumento","CAR_Laboratorio","CAR_DataAnalisi", "prescrizioni_mov"),$TableName);

# verifico se devo bloccare movimento perch� aperto da altri
if($SOGER->UserData['core_usersO12']=='0'){

	$sql = "SELECT ID_USR FROM ".$_GET['table']." WHERE ".$_GET['pri']."=".$_GET['filter'].";";
	$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

	if($FEDIT->DbRecordSet[0]['ID_USR']!='0000000' AND ($SOGER->UserData['core_usersID_USR']!=$FEDIT->DbRecordSet[0]['ID_USR'])){
		echo "<div id=\"Errors\" class=\"ErrType1\" onclick=\"javascript:document.getElementById('Errors').style.zIndex=-1\">";
		echo "L'utente non ha i permessi necessari per modificare i movimenti inseriti da altri utenti [cod. O12]";
		echo "</div>";
		$FEDIT->FGE_DisableFields(array("dt_in_trasp","hr_in_trasp","percorso"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_AUTO","ID_AUTST"),$TableName);
		$FEDIT->FGE_DisableFields(array("ID_RMK"),$TableName);
		}
	}


echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva dettaglio");

echo '<form name="tmp_contoterzi_mezzi">';
echo '<input type="hidden" value="" id="automezzoID_LIM" name="automezzoID_LIM" />';
echo '<input type="hidden" value="" id="rimorchioID_LIM" name="rimorchioID_LIM" />';
echo '</form>';

$RIF_REF = $FEDIT->DbRecordSet[0]["ID_RIF"];
$ID_RMK  = $FEDIT->DbRecordSet[0]["ID_RMK"];
$ID_AUTO = $FEDIT->DbRecordSet[0]["ID_AUTO"];
$DTMOV_IN_TRASP = ($FEDIT->DbRecordSet[0]["DTMOV"]=='0000-00-00' || is_null($FEDIT->DbRecordSet[0]["DTMOV"]))? null:$FEDIT->DbRecordSet[0]["DTMOV"];
$LegamiSISTRI	= false;
require("__scripts/MovimentiRifMovCarico.php");

if($SOGER->UserData["core_usersAutistaOTF"]=="1" && $IsAdr=="0"){
    $js = <<< ENDJS
    <script type="text/javascript">
    function show_hide(id){
        if(document.CreaAutista.HiddenTrasportatore.value<1)
            window.alert("Attenzione, nella scheda Generale non è stato selezionato il trasportatore");
        else
            $('#autista').toggle();
    }
    function creaAutista(){
        if(document.CreaAutista.nome.value=="" || document.CreaAutista.description.value=="")
            window.alert("Attenzione, tutti i campi sono obbligatori");
        else
            AjaxAutista("$TableName");
    }
    </script>
ENDJS;

    # firefox
    $css.="<style type=\"text/css\">\n";
    $css.="div#btn_nuovo_autista{\n";
    $css.="position: absolute; top: 282px; left: 854px; z-index: 10;";
    $css.="}\n";
    $css.="div#autista{\n";
    $css.="position:absolute;margin-left:-550px;z-index: 10; height: 225px;  padding: 3px; border: 2px #5C7D99 SOLID; background-color: #FFFFFF;";
    $css.="}\n";
    $css.="</style>\n";

    # ie
    $css.="<!--[if IE]>";
    $css.="<style type=\"text/css\">\n";
    $css.="div#btn_nuovo_autista{\n";
    $css.="position: absolute; top: 324px; left: 840px; z-index: 10;";
    $css.="}\n";
    $css.="div#autista{\n";
    $css.="position:absolute;margin-left:-550px;z-index: 10; height: 225px;  padding: 3px; border: 2px #5C7D99 SOLID; background-color: #FFFFFF;";
    $css.="}\n";
    $css.="</style>\n";
    $css.="<![endif]-->";

    echo $css;
    echo $js;

    $divAut ="<div id=\"btn_nuovo_autista\">\n";
        $divAut.="<input type=\"button\" class=\"FGEbutton\" name=\"\" value=\"Nuovo autista\" onclick=\"javascript:show_hide('autista');\" />\n";
        $divAut.="<div id=\"autista\" style=\"display:none;\">\n";
            $divAut.="<form name=\"CreaAutista\" method=\"post\" action=\"\" style=\"width:400px;\">\n";
                $divAut.="<fieldset class=\"FGEfieldset\" style=\"width:80%;margin-left:30px;\">\n";
                $divAut.="<legend class=\"FGElegend\">NUOVO AUTISTA</legend>\n";
                $divAut.="<div class=\"FGE1Col\" style=\"margin-left:30px;\">\n";
                    $divAut.="<label for=\"nome\" class=\"FGEmandatory\">nome</label><br/>\n";
                    $divAut.="<input type=\"text\" tabindex=\"1000\" class=\"FGEinput\" id=\"user_autisti:nome\" name=\"nome\" size=\"30\" maxlength=\"30\" value=\"\" onfocus=\"this.style.background='yellow';\"  onblur=\"this.style.background='white';\"/>\n";
                $divAut.="</div>\n";
                $divAut.="<div class=\"FGE1Col\" style=\"clear:both;margin-left:30px;\">\n";
                    $divAut.="<label for=\"description\" class=\"FGEmandatory\">cognome</label><br/>\n";
                    $divAut.="<input type=\"text\" tabindex=\"1001\" class=\"FGEinput\" id=\"user_autisti:description\" name=\"description\" size=\"30\" maxlength=\"30\" value=\"\" onfocus=\"this.style.background='yellow';\"  onblur=\"this.style.background='white';\"/>\n";
                $divAut.="</div>\n";
                $divAut.="<div class=\"FGESubmitRow\">\n";
                    $divAut.="<input type=\"button\" class=\"FGEbutton\" value=\"crea autista\" onclick=\"creaAutista();\" onfocus=\"this.style.background='yellow';this.style.color='#034373';\" onblur=\"this.style.background='#034373';this.style.color='white';\"/>\n";
                    $divAut.="<input type=\"button\" class=\"FGEbutton\" value=\"annulla\" onclick=\"javascript:show_hide('autista');\" onfocus=\"this.style.background='yellow';this.style.color='#034373';\" onblur=\"this.style.background='#034373';this.style.color='white';\"/>\n";
                $divAut.="</div>\n";
                $divAut.="</fieldset>\n";
                $divAut.="<input type=\"hidden\" name=\"HiddenTrasportatore\" value=\"".$ID_UIMT."\" />\n";
            $divAut.="</form>\n";
        $divAut.="</div>\n";
    $divAut.="</div>\n";
    echo $divAut;
}

############################
#......... FDA ............#
############################

$sql = "SELECT FDA_START_1, FDA_END_1, FDA_RESIDUI_1 FROM core_impianti WHERE ID_IMP='".$SOGER->UserData['core_impiantiID_IMP']."';";
$FEDIT->SDBRead($sql,"FDA",true,false);

$isFDA          = $SOGER->UserData['core_impiantiMODULO_FDA']? true:false;
if(
    strtotime(date('Y-M-d')) < strtotime($FEDIT->FDA[0]['FDA_START_1']) ||
    strtotime(date('Y-M-d')) > strtotime($FEDIT->FDA[0]['FDA_END_1']) ||
    $FEDIT->FDA[0]['FDA_RESIDUI_1'] < 1){
    $isFDA = false;
}

if(substr($FEDIT->DbServerData["db"], -4) != date('Y'))
    $isFDA = false;

if($SOGER->UserData['workmode'] != 'produttore')
    $isFDA = false;

if($isFDA){

    # FIREFOX CSS
    $fda_css.="<style type=\"text/css\">\n";
    $fda_css.="div#btn_verifica_targhe{\n";
    $fda_css.="position: absolute; top: 282px; left: 285px; z-index: 10;";
    $fda_css.="}\n";
    $fda_css.="div#btn_reset_verifica_targhe{\n";
    $fda_css.="position: absolute; top: 451px; left: 180px; z-index: 10;";
    $fda_css.="}\n";
    $fda_css.="</style>\n";
    echo $fda_css;

    $sql = "SELECT lov_cer.COD_CER AS cer, FKEcfiscT as codice_fiscale, NFORM, DTFORM ";
    $sql.= "FROM ".$_GET['table']." ";
    $sql.= "JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF ";
    $sql.= "JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER ";
    $sql.= "WHERE ".$_GET['pri']."=".$_GET['filter'].";";
    $FEDIT->SDBRead($sql,"FDA",true,false);

$FDA = <<< FDA
    <div id="btn_verifica_targhe">
        <input type="button" onblur="this.style.background='#034373';this.style.color='white';" onfocus="this.style.background='yellow';this.style.color='#034373';" value="Verifica targhe" class="FGEbutton">
    </div>

    <div id="btn_certificato_verifica_targhe" style="display:none;">
        <a href="__FDA/certificate.php?ID_MOV_F={$_GET['filter']}"><input type="button" onblur="this.style.background='#034373';this.style.color='white';" onfocus="this.style.background='yellow';this.style.color='#034373';" value="Download attestazione verifica" class="FGEbutton"></a>
    </div>

    <div id="btn_reset_verifica_targhe">
        <input type="button" onblur="this.style.background='#034373';this.style.color='white';" onfocus="this.style.background='yellow';this.style.color='#034373';" value="Reset Verifica targhe" class="FGEbutton">
    </div>

    <div id="Popup_FDA" title="Verifica compatibilità automezzi">
        <p>E\' in corso la verifica delle targhe nel database dell'Albo Nazionale Gestori Ambientali,
        l'operazione può richiedere qualche secondo.</p>

        <table style="width:100%;text-align:center;">
            <tbody>
                <tr>
                   <td style="width:33%;text-align:right;">
                        <span class="icona_pc"><img src="__css/icona_pc.jpg"></span>
                    </td>
                    <td style="width:34%;text-align:center;">
                        <span class="progress_bar hideWhenLoaded"><img src="__css/progress_bar.gif" /></span>
                    </td>
                    <td style="width:33%;text-align:left;">
                        <span class="icona_fda"><img src="__css/icona_fda.jpg"></span>
                    </td>
                </tr>
            </tbody>
        </table>

        <p id="LetturaFDAStatus">

            <input type="hidden" id="FDA-codice_fiscale" value="{$FEDIT->FDA[0]['codice_fiscale']}" />
            <input type="hidden" id="FDA-cer" value="{$FEDIT->FDA[0]['cer']}" />
            <input type="hidden" id="FDA-ID_MOV_F" value="{$_GET['filter']}" />
            <input type="hidden" id="FDA-NFORM" value="{$FEDIT->FDA[0]['NFORM']}" />
            <input type="hidden" id="FDA-DTFORM" value="{$FEDIT->FDA[0]['DTFORM']}" />

            <div class="hideWhenLoaded">
                <b>Stato avanzamento: </b>operazione in corso...
            </div>

            <div class="showWhenLoaded" style="text-align:center;display:none;">
                <b><u>La verifica ha dato esito <span id="FDAshortResponse"></span></u></b>
            </div>

            <div class="showWhenLoaded" style="display:none;margin-top:10px;">
                <span id="FDAfullResponse"></span>
            </div>
        </p>

    </div>
FDA;
    echo $FDA;
}
else{

	echo '<script type="text/javascript">';
	echo 'SOGER_CHECK_FDA = false;';
	echo '</script>';

}

if($SOGER->UserData["core_usersTargheOTF"]=="1"){

    $jsAction = $isFDA? "$('#btn_verifica_targhe').trigger('click');" : "AjaxTarghe('".$TableName."', 0);";

    $js = <<< ENDJSTARGHE
    <script type="text/javascript">
    function cleanTarghe(){
        document.CreaTarghe.user_automezzi_description.value="";
        document.CreaTarghe.user_rimorchi_description.value="";
        $('#BlackBox').prop('checked', false);
        $('#AdrAutomezzo').prop('checked', false);
        $('#AdrRimorchio').prop('checked', false);
        $('#ID_AUTO_TYPE').val(2);
        $('#ID_AUTO_TYPE').val(2);
    }
    function show_hide_targhe(id){
        if(document.CreaTarghe.HiddenTrasportatoreAuth.value<1)
            window.alert("Attenzione, nella scheda Generale non è stato selezionato il trasportatore");
        else{
            $('#targhe').toggle();
            if(!$('#targhe').is(":visible"))
                cleanTarghe();
        }
    }
    function CreaTargheFunct(){
        if(document.CreaTarghe.user_automezzi_description.value=="" && document.CreaTarghe.user_rimorchi_description.value=="")
            window.alert("Attenzione, almeno una delle due targhe deve essere inserita");
        else
            {$jsAction}
    }
    </script>
ENDJSTARGHE;

    # firefox
    $css.="<style type=\"text/css\">\n";
    $css.="div#btn_nuove_targhe{\n";
    $css.="position: absolute; top: 282px; left: 180px; z-index: 10;";
    $css.="}\n";
    $css.="table#NuoveTarghe, table#NuoveTarghe td, table#NuoveTarghe th{border:1px solid #000;}";
    $css.="table#NuoveTarghe th{background-color:#3C6383; color:#fff;}";
    $css.="div#targhe{\n";
    $css.="position:absolute;z-index: 10; height: 300px; width:620px; padding: 3px; border: 2px #5C7D99 SOLID; background-color: #FFFFFF;";
    $css.="}\n";
    $css.="</style>\n";

    # ie
    $css.="<!--[if IE]>";
    $css.="<style type=\"text/css\">\n";
    $css.="div#btn_nuove_targhe{\n";
    $css.="position: absolute; top: 324px; left: 640px; z-index: 10;";
    $css.="}\n";
    $css.="div#targhe{\n";
    $css.="position:absolute;z-index: 10; height: 300px; width:620px; padding: 3px; border: 2px #5C7D99 SOLID; background-color: #FFFFFF;";
    $css.="}\n";
    $css.="</style>\n";
    $css.="<![endif]-->";

    echo $css;
    echo $js;

    $display = $SOGER->UserData['core_impiantiMODULO_SIS']? 'inline':'none';

    $selectMezzi = <<< SELECT_MEZZI
        <select name="ID_AUTO_TYPE" id="ID_AUTO_TYPE" class="FGEinput" onfocus="this.style.background='yellow';" onblur="this.style.background='white';">
            <option value="1">ALTRO </option>
            <option value="2" selected>AUTOCARRO </option>
            <option value="3">CASSONE </option>
            <option value="4">CISTERNA </option>
            <option value="5">COMPATTATORE </option>
            <option value="6">CONTAINER CISTERNA </option>
            <option value="7">FURGONE </option>
            <option value="11">MOTRICE </option>
            <option value="8">PIANALE </option>
            <option value="9">RIBALTABILE </option>
            <option value="14">TRATTORE </option>
            <option value="15">TRATTRICE AGRICOLA </option>
            <option value="10">VAGONE FEROVIARIO </option>
        </select>
SELECT_MEZZI;

    $selectRimorchi = <<< SELECT_RIMORCHI
        <select name="ID_RMK_TYPE" id="ID_RMK_TYPE" class="FGEinput" onfocus="this.style.background='yellow';" onblur="this.style.background='white';">
            <option value="1">ALTRO </option>
            <option value="2" selected>CASSONE </option>
            <option value="3">CISTERNA </option>
            <option value="6">CONTAINER CISTERNA </option>
            <option value="8">PIANALE </option>
        </select>
SELECT_RIMORCHI;

    $divtarghe = <<< DIVTARGHE
        <div id="btn_nuove_targhe">
            <input type="button" class="FGEbutton" name="" value="Nuove targhe" onclick="javascript:show_hide_targhe('targhe');" />
            <div id="targhe" style="display:none;">
                <form name="CreaTarghe" method="post" action="" style="width:400px;">
                    <fieldset class="FGEfieldset" style="width:540px;margin-left:30px;">
                    <legend class="FGElegend">NUOVE TARGHE</legend>

                        <table id="NuoveTarghe" cellpadding="10" cellspacing="0">
                            <tr>
                                <th colspan="3">&nbsp;</th>
                                <th style="text-align:center;"><b>ADR?</b></th>
                                <th style="text-align:center;"><span style="display:{$display}"><b>Black box?</b></span></th>
                            </tr>
                            <tr>
                                <th>Targa automezzo</th>
                                <td><input style="width:80px;" type="text" class="FGEinput" id="user_automezzi:description" name="user_automezzi_description" size="30" maxlength="30" value="" onfocus="this.style.background='yellow';"  onblur="this.style.background='white';"/></td>
                                <td>{$selectMezzi}</td>
                                <td style="text-align:center;"><input type="checkbox" id="AdrAutomezzo" name="AdrAutomezzo" /></td>
                                <td style="text-align:center;"><input style="display:{$display}" type="checkbox" id="BlackBox" name="BlackBox" /></td>
                            </tr>
                            <tr>
                                <th>Targa rimorchio</th>
                                <td><input style="width:80px;" type="text" class="FGEinput" id="user_rimorchi:description" name="user_rimorchi_description" size="30" maxlength="30" value="" onfocus="this.style.background='yellow';"  onblur="this.style.background='white';"/></td>
                                <td>{$selectRimorchi}</td>
                                <td style="text-align:center;"><input type="checkbox" id="AdrRimorchio" name="AdrRimorchio" /></td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>

                    <div class="FGESubmitRow">
                        <input type="button" class="FGEbutton" value="crea targhe" onclick="CreaTargheFunct();" onfocus="this.style.background='yellow';this.style.color='#034373';" onblur="this.style.background='#034373';this.style.color='white';"/>
                        <input type="button" class="FGEbutton" value="annulla" onclick="javascript:show_hide_targhe('autista');" onfocus="this.style.background='yellow';this.style.color='#034373';" onblur="this.style.background='#034373';this.style.color='white';"/>
                    </div>
                    </fieldset>
                    <input type="hidden" name="HiddenTrasportatoreAuth" value="{$AuthTRA}" />
                    <input type="hidden" name="HiddenTrasportatoreAdr" value="{$IsAdr}" />
                </form>
            </div>
        </div>
DIVTARGHE;
    echo $divtarghe;
}


require_once("__includes/COMMON_sleepForgEdit.php");


if(!isset($AutomezzoRosette)){
$sql="SELECT ID_ORIGINE_DATI FROM user_automezzi WHERE ID_AUTO='".$ID_AUTO."';";
$FEDIT->SdbRead($sql,"DbRecordSetID_AUTO",true,false);
$AutomezzoRosette="'".$FEDIT->DbRecordSetID_AUTO[0]["ID_ORIGINE_DATI"]."'";
$js ="<script type=\"text/javascript\">\n";
$js.= <<< ENDJS
CUSTOM_ROSETTE('rosetteA',$AutomezzoRosette);
ENDJS;
$js.= "</script>";
echo $js;
}

if(!isset($RimorchioRosette)){
$sql="SELECT ID_ORIGINE_DATI FROM user_rimorchi WHERE ID_RMK='".$ID_RMK."';";
$FEDIT->SdbRead($sql,"DbRecordSetID_RMK",true,false);
$RimorchioRosette="'".$FEDIT->DbRecordSetID_RMK[0]["ID_ORIGINE_DATI"]."'";
$js ="<script type=\"text/javascript\">\n";
$js.= <<< ENDJS
CUSTOM_ROSETTE('rosetteR',$RimorchioRosette);
ENDJS;
$js.= "</script>";
echo $js;
}


if(isset($Automezzo) && !$multipleAutomezzi) {

$js ="<script type=\"text/javascript\">\n";
$js.= <<< ENDJS
CUSTOM_ROSETTE('rosetteA',$AutomezzoRosette);
ENDJS;
$js.= "</script>";
echo $js;

?>
<script type="text/javascript">
	// automezzo / adr (se disponibili)
	//CUSTOM_ROSETTE('rosetteA',$AutomezzoRosette);
	document.FGEForm_1["<?php echo $TableName; ?>:FKEadrAUT"].value = '<?php echo $AdrAutomezzo; ?>';
	document.FGEForm_1["<?php echo $TableName; ?>:FKEmzAUT"].value = '<?php echo $Automezzo; ?>';
	document.tmp_contoterzi_mezzi["automezzoID_LIM"].value = '<?php echo $ID_LIM; ?>';
</script>
<?php
}

?>
<script type="text/javascript">
	// ora e data inizio trasporto automatica
	<?php if($SOGER->UserData['core_usersFRM_SET_DATE']=='1'){ ?>
		if(document.FGEForm_1["<?php echo $TableName; ?>:dt_in_trasp"].value=="")
			document.FGEForm_1["<?php echo $TableName; ?>:dt_in_trasp"].value = '<?php echo $DTMOV_IN_TRASP? date("d/m/Y", strtotime($DTMOV_IN_TRASP)):date("d/m/Y"); ?>';
	<?php } ?>

	<?php if($SOGER->UserData['core_usersFRM_SET_HR']=='1'){ ?>
		if(document.FGEForm_1["<?php echo $TableName; ?>:hr_in_trasp"].value=="")
			document.FGEForm_1["<?php echo $TableName; ?>:hr_in_trasp"].value = '<?php echo date("H:i"); ?>';
	<?php } ?>
</script>
