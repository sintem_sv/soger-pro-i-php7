<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
#
$FEDIT->FGE_UseTables(array("lov_comuni_istat"));
$FEDIT->FGE_SetSelectFields(array("description","CAP","des_prov","shdes_prov","des_reg","nazione"),"lov_comuni_istat");
#
$FEDIT->FGE_DescribeFields();
#
?>


<?php

echo($FEDIT->FGE_DataGrid($SOGER->AppDescriptiveLocation,"__scripts/FGE_DataGridEdit.php","-----",true,"FEDIT",$_SESSION['SearchingFilter']));
#
require_once("__includes/COMMON_sleepForgEdit.php");
?>

<div class="FGE4Col" style="margin: 6px;">
<input type="button" class="FGEbutton" name="nuovo" value="crea nuovo comune" onclick="javascript: document.location='__scripts/status.php?area=UserEditComune<?php echo substr($SOGER->AppLocation, -1, 1); ?>'"/>
</div>
