<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;
$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_schede_rifiuti","lov_cer");
$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
//$FEDIT->FGE_SetSelectFields(array("ID_IMP","descrizione","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_rifiuti");
$FEDIT->FGE_SetSelectFields(array("ID_IMP","descrizione","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
$FEDIT->FGE_DescribeFields();
$ProfiloFields = "user_schede_rifiuti";
include("SOGER_FiltriProfilo.php");
#
$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
#
//$FEDIT->FGE_HideFields(array("ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_rifiuti");
$FEDIT->FGE_HideFields(array("ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
#
echo $FEDIT->SG_CaricoScaricoGrid($SOGER->AppDescriptiveLocation,"__scripts/RichiesteCaricoScaricoRTF.php","E--");
require_once("__includes/COMMON_sleepForgEdit.php");
?>
