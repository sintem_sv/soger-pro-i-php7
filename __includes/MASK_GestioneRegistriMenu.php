<?php 
global $SOGER; 
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
$FEDIT = unserialize($_SESSION['ForgEditBuffer']);
?>

<div class="divName"><span>GESTIONE REGISTRI</span></div>


<!-- REGISTRO INDUSTRIALE -->
<?php if($SOGER->UserData['core_impiantiREG_IND']=='1'){ ?>
<a href="__scripts/status.php?area=RegistroIndustriale" title="Registro industriale"><img src="__css/SMN_RegInd.gif" alt="Stampa registro industriale" class="NoBorder"/></a>
<?php } ?>

<!-- REGISTRO FISCALE -->
<a href="__scripts/status.php?area=RegistroFiscale" title="Registro fiscale"><img src="__css/SMN_RegFisc.gif" alt="Stampa registro fiscale" class="NoBorder"/></a>

<!-- REGISTRO UNICO P+D INVEMET -->
<?php if($SOGER->UserData['core_usersID_IMP']=='001INMIM'){ ?>
<a href="__scripts/status.php?area=RegistroUnicoPD" title="Registro Unico (P+D)"><img src="__css/SMN_RegUnicoPD.gif" alt="Stampa registro unico (P+D)" class="NoBorder"/></a>
<?php } ?>

<!-- REGISTRO FANGHI -->
<?php if($SOGER->UserData['core_usersFANGHI']=='1'){ ?>
<a href="__scripts/status.php?area=RegistroFanghi" title="Registro fanghi"><img src="__css/SMN_RegFanghi.gif" alt="Stampa registro fanghi" class="NoBorder"/></a>
<?php } ?>

<!-- REGISTRO ADR -->
<a href="__scripts/RegistroADR.php" title="Registro ADR"><img src="__css/SMN_RegADR.gif" alt="Stampa registro ADR" class="NoBorder"/></a>

<!-- NUMERAZIONE REGISTRO -->
<a href="__scripts/status.php?area=NumerazioneRegistro" title="Numerazione registro"><img src="__css/SMN_RegNum.gif" alt="Numerazione registro" class="NoBorder"/></a>

<!-- DOWNLOAD FRONTESPIZIO -->
<a href="__templates/Frontespizi.zip" target="_blank" title="Download frontespizio registro"><img src="__css/SMN_DownloadFrontespizio.gif" alt="Download frontespizio registro" class="NoBorder"/></a>

<!-- CONFIGURA NOTIFICHE -->
<?php
$SQL="SELECT mail FROM user_promemoria WHERE ID_USR='".$SOGER->UserData['core_usersID_USR']."';";
$FEDIT->SDBRead($SQL,"DbRecordSet",true,false);
//die(var_dump($FEDIT->DbRecordSet));
if(is_null($FEDIT->DbRecordSet)){
?>

<a href="__scripts/status.php?area=ConfiguraNotifiche" title="Configura notifiche"><img src="__css/SMN_ConfiguraNotificheRed.gif" alt="Configura notifiche" class="NoBorder"/></a>

<?php }else{ ?>

<a href="__scripts/status.php?area=ConfiguraNotifiche" title="Configura notifiche"><img src="__css/SMN_ConfiguraNotifiche.gif" alt="Configura notifiche" class="NoBorder"/></a>

<?php
	}


$_SESSION['ForgEditBuffer'] = serialize($FEDIT);
?>