<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require_once("__includes/COMMON_wakeForgEdit.php");
#
global $SOGER;


$FEDIT->FGE_FlushTableInfo();
$FEDIT->FGE_UseTables("user_schede_rifiuti_etsym");
$FEDIT->FGE_SetFormFields(array("ID_RIF","ID_IMP","P003","P011","P002","M004","M016","M013","M009","M008","M001","M014","M010","M015","M003","W003","W016","W021","W023","W028","W002","W001","W022","E003","E012","E011","E013","E004"),"user_schede_rifiuti_etsym");

//$sql = "SELECT ID_SF, pericoloso, H1, H2, H3A, H3B, H4, H5, H6, H7, H8, H9, H10, H11, H12, H13, H14, H15 FROM user_schede_rifiuti WHERE ID_RIF='".$_GET['ID_RIF']."';";
$sql = "SELECT ID_SF, pericoloso, HP1, HP2, HP3, HP4, HP5, HP6, HP7, HP8, HP9, HP10, HP11, HP12, HP13, HP14, HP15 FROM user_schede_rifiuti WHERE ID_RIF='".$_GET['ID_RIF']."';";
$FEDIT->SdbRead($sql,"DbRecordSetInfoRif",true,false);

if(!isset($_GET["pri"]) && !isset($_GET["filter"])) {

	$FEDIT->FGE_DescribeFields();

	$FEDIT->FGE_LookUpDefault("ID_IMP","user_schede_rifiuti_etsym");

	#
	$FEDIT->FGE_LookUpCFG("ID_RIF","user_schede_rifiuti_etsym");
	$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
	//$FEDIT->FGE_SetSelectFields(array("ID_RIF","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("ID_RIF","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
	$ProfiloFields = "user_schede_rifiuti";
	include("SOGER_FiltriProfilo.php");
	$FEDIT->FGE_SetOrder("lov_cer:COD_CER");
	//$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_rifiuti");
	$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#

	## SEMPRE
	$FEDIT->FGE_SetValue("P002","1","user_schede_rifiuti_etsym");	
	$FEDIT->FGE_SetValue("M009","1","user_schede_rifiuti_etsym");
	$FEDIT->FGE_SetValue("M008","1","user_schede_rifiuti_etsym");
	
	## INDEFINITO
	$FEDIT->FGE_SetValue("W003","0","user_schede_rifiuti_etsym");
	$FEDIT->FGE_SetValue("W022","0","user_schede_rifiuti_etsym");
	$FEDIT->FGE_SetValue("M014","0","user_schede_rifiuti_etsym");
	$FEDIT->FGE_SetValue("M015","0","user_schede_rifiuti_etsym");
	$FEDIT->FGE_SetValue("M003","0","user_schede_rifiuti_etsym");
	$FEDIT->FGE_SetValue("M016","0","user_schede_rifiuti_etsym");
	$FEDIT->FGE_SetValue("M001","0","user_schede_rifiuti_etsym");
	$FEDIT->FGE_SetValue("E013","0","user_schede_rifiuti_etsym");
	
	if($FEDIT->DbRecordSetInfoRif[0]["HP3"]=="1"){
		$FEDIT->FGE_SetValue("P003","1","user_schede_rifiuti_etsym");
		$FEDIT->FGE_SetValue("W021","1","user_schede_rifiuti_etsym");
		}
	else{
		$FEDIT->FGE_SetValue("P003","0","user_schede_rifiuti_etsym");
		$FEDIT->FGE_SetValue("W021","0","user_schede_rifiuti_etsym");
		}

	if($FEDIT->DbRecordSetInfoRif[0]["HP2"]=="1"){
		$FEDIT->FGE_SetValue("P011","1","user_schede_rifiuti_etsym");
		$FEDIT->FGE_SetValue("W028","1","user_schede_rifiuti_etsym");
		}
	else{
		$FEDIT->FGE_SetValue("P011","0","user_schede_rifiuti_etsym");
		$FEDIT->FGE_SetValue("W028","0","user_schede_rifiuti_etsym");
		}

	if($FEDIT->DbRecordSetInfoRif[0]["HP1"]=="1")
		$FEDIT->FGE_SetValue("W002","1","user_schede_rifiuti_etsym");
	else
		$FEDIT->FGE_SetValue("W002","0","user_schede_rifiuti_etsym");

	if($FEDIT->DbRecordSetInfoRif[0]["HP6"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP7"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP10"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP11"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP12"]=="1")
		$FEDIT->FGE_SetValue("W016","1","user_schede_rifiuti_etsym");
	else
		$FEDIT->FGE_SetValue("W016","0","user_schede_rifiuti_etsym");

	if($FEDIT->DbRecordSetInfoRif[0]["HP8"]=="1")
		$FEDIT->FGE_SetValue("W023","1","user_schede_rifiuti_etsym");
	else
		$FEDIT->FGE_SetValue("W023","0","user_schede_rifiuti_etsym");

	if($FEDIT->DbRecordSetInfoRif[0]["HP4"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP5"]=="1")
		$FEDIT->FGE_SetValue("W001","1","user_schede_rifiuti_etsym");
	else
		$FEDIT->FGE_SetValue("W001","0","user_schede_rifiuti_etsym");

	if($FEDIT->DbRecordSetInfoRif[0]["ID_SF"]=="4" | $FEDIT->DbRecordSetInfoRif[0]["pericoloso"]=="1")
		$FEDIT->FGE_SetValue("M004","1","user_schede_rifiuti_etsym");
	else
		$FEDIT->FGE_SetValue("M004","0","user_schede_rifiuti_etsym");

	if($FEDIT->DbRecordSetInfoRif[0]["ID_SF"]=="4" | $FEDIT->DbRecordSetInfoRif[0]["HP7"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP10"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP11"]=="1")
		$FEDIT->FGE_SetValue("M010","1","user_schede_rifiuti_etsym");
	else
		$FEDIT->FGE_SetValue("M010","0","user_schede_rifiuti_etsym");

	if($FEDIT->DbRecordSetInfoRif[0]["ID_SF"]=="4" | $FEDIT->DbRecordSetInfoRif[0]["HP8"]=="1"){
		$FEDIT->FGE_SetValue("M013","1","user_schede_rifiuti_etsym");
		$FEDIT->FGE_SetValue("E012","1","user_schede_rifiuti_etsym");
		}
	else{
		$FEDIT->FGE_SetValue("M013","0","user_schede_rifiuti_etsym");
		$FEDIT->FGE_SetValue("E012","0","user_schede_rifiuti_etsym");
		}

	if($FEDIT->DbRecordSetInfoRif[0]["pericoloso"]=="1"){
		$FEDIT->FGE_SetValue("W001","1","user_schede_rifiuti_etsym");
		$FEDIT->FGE_SetValue("E003","1","user_schede_rifiuti_etsym");
		$FEDIT->FGE_SetValue("E004","1","user_schede_rifiuti_etsym");
		}
	else{
		$FEDIT->FGE_SetValue("W001","0","user_schede_rifiuti_etsym");
		$FEDIT->FGE_SetValue("E003","0","user_schede_rifiuti_etsym");
		$FEDIT->FGE_SetValue("E004","0","user_schede_rifiuti_etsym");
		}

	if($FEDIT->DbRecordSetInfoRif[0]["HP4"]=="1" | $FEDIT->DbRecordSetInfoRif[0]["HP8"]=="1")
		$FEDIT->FGE_SetValue("E011","1","user_schede_rifiuti_etsym");
	else
		$FEDIT->FGE_SetValue("E011","0","user_schede_rifiuti_etsym");

	$FEDIT->FGE_SetValue("ID_RIF",$_GET['ID_RIF'],"user_schede_rifiuti_etsym");
	$FEDIT->FGE_SetValue("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti_etsym");
	
	$FEDIT->FGE_HideFields(array("ID_IMP"),"user_schede_rifiuti_etsym");
	$FEDIT->FGE_DisableFields(array("ID_RIF"),"user_schede_rifiuti_etsym");

	$FEDIT->FGE_SetTitle("P003","Segnali di divieto","user_schede_rifiuti_etsym");
	$FEDIT->FGE_SetTitle("M004","Segnali di obbligo","user_schede_rifiuti_etsym");
	$FEDIT->FGE_SetTitle("W003","Segnali di pericolo","user_schede_rifiuti_etsym");
	$FEDIT->FGE_SetTitle("E003","Segnali di emergenza","user_schede_rifiuti_etsym");

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"salva segnaletica");

} else {


	$FEDIT->FGE_DescribeFields();
	$FEDIT->FGE_LookUpDefault("ID_IMP","user_schede_rifiuti_etsym");

	#
	$FEDIT->FGE_LookUpCFG("ID_RIF","user_schede_rifiuti_etsym");
	$FEDIT->FGE_UseTables("lov_cer","user_schede_rifiuti");
	//$FEDIT->FGE_SetSelectFields(array("ID_RIF","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("ID_RIF","descrizione","ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_SetSelectFields(array("COD_CER"),"lov_cer");
	$FEDIT->FGE_SetFilter("ID_IMP",$SOGER->UserData["core_impiantiID_IMP"],"user_schede_rifiuti");
	$ProfiloFields = "user_schede_rifiuti";
	include("SOGER_FiltriProfilo.php");
	$FEDIT->FGE_SetOrder("lov_cer:COD_CER");
	//$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario","idSIS_regCrono"),"user_schede_rifiuti");
	$FEDIT->FGE_HideFields(array("ID_CER","ID_IMP","produttore","trasportatore","destinatario","intermediario"),"user_schede_rifiuti");
	$FEDIT->FGE_LookUpDescribe();
	$FEDIT->FGE_LookUpDone();
	#


	$FEDIT->FGE_HideFields(array("ID_IMP"),"user_schede_rifiuti_etsym");
	$FEDIT->FGE_DisableFields(array("ID_RIF"),"user_schede_rifiuti_etsym");

	$FEDIT->FGE_SetTitle("P003","Segnali di divieto","user_schede_rifiuti_etsym");
	$FEDIT->FGE_SetTitle("M004","Segnali di obbligo","user_schede_rifiuti_etsym");
	$FEDIT->FGE_SetTitle("W003","Segnali di pericolo","user_schede_rifiuti_etsym");
	$FEDIT->FGE_SetTitle("E003","Segnali di emergenza","user_schede_rifiuti_etsym");

	echo $FEDIT->FGE_MakeForm($SOGER->AppDescriptiveLocation,"modifica e salva segnaletica");

	}

require_once("__includes/COMMON_sleepForgEdit.php");
?>
