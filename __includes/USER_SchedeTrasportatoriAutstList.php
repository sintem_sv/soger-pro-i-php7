<?php
#
#	ForgEdit
#
require_once("__includes/COMMON_ForgEditClassFiles.php");
require("__includes/COMMON_wakeForgEditDG.php");
#
global $SOGER;
$FEDG->FGE_FlushTableInfo();
$FEDG->FGE_UseTables("user_autisti","user_impianti_trasportatori");
$FEDG->FGE_SetSelectFields(array("nome","description","adr","patente","rilascio","scadenza"),"user_autisti");
$FEDG->FGE_SetSelectFields(array("ID_AZT","description"),"user_impianti_trasportatori");
#
$FEDG->FGE_DescribeFields();
$FEDG->FGE_SetOrder("user_autisti:description");
$FEDG->FGE_SetFilter("ID_AZT",$_SESSION["FGE_IDs"]["user_aziende_trasportatori"],"user_impianti_trasportatori");
$FEDG->FGE_HideFields("ID_AZT","user_impianti_trasportatori");
#
echo($FEDG->FGE_DataGrid("Autisti in Archivio","__scripts/FGE_DataGridEdit.php","EDC--",true,"FEDG",$_SESSION['SearchingFilter']));
require_once("__includes/COMMON_sleepForgEditDG.php");
?>
<div class="FGE4Col">
<input type="button" class="FGEbutton" name="nuovo" value="crea nuovo autista" onclick="javascript: document.location='<?php echo $_SERVER["PHP_SELF"]?>'"/>
</div>
