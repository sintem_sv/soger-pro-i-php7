<?php
session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__libs/SQLFunct.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");

# Index1: Indice medio di saturazione dei trasporti
# Index2: Indice medio di trasporto: tonnellate trasportate per viaggio
# Index3: Indice medio di trasporto: tonnellate trasportate per kilometro


# COLONNA (PERIODO)
if($_POST['mese']!='')		$COLUMN = $_POST['mese'];
if($_POST['trimestre']!='') $COLUMN = $_POST['trimestre'];
if($_POST['anno']!='')		$COLUMN = 'anno';

# RULES
$RULES = ' AND TIPO="S" ';
$RULES.= ' AND user_movimenti_fiscalizzati.produttore=1 AND user_movimenti_fiscalizzati.trasportatore=0 AND user_movimenti_fiscalizzati.intermediario=0 AND user_movimenti_fiscalizzati.destinatario=0 ';
$RULES.= ' AND ID_UIMP<>0 ';
$RULES.= ' AND ID_UIMT<>0 ';
$RULES.= ' AND ID_UIMD<>0 ';
$RULES.= ' AND Transfrontaliero=0 ';

# TIME
if($COLUMN!='anno'){
	if(!strstr($COLUMN, "trim_")){
		$TIME.=" AND MONTHNAME(DTFORM)='".$COLUMN."' ";
		}
	else{
		switch($COLUMN){
			case "trim_I":
				$TIME.=" AND MONTHNAME(DTFORM) IN ('January', 'February', 'March') ";
				break;
			case "trim_II":
				$TIME.=" AND MONTHNAME(DTFORM) IN ('April', 'May', 'June') ";
				break;
			case "trim_III":
				$TIME.=" AND MONTHNAME(DTFORM) IN ('July', 'August', 'September') ";
				break;
			case "trim_IV":
				$TIME.=" AND MONTHNAME(DTFORM) IN ('October', 'November', 'December') ";
				break;
			}
		}
	}
$TIME.=" AND YEAR(DTFORM) = '".substr($_SESSION["DbInUse"], -4)."' ";

for($r=0;$r<count($_POST['CER']);$r++){

	# ID CER
	$CER	= $_POST['CER'][$r];

	# CODICE CER
	$CERCODE= $_POST['CERCODE'][$r];

	# ID RIFIUTO
	$RIF	= $_POST['RIF'][$r];


	# FSS
		# INDICE MIO
		$SQL="SELECT ".$COLUMN." FROM sdg_index1 WHERE ID_RIF='".$RIF."'";
		$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
		$MyINDEX = $FEDIT->DbRecordSet[0][$COLUMN];

		# INDICE DI RIFERIMENTO
		$SQL="SELECT ".$COLUMN." FROM sdg_index1_nazionali WHERE ID_CER='".$CER."'";
		$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
		$NazINDEX = $FEDIT->DbRecordSet[0][$COLUMN];

		# SCARTO % TRA INDICI - FATTORE DI SCOSTAMENTO
		if($MyINDEX>0 AND $NazINDEX>0)
			$FSS = round( ( ( $MyINDEX - $NazINDEX ) / $NazINDEX * 100), 2);
		else
			$FSS = 'n.d.';

	# FST
		# INDICE MIO
		$SQL="SELECT ".$COLUMN." FROM sdg_index2 WHERE ID_RIF='".$RIF."'";
		$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
		$MyINDEX = $FEDIT->DbRecordSet[0][$COLUMN];

		# INDICE DI RIFERIMENTO
		$SQL="SELECT ".$COLUMN." FROM sdg_index2_nazionali WHERE ID_CER='".$CER."'";
		$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
		$NazINDEX = $FEDIT->DbRecordSet[0][$COLUMN];

		# SCARTO % TRA INDICI - FATTORE DI SCOSTAMENTO
		if($MyINDEX>0 AND $NazINDEX>0)
			$FST = round( ( ( $MyINDEX - $NazINDEX ) / $NazINDEX * 100), 2);
		else
			$FST = 'n.d.';

	# FSK
		# INDICE MIO
		$SQL="SELECT ".$COLUMN." FROM sdg_index3 WHERE ID_RIF='".$RIF."'";
		$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
		$MyINDEX = $FEDIT->DbRecordSet[0][$COLUMN];

		# INDICE DI RIFERIMENTO
		$SQL="SELECT ".$COLUMN." FROM sdg_index3_nazionali WHERE ID_CER='".$CER."'";
		$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
		$NazINDEX = $FEDIT->DbRecordSet[0][$COLUMN];

		# SCARTO % TRA INDICI - FATTORE DI SCOSTAMENTO
		if($MyINDEX>0 AND $NazINDEX>0)
			$FSK = round( ( ( $MyINDEX - $NazINDEX ) / $NazINDEX * 100), 2);
		else
			$FSK = 'n.d.';

	# FSM & CEA
	if($FSS!='n.d.' AND $FST!='n.d.' AND $FSK!='n.d.'){
		$FSM	= round((($FSS + $FST + $FSK) / 3), 2);
		if($FSM>60)
			$CEA='A';
		elseif($FSM<=60 AND $FSM>20)
			$CEA='B';
		elseif($FSM<=20 AND $FSM>-20)
			$CEA='C';
		elseif($FSM<=-20 AND $FSM>-60)
			$CEA='D';
		elseif($FSM<=-60)
			$CEA='E';
		}
	else{
		$FSM	= 'n.d.';
		$CEA	= 'n.d.';
		}

/*
	# KG A DESTINO
	$SQL ="SELECT ROUND( SUM( IF( PS_DESTINO >0, PS_DESTINO, pesoN ) ) ) AS KG FROM user_movimenti_fiscalizzati ";
	$SQL.="WHERE user_movimenti_fiscalizzati.ID_RIF=".$RIF." ";
	$SQL.=$RULES;
	$SQL.=$TIME;
	$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
	$KG = $FEDIT->DbRecordSet[0]['KG'];
*/
	# prepare json string to return
	$FS[$r]["FSS"]	= $FSS;
	$FS[$r]["FST"]	= $FST;
	$FS[$r]["FSK"]	= $FSK;
	$FS[$r]["FSM"]	= $FSM;
	$FS[$r]["CEA"]	= $CEA;
	//$FS[$r]["KG"]	= number_format($KG, 0, ',', '.');

	}

echo json_encode($FS);

require_once("../__includes/COMMON_sleepForgEdit.php");
require_once("../__includes/COMMON_sleepSoger.php");

?>