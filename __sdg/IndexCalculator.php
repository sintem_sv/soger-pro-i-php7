<?php
session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__libs/SQLFunct.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("IndexCalculator.php");

# Index1 - IS: Indice medio di saturazione dei trasporti
# Index2 - IT: Indice medio di trasporto: tonnellate trasportate per viaggio
# Index3 - IK: Indice medio di trasporto: tonnellate trasportate per kilometro

function getISindex($CER, $RIF, $RULES, $TIME, &$FEDITobj){

	// Media trasportata del CER
	$SQL ="SELECT SUM( IF( PS_DESTINO >0, PS_DESTINO, pesoN ) ) AS QTA, COUNT( ID_MOV_F ) AS VIAGGI FROM user_movimenti_fiscalizzati ";
	$SQL.="WHERE ID_RIF=".$RIF." ";
	$SQL.=$RULES;
	$SQL.=$TIME;
	$FEDITobj->SdbRead($SQL,"DbRecordSet",true,false);
	$QTA	= $FEDITobj->DbRecordSet[0]['QTA'] / 1000; // --> converto kg in tonnellate
	$Viaggi	= $FEDITobj->DbRecordSet[0]['VIAGGI'];
	if($Viaggi>0)
		$CM		= $QTA/$Viaggi;
	else
		$CM		= 0;

	if($CM>0){
		// [10%] conferimenti
		$SQL ="SELECT COUNT(ID_MOV_F) AS VIAGGI_TOTALI FROM user_movimenti_fiscalizzati JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF ";
		$SQL.="WHERE user_schede_rifiuti.ID_CER=".$CER." ";
		$SQL.=$RULES;
		$SQL.=$TIME;
		$FEDITobj->SdbRead($SQL,"DbRecordSet",true,false);
		$ViaggiTotali	= $FEDITobj->DbRecordSet[0]['VIAGGI_TOTALI'];
		$LIMIT	= round($ViaggiTotali/10); 
		if($LIMIT==0) $LIMIT=1;		
	
		// Media trasportata nei [10%] migliori conferimenti
		$SQL= "SELECT SUM(QTA) AS QTAtop FROM (";
			$SQL.="SELECT IF( PS_DESTINO >0, PS_DESTINO, pesoN ) AS QTA FROM user_movimenti_fiscalizzati ";
			$SQL.="JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF ";
			$SQL.="WHERE user_schede_rifiuti.ID_CER=".$CER." ";
			$SQL.=$RULES;
			$SQL.=$TIME;
		$SQL.="ORDER BY pesoN DESC LIMIT 0, ".$LIMIT.") AS subquery";
		$FEDITobj->SdbRead($SQL,"DbRecordSet",true,false);
		$QTAtop	= $FEDITobj->DbRecordSet[0]['QTAtop'];
		$QTAtop = $QTAtop/1000; // --> converto kg in tonnellate
		$CMax	= $QTAtop/$LIMIT;

		// IS
		$IS		= $CM / $CMax;
		$IS		= round($IS, 2);
		}
	else
		// IS
		$IS		= 0;	

	return $IS;
	}



function getITindex($CER, $RIF, $RULES, $TIME, &$FEDITobj){
	// Media trasportata del CER
	$SQL ="SELECT SUM( IF( PS_DESTINO >0, PS_DESTINO, pesoN ) ) AS QTA, COUNT( ID_MOV_F ) AS VIAGGI FROM user_movimenti_fiscalizzati ";
	$SQL.="WHERE ID_RIF=".$RIF." ";
	$SQL.=$RULES;
	$SQL.=$TIME;
	$FEDITobj->SdbRead($SQL,"DbRecordSet",true,false);
	$QTA	= $FEDITobj->DbRecordSet[0]['QTA'] / 1000; // --> converto kg in tonnellate
	$Viaggi	= $FEDITobj->DbRecordSet[0]['VIAGGI'];
	if($Viaggi>0){
		$IT		= $QTA/$Viaggi;
		$IT		= round($IT, 2);
		}
	else
		$IT		= 0;
	return $IT;
	}

function getIKindex($CER, $RIF, $RULES, $TIME, &$FEDITobj){
	$SQL ="SELECT SUM( IF( PS_DESTINO >0, PS_DESTINO, pesoN ) ) AS QTA, SUM(Km) AS km ";
	$SQL.="FROM user_movimenti_fiscalizzati WHERE ID_RIF=".$RIF." ";
	$SQL.=$RULES;
	$SQL.=$TIME;
	$FEDITobj->SdbRead($SQL,"DbRecordSet",true,false);
	$QTA	= $FEDITobj->DbRecordSet[0]['QTA'];
	$km		= $FEDITobj->DbRecordSet[0]['km']/1000; // --> converto m in km
	if($km>0){
		$IK	= $QTA / $km;
		$IK = round($IK, 2);
		}
	else
		$IK = 0;

	return $IK;
	}


require_once("../__includes/COMMON_sleepForgEdit.php");
?>