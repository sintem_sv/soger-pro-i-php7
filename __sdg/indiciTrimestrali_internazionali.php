<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);


include("indiciRules.php");

# Index1: Indice medio di saturazione dei trasporti
# Index2: Indice medio di trasporto: tonnellate trasportate per viaggio
# Index3: Indice medio di trasporto: tonnellate trasportate per kilometro
# Index4: Indice medio di permanenza in sito


$TRIMESTRE[0]['months'] = "'July', 'August', 'September'";
$TRIMESTRE[0]['number'] = "III";
$TRIMESTRE[0]['year']	= "2013";
/*
$TRIMESTRE[0]['months'] = "'April', 'May', 'June'";
$TRIMESTRE[0]['number'] = "II";
$TRIMESTRE[0]['year']	= "2012";

$TRIMESTRE[0]['months'] = "'July', 'August', 'September'";
$TRIMESTRE[0]['number'] = "III";
$TRIMESTRE[0]['year']	= "2012";

$TRIMESTRE[0]['months'] = "'October', 'November', 'December'";
$TRIMESTRE[0]['number'] = "IV";
$TRIMESTRE[0]['year']	= "2012";
*/

$link = mysqli_connect('localhost', 'root', 'Ius87P78JskU');

if (!$link) {
    echo "Unable to connect to DB: " . mysqli_connect_error();
    exit;
}

for($t=0;$t<count($TRIMESTRE);$t++){

	# selezione db
	if (!mysqli_select_db("soger".$TRIMESTRE[$t]['year'])) {
		echo "Unable to select mydbname: " . mysqli_error($link);
		exit;
		}

	$SQL='SELECT * FROM lov_cer WHERE ID_CER IN (SELECT ID_CER FROM sdg_index1_internazionali WHERE trim_'.$TRIMESTRE[$t]['number'].' IS NULL);';
	//$SQL='SELECT * FROM lov_cer;';
	$cer = mysqli_query($link,$SQL);
	while($tmpCer = mysqli_fetch_assoc($cer)){

		echo "<h1>".$tmpCer['COD_CER']."</h1>";

		# Index1 - retrive data

		// Media trasportata del CER
		$SQL='SELECT SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN ) )/1000 AS QTA, COUNT( ID_MOV_F ) AS VIAGGI, SUM(Km)/1000 AS km FROM user_movimenti_fiscalizzati WHERE ID_RIF IN ( SELECT ID_RIF FROM user_schede_rifiuti WHERE ID_CER ='.$tmpCer['ID_CER'].' ) AND MONTHNAME(DTFORM) IN ('.$TRIMESTRE[$t]['months'].') ';
		$SQL.=$RULES;

		//echo $SQL."<hr>";

		echo "<h1>".date("H:i:s")." - RETRIVE DATA FOR COD_CER ".$tmpCer['COD_CER']." IN TRIMESTRE ".$TRIMESTRE[$t]['months']."</h1><br />\n";
		$result = mysqli_query($link,$SQL);
		echo "<h1>".date("H:i:s")." - DATA FOUND FOR COD_CER ".$tmpCer['COD_CER']." IN TRIMESTRE ".$TRIMESTRE[$t]['months']."</h1><br />\n";

		while($mese = mysqli_fetch_assoc($result)){

			$QTA	= round($mese['QTA'],2);
			$Viaggi	= round($mese['VIAGGI'],2);
			$km		= round($mese['km'],2);
			$CM		= round(($QTA/$Viaggi),2);						// qui la qta deve essere in t	
			
			// Media trasportata nei [10%] migliori conferimenti
			$LIMIT	= round($Viaggi/10); if($LIMIT==0) $LIMIT=1;
			$SQL='SELECT (SUM(QTA)/1000) AS QTAtop FROM (SELECT IF( PS_DESTINO >0, PS_DESTINO, pesoN ) AS QTA FROM `user_movimenti_fiscalizzati` WHERE ID_RIF IN (SELECT ID_RIF FROM user_schede_rifiuti WHERE ID_CER='.$tmpCer['ID_CER'].') AND MONTHNAME(DTFORM) IN ('.$TRIMESTRE[$t]['months'].')';
			$SQL.=$RULES;
			$SQL.='ORDER BY pesoN DESC LIMIT 0, '.$LIMIT.') AS subquery';
			//echo $SQL."<br><br>";
			
			echo "<h1>".date("H:i:s")." - CALCULATING INDEXES FOR COD_CER ".$tmpCer['COD_CER']." IN TRIMESTRE ".$TRIMESTRE[$t]['months']."</h1><br />\n";

			$result10 = mysqli_query($link,$SQL);
			while($mese10 = mysqli_fetch_assoc($result10)){
				$QTAtop	= round($mese10['QTAtop'],2);				// qui la qta deve essere in t	
				$CMax	= round(($QTAtop/$LIMIT),2);				// qui la qta deve essere in t	
				}
			$Index1 = round(($CM / $CMax),2);
			$Index2 = round(($QTA / $Viaggi),2);					// qui la qta deve essere in t			
			$Index3 = round(($QTA * 1000 / $km),2);					// qui la qta deve essere in kg

			echo "<h1>".date("H:i:s")." - INDEXES OK FOR COD_CER ".$tmpCer['COD_CER']." IN TRIMESTRE ".$TRIMESTRE[$t]['months']."</h1><br />\n";
			}

		
		if(!is_null($Index1)){
			$SQL	= "UPDATE sdg_index1_internazionali SET trim_".$TRIMESTRE[$t]['number']."=".$Index1." WHERE COD_CER='".$tmpCer['COD_CER']."';";
			//echo $SQL."<br>";
			mysqli_query($link,$SQL);
			}
		unset($Index1);
		
		if(!is_null($Index2)){
			$SQL	= "UPDATE sdg_index2_internazionali SET trim_".$TRIMESTRE[$t]['number']."=".$Index2." WHERE COD_CER='".$tmpCer['COD_CER']."';";
			//echo $SQL."<br>";
			mysqli_query($link,$SQL);
			}
		unset($Index2);
		
		if(!is_null($Index3)){
			$SQL	= "UPDATE sdg_index3_internazionali SET trim_".$TRIMESTRE[$t]['number']."=".$Index3." WHERE COD_CER='".$tmpCer['COD_CER']."';";
			//echo $SQL."<hr>";
			mysqli_query($link,$SQL);
			}
		unset($Index3);

		}
	}




mysqli_close($link);

?>