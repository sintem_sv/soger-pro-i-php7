<?php
session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__libs/SQLFunct.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__includes/COMMON_wakeSoger.php");

require_once("IndexCalculator.php");

# Index1: Indice medio di saturazione dei trasporti
# Index2: Indice medio di trasporto: tonnellate trasportate per viaggio
# Index3: Indice medio di trasporto: tonnellate trasportate per kilometro

# TABELLA (INDICE)
$TABLE	= 'sdg_index'.$_POST['INDICE'].'_nazionali';

# COLONNA (PERIODO)
if($_POST['mese']!='')		$COLUMN = $_POST['mese'];
if($_POST['trimestre']!='') $COLUMN = $_POST['trimestre'];
if($_POST['anno']!='')		$COLUMN = 'anno';


# RULES
$RULES = ' AND TIPO="S" ';
$RULES.= ' AND user_movimenti_fiscalizzati.produttore=1 AND user_movimenti_fiscalizzati.trasportatore=0 AND user_movimenti_fiscalizzati.intermediario=0 AND user_movimenti_fiscalizzati.destinatario=0 ';
$RULES.= ' AND ID_UIMP<>0 ';
$RULES.= ' AND ID_UIMT<>0 ';
$RULES.= ' AND ID_UIMD<>0 ';
$RULES.= ' AND Transfrontaliero=0 ';

# TIME
if($COLUMN!='anno'){
	if(!strstr($COLUMN, "trim_")){
		$TIME.=" AND MONTHNAME(DTFORM)='".$COLUMN."' ";
		}
	else{
		switch($COLUMN){
			case "trim_I":
				$TIME.=" AND MONTHNAME(DTFORM) IN ('January', 'February', 'March') ";
				break;
			case "trim_II":
				$TIME.=" AND MONTHNAME(DTFORM) IN ('April', 'May', 'June') ";
				break;
			case "trim_III":
				$TIME.=" AND MONTHNAME(DTFORM) IN ('July', 'August', 'September') ";
				break;
			case "trim_IV":
				$TIME.=" AND MONTHNAME(DTFORM) IN ('October', 'November', 'December') ";
				break;
			}
		}
	}
$TIME.=" AND YEAR(DTFORM) = '".substr($_SESSION["DbInUse"], -4)."' ";



for($r=0;$r<count($_POST['CER']);$r++){

	# ID CER
	$CER	= $_POST['CER'][$r];

	# CODICE CER
	$CERCODE= $_POST['CERCODE'][$r];

	# ID RIFIUTO
	$RIF	= $_POST['RIF'][$r];


	# INDICE DEL MIO IMPIANTO
	switch($_POST['INDICE']){
		case '1':
			$MyINDEX = getISindex($CER, $RIF, $RULES, $TIME, $FEDIT);
			break;
		case '2':
			$MyINDEX = getITindex($CER, $RIF, $RULES, $TIME, $FEDIT);
			break;
		case '3':
			$MyINDEX = getIKindex($CER, $RIF, $RULES, $TIME, $FEDIT);
			break;
		}


	# INDICE DI RIFERIMENTO
	$SQL="SELECT ".$COLUMN." FROM ".$TABLE." WHERE ID_CER='".$CER."'";
	$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
	$NazINDEX = $FEDIT->DbRecordSet[0][$COLUMN];


	# SCARTO % TRA INDICI - FATTORE DI SCOSTAMENTO
	if($MyINDEX>0 AND $NazINDEX>0)
		$FS = round( ( ( $MyINDEX - $NazINDEX ) / $NazINDEX * 100), 2);
	else
		$FS = 'n.d.';


	# KG A DESTINO
	$SQL ="SELECT ROUND( SUM( IF( PS_DESTINO >0, PS_DESTINO, pesoN ) ) ) AS KG FROM user_movimenti_fiscalizzati ";
	$SQL.="WHERE user_movimenti_fiscalizzati.ID_RIF=".$RIF." ";
	$SQL.=$RULES;
	$SQL.=$TIME;
	$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
	$KG = $FEDIT->DbRecordSet[0]['KG'];

	# KM MEDIO
	$SQL ="SELECT SUM(Km) AS KM, COUNT(ID_MOV_F) AS VIAGGI FROM user_movimenti_fiscalizzati ";
	$SQL.="WHERE user_movimenti_fiscalizzati.ID_RIF=".$RIF." ";
	$SQL.=$RULES;
	$SQL.=$TIME;
	$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
	$KM = $FEDIT->DbRecordSet[0]['KM'];

	# prepare json string to return
	$Indexes[$r]["IM"]	= $MyINDEX;
	$Indexes[$r]["IR"]	= $NazINDEX;
	$Indexes[$r]["SC"]	= $FS;
	$Indexes[$r]["KG"]	= number_format($KG, 0, ',', '.');
	$Indexes[$r]["KM"]	= number_format($KM, 0, ',', '.');


	### SALVO GLI INDICI IMPIANTO SUL DB!
	$MyTABLE	= 'sdg_index'.$_POST['INDICE'];

	// Verifico se � un INSERT o un UPDATE
	$SQL ="SELECT * FROM ".$MyTABLE." WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ID_RIF=".$RIF.";";
	$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);
	if($FEDIT->DbRecsNum>0){
		$SQL ="UPDATE ".$MyTABLE." SET ".$COLUMN."=".$MyINDEX." WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ID_RIF=".$RIF.";";
		}
	else{
		$SQL ="INSERT INTO ".$MyTABLE." (ID_IMP, ID_RIF, ".$COLUMN.") VALUES ('".$SOGER->UserData['core_usersID_IMP']."', ".$RIF.", ".$MyINDEX.");";
		}
	$FEDIT->SDBWrite($SQL,true,false);
	}

echo json_encode($Indexes);

require_once("../__includes/COMMON_sleepForgEdit.php");
require_once("../__includes/COMMON_sleepSoger.php");

?>