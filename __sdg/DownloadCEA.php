<?php

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/fpdf.php");
require_once("../__scripts/STATS_funct.php");
require_once("../__libs/SQLFunct.php");

if($_POST['trimestre']!=''){
	$TIME = substr($_POST['trimestre'], 5)." trimestre ".substr($_SESSION["DbInUse"], -4);
	}

if($_POST['anno']!='')
	$TIME = "Anno ".substr($_SESSION["DbInUse"], -4);

$DcName			= $_POST['FileName'];
$DESCRIPTION	= "Fattori di scostamento e classi di efficienza ambientale (CEA)";
$intestatario	= strtolower($SOGER->UserData["core_impiantidescription"]) . " (" . $SOGER->UserData["workmode"] . ")";


switch($_POST['DownloadType']){
	case "PDF":
		$orientation	="L";
		$um				="mm";
		$Format			= array(210,297);
		$ZeroMargin		= true;
		$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin,$DcName);

		$Xpos=20;
		$Ypos=10;

		# TITLE
		$FEDIT->FGE_PdfBuffer->SetXY(0,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',14);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,15,$DcName,0,"C"); 


		# DESCRIZIONE
		$Xpos=20;
		$Ypos+=15;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Descrizione:",0,"L");
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY(40,25);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$DESCRIPTION,0,"L");


		# INTESTATARIO
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Intestatario:",0,"L");
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY(40,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$intestatario,0,"L");


		# PERIODO
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Periodo di riferimento:",0,"L");
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY(55,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$TIME,0,"L"); 

		
		# PRIMA RIGA TABELLA
		$Ypos+=15;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetFillColor(108,198,83);

		$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,8,"CER","TLBR","C",1);

		$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(145,8,"Rifiuto","TLBR","C",1);

		$FEDIT->FGE_PdfBuffer->SetXY(170,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,8,"FSS","TLBR","C",1);

		$FEDIT->FGE_PdfBuffer->SetXY(195,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,8,"FST","TLBR","C",1);

		$FEDIT->FGE_PdfBuffer->SetXY(220,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,8,"FSK","TLBR","C",1);

		$FEDIT->FGE_PdfBuffer->SetXY(245,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(25,8,"FSM","TLBR","C",1);

		$FEDIT->FGE_PdfBuffer->SetXY(270,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,8,"CEA","TLBR","C",1);

		//$FEDIT->FGE_PdfBuffer->SetXY(255,$Ypos);
		//$FEDIT->FGE_PdfBuffer->MultiCell(30,8,"KG (a destino)","TLBR","C",1);
			
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);

		# CICLO DATI
		for($r=0;$r<(int)$_POST['rifiutiCounter'];$r++){
			
			$Ypos+=8;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

			$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,8,$_POST['ID_CERCODE_'.$r],"TLBR","C");

			$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(145,8,$_POST['IH_DESC_'.$r],"TLBR","L");

			$FEDIT->FGE_PdfBuffer->SetXY(170,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(25,8,$_POST['IH_FSS_'.$r],"TLBR","C");

			$FEDIT->FGE_PdfBuffer->SetXY(195,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(25,8,$_POST['IH_FST_'.$r],"TLBR","C");

			$FEDIT->FGE_PdfBuffer->SetXY(220,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(25,8,$_POST['IH_FSK_'.$r],"TLBR","C");

			$FEDIT->FGE_PdfBuffer->SetXY(245,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(25,8,$_POST['IH_FSM_'.$r],"TLBR","C");

			$FEDIT->FGE_PdfBuffer->SetXY(270,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,8,$_POST['IH_CEA_'.$r],"TLBR","C");

			//$FEDIT->FGE_PdfBuffer->SetXY(255,$Ypos);
			//$FEDIT->FGE_PdfBuffer->MultiCell(30,8,$_POST['IH_KG_'.$r],"TLBR","R");
			}



		# OUTPUT FILE
		$FEDIT->FGE_PdfBuffer->Output($DcName.".pdf","D");
		
		break;

	case "CSV":
		# TITLE
		$CSVbuf  = AddCSVcolumn($DcName);
		$CSVbuf .= AddCSVRow();
		$CSVbuf .= AddCSVRow();

		# DESCRIZIONE
		$CSVbuf .= AddCSVcolumn("Descrizione: ".$DESCRIPTION);	
		$CSVbuf .= AddCSVRow();

		# INTESTATARIO
		$CSVbuf .= AddCSVcolumn("Intestatario: ".$intestatario);
		$CSVbuf .= AddCSVRow();

		# PERIODO
		$CSVbuf .= AddCSVcolumn("Periodo di riferimento: ".$TIME);
		$CSVbuf .= AddCSVRow();

		# PRIMA RIGA TABELLA
		$CSVbuf .= AddCSVcolumn("CER");
		$CSVbuf .= AddCSVcolumn("Rifiuto");
		$CSVbuf .= AddCSVcolumn("FSS");			
		$CSVbuf .= AddCSVcolumn("FST");			
		$CSVbuf .= AddCSVcolumn("FSK");			
		$CSVbuf .= AddCSVcolumn("FSM");			
		$CSVbuf .= AddCSVcolumn("CEA");			
		//$CSVbuf .= AddCSVcolumn("KG (a destino)");			
		$CSVbuf .= AddCSVRow();

		# CICLO RIFIUTIO
		for($r=0;$r<(int)$_POST['rifiutiCounter'];$r++){
			$CSVbuf .= AddCSVcolumn($_POST['ID_CERCODE_'.$r]);
			$CSVbuf .= AddCSVcolumn($_POST['IH_DESC_'.$r]);
			$CSVbuf .= AddCSVcolumn($_POST['IH_FSS_'.$r]);
			$CSVbuf .= AddCSVcolumn($_POST['IH_FST_'.$r]);
			$CSVbuf .= AddCSVcolumn($_POST['IH_FSK_'.$r]);
			$CSVbuf .= AddCSVcolumn($_POST['IH_FSM_'.$r]);
			$CSVbuf .= AddCSVcolumn($_POST['IH_CEA_'.$r]);
			//$CSVbuf .= AddCSVcolumn($_POST['IH_KG_'.$r]);
			$CSVbuf .= AddCSVRow();		
			}

		CSVOut($CSVbuf,$DcName);	
		break;
	}	

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>