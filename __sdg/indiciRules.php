<?php

$RULES = ' AND TIPO="S" ';
$RULES.= ' AND user_movimenti_fiscalizzati.produttore=1 AND user_movimenti_fiscalizzati.trasportatore=0 AND user_movimenti_fiscalizzati.intermediario=0 AND user_movimenti_fiscalizzati.destinatario=0 ';
$RULES.= ' AND ID_UIMP<>0 ';
$RULES.= ' AND ID_UIMT<>0 ';
$RULES.= ' AND ID_UIMD<>0 ';
$RULES.= ' AND NMOV<>9999999 ';
$RULES.= ' AND PerRiclassificazione=0 ';
$RULES.= ' AND SenzaTrasporto=0 ';
$RULES.= ' AND TIPO_S_INTERNO=0 ';


function dateDiff($interval,$dateTimeBegin,$dateTimeEnd) {
//{{{
	$dateTimeBegin=strtotime($dateTimeBegin);
	$dateTimeEnd=strtotime($dateTimeEnd);

	if($dateTimeEnd === -1 | $dateTimeBegin===-1) {
		return("either start or end date is invalid");
	}
	$dif=$dateTimeEnd - $dateTimeBegin;
	switch($interval) {
		case "d"://days
		return(floor($dif/86400)); //86400s=1d
		case "ww"://Week
		return(floor($dif/604800)); //604800s=1week=1semana
		case "m": //similar result "m" dateDiff Microsoft
		$monthBegin=(date("Y",$dateTimeBegin)*12)+
		date("n",$dateTimeBegin);
		$monthEnd=(date("Y",$dateTimeEnd)*12)+
		date("n",$dateTimeEnd);
		$monthDiff=$monthEnd-$monthBegin;
		return($monthDiff);
		case "yyyy": //similar result "yyyy" dateDiff Microsoft
		return(date("Y",$dateTimeEnd) - date("Y",$dateTimeBegin));
		default:
		return(floor($dif/86400)); //86400s=1d
	} //}}}
}

?>