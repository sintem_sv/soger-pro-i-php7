<?php

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/fpdf.php");
require_once("../__scripts/STATS_funct.php");
require_once("../__libs/SQLFunct.php");

//die(var_dump($_POST));

if($_POST['mese']!=''){
	switch($_POST['mese']){
		case 'January':
			$mese='Gennaio';
			break;
		case 'February':
			$mese='Febbraio';
			break;
		case 'March':
			$mese='Marzo';
			break;
		case 'April':
			$mese='Aprile';
			break;
		case 'May':
			$mese='Maggio';
			break;
		case 'June':
			$mese='Giugno';
			break;
		case 'July':
			$mese='Luglio';
			break;
		case 'August':
			$mese='Agosto';
			break;
		case 'September':
			$mese='Settembre';
			break;
		case 'October':
			$mese='Ottobre';
			break;
		case 'November':
			$mese='Novembre';
			break;
		case 'December':
			$mese='Dicembre';
			break;
		}
	$TIME = $mese." ".substr($_SESSION["DbInUse"], -4);
	}
	

if($_POST['trimestre']!='')
	$TIME = substr($_POST['trimestre'], 5)." trimestre ".substr($_SESSION["DbInUse"], -4);

if($_POST['anno']!='')
	$TIME = "Anno ".substr($_SESSION["DbInUse"], -4);

$DcName			= $_POST['FileName'];
$DESCRIPTION	= "Indice di gestione aziendale e indice di gestione nazionale a confronto";
$intestatario	= strtolower($SOGER->UserData["core_impiantidescription"]) . " (" . $SOGER->UserData["workmode"] . ")";
$SQL			= "SELECT description AS INDICE FROM lov_sdg_index WHERE id=".$_POST['sdg'].";";
$FEDIT->SDBRead($SQL,"DbRecordSet");
$INDICE			= $FEDIT->DbRecordSet[0]['INDICE'];


switch($_POST['DownloadType']){
	case "PDF":
		$orientation	="L";
		$um				="mm";
		$Format			= array(210,297);
		$ZeroMargin		= true;
		$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin,$DcName);

		$Xpos=20;
		$Ypos=10;

		# TITLE
		$FEDIT->FGE_PdfBuffer->SetXY(0,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',14);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,15,$DcName,0,"C"); 


		# DESCRIZIONE
		$Xpos=20;
		$Ypos+=15;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Descrizione:",0,"L");
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY(40,25);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$DESCRIPTION,0,"L");


		# INTESTATARIO
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Intestatario:",0,"L");
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY(40,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$intestatario,0,"L");


		# PERIODO
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Periodo di riferimento:",0,"L");
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY(55,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$TIME,0,"L"); 

		
		# INDICE SELEZIONATO
		$Ypos+=7;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY(20,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,"Indice di gestione:",0,"L");
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY(55,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(0,4,$INDICE,0,"L"); 

		
		# PRIMA RIGA TABELLA
		$Ypos+=15;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',8);
		$FEDIT->FGE_PdfBuffer->SetFillColor(108,198,83);

		$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,8,"CER","TLBR","C",1);

		$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(155,8,"Rifiuto","TLBR","C",1);

		$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,8,"Indice azienda","TLBR","C",1);

		$FEDIT->FGE_PdfBuffer->SetXY(210,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(30,8,"Indice nazionale","TLBR","C",1);

		$FEDIT->FGE_PdfBuffer->SetXY(240,$Ypos);
		$FEDIT->FGE_PdfBuffer->MultiCell(15,8,"Scarto %","TLBR","C",1);
			
		$FEDIT->FGE_PdfBuffer->SetXY(255,$Ypos);
		if($_POST['sdg']<3)
			$FEDIT->FGE_PdfBuffer->MultiCell(30,8,"kg (a destino)","TLBR","C",1);
		else
			$FEDIT->FGE_PdfBuffer->MultiCell(30,8,"km medi","TLBR","C",1);
			
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',8);

		# CICLO DATI
		for($r=0;$r<(int)$_POST['rifiutiCounter'];$r++){
			
			$Ypos+=8;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

			$FEDIT->FGE_PdfBuffer->SetXY(10,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,8,$_POST['ID_CERCODE_'.$r],"TLBR","C");

			$FEDIT->FGE_PdfBuffer->SetXY(25,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(155,8,$_POST['IH_DESC_'.$r],"TLBR","L");

			$FEDIT->FGE_PdfBuffer->SetXY(180,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(30,8,$_POST['IH_IM_'.$r],"TLBR","C");

			$FEDIT->FGE_PdfBuffer->SetXY(210,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(30,8,$_POST['IH_IR_'.$r],"TLBR","C");

			$FEDIT->FGE_PdfBuffer->SetXY(240,$Ypos);
			$FEDIT->FGE_PdfBuffer->MultiCell(15,8,$_POST['IH_SC_'.$r],"TLBR","C");

			$FEDIT->FGE_PdfBuffer->SetXY(255,$Ypos);
			if($_POST['sdg']<3)
				$FEDIT->FGE_PdfBuffer->MultiCell(30,8,$_POST['IH_KG_'.$r],"TLBR","R");
			else
				$FEDIT->FGE_PdfBuffer->MultiCell(30,8,$_POST['IH_KM_'.$r],"TLBR","R");
			}



		# OUTPUT FILE
		$FEDIT->FGE_PdfBuffer->Output($DcName.".pdf","D");
		
		break;

	case "CSV":
		# TITLE
		$CSVbuf  = AddCSVcolumn($DcName);
		$CSVbuf .= AddCSVRow();
		$CSVbuf .= AddCSVRow();

		# DESCRIZIONE
		$CSVbuf .= AddCSVcolumn("Descrizione: ".$DESCRIPTION);	
		$CSVbuf .= AddCSVRow();

		# INTESTATARIO
		$CSVbuf .= AddCSVcolumn("Intestatario: ".$intestatario);
		$CSVbuf .= AddCSVRow();

		# PERIODO
		$CSVbuf .= AddCSVcolumn("Periodo di riferimento: ".$TIME);
		$CSVbuf .= AddCSVRow();

		# INDICE SELEZIONATO
		$CSVbuf .= AddCSVcolumn("Indice di gestione: ".$INDICE);
		$CSVbuf .= AddCSVRow();

		# PRIMA RIGA TABELLA
		$CSVbuf .= AddCSVcolumn("CER");
		$CSVbuf .= AddCSVcolumn("Rifiuto");
		$CSVbuf .= AddCSVcolumn("Indice azienda");			
		$CSVbuf .= AddCSVcolumn("Indice nazionale");			
		$CSVbuf .= AddCSVcolumn("Scarto %");
		if($_POST['sdg']<3)
			$CSVbuf .= AddCSVcolumn("kg (a destino)");			
		else
			$CSVbuf .= AddCSVcolumn("km medi");			
		$CSVbuf .= AddCSVRow();

		# CICLO RIFIUTIO
		for($r=0;$r<(int)$_POST['rifiutiCounter'];$r++){
			$CSVbuf .= AddCSVcolumn($_POST['ID_CERCODE_'.$r]);
			$CSVbuf .= AddCSVcolumn($_POST['IH_DESC_'.$r]);
			$CSVbuf .= AddCSVcolumn($_POST['IH_IM_'.$r]);
			$CSVbuf .= AddCSVcolumn($_POST['IH_IR_'.$r]);
			$CSVbuf .= AddCSVcolumn($_POST['IH_SC_'.$r]);
			if($_POST['sdg']<3)
				$CSVbuf .= AddCSVcolumn($_POST['IH_KG_'.$r]);
			else
				$CSVbuf .= AddCSVcolumn($_POST['IH_KM_'.$r]);
			$CSVbuf .= AddCSVRow();		
			}

		CSVOut($CSVbuf,$DcName);	
		break;
	}	

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>