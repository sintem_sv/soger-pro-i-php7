<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

include("indiciRules.php");

$MesePrecedente1		= date('n',mktime(0,0,0,date('n')-1,date('d'),date('Y')));
$MesePrecedente2		= date('n',mktime(0,0,0,date('n')-2,date('d'),date('Y')));
$MesePrecedente3		= date('n',mktime(0,0,0,date('n')-3,date('d'),date('Y')));

$MesePrecedenteName1	= date('F',mktime(0,0,0,date('n')-1,date('d'),date('Y')));
$MesePrecedenteName2	= date('F',mktime(0,0,0,date('n')-2,date('d'),date('Y')));
$MesePrecedenteName3	= date('F',mktime(0,0,0,date('n')-3,date('d'),date('Y')));

$AnnoMesePrecedente1	= date('Y');
if( (int) $MesePrecedente2 > (int) $MesePrecedente1 )
	$AnnoMesePrecedente2 = date('Y') - 1;
else 
	$AnnoMesePrecedente2 = date('Y');
if( (int) $MesePrecedente3 > (int) $MesePrecedente1 )
	$AnnoMesePrecedente3 = date('Y') - 1;
else 
	$AnnoMesePrecedente3 = date('Y');

$Mesi = Array();

$Mesi[0]['numero']	= $MesePrecedente3;
$Mesi[0]['nome']	= $MesePrecedenteName3;
$Mesi[0]['anno']	= $AnnoMesePrecedente3;

$Mesi[1]['numero']	= $MesePrecedente2;
$Mesi[1]['nome']	= $MesePrecedenteName2;
$Mesi[1]['anno']	= $AnnoMesePrecedente2;

$Mesi[2]['numero']	= $MesePrecedente1;
$Mesi[2]['nome']	= $MesePrecedenteName1;
$Mesi[2]['anno']	= $AnnoMesePrecedente1;

unset($Mesi);

/*

$Mesi[0]['numero']	= '1';
$Mesi[0]['nome']	= 'January';
$Mesi[0]['anno']	= '2013';

$Mesi[1]['numero']	= '2';
$Mesi[1]['nome']	= 'February';
$Mesi[1]['anno']	= '2013';

*/

$Mesi[0]['numero']	= '3';
$Mesi[0]['nome']	= 'March';
$Mesi[0]['anno']	= '2013';

$Mesi[1]['numero']	= '4';
$Mesi[1]['nome']	= 'April';
$Mesi[1]['anno']	= '2013';

/*

$Mesi[0]['numero']	= '5';
$Mesi[0]['nome']	= 'May';
$Mesi[0]['anno']	= '2013';

$Mesi[1]['numero']	= '6';
$Mesi[1]['nome']	= 'June';
$Mesi[1]['anno']	= '2013';

$Mesi[0]['numero']	= '7';
$Mesi[0]['nome']	= 'July';
$Mesi[0]['anno']	= '2013';

$Mesi[1]['numero']	= '8';
$Mesi[1]['nome']	= 'August';
$Mesi[1]['anno']	= '2013';

$Mesi[0]['numero']	= '9';
$Mesi[0]['nome']	= 'September';
$Mesi[0]['anno']	= '2013';

$Mesi[1]['numero']	= '10';
$Mesi[1]['nome']	= 'October';
$Mesi[1]['anno']	= '2013';

$Mesi[0]['numero']	= '11';
$Mesi[0]['nome']	= 'November';
$Mesi[0]['anno']	= '2013';

$Mesi[1]['numero']	= '12';
$Mesi[1]['nome']	= 'December';
$Mesi[1]['anno']	= '2013';

*/

$link = mysqli_connect('localhost', 'root', 'Ius87P78JskU');

if (!$link) {
    echo "Unable to connect to DB: " . mysqli_connect_error();
    exit;
}

# Index1: Indice medio di saturazione dei trasporti
# Index2: Indice medio di trasporto: tonnellate trasportate per viaggio
# Index3: Indice medio di trasporto: tonnellate trasportate per kilometro
# Index4: Indice medio di permanenza in sito

for($m=0;$m<count($Mesi);$m++){
	
	# selezione db
	if (!mysqli_select_db($link, "soger".$Mesi[$m]['anno'])){
		echo "Unable to select mydbname: " . mysqli_error($link);
		exit;
		}
	if (!mysqli_select_db($link, $FEDIT->DbServerData["db"])) {
		echo "Unable to select database ".$FEDIT->DbServerData["db"].": " . mysqli_error($link);
		exit;
	}

	$SQL='SELECT * FROM lov_cer;';
	$cer = mysqli_query($link,$SQL);
	while($tmpCer = mysqli_fetch_assoc($cer)){

		# Index1 - retrive data
		$SQL='SELECT SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN ) )/1000 AS QTA, COUNT( ID_MOV_F ) AS VIAGGI, SUM(Km)/1000 AS km FROM user_movimenti_fiscalizzati WHERE ID_RIF IN ( SELECT ID_RIF FROM user_schede_rifiuti WHERE ID_CER ='.$tmpCer['ID_CER'].' )';
		$SQL.=$RULES;
		$SQL.=' AND MONTHNAME(DTFORM)="'.$Mesi[$m]['nome'].'" ';
		$SQL.=' AND YEAR(DTFORM)="'.$Mesi[$m]['anno'].'" ';
		$SQL.='GROUP BY MONTH(DTFORM)';

		echo "<h1>".date("H:i:s")." - RETRIVE DATA FOR COD_CER ".$tmpCer['COD_CER']." IN ".$Mesi[$m]['nome']."</h1><br />\n";
		$result = mysqli_query($link,$SQL);
		echo "<h1>".date("H:i:s")." -DATA FOUND FOR COD_CER ".$tmpCer['COD_CER']." IN ".$Mesi[$m]['nome']."</h1><br />\n";

		while($mese = mysqli_fetch_assoc($result)){

			$QTA[$Mesi[$m]['nome']]		= round($mese['QTA'],2);
			$Viaggi[$Mesi[$m]['nome']]	= round($mese['VIAGGI'],2);
			$km[$Mesi[$m]['nome']]		= round($mese['km'],2);
			$CM[$Mesi[$m]['nome']]		= round(($QTA[$Mesi[$m]['nome']]/$Viaggi[$Mesi[$m]['nome']]),2);		// qui la qta deve essere in t	
			
			// Media trasportata nei [10%] migliori conferimenti
			$LIMIT	= round($Viaggi[$Mesi[$m]['nome']]/10); if($LIMIT==0) $LIMIT=1;
			$SQL='SELECT (SUM(QTA)/1000) AS QTAtop FROM (SELECT IF( PS_DESTINO >0, PS_DESTINO, pesoN ) AS QTA, MONTHNAME(DTFORM) AS MESE FROM `user_movimenti_fiscalizzati` WHERE ID_RIF IN (SELECT ID_RIF FROM user_schede_rifiuti WHERE ID_CER='.$tmpCer['ID_CER'].') AND MONTHNAME(DTFORM)="'.$Mesi[$m]['nome'].'" ';
			$SQL.=$RULES;
			$SQL.='ORDER BY pesoN DESC LIMIT 0, '.$LIMIT.') AS subquery';

			echo "<h1>".date("H:i:s")." - CALCULATING INDEXES FOR COD_CER ".$tmpCer['COD_CER']." IN ".$Mesi[$m]['nome']."</h1><br />\n";
			
			$result10 = mysqli_query($link,$SQL);
			while($mese10 = mysqli_fetch_assoc($result10)){
				$QTAtop[$Mesi[$m]['nome']]	= round($mese10['QTAtop'],2);								// qui la qta deve essere in t	
				$CMax[$Mesi[$m]['nome']]	= round(($QTAtop[$Mesi[$m]['nome']]/$LIMIT),2);					// qui la qta deve essere in t	
				}
			$Index1[$Mesi[$m]['nome']] = round(($CM[$Mesi[$m]['nome']] / $CMax[$Mesi[$m]['nome']]),2);
			$Index2[$Mesi[$m]['nome']] = round(($QTA[$Mesi[$m]['nome']] / $Viaggi[$Mesi[$m]['nome']]),2);		// qui la qta deve essere in t	
			$Index3[$Mesi[$m]['nome']] = round(($QTA[$Mesi[$m]['nome']] * 1000 / $km[$Mesi[$m]['nome']]),2);	// qui la qta deve essere in kg

			echo "<h1>".date("H:i:s")." - INDEXES OK FOR COD_CER ".$tmpCer['COD_CER']." IN ".$Mesi[$m]['nome']."</h1><br />\n";

			if(!is_null($Index1)){
				$SQL	= "UPDATE sdg_index1_internazionali SET ".$Mesi[$m]['nome']."=NULL WHERE ID_CER=".$tmpCer['ID_CER'];
				mysqli_query($link,$SQL);
				$SQL	= "UPDATE sdg_index1_internazionali SET ".$Mesi[$m]['nome']."=".$Index1[$Mesi[$m]['nome']]." WHERE ID_CER=".$tmpCer['ID_CER'];
				mysqli_query($link,$SQL);
				}
			unset($Index1);
		
			if(!is_null($Index2)){
				$SQL	= "UPDATE sdg_index2_internazionali SET ".$Mesi[$m]['nome']."=NULL WHERE ID_CER=".$tmpCer['ID_CER'];
				mysqli_query($link,$SQL);
				$SQL	= "UPDATE sdg_index2_internazionali SET ".$Mesi[$m]['nome']."=".$Index2[$Mesi[$m]['nome']]." WHERE ID_CER=".$tmpCer['ID_CER'];
				mysqli_query($link,$SQL);
				}
			unset($Index2);
	
			if(!is_null($Index3)){
				$SQL	= "UPDATE sdg_index3_internazionali SET ".$Mesi[$m]['nome']."=NULL WHERE ID_CER=".$tmpCer['ID_CER'];
				mysqli_query($link,$SQL);
				$SQL	= "UPDATE sdg_index3_internazionali SET ".$Mesi[$m]['nome']."=".$Index3[$Mesi[$m]['nome']]." WHERE ID_CER=".$tmpCer['ID_CER'];
				mysqli_query($link,$SQL);
				}
			unset($Index3);
			//die();
			}
		}
	}
	
	mysqli_close($link);

?>