<?php

/******* SETUP ********/
$YEAR = null; // if not set, current year is considered
include("indiciRules.php");
/*******  END  ********/

$link = mysqli_connect('localhost', 'root', 'Ius87P78JskU');

if (!$link) {
    echo "Unable to connect to DB: " . mysqli_connect_error();
    exit;
}

if (!mysqli_select_db($link, $YEAR?:date('Y'))) {
    echo "Unable to select database ".$FEDIT->DbServerData["db"].": " . mysqli_error($link);
    exit;
}


# Index1: Indice medio di saturazione dei trasporti
# Index2: Indice medio di trasporto: tonnellate trasportate per viaggio
# Index3: Indice medio di trasporto: tonnellate trasportate per kilometro
# Index4: Indice medio di permanenza in sito

//$DATE=' AND DTFORM>=\'2011-02-01\' AND DTFORM<\'2011-01-01\' ';


$SQL='SELECT * FROM lov_cer WHERE ID_CER IN (SELECT ID_CER FROM sdg_index1_internazionali WHERE anno IS NULL);';

$cer = mysqli_query($link,$SQL);
while($tmpCer = mysqli_fetch_assoc($cer)){

	echo "<h1>".$tmpCer['COD_CER']."</h1>";

	# Index1 - retrive data

	// Media trasportata del CER
	$SQL='SELECT SUM(IF( PS_DESTINO >0, PS_DESTINO, pesoN ) )/1000 AS QTA, COUNT( ID_MOV_F ) AS VIAGGI, SUM(Km)/1000 AS km FROM user_movimenti_fiscalizzati WHERE ID_RIF IN ( SELECT ID_RIF FROM user_schede_rifiuti WHERE ID_CER ='.$tmpCer['ID_CER'].' ) AND MONTHNAME(DTFORM) IN ("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December") ';
	$SQL.=$RULES;
	$result = mysqli_query($link,$SQL);
	while($mese = mysqli_fetch_assoc($result)){

		$QTA	= round($mese['QTA'],2);
		$Viaggi	= round($mese['VIAGGI'],2);
		$km		= round($mese['km'],2);
		$CM		= round(($QTA/$Viaggi),2);					// qui la qta deve essere in t	
		
		// Media trasportata nei [10%] migliori conferimenti
		$LIMIT	= round($Viaggi/10); if($LIMIT==0) $LIMIT=1;
		$SQL='SELECT (SUM(QTA)/1000) AS QTAtop FROM (SELECT IF( PS_DESTINO >0, PS_DESTINO, pesoN ) AS QTA FROM `user_movimenti_fiscalizzati` WHERE ID_RIF IN (SELECT ID_RIF FROM user_schede_rifiuti WHERE ID_CER='.$tmpCer['ID_CER'].') AND MONTHNAME(DTFORM) IN ("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")';
		$SQL.=$RULES;
		$SQL.='ORDER BY pesoN DESC LIMIT 0, '.$LIMIT.') AS subquery';
		//echo $SQL."<br><br>";
		$result10 = mysqli_query($link,$SQL);
		while($mese10 = mysqli_fetch_assoc($result10)){
			$QTAtop	= round($mese10['QTAtop'],2);			// qui la qta deve essere in t	
			$CMax	= round(($QTAtop/$LIMIT),2);			// qui la qta deve essere in t	
			}
		$Index1 = round(($CM / $CMax),2);
		$Index2 = round(($QTA / $Viaggi),2);				// qui la qta deve essere in t			
		$Index3 = round(($QTA * 1000 / $km),2);				// qui la qta deve essere in kg
		}

	
	if(!is_null($Index1)){
		$SQL	= "UPDATE sdg_index1_internazionali SET anno=".$Index1." WHERE COD_CER='".$tmpCer['COD_CER']."';";
		//echo $SQL."<br>";
		mysqli_query($link,$SQL);
		}
	unset($Index1);
	
	if(!is_null($Index2)){
		$SQL	= "UPDATE sdg_index2_internazionali SET anno=".$Index2." WHERE COD_CER='".$tmpCer['COD_CER']."';";
		//echo $SQL."<br>";
		mysqli_query($link,$SQL);
		}
	unset($Index2);
	
	if(!is_null($Index3)){
		$SQL	= "UPDATE sdg_index3_internazionali SET anno=".$Index3." WHERE COD_CER='".$tmpCer['COD_CER']."';";
		//echo $SQL."<hr>";
		mysqli_query($link,$SQL);
		}
	unset($Index3);

	}


mysqli_close($link);

?>