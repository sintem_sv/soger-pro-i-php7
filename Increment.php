<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
 <HEAD>
  <TITLE> New Document </TITLE>
  <META NAME="Generator" CONTENT="EditPlus">
  <META NAME="Author" CONTENT="">
  <META NAME="Keywords" CONTENT="">
  <META NAME="Description" CONTENT="">
 </HEAD>

 <BODY>

<?php

/*
manca user_autorizzazioni_pro
SELECT 
max(user_autisti.ID_AUTST) AS user_autisti, 
max(user_automezzi.ID_AUTO) AS user_automezzi, 
max(user_autorizzazioni_dest.ID_AUTHD) AS user_autorizzazioni_dest,
max(user_autorizzazioni_interm.ID_AUTHI) AS user_autorizzazioni_interm,
max(user_autorizzazioni_trasp.ID_AUTHT) AS user_autorizzazioni_trasp,
max(user_aziende_destinatari.ID_AZD) AS user_aziende_destinatari,
max(user_aziende_intermediari.ID_AZI) AS user_aziende_intermediari,
max(user_aziende_produttori.ID_AZP) AS user_aziende_produttori,
max(user_aziende_trasportatori.ID_AZT) AS user_aziende_trasportatori,
max(user_impianti_destinatari.ID_UIMD) AS user_impianti_destinatari,
max(user_impianti_intermediari.ID_UIMI) AS user_impianti_intermediari,
max(user_impianti_produttori.ID_UIMP) AS user_impianti_produttori,
max(user_impianti_trasportatori.ID_UIMT) AS user_impianti_trasportatori,
max(user_rimorchi.ID_RMK) AS user_rimorchi,
max(user_schede_rifiuti.ID_RIF) AS user_schede_rifiuti,
max(user_schede_rifiuti_deposito.ID_DEP) AS user_schede_rifiuti_deposito,
max(user_schede_rifiuti_etsym.ID_R_SYM) AS user_schede_rifiuti_etsym,
max(user_schede_sicurezza.ID_SK) AS user_schede_sicurezza
FROM user_autisti, user_automezzi, user_autorizzazioni_dest, user_autorizzazioni_interm, user_autorizzazioni_trasp, user_aziende_destinatari, user_aziende_intermediari, user_aziende_produttori, user_aziende_trasportatori, user_impianti_destinatari, user_impianti_intermediari, user_impianti_produttori, user_impianti_trasportatori, user_rimorchi, user_schede_rifiuti, user_schede_rifiuti_deposito, user_schede_rifiuti_etsym, user_schede_sicurezza
*/

if(isset($_POST['conferma'])){
	$query ="SET FOREIGN_KEY_CHECKS=0;";
	$query.="UPDATE user_autisti SET ID_AUTST=ID_AUTST+".$_POST['ID_AUTST'].", ID_UIMT=ID_UIMT+".$_POST['ID_UIMT'].";";
	$query.="UPDATE user_automezzi SET ID_AUTO=ID_AUTO+".$_POST['ID_AUTO'].", ID_AUTHT=ID_AUTHT+".$_POST['ID_AUTHT'].";";
	$query.="UPDATE user_rimorchi SET ID_RMK=ID_RMK+".$_POST['ID_RMK'].", ID_AUTHT=ID_AUTHT+".$_POST['ID_AUTHT'].";";
	$query.="UPDATE user_schede_rifiuti SET ID_RIF=ID_RIF+".$_POST['ID_RIF'].";";
	$query.="UPDATE user_schede_rifiuti_deposito SET ID_RIF=ID_RIF+".$_POST['ID_RIF'].";";
	$query.="UPDATE user_schede_rifiuti_etsym SET ID_RIF=ID_RIF+".$_POST['ID_RIF'].";";
	$query.="UPDATE user_schede_rifiuti_macchinari SET ID_RIF=ID_RIF+".$_POST['ID_RIF'].";";
	$query.="UPDATE user_schede_rifiuti_processi SET ID_RIF=ID_RIF+".$_POST['ID_RIF'].";";
	$query.="UPDATE user_schede_sicurezza SET ID_RIF=ID_RIF+".$_POST['ID_RIF'].";";
	$query.="UPDATE user_aziende_produttori SET ID_AZP=ID_AZP+".$_POST['ID_AZP'].";";
	$query.="UPDATE user_aziende_destinatari SET ID_AZD=ID_AZD+".$_POST['ID_AZD'].";";
	$query.="UPDATE user_aziende_intermediari SET ID_AZI=ID_AZI+".$_POST['ID_AZI'].";";
	$query.="UPDATE user_aziende_trasportatori SET ID_AZT=ID_AZT+".$_POST['ID_AZT'].";";
	$query.="UPDATE user_impianti_produttori SET ID_UIMP=ID_UIMP+".$_POST['ID_UIMP'].", ID_AZP=ID_AZP+".$_POST['ID_AZP'].";";
	$query.="UPDATE user_impianti_destinatari SET ID_UIMD=ID_UIMD+".$_POST['ID_UIMD'].", ID_AZD=ID_AZD+".$_POST['ID_AZD'].";";
	$query.="UPDATE user_impianti_trasportatori SET ID_UIMT=ID_UIMT+".$_POST['ID_UIMT'].", ID_AZT=ID_AZT+".$_POST['ID_AZT'].";";
	$query.="UPDATE user_impianti_intermediari SET ID_UIMI=ID_UIMI+".$_POST['ID_UIMI'].", ID_AZI=ID_AZI+".$_POST['ID_AZI'].";";
        $query.="UPDATE user_autorizzazioni_pro SET ID_AUTH=ID_AUTH+".$_POST['ID_AUTH'].", ID_UIMP=ID_UIMP+".$_POST['ID_UIMP'].", ID_RIF=ID_RIF+".$_POST['ID_RIF'].";";
	$query.="UPDATE user_autorizzazioni_dest SET ID_AUTHD=ID_AUTHD+".$_POST['ID_AUTHD'].", ID_UIMD=ID_UIMD+".$_POST['ID_UIMD'].", ID_RIF=ID_RIF+".$_POST['ID_RIF'].";";
	$query.="UPDATE user_autorizzazioni_trasp SET ID_AUTHT=ID_AUTHT+".$_POST['ID_AUTHT'].", ID_UIMT=ID_UIMT+".$_POST['ID_UIMT'].", ID_RIF=ID_RIF+".$_POST['ID_RIF'].";";
	$query.="UPDATE user_autorizzazioni_interm SET ID_AUTHI=ID_AUTHI+".$_POST['ID_AUTHI'].", ID_UIMI=ID_UIMI+".$_POST['ID_UIMI'].", ID_RIF=ID_RIF+".$_POST['ID_RIF'].";";
	$query.="UPDATE user_movimenti_fiscalizzati SET ID_MOV_F=ID_MOV_F+".$_POST['ID_MOV_F']." WHERE ID_MOV_F<>0;";
	$query.="UPDATE user_movimenti_fiscalizzati SET ID_RIF=ID_RIF+".$_POST['ID_RIF']." WHERE ID_RIF<>0;";
	$query.="UPDATE user_movimenti_fiscalizzati SET ID_AUTST=ID_AUTST+".$_POST['ID_AUTST']." WHERE ID_AUTST<>0;";
	$query.="UPDATE user_movimenti_fiscalizzati SET ID_RMK=ID_RMK+".$_POST['ID_RMK']." WHERE ID_RMK<>0;";
	$query.="UPDATE user_movimenti_fiscalizzati SET ID_AUTO=ID_AUTO+".$_POST['ID_AUTO']." WHERE ID_AUTO<>0;";
	$query.="UPDATE user_movimenti_fiscalizzati SET ID_AZP=ID_AZP+".$_POST['ID_AZP']." WHERE ID_AZP<>0;";
	$query.="UPDATE user_movimenti_fiscalizzati SET ID_AZD=ID_AZD+".$_POST['ID_AZD']." WHERE ID_AZD<>0;";
	$query.="UPDATE user_movimenti_fiscalizzati SET ID_AZT=ID_AZT+".$_POST['ID_AZT']." WHERE ID_AZT<>0;";
	$query.="UPDATE user_movimenti_fiscalizzati SET ID_AZI=ID_AZI+".$_POST['ID_AZI']." WHERE ID_AZI<>0;";
	$query.="UPDATE user_movimenti_fiscalizzati SET ID_UIMP=ID_UIMP+".$_POST['ID_UIMP']." WHERE ID_UIMP<>0;";
	$query.="UPDATE user_movimenti_fiscalizzati SET ID_UIMD=ID_UIMD+".$_POST['ID_UIMD']." WHERE ID_UIMD<>0;";
	$query.="UPDATE user_movimenti_fiscalizzati SET ID_UIMT=ID_UIMT+".$_POST['ID_UIMT']." WHERE ID_UIMT<>0;";
	$query.="UPDATE user_movimenti_fiscalizzati SET ID_UIMI=ID_UIMI+".$_POST['ID_UIMI']." WHERE ID_UIMI<>0;";
        $query.="UPDATE user_movimenti_fiscalizzati SET ID_AUTH=ID_AUTH+".$_POST['ID_AUTH']." WHERE ID_AUTH<>0;";
	$query.="UPDATE user_movimenti_fiscalizzati SET ID_AUTHD=ID_AUTHD+".$_POST['ID_AUTHD']." WHERE ID_AUTHD<>0;";
	$query.="UPDATE user_movimenti_fiscalizzati SET ID_AUTHT=ID_AUTHT+".$_POST['ID_AUTHT']." WHERE ID_AUTHT<>0;";
	$query.="UPDATE user_movimenti_fiscalizzati SET ID_AUTHI=ID_AUTHI+".$_POST['ID_AUTHI']." WHERE ID_AUTHI<>0;";
	$query.="SET FOREIGN_KEY_CHECKS=1;";
	echo "<textarea style=\"width:500px;height:300px;\">$query</textarea>";
	}

?>
  
<form name="increment" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
	<table>

		<tr>
			<th colspan="2">Autista e mezzi</th>
		</tr>

		<tr>
			<td>ID AUTISTA:</td>
			<td><input type="text" name="ID_AUTST" value="<?php if(isset($_POST['ID_AUTST'])) echo $_POST['ID_AUTST']; ?>" /></td>
		</tr>
		<tr>
			<td>ID AUTOMEZZO:</td>
			<td><input type="text" name="ID_AUTO" value="<?php if(isset($_POST['ID_AUTO'])) echo $_POST['ID_AUTO']; ?>" /></td>
		</tr>
		<tr>
			<td>ID RIMORCHIO:</td>
			<td><input type="text" name="ID_RMK" value="<?php if(isset($_POST['ID_RMK'])) echo $_POST['ID_RMK']; ?>" /></td>
		</tr>

		<tr>
			<th colspan="2">Aziende produttori</th>
		</tr>

		<tr>
			<td>ID AZIENDA PRODUTTORE:</td>
			<td><input type="text" name="ID_AZP" value="<?php if(isset($_POST['ID_AZP'])) echo $_POST['ID_AZP']; ?>" /></td>
		</tr>
		<tr>
			<td>ID IMPIANTO PRODUTTORE:</td>
			<td><input type="text" name="ID_UIMP" value="<?php if(isset($_POST['ID_UIMP'])) echo $_POST['ID_UIMP']; ?>" /></td>
		</tr>
                
                <tr>
			<td>ID AUTORIZZAZIONE PRODUTTORE:</td>
			<td><input type="text" name="ID_AUTH" value="<?php if(isset($_POST['ID_AUTH'])) echo $_POST['ID_AUTH']; ?>" /></td>
		</tr>
		
		<tr>
			<th colspan="2">Aziende destinatari</th>
		</tr>

		<tr>
			<td>ID AZIENDA DESTINATARIO:</td>
			<td><input type="text" name="ID_AZD" value="<?php if(isset($_POST['ID_AZD'])) echo $_POST['ID_AZD']; ?>" /></td>
		</tr>
		<tr>
			<td>ID IMPIANTO DESTINATARIO:</td>
			<td><input type="text" name="ID_UIMD" value="<?php if(isset($_POST['ID_UIMD'])) echo $_POST['ID_UIMD']; ?>" /></td>
		</tr>
		<tr>
			<td>ID AUTORIZZAZIONE DESTINATARIO:</td>
			<td><input type="text" name="ID_AUTHD" value="<?php if(isset($_POST['ID_AUTHD'])) echo $_POST['ID_AUTHD']; ?>" /></td>
		</tr>

		<tr>
			<th colspan="2">Aziende trasportatori</th>
		</tr>

		<tr>
			<td>ID AZIENDA TRASPORTATORE:</td>
			<td><input type="text" name="ID_AZT" value="<?php if(isset($_POST['ID_AZT'])) echo $_POST['ID_AZT']; ?>" /></td>
		</tr>
		<tr>
			<td>ID IMPIANTO TRASPORTATORE:</td>
			<td><input type="text" name="ID_UIMT" value="<?php if(isset($_POST['ID_UIMT'])) echo $_POST['ID_UIMT']; ?>" /></td>
		</tr>
		<tr>
			<td>ID AUTORIZZAZIONE TRASPORTATORE:</td>
			<td><input type="text" name="ID_AUTHT" value="<?php if(isset($_POST['ID_AUTHT'])) echo $_POST['ID_AUTHT']; ?>" /></td>
		</tr>

		<tr>
			<th colspan="2">Aziende intermediari</th>
		</tr>

		<tr>
			<td>ID AZIENDA INTERMEDIARIO:</td>
			<td><input type="text" name="ID_AZI" value="<?php if(isset($_POST['ID_AZI'])) echo $_POST['ID_AZI']; ?>" /></td>
		</tr>
		<tr>
			<td>ID IMPIANTO INTERMEDIARIO:</td>
			<td><input type="text" name="ID_UIMI" value="<?php if(isset($_POST['ID_UIMI'])) echo $_POST['ID_UIMI']; ?>" /></td>
		</tr>
		<tr>
			<td>ID AUTORIZZAZIONE INTERMEDIARIO:</td>
			<td><input type="text" name="ID_AUTHI" value="<?php if(isset($_POST['ID_AUTHI'])) echo $_POST['ID_AUTHI']; ?>" /></td>
		</tr>

		<tr>
			<th colspan="2">Schede rifiuti</th>
		</tr>

		<tr>
			<td>ID SCHEDA RIFIUTO:</td>
			<td><input type="text" name="ID_RIF" value="<?php if(isset($_POST['ID_RIF'])) echo $_POST['ID_RIF']; ?>" /></td>
		</tr>

		<tr>
			<th colspan="2">Movimentazione</th>
		</tr>

		<tr>
			<td>ID MOVIMENTO:</td>
			<td><input type="text" name="ID_MOV" value="<?php if(isset($_POST['ID_MOV'])) echo $_POST['ID_MOV']; ?>" /></td>
		</tr>

		<tr>
			<td>ID MOVIMENTO FISCALIZZATO:</td>
			<td><input type="text" name="ID_MOV_F" value="<?php if(isset($_POST['ID_MOV_F'])) echo $_POST['ID_MOV_F']; ?>" /></td>
		</tr>

		<tr>
			<th colspan="2"><input type="submit" name="conferma" value="conferma" /></th>
		</tr>

	</table>

</form>

 </BODY>
</HTML>
