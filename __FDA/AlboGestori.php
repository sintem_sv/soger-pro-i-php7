<?php

/**
 * Classe per connessione al server intermedio di consultazione albo gestori
 *
 * Linux: installare il modulo per supporto cURL (php-curl o simili)
 * XAMPP: abilitare cURL in php.ini (cercare "extension=php_curl.dll")
 *
 */
class AlboGestori
{

    private $options = [
        'url' => 'https://151.236.56.241',
        'port' => '8000', // 8000 = test; 8001 = prod
        'version' => 'v1',
        'username' => "MOLE0001",
        'password' => "ollOpcojOjUd5Cu4bawdEarr"
    ];

    private $basicCurlConfig = [
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_SSL_VERIFYPEER => false,    // self-signed
        CURLOPT_SSL_VERIFYHOST => false     // self-signed
    ];


    function __construct($nondefault_option_values = []) {

        // load extra options
        foreach ($nondefault_option_values as $key => $value) {
            $this->options[$key] = $value;
        }

        // setup authentication
        $this->basicCurlConfig[CURLOPT_USERPWD] =
                $this->options['username'].":".$this->options['password'];
    }



    /**
     * Queries the validation service
     * @param  string $p_iva  Partita iva transporter
     * @param  string $cer    CER code ("XX.YY.ZZ" or "XXYYZZ")
     * @param  string $targa1 Vehicle license plate
     * @param  string $targa2 Trailer license plate (optional)
     * @return mixed          Response (array) from service, or error string
     */
    public function validaConferimento($p_iva, $cer, $targa1, $targa2=null) {

        //$endpoint = $this->options['url'] . '/validate';
        $endpoint = $this->options['url'] . ':' . $this->options['port'];
        $endpoint.= '/' . $this->options['version'] . '/validate';

        $querystring = http_build_query([
              'piva'   => $p_iva,
              'cer'    => $cer,
              'targa1' => $targa1,
              'targa2' => $targa2
        ]);

        $curl = curl_init();
        curl_setopt_array($curl, $this->basicCurlConfig);
        curl_setopt($curl, CURLOPT_URL, $endpoint . '?' . $querystring);

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
          return "Error: " . $err;
        }

        switch ($httpcode) {
            case 200:
                return json_decode($response);
            case 401:
                return "Error: Unauthorized";
            default:
                return "Error: $httpcode";
                break;
        }

    }

}

