$(document).ready(function(){

    function setupDownloadCertificato(){
        if($("#user_movimenti_fiscalizzati\\:FDAchecked").val()==1){
            $("#btn_certificato_verifica_targhe").css('position', 'absolute');
            $("#btn_certificato_verifica_targhe").css('top', '285px');
            $("#btn_certificato_verifica_targhe").css('left', '395px');
            $("#btn_certificato_verifica_targhe").show();
        }
        else{
            $("#btn_certificato_verifica_targhe").hide();
        }
    }

    setupDownloadCertificato();

    $("#core_impianti\\:FDA_START_1").datepicker({dateFormat: 'dd/mm/yy'});
    $("#core_impianti\\:FDA_END_1").datepicker({dateFormat: 'dd/mm/yy'});
    $("#core_impianti\\:FDA_START_2").datepicker({dateFormat: 'dd/mm/yy'});
    $("#core_impianti\\:FDA_END_2").datepicker({dateFormat: 'dd/mm/yy'});

    $('#btn_verifica_targhe').click(function(){

        var fakeValue = 'garbage';
        var msg = "ATTENZIONE\n\n";

        var validation = $("#user_movimenti_fiscalizzati\\:ID_AUTO").val()!=fakeValue;
        if($('#targhe').is(":visible") == true)
            validation = document.CreaTarghe.user_automezzi_description.value!='' || document.CreaTarghe.user_rimorchi_description.value!='';

        if(validation){
            msg+="Cliccando sul bottone \"OK\" grazie al Sistema di Interoperabilita' con l'Albo Gestori Ambientali ";
            msg+="verra' eseguita la verifica della compatibilita' tra le targhe inserite, l'azienda di trasporto e il codice CER selezionato."
            if(window.confirm(msg))
                $( "#Popup_FDA" ).dialog("open");
        }
        else{
            msg+="Per procedere alla verifica e' necessario specificare almeno la targa dell'automezzo."
            window.alert(msg);
        }

    });

    $('#btn_reset_verifica_targhe').click(function(){
        $("#user_movimenti_fiscalizzati\\:ID_AUTO").val($("#user_movimenti_fiscalizzati\\:ID_AUTO option:first").val());
        $("#user_movimenti_fiscalizzati\\:ID_RMK").val($("#user_movimenti_fiscalizzati\\:ID_RMK option:first").val());
        $("#user_movimenti_fiscalizzati\\:FDAinvalid").val(0);
    });

    $( "#Popup_FDA" ).dialog({
        modal: true,
        autoOpen: false,
        resizable: false,
        width: 480,
        buttons: {
            "Chiudi": function() {
                $( this ).dialog( "close" );
            }
        },
        open: function( event, ui ) {

            var FDAfullResponse = '';
            var FDAshortResponse = '';
            var FDAchecked = $("#user_movimenti_fiscalizzati\\:FDAchecked").val();

            var codice_fiscale = $( '#FDA-codice_fiscale' ).val();
            var cer = $( '#FDA-cer' ).val();
            var ID_MOV_F = $( '#FDA-ID_MOV_F' ).val();
            var NFORM = $( '#FDA-NFORM' ).val();
            var DTFORM = $( '#FDA-DTFORM' ).val();

            if($('#targhe').is(":visible") == true){
                var targaMezzo1 = document.CreaTarghe.user_automezzi_description.value;
                var targaMezzo2 = document.CreaTarghe.user_rimorchi_description.value;
            }
            else{
                var targaMezzo1 = $( '#user_movimenti_fiscalizzati\\:ID_AUTO option:selected' ).text();
                var targaMezzo2 = $( '#user_movimenti_fiscalizzati\\:ID_RMK option:selected' ).text();
            }

            $.post('__FDA/FDA_reader.php', {FDAchecked:FDAchecked, codice_fiscale:codice_fiscale, cer:cer, targaMezzo1:targaMezzo1, targaMezzo2:targaMezzo2, ID_MOV_F:ID_MOV_F, NFORM:NFORM, DTFORM:DTFORM}, function(response){
                console.log(response);
                // mark record with check performed flag
                $("#user_movimenti_fiscalizzati\\:FDAchecked").val(1);
                setupDownloadCertificato();

                // handle response
                if(!response.result){
                    $( '#user_movimenti_fiscalizzati\\:FDAinvalid' ).val(1);
                    FDAshortResponse = "negativo";
                    FDAfullResponse += " Di seguito gli errori riscontrati:<br/><br/>";
                    if (response.message.indexOf(";")!=-1){
                        var errorList = response.message.split(";");
//                        console.log(errorList);
                        errorList.forEach(function(error){
                            //console.log(error);
                            if(error.trim().length>0){
                                var errorDetail = error.split(":");
                                //errorDetail[0] could be ERR or WARNING
                                FDAfullResponse+= "-" + errorDetail[1] + "<br/>";
                            }
                        });
                    } else {
                        FDAfullResponse+= " Errore sconosciuto. <br/>";
                    }
                }
                else
                    FDAshortResponse = "positivo";

                FDAfullResponse += "<br/>E' possibile scaricare la <a href=\"__FDA/certificate.php?ID_MOV_F=" + ID_MOV_F + "\"><b>certificazione della verifica</b></a> appena effettuata.";

                if($('#targhe').is(":visible") && response.result){
                    AjaxTarghe('user_movimenti_fiscalizzati', 1);
                    FDAfullResponse += "<br/><br/>Le targhe sono inoltre state salvate nell'anagrafica del fornitore.";
                }

                // Hide loading and show response
                $( '#FDAshortResponse' ).html(FDAshortResponse);
                $( '#FDAfullResponse' ).html(FDAfullResponse);
                $( '.hideWhenLoaded' ).hide();
                $( '.showWhenLoaded' ).show();
            },"json");
        },
        close: function( event, ui ) {
            $( '.hideWhenLoaded' ).show();
            $( '.showWhenLoaded' ).hide();
            $( '.FDAshortResponse' ).html('');
            $( '.FDAfullResponse' ).html('');

			// submit se esito positivo
			var esito_positivo = $( '#user_movimenti_fiscalizzati\\:FDAinvalid' ).val() == 0;

			// submit se esito negativo e configurazione consente comunque salvataggio
			var esito_negativo_con_riserve = $( '#user_movimenti_fiscalizzati\\:FDAinvalid' ).val() == 1 && !SOGER_FDALock;

			if(esito_positivo || esito_negativo_con_riserve)
				FGEForm_1_SendCheck(document.forms[0])
        }
    });

});