<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/fpdf.php");
include("../__libs/SQLFunct.php");
require_once("../__scripts/STATS_funct.php");

$SQL = "SELECT * FROM debug_fda WHERE ID_MOV_F=".$_GET['ID_MOV_F']." ORDER BY id DESC LIMIT 0,1";
$FEDIT->SDBRead($SQL,"DbRecordSet",true,false);
//die(var_dump($FEDIT->DbRecordSet[0]));

$NFORM = $FEDIT->DbRecordSet[0]['NFORM'];
$TIME = explode(" ", $FEDIT->DbRecordSet[0]['timeStampRichiesta']);
$FDA_DATE = explode("-", $TIME[0]);
$FDA_DATE = $FDA_DATE[2]."/".$FDA_DATE[1]."/".$FDA_DATE[0];
$FDA_TIME = $TIME[1];
$FDA_PARAMS = json_decode($FEDIT->DbRecordSet[0]['params']);
$FDA_RESPONSE = json_decode($FEDIT->DbRecordSet[0]['response']);
$FDA_ESITO = $FDA_RESPONSE->esito? "positivo":"negativo";

###########################################

$orientation    = "P";
$statTitle	= "ESITO DELLA VERIFICA DEL FORMULARIO ".$NFORM;
$DcName		= date("d/m/Y") . "--Esito_verifica_FIR_".$NFORM;

$um             = "mm";
$Format         = array(210,297);
$ZeroMargin     = true;
$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin,$DcName);

$Ypos		= 10;
$Xpos		= 10;

$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','b',14);
$FEDIT->FGE_PdfBuffer->MultiCell(190,15,$statTitle,0,"C");

$Ypos+= 25;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(190,5,"In data " . $FDA_DATE . ", alle ore " . $FDA_TIME,0,"L");

$text = "è stata richiesta la verifica dalla validità del conferimento avvenuto "
        . "con il formulario ".$NFORM.". I dati sottoposti a controllo "
        . "sono stati i seguenti:";
$Ypos+= 10;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(190,5,iconv('UTF-8', 'windows-1252', $text),0,"L");

$text = "Targa motrice: ".$FDA_PARAMS->targaMezzo1;
$Ypos+= 15;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(190,5,iconv('UTF-8', 'windows-1252', $text),0,"L");

if($FDA_PARAMS->targaMezzo2){
    $text = "Targa rimorchio: ".$FDA_PARAMS->targaMezzo2;
    $Ypos+= 5;
    $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
    $FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
    $FEDIT->FGE_PdfBuffer->MultiCell(190,5,iconv('UTF-8', 'windows-1252', $text),0,"L");
}

$text = "Codice fiscale dell'Azienda: ".$FDA_PARAMS->identificativo;
$Ypos+= 5;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(190,5,iconv('UTF-8', 'windows-1252', $text),0,"L");

$text = "Codice CER: ".$FDA_PARAMS->CER;
$Ypos+= 5;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(190,5,iconv('UTF-8', 'windows-1252', $text),0,"L");


if($FDA_RESPONSE->esito)
    $text = "La verifica ha dato esito ".$FDA_ESITO;
else
    $text = "La verifica ha dato esito ".$FDA_ESITO." per le seguenti motivazioni:";

$Ypos+= 10;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(190,5,iconv('UTF-8', 'windows-1252', $text),0,"L");

if(!$FDA_RESPONSE->esito){
    $Ypos+= 5;
    $errors = explode("; ", $FEDIT->DbRecordSet[0]['message']);
    foreach($errors as $error){
        if(strlen(trim($error))>0){
            $e = explode(":", $error);
            $Ypos+= 5;
            $FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
            $FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
            $FEDIT->FGE_PdfBuffer->MultiCell(190,5,iconv('UTF-8', 'windows-1252', "- ".$e[1]),0,"L");
        }
    }
}

$text = "Fonte dei dati";
$Ypos+= 15;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','b',10);
$FEDIT->FGE_PdfBuffer->MultiCell(190,5,iconv('UTF-8', 'windows-1252', $text),0,"L");

$text = "Si attesta che l’informazione è firmata digitalmente e proviene "
        . "dall’archivio online interoperabile dell’Albo Nazionale dei Gestori "
        . "Ambientali ed è da ritenersi valida al momento dell’esecuzione della verifica. ";
$Ypos+= 10;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(190,5,iconv('UTF-8', 'windows-1252', $text),0,"L");

PDFOut($FEDIT->FGE_PdfBuffer,$DcName);

?>