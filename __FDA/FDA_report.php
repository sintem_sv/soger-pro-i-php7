<?php

session_start();

require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/fpdf.php");
include("../__libs/SQLFunct.php");
require_once("../__scripts/STATS_funct.php");

###########################################

$SQL = "SELECT * FROM debug_fda ";
$SQL.= "WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' ";
$SQL.= "ORDER BY ID_MOV_F, timeStampRichiesta;";
$FEDIT->SDBRead($SQL,"DbRecordSet",true,false);

$CSVbuf  = AddCSVcolumn("N. verifica");
$CSVbuf .= AddCSVcolumn("Formulario");
$CSVbuf .= AddCSVcolumn("Data di emissione");
$CSVbuf .= AddCSVcolumn("Codice CER");
$CSVbuf .= AddCSVcolumn("Targa motrice");
$CSVbuf .= AddCSVcolumn("Targa rimorchio");
$CSVbuf .= AddCSVcolumn("Data/ora di verifica");
$CSVbuf .= AddCSVcolumn("Esito della verifica");
$CSVbuf .= AddCSVRow();

$ID_MOV_F_buffer = 0;
$Nverifica = 0;

for($m=0;$m<count($FEDIT->DbRecordSet);$m++){

    if($FEDIT->DbRecordSet[$m]['ID_MOV_F']!=$ID_MOV_F_buffer)
        $Nverifica++;

    /* params */
    $params = json_decode($FEDIT->DbRecordSet[$m]['params']);

    /* response */
    $response = json_decode($FEDIT->DbRecordSet[$m]['response']);
    $esito = $FEDIT->DbRecordSet[$m]['result']? "positivo":"negativo";
    if(!$FEDIT->DbRecordSet[$m]['result'])
        $esito .= ": ".$FEDIT->DbRecordSet[$m]['message'];

    $CSVbuf .= AddCSVcolumn($Nverifica);
    $CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['NFORM']);
    $CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['DTFORM']);
    $CSVbuf .= AddCSVcolumn($params->CER);
    $CSVbuf .= AddCSVcolumn($params->targaMezzo1);
    $CSVbuf .= AddCSVcolumn($params->targaMezzo2);
    $CSVbuf .= AddCSVcolumn($FEDIT->DbRecordSet[$m]['timeStampRichiesta']);
    $CSVbuf .= AddCSVcolumn($esito);
    $CSVbuf .= AddCSVRow();

    $ID_MOV_F_buffer = $FEDIT->DbRecordSet[$m]['ID_MOV_F'];
}

CSVOut($CSVbuf,"Conformitą_targhe_Albo-FIR");

?>