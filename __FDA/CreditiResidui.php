<div style="width:280px;background-color:#FFFFFF;margin-top:20px;">

    <div class="divName" style="text-align:center;height:25px;width:100%;background-color: #4D6980;color: #ffffff;">
        <span style="font-weight: bold;padding:5px;font-size: 13pt;font-family: verdana;text-transform: uppercase;">CREDITI ALBO GESTORI</span>
    </div>

    <div style="width:280px;text-align:center;padding-bottom:10px;">
        <?php
        $sql = "SELECT FDA_START_1, FDA_END_1, FDA_CREDITI_1, FDA_RESIDUI_1, FDA_START_2 FROM core_impianti WHERE ID_IMP='".$SOGER->UserData['core_impiantiID_IMP']."';";
        $FEDIT->SDBRead($sql,"FDA",true,false);

        /* crediti */
        $CreditiUtilizzati = $FEDIT->FDA[0]['FDA_CREDITI_1']-$FEDIT->FDA[0]['FDA_RESIDUI_1'];
        $warningCredits = ($CreditiUtilizzati >= ($FEDIT->FDA[0]['FDA_CREDITI_1']/100*80));
        $warningColor = $warningCredits? "red":"#000";
        echo "<p>Formulari residui:</p>";
        echo "<h1 style='display:inline;color:".$warningColor."'>" . $FEDIT->FDA[0]['FDA_RESIDUI_1'] . "</h1> <h5 style='display:inline'> / " . $FEDIT->FDA[0]['FDA_CREDITI_1'] . "</h5>";

        /* attivazione e scadenza */
        $exploded_attivazione = explode("-", $FEDIT->FDA[0]['FDA_START_1']);
        $attivazione = $exploded_attivazione[2]."/".$exploded_attivazione[1]."/".$exploded_attivazione[0];
        $exploded_scadenza = explode("-", $FEDIT->FDA[0]['FDA_END_1']);
        $scadenza = $exploded_scadenza[2]."/".$exploded_scadenza[1]."/".$exploded_scadenza[0];
        echo "<br /><br />Il servizio � attivo dal ".$attivazione." al <b>".$scadenza."</b>";
        if(is_null($FEDIT->FDA[0]['FDA_START_2']) && strtotime(date('Y-M-d')) >= strtotime('-1 month', strtotime($FEDIT->FDA[0]['FDA_END_1']))){
            $difference = strtotime($FEDIT->FDA[0]['FDA_END_1']) - strtotime(date('Y-M-d'));
            echo "<br/><b>Attenzione!</b> Mancano <h3 style='color:red;display:inline;'>".floor($difference/60/60/24)."</h3> giorni alla scadenza.";
        }
        ?>
    </div>

</div>