<?php

session_start();

require_once("AlboGestori.php");
require_once("custom_option_values.php");

require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");

global $SOGER;

$albo = new AlboGestori($custom_option_values);

/* input */
$codice_fiscale = $_POST['codice_fiscale'];
$cer = $_POST['cer'];
$targaMezzo1 = $_POST['targaMezzo1'];
$targaMezzo2 = $_POST['targaMezzo2'];
// ---
$FDAchecked = $_POST['FDAchecked'];
$ID_MOV_F = $_POST['ID_MOV_F'];
$NFORM = $_POST['NFORM'];
$DTFORM = $_POST['DTFORM'];

/* perform request */
$esito = $albo->validaConferimento($codice_fiscale, $cer, $targaMezzo1, $targaMezzo2);

$result = $esito->result? 1:0;
$timeStampRichiesta = gmdate("Y-m-d\TH:i:s", strtotime($esito->timestamp)+date("Z"));

/* log request into `debug_fda` table */
$sql = "INSERT INTO `debug_fda` ";
$sql.= "(`ID_MOV_F`, `NFORM`, `DTFORM`, `timeStampRichiesta`, `date`, `cer`, `mezzo1`, `mezzo2`, `params`, `response`, `status`, `result`, `message`, `ID_USR`, `ID_IMP`, `workmode`) ";
$sql.= "VALUES ('".$ID_MOV_F."', '".$NFORM."', '".$DTFORM."', '".$timeStampRichiesta."', '".date('Y-m-d')."', '".$cer."', '".$targaMezzo1."', '".$targaMezzo2."', '".addslashes(json_encode($esito->debug->params))."', '".addslashes(json_encode($esito->debug->response))."', '".$esito->status."', '". $result ."', '".addslashes($esito->message)."', '".$SOGER->UserData['core_usersID_USR'] . "', '".$SOGER->UserData['core_usersID_IMP']."', '".$SOGER->UserData['workmode']."');";
$FEDIT->SDBWrite($sql,true,false);

/* scarico credito */
if(!$FDAchecked){
    $sql = "UPDATE `core_impianti` SET `FDA_RESIDUI_1`=`FDA_RESIDUI_1`-1 WHERE `ID_IMP`='".$SOGER->UserData['core_usersID_IMP']."';";
    $FEDIT->SDBWrite($sql,true,false);
    $sql = "UPDATE `user_movimenti_fiscalizzati` SET `FDAchecked`=1 WHERE `ID_MOV_F`=".$ID_MOV_F.";";
    $FEDIT->SDBWrite($sql,true,false);
}

/* return request's result */
echo json_encode($esito);

?>