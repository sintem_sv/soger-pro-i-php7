<?php

session_start();
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__SIS/SIS_SOAP_SSL.php");

$CERT			= "certificates/".$SOGER->UserData['core_impianticodfisc']."/crt_key.pem";

$SelectSedi	= Array();
	

try{
	$SIS		= new SIS_SOAP($SOGER->UserData['core_usersSIS_identity'], $CERT);
	$Azienda	= $SIS->GetAzienda(Array("codiceFiscaleAzienda"=>$SOGER->UserData['core_impianticodfisc']));

	foreach ($Azienda->sediSummary AS $sede){

		$CurrentSede=Array();
		$CurrentSede['idSIS']		= $sede->idSIS;
		$CurrentSede['nomeSede']	= $sede->nomeSede;
		$CurrentSede['indirizzo']	= $sede->indirizzo . " " . $sede->nrCivico;

		$SQL ="SELECT description FROM lov_comuni_istat WHERE ISTAT_comune=".$sede->codiceIstatLocalita.";";
		$FEDIT->SdbRead($SQL,"Impianto");
		if(isset($FEDIT->Impianto)) $CurrentSede['indirizzo'].=", ".$FEDIT->Impianto[0]['description'];

		
		if(!is_null($SOGER->UserData['idSIS_sede']) AND $SOGER->UserData['idSIS_sede']==$sede->idSIS)
			$CurrentSede['selected']	= true;
		else
			$CurrentSede['selected']	= false;
	
		array_push($SelectSedi, $CurrentSede);
		unset($CurrentSede);

		}

	echo json_encode($SelectSedi);

	}

catch(SoapFault $SIS_error){
	
	$SISexcept	= SIS_SOAP::getSISException($SIS_error);
	$output		= "<p><b>Error code:</b><br />\n{$SISexcept->errorCode}<br />\n{$SISexcept->errorMessage}\n</p>";
	
	echo $output;

	}


require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>