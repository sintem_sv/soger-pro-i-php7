<?php

//
// Inserimento di una registrazione cronologica di carico
//

require_once("SIS_SOAP.php");


$id_sis_registro  = "20768";

$reg = new RegistrazioneCrono_Base;
$reg->codiceCerIIILivello = new Catalogo("15.01.03");

$reg->statoFisicoRifiuto  = new Catalogo("02");
// oppure potevo scrivere:
// $reg->statoFisicoRifiuto  = array("idCatalogo"=>"02");

$reg->quantita            = new LongNumber(5000000000);
$reg->descrizioneRifiuto  = "Imballaggi in legno";

#    $reg->numeroNotifica; // string
#    $reg->flagVeicoli_Dlgs_209_2003; // Flag
#    $reg->flagVeicoli_Dlgs_Art231_152_2006; // Flag
#    $reg->numeroVeicoliConferiti; // LongNumber
#    $reg->categoriaRAEE; // Catalogo
#    $reg->tipologiaRAEE; // Catalogo
#    $reg->flagRiutilizzoAppIntera; // Flag
#    $reg->flagOpRecuperoEnergia; // Flag
#    $reg->flagOpRecuperoMateria; // Flag
#    $reg->operazioneImpianto; // Catalogo
#    $reg->idSISSede_impiantoOrigine; // string
#    $reg->versioneSede_impiantoOrigine; // LongNumber
#    $reg->idSISSede_impiantoDestinazione; // string
#    $reg->versioneSede_impiantoDestinazione; // LongNumber
#    $reg->descrizioneAltroStatoFisico; // string
#    $reg->caratteristicaPericolo; // Catalogo
#    $reg->idSISSede_consegnatoA; // string
#    $reg->versioneSede_consegnatoA; // LongNumber


$s = new SIS_SOAP("paolo.vaccaneo3747");


try {

    echo "===== PutRegistrazioneCronoCarico  \n";
    $rv1 = $s->PutRegistrazioneCronoCarico(
                array(  
                "idSISRegistroCrono"=>$id_sis_registro,
                "registrazione" => $reg, 
        )); 
    echo "Creata registrazione di carico: $rv1\n";


} catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n\n";
    echo $s->getLastTransaction();
    var_dump($e->faultcode, $e->faultstring, $e->detail);
}


?>
