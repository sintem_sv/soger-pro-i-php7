<?php

// Gestione errori con eccezioni
// 

require_once("SIS_SOAP.php");

try {
    $s = new SIS_SOAP("paolo.vaccaneo2960");
    $catalogo = $s->GetVersioneCatalogo(array("catalogo"=>"CATALOGO_INESISTENTE")); 

} catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n";
}

?>
