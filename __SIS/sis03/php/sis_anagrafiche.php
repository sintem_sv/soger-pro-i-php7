<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once("SIS_SOAP_SSL.php");


$cod_fiscale = "00570070011";
//$id_sis      = "908992"; // -> sede
$cert="../../certificates/07017700019/crt_key.pem";

/*
210519
432692
432694
*/

$s = new SIS_SOAP("paolo.vaccaneo6548", $cert);

try {
   
	/*
	echo "<hr>===== GetVersioneAnagraficaAzienda <br /> [$cod_fiscale] \n";
    $rv1 = $s->GetVersioneAnagraficaAzienda(array("codiceFiscaleAzienda"=>$cod_fiscale)); 
    var_dump($rv1);
    echo " => " , $rv1->long , "\n\n";
	*/
    
	echo "<hr>===== GetAzienda <br /> [$cod_fiscale] \n";
    $rv2 = $s->GetAzienda(Array("codiceFiscaleAzienda"=>$cod_fiscale));
    var_dump_pre($rv2); 
    //echo " => " , $rv2->ragioneSociale , "\n\n";

	echo "<hr>===== RecuperaVersioniAziendaESedi <br /> [$cod_fiscale] \n";
    $rv90 = $s->RecuperaVersioniAziendaESedi(array("codiceFiscaleAzienda"=>$cod_fiscale)); 
     var_dump_pre($rv90);


    echo "<hr>===== GetSede <br /> [$id_sis] \n";
    $rv3 = $s->GetSede(array("idSIS"=>$id_sis));
     var_dump_pre($rv3); 
    //echo " => " , $rv3->indirizzo , "\n\n";    
	
/*
    echo "<hr>===== GetVeicoli <br /> [$id_sis] \n";    
    var_dump( $s->GetVeicoli(Array("idSISSede" => $id_sis )));     
*/


/*    
    echo "<hr>===== GetTokens [$id_sis] \n"; 
    $rv5 = $s->GetTokens(Array("idSISSede"  => $id_sis )); 
    var_dump($rv5);
    # foreach ($rv5["tokens"] as $tok) {  --- MODIFICATO IN
    foreach ($rv5 as $tok) {   
        echo " => " , $tok->serialNumber , "\n";
    }

    echo "<hr>===== GetRegistroCronologico [$id_sis] \n";  
    $rv6 = $s->GetRegistroCronologico(Array("idSISSede"  => $id_sis));
    var_dump($rv6);
    # foreach ($rv6["registriCronologici"] as $regcrono) { --- MODIFICATO IN 
    foreach ($rv6 as $regcrono) {   
        echo " => {$regcrono->idSIS}: {$regcrono->dataUltimoNumero}\n";
    }     

    echo "<hr>===== GetVersioneAnagrafica \n"; 
    $rv7_1 = $s->GetVersioneAnagrafica(Array("idSIS"=>$id_sis, "tipoAnagrafica"=>'SEDE'));
    echo "<hr>Versione sede $id_sis: {$rv7_1->long}\n";
    $rv7_2 = $s->GetVersioneAnagrafica(Array("idSIS"=>'20767', "tipoAnagrafica"=>'REGISTRO_CRONOLOGICO'));
    echo "<hr>Versione registro 20767: {$rv7_2->long}\n";


    echo "<hr>===== GetSediAziendePartner [$id_sis]\n"; 
    var_dump($s->GetSediAziendePartner(Array("idSISSede" =>$id_sis))); 

*/
} catch (SoapFault $e) {
    echo "<hr>";
	die(var_dump($e));
	$SISexcept = SIS_SOAP::getSISException($e);
	die(var_dump($SISexcept));
    echo "<hr>Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n";
}


function var_dump_pre($mixed = null) {
	echo '<pre>';
	var_dump($mixed);
	echo '</pre>';
	return null;
	}


?>
