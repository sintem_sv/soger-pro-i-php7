<?php

//
// Lettura di una registrazione cronologica
//

require_once("SIS_SOAP_SSL.php");


$id_sis_registro  = "43127";
$CERT		= "../../certificates/04259210153/crt_key.pem";

$id_sis_reg_crono1 = "27121046"; // Carico


$s = new SIS_SOAP("andrea.neri8002", $CERT);

try {

    echo "===== GetRegistrazioneCrono [$id_sis_reg_crono1] \n";
    $rv1 = $s->GetRegistrazioneCrono(array("idSISRegistrazioneCrono"=>$id_sis_reg_crono1)); 
    var_dump($rv1);

    echo "===== GetVersioneRegistrazione [$id_sis_reg_crono1] \n";
    $rv2 = $s->GetVersioneRegistrazione(array("idSISRegistrazioneCrono"=>$id_sis_reg_crono1));
    $versione = $rv2->long;
    echo " => $versione\n\n";


} catch (SoapFault $e) {
//    $SISexcept = SIS_SOAP::getSISException($e);
	die(var_dump($e));
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n\n";
    echo $s->getLastTransaction();
    var_dump($e->faultcode, $e->faultstring, $e->detail);
}


?>
