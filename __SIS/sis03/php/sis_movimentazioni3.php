<?php

//
// Elenco schede SISTRI associate a una registrazione
//

require_once("SIS_SOAP_SSL.php");

$id_sis_movimentazione = "3126062";

$CERT		= "../../certificates/07017700019/crt_key.pem";

$s = new SIS_SOAP("paolo.vaccaneo6548", $CERT);


try {

    echo "===== GetElencoSchedeSISTRI [$id_sis_movimentazione]\n";
    $rv1 = $s->GetElencoSchedeSISTRI(
        array("idSISMovimentazione" => $id_sis_movimentazione)); 
    var_dump($rv1);


    echo "\n\nSchede associate:\n--------------------------\n";

    foreach ($rv1 as $scheda)
        echo " * $scheda->idSIS\n";
    echo "\n";
    
    

} catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n\n";
    echo $s->getLastTransaction();
    var_dump($e->faultcode, $e->faultstring, $e->detail);
}



?>
