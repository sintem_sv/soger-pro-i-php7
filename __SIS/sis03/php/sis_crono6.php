<?php

//
// Inserimento di una registrazione cronologica di scarico
//

require_once("SIS_SOAP.php");

$id_sis_registro  = "20768";

$id_sis_smaltitore = "7478_SPER";

// Tre registrazioni crono CARICO dello stesso tipo di rifiuto, create 
// precedentemente
$id_sis_reg_crono1 = 17007; // Carico
$id_sis_reg_crono2 = 17008; // Carico
$id_sis_reg_crono3 = 17012; // Carico

$s = new SIS_SOAP("paolo.vaccaneo3747");


try {

    // Leggo le tre registrazioni di carico
    echo "===== GetRegistrazioneCrono [$id_sis_reg_crono1] \n";
    $rv1 = $s->GetRegistrazioneCrono(array("idSISRegistrazioneCrono"=>$id_sis_reg_crono1)); 
    echo "===== GetRegistrazioneCrono [$id_sis_reg_crono2] \n";
    $rv2 = $s->GetRegistrazioneCrono(array("idSISRegistrazioneCrono"=>$id_sis_reg_crono2)); 
    echo "===== GetRegistrazioneCrono [$id_sis_reg_crono3] \n";
    $rv3 = $s->GetRegistrazioneCrono(array("idSISRegistrazioneCrono"=>$id_sis_reg_crono3)); 


    // Di ogni carico, scarico la met�
    $quant1 = (int) ($rv1->registrazioneCrono_base->quantita->long / 2);
    $rcc1 = new RegistrazioneCronoCarico($id_sis_reg_crono1, $quant1);
 
    $quant2 = (int) ($rv2->registrazioneCrono_base->quantita->long / 2);
    $rcc2 = new RegistrazioneCronoCarico($id_sis_reg_crono2, $quant2);
  
    $quant3 = (int) ($rv3->registrazioneCrono_base->quantita->long / 2);
    $rcc3 = new RegistrazioneCronoCarico($id_sis_reg_crono3, $quant3);


    $quantita_totale = $quant1 + $quant2 + $quant3;
    echo "\nQuantit� totale: $quantita_totale [mg]\n\n";


    // Inserisco la registrazione di scarico
    $reg = new RegistrazioneCrono_Base;
    $reg->codiceCerIIILivello = array("idCatalogo"=>"15.01.03");
    $reg->statoFisicoRifiuto  = array("idCatalogo"=>"03");
    #$reg->descrizioneRifiuto  = "Imballaggi in legno";
    $reg->quantita            = new LongNumber($quantita_totale);
    $reg->idSISSede_impiantoDestinazione =  $id_sis_smaltitore;
    #$reg->idSISSede_consegnatoA = $id_sis_smaltitore;


#    $reg->numeroNotifica; // string
#    $reg->flagVeicoli_Dlgs_209_2003; // Flag
#    $reg->flagVeicoli_Dlgs_Art231_152_2006; // Flag
#    $reg->numeroVeicoliConferiti; // LongNumber
#    $reg->categoriaRAEE; // Catalogo
#    $reg->tipologiaRAEE; // Catalogo
#    $reg->flagRiutilizzoAppIntera; // Flag
#    $reg->flagOpRecuperoEnergia; // Flag
#    $reg->flagOpRecuperoMateria; // Flag
#    $reg->operazioneImpianto; // Catalogo
#    $reg->idSISSede_impiantoOrigine; // string
#    $reg->versioneSede_impiantoOrigine; // LongNumber
#    $reg->idSISSede_impiantoDestinazione; // string
#    $reg->versioneSede_impiantoDestinazione; // LongNumber
#    $reg->descrizioneAltroStatoFisico; // string
#    $reg->caratteristicaPericolo; // Catalogo
#    $reg->idSISSede_consegnatoA; // string
#    $reg->versioneSede_consegnatoA; // LongNumber


    echo "===== PutRegistrazioneCronoScarico  \n";
    $rvs = $s->PutRegistrazioneCronoScarico(
                array(  
                "idSISRegistroCrono"=>$id_sis_registro,
                "registrazioniCronoCarico"=>array($rcc1, $rcc2, $rcc3),
                "registrazione" => $reg, 
        )); 
    echo "Creata registrazione di scarico: $rvs\n";


} catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n\n";
    echo $s->getLastTransaction();
    var_dump($e->faultcode, $e->faultstring, $e->detail);
}


?>
