<?php

//
// Modifica di una registrazione cronologica di carico
//

require_once("SIS_SOAP.php");


// Inserire un id SIS di registrazione carico creata con sis_crono2.php
$id_sis_reg_crono = 17148; // Carico


$s = new SIS_SOAP("paolo.vaccaneo3747");

try {

    echo "===== GetRegistrazioneCrono [$id_sis_reg_crono] \n";
    $rv1 = $s->GetRegistrazioneCrono(array("idSISRegistrazioneCrono"=>$id_sis_reg_crono)); 

    echo "\n*** Registrazione Cronologica:\n";
    $rv1a = $rv1->registrazioneCrono_base;
    var_dump($rv1a);

    echo "\n*** Versione da modificare:\n";
    $versione = $rv1->versione;
    var_dump($versione);

    # Modifica dato
    $quant_rifiuto = $rv1a->quantita->long;
    $nuova_quant_rifiuto = ($quant_rifiuto=="5000000000"? "6000000000" : "5000000000");
    $rv1a->quantita->long = $nuova_quant_rifiuto;
    echo "\n*** Quant. rifiuto => da $quant_rifiuto a $nuova_quant_rifiuto [mg]\n\n";

    echo "===== UpdateRegistrazioneCronoCarico [$id_sis_reg_crono] \n";
    $rv3 = $s->UpdateRegistrazioneCronoCarico(
                array(  
                "idSISRegistrazioneCrono"=>$id_sis_reg_crono,
                "registrazione" => $rv1a, 
                "versionToUpdate"=>$versione,
        )); 
    var_dump($rv3);


} catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n\n";
    echo $s->getLastTransaction();
    var_dump($e->faultcode, $e->faultstring, $e->detail);
}


?>
