<?php

//
// Recupero di una scheda SISTRI
//

require_once("SIS_SOAP_SSL.php");

$id_sis_scheda  = "22910221";
$CERT		= "../../certificates/08670900151/crt_key.pem";

$s = new SIS_SOAP("antonio.lorusso3151",$CERT);


try {

    echo "===== GetSchedaSISTRI [$id_sis_scheda]\n";
    $rv1 = $s->GetSchedaSISTRI(
        array( "idSISScheda" => $id_sis_scheda, )); 
    var_dump($rv1);



    echo "===== GetMovimentazioneByIdScheda [$id_sis_scheda]\n";
    $rv2 = $s->GetMovimentazioneByIdScheda(
        array( "idSISScheda" => $id_sis_scheda ));
    #var_dump($rv2);
    echo "\n => Movimentazione corrispondente: {$rv2->idSIS}\n\n";


} catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n\n";
    echo $s->getLastTransaction();
    var_dump($e->faultcode, $e->faultstring, $e->detail);
}


?>
