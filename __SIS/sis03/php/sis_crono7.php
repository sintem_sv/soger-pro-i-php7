<?php

//
// Modifica di una registrazione cronologica di scarico
//

require_once("SIS_SOAP.php");


// Inserire un id SIS di registrazione scarico creata con sis_crono6.php
$id_sis_reg_crono = 17027; // Carico


$s = new SIS_SOAP("paolo.vaccaneo3747");

try {

    echo "===== GetRegistrazioneCrono [$id_sis_reg_crono] \n";
    $rv1 = $s->GetRegistrazioneCrono(array("idSISRegistrazioneCrono"=>$id_sis_reg_crono)); 

    echo "\n*** Registrazione Cronologica:\n";
    $rv1a = $rv1->registrazioneCrono_base;
    var_dump($rv1a);

    echo "\n*** Versione da modificare:\n";
    $versione = $rv1->versione;
    var_dump($versione);

    echo "\n*** Registrazioni carico associate:\n";
    $reg_assoc = $rv1->registrazioniCronoAssociate;
    var_dump($reg_assoc);
 
    // Converto da RegistrazioneCronoAssociata a RegistrazioneCronoCarico
    $reg_carico_array = array();
    foreach ($reg_assoc as $ra) {
        $reg_carico_array[] = new RegistrazioneCronoCarico(
                                    $ra->idSISRegistrazioneCrono, 
                                    $ra->quantitaMovimentata->long);
    }
array_pop($reg_carico_array);
    // Modifica dato
    // Tolgo 10kg dallo scarico del primo carico (e dal totale)
    #$reg_carico_array[0]->quantitaDaScaricare->long -= 10000000;
    #$rv1a->quantita->long -= 10000000;
    var_dump($reg_carico_array);
exit();

    echo "===== UpdateRegistrazioneCronoScarico [$id_sis_reg_crono] \n";
    $rv3 = $s->UpdateRegistrazioneCronoScarico(
                array(
                "idSISRegistrazioneCrono"=>$id_sis_reg_crono,
                "registrazioniCronoCarico"=>$reg_carico_array,
                "registrazione" => $rv1a, 
                "versionToUpdate"=>$versione,
        )); 
    var_dump($rv3);
    echo $s->getLastTransaction();

} catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n\n";
    echo $s->getLastTransaction();
    var_dump($e->faultcode, $e->faultstring, $e->detail);
}


?>UpdateRegistrazioneCronoScarico
