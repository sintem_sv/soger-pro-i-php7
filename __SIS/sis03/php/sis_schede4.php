<?php

//
// Creazione di una scheda SISTRI Produttore su movimentazione in bianco
//

require_once("SIS_SOAP.php");





// -------------------------------------------------------------------
// DATI IN INGRESSO (obbligatori dove non indicato)

# Catalogo e tipo
$id_sis_registro  = "20768";
$tipo_registro    = new Catalogo("PRD");

$codiceCerIIILivello  = new Catalogo("15.01.03");
$statoFisicoRifiuto   = new Catalogo("03");
$descrizioneRifiuto   = "Imballaggi in legno (pallet)"; // facoltativo
$quantita             = new LongNumber(5000000000);
$id_sis_trasportatore = "7477_SPER"; // in alternativa: specificare le tratte
$id_sis_destinatario  = "7478_SPER";
$operazioneImpianto   = new Catalogo("D4");    // Lagunaggio :)
$contatto = array(
    "nome"      => "Giovanni",
    "cognome"   => "Pautasso",
    "telefono"  => "011123456",
    "email"     => "gpautasso@example.com"  
);

$scheda = new SchedaSISTRI_Produttore;
$scheda->codiceCerIIILivello     = $codiceCerIIILivello;
$scheda->statoFisicoRifiuto      = $statoFisicoRifiuto;
$scheda->quantita                = $quantita;
$scheda->descrizioneRifiuto      = $descrizioneRifiuto;
$scheda->idSISSede_trasportatore = $id_sis_trasportatore;
$scheda->idSISSede_destinatario  = $id_sis_destinatario;
$scheda->operazioneImpianto      = $operazioneImpianto; 
$scheda->flagPrescrizioniParticolari = new Flag(false);
$scheda->numeroColli                 = new LongNumber(1);
$scheda->autorizzazione = "paolo.vaccaneo3747";
$scheda->autorizzazione = "abcd"; //** ?

$schedabase = new SchedaSISTRI_Base;
$schedabase->schedaSISTRI_produttore     = $scheda;
$schedabase->tipoRegCronologico          = $tipo_registro;
$schedabase->nomePersonaDaContattare     = $contatto["nome"];
$schedabase->cognomePersonaDaContattare  = $contatto["cognome"];
#$schedabase->telefonoPersonaDaContattare = $contatto["telefono"]; // facoltativo
#$schedabase->emailPersonaDaContattare    = $contatto["email"];    // facoltativo





$s = new SIS_SOAP("paolo.vaccaneo3747");

try {

    // Genero UNA movimentazione in bianco
    echo "===== RichiediCodiciPerMovimentazioniBianche\n";    
    $rv1 = $s->RichiediCodiciPerMovimentazioniBianche(array(
                    "howMany"=> new LongNumber(1) ));
    #var_dump($rv1);

    $movimentazione = $rv1[0];
    $id_sis_mov = $movimentazione->idSIS;
    print " ID SIS movimentazione in bianco: $id_sis_mov\n";
    
    
    echo "===== PutSchedaSISTRI_Produttore_BIANCA\n";
    $id_sis_scheda = $s->PutSchedaSISTRI_Produttore(array(
            "idSISMovimentazione"=>$id_sis_mov,
            "scheda"=>$schedabase, 
            ));
    print " ID SIS scheda: $id_sis_mov\n";




} catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n\n";
    echo $s->getLastTransaction();
    var_dump($e->faultcode, $e->faultstring, $e->detail);
}

















?>
