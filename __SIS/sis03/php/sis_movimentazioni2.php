<?php

//
// Ricerca di una registrazione cronologica
//

require_once("SIS_SOAP_SSL.php");

$id_sis_movimentazione = "10448740";
$CERT		= "../../certificates/08670900151/crt_key.pem";

$s = new SIS_SOAP("antonio.lorusso3151",$CERT);

try {

    echo "===== GetMovimentazioneRifiuto [$id_sis_movimentazione]\n";
    $rv1 = $s->GetElencoSchedeSistri(
        array("idSISMovimentazione" => $id_sis_movimentazione)); 
    var_dump($rv1);


    echo "\n\nSchede associate:\n--------------------------\n";

    foreach ($rv1->idSISSchedeAssociate as $idsis_scheda)
        echo " * $idsis_scheda\n";
    echo "\n";


} catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n\n";
    echo $s->getLastTransaction();
    var_dump($e->faultcode, $e->faultstring, $e->detail);
}



?>
