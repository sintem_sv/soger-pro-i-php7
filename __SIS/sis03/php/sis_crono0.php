<?php

//
// Ricerca delle registrazioni cronologiche in un registro dato
//

require_once("SIS_SOAP_SSL.php");

$id_sis_registro  = "43127";
$CERT		= "../../certificates/04259210153/crt_key.pem";

$s = new SIS_SOAP("pierluigi.ferri6416", $CERT);


$filtro_ricerca = new FiltroMovimentazioni;
$filtro_ricerca->dataEoraRegistrazioneInizio	= "2015-12-10T00:00:01.000+02:00";
$filtro_ricerca->dataEoraRegistrazioneFine		= "2015-12-22T19:05:01.000+02:00";
$filtro_ricerca->codiceCerIIILivello = new Catalogo("15.02.02");

try {

    echo "===== GetElencoRegistrazioniCrono [$id_sis_registro] \n";
    $rv1 = $s->GetElencoRegistrazioniCrono(
        array(
            "idSISRegistroCrono"  => $id_sis_registro,
            "filtroRegistrazioni" => $filtro_ricerca,
            "startItemPosition"   => new LongNumber(1) # sempre strettamente > 0
        )); 
    var_dump($rv1);


} catch (SoapFault $e) {
	die(var_dump($e));
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n\n";
    echo $s->getLastTransaction();
    var_dump($e->faultcode, $e->faultstring, $e->detail);
}


# Estraiamo i risultati

echo "\n\nInformazioni sui risultati\n--------------------------\n";

foreach ($rv1->info as $row)
    echo "$row\n";
echo "\n";

echo " -> Elementi corrispondenti alla ricerca: {$rv1->itemsIndividuati->long}\n";
echo " -> Elementi visualizzati: da {$rv1->startItemPosition->long} a {$rv1->endItemPosition->long}\n";
echo " -> Totale Elementi visualizzati: {$rv1->itemsRitornati->long}\n";


if ($rv1->itemsIndividuati->long>0) {

    echo "\n ID SIS dei record trovati:\n";

    foreach ($rv1->registrazioniCronoSummary as $summary)
        echo " * {$summary->idSIS} ({$summary->tipoRegistrazioneCrono->description})\n";
    echo "\n";
}

?>
