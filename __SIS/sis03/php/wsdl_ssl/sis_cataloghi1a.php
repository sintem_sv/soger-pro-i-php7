<?php

require_once("SIS_SOAP_SSL.php");

# CHIAVE: paolo.vaccaneo6627

# DELEGATI: 
# paolo.vaccaneo6548
# davide.moletto6957

$s = new SIS_SOAP("davide.moletto6957", "crt_key.pem");

try {
    $xmlcatalog = $s->GetCatalogo(array("catalogo"=>"TIPI_DOCUMENTO"));
    $catalog = new SimpleXMLElement($xmlcatalog); 
    
    foreach ($catalog->records->record as $cat)
        echo $cat->field[0]->valore, " => ", $cat->field[1]->valore, "\n";
    echo "\n";

} catch (SoapFault $e) {
    var_dump($e);
}

?>
