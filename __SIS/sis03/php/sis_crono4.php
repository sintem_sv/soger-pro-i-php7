<?php

//
// Modifica di una registrazione cronologica di carico su scheda gi� firmata
// (fallisce)
//

$id_sis_reg_crono = 15586; // Carico, gi� firmata


$s = new SIS_SOAP("paolo.vaccaneo3747");

try {

    echo "===== GetRegistrazioneCrono [$id_sis_reg_crono] \n";
    $rv1 = $s->GetRegistrazioneCrono(array("idSISRegistrazioneCrono"=>$id_sis_reg_crono)); 

    echo "\n*** Registrazione Cronologica:\n";
    $rv1a = $rv1->registrazioneCrono_base;
    var_dump($rv1a);

    echo "\n*** Versione da modificare:\n";
    $versione = $rv1->versione;
    var_dump($versione);

    # Modifica dato
    # Sovrascrivo la registrazione con gli stessi valori
    # NOTA: l'update fallisce sempre perch� la registrazione � stata firmata, 
    # quindi � diventata immutabile
    #

    echo "===== UpdateRegistrazioneCronoCarico [$id_sis_reg_crono] \n";
    $rv3 = $s->UpdateRegistrazioneCronoCarico(
                array(  
                "idSISRegistrazioneCrono"=>$id_sis_reg_crono,
                "registrazione" => $rv1a, 
                "versionToUpdate"=>$versione,
        )); 
    var_dump($rv3);


} catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n\n";
    echo $s->getLastTransaction();
    var_dump($e->faultcode, $e->faultstring, $e->detail);
}


?>
