<?php

// Gestione errori con is_soap_fault()
// 

require_once("SIS_SOAP.php");


// Connessione
$s = new SIS_SOAP("paolo.vaccaneo2960");
$catalogo = $s->GetVersioneCatalogo(array("catalogo"=>"CATALOGO_INESISTENTE")); 

if (is_soap_fault($catalogo) ) {

    $SISexcept = SIS_SOAP::getSISException($catalogo);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n";
    echo "\t(valore restituito)\n";
}


?>
