<?php
//
// Cancellazione di una scheda SISTRI
//

require_once("SIS_SOAP.php");

// Inserire un id SIS di una scheda  
// creata con sis_schede2.php
$id_sis_scheda = 19264; // Carico o scarico



$s = new SIS_SOAP("paolo.vaccaneo3747");

try {

    // Movimentazione corrispondente
    $rv1 = $s->GetMovimentazioneByIdScheda(array("idSISScheda"=>$id_sis_scheda));
    $id_sis_mov = $rv1->idSIS;

    echo "* Scheda SISTRI:  $id_sis_scheda\n";
    echo "* Movimentazione: $id_sis_mov\n\n";

    echo "===== DeleteSchedaSISTRI [$id_sis_scheda] \n";
    $s->DeleteSchedaSISTRI(array("idSISScheda"=>$id_sis_scheda)); 


} catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n\n";
    #echo $s->getLastTransaction();
    var_dump($e->faultcode, $e->faultstring, $e->detail);
}


try {
    # Cerco la scheda appena cancellata
    # NOTA: la ricerca fallisce sempre (se la cancellazione � andata a buon fine)
    #    
    echo "===== GetSchedaSISTRI [$id_sis_scheda]\n";
    $rv1 = $s->GetSchedaSISTRI(
        array( "idSISScheda" => $id_sis_scheda, )); 
    var_dump($rv1);

} catch (SoapFault $e) {
    echo "Scheda non trovata!\n";
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "[{$SISexcept->errorMessage}]\n\n";
}



try {
    # Cerco la movimentazione corrispondente 
    # SORPRESA: anche questa � stata cancellata (per schede produttore)
    #    
    
    echo "===== GetMovimentazioneRifiuto [$id_sis_mov]\n";
    $rv1 = $s->GetMovimentazioneRifiuto(
        array("idSISMovimentazione" => $id_sis_mov)); 
    var_dump($rv1);    
    

} catch (SoapFault $e) {
    echo "Movimentazione non trovata!\n";
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "[{$SISexcept->errorMessage}]\n\n";
}












?>
