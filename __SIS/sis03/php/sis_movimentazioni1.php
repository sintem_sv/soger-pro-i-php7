<?php

//
// Ricerca delle movimentazioni
//

require_once("SIS_SOAP_SSL.php");


$id_sis_registro = "28405";
$CERT		= "../../certificates/08670900151/crt_key.pem";

$s = new SIS_SOAP("antonio.lorusso3151",$CERT);


$filtro_ricerca = new FiltroMovimentazioni;
$filtro_ricerca->dataEoraMovimentazioneInizio	= "2015-12-16T00:00:01.000+02:00";
$filtro_ricerca->dataEoraMovimentazioneFine		= "2015-12-17T07:00:01.000+02:00";
$filtro_ricerca->codiceCerIIILivello = new Catalogo("07.07.01");

try {

    echo "===== GetElencoMovimentazioni\n";
    $rv1 = $s->GetElencoMovimentazioni(
        array(
			"idSISRegistroCrono"   => $id_sis_registro,
            "filtroMovimentazioni" => $filtro_ricerca,
            "startItemPosition"    => new LongNumber(1) # sempre strettamente > 0
        )); 
    
	var_dump($rv1);



} catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n\n";
    echo $s->getLastTransaction();
    var_dump_pre($e->faultcode, $e->faultstring, $e->detail);
    exit();
}


# Estraiamo i risultati

echo "\n\n<h1>Informazioni sui risultati\n--------------------------</h1>\n";

foreach ($rv1->info as $row)
    echo "$row<br />\n";



    foreach ($rv1->movimentazioni as $mov)
        echo " * {$mov->idSIS} ( ID SISTRI: {$mov->idSISTRI})<br />\n";
    echo "\n";



echo "\n";

/*
echo " -> Elementi corrispondenti alla ricerca: {$rv1->itemsIndividuati->long}\n";
echo " -> Elementi visualizzati: da {$rv1->startItemPosition->long} a {$rv1->endItemPosition->long}\n";
echo " -> Totale Elementi visualizzati: {$rv1->itemsRitornati->long}\n";


if ($rv1->itemsIndividuati->long>0) {

    echo "\n ID SIS dei record trovati:\n";

    foreach ($rv1->movimentazioni as $mov)
        echo " * {$mov->idSIS} ( ID SISTRI: {$mov->idSISTRI})<br />\n";
    echo "\n";
}



*/

function var_dump_pre($mixed = null) {
	echo '<pre>';
	var_dump($mixed);
	echo '</pre>';
	return null;
	}

?>
