<?php

//
// Creazione di una scheda SISTRI *Produttore*
//

require_once("SIS_SOAP.php");


// -------------------------------------------------------------------
// DATI IN INGRESSO (obbligatori dove non indicato)

# Catalogo e tipo
$id_sis_registro  = "20768";
$tipo_registro    = new Catalogo("PRD");

$codiceCerIIILivello  = new Catalogo("15.01.03");
$statoFisicoRifiuto   = new Catalogo("03");
$descrizioneRifiuto   = "Imballaggi in legno (pallet)"; // facoltativo
$quantita             = new LongNumber(5000000000);
$id_sis_trasportatore = "7477_SPER"; // in alternativa: specificare le tratte
$id_sis_destinatario  = "7478_SPER";
$operazioneImpianto   = new Catalogo("D4");    // Lagunaggio :)
$contatto = array(
    "nome"      => "Giovanni",
    "cognome"   => "Pautasso",
    "telefono"  => "011123456",
    "email"     => "gpautasso@example.com"  
);



$s = new SIS_SOAP("paolo.vaccaneo3747");

try {

    $scheda = new SchedaSISTRI_Produttore;
    $scheda->codiceCerIIILivello     = $codiceCerIIILivello;
    $scheda->statoFisicoRifiuto      = $statoFisicoRifiuto;
    $scheda->quantita                = $quantita;
    $scheda->descrizioneRifiuto      = $descrizioneRifiuto;
    $scheda->idSISSede_trasportatore = $id_sis_trasportatore;
    $scheda->idSISSede_destinatario  = $id_sis_destinatario;
    $scheda->operazioneImpianto      = $operazioneImpianto; 
    $scheda->flagPrescrizioniParticolari = new Flag(false);
    $scheda->numeroColli                 = new LongNumber(1);
    $scheda->autorizzazione = "paolo.vaccaneo3747";
    $scheda->autorizzazione = "abcd"; //** ?

    $schedabase = new SchedaSISTRI_Base;
    $schedabase->schedaSISTRI_produttore     = $scheda;
    $schedabase->tipoRegCronologico          = $tipo_registro;
    $schedabase->nomePersonaDaContattare     = $contatto["nome"];
    $schedabase->cognomePersonaDaContattare  = $contatto["cognome"];
    #$schedabase->telefonoPersonaDaContattare = $contatto["telefono"]; // facoltativo
    #$schedabase->emailPersonaDaContattare    = $contatto["email"];    // facoltativo
    
    echo "===== PutSchedaSISTRI_Produttore\n";
    $id_sis_scheda = $s->PutSchedaSISTRI_Produttore(array("scheda"=>$schedabase));
    var_dump($id_sis_scheda);

    echo "===== GetMovimentazioneByIdScheda [$id_sis_scheda]\n";
    $rv2 = $s->GetMovimentazioneByIdScheda(
        array( "idSISScheda" => $id_sis_scheda ));
    #var_dump($rv2);
    echo "\n => Movimentazione corrispondente: {$rv2->idSIS}\n\n";


    echo "===== GetSchedaSISTRI [$id_sis_scheda]\n";
    $rv1 = $s->GetSchedaSISTRI(
        array( "idSISScheda" => $id_sis_scheda, )); 
    var_dump($rv1);



} catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n\n";
    echo $s->getLastTransaction();
    var_dump($e->faultcode, $e->faultstring, $e->detail);
}

















?>
