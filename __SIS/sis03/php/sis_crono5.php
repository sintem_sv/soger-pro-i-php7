<?php

//
// Cancellazione di una registrazione cronologica
//

require_once("SIS_SOAP.php");

$id_sis_registro  = "1232941";
$CERT		= "../../certificates/12589530158/crt_key.pem";
$id_sis_reg_crono1 = 6469863; // Carico

$s = new SIS_SOAP("alberto.favero4154", $CERT);

try {

    echo "===== DeleteRegistrazioneCrono [$id_sis_reg_crono] \n";
    $rv = $s->DeleteRegistrazioneCrono(
                array("idSISRegistrazioneCrono"=>$id_sis_reg_crono)); 
    var_dump($rv);


    # Cerco la registrazione appena cancellata
    # NOTA: la ricerca fallisce sempre (se la cancellazione � andata a buon fine)
    #    
    echo "===== GetRegistrazioneCrono [$id_sis_reg_crono] \n";
    $rv1 = $s->GetRegistrazioneCrono(array("idSISRegistrazioneCrono"=>$id_sis_reg_crono)); 

    var_dump($rv1);



} catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n\n";
    #echo $s->getLastTransaction();
    var_dump($e->faultcode, $e->faultstring, $e->detail);
}


?>
