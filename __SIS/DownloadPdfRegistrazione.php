<?php
header("Content-type: application/pdf");
if(isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'],'MSIE'))
	header('Content-Type: application/force-download');
else
	header('Content-Type: application/octet-stream');
header('Content-Length: '.base64_decode($_POST['content']));
header('Content-Disposition: attachment; filename="'.$_POST['filename'].'"');
echo base64_decode($_POST['content']);
?>