<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
require_once("SIS_SOAP_SSL.php");
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");

global $SOGER;

// ID_MOV_F
$ID_MOV_F		= $_POST['ID_MOV_F'];

// idSIS_scheda
$idSIS_scheda	= $_POST['idSIS_scheda'];

$CERT		= "certificates/".$SOGER->UserData['core_impianticodfisc']."/crt_key.pem";
$s			= new SIS_SOAP($SOGER->UserData['core_usersSIS_identity'], $CERT);
$Response	= Array();

try {
	
	$UUID	= $s->generateUUID();
	$rv1	= $s->GetSchedaSISTRI(
				array(  
				"idSISScheda"=>$idSIS_scheda,
				)
			); 

	$scheda					= $s->GetMovimentazioneByIdScheda(array( "idSISScheda" => $idSIS_scheda ));
	$numero_scheda			= $scheda->idSISTRI;	
	
	if($rv1->statoSchedaSistri->idCatalogo=='BO'){

		$rv1	= $s->DeleteSchedaSISTRI(
					array(  
					"idSISScheda"=>$idSIS_scheda,
					)
				); 

		// idSIS_scheda = null in tabella movimenti
		$SQL ="UPDATE user_movimenti_fiscalizzati SET idSIS_scheda=NULL, idSIS_movimentazione=NULL, statoSchedaSistri=NULL, dataSchedaSistri_invio=NULL, numero_scheda=NULL WHERE ID_MOV_F=".$ID_MOV_F;
		$SQL.=" AND user_movimenti_fiscalizzati.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."'";
		$SQL.=" AND user_movimenti_fiscalizzati.".$SOGER->UserData['workmode']."=1";
		$FEDIT->SDBWrite($SQL,true,false);

		$Response['Esito'] = "operazione ultimata, Scheda SISTRI numero $numero_scheda (id $idSIS_scheda) cancellata da SISTRI.";
		$Response['WellDone'] = true;
		
		}
	else{
		$Response['Esito'] = "Impossibile eliminare la Scheda SISTRI numero $numero_scheda (id $idSIS_scheda) da SISTRI: la Scheda SISTRI è ".$rv1->statoSchedaSistri->description;
		$Response['WellDone'] = false;
		}
	} 

catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
	
	// Store error in DB	
	$SQL ="INSERT INTO `debug_sis` (`UUID`, `ID_IMP`, `ID_USR`, `SIS_identity`, `workmode`, `idSIS_sede`, `idSIS_regCrono`, `regCrono_type`, `TIME`, `errorCode`, `errorMessage`, `RequestHeaders`, `Request`, `ResponseHeaders`, `Response`) ";
	$SQL.="VALUES ('".$UUID."', '".$SOGER->UserData['core_usersID_IMP']."', '".$SOGER->UserData['core_usersID_USR']."', '".$SOGER->UserData['core_usersSIS_identity']."', '".$SOGER->UserData['workmode']."', '".$SOGER->UserData['core_usersidSIS_sede']."', '".$SOGER->UserData['core_usersidSIS_regCrono']."', '".$SOGER->UserData['core_usersregCrono_type']."', CURRENT_TIMESTAMP, '".addslashes($SISexcept->errorCode)."', '".addslashes($SISexcept->errorMessage)."', '".addslashes($s->__getLastRequestHeaders())."', '".addslashes($s->__getLastRequest())."', '".addslashes($s->__getLastResponseHeaders())."', '".addslashes($s->__getLastResponse())."');";
    $FEDIT->SDBWrite($SQL,true,false);

	// Notice error
	$errorMessage = explode("]:", $SISexcept->errorMessage);
    
	$Response['Esito'] = $errorMessage[1].".<br /><br />Se non è nota la causa dell'errore, aprire un ticket nella sezione \"Interoperabilità\" e descrivere la problematica riportando l'ID della transazione ".$UUID;
	$Response['WellDone'] = false;

	}
$Response = array_map('utf8_encode', $Response);
echo json_encode($Response);


require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>