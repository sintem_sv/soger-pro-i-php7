<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>

<HEAD>
	<TITLE>Upload public key</TITLE>
</HEAD>

<BODY>

	<?php if(!isset($_POST['submit_pk'])){ ?>
	<form id="form_up_pk" enctype="multipart/form-data" action="UploadPK.php" method="POST">
		<fieldset>
			<legend>Upload private key</legend>
				<input name="MAX_FILE_SIZE" value="1048576" type="hidden" />
				<input name="pk_file" id="pk_file" type="file" />
				<input type="submit" name="submit_pk" value="Invia" id="submit_pk" />
		</fieldset>	
	</form>
	
	<?php }else{ 


	//die(var_dump($_FILES));

	$msg="";

	do {
		if (is_uploaded_file($_FILES['pk_file']['tmp_name'])) {

			// Directory contenente file necessari all'autenticazione SIS
			$dir = 'certificates/'.$SOGER->UserData['core_impianticodfisc'].'/';
			chdir($dir);
			
			// Sposto il file nella cartella da me desiderata
			if (!move_uploaded_file($_FILES['pk_file']['tmp_name'], $_FILES['pk_file']['name'])) {
				$msg = "Errore nel caricamento, verificare i permessi per la cartella 'certificates'. \\n";
				break;
				}

			// Copio il contenuto di certificato.cer
			$handle1	= fopen("certificato.cer","r"); 
			$certificato= fread($handle1, filesize("certificato.cer")); 
			fclose ($handle1);

			// Copio il contenuto della private key
			$handle2	= fopen($_FILES['pk_file']['name'],"r"); 
			$privatekey = fread($handle2, filesize($_FILES['pk_file']['name'])); 
			fclose ($handle2);

			// Creo nuovo file concatenazione di certificato e private key
			$filename	= "crt_key.pem";
			if(file_exists($filename)) unlink($filename);
			$handle3	= fopen($filename, 'a');
			fwrite($handle3, $certificato."\r\n\r\n".$privatekey);

			}
		} 
	while (false);

	if($msg!=""){
		echo "<b>Si sono verificati i seguenti errori:</b><br />".$msg;
		}
	else{
		echo "<b>Public key caricata correttamente</b>\n";
		}
			
	chdir("../../");

	require_once("../__includes/COMMON_sleepSoger.php");
	} ?>

</BODY>

</HTML>

<?php
require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>