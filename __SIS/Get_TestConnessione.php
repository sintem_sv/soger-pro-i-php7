<?php

/*
error_reporting(E_ALL);
ini_set("display_errors", 1);
*/
session_start();
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("SIS_SOAP_SSL.php");

$CERT		= "certificates/".$SOGER->UserData['core_impianticodfisc']."/crt_key.pem";

$Response	= Array();

try{
	$SIS		= new SIS_SOAP($SOGER->UserData['core_usersSIS_identity'], $CERT);
	$SIS_version= $SIS->GetVersioneSIS();
	$P_S		= explode("; ", $SIS_version);
	$Protocollo	= explode(": ", $P_S[0]);
	$Software	= explode(": ", $P_S[1]);

	$Response['Protocollo']		= $Protocollo[1];
	$Response['Versione']		= $Software[1];
	$Response['Esito']			= "Connessione stabilita !";
	}
catch(SoapFault $SIS_error){
	$Response['Protocollo']		= "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n";
	$Response['Versione']		= "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n";
	$Response['Esito']			= SIS_SOAP::getSISException($SIS_error);
	}

echo json_encode($Response);

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>