<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("SIS_SOAP_SSL.php");

$CERT		= "certificates/".$SOGER->UserData['core_impianticodfisc']."/crt_key.pem";
$s			= new SIS_SOAP($SOGER->UserData['core_usersSIS_identity'], $CERT);
$Response	= Array();

$SQL = "SELECT quantita, FKEumis FROM user_movimenti_fiscalizzati WHERE ID_MOV_F=".$_POST['ID_MOV_F'].";";
$FEDIT->SdbRead($SQL,"DbRecordSet",true,false);

try{
	$UUID				= $s->generateUUID();
	$codiceCausale		= new Catalogo('EM');
	$annotazioniCausale	= 'Verifica annullabilita';

	$dati	= $s->RecuperaSchedaPerAnnullamentoConCausale(array(
		"idSISScheda"=>$_POST['id'],
		"annotazioniCausale"=>$annotazioniCausale,
		"codiceCausale"=>$codiceCausale)
		);

	$Response['codice_stato'] = 1;
	$Response['quantita'] = $FEDIT->DbRecordSet[0]['quantita']." ".$FEDIT->DbRecordSet[0]['FKEumis'];

	}
catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);

	// Store error in DB	
	$SQL ="INSERT INTO `debug_sis` (`UUID`, `ID_IMP`, `ID_USR`, `SIS_identity`, `workmode`, `idSIS_sede`, `idSIS_regCrono`, `regCrono_type`, `TIME`, `errorCode`, `errorMessage`, `RequestHeaders`, `Request`, `ResponseHeaders`, `Response`) ";
	$SQL.="VALUES ('".$UUID."', '".$SOGER->UserData['core_usersID_IMP']."', '".$SOGER->UserData['core_usersID_USR']."', '".$SOGER->UserData['core_usersSIS_identity']."', '".$SOGER->UserData['workmode']."', '".$SOGER->UserData['core_usersidSIS_sede']."', '".$SOGER->UserData['core_usersidSIS_regCrono']."', '".$SOGER->UserData['core_usersregCrono_type']."', CURRENT_TIMESTAMP, '".addslashes($SISexcept->errorCode)."', '".addslashes($SISexcept->errorMessage)."', '".addslashes($s->__getLastRequestHeaders())."', '".addslashes($s->__getLastRequest())."', '".addslashes($s->__getLastResponseHeaders())."', '".addslashes($s->__getLastResponse())."');";
    $FEDIT->SDBWrite($SQL,true,false);

	$Response['codice_stato'] = 0;
	}

echo json_encode($Response);


require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>