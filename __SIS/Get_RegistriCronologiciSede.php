<?php
session_start();
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__SIS/SIS_SOAP_SSL.php");

$CERT		= "certificates/".$SOGER->UserData['core_impianticodfisc']."/crt_key.pem";
$SIS		= new SIS_SOAP($SOGER->UserData['core_usersSIS_identity'], $CERT);


try{

	$Registri	= $SIS->GetRegistroCronologico(Array("idSISSede"=>$_POST['idSis_sede']));
	$SelectRegistri=Array();

	foreach($Registri AS $registro){
			
			$SQL="SELECT DESCRIZIONE_TIPO_REG_CRONO AS REGISTRO FROM sis_TIPI_REG_CRONOLOGICO WHERE ID_TIPO_REG_CRONOLOGICO='".$registro->tipoRegCronologico->idCatalogo."';";
			$FEDIT->SdbRead($SQL,"Registro");

			$CurrentRegistro=Array();
			$CurrentRegistro['idSIS']		= $registro->idSIS;
			$CurrentRegistro['codice']		= $registro->codiceRegistroCronologico;
			$CurrentRegistro['descrizione']	= $FEDIT->Registro[0]['REGISTRO'];
			if(!is_null($SOGER->UserData['idSIS_regCrono']) AND $SOGER->UserData['idSIS_regCrono']==$registro->idSIS)
				$CurrentRegistro['selected']	= true;
			else
				$CurrentRegistro['selected']	= false;
		
			array_push($SelectRegistri, $CurrentRegistro);
			unset($CurrentRegistro);

			}

	echo json_encode($SelectRegistri);
	}
catch (SoapFault $e) {
	echo var_dump($e);
	}


require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>