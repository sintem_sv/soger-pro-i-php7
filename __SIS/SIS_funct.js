$(document).ready(function(){

/*##########################################################
#
# UPLOAD PRIVATE KEY DA PAGINA INIZIALE
#
##########################################################*/

$( "#jqpopup_UploadPrivateKey" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	width: 400, 
	buttons: {
		"Chiudi": function() {
			$( this ).dialog( "close" );
			}
		}
	});

$( "#jqpopup_TestCert" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	width: 400, 
	buttons: {
		"Chiudi": function() {
			$( this ).dialog( "close" );
			}
		}
	});


$("#UpPk").click(function(){
	
	var gotCertificato=$("#gotCertificato").val();
		
	if(gotCertificato=='0')
		alert("Impossibile caricare private key, il certificato di interoperabilita' non e' presente sul server.");
	else{		
		// Show dialog box
		$( "#jqpopup_UploadPrivateKey" ).dialog("open");
		}

	});


$("#TestCert").click(function(){

	var gotIdentity		= $("#gotIdentity").val();
	var gotCertificato	= $("#gotCertificato").val();
	var gotPrivateKey	= $("#gotPk").val();
	var gotCF			= $("#gotCF").val();

	if(gotIdentity==0)		$("#ESITO_gotIdentity").html('<img src=\'__css/cross.png\' />'); else $("#ESITO_gotIdentity").html('<img src=\'__css/tick.png\' />');
	if(gotCertificato==0)	$("#ESITO_gotCertificato").html('<img src=\'__css/cross.png\' />'); else $("#ESITO_gotCertificato").html('<img src=\'__css/tick.png\' />');
	if(gotPrivateKey==0)	$("#ESITO_gotPk").html('<img src=\'__css/cross.png\' />'); else $("#ESITO_gotPk").html('<img src=\'__css/tick.png\' />');
	if(gotCF==0)			$("#ESITO_gotCF").html('<img src=\'__css/cross.png\' />'); else $("#ESITO_gotCF").html('<img src=\'__css/tick.png\' />');

	$('#ConnectionStatus').find("tr:gt(0)").remove();
	$('#ConnectionStatus tr:last').after('<tr id="Connecting"><td style="width:25%;text-align:right;"><span id="icona_pc"><img src="__css/icona_pc.jpg" /></span></td><td colspan="2" style="width:50%;text-align:center;"><span id="progress_bar"><img src="__css/progress_bar.gif" /></span></td><td style="width:25%;text-align:left;"><span id="icona_sistri"><img src="__css/SIS_logo_on.gif" /></span></td></tr>');

	if(gotIdentity==1 && gotCertificato==1 && gotPrivateKey==1 && gotCF==1){
		
		$.post('__SIS/Get_TestConnessione.php', function(phpResponse){
			
			//alert(dump(phpResponse));
			
			$('#ConnectionStatus #Connecting').remove();

			$('#ConnectionStatus tr:last').after('<tr><td style="width:50%;" colspan="2">Esito</td><td style="width:50%;text-align:right;" colspan="2">'+phpResponse.Esito+'</td></tr>');
			$('#ConnectionStatus tr:last').after('<tr><td style="width:50%;" colspan="2">Protocollo</td><td style="width:50%;text-align:right;" colspan="2">'+phpResponse.Protocollo+'</td></tr>');
			$('#ConnectionStatus tr:last').after('<tr><td style="width:50%;" colspan="2">Versione</td><td style="width:50%;text-align:right;" colspan="2">'+phpResponse.Versione+'</td></tr>');
			
			},"json");

		}
	else{
		$('#ConnectionStatus #Connecting').remove();
		$('#ConnectionStatus tr:last').after('<tr><td colspan="4">Impossibile stabilire una connessione con SISTRI. Verifica i parametri di configurazione</td></tr>');
		}

	// Show dialog box
	$( "#jqpopup_TestCert" ).dialog("open");

	});





/*##########################################################
#
# POPUP PER SELEZIONE SEDE DI RIFERIMENTO E REG. CRONOLOGICO
#
##########################################################*/

$( "#jqpopup_EditSedeAndRegistro" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	width: 400, 
	buttons: {
		"Salva selezione": function() {
				
			var idSIS_sede		= $("#Select_Sede").val();
			var idSIS_regCrono	= $('#Select_RegistroCronologico').val();
			var descrizione_sede= $('#Select_Sede option:selected').html()
			
			$.post('__SIS/Set_SedeAndRegistro.php', {idSIS_sede:idSIS_sede, descrizione_sede:descrizione_sede, idSIS_regCrono:idSIS_regCrono}, function(phpResponse){

				$("#DIV_AjaxCall").removeClass();
				$("#DIV_AjaxCall").addClass('ui-corner-all');
				$("#SPAN_AjaxCall").removeClass();
				$("#SPAN_AjaxCallTxt").html('');

				$("#DIV_AjaxCall").addClass(phpResponse.classe);
				$("#SPAN_AjaxCall").addClass(phpResponse.icon);
				$("#SPAN_AjaxCallTxt").html(phpResponse.message);

				$("#DescrizioneSede").html(descrizione_sede);
				$("#RegCrono").html(idSIS_regCrono);

				}, "json");
			},
		"Chiudi": function() {

			$("#DIV_AjaxCall").removeClass();
			$("#DIV_AjaxCall").addClass('ui-corner-all');
			$("#SPAN_AjaxCall").removeClass();
			$("#SPAN_AjaxCallTxt").html('');
			$( this ).dialog( "close" );

			}
		}
	});

$("#EditSedeAndRegistro").click(function(){

	// disabilito Select_Sede
	$('#Select_Sede').prop('disabled', 'disabled');
	$('#Select_RegistroCronologico').prop('disabled', 'disabled');
	$('#Select_RegistroCronologico option:not(:first)').remove();
	$('#AjaxStatus_Sede').html('<img src="__css/progress_bar.gif" />');

	// chiamata ajax per leggere le sedi
	$.post('__SIS/GetSediAzienda.php', {}, function(phpResponse){

		// elimino tutte le option successive alla prima
		$('#Select_Sede option:not(:first)').remove();
			
		// aggiungo option alla select
		for(s=0; s<phpResponse.length; s++){
			var sede=phpResponse[s];
			$("#Select_Sede").append('<option value='+sede.idSIS+'>'+sede.nomeSede+' - '+sede.indirizzo+'</option>');
			}
		
		// abilito Select_Sede
		$('#Select_Sede').prop('disabled', false);
		$('#AjaxStatus_Sede').html('');

		},"json");

	// Disable dialog box buttons
	$(".ui-dialog-buttonpane button:contains('Salva selezione')").button("disable");

	// Show dialog box
	$( "#jqpopup_EditSedeAndRegistro" ).dialog("open");

	});



/*##########################################################
#
# ALLA SELEZIONE DELLA SEDE, LEGGO REGISTRI CRONOLOGICI
#
##########################################################*/


$("#jqpopup_EditSedeAndRegistro").delegate("#Select_Sede", "change", function(){

	var idSis_sede = $("#Select_Sede").val();

	$('#Select_RegistroCronologico option:first').prop('selected', 'selected');
	$('#Select_RegistroCronologico').prop('disabled', 'disabled');
	$('#AjaxStatus_Registro').html('<img src=\"__css/progress_bar.gif\" />');

	if(idSis_sede!=0){
		
		// chiamata ajax per leggere i registri
		$.post('__SIS/Get_RegistriCronologiciSede.php', {idSis_sede:idSis_sede}, function(phpResponse){
			
			if(phpResponse!=''){

				// elimino tutte le option successive alla prima
				$('#Select_RegistroCronologico option:not(:first)').remove();

				// aggiungo option alla select
				for(r=0; r<phpResponse.length; r++){
					var registro=phpResponse[r];
					$("#Select_RegistroCronologico").append('<option value='+registro.codice+'>'+registro.codice+' '+registro.descrizione+'</option>');
					}		
				}
			
			// abilito select registro
			$('#Select_RegistroCronologico').prop('disabled', false);
			$('#AjaxStatus_Registro').html('');

			// Enable dialog box buttons
			if($("#Select_RegistroCronologico").val()!=0){
				$(".ui-dialog-buttonpane button:contains('Salva selezione')").button("enable");
				}
			
			},"json");

		// chiamata ajax per var in oggetto $SOGER
		// idSIS_sede		= $('#Select_Sede').val();
		// $.post('__SIS/Set_Sede.php', {idSIS_sede:idSIS_sede});

		} 
	else{
		// disabilito select
		$('#Select_RegistroCronologico').prop('disabled', 'disabled');
		$('#Select_RegistroCronologico option:first').attr('selected', 'selected');
		$('#AjaxStatus_Registro').html('');
		
		// Disable dialog box buttons
		$(".ui-dialog-buttonpane button:contains('Salva selezione')").button("disable");
		}
		

	});





$("#Select_RegistroCronologico").change(function(){
	if($("#Select_RegistroCronologico").val()!=0){
		// Enable dialog box buttons
		$(".ui-dialog-buttonpane button:contains('Salva selezione')").button("enable");
		}
	else{
		// Disable dialog box buttons
		$(".ui-dialog-buttonpane button:contains('Salva selezione')").button("disable");
		}
	});





/*##########################################################
#
# MENU' GESTIONE ANAGRAFICHE - ALLINEAMENTO CON SISTRI - SEDI LEGALI
#
##########################################################*/

	$( "#jqpopup_AllineamentoSISTRI_STEP1" ).dialog({
		modal: true,
		autoOpen: false,
		resizable: false,
		width: 480, 
		buttons: {
			"Conferma": function() {

				$( this ).dialog( "close" );

				// Disable dialog box buttons
				$(".ui-dialog-buttonpane button:contains('Visualizza esito')").button("disable");
				$(".ui-dialog-buttonpane button:contains('Chiudi')").button("disable");

				// Show dialog box - Step 2
				$( "#TD_user_aziende_produttori" ).html('<img src="__css/progress_bar.gif" />');
				$( "#TD_user_aziende_trasportatori" ).html('<img src="__css/progress_bar.gif" />');
				$( "#TD_user_aziende_destinatari" ).html('<img src="__css/progress_bar.gif" />');
				$( "#TD_user_aziende_intermediari" ).html('<img src="__css/progress_bar.gif" />');
				$( "#jqpopup_AllineamentoSISTRI_STEP2" ).dialog("open");

				var TablesToSync	= new Array('user_aziende_produttori','user_aziende_trasportatori','user_aziende_destinatari','user_aziende_intermediari');
				var PrimaryKey		= new Array('ID_AZP', 'ID_AZT', 'ID_AZD', 'ID_AZI');

				for(var t=0;t<TablesToSync.length;t++){
					$.ajax({
						async:false,
						type: 'POST',
						url: '__SIS/SincroAnagrafiche.php',
						data: {table:TablesToSync[t], primary:PrimaryKey[t]},
						success: function(data) {
							$('#TD_'+data).text('aggiornamento completato');
							}
						//beforeSend:function(){},
						//error:function(objXMLHttpRequest){}
						});
					}

				// Enable dialog box buttons
				$(".ui-dialog-buttonpane button:contains('Visualizza esito')").button("enable");
				$(".ui-dialog-buttonpane button:contains('Chiudi')").button("enable");

				},
			"Annulla": function() {
				$( this ).dialog( "close" );
				}
			}
		});

	$( "#jqpopup_AllineamentoSISTRI_STEP2" ).dialog({
		modal: true,
		autoOpen: false,
		resizable: false,
		width: 480, 
		buttons: {
			"Visualizza esito": function() {
				window.location.href = '__SIS/Get_SyncroLog.php';
				},
			"Chiudi": function() {
				// Unset $_SESSION['ID_USR']['SYNC_LOG']
				$.post('__SIS/Del_SyncroLog.php', {}, function(phpResponse){});
				$( this ).dialog( "close" );
				}	
			}
		});


	$('#AllineamentoSISTRI').click(function(){

		// Show dialog box - Step 1
		$( "#jqpopup_AllineamentoSISTRI_STEP1" ).dialog("open");

		});






/*##########################################################
#
# MENU' GESTIONE ANAGRAFICHE - ALLINEAMENTO CON SISTRI - UNITA' LOCALI
#
##########################################################*/

$( "#jqpopup_SyncroImpianto" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	width: 480,
	position: { my: "center", at: "center", of: document },
	buttons: {
		"Salva selezione": function() {

			$("#DIV_AjaxCall").removeClass();
			$("#DIV_AjaxCall").addClass('ui-corner-all');
			$("#SPAN_AjaxCall").removeClass();
			$("#SPAN_AjaxCallTxt").html('');
			
			var idSIS			= $("#Select_UnitaLocale option:selected").val();
			var versione		= $("#Select_UnitaLocale option:selected").attr("versione");
			var tab_impianto	= $("#Select_UnitaLocale").attr('tab_impianto');
			var id_impianto		= $("#Select_UnitaLocale").attr('id_impianto');
			
			$.post('__SIS/Set_idSISImpianto.php', {tab_impianto:tab_impianto, id_impianto:id_impianto, idSIS:idSIS, versione:versione}, function(phpResponse){
				
				$("#DIV_AjaxCall").addClass(phpResponse.classe);
				$("#SPAN_AjaxCall").addClass(phpResponse.icon);
				$("#SPAN_AjaxCallTxt").html(phpResponse.message);
				
				$('[id="'+tab_impianto+':'+id_impianto+'"]').attr('src', '__css/SIS_sincronizza_on.gif');
				$('[id="'+tab_impianto+':'+id_impianto+'"]').attr('current_idSIS', idSIS);

				}, "json");

			},

		"Reset selezione": function() {

			$("#Select_UnitaLocale").val(0);
			$(".tr_details").remove();
			$("#DIV_AjaxCall").removeClass();
			$("#DIV_AjaxCall").addClass('ui-corner-all');
			$("#SPAN_AjaxCall").removeClass();
			$("#SPAN_AjaxCallTxt").html('');
			
			var idSIS			= 0;
			var versione		= 0;
			var tab_impianto	= $("#Select_UnitaLocale").attr('tab_impianto');
			var id_impianto		= $("#Select_UnitaLocale").attr('id_impianto');
			
			$.post('__SIS/Set_idSISImpianto.php', {tab_impianto:tab_impianto, id_impianto:id_impianto, idSIS:idSIS, versione:versione}, function(phpResponse){
				
				$("#DIV_AjaxCall").addClass(phpResponse.classe);
				$("#SPAN_AjaxCall").addClass(phpResponse.icon);
				$("#SPAN_AjaxCallTxt").html(phpResponse.message);
				
				$('[id="'+tab_impianto+':'+id_impianto+'"]').attr('src', '__css/SIS_sincronizza_off.gif');
				$('[id="'+tab_impianto+':'+id_impianto+'"]').attr('current_idSIS', '');

				}, "json");

			},

		"Chiudi": function() {
			$(".tr_details").remove();
			$("#DIV_AjaxCall").removeClass();
			$("#DIV_AjaxCall").addClass('ui-corner-all');
			$("#SPAN_AjaxCall").removeClass();
			$("#SPAN_AjaxCallTxt").html('');
			$( this ).dialog( "close" );
			}
		}

	});


/*##########################################################
#
# DIALOG PER MATCH IMPIANTO SOGER - UL SISTRI
#
##########################################################*/
	
var SediDetails		= new Array();

$(document).on('click', '.IMG_SyncroImpianto', function(){

	var $tr				= $(this).closest('tr');
	var indirizzo		= $tr.find('td:eq(0)').text();
	var comune			= $tr.find('td:eq(1)').text();
	var id				= $(this).attr('id');
	var current_idSIS	= $(this).attr('current_idSIS');

	// Impianto So.Ge.R. Pro
	$("#impianto_pro").html(indirizzo + " - " + comune);

	// Delete option not first
	$('#Select_UnitaLocale option:not(:first)').remove();
	$('#Select_UnitaLocale').prop('disabled', 'disabled');

	// Show dialog box
	$( "#jqpopup_SyncroImpianto" ).dialog("open");
	// Disable dialog box buttons
	$(".ui-dialog-buttonpane button:contains('Salva selezione')").button("disable");

	// show loading image
	$('#AjaxStatus_UnitaLocale').html('<img src="__css/progress_bar.gif" />');

	// Ajax request to populate Select
	var params			= id.split(':');
	var tab_impianto	= params[0];
	var id_impianto		= params[1];
	
	$('#Select_UnitaLocale').attr('tab_impianto', tab_impianto);
	$('#Select_UnitaLocale').attr('id_impianto', id_impianto);	

	$.post('__SIS/Get_Azienda.php', {tab_impianto:tab_impianto, id_impianto:id_impianto}, function(phpResponse){

		// aggiungo option alla select
		for(r=0; r<phpResponse.length; r++){

			var counter				= r+1;
			var unita_locale		= phpResponse[r];
			var Sede_Sottocategorie = '';
			var selected			= '';
			
			if(unita_locale.idSIS==current_idSIS){
				selected			= 'selected';
				}

			$("#Select_UnitaLocale").append('<option '+selected+' op='+r+' value='+unita_locale.idSIS+' versione='+unita_locale.versione+'>'+counter+"] "+unita_locale.indirizzo+' - '+unita_locale.comune+'</option>');
			
			for(var s=0;s<unita_locale.sottocategorie.length;s++){
				Sede_Sottocategorie+="- "+unita_locale.sottocategorie[s]+"<br />";
				}

			//Sede_Sottocategorie+='</ul>';
			
			var Detail	 = '<tr class="tr_details"><th>Tipo di sede:</th><td>'+unita_locale.tipoSede+'</td></tr>';
			Detail		+= '<tr class="tr_details"><th>Sottocategorie:</th><td>'+Sede_Sottocategorie+'</td></tr>';

			SediDetails.push(Detail);

			}

		if(current_idSIS!=""){
			var OP = $("#Select_UnitaLocale option:selected").attr("op");
			$("#TABLE_MatchSedi tr:last").after(SediDetails[OP]);
			}


		$('#Select_UnitaLocale').prop('disabled', false);
		$('#AjaxStatus_UnitaLocale').html('');

		},"json");

	});



/*##########################################################
#
# CHANGE UL - MOSTRO DETAILS
#
##########################################################*/


$('#Select_UnitaLocale').change(function(){

	$(".tr_details").remove();

	if($("#Select_UnitaLocale").val()!='0'){
		var OP = $("#Select_UnitaLocale option:selected").attr("op");
		$("#TABLE_MatchSedi tr:last").after(SediDetails[OP]);
		// Enable dialog box buttons
		$(".ui-dialog-buttonpane button:contains('Salva selezione')").button("enable");
		}

	});








/*##########################################################
#
# ASK SUPER USER PIN TO ACCESS SIS
#
##########################################################*/


$( "#Popup_superpwd" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	width: 400, 
	buttons: {
		"Invia PIN": function() {
			check_superpwd();
			},
		"Chiudi": function() {
			$( this ).dialog( "close" );
			}
		}
	});

	
$('.jq_PermitSISTRI').click(function(){
	$( "#NMOV" ).val($(this).attr('NMOV'));
	$( "#ID_MOV_F" ).val($(this).attr('ID_MOV_F'));
	$( "#Popup_superpwd" ).dialog("open");
	});



/*##########################################################
#
# PREVIEW REGISTRO C/S
#
##########################################################*/


$( "#Popup_AnteprimaCS" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	width: 400, 
	buttons: {
		"Conferma": function() {
			AutorizzaInvio($('#NMOV').val(), $('#RegType').val());
			},
		"Chiudi": function() {
			$( this ).dialog( "close" );
			}
		}
	});





/*#########################################################
#
# INVIO REGISTRAZIONE CRONOLOGICA DI CARICO
#
#########################################################*/


$( "#Popup_InteroperabilitaSistri" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	width: 480, 
	buttons: {
		"Chiudi": function() {
			location.reload();
			$( this ).dialog( "close" );
			}
		}
	});

$( "#Popup_LetturaSchedaMovimentazione" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	width: 480, 
	buttons: {
		"Chiudi": function() {
			//location.reload();
			$( this ).dialog( "close" );
			}
		}
	});

$( "#Popup_LetturaRegistrazione" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	width: 480, 
	buttons: {
		"Chiudi": function() {
			//location.reload();
			$( this ).dialog( "close" );
			}
		}
	});

$( "#Popup_InvioRegCrono" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	closeOnEscape: false,
	beforeclose: function (event, ui) { return false; },
    dialogClass: "noclose",
	width: 480
	});

$( "#Popup_InvioScheda" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	closeOnEscape: false,
	beforeclose: function (event, ui) { return false; },
    dialogClass: "noclose",
	width: 480
	});

$( "#Popup_EliminaRegCrono" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	closeOnEscape: false,
	beforeclose: function (event, ui) { return false; },
    dialogClass: "noclose",
	width: 480
	});

$( "#Popup_EliminaScheda" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	closeOnEscape: false,
	beforeclose: function (event, ui) { return false; },
    dialogClass: "noclose",
	width: 480
	});

$( "#Popup_FirmaRegCrono" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	closeOnEscape: false,
	beforeclose: function (event, ui) { return false; },
    dialogClass: "noclose",
	width: 610
	});

$( "#Popup_AnnullaRegCrono" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	closeOnEscape: false,
	beforeclose: function (event, ui) { return false; },
    dialogClass: "noclose",
	width: 610
	});

$( "#Popup_AnnullaScheda" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	closeOnEscape: false,
	beforeclose: function (event, ui) { return false; },
    dialogClass: "noclose",
	width: 610
	});

$( "#Popup_FirmaScheda" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	closeOnEscape: false,
	beforeclose: function (event, ui) { return false; },
    dialogClass: "noclose",
	width: 610
	});

$( "#Popup_PdfRegistrazione" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	closeOnEscape: false,
	beforeclose: function (event, ui) { return false; },
    dialogClass: "noclose",
	width: 480
	});

$( "#Popup_PdfMovimentazione" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	closeOnEscape: false,
	beforeclose: function (event, ui) { return false; },
    dialogClass: "noclose",
	width: 480
	});

$( "#Popup_CompletamentoScaricoFIR" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	closeOnEscape: false,
	beforeclose: function (event, ui) { return false; },
    dialogClass: "noclose",
	width: 480,
	buttons: {
		"Chiudi": function() {
			$( this ).dialog( "close" );
			}
		}
	});

$( "#Popup_CausaleAnnullamentoRegCrono" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	closeOnEscape: false,
	beforeclose: function (event, ui) { return false; },
    dialogClass: "noclose",
	width: 480,
	buttons: {
		"Conferma": function() {
			ConfermaAnnullamento();
			},
		"Chiudi": function() {
			$( this ).dialog( "close" );
			}
		}
	});

$( "#Popup_CausaleAnnullamentoScheda" ).dialog({
	modal: true,
	autoOpen: false,
	resizable: false,
	closeOnEscape: false,
	beforeclose: function (event, ui) { return false; },
    dialogClass: "noclose",
	width: 480,
	buttons: {
		"Conferma": function() {
			ConfermaAnnullamento();
			},
		"Chiudi": function() {
			$( this ).dialog( "close" );
			}
		}
	});


$('td .InteroperabilitaSistri').click(function(){

	var ID_MOV_F				= $(this).attr('ID_MOV_F');
	var TIPO					= $(this).attr('TIPO');
	var TIPO_SISTRI				= $("#TIPO_SISTRI").val();    // 'scheda' o 'crono'
	var idSIS					= $(this).attr('idSIS');
	var idSIS_scheda			= $(this).attr('idSIS_scheda');
	var idSIS_movimentazione	= $(this).attr('idSIS_movimentazione');
	var statoRegistrazioniCrono = $(this).attr('statoRegistrazioniCrono');
	var statoSchedaSistri		= $(this).attr('statoSchedaSistri');
	var SenzaTrasporto			= $(this).attr('SenzaTrasporto');
	var statoSchedaSistri_trasportatore		= $(this).attr('statoSchedaSistri_trasportatore');
	var SIS_invia				= $(this).attr('SIS_invia');
	var SIS_elimina				= $(this).attr('SIS_elimina');
	var SIS_firma				= $(this).attr('SIS_firma');
	var SIS_annulla				= $(this).attr('SIS_annulla');
	var SIS_download			= $(this).attr('SIS_download');

	//alert("SenzaTrasporto: "+SenzaTrasporto+"\nID_MOVIMENTO: "+ID_MOV_F+"\n"+"idSIS: "+idSIS+"\n"+"idSIS_scheda: "+idSIS_scheda+"\n"+"idSIS_movimentazione: "+idSIS_movimentazione+"\n"+"statoRegistrazioniCrono: "+statoRegistrazioniCrono+"\n"+"statoSchedaSistri: "+statoSchedaSistri+"\n"+"TIPO: "+TIPO+"\n"+"TIPO_SISTRI: "+TIPO_SISTRI+"\n"+"SIS_invia: "+SIS_invia+"\n"+"SIS_elimina: "+SIS_elimina+"\n"+"SIS_firma: "+SIS_firma+"\n"+"SIS_annulla: "+SIS_annulla+"\n"+"SIS_download: "+SIS_download+"\n");

	$("#TIPO").val(TIPO);
	$("#ID_MOV_F").val(ID_MOV_F);
	$("#idSIS").val(idSIS);
	$("#idSIS_scheda").val(idSIS_scheda);
	$("#idSIS_movimentazione").val(idSIS_movimentazione);
	$("#statoRegistrazioniCrono").val(statoRegistrazioniCrono);
	$("#statoSchedaSistri").val(statoSchedaSistri);
	$("#statoSchedaSistri_trasportatore").val(statoSchedaSistri_trasportatore);
	$("#SenzaTrasporto").val(SenzaTrasporto);
	$("#SIS_invia img").attr("src", "__css/SIS_invia_"+SIS_invia+".gif");
	$("#SIS_elimina img").attr("src", "__css/SIS_elimina_"+SIS_elimina+".gif");
	$("#SIS_firma img").attr("src", "__css/SIS_firma_"+SIS_firma+".gif");
	$("#SIS_annulla img").attr("src", "__css/SIS_annulla_"+SIS_annulla+".gif");
	$("#SIS_download img").attr("src", "__css/SIS_download_"+SIS_download+".gif");

	// Show dialog box
	$( "#Popup_InteroperabilitaSistri" ).dialog("open");
	});


$("#SIS_invia").click(function(){

	var TIPO			= $("#TIPO").val();
	var TIPO_SISTRI		= $("#TIPO_SISTRI").val();
	var SenzaTrasporto	= $("#SenzaTrasporto").val();

	if(TIPO_SISTRI=='crono'){
		var OBJ2CHECK	= "#idSIS";
		var OBJ_desc	= "registrazione cronologica";
		}

	if(TIPO_SISTRI=='scheda'){
		var OBJ2CHECK	= "#idSIS_scheda";
		var OBJ_desc	= "scheda di movimentazione";
		}
	
	//alert(TIPO + " " + TIPO_SISTRI + " " + $("#statoSchedaSistri").val() + " " + SenzaTrasporto);

	if(TIPO=='S' && TIPO_SISTRI=='crono' && $("#statoSchedaSistri").val()!='FIRMATA' && SenzaTrasporto=='0'){
		alert("Impossibile inviare la "+OBJ_desc+": la relativa Scheda SISTRI non e' ancora stata firmata.");
		}
	else if($(OBJ2CHECK).val()=='null'){

		if(window.confirm("Il movimento verra' ora trasmesso a SISTRI: confermi?")){
	
			$( "#Popup_InteroperabilitaSistri" ).dialog("close");

			var ID_MOV_F	= $("#ID_MOV_F").val();
			var TIPO		= $("#TIPO").val();

			switch(TIPO){
				
				case 'C':
					
					// INVIO REGISTRAZIONE CRONOLOGICA CARICO
					$('#Popup_InvioRegCrono .progress_bar').html('<img src="__css/progress_bar.gif" />');
					$('#PutRegCronoStatus_txt').html('operazione in corso...');
					$("#Popup_InvioRegCrono" ).dialog("open");

					$.post('__SIS/PutRegistrazioneCronoCarico.php', {ID_MOV_F:ID_MOV_F}, function(phpResponse){
						
						$('#Popup_InvioRegCrono .progress_bar').html('');
						$('#PutRegCronoStatus_txt').html(phpResponse.Esito);

						if(phpResponse.WellDone){
							// Set hidden field #idSIS value
							$(OBJ2CHECK).val(phpResponse.idSIS);
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('idSIS', phpResponse.idSIS);
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('statoRegistrazioniCrono', phpResponse.statoRegistrazioniCrono);
							$("td#"+ID_MOV_F+" .SIS_status_span").html(phpResponse.statoRegistrazioniCrono);
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_invia', 'off');
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_elimina', 'on');
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_firma', 'on');
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_download', 'off');

							// Block / unblock other SIS functions
							$("#SIS_invia img").attr("src", "__css/SIS_invia_off.gif");
							$("#SIS_elimina img").attr("src", "__css/SIS_elimina_on.gif");
							$("#SIS_firma img").attr("src", "__css/SIS_firma_on.gif");
							$("#SIS_download img").attr("src", "__css/SIS_download_off.gif");
							}
						
						// Enable dialog box buttons
						var myButtons = {
							"Chiudi": function() {
								$( this ).dialog( "close" );
								$( "#Popup_InteroperabilitaSistri" ).dialog("open");
								}
							};
						$('#Popup_InvioRegCrono').dialog('option', 'buttons', myButtons);

						},'json');
					break;

				case "S":

					switch(TIPO_SISTRI){

						case 'crono':
							// INVIO REGISTRAZIONE CRONOLOGICA SCARICO
							$('#Popup_InvioRegCrono .progress_bar').html('<img src="__css/progress_bar.gif" />');
							$('#PutRegCronoStatus_txt').html('operazione in corso...');
							$("#Popup_InvioRegCrono" ).dialog("open");

							$.post('__SIS/PutRegistrazioneCronoScarico.php', {ID_MOV_F:ID_MOV_F}, function(phpResponse){
								
								$('#Popup_InvioRegCrono .progress_bar').html('');
								$('#PutRegCronoStatus_txt').html(phpResponse.Esito);

								if(phpResponse.WellDone){
									// Set hidden field #idSIS value
									$(OBJ2CHECK).val(phpResponse.idSIS);
									$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('idSIS', phpResponse.idSIS);
									$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('statoRegistrazioniCrono', phpResponse.statoRegistrazioniCrono);
									$("td#"+ID_MOV_F+" .SIS_status_span").html(phpResponse.statoRegistrazioniCrono);
									$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_invia', 'off');
									$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_elimina', 'on');
									$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_firma', 'on');
									$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_download', 'off');

									// Block / unblock other SIS functions
									$("#SIS_invia img").attr("src", "__css/SIS_invia_off.gif");
									$("#SIS_elimina img").attr("src", "__css/SIS_elimina_on.gif");
									$("#SIS_firma img").attr("src", "__css/SIS_firma_on.gif");
									$("#SIS_download img").attr("src", "__css/SIS_download_off.gif");
									}
								
								// Enable dialog box buttons
								var myButtons = {
									"Chiudi": function() {
										$( this ).dialog( "close" );
										$( "#Popup_InteroperabilitaSistri" ).dialog("open");
										}
									};
								$('#Popup_InvioRegCrono').dialog('option', 'buttons', myButtons);

								},'json');
							break;

						case 'scheda':

							// INVIO SCHEDA SISTRI PRODUTTORE ( E MOVIMENTO DI SCARICO!!! )
							$('#Popup_InvioScheda .progress_bar').html('<img src="__css/progress_bar.gif" />');
							$('#PutSchedaStatus_txt').html('operazione in corso...');
							$("#Popup_InvioScheda" ).dialog("open");
							
							$.post('__SIS/PutScheda.php', {ID_MOV_F:ID_MOV_F}, function(phpResponse){
								
								$('#Popup_InvioScheda .progress_bar').html('');
								$('#PutSchedaStatus_txt').html(phpResponse.Esito);

								if(phpResponse.WellDone){
									// Set hidden field #idSIS value
									$(OBJ2CHECK).val(phpResponse.idSIS_scheda);
									$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('idSIS_scheda', phpResponse.idSIS_scheda);
									$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('statoSchedaSistri', phpResponse.statoSchedaSistri);
									$("td#"+ID_MOV_F+" .SIS_status_span").html(phpResponse.statoSchedaSistri);
									$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_invia', 'off');
									$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_elimina', 'on');
									$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_firma', 'on');
									$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_download', 'off');

									// Block / unblock other SIS functions
									$("#SIS_invia img").attr("src", "__css/SIS_invia_off.gif");
									$("#SIS_elimina img").attr("src", "__css/SIS_elimina_on.gif");
									$("#SIS_firma img").attr("src", "__css/SIS_firma_on.gif");
									$("#SIS_download img").attr("src", "__css/SIS_download_off.gif");
									}
								
								// Enable dialog box buttons
								var myButtons = {
									"Chiudi": function() {
										$( this ).dialog( "close" );
										$( "#Popup_InteroperabilitaSistri" ).dialog("open");
										}
									};
								$('#Popup_InvioScheda').dialog('option', 'buttons', myButtons);

								},'json');
							break;

						}

					break;

				default:
					alert(TIPO);
					break;
					
				}
			}
		}
	else{
		alert("Impossibile procedere con l'invio della "+OBJ_desc+": la "+OBJ_desc+" e' gia' stata inviata a SISTRI.");
		}

	});


$("#SIS_elimina").click(function(){

	var TIPO_SISTRI	= $("#TIPO_SISTRI").val();

	if(TIPO_SISTRI=='crono'){
		var OBJ2CHECK	= "#idSIS";
		var OBJ_desc	= "registrazione cronologica";
		var OBJ_status	= "#statoRegistrazioniCrono";
		}

	if(TIPO_SISTRI=='scheda'){
		var OBJ2CHECK	= "#idSIS_scheda";
		var OBJ_desc	= "Scheda SISTRI";
		var OBJ_status	= "#statoSchedaSistri";
		}

	if($(OBJ2CHECK).val()=='null'){
		alert("Impossibile eliminare la "+OBJ_desc+": la "+OBJ_desc+" non e' ancora stata inviata a SISTRI.");
		}

	else if($(OBJ_status).val()=='FIRMATA'){
		alert("Impossibile eliminare "+OBJ_desc+": la "+OBJ_desc+" e' gia' stata firmata.");
		}

	else if($(OBJ_status).val()=='ANNULLATA'){
		alert("Impossibile eliminare "+OBJ_desc+": la "+OBJ_desc+" e' stata annullata.");
		}

	else{
		if(window.confirm("La "+OBJ_desc+" verra' ora eliminata da SISTRI: confermi?")){
			
			$( "#Popup_InteroperabilitaSistri" ).dialog("close");

			var ID_MOV_F		= $("#ID_MOV_F").val();
			var idSIS			= $("#idSIS").val();			
			var idSIS_scheda	= $("#idSIS_scheda").val();

			switch(TIPO_SISTRI){

				case 'crono':

					$('#Popup_EliminaRegCrono .progress_bar').html('<img src="__css/progress_bar.gif" />');
					$('#EliminaRegCronoStatus_txt').html('operazione in corso...');
					$( "#Popup_EliminaRegCrono" ).dialog("open");

					$.post('__SIS/DeleteRegistrazioneCrono.php', {ID_MOV_F:ID_MOV_F, idSIS:idSIS}, function(phpResponse){

						$('#Popup_EliminaRegCrono .progress_bar').html('');
						$('#EliminaRegCronoStatus_txt').html(phpResponse.Esito);
								
						if(phpResponse.WellDone){
							// Set hidden field #idSIS value
							$("#idSIS").val('null');
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('idSIS', 'null');
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('statoRegistrazioniCrono', 'null');
							$("td#"+ID_MOV_F+" .SIS_status_span").html('NON INVIATA');
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_invia', 'on');
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_elimina', 'off');
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_firma', 'off');
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_download', 'off');

							// Block / unblock other SIS functions
							$("#SIS_invia img").attr("src", "__css/SIS_invia_on.gif");
							$("#SIS_elimina img").attr("src", "__css/SIS_elimina_off.gif");
							$("#SIS_firma img").attr("src", "__css/SIS_firma_off.gif");
							$("#SIS_download img").attr("src", "__css/SIS_download_off.gif");
							}
								
						// Enable dialog box buttons
						var myButtons = {
							"Chiudi": function() {
								$( this ).dialog( "close" );
								$( "#Popup_InteroperabilitaSistri" ).dialog("open");
								}
							};
						$('#Popup_EliminaRegCrono').dialog('option', 'buttons', myButtons);

						},'json');

					break;

				case 'scheda':
					$('#Popup_EliminaScheda .progress_bar').html('<img src="__css/progress_bar.gif" />');
					$('#EliminaSchedaStatus_txt').html('operazione in corso...');
					$("#Popup_EliminaScheda").dialog("open");

					$.post('__SIS/DeleteSchedaSISTRI.php', {ID_MOV_F:ID_MOV_F, idSIS_scheda:idSIS_scheda}, function(phpResponse){

						$('#Popup_EliminaScheda .progress_bar').html('');
						$('#EliminaSchedaStatus_txt').html(phpResponse.Esito);
								
						if(phpResponse.WellDone){
							// Set hidden field #idSIS value
							$("#idSIS_scheda").val('null');
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('idSIS_scheda', 'null');
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('statoSchedaSistri', 'null');
							$("td#"+ID_MOV_F+" .SIS_status_span").html('NON INVIATA');
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_invia', 'on');
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_elimina', 'off');
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_firma', 'off');
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_download', 'off');

							// Block / unblock other SIS functions
							$("#SIS_invia img").attr("src", "__css/SIS_invia_on.gif");
							$("#SIS_elimina img").attr("src", "__css/SIS_elimina_off.gif");
							$("#SIS_firma img").attr("src", "__css/SIS_firma_off.gif");
							$("#SIS_download img").attr("src", "__css/SIS_download_off.gif");
							}
								
						// Enable dialog box buttons
						var myButtons = {
							"Chiudi": function() {
								$( this ).dialog( "close" );
								$( "#Popup_InteroperabilitaSistri" ).dialog("open");
								}
							};
						$('#Popup_EliminaScheda').dialog('option', 'buttons', myButtons);

						},'json');
					break;
				}

			}

		}
	});


$("#SIS_firma").click(function(){
	
	var TIPO		= $("#TIPO").val();
	var TIPO_SISTRI	= $("#TIPO_SISTRI").val();

	if(TIPO_SISTRI=='crono'){
		var OBJ2CHECK	= "#idSIS";
		var OBJ_desc	= "registrazione cronologica";
		var OBJ_status	= "#statoRegistrazioniCrono";
		}

	if(TIPO_SISTRI=='scheda'){
		var OBJ2CHECK	= "#idSIS_scheda";
		var OBJ_desc	= "Scheda SISTRI";
		var OBJ_status	= "#statoSchedaSistri";
		}
		
	if( $(OBJ2CHECK).val()!='null' && ( $(OBJ_status).val()!='FIRMATA' || TIPO_SISTRI=='scheda') && $(OBJ_status).val()!='ANNULLATA' ){
		
		if(window.confirm("La "+OBJ_desc+" verra' ora firmata: confermi?")){
			
			$( "#Popup_InteroperabilitaSistri" ).dialog("close");

			var ID_MOV_F		= $("#ID_MOV_F").val();
			var idSIS			= $("#idSIS").val();			
			var idSIS_scheda	= $("#idSIS_scheda").val();
			var StartVal		= $('#objs_url').val();

			var parameters		=  '?id=' + $(OBJ2CHECK).val() + '&ID_MOV_F=' + ID_MOV_F + '&IsUpdate=';
			
			if(TIPO_SISTRI=='scheda' && $(OBJ_status).val()=='FIRMATA') 
				parameters += '1'; 
			else 
				parameters += '0';

			if(TIPO_SISTRI=='scheda')
				parameters += '&prepara=sign_prepara_scheda';
			else
				parameters += '&prepara=sign_prepara_registrazione';
                        
                        parameters+= '&SID='+SID;
                        
                        $("#IFrameApplet").attr('src', '__SIS/signature/lib/genJNLP.php' + parameters);

			switch(TIPO_SISTRI){
			
				case 'crono':
					$( "#Popup_FirmaRegCrono" ).dialog("open");
								
					// Enable dialog box buttons
					var myButtons = {
						"Chiudi": function() {
						
							var ID_MOV_F	= $("#ID_MOV_F").val();
							var idSIS		= $("#idSIS").val();
						
							// Verifico esito firma
							$.post('__SIS/GetRegistrazioneCronoStatus.php', {ID_MOV_F:ID_MOV_F, idSIS:idSIS}, function(phpResponse){
								
								$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('statoRegistrazioniCrono', phpResponse);
								$("td#"+ID_MOV_F+" .SIS_status_span").html(phpResponse);
								$("#statoRegistrazioniCrono").val(phpResponse);
							
								if(phpResponse=='FIRMATA'){
									$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_invia', 'off');
									$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_elimina', 'off');
									$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_firma', 'off');
									$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_annulla', 'on');
									$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_download', 'on');

									// Block / unblock other SIS functions
									$("#SIS_invia img").attr("src", "__css/SIS_invia_off.gif");
									$("#SIS_elimina img").attr("src", "__css/SIS_elimina_off.gif");
									$("#SIS_firma img").attr("src", "__css/SIS_firma_off.gif");
									$("#SIS_annulla img").attr("src", "__css/SIS_annulla_on.gif");
									$("#SIS_download img").attr("src", "__css/SIS_download_on.gif");
									}
								
								$( "#objs_url" ).val(StartVal);
								$( "#Popup_FirmaRegCrono" ).dialog( "close" );
								$( "#Popup_InteroperabilitaSistri" ).dialog("open");
								
								}, 'text');
							
							}
						};

					$('#Popup_FirmaRegCrono').dialog('option', 'buttons', myButtons);
				break;


			case 'scheda':
				$( "#Popup_FirmaScheda" ).dialog("open");
							
				// Enable dialog box buttons
				var myButtons = {
					"Chiudi": function() {
					
						var ID_MOV_F		= $("#ID_MOV_F").val();
						var idSIS_scheda	= $("#idSIS_scheda").val();
					
						// Verifico esito firma
						$.post('__SIS/GetSchedaSISTRI_ProduttoreStatus.php', {ID_MOV_F:ID_MOV_F, idSIS_scheda:idSIS_scheda}, function(phpResponse){
							
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('statoSchedaSistri', phpResponse);
							$("td#"+ID_MOV_F+" .SIS_status_span").html(phpResponse);
							$("#statoSchedaSistri").val(phpResponse);
						
							if(phpResponse=='FIRMATA'){
								$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_invia', 'off');
								$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_elimina', 'off');
								$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_firma', 'off');
								$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_annulla', 'on');
								$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_download', 'on');

								// Block / unblock other SIS functions
								$("#SIS_invia img").attr("src", "__css/SIS_invia_off.gif");
								$("#SIS_elimina img").attr("src", "__css/SIS_elimina_off.gif");
								$("#SIS_firma img").attr("src", "__css/SIS_firma_off.gif");
								$("#SIS_annullaa img").attr("src", "__css/SIS_annulla_off.gif");
								$("#SIS_download img").attr("src", "__css/SIS_download_on.gif");
								}
							
							$( "#objs_url" ).val(StartVal);
							$( "#Popup_FirmaScheda" ).dialog( "close" );
							$( "#Popup_InteroperabilitaSistri" ).dialog("open");
							
							}, 'text');
						
						}
					};

				$('#Popup_FirmaScheda').dialog('option', 'buttons', myButtons);
				break;
				}
			}
		}

	// NON POSSO FIRMARE
	else{
		if($(OBJ2CHECK).val()=='null'){
			alert("Impossibile firmare la "+OBJ_desc+": la "+OBJ_desc+" non e' ancora stata inviata a SISTRI.");
			}

		else if($(OBJ_status).val()=='FIRMATA'){
			alert("Impossibile firmare la "+OBJ_desc+": la "+OBJ_desc+" e' gia' stata firmata.");
			}

		else if($(OBJ_status).val()=='ANNULLATA'){
			alert("Impossibile firmare la "+OBJ_desc+": la "+OBJ_desc+" e' stata annullata.");
			}

		}

	});



$("#SIS_annulla").click(function(){
	
	var TIPO_SISTRI	= $("#TIPO_SISTRI").val();
	var ID_MOV_F	= $("#ID_MOV_F").val();
	var TIPO		= $("#TIPO").val();

	if(TIPO_SISTRI=='crono'){
		var OBJ2CHECK	= "#idSIS";
		var OBJ_desc	= "registrazione cronologica";
		var OBJ_status	= "#statoRegistrazioniCrono";
		var GetAnnullabilita = "GetAnnullabilitaRegCrono.php";
		}

	if(TIPO_SISTRI=='scheda'){
		var OBJ2CHECK	= "#idSIS_scheda";
		var OBJ_desc	= "Scheda SISTRI";
		var OBJ_status	= "#statoSchedaSistri";
		var GetAnnullabilita = "GetAnnullabilitaScheda.php";
		}

	if( $("#SIS_annulla img").attr("src").indexOf('off')==-1){
		
		// POSSO ANNULLARE
		if( $(OBJ2CHECK).val()!='null' && $(OBJ_status).val()=='FIRMATA' ){

			$.post('__SIS/'+GetAnnullabilita, {id:$(OBJ2CHECK).val(), ID_MOV_F:ID_MOV_F}, function(phpResponse){

				switch(phpResponse.codice_stato){
					case 1:
						$('#Popup_InteroperabilitaSistri').dialog("close");
						if(TIPO_SISTRI=='crono'){
							if(TIPO=='S')
								$("tr.QuantitaOriginale").hide();
							else
								$("#QuantitaOriginale").val("Quantita' originaria: "+phpResponse.quantita);
							$('#Popup_CausaleAnnullamentoRegCrono').dialog("open");
							}
						else{
							$("#QuantitaOriginale").val("Quantita' originaria: "+phpResponse.quantita);
							$('#Popup_CausaleAnnullamentoScheda').dialog("open");
							}
						break;
					case 0:
						alert("Impossibile annullare, vi sono altri record legati alla "+OBJ_desc);
						break;			
					}
				
				}, 'json');

			}

		// NON POSSO ANNULLARE
		else{
			if($(OBJ2CHECK).val()=='null'){
				alert("Impossibile annullare la "+OBJ_desc+": la "+OBJ_desc+" non e' ancora stata inviata a SISTRI.");
				}

			else if($(OBJ_status).val()!='FIRMATA'){
				alert("Impossibile annullare la "+OBJ_desc+": la "+OBJ_desc+" non e' ancora stata firmata. E' possibile procedere con l'eliminazione.");
				}

			}
		}
	else{
		alert("Impossibile annullare la "+OBJ_desc+" selezionata");
		}

	});






function ConfermaAnnullamento(){
	
	var TIPO_SISTRI	= $("#TIPO_SISTRI").val();
	var TIPO		= $("#TIPO").val();

	if(TIPO_SISTRI=='crono'){
		var OBJ2CHECK	= "#idSIS";
		var OBJ_desc	= "registrazione cronologica";
		var OBJ_status	= "#statoRegistrazioniCrono";
		var popupFirma	= "#Popup_AnnullaRegCrono";
		var preparaScript = 'sign_prepara_annulla_registrazione';
		}

	if(TIPO_SISTRI=='scheda'){
		var OBJ2CHECK	= "#idSIS_scheda";
		var OBJ_desc	= "Scheda SISTRI";
		var OBJ_status	= "#statoSchedaSistri";
		var popupFirma	= "#Popup_AnnullaScheda";
		var preparaScript = 'sign_prepara_annulla_scheda';
		}
		
	var msg = "La "+OBJ_desc+" verra' ora annullata: confermi?";
	if(TIPO_SISTRI=='scheda' || TIPO=='C'){
		msg+= "\nI quantitativi del movimento selezionato verranno inoltre azzerati.";
		}
	else{
		msg+= "\nI quantitativi del movimento selezionato non verranno azzerati sino all'annullamento della Scheda SISTRI.";
		}
		
	if(window.confirm(msg)){
			
		var ID_MOV_F			= $("#ID_MOV_F").val();
		var idSIS				= $("#idSIS").val();			
		var idSIS_scheda		= $("#idSIS_scheda").val();
		var StartVal			= $('#objs_url').val();
		var TIPO				= $("#TIPO").val();
		var QuantitaOriginale	= $("#QuantitaOriginale").val();

		codiceCausale		= $("#CausaleAnnullamento").val();
		annotazioniCausale	= encodeURIComponent($("#CausaleAnnullamento_description").val());

		var parameters		=  '?id=' + $(OBJ2CHECK).val() + '&ID_MOV_F=' + ID_MOV_F + '&IsUpdate=0&prepara=' + preparaScript;
		parameters			+= '&Annullamento&codiceCausale=' + codiceCausale + '&annotazioniCausale=' + annotazioniCausale;
		parameters			+= '&TIPO=' + TIPO;
		parameters			+= '&QuantitaOriginale=' + QuantitaOriginale;
                parameters                      += '&SID='+SID;
                        
                $("#IFrameApplet_annulla").attr('src', '__SIS/signature/lib/genJNLP.php' + parameters);
                
		switch(TIPO_SISTRI){

			case 'crono':

				$( "#Popup_CausaleAnnullamentoRegCrono" ).dialog("close");
				$( popupFirma ).dialog("open");

				// Enable dialog box buttons
				var myButtons = {
					"Chiudi": function() {
					
						var ID_MOV_F	= $("#ID_MOV_F").val();
						var idSIS		= $("#idSIS").val();
					
						// Verifico esito firma annullamento
						$.post('__SIS/GetRegistrazioneCronoStatus.php', {ID_MOV_F:ID_MOV_F, idSIS:idSIS}, function(phpResponse){
							
							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('statoRegistrazioniCrono', phpResponse);
							$("td#"+ID_MOV_F+" .SIS_status_span").html(phpResponse);
							$("#statoRegistrazioniCrono").val(phpResponse);
						
							if(phpResponse=='ANNULLATA'){
								$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_invia', 'off');
								$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_elimina', 'off');
								$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_firma', 'off');
								$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_annulla', 'off');
								$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_download', 'on');

								// Block / unblock other SIS functions
								$("#SIS_invia img").attr("src", "__css/SIS_invia_off.gif");
								$("#SIS_elimina img").attr("src", "__css/SIS_elimina_off.gif");
								$("#SIS_firma img").attr("src", "__css/SIS_firma_off.gif");
								$("#SIS_annulla img").attr("src", "__css/SIS_annulla_off.gif");
								$("#SIS_download img").attr("src", "__css/SIS_download_on.gif");
								}
							
							$( "#objs_url" ).val(StartVal);
							$( "#Popup_AnnullaRegCrono" ).dialog( "close" );
							$( "#Popup_InteroperabilitaSistri" ).dialog("open");
							
							}, 'text');
						
						}
					};
				break;

			case 'scheda':

				$( "#Popup_CausaleAnnullamentoScheda" ).dialog("close");
				$( popupFirma ).dialog("open");

				// Enable dialog box buttons
				var myButtons = {
					"Chiudi": function() {
					
						var ID_MOV_F	= $("#ID_MOV_F").val();
						var idSIS_scheda= $("#idSIS_scheda").val();
					
						// Verifico esito firma annullamento
						$.post('__SIS/GetSchedaSISTRI_ProduttoreStatus.php', {ID_MOV_F:ID_MOV_F, idSIS_scheda:idSIS_scheda}, function(phpResponse){

							$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('statoSchedaSistri', phpResponse);
							$("td#"+ID_MOV_F+" .SIS_status_span").html(phpResponse);
							$("#statoSchedaSistri_produttore").val(phpResponse);
						
							if(phpResponse=='ANNULLATA'){
								$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_invia', 'off');
								$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_elimina', 'off');
								$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_firma', 'off');
								$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_annulla', 'off');
								$("td#"+ID_MOV_F+" .InteroperabilitaSistri").attr('SIS_download', 'on');

								// Block / unblock other SIS functions
								$("#SIS_invia img").attr("src", "__css/SIS_invia_off.gif");
								$("#SIS_elimina img").attr("src", "__css/SIS_elimina_off.gif");
								$("#SIS_firma img").attr("src", "__css/SIS_firma_off.gif");
								$("#SIS_annulla img").attr("src", "__css/SIS_annulla_off.gif");
								$("#SIS_download img").attr("src", "__css/SIS_download_on.gif");
								}
							
							$( "#objs_url" ).val(StartVal);
							$( "#Popup_AnnullaScheda" ).dialog( "close" );
							$( "#Popup_InteroperabilitaSistri" ).dialog("open");
							
							}, 'text');
						
						}
					};
				break;
			}

		$(popupFirma).dialog('option', 'buttons', myButtons);

		}
	}










$("#SIS_download").click(function(){
	
	var TIPO_SISTRI	= $("#TIPO_SISTRI").val();

	if(TIPO_SISTRI=='crono'){
		var OBJ2CHECK	= "#idSIS";
		var OBJ_desc	= "Registrazione cronologica";
		var OBJ_status	= "#statoRegistrazioniCrono";
		}

	if(TIPO_SISTRI=='scheda'){
		var OBJ2CHECK	= "#idSIS_movimentazione";
		var OBJ_desc	= "Scheda di movimentazione";
		var OBJ_status	= "#statoSchedaSistri";
		}

	if($(OBJ2CHECK).val()=='null'){
		alert("Impossibile scaricare la "+OBJ_desc+": la "+OBJ_desc+" non e' ancora stata inviata a SISTRI.");
		}

	else{
		if(window.confirm("La "+OBJ_desc+" verra' ora scaricata da SISTRI: confermi?")){

			if(TIPO_SISTRI=='crono'){
				// DOWNLOAD REG. CRONOLOGICA
				$('#Popup_InteroperabilitaSistri').dialog("close");
				$('#Popup_PdfRegistrazione .progress_bar').html('<img src="__css/progress_bar.gif" />');
				$('#GetPdfRegistrazioneStatus_txt').html('operazione in corso...');
				$('#Popup_PdfRegistrazione').dialog("open");

				var idSIS = $(OBJ2CHECK).val();

				$.post('__SIS/GetPdfRegistrazione.php', {idSIS:idSIS}, function(phpResponse){

					$('#Popup_PdfRegistrazione .progress_bar').html('');
					$('#GetPdfRegistrazioneStatus_txt').html(phpResponse.Esito);
					
					if(phpResponse.WellDone)
						$.download('__SIS/DownloadPdfRegistrazione.php','filename='+phpResponse.PDF_name+'&format=pdf&content=' + phpResponse.PDF_content );
							
					// Enable dialog box buttons
					var myButtons = {
						"Chiudi": function() {
							$( this ).dialog( "close" );
							$( "#Popup_InteroperabilitaSistri" ).dialog("open");
							}
						};

					$('#Popup_PdfRegistrazione').dialog('option', 'buttons', myButtons);

					},'json');
				}
			else{
				// DOWNLOAD SCHEDA MOVIMENTAZIONE
				$('#Popup_InteroperabilitaSistri').dialog("close");
				$('#Popup_PdfMovimentazione .progress_bar').html('<img src="__css/progress_bar.gif" />');
				$('#GetPdfMovimentazioneStatus_txt').html('operazione in corso...');
				$('#Popup_PdfMovimentazione').dialog("open");

				var idSISMovimentazione = $(OBJ2CHECK).val();

				$.post('__SIS/GetPdfMovimentazione.php', {idSISMovimentazione:idSISMovimentazione}, function(phpResponse){

					$('#Popup_PdfMovimentazione .progress_bar').html('');
					$('#GetPdfMovimentazioneStatus_txt').html(phpResponse.Esito);
					
					if(phpResponse.WellDone)
						$.download('__SIS/DownloadPdfMovimentazione.php','filename='+phpResponse.PDF_name+'&format=pdf&content=' + phpResponse.PDF_content );
							
					// Enable dialog box buttons
					var myButtons = {
						"Chiudi": function() {
							$( this ).dialog( "close" );
							$( "#Popup_InteroperabilitaSistri" ).dialog("open");
							}
						};

					$('#Popup_PdfMovimentazione').dialog('option', 'buttons', myButtons);

					},'json');

				}
			}

		}

	});



$('td .LetturaSchedaMovimentazione').click(function(){

	var $td						= $(this).closest('td');
	
	var ID_MOV_F				= $(this).attr('ID_MOV_F');
	var idSIS_movimentazione	= $(this).attr('idSIS_movimentazione');
	var idSIS_scheda			= $(this).attr('idSIS_scheda');
	var STATUS					= $td.find('span.SIS_status_span').text();
	
	//alert("ID_MOVIMENTO: "+ID_MOV_F+"\n"+"idSIS_scheda: "+idSIS_scheda+"\n"+"idSIS_movimentazione: "+idSIS_movimentazione+"\n"+"STATUS: "+STATUS);

	if(STATUS=='NON INVIATA'){
		alert("Impossibile recuperare i dati della Scheda di Movimentazione: la Scheda SISTRI Produttore non e' ancora stata inviata a SISTRI.");
		}

	else{
		// apro popup
		$( "#Popup_LetturaSchedaMovimentazione" ).dialog("open");

		// mostro loading
		$("table#DatiSchedaSistriProduttore td span").html("<img src=\"__css/progress_bar.gif\" />");
		$("table#DatiSchedaSistriTrasportatore td span").html("<img src=\"__css/progress_bar.gif\" />");
		$("table#DatiSchedaSistriDestinatario td span").html("<img src=\"__css/progress_bar.gif\" />");

		// leggo dati produttore
		$.post('__SIS/GetSchedaSISTRI_ProduttoreStatus.php', {ID_MOV_F:ID_MOV_F, idSIS_scheda:idSIS_scheda}, function(phpResponse){
			
			$td.find(' .InteroperabilitaSistri').attr('statoSchedaSistri', phpResponse);
			$("td#"+ID_MOV_F+" .SIS_status_span").html(phpResponse);
			$("table#DatiSchedaSistriProduttore td span#statoSchedaSistri_produttore").text(phpResponse);
			
			LetturaMovimentazioneEnd();

			},'text');

		// leggo dati trasportatore
		$.post('__SIS/GetSchedaSISTRI_Trasportatore.php', {ID_MOV_F:ID_MOV_F, idSIS_movimentazione:idSIS_movimentazione}, function(phpResponse){
			
			$td.find('.InteroperabilitaSistri').attr('statoSchedaSistri_trasportatore', phpResponse.statoSchedaSistri_trasportatore);
					
//			$("table#DatiSchedaSistriTrasportatore td span#statoSchedaSistri_trasportatore").text(phpResponse.statoSchedaSistri_trasportatore);
			$("table#DatiSchedaSistriTrasportatore td .forSureUniqueClass").text(phpResponse.statoSchedaSistri_trasportatore);
			$("table#DatiSchedaSistriTrasportatore td span#dataOraFirma_trasportatore").text(phpResponse.dataOraFirma);
			$("table#DatiSchedaSistriTrasportatore td span#dataOraModifica_trasportatore").text(phpResponse.dataOraModifica);
			$("table#DatiSchedaSistriTrasportatore td span#dataInizioPianificata").text(phpResponse.dataInizioPianificata);
			$("table#DatiSchedaSistriTrasportatore td span#oraInizioPianificata").text(phpResponse.oraInizioPianificata);
			$("table#DatiSchedaSistriTrasportatore td span#conducente").text(phpResponse.conducente);
			$("table#DatiSchedaSistriTrasportatore td span#targaAutomezzo").text(phpResponse.targaAutomezzo);
			$("table#DatiSchedaSistriTrasportatore td span#targaRimorchio").text(phpResponse.targaRimorchio);
			
			// se traportatore ha messo targhe, aggiungo bottone importazione
                            if(phpResponse.targaAutomezzo!='null' && !$(".RifiutoRespinto").length){

				// set buttons			
				var myButtons = {
					"Importa le targhe nel Database Soger": function() {
							
						var automezzo = phpResponse.targaAutomezzo;
						var rimorchio = phpResponse.targaRimorchio;

						$.post('__SIS/CheckDatiTrasporto.php', {ID_MOV_F:ID_MOV_F, conducente:phpResponse.conducente, automezzo:phpResponse.targaAutomezzo, rimorchio:phpResponse.targaRimorchio}, function(phpResponse){
							
							var msg ="ATTENZIONE\n\nSi sta per procedere all'inserimento delle targhe nell'anagrafica del trasportatore e nel formulario.\n\n";

							// ID_AUTO  |  ID_RMK
							// 'null'	-> non indicato in Sistri
							// 0		-> indicato ma non presente in DB
							// >0		-> indicato e presente in DB

							switch(phpResponse.ID_AUTO){
								case 'null':
									msg +="Targa automezzo: non indicata dal trasportatore in SISTRI\n";
									break;
								case 0:
									msg +="Targa automezzo "+automezzo+": la targa non e' presente nell'anagrafica del trasportatore; verra' importata e inserita nel formulario\n";
									break;
								default:
									msg +="Targa automezzo "+automezzo+": la targa e' presente nell'anagrafica del trasportatore; verra' inserita nel formulario\n";
									break;
								}

							switch(phpResponse.ID_RMK){
								case 'null':
									msg +="Targa rimorchio: non indicata dal trasportatore in SISTRI\n";
									break;
								case 0:
									msg +="Targa rimorchio "+rimorchio+": la targa non e' presente nell'anagrafica del trasportatore; verra' importata e inserita nel formulario\n";
									break;
								default:
									msg +="Targa rimorchio "+rimorchio+": la targa e' presente nell'anagrafica del trasportatore; verra' inserita nel formulario\n";
									break;
								}

							msg +="\n\nDesideri procedere?";

							if(window.confirm(msg)){
								$.post('__SIS/UpdateDatiTrasporto.php', {ID_MOV_F:ID_MOV_F, ID_AUTO:phpResponse.ID_AUTO, ID_RMK:phpResponse.ID_RMK, automezzo:automezzo, rimorchio:rimorchio}, function(phpResponse){
									if(automezzo)	$(".AutomezzoFromSISTRI_"+ID_MOV_F).text(automezzo);
									if(rimorchio)	$(".RimorchioFromSISTRI_"+ID_MOV_F).text(rimorchio);
									window.alert(phpResponse);
									});
								}

							}, 'json');

						},
					"Chiudi": function() {
						$( this ).dialog( "close" );
						}
					};

				$('#Popup_LetturaSchedaMovimentazione').dialog('option', 'buttons', myButtons);

				}

			LetturaMovimentazioneEnd();

			},'json');
		

		// leggo dati destinatario
		$.post('__SIS/GetSchedaSISTRI_Destinatario.php', {ID_MOV_F:ID_MOV_F, idSIS_movimentazione:idSIS_movimentazione}, function(phpResponse){
			
			$td.find('.InteroperabilitaSistri').attr('statoSchedaSistri_destinatario', phpResponse.statoSchedaSISTRI_destinatario);
					
			$("table#DatiSchedaSistriDestinatario td span#statoSchedaSistri_destinatario").text(phpResponse.statoSchedaSistri_destinatario);
			$("table#DatiSchedaSistriDestinatario td span#esitoTrasporto").text(phpResponse.esitoTrasporto);
			$("table#DatiSchedaSistriDestinatario td span#flagAttesaVerificaAnalitica").text(phpResponse.flagAttesaVerificaAnalitica);
			$("table#DatiSchedaSistriDestinatario td span#dataConclusioneProcesso").text(phpResponse.dataConclusioneProcesso);
			$("table#DatiSchedaSistriDestinatario td span#oraConclusioneProcesso").text(phpResponse.oraConclusioneProcesso);
			$("table#DatiSchedaSistriDestinatario td span#quantitaRicevuta").text(phpResponse.quantitaRicevuta);

			LetturaMovimentazioneEnd();
			
			}, 'json');
		
		}

	});







$('td .CompletamentoScaricoFIR').click(function(){

	var $td			= $(this).closest('td');
	var ID_MOV_F	= $(this).attr('ID_MOV_F');
	var ID_RIF		= $(this).attr('ID_RIF');
	$( "#Popup_CompletamentoScaricoFIR" ).dialog("open");
		
	$.post('__SIS/GetLastNMOV.php', {NeedEcho:true}, function(phpResponse){
		// phpResponse = NMOV_F|NMOV_I
		var NMOVs	= phpResponse.split("|");
		var NMOV_F	= NMOVs[0];
		var NMOV_I	= NMOVs[1];
		
		$( "#CompletamentoScarico_NMOV" ).val(NMOV_F);
		
		// Enable dialog box buttons
		var myButtons = {
			"Salva": function() {
				// validazione dei dati
				// NFORM obbligatorio
				// DTMOV e DTFORM regExp
				var NMOV	= $("#CompletamentoScarico_NMOV").val();
				var NFORM	= $("#CompletamentoScarico_NFORM").val();
				var DTMOV	= $("#CompletamentoScarico_DTMOV").val();
				var DTFORM	= $("#CompletamentoScarico_DTFORM").val();
				
				var RxpPattern = /^(0[1-9]|1[0-9]|2[0-9]|3[01]|[1-9])\/+(0[1-9]|1[0-2]|[1-9])\/+(19|20)[0-9]{2}$/;
				
				if(!DTMOV.match(RxpPattern) || !DTFORM.match(RxpPattern)){
					alert("Attenzione!\n\nLe date devono essere indicate nel formato gg/mm/aaaa.");
					}
				else if(trim(NFORM)==""){
					alert("Attenzione!\n\nIl numero di formulario e' un campo obbligatorio.");
					}
				else{
					$.post('__SIS/CompletamentoScaricoFIR.php', {ID_MOV_F:ID_MOV_F, NMOV_F:NMOV_F, NMOV_I:NMOV_I, NFORM:NFORM, DTMOV:DTMOV, DTFORM:DTFORM, ID_RIF:ID_RIF}, function(phpResponse){
						if(phpResponse=='1'){
							alert("Salvataggio ultimato, ora la pagina verra' ricaricata.");
							location.reload();
							}
						else{
							alert("Impossibile salvare: sul registro industriale sono presenti movimenti da fiscalizzare con data antecedente allo scarico odierno.");
							}
						});
					}
				},

			"Chiudi": function() {
				$( this ).dialog( "close" );
				}
			};

		$('#Popup_CompletamentoScaricoFIR').dialog('option', 'buttons', myButtons);
		
		}, 'text');

	});





$('td .LetturaRegistrazione').click(function(){

	var $td			= $(this).closest('td');
	var ID_MOV_F	= $td.attr('id');
	var idSIS		= $(this).attr('idSIS');
	var STATUS		= $td.find('span.SIS_status_span').text();
	
	//alert("ID_MOVIMENTO: "+ID_MOV_F+"\n"+"idSIS: "+idSIS+"\n"+"STATUS: "+STATUS);

	if(STATUS=='NON INVIATA'){
		alert("Impossibile recuperare lo stato della Registrazione Cronologica: la Registrazione Cronologica non e' ancora stata inviata a SISTRI.");
		}
	else{
		$("#ID_MOV_F").val(ID_MOV_F);
		$("#idSIS").val(idSIS);

		// Show dialog box
		$( "#Popup_LetturaRegistrazione" ).dialog("open");

		$.post('__SIS/GetRegistrazioneCronoStatus.php', {idSIS:idSIS, ID_MOV_F:ID_MOV_F}, function(phpResponse){

			$('#Popup_LetturaRegistrazione .progress_bar').html('');
			$('#LetturaRegistrazioneStatus_txt').html("lettura ultimata, la Registrazione Cronologica risulta "+phpResponse+" in Sistri.");

			if(phpResponse=='FIRMATA'){
				// inibisco nuova firma
				$td.find('.InteroperabilitaSistri').attr('SIS_firma', 'off');
				$("#SIS_firma img").attr("src", "__css/SIS_firma_off.gif");
				}

			$td.find('span.SIS_status_span').text(phpResponse)			

			var myButtons = {
					"Chiudi": function() {
						$( this ).dialog( "close" );
						}
					};

			$('#Popup_LetturaRegistrazione').dialog('option', 'buttons', myButtons);

			},'text');

		}

	});








// END
});





function check_superpwd(){
	
	var PIN		= $('#superpwd').val();
	var NMOV	= $('#NMOV').val();
	var ID_MOV_F= $('#ID_MOV_F').val();
	var RegType	= $('#RegType').val();

	// per ora verifico giusto che non sia vuoto
	if(PIN!=''){

		//loading while ajax is checking pwd
		$('#Popup_superpwd').ajaxStart(function() {
			$('#PINcontainer').html('<img src="__css/progress_bar.gif" />');
			});

		//check if PIN is correct
		$.post('__SIS/CheckSuperPWD.php', {PIN:PIN}, function(phpResponse){
			switch(phpResponse){

				case '0': // PIN errato
					// alert error
					alert('Attenzione!\r\nIl PIN inserito non e\' corretto.');
					$('#PINcontainer').html('<input type=\"password\" id=\"superpwd\" name=\"superpwd\">');
					break;

				case '1': // PIN corretto
					$( "#superpwd" ).val("")
					$( "#Popup_superpwd" ).dialog("close");
					$('.DownloadAnteprimaCS').attr("href", "__SIS/View_AnteprimaCS.php?NMOV="+NMOV+"&ID_MOV_F="+ID_MOV_F+"&RegType="+RegType);
					$( "#Popup_AnteprimaCS" ).dialog("open");

					break;
				}
			});

		}
	else{
		window.alert('Non e\' stato inserito alcun PIN');
		}
	}





function LetturaMovimentazioneEnd(){

	// check 3 status fields
	var sSSP	= $("span#statoSchedaSistri_produttore").html();
	var sSST	= $(".forSureUniqueClass").html();
	var sSSD	= $("span#statoSchedaSistri_destinatario").html();

	var statoSchedaSistri_produttore = false;
	var statoSchedaSistri_trasportatore = false;
	var statoSchedaSistri_destinatario = false;

	if (typeof sSSP != 'undefined')
		statoSchedaSistri_produttore	= (sSSP.indexOf('progress_bar') == -1);
	if (typeof sSST != 'undefined')
		statoSchedaSistri_trasportatore	= (sSST.indexOf('progress_bar') == -1);
	if (typeof sSSD != 'undefined')
		statoSchedaSistri_destinatario	= (sSSD.indexOf('progress_bar') == -1);

	//var msg ="Scheda Sistri:\n\n";
	//msg+="produttore: "+sSSP+" -- "+statoSchedaSistri_produttore+"\n";
	//msg+="trasportatore: "+sSST+" -- "+statoSchedaSistri_trasportatore+"\n";
	//msg+="destinatario: "+sSSD+" -- "+statoSchedaSistri_destinatario+"\n";
	//alert(msg);

	if(statoSchedaSistri_produttore && statoSchedaSistri_trasportatore && statoSchedaSistri_destinatario){
		$("span.progress_bar").html('');
		$("span#LetturaSchedaMovimentazioneStatus_txt").html('lettura della Scheda di Movimentazione ultimata');
		}

	}






function AutorizzaInvio(NMOV, RegType){

	//alert(NMOV);
	//alert(RegType);

	if($('#ConfermaCS').is(':checked')){
		
		//$(".ui-dialog-buttonpane button:contains('Conferma')").button("disable");
		$(".ui-dialog-buttonpane button:contains('Conferma')").hide();
		$(".ui-dialog-buttonpane button:contains('Chiudi')").hide();
		
		//loading while ajax is checking pwd
		$('#Popup_superpwd').ajaxStart(function() {

			// Disable dialog box buttons
			
			var newContent	="<p style='text-align:center'>Approvazione dei movimenti in corso...<br /><img src=\"__css/progress_bar.gif\" /></p>";
			$('#Popup_AnteprimaCS').html(newContent);
			
			});

		$.post('__SIS/AutorizzaInvioSISTRI.php', {NMOV:NMOV, RegType:RegType}, function(phpResponse){
			
			//alert(phpResponse);
			var newContent	="<p style='text-align:center'>Operazione ultimata!<br /><br /><a href=\"#\" onclick=\"javascript:location.reload();\" style=\"font-weight:bold;text-decoration:none;cursor:pointer;\">Click qui per aggiornare</a></p>";
			$('#Popup_AnteprimaCS').html(newContent);
			
			});

		}
	else{
		alert("Attenzione, confermare di aver preso visione dell'anteprima e autorizzare l'invio dei movimenti a SISTRI");
		}


	}