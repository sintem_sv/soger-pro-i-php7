<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
require_once("SIS_SOAP_SSL.php");
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");

global $SOGER;

$Response	= Array();


// Inserimento di una registrazione cronologica di scarico
$reg = new RegistrazioneCrono_Base;

// Select data from Soger
$SQL ="SELECT lov_cer.COD_CER, lov_cer.PERICOLOSO, user_movimenti_fiscalizzati.ID_RIF, user_schede_rifiuti.H1, user_schede_rifiuti.H2, user_schede_rifiuti.H3A, user_schede_rifiuti.H3B, user_schede_rifiuti.H4, user_schede_rifiuti.H5, user_schede_rifiuti.H6, user_schede_rifiuti.H7, user_schede_rifiuti.H8, user_schede_rifiuti.H9, user_schede_rifiuti.H10, user_schede_rifiuti.H11, user_schede_rifiuti.H12, user_schede_rifiuti.H13, user_schede_rifiuti.H14, user_schede_rifiuti.H15, user_schede_rifiuti.ClassificazioneHP, user_schede_rifiuti.HP1, user_schede_rifiuti.HP2, user_schede_rifiuti.HP3, user_schede_rifiuti.HP4, user_schede_rifiuti.HP5, user_schede_rifiuti.HP6, user_schede_rifiuti.HP7, user_schede_rifiuti.HP8, user_schede_rifiuti.HP9, user_schede_rifiuti.HP10, user_schede_rifiuti.HP11, user_schede_rifiuti.HP12, user_schede_rifiuti.HP13, user_schede_rifiuti.HP14, user_schede_rifiuti.HP15, user_schede_rifiuti.descrizione, pesoN, user_schede_rifiuti.ID_SF, user_schede_rifiuti.ID_SF, lov_operazioni_rs.description as destinatoA, user_impianti_destinatari.idSIS as idSIS_D, user_impianti_destinatari.versione as Versione_D, user_movimenti_fiscalizzati.NOTER, user_schede_rifiuti.NOTER AS NOTE_RIFIUTO, user_movimenti_fiscalizzati.lotto, lov_cer.description AS NomeCER, user_impianti_produttori.FuoriUnitaLocale, user_impianti_produttori.description AS indirizzoP, lov_comuni_istat.codice_catastale, user_movimenti_fiscalizzati.SenzaTrasporto ";
$SQL.="FROM user_movimenti_fiscalizzati ";
$SQL.="JOIN user_impianti_produttori ON user_movimenti_fiscalizzati.ID_UIMP=user_impianti_produttori.ID_UIMP ";
$SQL.="JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM=user_impianti_produttori.ID_COM ";
$SQL.="JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF ";
$SQL.="JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
$SQL.="JOIN lov_operazioni_rs ON user_movimenti_fiscalizzati.ID_OP_RS=lov_operazioni_rs.ID_OP_RS ";
$SQL.="JOIN user_impianti_destinatari ON user_movimenti_fiscalizzati.ID_UIMD=user_impianti_destinatari.ID_UIMD ";
$SQL.="WHERE ID_MOV_F=".$_POST['ID_MOV_F']." AND user_movimenti_fiscalizzati.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND user_movimenti_fiscalizzati.".$SOGER->UserData['workmode']."=1;";
$FEDIT->SdbRead($SQL,"Scarico");

## Verifico le relazioni scarico-carico
$TableName		= "user_movimenti_fiscalizzati";
$LegamiSISTRI	= true;
$RIF_REF		= $FEDIT->Scarico[0]['ID_RIF'];
require("../__scripts/MovimentiRifMovCarico.php");


// registro cronologico
$id_sis_registro  = $SOGER->UserData['core_usersidSIS_regCrono'];

// codice CER
$CER_array=str_split($FEDIT->Scarico[0]['COD_CER'], 2);
$CER=$CER_array[0].".".$CER_array[1].".".$CER_array[2];
$reg->codiceCerIIILivello = new Catalogo($CER);

// stato fisico
switch($FEDIT->Scarico[0]['ID_SF']){
	case 1: // Solido pulverulento -> In polvere o pulverulenti
		$ID_STATO_FISICO_RIFIUTO = '01';
		break;
	case 2: // Solido non pulverulento -> Solidi
		$ID_STATO_FISICO_RIFIUTO = '02';
		break;
	case 3: // Fangoso palabile -> Fangosi
		$ID_STATO_FISICO_RIFIUTO = '04';
		break;
	case 4: // Liquido ->  Liquidi
		$ID_STATO_FISICO_RIFIUTO = '05';
		break;
	}

$reg->statoFisicoRifiuto  = new Catalogo($ID_STATO_FISICO_RIFIUTO);

// pesoN convertito in mg
$milligrammi = $FEDIT->Scarico[0]['pesoN']*1000000;
$reg->quantita            = new LongNumber((string) $milligrammi);

// descrizione
$reg->descrizioneRifiuto  = utf8_encode($FEDIT->Scarico[0]['descrizione']);

// CLASSI DI PERICOLSO
$NOTA_CLASSIH = "";
if($FEDIT->Scarico[0]['PERICOLOSO']=='1'){

	$reg->caratteristicaPericolo = array();

	## CLASSI H
	if( strtotime(date('Y-m-d')) < strtotime('2015-06-01') ){
		if($FEDIT->Scarico[0]['H1']=='1') $reg->caratteristicaPericolo[] = new Catalogo("H1");
		if($FEDIT->Scarico[0]['H2']=='1') $reg->caratteristicaPericolo[] = new Catalogo("H2");
		if($FEDIT->Scarico[0]['H3A']=='1') $reg->caratteristicaPericolo[] = new Catalogo("H3A");
		if($FEDIT->Scarico[0]['H3B']=='1') $reg->caratteristicaPericolo[] = new Catalogo("H3B");
		if($FEDIT->Scarico[0]['H4']=='1') $reg->caratteristicaPericolo[] = new Catalogo("H4");
		if($FEDIT->Scarico[0]['H5']=='1') $reg->caratteristicaPericolo[] = new Catalogo("H5");
		if($FEDIT->Scarico[0]['H6']=='1') $reg->caratteristicaPericolo[] = new Catalogo("H6");
		if($FEDIT->Scarico[0]['H7']=='1') $reg->caratteristicaPericolo[] = new Catalogo("H7");
		if($FEDIT->Scarico[0]['H8']=='1') $reg->caratteristicaPericolo[] = new Catalogo("H8");
		if($FEDIT->Scarico[0]['H9']=='1') $reg->caratteristicaPericolo[] = new Catalogo("H9");
		if($FEDIT->Scarico[0]['H10']=='1') $reg->caratteristicaPericolo[] = new Catalogo("H10");
		if($FEDIT->Scarico[0]['H11']=='1') $reg->caratteristicaPericolo[] = new Catalogo("H11");
		if($FEDIT->Scarico[0]['H12']=='1') $reg->caratteristicaPericolo[] = new Catalogo("H12");
		if($FEDIT->Scarico[0]['H13']=='1') $reg->caratteristicaPericolo[] = new Catalogo("H13");
		if($FEDIT->Scarico[0]['H14']=='1') $reg->caratteristicaPericolo[] = new Catalogo("H14");
		if($FEDIT->Scarico[0]['H15']=='1') $reg->caratteristicaPericolo[] = new Catalogo("H15");
		}
	## CLASSI HP
	else{
		if($FEDIT->Scarico[0]['HP1']=='1') $reg->caratteristicaPericolo[] = new Catalogo("HP1");
		if($FEDIT->Scarico[0]['HP2']=='1') $reg->caratteristicaPericolo[] = new Catalogo("HP2");
		if($FEDIT->Scarico[0]['HP3']=='1') $reg->caratteristicaPericolo[] = new Catalogo("HP3");
		if($FEDIT->Scarico[0]['HP4']=='1') $reg->caratteristicaPericolo[] = new Catalogo("HP4");
		if($FEDIT->Scarico[0]['HP5']=='1') $reg->caratteristicaPericolo[] = new Catalogo("HP5");
		if($FEDIT->Scarico[0]['HP6']=='1') $reg->caratteristicaPericolo[] = new Catalogo("HP6");
		if($FEDIT->Scarico[0]['HP7']=='1') $reg->caratteristicaPericolo[] = new Catalogo("HP7");
		if($FEDIT->Scarico[0]['HP8']=='1') $reg->caratteristicaPericolo[] = new Catalogo("HP8");
		if($FEDIT->Scarico[0]['HP9']=='1') $reg->caratteristicaPericolo[] = new Catalogo("HP9");
		if($FEDIT->Scarico[0]['HP10']=='1') $reg->caratteristicaPericolo[] = new Catalogo("HP10");
		if($FEDIT->Scarico[0]['HP11']=='1') $reg->caratteristicaPericolo[] = new Catalogo("HP11");
		if($FEDIT->Scarico[0]['HP12']=='1') $reg->caratteristicaPericolo[] = new Catalogo("HP12");
		if($FEDIT->Scarico[0]['HP13']=='1') $reg->caratteristicaPericolo[] = new Catalogo("HP13");
		if($FEDIT->Scarico[0]['HP14']=='1') $reg->caratteristicaPericolo[] = new Catalogo("HP14");
		if($FEDIT->Scarico[0]['HP15']=='1') $reg->caratteristicaPericolo[] = new Catalogo("HP15");
		// classi H nelle annotazioni
		if($FEDIT->Scarico[0]['H1']=='1') $NOTA_CLASSIH.=" H1";
		if($FEDIT->Scarico[0]['H2']=='1') $NOTA_CLASSIH.=" H2";
		if($FEDIT->Scarico[0]['H3A']=='1') $NOTA_CLASSIH.=" H3A";
		if($FEDIT->Scarico[0]['H3B']=='1') $NOTA_CLASSIH.=" H3B";
		if($FEDIT->Scarico[0]['H4']=='1') $NOTA_CLASSIH.=" H4";
		if($FEDIT->Scarico[0]['H5']=='1') $NOTA_CLASSIH.=" H5";
		if($FEDIT->Scarico[0]['H6']=='1') $NOTA_CLASSIH.=" H6";
		if($FEDIT->Scarico[0]['H7']=='1') $NOTA_CLASSIH.=" H7";
		if($FEDIT->Scarico[0]['H8']=='1') $NOTA_CLASSIH.=" H8";
		if($FEDIT->Scarico[0]['H9']=='1') $NOTA_CLASSIH.=" H9";
		if($FEDIT->Scarico[0]['H10']=='1') $NOTA_CLASSIH.=" H10";
		if($FEDIT->Scarico[0]['H11']=='1') $NOTA_CLASSIH.=" H11";
		if($FEDIT->Scarico[0]['H12']=='1') $NOTA_CLASSIH.=" H12";
		if($FEDIT->Scarico[0]['H13']=='1') $NOTA_CLASSIH.=" H13";
		if($FEDIT->Scarico[0]['H14']=='1') $NOTA_CLASSIH.=" H14";
		if($FEDIT->Scarico[0]['H15']=='1') $NOTA_CLASSIH.=" H15";	
		}
	}

# annotazioni
$note = "";

# note registro scritte manualmente
if(!is_null($FEDIT->Scarico[0]["NOTER"]) AND trim($FEDIT->Scarico[0]["NOTER"])!='')
	$note .= $FEDIT->Scarico[0]["NOTER"] . "\n";
# note della scheda rifiuto
if(!is_null($FEDIT->Scarico[0]["NOTE_RIFIUTO"]) AND trim($FEDIT->Scarico[0]["NOTE_RIFIUTO"])!='')
	$note .= $FEDIT->Scarico[0]["NOTE_RIFIUTO"]."\n";
# numero di lotto
if(!is_null($FEDIT->Scarico[0]["lotto"]) AND trim($FEDIT->Scarico[0]["lotto"])!='')
	$note .="Lotto numero: " . trim($FEDIT->Scarico[0]["lotto"]) . "\n";
# nome cer
if($SOGER->UserData["core_usersREG_NOMECER"]=="1")
	$note .="Nome CER: ".$FEDIT->Scarico[0]["NomeCER"]."\n";

# classi H?
//if(trim($NOTA_CLASSIH)!='')
//	$note .= "Classificazione secondo Direttiva 2008/98/CE: ".$NOTA_CLASSIH;

$reg->annotazioni	= utf8_encode($note);


#    $reg->posizioneRifiuto; // PosizioneRifiuto
if($FEDIT->Scarico[0]['FuoriUnitaLocale']=='1'){
	$indirizzo					= utf8_encode($FEDIT->Scarico[0]['indirizzoP']);
	$codiceCatastale			= $FEDIT->Scarico[0]['codice_catastale'];
	$RifiutoFuoriUnitaLocale	= new RifiutoFuoriUnitaLocale($indirizzo, $codiceCatastale);
	$PosizioneRifiuto			= new PosizioneRifiuto(null, $RifiutoFuoriUnitaLocale);
	$reg->posizioneRifiuto		= $PosizioneRifiuto;
	}


// carichi di riferimento e qta scaricate
$SQL="SELECT CARICO_idSIS, QTA_PRELEVATA FROM user_legami_carico_scarico WHERE MOV_KEY='ID_MOV_F' AND SCARICO_ID=".$_POST['ID_MOV_F'];
$FEDIT->SdbRead($SQL,"Carichi");
$registrazioniCronoCarico=array();
for($c=0;$c<count($FEDIT->Carichi);$c++){
		
	$registrazioniCronoCarico[] = new RegistrazioneCronoCarico($FEDIT->Carichi[$c]['CARICO_idSIS'], $FEDIT->Carichi[$c]['QTA_PRELEVATA']);

	# un carico non � stato inviato a Sistri
	if($FEDIT->Carichi[$c]['CARICO_idSIS']==''){

		$Response['Esito']		= "impossibile completare l'operazione. Uno dei movimenti di carico cui si fa riferimento non � stato inviato a SISTRI.";
		$Response['WellDone']	= false;
		$Response['idSIS']		= 'null';
		
		$Response = array_map('utf8_encode', $Response);
		echo json_encode($Response);
		return;
		}

	}

// rifiuto destinato a - op r-s
$reg->operazioneImpianto = new Catalogo($FEDIT->Scarico[0]['destinatoA']);

// rifiuto destinato a - dati destinatario
$reg->idSISSede_impiantoDestinazione = $FEDIT->Scarico[0]['idSIS_D'];
$reg->versioneSede_impiantoDestinazione = new LongNumber((string) $FEDIT->Scarico[0]['Versione_D']);

// causale creazione
if($FEDIT->Scarico[0]['SenzaTrasporto']=='1')
	$reg->causaleCreazione		 = new Catalogo("ST");

//die(var_dump($registrazioniCronoCarico));

#
#	ALTRI PARAMETRI TRASMETTIBILI
#
#    $reg->codRec_1013; // Catalogo
#    $reg->numeroNotifica; // string
#    $reg->numeroSerieSpedizione; // string
#    $reg->flagVeicoli_Dlgs_209_2003; // Flag
#    $reg->flagVeicoli_Dlgs_Art231_152_2006; // Flag
#    $reg->numeroVeicoliConferiti; // LongNumber
#    $reg->categoriaRAEE; // Catalogo
#    $reg->tipologiaRAEE; // Catalogo
#    $reg->flagRiutilizzoAppIntera; // Flag
#    $reg->flagOpRecuperoEnergia; // Flag
#    $reg->flagOpRecuperoMateria; // Flag
#    $reg->idSISSede_impiantoOrigine; // string
#    $reg->versioneSede_impiantoOrigine; // LongNumber
#    $reg->descrizioneAltroStatoFisico; // string
#    $reg->idSISSede_consegnatoA; // string
#    $reg->versioneSede_consegnatoA; // LongNumber
#    $reg->causaleCreazione; // Catalogo
#    $reg->causaleModifica; // Catalogo
#	 $reg->flagConferitoDaPrivato; // Flag
#	 $reg->volume; // Double

//die(var_dump($registrazioniCronoCarico));

$CERT		= "certificates/".$SOGER->UserData['core_impianticodfisc']."/crt_key.pem";
$s			= new SIS_SOAP($SOGER->UserData['core_usersSIS_identity'], $CERT);

try {
	$UUID	= $s->generateUUID();
    $rv1	= $s->PutRegistrazioneCronoScarico(
                array(  
                "idSISRegistroCrono" => $id_sis_registro,
				"registrazioniCronoCarico" => $registrazioniCronoCarico,
                "registrazione" => $reg, 
				"UUID_Transazione" => $UUID, 
        )); 
	
	$rv2	= $s->GetRegistrazioneCrono(
				array(  
				"idSISRegistrazioneCrono"=>$rv1,
				)
			);

	$statoRegistrazioniCrono = $rv2->statoRegistrazioniCrono->description;
	
	// aggiorno idSIS in tabella movimenti
 	$SQL="UPDATE user_movimenti_fiscalizzati SET idSIS='".$rv1."', dataRegistrazioniCrono_invio='".date('Y-m-d')."', statoRegistrazioniCrono='".$statoRegistrazioniCrono."' WHERE ID_MOV_F=".$_POST['ID_MOV_F']." AND user_movimenti_fiscalizzati.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' ";
	$SQL.="AND user_movimenti_fiscalizzati.".$SOGER->UserData['workmode']."=1 ";
	$FEDIT->SDBWrite($SQL,true,false);

	// aggiorno idSIS in tabella legami c/s
	$SQL="UPDATE user_legami_carico_scarico SET SCARICO_idSIS='".$rv1."' WHERE MOV_KEY='ID_MOV_F' AND SCARICO_ID=".$_POST['ID_MOV_F'].";";
	$FEDIT->SDBWrite($SQL,true,false);

	/* ASSOCIAZIONE REG. SCARICO // SCHEDA SISTRI */
	if($FEDIT->Scarico[0]['SenzaTrasporto']=='0'){
		$idSISRegistrazioneCrono	= $rv1;
		
		$SQL="SELECT idSIS_scheda FROM user_movimenti_fiscalizzati WHERE idSIS = '".$idSISRegistrazioneCrono."';";
		$FEDIT->SdbRead($SQL,"idSIS_scheda");
		$idSISScheda				= $FEDIT->idSIS_scheda[0]['idSIS_scheda'];

		$rv2	= $s->AssociaRegistrazioneScheda(
					array(
					"idSISRegistrazioneCrono"	=> $idSISRegistrazioneCrono,
					"idSISScheda"				=> $idSISScheda
					)
				);
		}
	/* ========================================== */


	$Response['Esito']		= "operazione ultimata, registrazione cronologica $rv1 creata in SISTRI.";
	$Response['WellDone']	= true;
	$Response['idSIS']		= $rv1;
	$Response['statoRegistrazioniCrono']		= $statoRegistrazioniCrono;

	} 
catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);

	// Store error in DB	
	$SQL ="INSERT INTO `debug_sis` (`UUID`, `ID_IMP`, `ID_USR`, `SIS_identity`, `workmode`, `idSIS_sede`, `idSIS_regCrono`, `regCrono_type`, `TIME`, `errorCode`, `errorMessage`, `RequestHeaders`, `Request`, `ResponseHeaders`, `Response`) ";
	$SQL.="VALUES ('".$UUID."', '".$SOGER->UserData['core_usersID_IMP']."', '".$SOGER->UserData['core_usersID_USR']."', '".$SOGER->UserData['core_usersSIS_identity']."', '".$SOGER->UserData['workmode']."', '".$SOGER->UserData['core_usersidSIS_sede']."', '".$SOGER->UserData['core_usersidSIS_regCrono']."', '".$SOGER->UserData['core_usersregCrono_type']."', CURRENT_TIMESTAMP, '".addslashes($SISexcept->errorCode)."', '".addslashes($SISexcept->errorMessage)."', '".addslashes($s->__getLastRequestHeaders())."', '".addslashes($s->__getLastRequest())."', '".addslashes($s->__getLastResponseHeaders())."', '".addslashes($s->__getLastResponse())."');";
    $FEDIT->SDBWrite($SQL,true,false);

	// Notice error
	$errorMessage = explode("]:", $SISexcept->errorMessage);
    
	$Response['Esito']		= htmlspecialchars($errorMessage[1]).".<br /><br />Se non � nota la causa dell'errore, aprire un ticket nella sezione \"Interoperabilit�\" e descrivere la problematica riportando l'ID della transazione ".$UUID;
	$Response['WellDone']	= false;
	$Response['idSIS']		= 'null';
	$Response['statoRegistrazioniCrono']		= 'null';

	}

$Response = array_map('utf8_encode', $Response);
echo json_encode($Response);

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>