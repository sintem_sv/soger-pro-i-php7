<?php


// ---------------------------------------------------------------------
// CONFIGURATION

// Location
define('SERVER_URL',     'http://sis.sistri.it/SIS/services/SIS?wsdl');
define('SERVER_URL_SSL', 'https://sisssl.sistri.it/SIS/services/SIS?wsdl');
// Target namespace
define('SERVER_URI',     'http://sis.sistri.it/SIS/services/SIS/');
define('SERVER_URI_SSL', 'https://sisssl.sistri.it/SIS/services/SIS/');


// Uses exceptions instead of returning SoapError objects
define('SIS_EXCEPTIONS', 1);
// Enables tracing functions
define('SIS_DEBUG', 1);
// ---------------------------------------------------------------------



/**
 *	Class for SOAP access to SIS.
 *
 *	Exposes all the available web services.
 */
class SIS_SOAP extends SoapClient {

    private $identity;

	/**
     *  Class constructor.
     *
     *	@param string $username - Username for access.
     *	@return nothing.
     */
    function __construct($username, 
						 $local_cert, 
    					 $uri = SERVER_URI_SSL, 
    					 $location = SERVER_URL_SSL) 
    {
        $this->identity = $username;

        parent::__construct(SERVER_URL_SSL, array(
	        'uri'        => $uri,
                'location'   => $location,
                'local_cert' => $local_cert,
                'trace'      => SIS_DEBUG,
                'exceptions' => SIS_EXCEPTIONS,
                'features'   => SOAP_USE_XSI_ARRAY_TYPE | SOAP_SINGLE_ELEMENT_ARRAYS,
                'stream_context' => stream_context_create(array('ssl'=>array('verify_peer'=>false))),
	    ));
    }

	/**
     *  __call() "magic" method.
     *
     *	Wraps parent::__call() adding 'identity' parameter 
     *           par>callWebService("methodname", $params) creating
     *  the method at runtime.
     *  Enhances the code readability.
     *
     *	@param string $method - method name
     *	@param array $args - Other (optional) arguments
     *	@return parent::__call($method, $args) return values, shortened by 1 "level"
     */
    public function __call($method, $args) 
    {
        $args[0]['identity'] = $this->identity;

        $rv = parent::__call($method, $args);

        if (!SIS_EXCEPTIONS && is_soap_fault($rv))
            return $rv;
        $rva = (array) $rv;
        
        if ( count($rva)>1 ) 
            return $rva;
        return array_shift($rva);
    }


	function generateUUID() {
		if (function_exists('com_create_guid') === true){
			return trim(com_create_guid(), '{}');
			}
		return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
	}



	/**
     *  Gets last SOAP transaction (HTTP dump), both request and response
     *
     *	@param none
     *	@return string
     */
    function getLastTransaction() {
      if (SIS_DEBUG)
        return "\n\nDumping request: \n".$this->__getLastRequestHeaders()
             . "---------------------\n".$this->__getLastRequest()
             . "\n\nDumping response:\n".$this->__getLastResponseHeaders()
             . "---------------------\n".$this->__getLastResponse()
             . "\n\n";
      else
        return "";
    }


	/**
     *  Extracts SISException object from a SoapFault object
     *
     *	@param SoapFault $e method name
     *	@return SISException object
     */


	    static public function getSISException(SoapFault $e)
    {
        $e0 = ((array) $e->detail);
		//$e0 = ((array) $e->faultstring);
        return array_shift($e0);
    }
/*


	static public function getSISException(SoapFault $e)
    {
		if(defined($e->detail)){
			$e0 = ((array) $e->detail);
			return array_shift($e0);
			}
		else{
			$el = new SISException;
			$el->errorCode = $e->faultcode;
			$el->errorMessage = $e->faultstring;
			return $el;
			}
    }
*/


}








class LongNumber {
    public $long;

  	/**
     *  Class constructor to easily initialize a new instance.
     *
     *	@param int $number - The long number.
     *	@return nothing.
     */
    function __construct($n = null) 
    {
        if ($n != null)
            $this->set($n);
    }  

    public function set($n){
         $this->long = new SoapVar($n, XSD_STRING, "string", 
                                    "http://www.w3.org/2001/XMLSchema");
		}
    
    public function __toString(){
        return (string) $this->long->enc_value;
		}

}


class DoubleNumber {
    public $double;

  	/**
     *  Class constructor to easily initialize a new instance.
     *
     *	@param int $number - The double number.
     *	@return nothing.
     */
    function __construct($n = null) 
    {
        if ($n != null)
            $this->set($n);
    }  

    public function set($n){
         $this->double = new SoapVar($n, XSD_STRING, "string", 
                                    "http://www.w3.org/2001/XMLSchema");
		}
    
    public function __toString(){
        return (string) $this->double->enc_value;
		}
	
	}


class PosizioneRifiuto {
	public $rifiutoPressoUnitaLocale; // RifiutoPressoUnitaLocale
	public $rifiutoFuoriUnitaLocale; // RifiutoFuoriUnitaLocale
	
	/**
     *  Class constructor to easily initialize a new instance.
     .
     *
     *	@param int $number - The long number.
     *	@return nothing.
     */
    
	function __construct($rifiutoPressoUnitaLocale = null, $rifiutoFuoriUnitaLocale = null){
        $this->rifiutoPressoUnitaLocale	= $rifiutoPressoUnitaLocale;
		$this->rifiutoFuoriUnitaLocale	= $rifiutoFuoriUnitaLocale;
		}  
	}

class RifiutoPressoUnitaLocale {
	public $idSISUnitaLocale; // string
	public $versioneUnitaLocale; // LongNumber
	}

class RifiutoFuoriUnitaLocale {
	public $indirizzo; // string
	public $codiceCatastale; // string
  	
	/**
     *  Class constructor to easily initialize a new instance.
     .
     *
     *	@param int $number - The long number.
     *	@return nothing.
     */
    
	function __construct($indirizzo, $codiceCatastale){
        $this->indirizzo = $indirizzo;
		$this->codiceCatastale = $codiceCatastale;
		}  
	}




class Flag {
    public $boolean;

  	/**
     *  Class constructor to easily initialize a new instance.
     .
     *
     *	@param int $number - The long number.
     *	@return nothing.
     */
    function __construct($f = false) 
    {
        $this->boolean = $f;
    }  
    
    public function __toString()
    {
        return (string) $this->boolean;
    }

}



class Catalogo {
    public $idCatalogo; // string
    public $description; // string

  	/**
     *  Class constructor to easily initialize a new instance.
     .
     *
     *	@param int $number - The long number.
     *	@return nothing.
     */
    function __construct($id = null) 
    {
        $this->idCatalogo = $id;
    }  
    
    public function __toString()
    {
        return (string) $this->idCatalogo;
    }

}



class RegistrazioneCronoCarico {
    public $quantitaDaScaricare; // LongNumber (*)
    public $idSISRegistrazioneCronoCarico; // string (*)

  	/**
     *  Class constructor to easily initialize a new instance.
     *
     *	@param string $id_sis - ID Registrazione Crono Carico
     *	@param int $quantita  - quantita' da scaricare.
     *	@return nothing.
     */
    function __construct($id_sis = null, $quantita = null) 
    {
        $this->idSISRegistrazioneCronoCarico = $id_sis;
        $this->quantitaDaScaricare = new LongNumber((string) $quantita);
    }  

}



class FiltroRegistrazioni {
  public $tipoRegistrazioneCrono; // Catalogo
  public $statoRegistrazioniCrono; // Catalogo
  public $codiceCerIIILivello; // Catalogo
  public $dataEoraRegistrazioneInizio; // dateTime
  public $dataEoraRegistrazioneFine; // dateTime
}



class RegistrazioneCrono_Base {
  public $codiceCerIIILivello; // Catalogo (*)
  public $statoFisicoRifiuto; // Catalogo (*)
  public $codRec_1013; // Catalogo
  public $quantita; // LongNumber
  public $descrizioneRifiuto; // string
  public $numeroNotifica; // string
  public $numeroSerieSpedizione; // string
  public $annotazioni; // string
  public $flagVeicoli_Dlgs_209_2003; // Flag
  public $flagVeicoli_Dlgs_Art231_152_2006; // Flag
  public $numeroVeicoliConferiti; // LongNumber
  public $categoriaRAEE; // Catalogo
  public $tipologiaRAEE; // Catalogo
  public $flagRiutilizzoAppIntera; // Flag
  public $flagOpRecuperoEnergia; // Flag
  public $flagOpRecuperoMateria; // Flag
  public $operazioneImpianto; // Catalogo
  public $idSISSede_impiantoOrigine; // string
  public $versioneSede_impiantoOrigine; // LongNumber
  public $idSISSede_impiantoDestinazione; // string
  public $versioneSede_impiantoDestinazione; // LongNumber
  public $descrizioneAltroStatoFisico; // string
  public $caratteristicaPericolo; // Catalogo
  public $idSISSede_consegnatoA; // string
  public $versioneSede_consegnatoA; // LongNumber
  public $posizioneRifiuto; // PosizioneRifiuto
  public $causaleCreazione; // Catalogo
  public $causaleModifica; // Catalogo
  public $flagConferitoDaPrivato; // Flag
  public $volume; // DoubleNumber
}


class SchedaSISTRI_Base {
    public $tipoRegCronologico; // Catalogo
    public $annotazioni; // string

    public $schedaSISTRI_produttore; // SchedaSISTRI_Produttore
    public $schedaSISTRI_trasportatore; // SchedaSISTRI_Trasportatore
    public $schedaSISTRI_destinatario; // SchedaSISTRI_Destinatario
    public $schedaSISTRI_prod_trasp_cp; // SchedaSISTRI_Prod_Trasp_CP
    public $idSISSedi_intermediari; // string

    public $versioniSedi_intermediari; // LongNumber
    public $idSISSede_consorzio; // string
    public $versioneSede_consorzio; // LongNumber
    public $idSISSede_unitaLocale; // string
    public $versioneSede_unitaLocale; // LongNumber
    public $nomePersonaDaContattare; // string
    public $cognomePersonaDaContattare; // string
    public $telefonoPersonaDaContattare; // string
    public $emailPersonaDaContattare; // string
}


class SchedaSISTRI_Produttore {
  public $codiceCerIIILivello; // Catalogo (*)
  public $caratteristichePericolo; // Catalogo
  public $descrizioneRifiuto; // string
  public $statoFisicoRifiuto; // Catalogo (*) 
  public $quantita; // LongNumber (*)
  public $numeroColli; // LongNumber
  public $flagPesoADestino; // Flag
  public $operazioneImpianto; // Catalogo (*) 
  public $tipoImballaggio; // Catalogo
  public $flagPrescrizioniParticolari; // Flag (*) 
  public $descrizionePrescrizioni; // string -
  public $flagTrasportoADR; // Flag -
  public $classeADR; // Catalogo -
  public $numeroONU; // Catalogo 
  public $flagDLGS_209_2003; // Flag
  public $flagDLGS_ART231_152_2006; // Flag
  public $numeroVeicoli; // LongNumber
  public $numeroCertificato; // string
  public $laboratorio; // string
  public $dataCertificato; // dateTime
  public $listaAllegati; // DocumentData_Base[] (?)
  public $codRec_1013; // Catalogo
  public $numeroNotifica; // string
  public $numeroSerieSpedizione; // string
  public $idSISSede_trasportatore; // string (*)
  public $versioneSede_trasportatore; // LongNumber
  public $idSISSede_destinatario; // string (*)
  public $versioneSede_destinatario; // LongNumber
  public $autorizzazione; // string
  public $quantita_150101; // LongNumber
  public $quantita_150104; // LongNumber
  public $quantita_150102; // LongNumber
  public $quantita_150107; // LongNumber
  public $quantita_150103; // LongNumber
  public $altroTipoImballaggio; // string
  public $quantitaTotale_150106; // LongNumber
}



class SchedaSISTRI_Trasportatore {
  public $targaAutomezzo; // string
  public $targaRimorchio; // string
  public $annotazioniConducente; // string
  public $dataInizioPianificata; // dateTime
  public $dataOraCarico; // dateTime
  public $annotazioniCarico; // string
  public $dataOraScarico; // dateTime
  public $annotazioniScarico; // string
  public $dataFinePianificata; // dateTime
  public $conducente; // string
  public $percorso; // string
  public $progressivoTratta; // LongNumber
  public $idSISSede_origineTratta; // string
  public $versioneSede_origineTratta; // LongNumber
  public $idSISSede_destinazioneTratta; // string
  public $versioneSede_destinazioneTratta; // LongNumber
  public $responsabileCarico; // string
  public $responsabileScarico; // string
  public $identificativoCarro; // string
  public $numeroTracce; // string
  public $identificativoNave; // string
  public $comandanteNave; // string
  public $tipoTrasporto; // Catalogo
}


class SchedaSISTRI_Destinatario {
  public $quantitaRicevuta; // LongNumber
  public $flagIncenerimento; // Flag
  public $esitoTrasporto; // Catalogo
  public $flagAttesaVerificaAnalitica; // Flag
}


class SchedaSISTRI_Prod_Trasp_CP {
  public $codiceCerIIILivello; // Catalogo
  public $caratteristichePericolo; // Catalogo
  public $descrizioneRifiuto; // string
  public $statoFisicoRifiuto; // Catalogo
  public $quantita; // LongNumber
  public $numeroColli; // LongNumber
  public $flagPesoADestino; // Flag
  public $operazioneImpianto; // Catalogo
  public $tipoImballaggio; // Catalogo
  public $flagPrescrizioniParticolari; // Flag
  public $descrizionePrescrizioni; // string
  public $flagTrasportoADR; // Flag
  public $classeADR; // Catalogo
  public $numeroONU; // Catalogo
  public $altroTipoImballaggio; // string
  public $numeroCertificato; // string
  public $laboratorio; // string
  public $dataCertificato; // dateTime
  public $listaAllegati; // DocumentData_Base
  public $idSISSede_destinatario; // string
  public $versioneSede_destinatario; // LongNumber
  public $autorizzazione; // string
  public $numeroNotifica; // string
  public $numeroSerieSpedizione; // string
  public $targaAutomezzo; // string
  public $targaRimorchio; // string
  public $annotazioniConducente; // string
  public $dataInizioPianificata; // dateTime
  public $dataFinePianificata; // dateTime
  public $dataOraCarico; // dateTime
  public $annotazioniCarico; // string
  public $dataOraScarico; // dateTime
  public $annotazioniScarico; // string
  public $conducente; // string
}



class Tratta_Base {
  public $progressivo; // LongNumber
  public $idSISSede_trasportatore; // string
  public $versioneSede_trasportatore; // LongNumber
  public $flagOperatoreLogistico; // Flag
}


class FiltroMovimentazioni {
  public $dataEoraMovimentazioneInizio; // dateTime
  public $dataEoraMovimentazioneFine; // dateTime
  public $movimentazioniDaMostrare; // string
}


class SISException {
  public $errorCode; // string
  public $errorMessage; // string
}



?>
