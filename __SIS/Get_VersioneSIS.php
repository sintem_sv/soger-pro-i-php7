<div style="width:264px;border: solid 1px #4D6980;background-color:#FFFFFF;padding:1px;margin-left:10px;">

	<div class="divName" style="text-align:center;height:25px;width:100%;background-color: #4D6980;color: #ffffff;"><span style="font-weight: bold;padding:5px;font-size: 13pt;font-family: verdana;text-transform: uppercase;">VERSIONE SIS</span></div>	

	<div style="padding:10px;">

		<table style="margin-bottom:20px;width:240px;">
	
			<?php 
			if(!is_null($SOGER->UserData['core_usersSIS_identity']) AND $SOGER->UserData['core_usersSIS_identity']!='') 
				$Identity = $SOGER->UserData['core_usersSIS_identity']; 
			else 
				$Identity = "non specificato"; 
			?>

			<tr>
				<td style="font-size:8pt;"><b>Identity:</b></td>
				<td style="font-size:8pt;text-align:right"><?php echo $Identity; ?></td>
			</tr>

			<?php 
			$CERT		= "__SIS/certificates/".$SOGER->UserData['core_impianticodfisc']."/crt_key.pem";
			
			try{
				$SIS		= new SIS_SOAP($SOGER->UserData['core_usersSIS_identity'], $CERT);
				$SIS_version = $SIS->GetVersioneSIS();
				$P_S		= explode("; ", $SIS_version);
				$Protocollo	= explode(": ", $P_S[0]);
				$Software	= explode(": ", $P_S[1]);
				$Protocollo = $Protocollo[1];
				$Software	= $Software[1];
				}
			catch(SoapFault $SIS_error){
				$SISexcept	= SIS_SOAP::getSISException($SIS_error);
				$Protocollo = "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n";
				$Software	= "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n";
				}
			?>

			<tr>
				<td style="font-size:8pt;"><b>Protocollo: </b></td>
				<td style="font-size:8pt;text-align:right"><?php echo $Protocollo; ?></td>
			</tr>

			<tr>
				<td style="font-size:8pt;"><b>SIS Sw: </b></td>
				<td style="font-size:8pt;text-align:right"><?php echo $Software; ?></td>
			</tr>

			<?php
			// certificato interoperabilitÓ
			if(file_exists("__SIS/certificates/".$SOGER->UserData['core_impianticodfisc']."/certificato.cer")){
				$gotCertificato		= 1;
				$certificato		= "Certificato caricato su server";
				$icona_certificato	= "certificato.gif";
				}
			else{
				$gotCertificato		= 0;
				$certificato		= "Certificato interoperabilitÓ non inviato";
				$icona_certificato	= "certificato_off.gif";
				}
			?>

			<tr>
				<td style="font-size:8pt;"><b>Certificato SIS: </b></td>
				<td style="font-size:8pt;text-align:right"><img src="__css/<?php echo $icona_certificato; ?>" alt="<?php echo $certificato; ?>" title="<?php echo $certificato; ?>" /></td>
			</tr>

			<?php
			// crt_key.pem ( certificato + private )
			if(file_exists("__SIS/certificates/".$SOGER->UserData['core_impianticodfisc']."/crt_key.pem")){
				$gotPk		= 1;
				$pk			= "Private key caricata su server";
				$icona_pk	= "private_key.gif";
				}
			else{
				$gotPk		= 0;
				$pk			= "Private key non caricata su server";
				$icona_pk	= "private_key_off.gif";
				}
			?>

			<tr>
				<td style="font-size:8pt;"><b>Private key: </b></td>
				<td style="font-size:8pt;text-align:right"><img src="__css/<?php echo $icona_pk; ?>" alt="<?php echo $pk; ?>" title="<?php echo $pk; ?>" /></td>
			</tr>

			<tr>
				<td colspan="2">
					<div style="width: 240px;height: 20px; text-align: center; margin-left: -10px;margin-top:10px;" id="UpPk">
						<span style="background-color: rgb(92, 125, 153); border: 1px solid rgb(255, 255, 255); width: 100%; padding: 5px;">
							<a style="color: rgb(255, 255, 255); text-decoration: none;" href="#">Carica private key sul server</a>
						</span>
						<input type="hidden" name="gotCertificato" id="gotCertificato" value="<?php echo $gotCertificato; ?>" />
						<input type="hidden" name="gotPk" id="gotPk" value="<?php echo $gotPk; ?>" />
					</div>
				</td>
			</tr>

		</table>

	</div>

</div>