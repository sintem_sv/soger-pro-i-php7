<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
require_once("SIS_SOAP_SSL.php");
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");

global $SOGER;

$CERT		= "certificates/".$SOGER->UserData['core_impianticodfisc']."/crt_key.pem";
$s			= new SIS_SOAP($SOGER->UserData['core_usersSIS_identity'], $CERT);
$Response	= Array();

$idSISMovimentazione	= $_POST['idSIS_movimentazione'];
//$idSISMovimentazione	= '3126062';


try {
	
	$UUID	= $s->generateUUID();
	$rv1	= $s->GetElencoSchedeSistri(
				array(  
				"idSISMovimentazione"=>$idSISMovimentazione,
				)
			);

	for($i=0;$i<count($rv1);$i++){
		
		$TipiReg_Trasportatori = array('TCP', 'ATR', 'TRS', 'TRC');

		if(in_array($rv1[$i]->tipoRegCronologico->idCatalogo, $TipiReg_Trasportatori)){
			$FirmaTrasportatore = true;
			$idSIS_SchedaTrasportatore = $rv1[$i]->idSIS;
			}
		else $FirmaTrasportatore = false;
		}
	
	if(!$FirmaTrasportatore){
		$Response['statoSchedaSistri_trasportatore']	= 'BOZZA';
		$Response['dataOraFirma']						= 'null';
		$Response['dataOraModifica']					= 'null';
		$Response['dataInizioPianificata']				= 'null';
		$Response['oraInizioPianificata']				= 'null';
		$Response['conducente']							= 'null';
		$Response['targaAutomezzo']						= 'null';
		$Response['targaRimorchio']						= 'null';

		$SQL="UPDATE user_movimenti_fiscalizzati SET statoSchedaSistri_trasportatore='".$Response['statoSchedaSistri_trasportatore']."' WHERE ID_MOV_F=".$_POST['ID_MOV_F'].";";
		$FEDIT->SDBWrite($SQL,true,false);

		}
	else{
		$rv2	= $s->GetSchedaSISTRI(
			array(  
			"idSISScheda"=>$idSIS_SchedaTrasportatore,
				)
			);
		
		$Response['statoSchedaSistri_trasportatore']	= 'FIRMATA';

		// se � in bozza...
		if(!is_null($rv2->dataFirma)){
			$dataOraFirma									= $rv2->dataFirma;
			$dataOraFirma_array								= explode("T",$dataOraFirma);
			$dataFirma_array								= explode("-",$dataOraFirma_array[0]);
			$dataFirma										= $dataFirma_array[2]."/".$dataFirma_array[1]."/".$dataFirma_array[0];
			$oraFirma_array									= explode(".",$dataOraFirma_array[1]);
			$oraFirma										= $oraFirma_array[0];
			$dataOraFirma									= $oraFirma . ' del ' . $dataFirma;
			}
		else{
			$dataOraFirma = '';
			}
		
		$dataOraModifica								= $rv2->dataModifica;
		$dataOraModifica_array							= explode("T",$dataOraModifica);
		$dataModifica_array								= explode("-",$dataOraModifica_array[0]);
		$dataModifica									= $dataModifica_array[2]."/".$dataModifica_array[1]."/".$dataModifica_array[0];
		$oraModifica_array								= explode(".",$dataOraModifica_array[1]);
		$oraModifica									= $oraModifica_array[0];
		
		$dataInizioPianificata							= $rv2->schedaSISTRI_base->schedaSISTRI_trasportatore->dataInizioPianificata;
		$dataInizioPianificata_array					= explode("T",$dataInizioPianificata);
		$dataInizio_array								= explode("-",$dataInizioPianificata_array[0]);
		$dataInizio										= $dataInizio_array[2]."/".$dataInizio_array[1]."/".$dataInizio_array[0];
		$oraInizio_array								= explode(".",$dataInizioPianificata_array[1]);
		$oraInizio										= $oraInizio_array[0];
				
		$Response['dataOraFirma']						= $dataOraFirma;
		$Response['dataOraModifica']					= $oraModifica . ' del ' . $dataModifica;
		$Response['dataInizioPianificata']				= $dataInizio;
		$Response['oraInizioPianificata']				= $oraInizio;
		$Response['conducente']							= $rv2->schedaSISTRI_base->schedaSISTRI_trasportatore->conducente;
		$Response['targaAutomezzo']						= $rv2->schedaSISTRI_base->schedaSISTRI_trasportatore->targaAutomezzo;
		$Response['targaRimorchio']						= $rv2->schedaSISTRI_base->schedaSISTRI_trasportatore->targaRimorchio;
		
		
		$SQL="UPDATE user_movimenti_fiscalizzati SET statoSchedaSistri_trasportatore='".$Response['statoSchedaSistri_trasportatore']."' WHERE ID_MOV_F=".$_POST['ID_MOV_F'].";";
		$FEDIT->SDBWrite($SQL,true,false);
		}

	} 
catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
	
	// Store error in DB	
	$SQL ="INSERT INTO `debug_sis` (`UUID`, `ID_IMP`, `ID_USR`, `SIS_identity`, `workmode`, `idSIS_sede`, `idSIS_regCrono`, `regCrono_type`, `TIME`, `errorCode`, `errorMessage`, `RequestHeaders`, `Request`, `ResponseHeaders`, `Response`) ";
	$SQL.="VALUES ('".$UUID."', '".$SOGER->UserData['core_usersID_IMP']."', '".$SOGER->UserData['core_usersID_USR']."', '".$SOGER->UserData['core_usersSIS_identity']."', '".$SOGER->UserData['workmode']."', '".$SOGER->UserData['core_usersidSIS_sede']."', '".$SOGER->UserData['core_usersidSIS_regCrono']."', '".$SOGER->UserData['core_usersregCrono_type']."', CURRENT_TIMESTAMP, '".addslashes($SISexcept->errorCode)."', '".addslashes($SISexcept->errorMessage)."', '".addslashes($s->__getLastRequestHeaders())."', '".addslashes($s->__getLastRequest())."', '".addslashes($s->__getLastResponseHeaders())."', '".addslashes($s->__getLastResponse())."');";
    $FEDIT->SDBWrite($SQL,true,false);
	
	$Response['statoSchedaSistri_trasportatore']	= 'null';
	$Response['dataOraFirma']						= 'null';
	$Response['dataOraModifica']					= 'null';
	$Response['dataInizioPianificata']				= 'null';
	$Response['oraInizioPianificata']				= 'null';
	$Response['conducente']							= 'null';
	$Response['targaAutomezzo']						= 'null';
	$Response['targaRimorchio']						= 'null';

	}

echo json_encode($Response);

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>