<?

if (!defined('STDIN'))
    header('Content-type: text/plain'); 

echo "Server: ", `uname -a`, "\n";

echo "----------------------------------\n";

echo "Dim int: " , PHP_INT_SIZE, "\n";
echo "Max int: " , PHP_INT_MAX,  "\n\n";

echo "[1]", "1310051666394", "\n";
echo "[2]", 1310051666394, "\n";
echo "[3]", 2147483647, "\n";

echo "----------------------------------\n";
printf("%x\n", 1310051666394);
printf("%x\n", 2147483647);

?>

