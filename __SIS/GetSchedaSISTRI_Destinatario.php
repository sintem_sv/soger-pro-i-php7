<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
require_once("SIS_SOAP_SSL.php");
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");

global $SOGER;

$CERT		= "certificates/".$SOGER->UserData['core_impianticodfisc']."/crt_key.pem";
$s			= new SIS_SOAP($SOGER->UserData['core_usersSIS_identity'], $CERT);
$Response	= Array();

$idSISMovimentazione	= $_POST['idSIS_movimentazione'];
//$idSISMovimentazione	= '3126062';


try {
	
	$UUID	= $s->generateUUID();
	$rv1	= $s->GetElencoSchedeSistri(
				array(  
				"idSISMovimentazione"=>$idSISMovimentazione,
				)
			);


	$FirmaDestinatario = false;
		
	for($i=0;$i<count($rv1);$i++){
		
		$TipiReg_Destinatari = array('GDI', 'GRS', 'GIC', 'GRA', 'GDR', 'GFV', 'GRR', 'RD1', 'GRC');

		if(in_array($rv1[$i]->tipoRegCronologico->idCatalogo, $TipiReg_Destinatari)){
			$FirmaDestinatario = true;
			$idSIS_SchedaDestinatario = $rv1[$i]->idSIS;
			}
		
		}
	
	if(!$FirmaDestinatario){
		$Response['statoSchedaSistri_destinatario']			= 'BOZZA';
		$Response['esitoTrasporto']							= 'null';
		$Response['flagAttesaVerificaAnalitica']			= 'null';
		$Response['dataConclusioneProcesso']				= 'null';
		$Response['oraConclusioneProcesso']					= 'null';
		$Response['quantitaRicevuta']						= 'null';
		}
	else{
		$rv2	= $s->GetSchedaSISTRI(
			array(  
			"idSISScheda"=>$idSIS_SchedaDestinatario,
				)
			);

		$Response['statoSchedaSistri_destinatario']			= 'FIRMATA';
		$Response['esitoTrasporto']				= $rv2->schedaSISTRI_base->schedaSISTRI_destinatario->esitoTrasporto->description;
		$dataConclusioneProcesso				= $rv2->schedaSISTRI_base->dataOraConclusioneProcesso;
		$dataConclusioneProcesso_array			= explode("T",$dataConclusioneProcesso);
		$dataConclusione_array					= explode("-",$dataConclusioneProcesso_array[0]);
		$dataConclusione						= $dataConclusione_array[2]."/".$dataConclusione_array[1]."/".$dataConclusione_array[0];
		$oraConclusione_array					= explode(".",$dataConclusioneProcesso_array[1]);
		$oraConclusione							= $oraConclusione_array[0];
		if($rv2->schedaSISTRI_base->schedaSISTRI_destinatario->flagAttesaVerificaAnalitica->boolean)
			$flagAttesaVerificaAnalitica		= 's�';
		else
			$flagAttesaVerificaAnalitica		= 'no';
		$Response['flagAttesaVerificaAnalitica']= $flagAttesaVerificaAnalitica;
		$Response['dataConclusioneProcesso']	= $dataConclusione;
		$Response['oraConclusioneProcesso']		= $oraConclusione;
		$Response['quantitaRicevuta']			= ($rv2->schedaSISTRI_base->schedaSISTRI_destinatario->quantitaRicevuta->long / 1000000).' kg';

		}

	} 
catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
	
	// Store error in DB	
	$SQL ="INSERT INTO `debug_sis` (`UUID`, `ID_IMP`, `ID_USR`, `SIS_identity`, `workmode`, `idSIS_sede`, `idSIS_regCrono`, `regCrono_type`, `TIME`, `errorCode`, `errorMessage`, `RequestHeaders`, `Request`, `ResponseHeaders`, `Response`) ";
	$SQL.="VALUES ('".$UUID."', '".$SOGER->UserData['core_usersID_IMP']."', '".$SOGER->UserData['core_usersID_USR']."', '".$SOGER->UserData['core_usersSIS_identity']."', '".$SOGER->UserData['workmode']."', '".$SOGER->UserData['core_usersidSIS_sede']."', '".$SOGER->UserData['core_usersidSIS_regCrono']."', '".$SOGER->UserData['core_usersregCrono_type']."', CURRENT_TIMESTAMP, '".addslashes($SISexcept->errorCode)."', '".addslashes($SISexcept->errorMessage)."', '".addslashes($s->__getLastRequestHeaders())."', '".addslashes($s->__getLastRequest())."', '".addslashes($s->__getLastResponseHeaders())."', '".addslashes($s->__getLastResponse())."');";
    $FEDIT->SDBWrite($SQL,true,false);
	
	$Response['statoSchedaSistri_destinatario']			= 'null';
	$Response['esitoTrasporto']							= 'null';
	$Response['flagAttesaVerificaAnalitica']			= 'null';
	$Response['dataConclusioneProcesso']				= 'null';
	$Response['oraConclusioneProcesso']					= 'null';
	$Response['quantitaRicevuta']						= 'null';

	}

echo json_encode($Response);

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>