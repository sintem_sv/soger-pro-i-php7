<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
require_once("SIS_SOAP_SSL.php");
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");

global $SOGER;

$CERT		= "certificates/".$SOGER->UserData['core_impianticodfisc']."/crt_key.pem";
$s			= new SIS_SOAP($SOGER->UserData['core_usersSIS_identity'], $CERT);
$Response	= Array();

try {
	
	$UUID	= $s->generateUUID();
	$rv1	= $s->GetSchedaSISTRI(
				array(  
				"idSISScheda"=>$_POST['idSIS_scheda'],
				)
			);

	$statoSchedaSistri = $rv1->statoSchedaSistri->description;
	$scheda	= $s->GetMovimentazioneByIdScheda(array( "idSISScheda" => $_POST['idSIS_scheda'] ));
	$numero_scheda = $scheda->idSISTRI;

	// aggiorno idSIS in tabella movimenti
 	$SQL ="UPDATE user_movimenti_fiscalizzati ";
	$SQL.="SET statoSchedaSistri='".$statoSchedaSistri."', numero_scheda='".$numero_scheda."' WHERE ID_MOV_F=".$_POST['ID_MOV_F']." ";
	$SQL.="AND user_movimenti_fiscalizzati.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' ";
	$SQL.="AND user_movimenti_fiscalizzati.".$SOGER->UserData['workmode']."=1 ";
	$SQL.="AND user_movimenti_fiscalizzati.idSIS_scheda=".$_POST['idSIS_scheda'].";";
	$FEDIT->SDBWrite($SQL,true,false);

	} 
catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
	
	// Store error in DB	
	$SQL ="INSERT INTO `debug_sis` (`UUID`, `ID_IMP`, `ID_USR`, `SIS_identity`, `workmode`, `idSIS_sede`, `idSIS_regCrono`, `regCrono_type`, `TIME`, `errorCode`, `errorMessage`, `RequestHeaders`, `Request`, `ResponseHeaders`, `Response`) ";
	$SQL.="VALUES ('".$UUID."', '".$SOGER->UserData['core_usersID_IMP']."', '".$SOGER->UserData['core_usersID_USR']."', '".$SOGER->UserData['core_usersSIS_identity']."', '".$SOGER->UserData['workmode']."', '".$SOGER->UserData['core_usersidSIS_sede']."', '".$SOGER->UserData['core_usersidSIS_regCrono']."', '".$SOGER->UserData['core_usersregCrono_type']."', CURRENT_TIMESTAMP, '".addslashes($SISexcept->errorCode)."', '".addslashes($SISexcept->errorMessage)."', '".addslashes($s->__getLastRequestHeaders())."', '".addslashes($s->__getLastRequest())."', '".addslashes($s->__getLastResponseHeaders())."', '".addslashes($s->__getLastResponse())."');";
    $FEDIT->SDBWrite($SQL,true,false);

	// leggo statoSchedaSistri da tabella movimenti
 	$SQL ="SELECT statoSchedaSistri FROM user_movimenti_fiscalizzati ";
	$SQL.="WHERE ID_MOV_F=".$_POST['ID_MOV_F']." ";
	$SQL.="AND user_movimenti_fiscalizzati.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' ";
	$SQL.="AND user_movimenti_fiscalizzati.".$SOGER->UserData['workmode']."=1 ";
	$SQL.="AND user_movimenti_fiscalizzati.idSIS_scheda=".$_POST['idSIS_scheda'].";";
	$FEDIT->SdbRead($SQL,"statoSchedaSistri");
	$statoSchedaSistri = $FEDIT->statoSchedaSistri[0]['statoSchedaSistri'];
	}

echo $statoSchedaSistri;

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>