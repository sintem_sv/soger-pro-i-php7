<?php

$id_sis_registro	= $SOGER->UserData['core_usersidSIS_regCrono'];
$tipo_registro		= new Catalogo($SOGER->UserData['core_usersregCrono_type']);

$scheda				= new SchedaSISTRI_Produttore;

// Select data from Soger
$SQL ="SELECT lov_cer.COD_CER, lov_cer.description AS DESC_CER, lov_cer.PERICOLOSO, user_schede_rifiuti.H1, user_schede_rifiuti.H2, user_schede_rifiuti.H3A, user_schede_rifiuti.H3B, user_schede_rifiuti.H4, user_schede_rifiuti.H5, user_schede_rifiuti.H6, user_schede_rifiuti.H7, user_schede_rifiuti.H8, user_schede_rifiuti.H9, user_schede_rifiuti.H10, user_schede_rifiuti.H11, user_schede_rifiuti.H12, user_schede_rifiuti.H13, user_schede_rifiuti.H14, user_schede_rifiuti.H15, user_schede_rifiuti.ClassificazioneHP, user_schede_rifiuti.HP1, user_schede_rifiuti.HP2, user_schede_rifiuti.HP3, user_schede_rifiuti.HP4, user_schede_rifiuti.HP5, user_schede_rifiuti.HP6, user_schede_rifiuti.HP7, user_schede_rifiuti.HP8, user_schede_rifiuti.HP9, user_schede_rifiuti.HP10, user_schede_rifiuti.HP11, user_schede_rifiuti.HP12, user_schede_rifiuti.HP13, user_schede_rifiuti.HP14, user_schede_rifiuti.HP15, user_schede_rifiuti.descrizione, quantita, pesoN, user_schede_rifiuti.ID_SF, lov_operazioni_rs.description as destinatoA, user_impianti_destinatari.idSIS as idSIS_D, user_impianti_destinatari.versione as Versione_D, user_autorizzazioni_dest.num_aut as Autorizzazione_D, user_impianti_trasportatori.idSIS as idSIS_T, user_impianti_trasportatori.versione as Versione_T, user_movimenti_fiscalizzati.ID_AZI, user_impianti_intermediari.idSIS as idSIS_I, user_impianti_intermediari.versione as Versione_I, USER_nome, USER_cognome, USER_email, USER_telefono, COLLI, user_movimenti_fiscalizzati.prescrizioni_mov, user_schede_rifiuti.imballaggio, ID_TIPO_IMBALLAGGIO, ALTRO_TIPO_IMBALLAGGIO, lov_num_onu.description as ONU, user_movimenti_fiscalizzati.ID_ONU, lov_num_onu.classe AS ID_CLASSE_ADR, user_movimenti_fiscalizzati.adr, user_movimenti_fiscalizzati.QL, user_schede_rifiuti.ONU_per, user_schede_rifiuti.ADR_NOTE, user_movimenti_fiscalizzati.CAR_NumDocumento, user_movimenti_fiscalizzati.CAR_DataAnalisi, user_movimenti_fiscalizzati.CAR_Laboratorio, VER_DESTINO, user_movimenti_fiscalizzati.NOTEF, user_schede_rifiuti.NOTEF AS NOTE_RIFIUTO, user_movimenti_fiscalizzati.FKEcfiscP, user_movimenti_fiscalizzati.FKEcfiscD, user_movimenti_fiscalizzati.FKEcfiscT, user_movimenti_fiscalizzati.ID_CAR, user_movimenti_fiscalizzati.ID_COMM, user_movimenti_fiscalizzati.ID_AZP, user_movimenti_fiscalizzati.ID_AZT, user_movimenti_fiscalizzati.ID_AZD, user_autorizzazioni_dest.note AS NoteAuthD, user_autorizzazioni_trasp.note AS NoteAuthT, user_movimenti_fiscalizzati.ID_AUTHI,lov_misure.description AS Umisura, lov_operazioni_rs_sec.description AS OP_RS2, lov_operazioni_rs_sec.ID_OP_RS2, user_movimenti_fiscalizzati.ID_RIF, user_impianti_produttori.FuoriUnitaLocale, user_impianti_produttori.description AS indirizzoP, lov_comuni_istat.codice_catastale, idSIS_scheda, ";
$SQL.="user_movimenti_fiscalizzati.ID_UIMP,user_movimenti_fiscalizzati.ID_UIMT,user_movimenti_fiscalizzati.ID_UIMD,user_movimenti_fiscalizzati.ID_UIMI "; 
$SQL.="FROM user_movimenti_fiscalizzati ";
$SQL.="JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF ";
$SQL.="JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
$SQL.="JOIN lov_operazioni_rs ON user_movimenti_fiscalizzati.ID_OP_RS=lov_operazioni_rs.ID_OP_RS ";
$SQL.="JOIN user_impianti_destinatari ON user_movimenti_fiscalizzati.ID_UIMD=user_impianti_destinatari.ID_UIMD ";
$SQL.="JOIN user_autorizzazioni_dest ON user_movimenti_fiscalizzati.ID_AUTHD=user_autorizzazioni_dest.ID_AUTHD ";
$SQL.="JOIN user_impianti_trasportatori ON user_movimenti_fiscalizzati.ID_UIMT=user_impianti_trasportatori.ID_UIMT ";
$SQL.="LEFT JOIN user_impianti_intermediari ON user_impianti_intermediari.ID_UIMI=user_movimenti_fiscalizzati.ID_UIMI ";
$SQL.="LEFT JOIN lov_num_onu ON user_movimenti_fiscalizzati.ID_ONU=lov_num_onu.ID_ONU ";
$SQL.="JOIN lov_misure ON lov_misure.ID_UMIS=user_schede_rifiuti.ID_UMIS  ";
$SQL.="JOIN user_impianti_produttori ON user_movimenti_fiscalizzati.ID_UIMP=user_impianti_produttori.ID_UIMP  ";
$SQL.="JOIN lov_comuni_istat ON lov_comuni_istat.ID_COM=user_impianti_produttori.ID_COM  ";
$SQL.="LEFT JOIN lov_operazioni_rs_sec ON user_autorizzazioni_dest.ID_OP_RS2=lov_operazioni_rs_sec.ID_OP_RS2  ";
$SQL.="JOIN user_autorizzazioni_trasp ON user_movimenti_fiscalizzati.ID_AUTHT=user_autorizzazioni_trasp.ID_AUTHT  ";
$SQL.="WHERE ID_MOV_F=".$ID_MOV_F." AND user_movimenti_fiscalizzati.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND user_movimenti_fiscalizzati.".$SOGER->UserData['workmode']."=1;";

$FEDIT->SdbRead($SQL,"Scarico");





################################################################################################################

$NOTEformulario		= $FEDIT->Scarico[0]["NOTEF"];
$NOTEformularioADR	= "";
$annotazioni		= "";

# NOTE MANUALI
if(is_null($NOTEformulario) AND trim($NOTEformulario)=='')
	$NOTEformulario = "";
else
	$NOTEformulario = $NOTEformulario."\n";

# NOTE RELATIVE AL RIFIUTO
if(!is_null($FEDIT->Scarico[0]["NOTE_RIFIUTO"]) AND trim($FEDIT->Scarico[0]["NOTE_RIFIUTO"])!='')
	$NOTEformulario.= $FEDIT->Scarico[0]["NOTE_RIFIUTO"]."\n";

if(!is_null($FEDIT->Scarico[0]['NoteAuthD']) AND $FEDIT->Scarico[0]['NoteAuthD']!="")
	$NOTEformulario.="Note aut. dest.: ".$FEDIT->Scarico[0]['NoteAuthD']."\n";

if(!is_null($FEDIT->Scarico[0]['NoteAuthT']) AND $FEDIT->Scarico[0]['NoteAuthT']!="")
	$NOTEformulario.="Note aut. trasp.: ".$FEDIT->Scarico[0]['NoteAuthT']."\n";


if($SOGER->UserData["core_usersFRM_DIS_INT"]=="0" && $FEDIT->Scarico[0]['ID_AZI']!="0" && !is_null($FEDIT->Scarico[0]['ID_AZI'])) {
	$sql = "SELECT user_aziende_intermediari.description AS intDes,user_aziende_intermediari.codfisc,user_aziende_intermediari.piva,user_impianti_intermediari.telefono,user_impianti_intermediari.email,user_impianti_intermediari.cellulare,user_impianti_intermediari.printRecapitoInFir,user_aziende_intermediari.indirizzo,user_aziende_intermediari.esclusoFIR";
        $sql .= ",lov_comuni_istat.description AS comDes,lov_comuni_istat.shdes_prov";
        $sql .= " FROM user_impianti_intermediari";
        $sql .= " JOIN lov_comuni_istat ON user_impianti_intermediari.ID_COM=lov_comuni_istat.ID_COM";
        $sql .= " JOIN user_aziende_intermediari ON user_aziende_intermediari.ID_AZI=user_impianti_intermediari.ID_AZI";
        $sql .= " WHERE user_impianti_intermediari.ID_UIMI=".$FEDIT->Scarico[0]['ID_UIMI'].";";
	$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
	
	if($FEDIT->DbRecordSet[0]["esclusoFIR"]==0){									
		$Intermediario = $FEDIT->DbRecordSet[0]["intDes"] . " - " . $FEDIT->DbRecordSet[0]["indirizzo"] . " - " . $FEDIT->DbRecordSet[0]["comDes"] . " (" . $FEDIT->DbRecordSet[0]["shdes_prov"] . ") - Codice Fiscale: " . $FEDIT->DbRecordSet[0]["codfisc"];
	
		if($FEDIT->DbRecordSet[0]["codfisc"]!=$FEDIT->DbRecordSet[0]["piva"] && $FEDIT->DbRecordSet[0]["piva"]!="")
			$Intermediario.=" P.Iva: ".$FEDIT->DbRecordSet[0]["piva"];				
		
		if($FEDIT->DbRecordSet[0]['printRecapitoInFir']==1){
                    if($FEDIT->DbRecordSet[0]['telefono']!=""){
                        $Intermediario.=" Tel: ".$FEDIT->DbRecordSet[0]['telefono'];
                        }
                    elseif($FEDIT->DbRecordSet[0]['cellulare']!=""){
                        $Intermediario.=" Cell: ".$FEDIT->DbRecordSet[0]['cellulare'];
                        }
                    elseif($FEDIT->DbRecordSet[0]['email']!=""){
                        $Intermediario.=" Mail: ".$FEDIT->DbRecordSet[0]['email'];
                        }
                    }
		if(!is_null($FEDIT->Scarico[0]['ID_AUTHI'])){
			$sql ="SELECT num_aut, rilascio, note FROM user_autorizzazioni_interm WHERE ID_AUTHI=".$FEDIT->Scarico[0]['ID_AUTHI'];
			$FEDIT->SDBRead($sql,"DbRecordSetAuthI",true,false);
			$rilascio=explode("-",$FEDIT->DbRecordSetAuthI[0]['rilascio']);
			$rilascio=$rilascio[2]."/".$rilascio[1]."/".$rilascio[0];
			$Intermediario.=" Autorizzazione: ".$FEDIT->DbRecordSetAuthI[0]['num_aut']." del ".$rilascio;
			if($FEDIT->DbRecordSetAuthI[0]['note']!=''){
				$Intermediario.=" Note: ".addslashes($FEDIT->DbRecordSetAuthI[0]['note']);
				}
			}

		$NOTEformulario.="INTERMEDIARIO: " . $Intermediario."\n";
		}
	}

# note relative al trasporto ADR
if($SOGER->UserData["core_usersFRM_ADR"]=="1") {
	if(!is_null($FEDIT->Scarico[0]['ID_ONU']) && $FEDIT->Scarico[0]['ID_ONU']!=0) {
		$sql = "SELECT ONU_DES as des, lov_num_onu.description as num, eti, imb, classe, codice_galleria, ADR_2_1_3_5_5, ADR_PERICOLOSO_AMBIENTE, categoria_trasporto FROM user_schede_rifiuti JOIN lov_num_onu ON lov_num_onu.ID_ONU=user_schede_rifiuti.ID_ONU WHERE ID_RIF=".$FEDIT->Scarico[0]['ID_RIF'].";";
		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
		
		if($FEDIT->DbRecordSet[0]["des"]==""){
			$sql = "SELECT des, description as num FROM lov_num_onu WHERE ID_ONU=".$FEDIT->Scarico[0]['ID_ONU'].";";
			$FEDIT->SDBRead($sql,"DbRecordSetONU",true,false);
			$ONUdes=$FEDIT->DbRecordSetONU[0]["des"];
			$ONUnum=$FEDIT->DbRecordSetONU[0]["num"];
			$CategoriaADR=$FEDIT->DbRecordSetONU[0]["categoria_trasporto"];
			}
		else{
			$ONUdes=$FEDIT->DbRecordSet[0]["des"];
			$ONUnum=$FEDIT->DbRecordSet[0]["num"];
			$CategoriaADR=$FEDIT->DbRecordSet[0]["categoria_trasporto"];
			}
		
		
		if($FEDIT->DbRecsNum>0) {

			$ImballaggioVNR = ($FEDIT->Scarico[0]['COD_CER']=="150110" && $ONUnum!='3509')? true:false;
			
			if($ImballaggioVNR){
				$NOTEformularioADR.="IMBALLAGGI VUOTI";								
				}
			else{
				if($FEDIT->DbRecordSet[0]["ADR_2_1_3_5_5"]==0)
					$NOTEformularioADR.="UN " . $ONUnum . " RIFIUTO " . $ONUdes;
				else
					$NOTEformularioADR.="UN " . $ONUnum . " " . $ONUdes;
				}

			if(!is_null($FEDIT->Scarico[0]['ONU_per']) && $FEDIT->Scarico[0]['ONU_per']!="") {
				$NOTEformularioADR.= ", (".$FEDIT->Scarico[0]['ONU_per'].")";
				}

			if(!is_null($FEDIT->DbRecordSet[0]["eti"]) && $FEDIT->DbRecordSet[0]["eti"]!="") {
                                $etis = explode("+", $FEDIT->DbRecordSet[0]["eti"]);
                                $other_etis = "";
                                $oe = array();
                                if(count($etis)>1){
                                    $other_etis.= " (";
                                    for($e=1;$e<count($etis);$e++){
                                        $oe[] = $etis[$e];
                                    }
                                    $other_etis.= implode(", ", $oe);
                                    $other_etis.= ")";
                                }
                                $eti = $etis[0].$other_etis;
				$NOTEformularioADR.= ", " . $eti;
				}

			if(!$ImballaggioVNR){
				if(!is_null($FEDIT->DbRecordSet[0]["imb"]) && $FEDIT->DbRecordSet[0]["imb"]!="") {
					$NOTEformularioADR.= ", " . $FEDIT->DbRecordSet[0]["imb"];
					}
				}

			// codice restrizione galleria
			if(!$ImballaggioVNR)
				$NOTEformularioADR.= " " . $FEDIT->DbRecordSet[0]["codice_galleria"];

			// NOTE ADR (es. codice imballaggio)
			if(!is_null($FEDIT->Scarico[0]['ADR_NOTE']) AND trim($FEDIT->Scarico[0]['ADR_NOTE'])!='')
				$NOTEformularioADR.=" - ".$FEDIT->Scarico[0]['ADR_NOTE']." - ";

			// CAP. 5.4.1.1.18 - PERICOLOSO PER L'AMBIENTE 
			$ONU_ESCLUSI = array("3082", "3077");
			if(!in_array($ONUnum, $ONU_ESCLUSI) AND $FEDIT->DbRecordSet[0]["ADR_PERICOLOSO_AMBIENTE"]==1)
				$NOTEformularioADR.=" PERICOLOSO PER L'AMBIENTE. ";

			// CAP. 2.1.3.5.5
			if($FEDIT->DbRecordSet[0]["ADR_2_1_3_5_5"]==1)
				$NOTEformularioADR.=" RIFIUTI CONFORMI AL CAP. 2.1.3.5.5 . ";

			// Oli in ADR
			$CER_OLI = array("120106", "120107", "120110", "120119", "130101", "130109", "130110", "130111", "130112", "130113", "130204", "130205", "130206", "130207", "130208", "130301", "130306", "130307", "130308", "130309", "130310", "130401", "130402", "130403", "130506", "130701", "130702", "130703");
			$ONU_OLI = array("3082", "3077");
			if($SOGER->UserData['core_usersFRM_ADR_OLI']==1 AND in_array($FEDIT->Scarico[0]['COD_CER'], $CER_OLI) AND in_array($ONUnum, $ONU_OLI))
				$NOTEformularioADR.= "Classificazione ADR conforme al CAP. 2.1.3.1 e 2.1.3.9 . ";

			// classe 6.2
			if($FEDIT->DbRecordSet[0]['classe']=='6.2'){
				## riferimento del produttore
				if($FEDIT->Scarico[0]['ID_UIMP']<>'0000000'){
					$SQL_UIMP = "SELECT ONU_62 FROM user_impianti_produttori WHERE ID_UIMP=".$FEDIT->Scarico[0]['ID_UIMP'].";";
					$FEDIT->SDBRead($SQL_UIMP,"DbRecordSet62UIMP",true,false);
					if(!is_null($FEDIT->DbRecordSet62UIMP[0]["ONU_62"]) && $FEDIT->DbRecordSet62UIMP[0]["ONU_62"]!='')
						$NOTEformularioADR.="Rifiuto classe 6.2 - Persona di riferimento per il produttore: ".$FEDIT->DbRecordSet62UIMP[0]["ONU_62"].". ";
				}
				## riferimento del trasportatore
				if($FEDIT->Scarico[0]['ID_UIMT']<>'0000000'){
					$SQL_UIMT = "SELECT ONU_62 FROM user_impianti_trasportatori WHERE ID_UIMT=".$FEDIT->Scarico[0]['ID_UIMT'].";";
					$FEDIT->SDBRead($SQL_UIMT,"DbRecordSet62UIMT",true,false);
					if(!is_null($FEDIT->DbRecordSet62UIMT[0]["ONU_62"]) && $FEDIT->DbRecordSet62UIMT[0]["ONU_62"]!='')
						$NOTEformularioADR.="Rifiuto classe 6.2 - Persona di riferimento per il trasportatore: ".$FEDIT->DbRecordSet62UIMT[0]["ONU_62"].". ";
				}
				## riferimento del destinatario
				if($FEDIT->Scarico[0]['ID_UIMD']<>'0000000'){
					$SQL_UIMD = "SELECT ONU_62 FROM user_impianti_destinatari WHERE ID_UIMD=".$FEDIT->Scarico[0]['ID_UIMD'].";";
					$FEDIT->SDBRead($SQL_UIMD,"DbRecordSet62UIMD",true,false);
					if(!is_null($FEDIT->DbRecordSet62UIMD[0]["ONU_62"]) && $FEDIT->DbRecordSet62UIMD[0]["ONU_62"]!='')
						$NOTEformularioADR.="Rifiuto classe 6.2 - Persona di riferimento per il destinatario: ".$FEDIT->DbRecordSet62UIMD[0]["ONU_62"].". ";
				}
				## riferimento del intermediario
				if($FEDIT->Scarico[0]['ID_UIMI']<>'0000000'){
					$SQL_UIMI = "SELECT ONU_62 FROM user_impianti_intermediari WHERE ID_UIMI=".$FEDIT->Scarico[0]['ID_UIMI'].";";
					$FEDIT->SDBRead($SQL_UIMI,"DbRecordSet62UIMI",true,false);
					if(!is_null($FEDIT->DbRecordSet62UIMI[0]["ONU_62"]) && $FEDIT->DbRecordSet62UIMI[0]["ONU_62"]!='')
						$NOTEformularioADR.="Rifiuto classe 6.2 - Persona di riferimento per l'intermediario: ".$FEDIT->DbRecordSet62UIMI[0]["ONU_62"].". ";
				}
			}

			if($FEDIT->Scarico[0]['QL']=="1") {
				if(!$ImballaggioVNR){
					$NOTEformularioADR .= " Esente dal trasporto ADR per unit� di trasporto, Cap. 1.1.3.6, categoria di trasporto ".$CategoriaADR.". ";
					}
				else{
					$NOTEformularioADR .= " Esente dal trasporto ADR per unit� di trasporto, Cap. 1.1.3.6, categoria di trasporto 4. ";
					}
				}

			$NOTEformulario .= $NOTEformularioADR."\n";

		}
	}
}

if($FEDIT->Scarico[0]['Umisura']!="Kg.")
	$NOTEformulario .= "Quantit�: ". $FEDIT->Scarico[0]['quantita']." ".$FEDIT->Scarico[0]['Umisura']."\n";
	
if($SOGER->UserData["core_usersFRM_PRINT_CER"]=="1")
	$NOTEformulario .= "NOME CER: ".$FEDIT->Scarico[0]['DESC_CER']."\n";

if(!is_null($FEDIT->Scarico[0]['ID_OP_RS2']) && $FEDIT->Scarico[0]['ID_OP_RS2']!="0" && $SOGER->UserData["core_usersOPRDsec"]=="1")
	$NOTEformulario .= "Altra causale di recupero/smaltimento autorizzata: " . $FEDIT->Scarico[0]['OP_RS2']. "\n";


################################################################################################################






// codice CER
$CER_array=str_split($FEDIT->Scarico[0]['COD_CER'], 2);
$CER=$CER_array[0].".".$CER_array[1].".".$CER_array[2];
$scheda->codiceCerIIILivello  = new Catalogo($CER);

// CLASSI DI PERICOLSO
$NOTA_CLASSIH = "";
if($FEDIT->Scarico[0]['PERICOLOSO']=='1'){

	## CLASSI H
	if($FEDIT->Scarico[0]['ClassificazioneHP']=='0'){
		if($FEDIT->Scarico[0]['H1']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H1");
		if($FEDIT->Scarico[0]['H2']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H2");
		if($FEDIT->Scarico[0]['H3A']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H3A");
		if($FEDIT->Scarico[0]['H3B']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H3B");
		if($FEDIT->Scarico[0]['H4']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H4");
		if($FEDIT->Scarico[0]['H5']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H5");
		if($FEDIT->Scarico[0]['H6']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H6");
		if($FEDIT->Scarico[0]['H7']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H7");
		if($FEDIT->Scarico[0]['H8']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H8");
		if($FEDIT->Scarico[0]['H9']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H9");
		if($FEDIT->Scarico[0]['H10']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H10");
		if($FEDIT->Scarico[0]['H11']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H11");
		if($FEDIT->Scarico[0]['H12']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H12");
		if($FEDIT->Scarico[0]['H13']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H13");
		if($FEDIT->Scarico[0]['H14']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H14");
		if($FEDIT->Scarico[0]['H15']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H15");
		}
	## CLASSI HP
	else{
		if($FEDIT->Scarico[0]['HP1']=='1') $scheda->caratteristichePericolo[] = new Catalogo("HP1");
		if($FEDIT->Scarico[0]['HP2']=='1') $scheda->caratteristichePericolo[] = new Catalogo("HP2");
		if($FEDIT->Scarico[0]['HP3']=='1') $scheda->caratteristichePericolo[] = new Catalogo("HP3");
		if($FEDIT->Scarico[0]['HP4']=='1') $scheda->caratteristichePericolo[] = new Catalogo("HP4");
		if($FEDIT->Scarico[0]['HP5']=='1') $scheda->caratteristichePericolo[] = new Catalogo("HP5");
		if($FEDIT->Scarico[0]['HP6']=='1') $scheda->caratteristichePericolo[] = new Catalogo("HP6");
		if($FEDIT->Scarico[0]['HP7']=='1') $scheda->caratteristichePericolo[] = new Catalogo("HP7");
		if($FEDIT->Scarico[0]['HP8']=='1') $scheda->caratteristichePericolo[] = new Catalogo("HP8");
		if($FEDIT->Scarico[0]['HP9']=='1') $scheda->caratteristichePericolo[] = new Catalogo("HP9");
		if($FEDIT->Scarico[0]['HP10']=='1') $scheda->caratteristichePericolo[] = new Catalogo("HP10");
		if($FEDIT->Scarico[0]['HP11']=='1') $scheda->caratteristichePericolo[] = new Catalogo("HP11");
		if($FEDIT->Scarico[0]['HP12']=='1') $scheda->caratteristichePericolo[] = new Catalogo("HP12");
		if($FEDIT->Scarico[0]['HP13']=='1') $scheda->caratteristichePericolo[] = new Catalogo("HP13");
		if($FEDIT->Scarico[0]['HP14']=='1') $scheda->caratteristichePericolo[] = new Catalogo("HP14");
		if($FEDIT->Scarico[0]['HP15']=='1') $scheda->caratteristichePericolo[] = new Catalogo("HP15");
		// classi H nelle annotazioni
		if($FEDIT->Scarico[0]['H1']=='1') $NOTA_CLASSIH.=" H1";
		if($FEDIT->Scarico[0]['H2']=='1') $NOTA_CLASSIH.=" H2";
		if($FEDIT->Scarico[0]['H3A']=='1') $NOTA_CLASSIH.=" H3A";
		if($FEDIT->Scarico[0]['H3B']=='1') $NOTA_CLASSIH.=" H3B";
		if($FEDIT->Scarico[0]['H4']=='1') $NOTA_CLASSIH.=" H4";
		if($FEDIT->Scarico[0]['H5']=='1') $NOTA_CLASSIH.=" H5";
		if($FEDIT->Scarico[0]['H6']=='1') $NOTA_CLASSIH.=" H6";
		if($FEDIT->Scarico[0]['H7']=='1') $NOTA_CLASSIH.=" H7";
		if($FEDIT->Scarico[0]['H8']=='1') $NOTA_CLASSIH.=" H8";
		if($FEDIT->Scarico[0]['H9']=='1') $NOTA_CLASSIH.=" H9";
		if($FEDIT->Scarico[0]['H10']=='1') $NOTA_CLASSIH.=" H10";
		if($FEDIT->Scarico[0]['H11']=='1') $NOTA_CLASSIH.=" H11";
		if($FEDIT->Scarico[0]['H12']=='1') $NOTA_CLASSIH.=" H12";
		if($FEDIT->Scarico[0]['H13']=='1') $NOTA_CLASSIH.=" H13";
		if($FEDIT->Scarico[0]['H14']=='1') $NOTA_CLASSIH.=" H14";
		if($FEDIT->Scarico[0]['H15']=='1') $NOTA_CLASSIH.=" H15";
		}
	}

# classi H?
//if($SOGER->UserData["core_usersPRNT_CLASSIH"]=="1") {
//	if(trim($NOTA_CLASSIH)!='')
//		$NOTEformulario .= "Classificazione secondo Direttiva 2008/98/CE: ".$NOTA_CLASSIH;
//	}


// descrizione
$scheda->descrizioneRifiuto   = utf8_encode($FEDIT->Scarico[0]['descrizione']);

// stato fisico
switch($FEDIT->Scarico[0]['ID_SF']){
	case 1: // Solido pulverulento -> In polvere o pulverulenti
		$ID_STATO_FISICO_RIFIUTO = '01';
		break;
	case 2: // Solido non pulverulento -> Solidi
		$ID_STATO_FISICO_RIFIUTO = '02';
		break;
	case 3: // Fangoso palabile -> Fangosi
		$ID_STATO_FISICO_RIFIUTO = '04';
		break;
	case 4: // Liquido ->  Liquidi
		$ID_STATO_FISICO_RIFIUTO = '05';
		break;
	}
$scheda->statoFisicoRifiuto   = new Catalogo($ID_STATO_FISICO_RIFIUTO);

// pesoN convertito in mg
$milligrammi		= $FEDIT->Scarico[0]['pesoN']*1000000;
$scheda->quantita	= new LongNumber((string) $milligrammi);

// colli
$scheda->numeroColli	= new LongNumber((string) $FEDIT->Scarico[0]['COLLI']);

// verifica a destino
if($FEDIT->Scarico[0]['VER_DESTINO']==1)
	$scheda->flagPesoADestino = new Flag(true);
else
	$scheda->flagPesoADestino = new Flag(false);

// operazione r/s
$scheda->operazioneImpianto   = new Catalogo($FEDIT->Scarico[0]['destinatoA']);

// tipoImballaggio
$scheda->tipoImballaggio	  = new Catalogo($FEDIT->Scarico[0]['ID_TIPO_IMBALLAGGIO']);

// prescrizioni particolari per trasporto
if(!is_null($FEDIT->Scarico[0]['prescrizioni_mov']) && trim($FEDIT->Scarico[0]['prescrizioni_mov'])!=''){
	$scheda->flagPrescrizioniParticolari	= new Flag(true);
	$scheda->descrizionePrescrizioni		= utf8_encode($FEDIT->Scarico[0]['prescrizioni_mov']);
	}
else{
	$scheda->flagPrescrizioniParticolari	= new Flag(false);
	}

// adr
if($FEDIT->Scarico[0]['adr']==1){
	$scheda->flagTrasportoADR	= new Flag(true);
	$scheda->classeADR			= new Catalogo($FEDIT->Scarico[0]['ID_CLASSE_ADR']);
	$scheda->numeroONU			= new Catalogo($FEDIT->Scarico[0]['ONU']);
	}
else
	$scheda->flagTrasportoADR = new Flag(false);

// analisi
if(!is_null($FEDIT->Scarico[0]['CAR_NumDocumento']) && trim($FEDIT->Scarico[0]['CAR_NumDocumento'])!=''){
	$scheda->numeroCertificato	= utf8_encode($FEDIT->Scarico[0]['CAR_NumDocumento']);
	$scheda->laboratorio		= utf8_encode($FEDIT->Scarico[0]['CAR_Laboratorio']);
	if($FEDIT->Scarico[0]['CAR_DataAnalisi']!=''){
		$splitted=explode("-",$FEDIT->Scarico[0]['CAR_DataAnalisi']);
		$giorno=$splitted[2];
		$mese=$splitted[1];
		$anno=$splitted[0];
		$DataCertificato = $anno."-".$mese."-".$giorno."T00:00:00";
		$scheda->dataCertificato	= $DataCertificato; //dateTime 2001-10-26T21:32:52
		}
	}

// trasportatore
$scheda->idSISSede_trasportatore	= $FEDIT->Scarico[0]['idSIS_T'];
$scheda->versioneSede_trasportatore = new LongNumber((string) $FEDIT->Scarico[0]['Versione_T']);

// destinatario
$scheda->idSISSede_destinatario		= $FEDIT->Scarico[0]['idSIS_D'];
$scheda->versioneSede_destinatario	= new LongNumber((string) $FEDIT->Scarico[0]['Versione_D']);
$scheda->autorizzazione				= utf8_encode($FEDIT->Scarico[0]['Autorizzazione_D']);

// altroTipoImballaggio
$scheda->altroTipoImballaggio		= utf8_encode($FEDIT->Scarico[0]['ALTRO_TIPO_IMBALLAGGIO']);



#    $scheda->posizioneRifiuto; // PosizioneRifiuto
if($FEDIT->Scarico[0]['FuoriUnitaLocale']=='1'){
	$indirizzo					= utf8_encode($FEDIT->Scarico[0]['indirizzoP']);
	$codiceCatastale			= $FEDIT->Scarico[0]['codice_catastale'];
	$RifiutoFuoriUnitaLocale	= new RifiutoFuoriUnitaLocale($indirizzo, $codiceCatastale);
	$PosizioneRifiuto			= new PosizioneRifiuto(null, $RifiutoFuoriUnitaLocale);
	$scheda->posizioneRifiuto	= $PosizioneRifiuto;
	}




#
#	ALTRI PARAMETRI TRASMETTIBILI oggetto $scheda (schedaSISTRI_produttore)
#
#    $scheda->flagVeicoli_Dlgs_209_2003; // Flag
#    $scheda->flagVeicoli_Dlgs_Art231_152_2006; // Flag
#    $scheda->numeroVeicoli; // LongNumber
#    $scheda->listaAllegati; // DocumentData_Base
#    $scheda->codRec_1013; // Catalogo
#    $scheda->numeroNotifica; // string
#    $scheda->numeroSerieSpedizione; // string
#    $scheda->quantita_150101; // LongNumber
#    $scheda->quantita_150104; // LongNumber
#    $scheda->quantita_150102; // LongNumber
#    $scheda->quantita_150107; // LongNumber
#    $scheda->quantita_150103; // LongNumber
#    $scheda->quantitaTotale_150106; // LongNumber
#    $scheda->descrizioneAltroStatoFisico; // string
#    $scheda->posizioneRifiuto; // PosizioneRifiuto
#    $scheda->codRec_1013_Produttore; // Catalogo
#    $scheda->numeroNotifica_Produttore; // string
#    $scheda->numeroSerieSpedizione_Produttore; // string
#    $scheda->volume; // DoubleNumber
#    $scheda->flagCircuitoMicroraccolta; // Flag

#
#	ALTRI PARAMETRI TRASMETTIBILI oggetto $schedabase (schedaSISTRI_Base)
#
#    $schedabase->schedaSISTRI_trasportatore;	// SchedaSISTRI_Trasportatore
#    $schedabase->schedaSISTRI_destinatario;	// SchedaSISTRI_Destinatario
#    $schedabase->schedaSISTRI_prod_trasp_cp;	// SchedaSISTRI_Prod_Trasp_CP
#    $schedabase->idSISSedi_intermediari;		// string
#    $schedabase->versioniSedi_intermediari;	// LongNumber
#    $schedabase->idSISSede_consorzio;			// string
#    $schedabase->versioneSede_consorzio;		// LongNumber
#    $schedabase->idSISSede_unitaLocale;		// string
#    $schedabase->versioneSede_unitaLocale;		// LongNumber
#    $schedabase->dataOraConsclusioneProcesso;	// dateTime
#    $schedabase->causaleCreazione;				// Catalogo
#    $schedabase->causaleModifica;				// Catalogo



$contatto = array(
	"nome"      => utf8_encode($FEDIT->Scarico[0]['USER_nome']),
	"cognome"   => utf8_encode($FEDIT->Scarico[0]['USER_cognome']),
	"telefono"  => utf8_encode($FEDIT->Scarico[0]['USER_telefono']),
	"email"     => utf8_encode($FEDIT->Scarico[0]['USER_email'])
	);


$schedabase = new SchedaSISTRI_Base;
$schedabase->tipoRegCronologico          = $tipo_registro;
$schedabase->annotazioni		         = utf8_encode($NOTEformulario);
$schedabase->schedaSISTRI_produttore     = $scheda;
$schedabase->nomePersonaDaContattare     = $contatto["nome"];
$schedabase->cognomePersonaDaContattare  = $contatto["cognome"];
$schedabase->telefonoPersonaDaContattare = $contatto["telefono"];
$schedabase->emailPersonaDaContattare    = $contatto["email"];

// intermediari
$idSISSedi_intermediari					= $FEDIT->Scarico[0]['idSIS_I'];
$schedabase->idSISSedi_intermediari		= array($idSISSedi_intermediari);
$versioniSedi_intermediari				= new LongNumber((string) $FEDIT->Scarico[0]['Versione_I']);
$schedabase->versioniSedi_intermediari	= array($versioniSedi_intermediari);

?>