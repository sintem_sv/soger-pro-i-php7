<div style="width:280px;background-color:#FFFFFF;margin-top:20px;">

	<div class="divName" style="text-align:center;height:25px;width:100%;background-color: #4D6980;color: #ffffff;">
		<span style="font-weight: bold;padding:5px;font-size: 13pt;font-family: verdana;text-transform: uppercase;">DATI ACCESSO SIS</span>
	</div>	

	<div style="padding:10px;">

		<table style="table-layout:fixed;width: 100%;">
	
			<?php 
			if(!is_null($SOGER->UserData['core_usersSIS_identity']) AND $SOGER->UserData['core_usersSIS_identity']!=''){
				$Identity = $SOGER->UserData['core_usersSIS_identity']; 
				$gotIdentity = 1;
				}
			else{ 
				$Identity = "non specificato"; 
				$gotIdentity = 0;
				}
			
			if(!is_null($SOGER->UserData['core_impianticodfisc']) AND $SOGER->UserData['core_impianticodfisc']!=''){
				$CF = $SOGER->UserData['core_impianticodfisc']; 
				$gotCF = 1;
				}
			else{ 
				$CF = "non specificato"; 
				$gotCF = 0;
				}
			?>

			<tr>
				<td style="font-size:8pt;"><b>Identity:</b></td>
				<td style="font-size:8pt;text-align:right"><?php echo $Identity; ?></td>
			</tr>

			<?php
			// certificato interoperabilitą
			if(file_exists("__SIS/certificates/".$SOGER->UserData['core_impianticodfisc']."/certificato.cer")){
				$gotCertificato		= 1;
				$certificato		= "Certificato caricato su server";
				$icona_certificato	= "certificato.gif";
				}
			else{
				$gotCertificato		= 0;
				$certificato		= "Certificato interoperabilitą non inviato";
				$icona_certificato	= "certificato_off.gif";
				}
			?>

			<tr>
				<td style="font-size:8pt;"><b>Certificato SIS: </b></td>
				<td style="font-size:8pt;text-align:right"><img src="__css/<?php echo $icona_certificato; ?>" alt="<?php echo $certificato; ?>" title="<?php echo $certificato; ?>" /></td>
			</tr>

			<?php
			// crt_key.pem ( certificato + private )
			if(file_exists("__SIS/certificates/".$SOGER->UserData['core_impianticodfisc']."/crt_key.pem")){
				$gotPk		= 1;
				$pk			= "Private key caricata su server";
				$icona_pk	= "private_key.gif";
				}
			else{
				$gotPk		= 0;
				$pk			= "Private key non caricata su server";
				$icona_pk	= "private_key_off.gif";
				}
			?>

			<tr>
				<td style="font-size:8pt;"><b>Private key: </b></td>
				<td id="PrivateKeyIcon" style="font-size:8pt;text-align:right"><img src="__css/<?php echo $icona_pk; ?>" alt="<?php echo $pk; ?>" title="<?php echo $pk; ?>" /></td>
			</tr>

		</table>

	</div>

	<table style="width:100%;" cellpadding="0" cellspacing="5">

		<tr>

			<td style="width:50%;background-color:#5C7D99;color:#fff;">
				<div alt="Carica private key sul server" title="Carica private key sul server" id="UpPk" style="padding-top:10px;padding-bottom:10px;width:100%;cursor:pointer;text-align:center;">
					Carica private key<br />sul server
				</div>
				<input type="hidden" name="gotCertificato" id="gotCertificato" value="<?php echo $gotCertificato; ?>" />
				<input type="hidden" name="gotPk" id="gotPk" value="<?php echo $gotPk; ?>" />
				<input type="hidden" name="gotIdentity" id="gotIdentity" value="<?php echo $gotIdentity; ?>" />
				<input type="hidden" name="gotCF" id="gotCF" value="<?php echo $gotCF; ?>" />
			</td>

			<td style="width:50%;background-color:#5C7D99;color:#fff;">
				<div alt="Verifica interoperabilitą" title="Verifica interoperabilitą" id="TestCert" style="padding-top:10px;padding-bottom:10px;width:100%;cursor:pointer;text-align:center;">
					Verifica<br />interoperabilitą
				</div>
			</td>			

		</tr>

	</table>


</div>





<!-- HIDDEN DIALOG -->

<div id='jqpopup_TestCert' title='Verifica connettivitą' style="display:none;">

	<table style="width:100%;border: 1px solid #4D6980;">

		<tr>
			<th colspan="4" style="padding:10px;background-color:#4D6980;color:#fff;">Verifica dei parametri di configurazione</th>
		</tr>

		<tr>
			<td style="width:25%">Codice fiscale</td>
			<td style="width:25%;text-align:center" id="ESITO_gotCF"></td>
			<td style="width:25%">Identity</td>
			<td style="width:25%;text-align:center" id="ESITO_gotIdentity"></td>
		</tr>

		<tr>
			<td style="width:25%">Certificato</td>
			<td style="width:25%;text-align:center" id="ESITO_gotCertificato"></td>
			<td style="width:25%">Private key</td>
			<td style="width:25%;text-align:center" id="ESITO_gotPk"></td>
		</tr>

	</table>

	<table style="width:100%;margin-top:30px;border: 1px solid #4D6980;" id="ConnectionStatus">
		<tr>
			<th colspan="4" style="padding:10px;background-color:#4D6980;color:#fff;">Connessione a SISTRI</th>
		</tr>
	</table>

</div>

<div id='jqpopup_UploadPrivateKey' title='Upload private key' style="display:none;">

	<p>Clicca su "Seleziona private key" per caricare la chiave privata rilasciata da SISTRI:</p>

	<form enctype="multipart/form-data" method="post">
		<div id="queue"></div>
		<input id="FILE_PrivateKey" name="FILE_PrivateKey" type="file" multiple="true">
		<style type="text/css">
			.uploadify-button {
				background-color: transparent;
				border: none;
				padding: 0;
				}
			.uploadify:hover .uploadify-button {
				background-color: transparent;
				}
		</style> 
		<script type="text/javascript">
		<?php $timestamp = time(); ?>
		$(function() {
			$('#FILE_PrivateKey').uploadify({
				'onUploadSuccess' : function(file, data, response) {
					alert(data);
					if (data.indexOf("Attenzione!") == -1)
						$('#PrivateKeyIcon').html('<img src="__css/private_key.gif" alt="Private key caricata su server" title="Private key caricata su server" />');
						},
				'removeCompleted' : true,
				'formData'      : {
					'DestinationFolder' : '../__SIS/certificates/<?php echo $SOGER->UserData['core_impianticodfisc']; ?>/',
					'timestamp' : '<?php echo $timestamp;?>',
					'token'     : '<?php echo md5('unique_salt' . $timestamp);?>'
					},
				'fileTypeExts' : '*.key',
				'swf'      : '__css/images/uploadify.swf',
				'uploader' : '__scripts/uploadify.php',
				'buttonImage' : '__css/images/seleziona_private_key.gif',
				'width'    : 180,
				'height'   : 24
			});

		});
		</script>

	</form>
		
</div>