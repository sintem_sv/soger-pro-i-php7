<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
require_once("SIS_SOAP_SSL.php");
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");

global $SOGER;


$SQL ="SELECT adr, ID_AUTHT FROM user_movimenti_fiscalizzati WHERE ID_MOV_F=".$_POST['ID_MOV_F'].";";
$FEDIT->SdbRead($SQL,"IDsT");

if($_POST['ID_AUTO']!='null'){
	if($_POST['ID_AUTO']==0){
		$SQL ="INSERT INTO `user_automezzi` (`ID_AUTO` , `description` , `PREV_description` , `adr` , `BlackBox` , `ID_AUTHT` , `ID_ORIGINE_DATI` , `ID_LIM` , `ID_MZ_TRA` , `inherit_edits` , `COD_IMPORT` ) VALUES ( NULL , '".$_POST['automezzo']."', '".$_POST['automezzo']."', '".$FEDIT->IDsT[0]['adr']."', '0', '".$FEDIT->IDsT[0]['ID_AUTHT']."', '1', '1', '2', '0', NULL);";
		$FEDIT->SDBWrite($SQL,true,false);
		$ID_AUTO = $FEDIT->DbLastID;
		}
	else $ID_AUTO = $_POST['ID_AUTO'];
	if($FEDIT->IDsT[0]['adr']=='1'){
		$ADR = "si";
		$SQL = "UPDATE user_automezzi SET adr=1 WHERE ID_AUTO=".$ID_AUTO.";";
		$FEDIT->SDBWrite($SQL,true,false);
		}
	else $ADR = "no";

	$SQL ="UPDATE user_movimenti_fiscalizzati SET ID_AUTO=".$ID_AUTO.", FKEadrAUT='".$ADR."', FKEmzAUT='AUTOCARRO' WHERE ID_MOV_F=".$_POST['ID_MOV_F'].";";
	$FEDIT->SDBWrite($SQL,true,false);
	}

if($_POST['ID_RMK']!='null'){
	if($_POST['ID_RMK']==0){
		$SQL ="INSERT INTO `user_rimorchi` (`ID_RMK` , `description` , `PREV_description` , `adr` , `ID_AUTHT` , `ID_ORIGINE_DATI` , `ID_LIM` , `ID_MZ_RMK` , `inherit_edits` , `COD_IMPORT` ) VALUES ( NULL , '".$_POST['rimorchio']."', '".$_POST['rimorchio']."', '".$FEDIT->IDsT[0]['adr']."', '".$FEDIT->IDsT[0]['ID_AUTHT']."', '1', '1', '2', '0', NULL);";
		$FEDIT->SDBWrite($SQL,true,false);
		$ID_RMK = $FEDIT->DbLastID;
		}
	else $ID_RMK = $_POST['ID_RMK'];
	if($FEDIT->IDsT[0]['adr']=='1'){
		$SQL = "UPDATE user_rimorchi SET adr=1 WHERE ID_RMK=".$ID_RMK.";";
		$FEDIT->SDBWrite($SQL,true,false);
		$ADR = "si";
		}
	else $ADR = "no";

	$SQL ="UPDATE user_movimenti_fiscalizzati SET ID_RMK=".$ID_RMK.", FKEadrRMK='".$ADR."', FKEmzRMK='CASSONE' WHERE ID_MOV_F=".$_POST['ID_MOV_F'].";";
	$FEDIT->SDBWrite($SQL,true,false);
	}


echo "Operazione ultimata!";

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>