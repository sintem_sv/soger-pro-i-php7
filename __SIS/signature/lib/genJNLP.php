<?php

$jnlpFileName = 'sign' . $_GET['id'] . '.jnlp';
$thisFileName = basename($_SERVER['PHP_SELF']) . '?' . $_SERVER['QUERY_STRING'];
header('Content-Type: application/x-java-jnlp-file');
header('Content-Disposition: inline; filename="' . $jnlpFileName . '"');

$s          = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
$protocol   = substr(strtolower($_SERVER["SERVER_PROTOCOL"]), 0, strpos(strtolower($_SERVER["SERVER_PROTOCOL"]), "/")) . $s;
$loadFrom   = $protocol . "://" . $_SERVER['SERVER_NAME'] . "/soger/";
if(substr($loadFrom, strlen($loadFrom)-6)!="soger/") $loadFrom.="soger/";

?>

<?xml version="1.0" encoding="UTF-8" ?>
<jnlp href="<?php echo $thisFileName; ?>" spec="1.0+" codebase="<?php echo $loadFrom; ?>__SIS/signature/lib/" >

    <information>
        <title>SOGER PRO - Firma SISTRI</title>
        <vendor>Sintem Srl</vendor>
    </information>

    <resources>
        <java version="1.6+"/>
        <jar href="Signature.jar" main="true"/>
        <jar href="json_simple-1.1.1.jar"/>
        <jar href="commons-codec-1.9.jar"/>
    </resources>

    <application-desc main-class="Signature">
        <argument><?php echo $loadFrom; ?>__SIS/<?php echo $_GET['prepara']; ?>.php?SID=<?php echo $_GET['SID']; ?>&id=<?php echo $_GET['id']; ?>&ID_MOV_F=<?php echo $_GET['ID_MOV_F']; ?>&IsUpdate=<?php echo $_GET['IsUpdate']; if(isset($_GET['Annullamento'])){ echo "&codiceCausale=".$_GET['codiceCausale']."&annotazioniCausale=".urlencode($_GET['annotazioniCausale']); } if(isset($_GET['TIPO'])){ echo "&TIPO=".$_GET['TIPO']; } if(isset($_GET['QuantitaOriginale'])){ echo "&QuantitaOriginale=".urlencode($_GET['QuantitaOriginale']); } ?></argument>
    </application-desc>

    <security>
        <all-permissions/>
    </security>

</jnlp>