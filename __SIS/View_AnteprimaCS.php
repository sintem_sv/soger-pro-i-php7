<?php
session_start();
require_once("../__scripts/ForgEdit_includes.inc");
require_once("../__scripts/Soger_includes.inc");
require_once("../__scripts/STATS_funct.php");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("../__libs/SQLFunct.php");
require_once("../__libs/fpdf.php");

global $SOGER;
global $FEDIT;

$NMOV		= $_GET['NMOV'];
$ID_MOV_F	= $_GET['ID_MOV_F'];
$RegType	= $_GET['RegType'];

$orientation='L';
$um="mm";
$Format = array(210,297);
$ZeroMargin = true;
$DcName='Anteprima movimenti di carico e scarico da inviare a SISTRI';
$FEDIT->FGE_PdfOutput($orientation,$um,$Format,$ZeroMargin,$DcName);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',7);


$Xpos = 2;
$Ypos = 0;

/*
$Data=explode("/",$_GET['da']);
$Data1=$Data[2]."-".$Data[1]."-".$Data[0];

$Data=explode("/",$_GET['a']);
$Data2=$Data[2]."-".$Data[1]."-".$Data[0];
*/

#
# TITOLO
#
if($RegType=='crono')
	$statTitle = "Visualizzazione in anteprima dei dati del registro cronologico per invio al SISTRI";
if($RegType=='scheda')
	$statTitle = "Visualizzazione in anteprima delle Schede per invio al SISTRI";

$Ypos+=10;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','B',14);
$FEDIT->FGE_PdfBuffer->MultiCell(293,15,$statTitle,"TLBR","C"); 

#
# AZIENDA
#
$Ypos+=12;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$sql="SELECT core_impianti.description, indirizzo, lov_comuni_istat.description as comune, lov_comuni_istat.CAP, lov_comuni_istat.shdes_prov as prov ";
$sql.="FROM core_impianti JOIN lov_comuni_istat ON core_impianti.ID_COM = lov_comuni_istat.ID_COM ";
$sql.="WHERE core_impianti.ID_IMP='".$SOGER->UserData['core_impiantiID_IMP']."';";
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
$InfoAZ=$FEDIT->DbRecordSet[0]['description']." - ".$FEDIT->DbRecordSet[0]['indirizzo']." - ".$FEDIT->DbRecordSet[0]['CAP']." - ".$FEDIT->DbRecordSet[0]['comune']." (".$FEDIT->DbRecordSet[0]['prov'].")";
$FEDIT->FGE_PdfBuffer->MultiCell(200,15,$InfoAZ,0,"L"); 

#
# PERIODO
#
/*
$Ypos+=6;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(200,15,"Settimana dal ".$_GET['da']." al ".$_GET['a'],0,"L"); 
*/

#
# ULTIMA FISCALIZZAZIONE
#
if($SOGER->UserData["workmode"]=="produttore") $Field="Plast_fisc"; else $Field="Dlast_fisc";
$sql="SELECT ".$Field." FROM core_impianti WHERE ID_IMP='".$SOGER->UserData["core_usersID_IMP"]."';";
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
$dataExp=explode("-",$FEDIT->DbRecordSet[0][$Field]);
$data=$dataExp[2]."/".$dataExp[1]."/".$dataExp[0];
$Ypos+=6;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(200,15,"Data ultima fiscalizzazione: ".$data,0,"L"); 



#
# SELEZIONO MOVIMENTI
#
$sql  = " SELECT ";
$sql .= " TIPO, DTMOV, NMOV, FKESF, quantita, FKErifCarico, FKEumis, user_movimenti_fiscalizzati.adr, ";
$sql .= " user_schede_rifiuti.descrizione, lov_cer.COD_CER, ";
$sql .= " user_schede_rifiuti.H1, user_schede_rifiuti.H2, user_schede_rifiuti.H3A, user_schede_rifiuti.H3B, user_schede_rifiuti.H4, ";
$sql .= " user_schede_rifiuti.H5, user_schede_rifiuti.H6, user_schede_rifiuti.H7, user_schede_rifiuti.H8, user_schede_rifiuti.H9, ";
$sql .= " user_schede_rifiuti.H10, user_schede_rifiuti.H11, user_schede_rifiuti.H12, user_schede_rifiuti.H13, user_schede_rifiuti.H14, user_schede_rifiuti.H15,  ";
$sql .= " user_aziende_trasportatori.description AS trasportatore, user_autorizzazioni_trasp.num_aut AS numAlbo, ";
$sql .= " user_aziende_destinatari.description AS destinatario, user_autorizzazioni_dest.num_aut AS numAut, ";
$sql .= " user_aziende_intermediari.description AS intermediario, user_autorizzazioni_interm.num_aut AS numAutInt ";
$sql .= " FROM user_movimenti_fiscalizzati"; 
$sql .= " JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF";
$sql .= " JOIN lov_cer ON lov_cer.ID_CER=user_schede_rifiuti.ID_CER";
$sql .= " LEFT JOIN user_aziende_trasportatori ON user_movimenti_fiscalizzati.ID_AZT=user_aziende_trasportatori.ID_AZT ";
$sql .= " LEFT JOIN user_aziende_destinatari ON user_movimenti_fiscalizzati.ID_AZD=user_aziende_destinatari.ID_AZD ";
$sql .= " LEFT JOIN user_aziende_intermediari ON user_movimenti_fiscalizzati.ID_AZI=user_aziende_intermediari.ID_AZI ";
$sql .= " LEFT JOIN user_autorizzazioni_trasp ON user_movimenti_fiscalizzati.ID_AUTHT=user_autorizzazioni_trasp.ID_AUTHT ";
$sql .= " LEFT JOIN user_autorizzazioni_dest ON user_movimenti_fiscalizzati.ID_AUTHD=user_autorizzazioni_dest.ID_AUTHD ";
$sql .= " LEFT JOIN user_autorizzazioni_interm ON user_movimenti_fiscalizzati.ID_AUTHI=user_autorizzazioni_interm.ID_AUTHI ";
if($RegType=='crono')
	$sql .= " WHERE SIS_OK=0 ";
if($RegType=='scheda')
	$sql .= " WHERE SIS_OK_SCHEDA=0 AND TIPO='S' ";

if($NMOV==9999999)
	$sql.=" AND (NMOV<".$NMOV." OR (NMOV=".$NMOV." AND ID_MOV_F<=".$ID_MOV_F.")) ";
else
	$sql.=" AND NMOV<=".$NMOV." ";

$TableSpec = "user_movimenti_fiscalizzati.";
include("../__includes/SOGER_DirectProfilo.php");
$sql .= " AND user_movimenti_fiscalizzati.ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "'";
// MANDIAMO A SISTRI SOLO MOVIMENTI PERICOLOSI
$sql .= " AND user_schede_rifiuti.pericoloso=1 ";
$sql .= " ORDER BY NMOV ASC";
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

//die($sql);
//die(var_dump($FEDIT->DbRecordSet));

#
# DIVIDO CARICHI DA SCARICHI
#
$IndC=array();
$IndS=array();
for($i=0;$i<count($FEDIT->DbRecordSet);$i++){
	if($FEDIT->DbRecordSet[$i]['TIPO']=="C")
		$IndC[]=$FEDIT->DbRecordSet[$i];
	else
		$IndS[]=$FEDIT->DbRecordSet[$i];
	}



#
# INTESTAZIONE CARICHI
#
$Ypos+=15;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',12);
$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
$FEDIT->FGE_PdfBuffer->MultiCell(293,7,"Movimenti di Carico","TLBR","C",1);
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',11);
$FEDIT->FGE_PdfBuffer->MultiCell(293,7,"Registrazioni fiscali","TLBR","C",1);
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
#
# CICLO CARICHI
#
$Xpos=2;


if(count($IndC)>0){
		

	$DTbuf="0000-00-00";
	for($c=0;$c<count($IndC);$c++){
	
		if($IndC[$c]['DTMOV']!=$DTbuf){
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			## data ##
			$Xpos=2;
			$dataExp=explode("-",$IndC[$c]['DTMOV']);
			$data=$dataExp[2]."/".$dataExp[1]."/".$dataExp[0];
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',11);
			$FEDIT->FGE_PdfBuffer->SetFillColor(138, 175, 250);
			$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);
			$FEDIT->FGE_PdfBuffer->MultiCell(293,7,"Data: ".$data,"TLBR","L",1);
			$DTbuf=$IndC[$c]['DTMOV'];
			$Ypos+=7;
			## intestazione colonne ##
			$Xpos=2;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(13,7,"Rigo N�","TLBR","C",1);
			$Xpos+=13;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(140,7,"Scheda rifiuto","TLBR","L",1);
			$Xpos+=140;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(40,7,"Stato fisico","TLBR","L",1);
			$Xpos+=40;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(40,7,"Quantit�","TLBR","C",1);
			$Xpos+=40;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(10,7,"ADR","TLBR","C",1);
			$Xpos+=10;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(50,7,"Pericolo (H)","TLBR","L",1);
			$Ypos+=7;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			}

		
		$Xpos=2;
		$YposFiscC[]=$Ypos;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);

		
		## riga ##
		$Ypos+=5;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$Ypos-=5;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);
		$FEDIT->FGE_PdfBuffer->MultiCell(13,10,$IndC[$c]['NMOV'],"TLBR","C");
		$Xpos+=13;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(140,10,"","TLBR","L");
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(140,10,$IndC[$c]['COD_CER']." - ".$IndC[$c]['descrizione'],"TLBR","L");
		$Xpos+=140;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,10,$IndC[$c]['FKESF'],"TLBR","L");
		$Xpos+=40;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,10,number_format_unlimited_precision($IndC[$c]['quantita'])." ".$IndC[$c]['FKEumis'],"TLBR","C");
		$Xpos+=40;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		if($IndC[$c]['adr']=="1") $adr="S�"; else $adr="No";
		$FEDIT->FGE_PdfBuffer->MultiCell(10,10,$adr,"TLBR","C");
		$Xpos+=10;
		if($IndC[$c]['H1']==1) $classiH[]="H1";
		if($IndC[$c]['H2']==1) $classiH[]="H2";
		if($IndC[$c]['H3A']==1) $classiH[]="H3A";
		if($IndC[$c]['H3B']==1) $classiH[]="H3B";
		if($IndC[$c]['H4']==1) $classiH[]="H4";
		if($IndC[$c]['H5']==1) $classiH[]="H5";
		if($IndC[$c]['H6']==1) $classiH[]="H6";
		if($IndC[$c]['H7']==1) $classiH[]="H7";
		if($IndC[$c]['H8']==1) $classiH[]="H8";
		if($IndC[$c]['H9']==1) $classiH[]="H9";
		if($IndC[$c]['H10']==1) $classiH[]="H10";
		if($IndC[$c]['H11']==1) $classiH[]="H11";
		if($IndC[$c]['H12']==1) $classiH[]="H12";
		if($IndC[$c]['H13']==1) $classiH[]="H13";
		if($IndC[$c]['H14']==1) $classiH[]="H14";
		if($IndC[$c]['H15']==1) $classiH[]="H15";
		$displayH="";
		for($h=0;$h<count($classiH);$h++)
			$displayH.=$classiH[$h]." ";
		unset($classiH);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(50,10,$displayH,"TLBR","L");
		$displayH="";
		$Xpos+=10; 

		$Ypos+=10;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$YstartS=$Ypos;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		}
	}
else{
	$Ypos+=7;
	$Xpos=2;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',12);
	$FEDIT->FGE_PdfBuffer->MultiCell(293,7,"Non vi sono carichi fiscalizzati nell'intervallo scelto","TLBR","C");
	$Ypos+=20;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$YstartS=$Ypos;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	}






$Xpos=2;
$Ypos=$YstartS;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
#
# INTESTAZIONE SCARICHI
#
$Ypos+=15;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',12);
$FEDIT->FGE_PdfBuffer->SetFillColor(0,32,96);
$FEDIT->FGE_PdfBuffer->SetTextColor(255,255,255);
$FEDIT->FGE_PdfBuffer->MultiCell(293,7,"Movimenti di Scarico","TLBR","C",1);
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',11);
$FEDIT->FGE_PdfBuffer->MultiCell(293,7,"Registrazioni fiscali","TLBR","C",1);
$Ypos+=7;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);


#
# CICLO SCARICHI
#
$Xpos=2;

if(count($IndS)>0){
		
	$DTbuf="xxxx-xx-xx";
	for($c=0;$c<count($IndS);$c++){
	
		if($IndS[$c]['DTMOV']!=$DTbuf){
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			## data ##
			$Xpos=2;
			$dataExp=explode("-",$IndS[$c]['DTMOV']);
			$data=$dataExp[2]."/".$dataExp[1]."/".$dataExp[0];
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',11);
			if($data=='00/00/0000') $data = "n.d.";
			$FEDIT->FGE_PdfBuffer->MultiCell(293,7,"Data: ".$data,"TLBR","L",1);
			$DTbuf=$IndS[$c]['DTMOV'];
			
			## intestazione colonne ##
			$Xpos=2;
			$Ypos+=7;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->SetFillColor(138, 175, 250);
			$FEDIT->FGE_PdfBuffer->SetTextColor(0,0,0);
			$FEDIT->FGE_PdfBuffer->MultiCell(13,14,"Rigo N�","TLBR","C",1);
			$Xpos+=13;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(140,14,"Scheda rifiuto","TLBR","L",1);
			$Xpos+=140;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(40,14,"Stato fisico","TLBR","L",1);
			$Xpos+=40;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(40,7,"Quantit�","TLBR","C",1);

			$Ypos+=7;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(40,7,"Rif. Carico","TLBR","C",1);
			$Ypos-=7;

			$Xpos+=40;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(10,14,"ADR","TLBR","C",1);

			$Xpos+=10;
			$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
			$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
			$FEDIT->FGE_PdfBuffer->MultiCell(50,14,"Pericolo (H)","TLBR","L",1);
			
			$Ypos+=14;
			CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
			}




		$Xpos=2;
		$YposFiscS[]=$Ypos;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		## riga ##
		if($Ypos>150){
			$FEDIT->FGE_PdfBuffer->addpage();
			$Ypos = 10;
			}
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		if($IndS[$c]['NMOV']==9999999)
			$NMOV = 'n.d.';
		else
			$NMOV = $IndS[$c]['NMOV'];
		$FEDIT->FGE_PdfBuffer->MultiCell(13,40,$NMOV,"TLBR","C");
		$Xpos+=13;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(140,40,"","TLBR","L");
		$Ypos+=2;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(140,3,$IndS[$c]['COD_CER']." - ".$IndS[$c]['descrizione'],"","L");
		## soggetti
		if($IndS[$c]['trasportatore']!='')
			$Soggetti ="\nTrasportatore: ".$IndS[$c]['trasportatore']." (".$IndS[$c]['numAlbo'].")\n";		
		if($IndS[$c]['destinatario']!='')
			$Soggetti.="Destinatario: ".$IndS[$c]['destinatario']." (".$IndS[$c]['numAut'].")\n";
		if($IndS[$c]['intermediario']!='')
			$Soggetti.="Intermediario: ".$IndS[$c]['intermediario']." (".$IndS[$c]['numAutInt'].")\n";
		$Ypos+=7;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(140,4,$Soggetti,"T","L");
		$Ypos-=9;
		##
		$Xpos+=140;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,40,"","TLBR","L");
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,5,$IndS[$c]['FKESF'],"","L");
		$Xpos+=40;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,20,number_format_unlimited_precision($IndS[$c]['quantita'])." ".$IndS[$c]['FKEumis'],"TLBR","C");

		$Ypos+=20;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,20,"","TLBR","C");
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(40,5,str_replace(",",", ",$IndS[$c]['FKErifCarico']),"","C");
		$Ypos-=20;

		$Xpos+=40;
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		if($IndS[$c]['adr']=="1") $adr="S�"; else $adr="No";
		$FEDIT->FGE_PdfBuffer->MultiCell(10,40,$adr,"TLBR","C");
		$Xpos+=10;
		if($IndS[$c]['H1']==1) $classiH[]="H1";
		if($IndS[$c]['H2']==1) $classiH[]="H2";
		if($IndS[$c]['H3A']==1) $classiH[]="H3A";
		if($IndS[$c]['H3B']==1) $classiH[]="H3B";
		if($IndS[$c]['H4']==1) $classiH[]="H4";
		if($IndS[$c]['H5']==1) $classiH[]="H5";
		if($IndS[$c]['H6']==1) $classiH[]="H6";
		if($IndS[$c]['H7']==1) $classiH[]="H7";
		if($IndS[$c]['H8']==1) $classiH[]="H8";
		if($IndS[$c]['H9']==1) $classiH[]="H9";
		if($IndS[$c]['H10']==1) $classiH[]="H10";
		if($IndS[$c]['H11']==1) $classiH[]="H11";
		if($IndS[$c]['H12']==1) $classiH[]="H12";
		if($IndS[$c]['H13']==1) $classiH[]="H13";
		if($IndS[$c]['H14']==1) $classiH[]="H14";
		if($IndS[$c]['H15']==1) $classiH[]="H15";
		$displayH="";
		for($h=0;$h<count($classiH);$h++)
			$displayH.=$classiH[$h]." ";
		unset($classiH);
		$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
		$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',9);
		$FEDIT->FGE_PdfBuffer->MultiCell(50,40,$displayH,"TLBR","L");
		$displayH="";
		$Xpos+=28; 
				
		$Ypos+=40;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		$YstartS=$Ypos;
		CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
		}
	}
else{
	$Ypos+=7;
	$Xpos=2;
	CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
	$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
	$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',12);
	$FEDIT->FGE_PdfBuffer->MultiCell(293,7,"Non vi sono scarichi fiscalizzati nell'intervallo scelto","TLBR","C");
	}

#
# DATA E FIRMA
#

$Ypos+=10;
$Xpos=2;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',10);
$FEDIT->FGE_PdfBuffer->MultiCell(293,15,"NOTA: i dati in visione, una volta confermati, non saranno pi� assoggettabili a modifiche.","","L"); 


$Ypos+=25;
$Xpos=200;
CheckYPos($Ypos,$FEDIT->FGE_PdfBuffer);
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',14);
$FEDIT->FGE_PdfBuffer->MultiCell(50,15,"FIRMA","","C"); 

$Xpos=25;
$FEDIT->FGE_PdfBuffer->SetXY($Xpos,$Ypos);
$FEDIT->FGE_PdfBuffer->SetFont('Helvetica','',14);
$FEDIT->FGE_PdfBuffer->MultiCell(50,15,date('d/m/Y'),"","C"); 



#
# OUTPUT
#

$FEDIT->FGE_PdfBuffer->Output($DcName.".pdf","D");
unset($FEDIT->FGE_PdfBuffer);

?>