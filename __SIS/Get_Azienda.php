<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("SIS_SOAP_SSL.php");

$TableName	= $_POST['tab_impianto'];
$Filter		= $_POST['id_impianto'];

switch($TableName){
	case "user_impianti_produttori":
		$Primary	= "ID_UIMP";
		$Parent		= "user_aziende_produttori";
		$PrimaryPar = "ID_AZP";
		break;
	case "user_impianti_trasportatori":
		$Primary	= "ID_UIMT";
		$Parent		= "user_aziende_trasportatori";
		$PrimaryPar = "ID_AZT";
		break;
	case "user_impianti_destinatari":
		$Primary	= "ID_UIMD";
		$Parent		= "user_aziende_destinatari";
		$PrimaryPar = "ID_AZD";
		break;
	case "user_impianti_intermediari":
		$Primary	= "ID_UIMI";
		$Parent		= "user_aziende_intermediari";
		$PrimaryPar = "ID_AZI";
		break;
		}


$CERT		= "certificates/".$SOGER->UserData['core_impianticodfisc']."/crt_key.pem";
$SIS		= new SIS_SOAP($SOGER->UserData['core_usersSIS_identity'], $CERT);
$CreateLog	= false;

$Response	= Array();


## Select record impianto from TableName
$SQL ="SELECT ".$Parent.".codfisc as codfisc ";
$SQL.="FROM ".$TableName." ";
$SQL.="JOIN ".$Parent." ON ".$TableName.".".$PrimaryPar."=".$Parent.".".$PrimaryPar." ";
$SQL.="WHERE ".$Primary."=".$Filter.";";
$FEDIT->SdbRead($SQL,"Impianto");

/*

$ImpiantoSoger['idSoger']	= $Filter;
$ImpiantoSoger['idSIS']		= $FEDIT->Impianto[0]['idSIS'];
$ImpiantoSoger['indirizzo']	= $FEDIT->Impianto[0]['indirizzo'];
$ImpiantoSoger['comune']	= $FEDIT->Impianto[0]['comune'];
$ImpiantoSoger['provincia']	= $FEDIT->Impianto[0]['provincia'];
array_push($Response, $ImpiantoSoger);

*/

try{
	$AZIENDA	= $SIS->GetAzienda(Array("codiceFiscaleAzienda"=>$FEDIT->Impianto[0]['codfisc']));
	foreach($AZIENDA->sediSummary as $SEDE){
		$UnitaLocale['idSoger']		= '';
		$UnitaLocale['idSIS']		= $SEDE->idSIS;
		$UnitaLocale['versione']	= $SEDE->versione->long;
		$UnitaLocale['indirizzo']	= $SEDE->indirizzo." ".$SEDE->nrCivico;
		$SQL="SELECT description, shdes_prov FROM lov_comuni_istat WHERE ISTAT_comune='".$SEDE->codiceIstatLocalita."';";
		$FEDIT->SdbRead($SQL,"Comune");
		if(isset($FEDIT->Comune)){
			$UnitaLocale['comune']		= $FEDIT->Comune[0]['description'];
			$UnitaLocale['provincia']	= $FEDIT->Comune[0]['shdes_prov'];
			}
		else{
			$UnitaLocale['comune']		= 'Comune non riconosciuto, contattare l\'assitenza';
			$UnitaLocale['provincia']	= '';
			}
		$UnitaLocale['tipoSede']	= $SEDE->tipoSede->idCatalogo;
		$Sottocategorie = array();
		if(isset($SEDE->sottocategorie)){
			foreach($SEDE->sottocategorie AS $attivita){
				$Sottocategorie[]	= $attivita->description;
				}
			}
		$UnitaLocale['sottocategorie'] = $Sottocategorie;
		unset($Sottocategorie);
		array_push($Response, $UnitaLocale);
		unset($UnitaLocale);
		}
	}
catch (SoapFault $e) {
	$SISexcept = SIS_SOAP::getSISException($e);
	echo $SISexcept;
	}



echo json_encode($Response);



require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>