<?php
/*
error_reporting(E_ALL);
ini_set("display_errors", 1);
*/
session_start();
require_once("../__scripts/Soger_includes.inc");
require_once("../__includes/COMMON_wakeSoger.php");

$log		= $_SESSION[$SOGER->UserData['core_usersID_USR']]['SYNC_LOG'];
$tmpName	= tempnam(sys_get_temp_dir(), 'SISLOG_');
$file		= fopen($tmpName, 'w');

fwrite($file, $log);
fclose($file);

header('Content-Description: File Transfer');
header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename=Esito-sincronizzazione-anagrafiche.txt');
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($tmpName));

ob_clean();
flush();
readfile($tmpName);

unlink($tmpName);

require_once("../__includes/COMMON_sleepSoger.php");
?>