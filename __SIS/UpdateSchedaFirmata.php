<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
require_once("SIS_SOAP_SSL.php");
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");


$id_sis_registro	= $SOGER->UserData['core_usersidSIS_regCrono'];
$tipo_registro		= new Catalogo($SOGER->UserData['core_usersregCrono_type']);
$annotazioni		= "";

$scheda				= new SchedaSISTRI_Produttore;

// Select data from Soger
$SQL ="SELECT idSIS_scheda, numero_scheda, lov_cer.COD_CER, lov_cer.PERICOLOSO, user_movimenti_fiscalizzati.ID_RIF, user_schede_rifiuti.H1, user_schede_rifiuti.H2, user_schede_rifiuti.H3A, user_schede_rifiuti.H3B, user_schede_rifiuti.H4, user_schede_rifiuti.H5, user_schede_rifiuti.H6, user_schede_rifiuti.H7, user_schede_rifiuti.H8, user_schede_rifiuti.H9, user_schede_rifiuti.H10, user_schede_rifiuti.H11, user_schede_rifiuti.H12, user_schede_rifiuti.H13, user_schede_rifiuti.H14, user_schede_rifiuti.H15, user_schede_rifiuti.descrizione, pesoN, user_schede_rifiuti.ID_SF, lov_operazioni_rs.description as destinatoA, user_impianti_destinatari.idSIS as idSIS_D, user_impianti_destinatari.versione as Versione_D, user_autorizzazioni_dest.num_aut as Autorizzazione_D, user_impianti_trasportatori.idSIS as idSIS_T, user_impianti_trasportatori.versione as Versione_T, user_impianti_intermediari.idSIS as idSIS_I, user_impianti_intermediari.versione as Versione_I, USER_nome, USER_cognome, USER_email, USER_telefono, COLLI, user_movimenti_fiscalizzati.prescrizioni_mov, ID_TIPO_IMBALLAGGIO, ALTRO_TIPO_IMBALLAGGIO, lov_num_onu.description as ONU, lov_num_onu.classe AS ID_CLASSE_ADR, user_movimenti_fiscalizzati.adr, user_movimenti_fiscalizzati.CAR_NumDocumento, user_movimenti_fiscalizzati.CAR_DataAnalisi, user_movimenti_fiscalizzati.CAR_Laboratorio, VER_DESTINO ";
$SQL.="FROM user_movimenti_fiscalizzati ";
$SQL.="JOIN user_schede_rifiuti ON user_movimenti_fiscalizzati.ID_RIF=user_schede_rifiuti.ID_RIF ";
$SQL.="JOIN lov_cer ON user_schede_rifiuti.ID_CER=lov_cer.ID_CER ";
$SQL.="JOIN lov_operazioni_rs ON user_movimenti_fiscalizzati.ID_OP_RS=lov_operazioni_rs.ID_OP_RS ";
$SQL.="JOIN user_impianti_destinatari ON user_movimenti_fiscalizzati.ID_UIMD=user_impianti_destinatari.ID_UIMD ";
$SQL.="JOIN user_autorizzazioni_dest ON user_movimenti_fiscalizzati.ID_AUTHD=user_autorizzazioni_dest.ID_AUTHD ";
$SQL.="JOIN user_impianti_trasportatori ON user_movimenti_fiscalizzati.ID_UIMT=user_impianti_trasportatori.ID_UIMT ";
$SQL.="LEFT JOIN user_impianti_intermediari ON user_impianti_intermediari.ID_UIMI=user_movimenti_fiscalizzati.ID_UIMI ";
$SQL.="LEFT JOIN lov_num_onu ON user_movimenti_fiscalizzati.ID_ONU=lov_num_onu.ID_ONU ";
$SQL.="WHERE ID_MOV_F=2 AND user_movimenti_fiscalizzati.ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND user_movimenti_fiscalizzati.".$SOGER->UserData['workmode']."=1;";

//$SQL.="AND user_movimenti_fiscalizzati.idSIS_regCrono='".$SOGER->UserData['core_usersidSIS_regCrono']."' ";

$FEDIT->SdbRead($SQL,"Scarico");

## Verifico le relazioni scarico-carico
$TableName		= "user_movimenti_fiscalizzati";
$LegamiSISTRI	= true;
$RIF_REF		= $FEDIT->Scarico[0]['ID_RIF'];
require("../__scripts/MovimentiRifMovCarico.php");


// codice CER
$CER_array=str_split($FEDIT->Scarico[0]['COD_CER'], 2);
$CER=$CER_array[0].".".$CER_array[1].".".$CER_array[2];
$scheda->codiceCerIIILivello  = new Catalogo($CER);

// classi H
if($FEDIT->Scarico[0]['PERICOLOSO']=='1'){
	$reg->caratteristicaPericolo = array();
	if($FEDIT->Scarico[0]['H1']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H1");
	if($FEDIT->Scarico[0]['H2']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H2");
	if($FEDIT->Scarico[0]['H3A']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H3A");
	if($FEDIT->Scarico[0]['H3B']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H3B");
	if($FEDIT->Scarico[0]['H4']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H4");
	if($FEDIT->Scarico[0]['H5']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H5");
	if($FEDIT->Scarico[0]['H6']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H6");
	if($FEDIT->Scarico[0]['H7']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H7");
	if($FEDIT->Scarico[0]['H8']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H8");
	if($FEDIT->Scarico[0]['H9']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H9");
	if($FEDIT->Scarico[0]['H10']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H10");
	if($FEDIT->Scarico[0]['H11']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H11");
	if($FEDIT->Scarico[0]['H12']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H12");
	if($FEDIT->Scarico[0]['H13']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H13");
	if($FEDIT->Scarico[0]['H14']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H14");
	if($FEDIT->Scarico[0]['H15']=='1') $scheda->caratteristichePericolo[] = new Catalogo("H15");
	}

// descrizione
$scheda->descrizioneRifiuto   = utf8_encode($FEDIT->Scarico[0]['descrizione']);

// stato fisico
switch($FEDIT->Scarico[0]['ID_SF']){
	case 1: // Solido pulverulento -> In polvere o pulverulenti
		$ID_STATO_FISICO_RIFIUTO = '01';
		break;
	case 2: // Solido non pulverulento -> Solidi
		$ID_STATO_FISICO_RIFIUTO = '02';
		break;
	case 3: // Fangoso palabile -> Fangosi
		$ID_STATO_FISICO_RIFIUTO = '04';
		break;
	case 4: // Liquido ->  Liquidi
		$ID_STATO_FISICO_RIFIUTO = '05';
		break;
	}
$scheda->statoFisicoRifiuto   = new Catalogo($ID_STATO_FISICO_RIFIUTO);

// pesoN convertito in mg
$milligrammi		= $FEDIT->Scarico[0]['pesoN']*1000000;
$scheda->quantita	= new LongNumber((string) $milligrammi);

// colli
$scheda->numeroColli	= new LongNumber((string) $FEDIT->Scarico[0]['COLLI']);

// verifica a destino
if($FEDIT->Scarico[0]['VER_DESTINO']==1)
	$scheda->flagPesoADestino = new Flag(true);
else
	$scheda->flagPesoADestino = new Flag(false);

// operazione r/s
$scheda->operazioneImpianto   = new Catalogo($FEDIT->Scarico[0]['destinatoA']);

// tipoImballaggio
$scheda->tipoImballaggio	  = new Catalogo($FEDIT->Scarico[0]['ID_TIPO_IMBALLAGGIO']);

// prescrizioni particolari per trasporto
if(!is_null($FEDIT->Scarico[0]['prescrizioni_mov']) && trim($FEDIT->Scarico[0]['prescrizioni_mov'])!=''){
	$scheda->flagPrescrizioniParticolari	= new Flag(true);
	$scheda->descrizionePrescrizioni		= utf8_encode($FEDIT->Scarico[0]['prescrizioni_mov']);
	}
else{
	$scheda->flagPrescrizioniParticolari	= new Flag(false);
	}

// adr
if($FEDIT->Scarico[0]['adr']==1){
	$scheda->flagTrasportoADR	= new Flag(true);
	$scheda->classeADR			= new Catalogo($FEDIT->Scarico[0]['ID_CLASSE_ADR']);
	$scheda->numeroONU			= new Catalogo($FEDIT->Scarico[0]['ONU']);
	}
else
	$scheda->flagTrasportoADR = new Flag(false);

// analisi
if(!is_null($FEDIT->Scarico[0]['CAR_NumDocumento']) && trim($FEDIT->Scarico[0]['CAR_NumDocumento'])!=''){
	$scheda->numeroCertificato	= utf8_encode($FEDIT->Scarico[0]['CAR_NumDocumento']);
	$scheda->laboratorio		= utf8_encode($FEDIT->Scarico[0]['CAR_Laboratorio']);
	if($FEDIT->Scarico[0]['CAR_DataAnalisi']!=''){
		$splitted=explode("-",$FEDIT->Scarico[0]['CAR_DataAnalisi']);
		$giorno=$splitted[2];
		$mese=$splitted[1];
		$anno=$splitted[0];
		$DataCertificato = $anno."-".$mese."-".$giorno."T00:00:00";
		$scheda->dataCertificato	= $DataCertificato; //dateTime 2001-10-26T21:32:52
		}
	}

// trasportatore
$scheda->idSISSede_trasportatore	= $FEDIT->Scarico[0]['idSIS_T'];
$scheda->versioneSede_trasportatore = new LongNumber((string) $FEDIT->Scarico[0]['Versione_T']);

// destinatario
$scheda->idSISSede_destinatario		= $FEDIT->Scarico[0]['idSIS_D'];
$scheda->versioneSede_destinatario	= new LongNumber((string) $FEDIT->Scarico[0]['Versione_D']);
$scheda->autorizzazione				= utf8_encode($FEDIT->Scarico[0]['Autorizzazione_D']);

// altroTipoImballaggio
$scheda->altroTipoImballaggio		= utf8_encode($FEDIT->Scarico[0]['ALTRO_TIPO_IMBALLAGGIO']);




#
#	ALTRI PARAMETRI TRASMETTIBILI oggetto $scheda (schedaSISTRI_produttore)
#
#    $scheda->flagVeicoli_Dlgs_209_2003; // Flag
#    $scheda->flagVeicoli_Dlgs_Art231_152_2006; // Flag
#    $scheda->numeroVeicoli; // LongNumber
#    $scheda->listaAllegati; // DocumentData_Base
#    $scheda->codRec_1013; // Catalogo
#    $scheda->numeroNotifica; // string
#    $scheda->numeroSerieSpedizione; // string
#    $scheda->quantita_150101; // LongNumber
#    $scheda->quantita_150104; // LongNumber
#    $scheda->quantita_150102; // LongNumber
#    $scheda->quantita_150107; // LongNumber
#    $scheda->quantita_150103; // LongNumber
#    $scheda->quantitaTotale_150106; // LongNumber
#    $scheda->descrizioneAltroStatoFisico; // string
#    $scheda->posizioneRifiuto; // PosizioneRifiuto
#    $scheda->codRec_1013_Produttore; // Catalogo
#    $scheda->numeroNotifica_Produttore; // string
#    $scheda->numeroSerieSpedizione_Produttore; // string
#    $scheda->volume; // DoubleNumber
#    $scheda->flagCircuitoMicroraccolta; // Flag

#
#	ALTRI PARAMETRI TRASMETTIBILI oggetto $schedabase (schedaSISTRI_Base)
#
#    $schedabase->schedaSISTRI_trasportatore;	// SchedaSISTRI_Trasportatore
#    $schedabase->schedaSISTRI_destinatario;	// SchedaSISTRI_Destinatario
#    $schedabase->schedaSISTRI_prod_trasp_cp;	// SchedaSISTRI_Prod_Trasp_CP
#    $schedabase->idSISSedi_intermediari;		// string
#    $schedabase->versioniSedi_intermediari;	// LongNumber
#    $schedabase->idSISSede_consorzio;			// string
#    $schedabase->versioneSede_consorzio;		// LongNumber
#    $schedabase->idSISSede_unitaLocale;		// string
#    $schedabase->versioneSede_unitaLocale;		// LongNumber
#    $schedabase->dataOraConsclusioneProcesso;	// dateTime
#    $schedabase->causaleCreazione;				// Catalogo
#    $schedabase->causaleModifica;				// Catalogo

//die(var_dump($scheda));


$contatto = array(
    "nome"      => utf8_encode($FEDIT->Scarico[0]['USER_nome']),
    "cognome"   => utf8_encode($FEDIT->Scarico[0]['USER_cognome']),
    "telefono"  => utf8_encode($FEDIT->Scarico[0]['USER_telefono']),
    "email"     => utf8_encode($FEDIT->Scarico[0]['USER_email'])
	);


$CERT		= "certificates/".$SOGER->UserData['core_impianticodfisc']."/crt_key.pem";
$s			= new SIS_SOAP($SOGER->UserData['core_usersSIS_identity'], $CERT);

try {

    $schedabase = new SchedaSISTRI_Base;
	$schedabase->tipoRegCronologico          = $tipo_registro;
    $schedabase->annotazioni		         = $annotazioni;
    $schedabase->schedaSISTRI_produttore     = $scheda;
    $schedabase->nomePersonaDaContattare     = $contatto["nome"];
    $schedabase->cognomePersonaDaContattare  = $contatto["cognome"];
    $schedabase->telefonoPersonaDaContattare = $contatto["telefono"];
    $schedabase->emailPersonaDaContattare    = $contatto["email"];

	$UUID					= $s->generateUUID();
    $SchedaFirmataPerUpdate	= $s->GetSchedaFirmataPerUpdate(array( "idSISScheda" => $FEDIT->Scarico[0]['idSIS_scheda'], "scheda" => $schedabase));
    $numero_scheda = $FEDIT->Scarico[0]['numero_scheda'];

	$Response['Esito']					= "operazione ultimata, Scheda SISTRI numero $numero_scheda (id $idSIS_scheda) creata in SISTRI.";
	$Response['WellDone']				= true;
	$Response['idSIS_scheda']			= $idSIS_scheda;
	$Response['idSIS_movimentazione']	= $idSIS_movimentazione;
        $Response['numero_scheda']	= $numero_scheda;
	$Response['statoSchedaSistri']		= "BOZZA";
	}

catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);

	// Store error in DB
	$SQL ="INSERT INTO `debug_sis` (`UUID`, `ID_IMP`, `ID_USR`, `SIS_identity`, `workmode`, `idSIS_sede`, `idSIS_regCrono`, `regCrono_type`, `TIME`, `errorCode`, `errorMessage`, `RequestHeaders`, `Request`, `ResponseHeaders`, `Response`) ";
	$SQL.="VALUES ('".$UUID."', '".$SOGER->UserData['core_usersID_IMP']."', '".$SOGER->UserData['core_usersID_USR']."', '".$SOGER->UserData['core_usersSIS_identity']."', '".$SOGER->UserData['workmode']."', '".$SOGER->UserData['core_usersidSIS_sede']."', '".$SOGER->UserData['core_usersidSIS_regCrono']."', '".$SOGER->UserData['core_usersregCrono_type']."', CURRENT_TIMESTAMP, '".addslashes($SISexcept->errorCode)."', '".addslashes($SISexcept->errorMessage)."', '".addslashes($s->__getLastRequestHeaders())."', '".addslashes($s->__getLastRequest())."', '".addslashes($s->__getLastResponseHeaders())."', '".addslashes($s->__getLastResponse())."');";
    $FEDIT->SDBWrite($SQL,true,false);

	// Notice error
	$errorMessage = explode("]:", $SISexcept->errorMessage);

	$Response['Esito']					= htmlspecialchars($errorMessage[1]).".<br /><br />Se non è nota la causa dell'errore, aprire un ticket nella sezione \"Interoperabilità\" e descrivere la problematica riportando l'ID della transazione ".$UUID;
	$Response['WellDone']				= false;
	$Response['idSIS_scheda']			= 'null';
	$Response['idSIS_movimentazione']	= 'null';
        $Response['numero_scheda']	= 'null';
	$Response['statoSchedaSistri']		= 'null';
	}

echo json_encode($Response);

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>