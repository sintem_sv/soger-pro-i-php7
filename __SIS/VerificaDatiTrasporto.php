<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
require_once("SIS_SOAP_SSL.php");
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");

global $SOGER;

$Response	= Array();

try{

	$SQL ="SELECT adr, ID_AUTHT FROM user_movimenti_fiscalizzati WHERE ID_MOV_F=".$_POST['ID_MOV_F'].";";
	$FEDIT->SdbRead($SQL,"IDsT");

	# AUTOMEZZO
	if($_POST['automezzo']!='null' AND trim($_POST['automezzo'])!=''){
		
		$SQL ="SELECT ID_AUTO FROM user_automezzi WHERE REPLACE(description, ' ', '')='".trim($_POST['automezzo'])."' AND ID_AUTHT=".$FEDIT->IDsT[0]['ID_AUTHT'].";";
		$FEDIT->SdbRead($SQL,"ExistAutomezzo");
		
		if($FEDIT->DbRecsNum>0)
			$Response['ID_AUTO'] = $FEDIT->ExistAutomezzo[0]['ID_AUTO'];
		else
			$Response['ID_AUTO'] = 0;

		}
	else
		$Response['ID_AUTO'] = 'null';

	# RIMORCHIO
	if($_POST['rimorchio']!='null' AND trim($_POST['rimorchio'])!=''){
		
		$SQL ="SELECT ID_RMK FROM user_rimorchi WHERE REPLACE(description, ' ', '')='".trim($_POST['rimorchio'])."' AND ID_AUTHT=".$FEDIT->IDsT[0]['ID_AUTHT'].";";
		$FEDIT->SdbRead($SQL,"ExistRimorchio");
		
		if($FEDIT->DbRecsNum>0)
			$Response['ID_RMK'] = $FEDIT->ExistRimorchio[0]['ID_RMK'];
		else
			$Response['ID_RMK'] = 0;

		}
	else
		$Response['ID_RMK'] = 'null';

	}
catch (Exception $e) {
	echo $e->getMessage();
	}	


echo json_encode($Response);

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>