<?php

//
// Generazione dati per la firma di una registrazione di carico
// (firmare = inviare al SIS il risultato delle operazioni dell'applet)
//

require_once("SIS_SOAP.php");
$debug_email_to = 'programmazione@sintem.it';


// Dati utente
$userid = "paolo.vaccaneo6548";
$id_registro  = "1156374";

// URL per firmare una *registrazione*
$url_firma_reg = "http://195.88.6.144/test_envi/__SIS/firma/sis_sig_registrazione.php";
// URL per firmare una *scheda*
$url_firma_sch = "http://195.88.6.144/test_envi/__SIS/firma/sis_sig_scheda.php";


$s = new SIS_SOAP($userid);

// genero un l'elenco documenti da firmare in formato JSON

echo "[\n";

try {
    echo sig_reg_nuova($id_registro, "15.01.03", "02", 5000000000, "Imballaggi");
    //echo sig_firma(27260);
    //echo sig_firma(27258);
    //echo sig_reg(24089);
    //echo sig_reg_nuova($id_registro, "15.01.02", "02", 2000000000, "Contenitori");
} 

catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n\n";
    //var_dump($e->faultcode, $e->faultstring, $e->detail);
}

echo "]\n";


function sig_reg($idsis) {

    global $userid;
    global $s;
    global $url_firma_reg;
    global $debug_email_to;
    
    $dati = $s->GetRegistrazionePerFirma(array("idSISRegistrazioneCrono"=>$idsis));    
    printf('{"uid":"%s","hash":"%s","attach":null,"idsis":"%u","timestamp":"%u","target_url":"%s"},'."\n",
        $userid,
        $dati['documentDataPerFirma']->hash,
        $idsis,
        $dati['documentDataPerFirma']->istanteTemporaleGenerazioneDoc->long,
        $url_firma_reg
    );

    mail($debug_email_to, date('Y-m-d H:i:s')." DATI PER FIRMA ".$idsis, 
        sprintf("Encoded:\n%s\n\nHash:\n%s\n\ntimestamp: %u",
            $dati['documentDataPerFirma']->encodedDoc,
            $dati['documentDataPerFirma']->hash,
            $dati['documentDataPerFirma']->istanteTemporaleGenerazioneDoc->long
        )); 

}


function sig_reg_nuova($registro, $codice_cer, $stato_fisico, $quantita, $descrizione) {

    global $s;

    $reg = new RegistrazioneCrono_Base;
    $reg->codiceCerIIILivello = new Catalogo($codice_cer);
    $reg->statoFisicoRifiuto  = new Catalogo($stato_fisico);
    $reg->quantita            = new LongNumber($quantita);
    $reg->descrizioneRifiuto  = $descrizione;

	$idsis = $s->PutRegistrazioneCronoCarico(
                array(  
                "idSISRegistroCrono"=>$registro,
                "registrazione" => $reg, 
    )); 
    return sig_reg($idsis);
}



function sig_firma($idsis) {

    global $userid;
    global $s;
    global $url_firma_sch;
    global $debug_email_to;
    
    $dati = $s->GetSchedaPerFirma(array("idSISScheda"=>$idsis));    

    $s_data = array(
        'uid'       => $userid,
        'idsis'     => (string) $idsis,
        'hash'      => $dati['documentDataPerFirma']->hash,
        'attach'    => $dati['documentDataPerFirma']->hashesAllegati,
        'timestamp' => (string) 
          $dati['documentDataPerFirma']->istanteTemporaleGenerazioneDoc->long,
        'target_url'=> $url_firma_sch,
    );  

    mail($debug_email_to, date('Y-m-d H:i:s')." DATI PER FIRMA $idsis", 
             print_r($s_data, true));

    return json_encode($s_data);      
}









