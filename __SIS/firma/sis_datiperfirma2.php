<?php

//
// Generazione dati per la firma di una registrazione di carico
// (firmare = inviare al SIS il risultato delle operazioni dell'applet)
//

require_once("SIS_SOAP.php");
$debug_email_to = 'programmazione@sintem.it';


// Dati utente
$userid = "mario.blu0514";
$id_registro  = "20749";

// URL per firmare una *registrazione*
$url_firma_reg = "http://195.88.6.144/test_envi/__SIS/firma/sis_sig_registrazione.php";
// URL per firmare una *scheda*
$url_firma_sch = "http://195.88.6.144/test_envi/__SIS/firma/sis_sig_scheda.php";
// URL per annullare una *registrazione*
$url_annulla_reg  = "http://195.88.6.144/test_envi/__SIS/firma/sis_sig_reg_annulla.php";
// URL per modificare una *registrazione*
$url_modifica_reg = "http://195.88.6.144/test_envi/__SIS/firma/sis_sig_reg_modifica.php";


$s = new SIS_SOAP($userid);

// genero un l'elenco documenti da firmare in formato JSON

echo "[\n";

try {
    sig_reg(26481);
    //sig_reg_nuova($id_registro, "15.01.02", "02", 2000000000, "Contenitori");
    //sig_reg_nuova($id_registro, "15.01.03", "02", 5000000000, "Imballaggi");
    //sig_reg_annulla(24533, "Registrazione di test", "MR");
    //sig_reg_modifica(25428);
    //sig_reg(24180);
}

catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
    echo "Errore\t{$SISexcept->errorCode}:\n\t{$SISexcept->errorMessage}\n\n";
    //var_dump($e->faultcode, $e->faultstring, $e->detail);
}

echo "\n]";


function sig_reg($idsis) {

    global $userid;
    global $s;
    global $url_firma_reg;
    global $debug_email_to;
    
    $dati = $s->GetRegistrazionePerFirma(array("idSISRegistrazioneCrono"=>$idsis));   

    printf('{"uid":"%s","hash":"%s","attach":null,"idsis":"%u","timestamp":"%s","target_url":"%s"},',
        $userid,
        $dati['documentDataPerFirma']->hash,
        $idsis,
        $dati['documentDataPerFirma']->istanteTemporaleGenerazioneDoc->long,
        $url_firma_reg
    );

	$handler=fopen("log0.txt", "a");
	$log=sprintf('{"uid":"%s","hash":"%s","attach":null,"idsis":"%u","timestamp":"%s","target_url":"%s"},',
        $userid,
        $dati['documentDataPerFirma']->hash,
        $idsis,
        $dati['documentDataPerFirma']->istanteTemporaleGenerazioneDoc->long,
        $url_firma_reg);
	fwrite($handler, $log);
	fclose($handler);


}


function sig_reg_nuova($registro, $codice_cer, $stato_fisico, $quantita, $descrizione) {

    global $s;

    $reg = new RegistrazioneCrono_Base;
    $reg->codiceCerIIILivello = new Catalogo($codice_cer);
    $reg->statoFisicoRifiuto  = new Catalogo($stato_fisico);
    $reg->quantita            = new LongNumber($quantita);
    $reg->descrizioneRifiuto  = $descrizione;

	$idsis = $s->PutRegistrazioneCronoCarico(
                array(  
                "idSISRegistroCrono"=>$registro,
                "registrazione" => $reg, 
    )); 
    return sig_reg($idsis);
}



function sig_scheda($idsis) {

    global $userid;
    global $s;
    global $url_firma_sch;
    global $debug_email_to;
    
    $dati = $s->GetSchedaPerFirma(array("idSISScheda"=>$idsis));    

    $s_data = array(
        'uid'       => $userid,
        'idsis'     => (string) $idsis,
        'hash'      => $dati['documentDataPerFirma']->hash,
        'attach'    => $dati['documentDataPerFirma']->hashesAllegati,
        'timestamp' => (string) 
          $dati['documentDataPerFirma']->istanteTemporaleGenerazioneDoc->long,
        'target_url'=> $url_firma_sch,
    );  

    mail($debug_email_to, date('Y-m-d H:i:s')." DATI PER FIRMA $idsis", 
             print_r($s_data, true));

    return json_encode($s_data);      
}



function sig_reg_annulla($idsis, $causale, $codicecausale = "") {

    global $userid;
    global $s;
    global $url_annulla_reg;
    global $debug_email_to;
    
    if ($codicecausale)
        $dati = $s->RecuperaRegistrazionePerAnnullamentoConCausale(
            array("idSISRegistrazioneCrono"=>$idsis, 
                  "annotazioniCausale" => $causale, 
                  "codiceCausale" => new Catalogo($codicecausale)
                  ));    
    else
        $dati = $s->GetRegistrazionePerAnnullamento(
            array("idSISRegistrazioneCrono"=>$idsis, "causale" => $causale));  


    printf('{"uid":"%s","hash":"%s","attach":null,"idsis":"%u","timestamp":"%u","target_url":"%s","causale":"%s","codicecausale":"%s"},',
        $userid,
        $dati['documentDataPerFirma']->hash,
        $idsis,
        $dati['documentDataPerFirma']->istanteTemporaleGenerazioneDoc->long,
        $url_annulla_reg,
        $causale,
        $codicecausale
    );

    mail($debug_email_to, date('Y-m-d H:i:s')." ANNULLAMENTO ".$idsis, 
        sprintf("Encoded:\n%s\n\nHash:\n%s\n\ntimestamp: %u",
            $dati['documentDataPerFirma']->encodedDoc,
            $dati['documentDataPerFirma']->hash,
            $dati['documentDataPerFirma']->istanteTemporaleGenerazioneDoc->long
        ));  

}





function sig_reg_modifica($idsis) {

    global $userid;
    global $s;
    global $url_modifica_reg;
    global $debug_email_to;
    
    // Leggo la registrazione e modifico i dati   
    $dati = $s->GetRegistrazioneCrono(array("idSISRegistrazioneCrono"=>$idsis));
    $reg = $dati->registrazioneCrono_base;
    $reg->quantita = new LongNumber($reg->quantita->long - 1000);

    #var_dump($reg);

    $reg2 = new RegistrazioneCrono_Base;
    #$reg2->codiceCerIIILivello = new Catalogo($reg->codiceCerIIILivello->idCatalogo);
    #$reg2->statoFisicoRifiuto  = new Catalogo($reg->statoFisicoRifiuto->idCatalogo);
    #$reg2->quantita            = new LongNumber($reg->quantita->long - 1000);
    #$reg2->descrizioneRifiuto  = $reg->descrizioneRifiuto;
    
    $reg2->codiceCerIIILivello = new Catalogo("15.01.03");
    $reg2->statoFisicoRifiuto  = new Catalogo("02");
    $reg2->quantita            = new LongNumber(4900000000);
    $reg2->descrizioneRifiuto  = "Imballaggi";    
    
    var_dump($reg2);    
    
    #exit();
    $dati = $s->RecuperaRegistrazioneFirmataPerModifica(array(
                    "idSISRegistrazioneCrono" => $idsis, 
                    "registrazione" => $reg2, ));

    printf('{"uid":"%s","hash":"%s","attach":null,"idsis":"%u","timestamp":"%u","target_url":"%s","registrazione":"%s"},',
        $userid,
        $dati['documentDataPerFirma']->hash,
        $idsis,
        $dati['documentDataPerFirma']->istanteTemporaleGenerazioneDoc->long,
        $url_modifica_reg,
        json_encode($reg)
    );

    mail($debug_email_to, date('Y-m-d H:i:s')." ANNULLAMENTO ".$idsis, 
        sprintf("Encoded:\n%s\n\nHash:\n%s\n\ntimestamp: %u",
            $dati['documentDataPerFirma']->encodedDoc,
            $dati['documentDataPerFirma']->hash,
            $dati['documentDataPerFirma']->istanteTemporaleGenerazioneDoc->long
        ));  

}








