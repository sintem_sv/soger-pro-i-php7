<?php

//
// Firma di una registrazione di carico
//
// Con "firma" qui si intende: inviare al SIS il risultato delle operazioni 
// compiute dall'applet java per mezzo dei certificati sulla chiavetta.
// Lo script viene invocato dalla chiavetta che ne intercetta il messaggio di 
// uscita.
//

require_once("SIS_SOAP.php");

$DEBUG = 1;
$debug_email_to = 'programmazione@sintem.it';

$params = json_decode($_POST['data'], true);

// Per registrazione differita:
$params =  array(
    'uid'       => "mario.blu0514",
    'hash'      => trim(file_get_contents('differita/hash')),
    'sign'      => file_get_contents('differita/sign'),
    'cert'      => file_get_contents('differita/cert'),
    'idsis'     => trim(file_get_contents('differita/idsis')),
    'timestamp' => trim(file_get_contents('differita/timestamp')),
);


try {
    $s = new SIS_SOAP($params['uid']); 

    $dati = $s->FirmaRegistrazione(
        array(           
            "idSISRegistrazioneCrono"             => $params['idsis'],
            "hashRegistrazione"                   => $params['hash'],
            "hashRegistrazione_FIRMATAdaIdentity" => $params['sign'], 
            "certificatoX509_identity"            => $params['cert'], 
            "istanteTemporaleGenerazioneDoc"      => array( 'long' => $params['timestamp'])
        ));
    echo "OK Registrazione firmata."; // OK, operazione riuscita

} catch (Exception $e) {

    echo "ERR Registrazione: firma errata."; // KO, operazione fallita
	$debug=date('Y-m-d H:i:s')." ERRORE sig registrazione\n\n" 
            ."Exception details:\n----------\n".print_r($e->detail, true)."\n\n"
            ."POST params:     \n----------\n" . print_r($_POST, true)."\n\n"
            ."Full exception contents:\n----------\n" . print_r($e, true)."\n\n"
            ."Transaction dump:\n----------\n" . print_r($s->getLastTransaction(), true);
	echo $debug;

}
