<?php

//
// Firma di una registrazione di carico
//
// Con "firma" qui si intende: inviare al SIS il risultato delle operazioni 
// compiute dall'applet java per mezzo dei certificati sulla chiavetta.
// Lo script viene invocato dalla chiavetta che ne intercetta il messaggio di 
// uscita.
//

require_once("SIS_SOAP.php");

$DEBUG = 1;
$debug_email_to = 'programmazione@sintem.it';

try {

    $params = json_decode($_POST['data'], true);

    if ($DEBUG) {
        mail($debug_email_to, date('Y-m-d H:i:s')." BEGIN sig registrazione", 
            "params:     \n----------\n" . print_r($params, true)."\n\n"
        );
    }

    $s = new SIS_SOAP($params['uid']); 

    $dati = $s->FirmaRegistrazione(
        array(           
            "idSISRegistrazioneCrono"             => $params['idsis'],
            "hashRegistrazione"                   => $params['hash'],
            "hashRegistrazione_FIRMATAdaIdentity" => $params['sign'], 
            "certificatoX509_identity"            => $params['cert'], 
            "istanteTemporaleGenerazioneDoc"      => new LongNumber($params['timestamp'])
        ));
    echo "OK Registrazione firmata."; // OK, operazione riuscita

	
	$handler=fopen("log1.txt", "a");
	
	$log=date('Y-m-d H:i:s')." SUCCESS sig registrazione\n\n" 
            ."POST params:     \n----------\n" . print_r($_POST, true)."\n\n"
            ."Transaction dump:\n----------\n" . print_r($s->getLastTransaction(), true)
            ."\n\n";

	fwrite($handler, $log);
	fclose($handler);

    if ($DEBUG) {
        mail($debug_email_to, date('Y-m-d H:i:s')." SUCCESS sig registrazione", 
            "POST params:     \n----------\n" . print_r($_POST, true)."\n\n"
            ."Transaction dump:\n----------\n" . print_r($s->getLastTransaction(), true)
            ."\n\n"   
        );
    }

} catch (Exception $e) {

    //echo "ERR Registrazione: firma errata."; // KO, operazione fallita
	echo var_dump($params);

	$handler=fopen("log1.txt", "a");

	$log=date('Y-m-d H:i:s')." ERRORE sig registrazione\n\n"
            .	"Exception details:\n----------\n".print_r($e->detail, true)."\n\n"
            ."POST params:     \n----------\n" . print_r($_POST, true)."\n\n"
            ."Full exception contents:\n----------\n" . print_r($e, true)."\n\n"
            ."Transaction dump:\n----------\n" . print_r($s->getLastTransaction(), true)
            ."\n\n";

	fwrite($handler, $log);
	fclose($handler);


    if ($DEBUG) {
        mail($debug_email_to, date('Y-m-d H:i:s')." ERRORE sig registrazione", 
            "Exception details:\n----------\n".print_r($e->detail, true)."\n\n"
            ."POST params:     \n----------\n" . print_r($_POST, true)."\n\n"
            ."Full exception contents:\n----------\n" . print_r($e, true)."\n\n"
            ."Transaction dump:\n----------\n" . print_r($s->getLastTransaction(), true)
            ."\n\n"   
        );
    }
}
