<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");

$TableName	= $_POST['tab_impianto'];

switch($TableName){
	case "user_impianti_produttori":
		$Primary	= "ID_UIMP";
		break;
	case "user_impianti_trasportatori":
		$Primary	= "ID_UIMT";
		break;
	case "user_impianti_destinatari":
		$Primary	= "ID_UIMD";
		break;
	case "user_impianti_intermediari":
		$Primary	= "ID_UIMI";
		break;
		}

if($_POST['idSIS']<>0)
	$SQL="UPDATE ".$_POST['tab_impianto']." SET idSIS='".$_POST['idSIS']."', versione='".$_POST['versione']."' WHERE ".$Primary."=".$_POST['id_impianto'].";";
else
	$SQL="UPDATE ".$_POST['tab_impianto']." SET idSIS=NULL, versione=NULL WHERE ".$Primary."=".$_POST['id_impianto'].";";

$FEDIT->SDBWrite($SQL,true,false);

$Response = array();
$Response['classe']		= 'ui-state-highlight';
$Response['icon']		= 'ui-icon ui-icon-info';
$Response['message']	= 'Salvataggio completato';
echo json_encode($Response);


require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>