<div style="margin-top:20px;width:280px;background-color:#FFFFFF;padding-bottom:10px;">

	<div class="divName" style="text-align:center;height:25px;width:100%;background-color: #4D6980;color: #ffffff;"><span id="testjq" style="font-weight: bold;padding:5px;font-size: 13pt;font-family: verdana;text-transform: uppercase;">REGISTRO SISTRI</span></div>	
	
	<div style="padding:10px;width:244px;">

		<table style="table-layout:fixed;width: 100%;">

			<tr>
				<td style="font-size:8pt;"><b>Sede di riferimento:</b></td>
			</tr>

			<tr>
				<td style="font-size:8pt;text-align:right" id="DescrizioneSede">
					<?php
					if(!is_null($SOGER->UserData['core_userssede_description'])){
						echo $SOGER->UserData['core_userssede_description'];
						}
					else
						echo "non impostato";
					?>
				</td>
			</tr>

			<tr>
				<td style="font-size:8pt;"><b>Registro cronologico:</b></td>
			</tr>

			<tr>
				<td style="font-size:8pt;text-align:right" id="RegCrono">
					<?php
					if(!is_null($SOGER->UserData['core_usersidSIS_regCrono'])){
						$regCrono	 = $SOGER->UserData['core_usersregCrono_type']."-".$SOGER->UserData['core_usersidSIS_regCrono'];
						echo $regCrono;
						}
					else
						echo "non impostato";
					?>
				</td>
			</tr>

		</table>

	</div>

	<div id="EditSedeAndRegistro" style="cursor:pointer;margin:10px;width:240px;background-color:#5C7D99;text-align:center;height:20px;padding:10px;color:#fff;">
	Seleziona sede e registro
	</div>

</div>



<!-- HIDDEN DIALOG -->
<div id='jqpopup_EditSedeAndRegistro' title='Seleziona sede e registro'>
	<form name="Set_Sede" method="post" action="__SIS/Set_Sede.php">
		
		<table>
			
			<tr>
				<td><b>Sede di riferimento:</b></td>
			
				<td align="right">
					<select style='width: 150px; margin-left:5px; font-size:10;' name="Select_Sede" id="Select_Sede">
						<option value='0'>Seleziona la sede di riferimento</option>
					</select>
				</td>
				
				<td id="AjaxStatus_Sede"></td>
			</tr>

			<tr>
				<td><b>Registro cronologico:</b></td>

				<td align="right">
					<select style='width: 150px; margin-left:5px; font-size:10;' name="Select_RegistroCronologico" id="Select_RegistroCronologico">
						<option value='0'>Seleziona il registro cronologico</option>
					</select>
				</td>
				
				<td id="AjaxStatus_Registro"></td>
			</tr>

			<tr>
				<td colspan="2">
					<div class="ui-widget">
						<div id="DIV_AjaxCall" class="ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
							<p id="P_AjaxCall">
								<span id="SPAN_AjaxCall" style="float: left; margin-right: .3em;"></span>
								<span id="SPAN_AjaxCallTxt"></span>
							</p>
						</div>
					</div>
				</td>
			</tr>

		</table>

	</form>
</div>