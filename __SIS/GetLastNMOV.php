<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
require_once("SIS_SOAP_SSL.php");
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");

global $SOGER;

$sql = "SELECT MAX(NMOV) AS NMOV FROM user_movimenti_fiscalizzati WHERE ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "' AND NMOV<>9999999 ";
$sql.= "AND ".$SOGER->UserData['workmode']."=1;";
$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

if(is_null($FEDIT->DbRecordSet[0]["NMOV"])) {
	switch($SOGER->UserData['workmode']){
		case 'produttore':
			$TabImpianto	= 'user_impianti_produttori';
			$TabAzienda		= 'user_aziende_produttori';
			$PrimaryAz		= 'ID_AZP';
			break;
		case 'trasportatore':
			$TabImpianto	= 'user_impianti_trasportatori';
			$TabAzienda		= 'user_aziende_trasportatori';
			$PrimaryAz		= 'ID_AZT';
			break;
		case 'destinatario':
			$TabImpianto	= 'user_impianti_destinatari';
			$TabAzienda		= 'user_aziende_destinatari';
			$PrimaryAz		= 'ID_AZD';
			break;
		case 'intermediario':
			$TabImpianto	= 'user_impianti_intermediari';
			$TabAzienda		= 'user_aziende_intermediari';
			$PrimaryAz		= 'ID_AZI';
			break;
		}
	$sql = "SELECT NFirstMOV FROM ".$TabImpianto." ";
	$sql.= "JOIN ".$TabAzienda." ON ".$TabImpianto.".".$PrimaryAz." = ".$TabAzienda.".".$PrimaryAz." ";
	$sql.= "WHERE ".$TabAzienda.".ID_IMP = '".$SOGER->UserData["core_impiantiID_IMP"]."';";

	$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
	$NumMov_F = $FEDIT->DbRecordSet[0]["NFirstMOV"];
	} 
else {
	$NumMov_F = $FEDIT->DbRecordSet[0]["NMOV"]+1;
	}




if($SOGER->UserData['core_impiantiREG_IND']==1){
	$sql = "SELECT MAX(NMOV) AS NMOV FROM user_movimenti WHERE ID_IMP='" . $SOGER->UserData["core_impiantiID_IMP"] . "' AND NMOV<>9999999 ";
	$sql.= "AND ".$SOGER->UserData['workmode']."=1;";
	$FEDIT->SDBRead($sql,"DbRecordSet",true,false);

	if(is_null($FEDIT->DbRecordSet[0]["NMOV"])) {
		switch($SOGER->UserData['workmode']){
			case 'produttore':
				$TabImpianto	= 'user_impianti_produttori';
				$TabAzienda		= 'user_aziende_produttori';
				$PrimaryAz		= 'ID_AZP';
				break;
			case 'trasportatore':
				$TabImpianto	= 'user_impianti_trasportatori';
				$TabAzienda		= 'user_aziende_trasportatori';
				$PrimaryAz		= 'ID_AZT';
				break;
			case 'destinatario':
				$TabImpianto	= 'user_impianti_destinatari';
				$TabAzienda		= 'user_aziende_destinatari';
				$PrimaryAz		= 'ID_AZD';
				break;
			case 'intermediario':
				$TabImpianto	= 'user_impianti_intermediari';
				$TabAzienda		= 'user_aziende_intermediari';
				$PrimaryAz		= 'ID_AZI';
				break;
			}
		$sql = "SELECT NFirstMOV FROM ".$TabImpianto." ";
		$sql.= "JOIN ".$TabAzienda." ON ".$TabImpianto.".".$PrimaryAz." = ".$TabAzienda.".".$PrimaryAz." ";
		$sql.= "WHERE ".$TabAzienda.".ID_IMP = '".$SOGER->UserData["core_impiantiID_IMP"]."';";

		$FEDIT->SDBRead($sql,"DbRecordSet",true,false);
		$NumMov_I = $FEDIT->DbRecordSet[0]["NFirstMOV"];
		} 
	else {
		$NumMov_I = $FEDIT->DbRecordSet[0]["NMOV"]+1;
		}
	}
else
	$NumMov_I = 0;







echo $NumMov_F."|".$NumMov_I;

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>