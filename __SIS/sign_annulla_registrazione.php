<?php
error_reporting(E_ALL);

ini_set("display_errors", 1);
session_start();
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("SIS_SOAP_SSL.php");

//
// Firma per annullo di una registrazione di carico
//
// Con "firma" qui si intende: inviare al SIS il risultato delle operazioni 
// compiute dall'applet java per mezzo dei certificati sulla chiavetta.
// Lo script viene invocato dalla chiavetta che ne intercetta il messaggio di 
// uscita.
//


$params		= json_decode($_POST['data'], true);

$CERT		= "certificates/".$SOGER->UserData['core_impianticodfisc']."/crt_key.pem";

try {

	$s		= new SIS_SOAP($params['uid'],$CERT);
	$UUID	= $s->generateUUID();
	$codiceCausale  = $params['codiceCausale'];
	$TIPO	= $params['TIPO'];
	
	$soapdata = array(           
        "idSISRegistrazioneCrono"             => $params['idsis'],
        "hashRegistrazione"                   => $params['hash'],
        "hashRegistrazione_FIRMATAdaIdentity" => $params['sign'], 
        "certificatoX509_identity"            => $params['cert'], 
        "istanteTemporaleGenerazioneDoc"      => array("long" =>
		new SoapVar($params['timestamp'], 
					XSD_STRING, 
					"string", 
					"http://www.w3.org/2001/XMLSchema"), ),
		"identificativoUtenteGestionale"	  => $params['identificativoUtenteGestionale'],
		"annotazioniCausale"				  => $params['annotazioniCausale'],
		"codiceCausale"						  => $codiceCausale,
		);

	$dati	= $s->AnnullaRegistrazioneConCausale($soapdata);

	$rv2	= $s->GetRegistrazioneCrono(
			array(  
			"idSISRegistrazioneCrono"=>$params['idsis'],
			)
		);
	$statoRegistrazioniCrono = $rv2->statoRegistrazioniCrono->description;

	// aggiorno statoRegistrazioniCrono in tabella movimenti
	$SQL ="UPDATE user_movimenti_fiscalizzati SET statoRegistrazioniCrono='".$statoRegistrazioniCrono."', ANN_registrazione=1, ANN_registrazione_causale='".addslashes($params['annotazioniCausale'])."' ";
	if($TIPO=='C')
		$SQL.=", quantita=0, quantita_residua=0, qta_hidden=0, tara=0, pesoL=0, pesoN=0, NOTER=CONCAT(IFNULL(NOTER, ''), ' ', '".addslashes($params['QuantitaOriginale'])."') ";
	$SQL.="WHERE idSIS='".$params['idsis']."' AND ".$SOGER->UserData['workmode']."=1 AND ID_IMP='".$SOGER->UserData['core_usersID_IMP']."';";
	$FEDIT->SDBWrite($SQL,true,false);
        
        // idSIS = null in tabella legami c/s se � un carico
        if($TIPO=='C')
            $SQL ="UPDATE user_legami_carico_scarico SET CARICO_idSIS=NULL WHERE MOV_KEY='ID_MOV_F' AND CARICO_idSIS='".$params['idsis']."';";
        // elimino record se � uno scarico
        else
            $SQL ="DELETE FROM user_legami_carico_scarico WHERE MOV_KEY='ID_MOV_F' AND SCARICO_idSIS='".$params['idsis']."';";
        $FEDIT->SDBWrite($SQL,true,false);
	
	echo "OK Registrazione annullata."; // OK, operazione riuscita

} catch (SoapFault $e) {
	
	// Store error in DB	
	$SQL ="INSERT INTO `debug_sis` (`UUID`, `ID_IMP`, `ID_USR`, `SIS_identity`, `workmode`, `idSIS_sede`, `idSIS_regCrono`, `regCrono_type`, `TIME`, `errorCode`, `errorMessage`, `RequestHeaders`, `Request`, `ResponseHeaders`, `Response`) ";
	$SQL.="VALUES ('".$UUID."', '".$SOGER->UserData['core_usersID_IMP']."', '".$SOGER->UserData['core_usersID_USR']."', '".$SOGER->UserData['core_usersSIS_identity']."', '".$SOGER->UserData['workmode']."', '".$SOGER->UserData['core_usersidSIS_sede']."', '".$SOGER->UserData['core_usersidSIS_regCrono']."', '".$SOGER->UserData['core_usersregCrono_type']."', CURRENT_TIMESTAMP, 'ERR Registrazione: firma errata.', 'ERR Registrazione: firma errata.', '".addslashes($s->__getLastRequestHeaders())."', '".addslashes($s->__getLastRequest())."', '".addslashes($s->__getLastResponseHeaders())."', '".addslashes($s->__getLastResponse())."');";
    $FEDIT->SDBWrite($SQL,true,false);

	// Notice error
 
	echo "ERR Registrazione cronologica: firma errata. ".htmlspecialchars($e->getMessage).".<br /><br />Se non � nota la causa dell'errore, aprire un ticket nella sezione \"Interoperabilit�\" e descrivere la problematica riportando l'ID della transazione ".$UUID;

}


require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>