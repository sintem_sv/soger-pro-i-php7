<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("SIS_SOAP_SSL.php");

$CERT		= "certificates/".$SOGER->UserData['core_impianticodfisc']."/crt_key.pem";
$s			= new SIS_SOAP($SOGER->UserData['core_usersSIS_identity'], $CERT);

//$idSISMovimentazione		= '3126062'; --> INVEMET!
$idSISMovimentazione		= $_POST['idSISMovimentazione'];
$Response	= Array();


try{
	$UUID	= $s->generateUUID();
	$doc	= $s->GetPdfMovimentazione(Array("idSISMovimentazione"=>$idSISMovimentazione));

	$Response['WellDone']		= true;
	$Response['Esito']			= "Avvio del download";
	$Response['PDF_content']	= $doc["encodedDocument"];
	$Response['PDF_name']		= "Scheda_SISTRI_Movimentazione_".$idSISMovimentazione.".pdf";
	}
catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
	
	// Store error in DB	
	$SQL ="INSERT INTO `debug_sis` (`UUID`, `ID_IMP`, `ID_USR`, `SIS_identity`, `workmode`, `idSIS_sede`, `idSIS_regCrono`, `regCrono_type`, `TIME`, `errorCode`, `errorMessage`, `RequestHeaders`, `Request`, `ResponseHeaders`, `Response`) ";
	$SQL.="VALUES ('".$UUID."', '".$SOGER->UserData['core_usersID_IMP']."', '".$SOGER->UserData['core_usersID_USR']."', '".$SOGER->UserData['core_usersSIS_identity']."', '".$SOGER->UserData['workmode']."', '".$SOGER->UserData['core_usersidSIS_sede']."', '".$SOGER->UserData['core_usersidSIS_regCrono']."', '".$SOGER->UserData['core_usersregCrono_type']."', CURRENT_TIMESTAMP, '".addslashes($SISexcept->errorCode)."', '".addslashes($SISexcept->errorMessage)."', '".addslashes($s->__getLastRequestHeaders())."', '".addslashes($s->__getLastRequest())."', '".addslashes($s->__getLastResponseHeaders())."', '".addslashes($s->__getLastResponse())."');";
    $FEDIT->SDBWrite($SQL,true,false);
	
	// Notice error
	$errorMessage = explode("]:", $SISexcept->errorMessage);

	$Response['WellDone']		= false;
	$Response['Esito']			= htmlspecialchars($errorMessage[1]).".<br /><br />Se non Ŕ nota la causa dell'errore, aprire un ticket nella sezione \"InteroperabilitÓ\" e descrivere la problematica riportando l'ID della transazione ".$UUID;
	$Response['PDF_content']	= 'null';
	$Response['PDF_name']		= 'null';
	}

$Response = array_map('utf8_encode', $Response);
echo json_encode($Response);

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>