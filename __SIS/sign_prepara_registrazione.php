<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
session_id($_GET['SID']);
session_start();
require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("SIS_SOAP_SSL.php");

//
// Generazione dati per la firma di una registrazione di carico
// (firmare = inviare al SIS il risultato delle operazioni dell'applet)
//


// Dati utente
$userid			= $SOGER->UserData['core_usersSIS_identity'];
$id_registro            = $SOGER->UserData['core_usersidSIS_regCrono'];
$CERT			= "certificates/".$SOGER->UserData['core_impianticodfisc']."/crt_key.pem";

// URL per firmare una *registrazione*
$s          = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
$protocol   = substr(strtolower($_SERVER["SERVER_PROTOCOL"]), 0, strpos(strtolower($_SERVER["SERVER_PROTOCOL"]), "/")) . $s;
$loadFrom   = $protocol . "://" . $_SERVER['SERVER_NAME'] . "/";
if(substr($loadFrom, strlen($loadFrom)-6)!="soger/") $loadFrom.="soger/";

$url_firma_reg	= $loadFrom."__SIS/sign_registrazione.php";

$s          = new SIS_SOAP($userid, $CERT);

// genero un l'elenco documenti da firmare in formato JSON

echo "[";

try {
    $UUID	= $s->generateUUID();
    $dati	= $s->GetRegistrazionePerFirma(array("idSISRegistrazioneCrono"=>$_GET['id']));    
    $s_data = array(
            'uid'       => $userid,
            'idsis'     => (string) $_GET['id'],
            'hash'      => $dati['documentDataPerFirma']->hash,
            'attach'    => $dati['documentDataPerFirma']->hashesAllegati,
            'timestamp' => (string) 
              $dati['documentDataPerFirma']->istanteTemporaleGenerazioneDoc->long,
            'target_url'=> $url_firma_reg,
            'identificativoUtenteGestionale'	  => $SOGER->UserData['core_usersID_USR'],
            );
    echo json_encode($s_data);      
} 

catch (SoapFault $e) {
    $SISexcept = SIS_SOAP::getSISException($e);
	
    // Store error in DB	
    $SQL ="INSERT INTO `debug_sis` (`UUID`, `ID_IMP`, `ID_USR`, `SIS_identity`, `workmode`, `idSIS_sede`, `idSIS_regCrono`, `regCrono_type`, `TIME`, `errorCode`, `errorMessage`, `RequestHeaders`, `Request`, `ResponseHeaders`, `Response`) ";
    $SQL.="VALUES ('".$UUID."', '".$SOGER->UserData['core_usersID_IMP']."', '".$SOGER->UserData['core_usersID_USR']."', '".$SOGER->UserData['core_usersSIS_identity']."', '".$SOGER->UserData['workmode']."', '".$SOGER->UserData['core_usersidSIS_sede']."', '".$SOGER->UserData['core_usersidSIS_regCrono']."', '".$SOGER->UserData['core_usersregCrono_type']."', CURRENT_TIMESTAMP, '".addslashes($SISexcept->errorCode)."', '".addslashes($SISexcept->errorMessage)."', '".addslashes($s->__getLastRequestHeaders())."', '".addslashes($s->__getLastRequest())."', '".addslashes($s->__getLastResponseHeaders())."', '".addslashes($s->__getLastResponse())."');";
    $FEDIT->SDBWrite($SQL,true,false);

    // Notice error
    $errorMessage = explode("]:", $SISexcept->errorMessage);

    echo htmlspecialchars($errorMessage[1]).".<br /><br />Se non Ŕ nota la causa dell'errore, aprire un ticket nella sezione \"InteroperabilitÓ\" e descrivere la problematica riportando l'ID della transazione ".$UUID;
}

echo "]\n";


require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>