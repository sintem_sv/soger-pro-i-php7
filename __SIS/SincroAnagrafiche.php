<?php
session_start();
/*
error_reporting(E_ALL);
ini_set("display_errors", 1);
*/

require_once("../__scripts/Soger_includes.inc");
require_once("../__classes/ForgEdit2.class");
require_once("../__classes/ForgEdit.RegExp");
require_once("../__classes/DbLink.class");
require_once("../__includes/COMMON_wakeSoger.php");
require_once("../__includes/COMMON_wakeForgEdit.php");
require_once("SIS_SOAP_SSL.php");

$CERT		= "certificates/".$SOGER->UserData['core_impianticodfisc']."/crt_key.pem";
$SIS		= new SIS_SOAP($SOGER->UserData['core_usersSIS_identity'], $CERT);
$TableName	= $_POST['table'];
$PrimaryKey	= $_POST['primary'];


### LOG ###

if(isset($_SESSION[$SOGER->UserData['core_usersID_USR']]['SYNC_LOG'])){
	$log = $_SESSION[$SOGER->UserData['core_usersID_USR']]['SYNC_LOG'];
	}
else {
	$log ="";
	}
	
$log.="[ Utente ".$SOGER->UserData['core_usersusr']." | ".date('Y-m-d H:i:s')." ]\r\n\r\n";

### LOG ###

switch($TableName){
	case 'user_aziende_produttori':
		$CompanyType	="PRODUTTORE";
		break;
	case 'user_aziende_trasportatori':
		$CompanyType	="TRASPORTATORE";
		break;
	case 'user_aziende_destinatari':
		$CompanyType	="DESTINATARIO";
		break;
	case 'user_aziende_intermediari':
		$CompanyType	="INTERMEDIARIO";
		break;
	}

## Select records from TableName
$SQL="SELECT ".$PrimaryKey.", ".$TableName.".description as RagioneSociale, indirizzo, ".$TableName.".ID_COM, codfisc, lov_comuni_istat.description AS Comune FROM ".$TableName." JOIN lov_comuni_istat ON ".$TableName.".ID_COM=lov_comuni_istat.ID_COM WHERE ID_IMP='".$SOGER->UserData['core_usersID_IMP']."' AND ".$SOGER->UserData['workmode']."=1;";
$FEDIT->SdbRead($SQL,"Fornitori");

## Cycle company
if($FEDIT->DbRecsNum>0) {
	for($c=0;$c<count($FEDIT->Fornitori);$c++){
		
		try{			
			
			## Retrive data from SISTRI
			$AZIENDA	= $SIS->GetAzienda(Array("codiceFiscaleAzienda"=>$FEDIT->Fornitori[$c]['codfisc']));
			$RAG_SOCIALE= $AZIENDA->ragioneSociale;
			$VERSIONE	= $AZIENDA->versione->long;
			$ID_SIS		= $AZIENDA->idSIS;
			$INDIRIZZO	= $AZIENDA->sedeLegale->indirizzo." ".$AZIENDA->sedeLegale->nrCivico;
			$SQL		= "SELECT description FROM lov_comuni_istat WHERE ISTAT_comune='".$AZIENDA->sedeLegale->codiceIstatLocalita."'";
			$FEDIT->SdbRead($SQL,"Comune");
			$COMUNE		= $FEDIT->Comune[0]['description'];

			## Update Soger archive
			$SQL		= "UPDATE ".$TableName." SET versione='".$VERSIONE."', idSIS='".$ID_SIS."' WHERE ".$PrimaryKey."=".$FEDIT->Fornitori[$c][$PrimaryKey].";";
			$FEDIT->SDBWrite($SQL,true,false);
			//$log.="\r\n".$SQL."\r\n";

			## Retrive data from Soger
			$INDIRIZZO_PRO	= $FEDIT->Fornitori[$c]['indirizzo'];
			$COMUNE_PRO		= $FEDIT->Fornitori[$c]['Comune'];
			$RAG_SOCIALE_PRO= $FEDIT->Fornitori[$c]['RagioneSociale'];
			
			## Prepare LOG
			$log.="\r\n";
			$log.=$RAG_SOCIALE_PRO. " - SEDE LEGALE - ".$CompanyType."\r\n";
			$log.="Codice fiscale ".$FEDIT->Fornitori[$c]['codfisc']."\r\n";
			$log.="Dati So.Ge.R. PRO:    ".$RAG_SOCIALE_PRO." - ".$INDIRIZZO_PRO.", ".$COMUNE_PRO."\r\n";
			$log.="Dati archivio SISTRI: ".$RAG_SOCIALE." - ".$INDIRIZZO.", ".$COMUNE."\r\n";
			$log.="\r\n\r\n";
			}

		catch (SoapFault $e) {

			## Catch error message
			$SISexcept = SIS_SOAP::getSISException($e);

			## Retrive data from Soger
			$INDIRIZZO_PRO	= $FEDIT->Fornitori[$c]['indirizzo'];
			$COMUNE_PRO		= $FEDIT->Fornitori[$c]['Comune'];
			$RAG_SOCIALE_PRO= $FEDIT->Fornitori[$c]['RagioneSociale'];

			## Prepare LOG
			$log.="\r\n";
			$log.=$RAG_SOCIALE_PRO. " - SEDE LEGALE - ".$CompanyType."\r\n";
			$log.="Codice fiscale ".$FEDIT->Fornitori[$c]['codfisc']."\r\n";
			$log.="Dati So.Ge.R. PRO:    ".$RAG_SOCIALE_PRO." - ".$INDIRIZZO_PRO.", ".$COMUNE_PRO."\r\n";
			$log.="Dati archivio SISTRI: Nessuna azienda iscritta a SISTRI con il codice fiscale indicato; contattate l'azienda e verificate lo stato d'iscrizione.\r\n";
			//$log.="Qualora non sia obbligata all'iscrizione al SISTRI, utilizzate la funzione \"Inserisci azienda partner\".\r\n";
			//$log.="Error details: ".$SISexcept->errorCode." - ".$SISexcept->errorMessage."\r\n";
			$log.="\r\n\r\n";

			## Debug
			//echo "<h1>NO MATCH FOR CODICE FISCALE ".$FEDIT->Fornitori[$c]['codfisc']."</h1>";
			//echo var_dump($SISexcept);
			//echo "<br /><br /><hr><br /><br />";
			}
		}
	}

// Write log into txt file

/*
chdir("LOG/SINCRO/");
$LogFile	= $SOGER->UserData['core_usersID_IMP']."-".$SOGER->UserData['workmode'].".txt";
$fh			= fopen($LogFile, 'a');
fwrite($fh, $log);
// Close file handler
fclose($fh);
chdir("../../");
*/

// Write log into session var
$_SESSION[$SOGER->UserData['core_usersID_USR']]['SYNC_LOG'] = $log;

// return table name to update request status
echo $TableName;

require_once("../__includes/COMMON_sleepSoger.php");
require_once("../__includes/COMMON_sleepForgEdit.php");
?>