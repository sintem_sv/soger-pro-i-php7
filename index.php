<?php

session_start();
$JScript='';

// let's make sure the $_SERVER['DOCUMENT_ROOT'] variable is set
if(!isset($_SERVER['DOCUMENT_ROOT'])){ if(isset($_SERVER['SCRIPT_FILENAME'])){
$_SERVER['DOCUMENT_ROOT'] = str_replace( '\\', '/', substr($_SERVER['SCRIPT_FILENAME'], 0, 0-strlen($_SERVER['PHP_SELF'])));
}; };
if(!isset($_SERVER['DOCUMENT_ROOT'])){ if(isset($_SERVER['PATH_TRANSLATED'])){
$_SERVER['DOCUMENT_ROOT'] = str_replace( '\\', '/', substr(str_replace('\\\\', '\\', $_SERVER['PATH_TRANSLATED']), 0, 0-strlen($_SERVER['PHP_SELF'])));
}; };
// $_SERVER['DOCUMENT_ROOT'] is now set - you can use it as usual...



#
#	Libs
#
require_once("__libs/Sajax.php");
require_once("__libs/SQLFunct.php");
#
#	Application
#
require_once("__classes/Core.class");
require_once("__classes/Logic.class");
require_once("__classes/Visual.class");
require_once("__SIS/SIS_SOAP_SSL.php");
require_once("__includes/COMMON_wakeSoger.php");
//require_once("__includes/COMMON_wakeSIS.php");

if(isset($_GET["table"]) && isset($_GET["filter"]) && isset($_GET["table"])) {
	if(!isset($_GET["hash"])) {
			$DevWarn = "missing hash";
			$SOGER->SetFeedback($DevWarn,"1");
			$SOGER->AppLocation = "UserMainMenu";
			require_once("__includes/COMMON_sleepSoger.php");
			header("location: ./");
	} else {
		if(MakeUrlHash($_GET["table"],$_GET["pri"],$_GET["filter"])!=$_GET["hash"]) {
			$DevWarn = "hash mismatch";
			$SOGER->SetFeedback($DevWarn,"1");
			$SOGER->AppLocation = "UserMainMenu";
			require_once("__includes/COMMON_sleepSoger.php");
			header("location: ./");
		}

	}
}

## BLOCCO MOVIMENTI AUTORIZZAZIONI SCADUTE ##
if(isset($SOGER->UserData["core_usersAUT_SCAD_LOCK"])) {
	if($SOGER->UserData["core_usersAUT_SCAD_LOCK"]=="1"&&(
		$SOGER->AppLocation=="UserNuovoMovCarico"|
		$SOGER->AppLocation=="UserNuovoMovCaricoF"|
		$SOGER->AppLocation=="UserNuovoMovScarico"|
		$SOGER->AppLocation=="UserNuovoMovScaricoF"|
		$SOGER->AppLocation=="UserNuovaSchedaSistri"|
		$SOGER->AppLocation=="UserNuovaSchedaSistriF"|
		$SOGER->AppLocation=="UserNuovoMovInter")) {
		//require("__js/SOGER_AutLockOn.js");
		$JScript.=file_get_contents("__js/SOGER_AutLockOn.js");
	}else {
		//require("__js/SOGER_AutLockOff.js");
		$JScript.=file_get_contents("__js/SOGER_AutLockOff.js");
	}
}
else {
	//require("__js/SOGER_AutLockOff.js");
	$JScript.=file_get_contents("__js/SOGER_AutLockOff.js");
}


## BLOCCO MOVIMENTI SENZA C.PROPRIO/C.TERZI ##
if(isset($SOGER->UserData["core_usersCONTO_TERZI_LOCK"])) {

	if($SOGER->UserData["core_usersCONTO_TERZI_LOCK"]=="1"&&(
		$SOGER->AppLocation=="UserNuovoMovCarico"|
		$SOGER->AppLocation=="UserNuovoMovCaricoF"|
		$SOGER->AppLocation=="UserNuovoMovScarico"|
		$SOGER->AppLocation=="UserNuovoMovScaricoF"|
		$SOGER->AppLocation=="UserNuovaSchedaSistri"|
		$SOGER->AppLocation=="UserNuovaSchedaSistriF"|
		$SOGER->AppLocation=="UserNuovoMovInter"|
		$SOGER->AppLocation=="UserNuovoMovCaricoDettaglio"|
		$SOGER->AppLocation=="UserNuovoMovCaricoDettaglioF"|
		$SOGER->AppLocation=="UserNuovoMovScaricoDettaglio"|
		$SOGER->AppLocation=="UserNuovoMovScaricoDettaglioF"|
		$SOGER->AppLocation=="UserNuovoMovInterDettaglio")) {
		//require("__js/SOGER_ContoTerziLockOn.js");
		$JScript.=file_get_contents("__js/SOGER_ContoTerziLockOn.js");
		}
	else {
		//require("__js/SOGER_ContoTerziLockOff.js");
		$JScript.=file_get_contents("__js/SOGER_ContoTerziLockOff.js");
		}
	}
else {
	//require("__js/SOGER_ContoTerziLockOff.js");
	$JScript.=file_get_contents("__js/SOGER_ContoTerziLockOff.js");
	}

## BLOCCO MOVIMENTI CHE NON PASSANO VALIDAZIONE FDA ##
if(isset($SOGER->UserData["core_usersFDA_LOCK"]) && isset($SOGER->UserData["core_impiantiMODULO_FDA"])) {
    if(
        $SOGER->UserData["core_usersFDA_LOCK"]==1 &&
        $SOGER->UserData["core_impiantiMODULO_FDA"]==1 &&
        ($SOGER->AppLocation=="UserNuovoMovScaricoDettaglio" || $SOGER->AppLocation=="UserNuovoMovScaricoDettaglioF" || $SOGER->AppLocation=="UserNuovoMovScaricoDettaglioFiscale")
        ) {
        $JScript.=file_get_contents("__js/SOGER_FDALockOn.js");
    }
    else
        $JScript.=file_get_contents("__js/SOGER_FDALockOff.js");
}
else
    $JScript.=file_get_contents("__js/SOGER_FDALockOff.js");

## BLOCCO MOVIMENTI SENZA TRASPORTATORE ##
if(isset($SOGER->UserData["core_usersCHECK_TRASPORTATORE"])) {

	if($SOGER->UserData["core_usersCHECK_TRASPORTATORE"]=="1" && (
		$SOGER->AppLocation=="UserNuovoMovScarico" |
		$SOGER->AppLocation=="UserNuovoMovScaricoF" |
		$SOGER->AppLocation=="UserNuovaSchedaSistri" |
		$SOGER->AppLocation=="UserNuovaSchedaSistriF")){
		//require("__js/SOGER_CheckTrasportatoreOn.js");
		$JScript.=file_get_contents("__js/SOGER_CheckTrasportatoreOn.js");
		}
	else {
		//require("__js/SOGER_CheckTrasportatoreOff.js");
		$JScript.=file_get_contents("__js/SOGER_CheckTrasportatoreOff.js");
		}
	}
else {
	//require("__js/SOGER_CheckTrasportatoreOn.js");
	$JScript.=file_get_contents("__js/SOGER_CheckTrasportatoreOn.js");
	}

## BLOCCO MOVIMENTI SENZA DESTINATARIO ##
if(isset($SOGER->UserData["core_usersCHECK_DESTINATARIO"])) {

	if($SOGER->UserData["core_usersCHECK_DESTINATARIO"]=="1" && (
		$SOGER->AppLocation=="UserNuovoMovScarico" |
		$SOGER->AppLocation=="UserNuovoMovScaricoF" |
		$SOGER->AppLocation=="UserNuovaSchedaSistri" |
		$SOGER->AppLocation=="UserNuovaSchedaSistriF")){
		//require("__js/SOGER_CheckDestinatarioOn.js");
		$JScript.=file_get_contents("__js/SOGER_CheckDestinatarioOn.js");
		}
	else {
		//require("__js/SOGER_CheckDestinatarioOff.js");
		$JScript.=file_get_contents("__js/SOGER_CheckDestinatarioOff.js");
		}
	}
else {
	//require("__js/SOGER_CheckDestinatarioOn.js");
	$JScript.=file_get_contents("__js/SOGER_CheckDestinatarioOn.js");
	}

## VERIFICA PRESENZA C.PROPRIO/C.TERZI ##
if(isset($SOGER->UserData["core_usersCONTO_TERZI_CHECK"])) {
	if($SOGER->UserData["core_usersCONTO_TERZI_CHECK"]=="1"&&(
		$SOGER->AppLocation=="UserNuovoMovCarico"|
		$SOGER->AppLocation=="UserNuovoMovCaricoF"|
		$SOGER->AppLocation=="UserNuovoMovScarico"|
		$SOGER->AppLocation=="UserNuovoMovScaricoF"|
		$SOGER->AppLocation=="UserNuovaSchedaSistri"|
		$SOGER->AppLocation=="UserNuovaSchedaSistriF"|
		$SOGER->AppLocation=="UserNuovoMovInter"|
		$SOGER->AppLocation=="UserNuovoMovCaricoDettaglio"|
		$SOGER->AppLocation=="UserNuovoMovCaricoDettaglioF"|
		$SOGER->AppLocation=="UserNuovoMovScaricoDettaglio"|
		$SOGER->AppLocation=="UserNuovoMovScaricoDettaglioF"|
		$SOGER->AppLocation=="UserNuovoMovInterDettaglio")) {
		//require("__js/SOGER_ContoTerziCheckOn.js");
		$JScript.=file_get_contents("__js/SOGER_ContoTerziCheckOn.js");
		}
	else {
		//require("__js/SOGER_ContoTerziCheckOff.js");
		$JScript.=file_get_contents("__js/SOGER_ContoTerziCheckOff.js");
	}
}
else {
	//require("__js/SOGER_ContoTerziCheckOff.js");
	$JScript.=file_get_contents("__js/SOGER_ContoTerziCheckOff.js");
	}

## VERIFICA CONFERIMENTI ##
if(isset($SOGER->UserData["core_usersck_validazione_conferimento"]) && isset($SOGER->UserData["core_impiantiMODULO_FDA"]) ) {
    if($SOGER->UserData["core_usersck_validazione_conferimento"]=="1" && (
        $SOGER->AppLocation=="UserNuovoMovScaricoDettaglio" |
        $SOGER->AppLocation=="UserNuovoMovScaricoDettaglioF" |
        $SOGER->AppLocation=="UserNuovoMovScaricoDettaglioFiscale") && $SOGER->UserData["core_impiantiMODULO_FDA"]==1) {
            $JScript.=file_get_contents("__js/SOGER_FDACheckOn.js");
        }
    else {
        $JScript.=file_get_contents("__js/SOGER_FDACheckOff.js");
    }
}
else {
    $JScript.=file_get_contents("__js/SOGER_FDACheckOff.js");
}

if(!isset($SOGER->UserData['core_usersusr'])) $NotLoggedIn=true; else $NotLoggedIn=false;

if(!is_null($SOGER->AppLocation) && $NotLoggedIn)
	$SOGER->AppLocation = NULL;
if(!isset($_GET['App']))
	$SOGER->Locator('soger');
else
	$SOGER->Locator($_GET['App']);

$ChildLocation = array("UserNuovoMovSchedaAllegatoVII", "UserNuovoMovSchedaAllegatoVIIF", "UserNuovoMovSchedaAllegatoVIIFiscale", "UserNuovoMovScaricoAllegatoVII", "UserNuovoMovScaricoAllegatoVIIF", "UserNuovoMovScaricoAllegatoVIIFiscale", "UserNuovoMovScaricoDettaglio", "UserNuovoMovScaricoDettaglioF", "UserNuovoMovScaricoDettaglioFiscale", "UserNuovoMovCaricoDettaglio", "UserNuovoMovCaricoDettaglioF", "UserSchedaRifiutoCarattMacchinari", "UserSchedaRifiutoCarattProcessi", "UserSchedaRifiutoCarattImmagine", "UserNuovoProduttoreImpianto", "UserNuovoProduttoreAutorizzazioni", "UserNuovoProduttoreAllegati", "UserNuovoContrattoProduttore", "UserProduttoreContratti", "UserProduttoreContrattoCondizioni", "UserNuovoDestinatarioImpianto", "UserNuovoDestinatarioAutorizzazioni", "UserNuovoDestinatarioAllegati", "UserDestinatarioContratti", "UserNuovoContrattoDestinatario", "UserDestinatarioContrattoCondizioni", "UserNuovoIntermediarioImpianto", "UserNuovoIntermediarioAutorizzazioni", "UserNuovoIntermediarioAllegati", "UserIntermediarioContratti", "UserNuovoContrattoIntermediario", "UserIntermediarioContrattoCondizioni", "UserNuovoTrasportatoreImpianto", "UserNuovoTrasportatoreAutorizzazioni", "UserNuovoTrasportatoreAutomezzi", "UserNuovoTrasportatoreAutisti", "UserNuovoTrasportatoreRimorchi", "UserNuovoTrasportatoreAllegati", "UserNuovoContrattoTrasportatore", "UserTrasportatoreContratti", "UserTrasportatoreContrattoCondizioni");

if(!$_SESSION['PressBack']){
	if($_SESSION['PrevAppLocation'][count($_SESSION['PrevAppLocation'])-1]!=$SOGER->AppLocation && strpos($SOGER->AppLocation, "login")===false && strpos($SOGER->AppLocation, "UserMovimentiCosti")===false && !in_array($SOGER->AppLocation, $ChildLocation))
		$_SESSION['PrevAppLocation'][] = $SOGER->AppLocation;
	}
else{
	unset($_SESSION['PrevAppLocation'][count($_SESSION['PrevAppLocation'])-1]);
	$_SESSION['PressBack'] = false;
	}


$SOGER->MakeDIV();
//print_r("APP_LOCATION: ".$SOGER->AppLocation."<br/> APP_BACK_STATUS: ".$SOGER->AppBackStatus."<br/> PREV_APP_LOCATION: ".$SOGER->PrevAppLocation."<br/> SESSION_PREV_APP_LOCATION: ".var_dump($_SESSION['PrevAppLocation']));

// EXCEPTIONS TO STANDARD BACK STATUS
if(strpos($SOGER->AppLocation, "login")===false && strpos($SOGER->AppLocation, "Dettaglio")===false && strpos($SOGER->AppLocation, "UserMovimentiCosti")===false){
	switch($SOGER->AppLocation){
		case "UserSchedaRifiutoCarattMacchinari":
		case "UserSchedaRifiutoCarattProcessi":
		case "UserSchedaRifiutoCarattImmagine":
			$SOGER->AppBackStatus = "UserSchedeRifiuti_caratterizzazioni";
			break;
		case "UserNuovoProduttoreImpianto":
		case "UserNuovoProduttoreAutorizzazioni":
		case "UserNuovoProduttoreAllegati":
		case "UserNuovoContrattoProduttore":
		case "UserProduttoreContratti":
		case "UserProduttoreContrattoCondizioni":
		case "UserCondizioneContrattoProduttore":
			$SOGER->AppBackStatus = "UserSchedeProduttori";
			break;
		case "UserNuovoDestinatarioImpianto":
		case "UserNuovoDestinatarioAutorizzazioni":
		case "UserNuovoDestinatarioAllegati":
		case "UserDestinatarioContratti":
		case "UserNuovoContrattoDestinatario":
		case "UserDestinatarioContrattoCondizioni":
		case "UserCondizioneContrattoDestinatario":
			$SOGER->AppBackStatus = "UserSchedeDestinatari";
			break;
		case "UserNuovoIntermediarioImpianto":
		case "UserNuovoIntermediarioAutorizzazioni":
		case "UserNuovoIntermediarioAllegati":
		case "UserIntermediarioContratti":
		case "UserNuovoContrattoIntermediario":
		case "UserIntermediarioContrattoCondizioni":
		case "UserCondizioneContrattoIntermediario":
			$SOGER->AppBackStatus = "UserSchedeIntermediari";
			break;
		case "UserNuovoTrasportatoreImpianto":
		case "UserNuovoTrasportatoreAutorizzazioni":
		case "UserNuovoTrasportatoreAutomezzi":
		case "UserNuovoTrasportatoreAutisti":
		case "UserNuovoTrasportatoreRimorchi":
		case "UserNuovoTrasportatoreAllegati":
		case "UserNuovoContrattoTrasportatore";
		case "UserTrasportatoreContratti";
		case "UserTrasportatoreContrattoCondizioni";
		case "UserCondizioneContrattoTrasportatore":
			$SOGER->AppBackStatus = "UserSchedeTrasportatori";
			break;

		case "UserConfigControlli":
		case "UserConfigCostiViaggio":
		case "UserConfigStampe":
		case "UserConfigAllarmi":
		case "UserDB":
		case "UserRegistrazioneIntestatario":
		case "UserRegistrazione":
			$SOGER->AppBackStatus = "UserMainMenu";
			break;


		default:
			$SOGER->AppBackStatus = $_SESSION['PrevAppLocation'][count($_SESSION['PrevAppLocation'])-2];
			break;
		}
	}
/*
echo "DUMP SESSIONE:";
var_dump($_SESSION['PrevAppLocation']);
echo "SONO IN:";
var_dump($SOGER->AppLocation);
echo "TORNO IN:";
var_dump($SOGER->AppBackStatus);
*/

echo $SOGER->HTMLpack($JScript);

require_once("__includes/COMMON_sleepSoger.php");
//require_once("__includes/COMMON_sleepSIS.php");
?>